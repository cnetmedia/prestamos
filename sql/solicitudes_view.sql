CREATE VIEW
solicitudes_view (id, fecha_registro, origen, cliente, cedula, salario, cantidad_solicitada,
empresa, estado, disposicion, asignado, cliente_id, usuarios_id) AS
SELECT
    `solicitudes`.`id`                                    AS `id`,
    `solicitudes`.`fecha_registro`                        AS `fecha_registro`,
    `clientes`.`fuente_cliente`                           AS `origen`,
    concat(`clientes`.`nombre`,' ',`clientes`.`apellido`) AS `cliente`,
    `clientes`.`cedula`                                   AS `cedula`,
    `clientes`.`salario`                                  AS `salario`,
    `clientes`.`cantidad_solicitada`                      AS `cantidad_solicitada`,
    (
        SELECT
            `companias`.`nombre`
        FROM
            `companias`
        WHERE
            (
                `companias`.`id` = `clientes`.`companias_id`)) AS `empresa`,
    (
        SELECT
            `estados`.`estado`
        FROM
            `estados`
        WHERE
            (
                `estados`.`id` = `solicitudes`.`estados_id`)) AS `estado`,
    (
        SELECT
            `disposiciones`.`nombre`
        FROM
            `disposiciones`
        WHERE
            (
                `disposiciones`.`id` = `solicitudes`.`disposiciones_id`)) AS `disposicion`,
    (
        SELECT
            concat(`usuarios`.`nombres`,' ',`usuarios`.`apellidos`)
        FROM
            `usuarios`
        WHERE
            (
                `solicitudes`.`usuarios_id` = `usuarios`.`id`)) AS `asignado`,
    `clientes`.`id`                                             AS `cliente_id`,
    `solicitudes`.`usuarios_id`                                 AS `usuarios_id`
FROM
    (`solicitudes`
JOIN
    `clientes`)
WHERE
    (
        `solicitudes`.`clientes_id` = `clientes`.`id`);