<?php

    $data = $argv[1];
    $db = getcwd()."/db/".$data;

    if( is_file($db) )
    {

        $uname = explode(" ",php_uname());
        $os = $uname[0];
        switch ($os){
          case 'Windows':
            $driver = '{Microsoft Access Driver (*.mdb)}';
            break;
          case 'Linux':
            $driver = 'MDBTools';
            break;
          default:
            exit("Don't know about this OS");
        }

        $conexion = odbc_pconnect("Driver=".$driver.";DBQ=".$db,"","");
        if (!$conexion) {
            exit( "Error: No se pudo completar la conexion ");  
        } else {
            $sql = $argv[2];
            $rs = odbc_exec( $conexion, $sql);
            $array = array();
            while( $row = odbc_fetch_array($rs) ) { 
                $array[] = $row;
            }

            echo json_encode($array);
        }
    }else {
        exit("Error: No existe archivo ".$argv[1]);
    }

    odbc_close( $conexion );
