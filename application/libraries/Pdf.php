<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/pdfmerger/tcpdf/tcpdf.php';

class Pdf extends PDFMerger\TCPDF
{
	private $nombre_reporte;

    function __construct()
    {
        parent::__construct();
        $this->nombre_reporte = '';
    }    

    public function setNombreReporte($nombre_reporte){
    	$this->nombre_reporte = $nombre_reporte;
    	return $this;
    }

    public function getNombreReporte(){
    	return $this->nombre_reporte;
    }

    public function Header() {
    	$headerfont = $this->getHeaderFont();
		$headerdata = $this->getHeaderData();
		$this->SetTextColorArray($this->header_text_color);
		$this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);

		if($this->getNombreReporte() === 'condiciones_generales'){
			$this->SetX($this->GetX()+103);
		}
		
		$this->Image(K_PATH_IMAGES.$headerdata['logo'], '', '', $headerdata['logo_width']);		
	}    
}
/* application/libraries/Pdf.php */