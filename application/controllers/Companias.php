<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Companias extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('companias_model','admin_model','usuarios_model','notas_compania_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');

		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}

	}

	public function index()	{		
		$insertar = false;	
		if ($this->admin_model->get_insertar($this->roluser) == 1) { $insertar = true; }
		$data['insertar'] = $insertar;
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$this->load->view('templates/header',$data);
		$this->load->view('admin/companias/gestionar_companias',$data);
		$this->load->view('templates/footer',$data);
	}

	function check_default($post_string) {
		return $post_string == '0' ? FALSE : TRUE;
	}

	public function ajax_list()	{
		$list = $this->companias_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $companies) {
			$no++;
			$row = array();
			$row[] = $companies->nombre;
			$row[] = $companies->ruc;
			$row[] = $companies->telefono;
			$row[] = $companies->correo;
			//add html for action
			if ($this->admin_model->get_modificar($this->roluser) == 1 && $this->admin_model->get_eliminar($this->roluser) == 1) {
				$row[] = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="notes_companies('."'".$companies->id."'".')"><i class="fa fa-file"></i> Notas </a>
				<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_companies('."'".$companies->id."'".')"><i class="fa fa-folder"></i> Ver </a>
				<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_companies('."'".$companies->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
				<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$companies->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
			} elseif ($this->admin_model->get_modificar($this->roluser) == 1) {
				$row[] = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="notes_companies('."'".$companies->id."'".')"><i class="fa fa-file"></i> Notas </a>
				<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_companies('."'".$companies->id."'".')"><i class="fa fa-folder"></i> Ver </a>
				<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_companies('."'".$companies->id."'".')"><i class="fa fa-pencil"></i> Editar</a>';
			} elseif ($this->admin_model->get_eliminar($this->roluser) == 1) {
				$row[] = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="notes_companies('."'".$companies->id."'".')"><i class="fa fa-file"></i> Notas </a>
				<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_companies('."'".$companies->id."'".')"><i class="fa fa-folder"></i> Ver </a>
				<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$companies->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
			} else {
				$row[] = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="notes_companies('."'".$companies->id."'".')"><i class="fa fa-file"></i> Notas </a>
				<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_companies('."'".$companies->id."'".')"><i class="fa fa-folder"></i> Ver </a>';
			}		
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->companias_model->count_all(),
			"recordsFiltered" => $this->companias_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add() {
		if ($this->admin_model->get_insertar($this->roluser) == 1) {
			$this->form_validation->set_rules('nombre','nombre de la compañia','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('direccion','dirección','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono','teléfono','required|trim|xss_clean');
			$this->form_validation->set_rules('ruc','ruc','trim|xss_clean');
			$this->form_validation->set_rules('sitio_web','sitio web','trim|xss_clean');
			$this->form_validation->set_rules('ano_constitucion','año de constitución','required|trim|xss_clean');
			$this->form_validation->set_rules('correo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('nombre_rrhh','nombre del recursos humano','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono_rrhh','teléfono del recursos humano','required|trim|xss_clean');
			$this->form_validation->set_rules('nombre_contabilidad','nombre de contabilidad','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono_contabilidad','teléfono de contabilidad','required|trim|xss_clean');
			$this->form_validation->set_rules('forma_pago','forma de pagos','required|trim|xss_clean|callback_check_default');
			$this->form_validation->set_rules('cantidad_empleados','cantidad de empleados','required|trim|xss_clean');
			$this->form_validation->set_rules('rubro','rubro','required|trim|xss_clean');
			$this->form_validation->set_rules('nombre_firma','nombre firma','required|trim|xss_clean');
			$this->form_validation->set_rules('posicion_firma','posición firma','required|trim|xss_clean');

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));

/*				echo json_encode(array(
					'validate'=>FALSE, 
					'nombre'=>form_error('nombre')
				));*/

			} else {
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'direccion' => $this->input->post('direccion'),
					'telefono' => $this->input->post('telefono'),
					'ruc' => $this->input->post('ruc'),
					'sitio_web' => $this->input->post('sitio_web'),
					'ano_constitucion' => $this->input->post('ano_constitucion'),
					'correo' => $this->input->post('correo'),
					'nombre_rrhh' => $this->input->post('nombre_rrhh'),
					'telefono_rrhh' => $this->input->post('telefono_rrhh'),
					'nombre_contabilidad' => $this->input->post('nombre_contabilidad'),
					'telefono_contabilidad' => $this->input->post('telefono_contabilidad'),
					'forma_pago' => $this->input->post('forma_pago'),
					'cantidad_empleados' => $this->input->post('cantidad_empleados'),
					'rubro' => $this->input->post('rubro'),
					'nombre_firma' => $this->input->post('nombre_firma'),
					'posicion_firma' => $this->input->post('posicion_firma'),
					'usuarios_id' => $this->session->userdata('id_user'),
					'fecha_creacion' =>  date('Y-m-d H:i:s')
				);
				$insert = $this->companias_model->save($data);
				echo json_encode(array("status" => TRUE));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para añadir compañias.'));
		}
	}

	public function ajax_view($id) {
		$data = $this->companias_model->get_by_id($id);
		$user_creator = $this->usuarios_model->get_by_id($data->usuarios_id);
		$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
		$cadena = $user_creator->nombres;
		$nombre = explode(' ',$cadena);
		$cadena1 = $user_creator->apellidos;
		$apellido = explode(' ',$cadena1);
		$data->usuarios_id = $nombre[0] . " " . $apellido[0] . ", <b>" . $rol_creator->descripcion . ".</b>";
		echo json_encode($data);
	}

	public function ajax_edit($id) {
		if ($this->admin_model->get_modificar($this->roluser) == 1){
			$data = $this->companias_model->get_by_id($id);
			echo json_encode($data);
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar compañias.'));
		}

	}

	public function ajax_update() {
		if ($this->admin_model->get_modificar($this->roluser) == 1){
			$this->form_validation->set_rules('nombre','nombre de la compañia','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('direccion','dirección','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono','teléfono','required|trim|xss_clean');
			$this->form_validation->set_rules('ruc','ruc','trim|xss_clean');
			$this->form_validation->set_rules('sitio_web','sitio web','trim|xss_clean');
			$this->form_validation->set_rules('ano_constitucion','año de constitución','required|trim|xss_clean');
			$this->form_validation->set_rules('correo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('nombre_rrhh','nombre del recursos humano','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono_rrhh','teléfono del recursos humano','required|trim|xss_clean');
			$this->form_validation->set_rules('nombre_contabilidad','nombre de contabilidad','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono_contabilidad','teléfono de contabilidad','required|trim|xss_clean');
			$this->form_validation->set_rules('forma_pago','forma de pagos','required|trim|xss_clean|callback_check_default');
			$this->form_validation->set_rules('cantidad_empleados','cantidad de empleados','required|trim|xss_clean');
			$this->form_validation->set_rules('rubro','rubro','required|trim|xss_clean');
			$this->form_validation->set_rules('nombre_firma','nombre firma','required|trim|xss_clean');
			$this->form_validation->set_rules('posicion_firma','posición firma','required|trim|xss_clean');

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'direccion' => $this->input->post('direccion'),
					'telefono' => $this->input->post('telefono'),
					'ruc' => $this->input->post('ruc'),
					'sitio_web' => $this->input->post('sitio_web'),
					'ano_constitucion' => $this->input->post('ano_constitucion'),
					'correo' => $this->input->post('correo'),
					'nombre_rrhh' => $this->input->post('nombre_rrhh'),
					'telefono_rrhh' => $this->input->post('telefono_rrhh'),
					'nombre_contabilidad' => $this->input->post('nombre_contabilidad'),
					'telefono_contabilidad' => $this->input->post('telefono_contabilidad'),
					'forma_pago' => $this->input->post('forma_pago'),
					'cantidad_empleados' => $this->input->post('cantidad_empleados'),
					'rubro' => $this->input->post('rubro'),
					'nombre_firma' => $this->input->post('nombre_firma'),
					'posicion_firma' => $this->input->post('posicion_firma'),
				);
				$this->companias_model->update(array('id' => $this->input->post('id')), $data);
				echo json_encode(array("status" => TRUE));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar compañias.'));
		}
	}

	public function ajax_delete($id) {
		if ($this->admin_model->get_eliminar($this->roluser) == 1) {
			$this->companias_model->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para eliminar.'));
		}
	}

	public function add_notes(){
		$this->form_validation->set_rules('nota','nota','required|trim|xss_clean');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$data = array(
				'nota' => $this->input->post('nota'),
				'companias_id' => $this->input->post('cid'),
				'usuarios_id' => $this->session->userdata('id_user'),
				'fecha_creacion' =>  date('Y-m-d H:i:s')
			);
			$insert = $this->notas_compania_model->save($data);
			echo json_encode(array("status" => TRUE));
		}
	}

	public function notas_compania($companias_id){
		$data = $this->notas_compania_model->get_by_companias_id($companias_id);

		foreach ($data as $row) {
			$user_creator = $this->usuarios_model->get_by_id($row->usuarios_id);
			$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
			$cadena = $user_creator->nombres;
			$nombre = explode(' ',$cadena);
			$cadena1 = $user_creator->apellidos;
			$apellido = explode(' ',$cadena1);
			$row->usuarios_id = $nombre[0] . " " . $apellido[0] . ", <b>" . $rol_creator->descripcion . ".</b>";
		}

		echo json_encode($data);
	}

}