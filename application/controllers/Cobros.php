<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Cobros extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('cobros_model','aprobacion_model','admin_model','clientes_model','usuarios_model','companias_model','productos_model','cotizacion_model','solicitudes_model','agenda_retiro_model','desempleados_model','cobros_view_model','solicitudes_aprobacion_model','cobrados_view_model','desempleados_view_model', 'deuda_total_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('download', 'file', 'url', 'html', 'form','mysql_to_excel_helper'));
		$this->load->database('default');

		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}
		//$this->preparar_database();
	}
	
	public function index()	{
		$data['title'] = "Prestamos 911";
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['estado_cobros'] = $this->cobros_model->get_estado_cobros();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/cobros/gestionar_cobros',$data);
		$this->load->view('templates/footer',$data);
	}

	function check_default($post_string) {
		return $post_string == '0' ? FALSE : TRUE;
	}

	/*	
	public function ajax_list($compania_id = NULL)	{

		if ($compania_id != NULL) {
			$compania = $this->companias_model->get_by_id($compania_id);
			$_POST['search']['value'] = $compania->nombre;
		}		

		$list = $this->cobros_view_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = "$".$fila->cantidad_aprobada;
			$row[] = date('d-m-Y', strtotime(str_replace("/","-",$fila->fecha_entrega)));
			$row[] = date('d-m-Y', strtotime(str_replace("/","-",$fila->fecha_primer_descuento)));

			$cobros = $this->cobros_model->cobros_by_aprobacion_id($fila->id);
			if ($cobros) {
				$cobroarray = array();
				foreach ($cobros as $filacobro) {
					$cobroarray[] = $filacobro->id;
				}
				$cobroid = max($cobroarray);
				$cobro = $this->cobros_model->get_by_id($cobroid);
				$fec = date("d-m-Y", strtotime("$cobro->fecha_correspondiente + 1 months"));
			} else {
				$fec = date("d-m-Y", strtotime("$fila->fecha_primer_descuento + 46 days"));
				$fm = explode('-', $fec);
				if ($fm[0] == 16 || $fm[0] == 31) {
					$fec = date("d-m-Y", strtotime("$fila->fecha_primer_descuento + 45 days"));
				} elseif ($fm[0] == 14) {
					$fec = date("d-m-Y", strtotime("$fila->fecha_primer_descuento + 47 days"));
				} elseif ($fm[0] == 17) {
					$fec = date("d-m-Y", strtotime("$fila->fecha_primer_descuento + 44 days"));
				}

				$feb = date('d-m-Y', strtotime(str_replace("/","-",$fila->fecha_primer_descuento)));

				$fech1 = date("d-m-Y", strtotime($feb));
				$t1 = explode('-', $fech1);
				if ($t1[0] == 15 && $t1[1] == 1) {
					$fech2 = date("d-m-Y", strtotime($fec));
					$t2 = explode('-', $fech2);
					if ($t2[1] > 2) {
						$diaF = $t2[0];
						$fec = date("d-m-Y", strtotime("$fec - $diaF days"));
					}
				}

				$fmb = explode('-', $feb);
				if ($fmb[1] == 2 && $fm[0] < 15) {
					$fm1 = explode('-', $fec);
					$mdia = $fm1[0];
					$fec = date("d-m-Y", strtotime("$fec - $mdia days"));
					$fm2 = explode('-', $fec);
					if ($fm2[0] == 31) {
						$fec = date("d-m-Y", strtotime("$fec - 1 days"));
					}
				}

			}
			$row[] = $fec;

			$row[] = $fila->compania;			
			$row[] = $fila->estado_cobro;
			if ($this->roluser == 1 ){
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="remove_payment('."'".$fila->id."'".')"><i class="fa fa-usd"></i> Pagos </a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->clientes_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';
			} else {
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>';
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cobros_view_model->count_all(),
			"recordsFiltered" => $this->cobros_view_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}*/

	public function ajax_list($compania_id = NULL)	{
		if ($compania_id != NULL) {
			$compania = $this->companias_model->get_by_id($compania_id);
			$_POST['search']['value'] = $compania->nombre;
		}		

		$list = $this->cobros_view_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = "$".$fila->cantidad_aprobada;
			$row[] = $fila->fecha_entrega;
			$row[] = $fila->fecha_primer_descuento;
			$row[] = $fila->ultimo_pago;
			$row[] = $fila->monto;
			$row[] = $fila->fecha_pendiente;
			$row[] = $fila->compania;			
			$row[] = $fila->estado_cobro;

			$verify_moroso = $this->cobros_view_model->verify_morosos($fila->id,date('Y-m-d'));
			$moroso = '';
			if ($verify_moroso) {
				$moroso = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_morosos('."'".$fila->id."'".')"><i class="fa fa-envelope-o"></i> Moroso </a>';
			}

			if ($this->roluser == 1 ){
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="remove_payment('."'".$fila->id."'".')"><i class="fa fa-usd"></i> Pagos </a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->clientes_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>'.$moroso . 
					'<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->clientes_id."'".')"><i class="fa fa-file"></i> Notas</a>';
			} else {
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>'.$moroso .
					'<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->clientes_id."'".')"><i class="fa fa-file"></i> Notas</a>';
			}
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cobros_view_model->count_all(),
			"recordsFiltered" => $this->cobros_view_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_list_sin_garantias($compania_id = NULL)	{
		$estado = $this->input->get('estado');
		if ($compania_id != NULL) {
			$compania = $this->companias_model->get_by_id($compania_id);
			$_POST['search']['value'] = $compania->nombre;
		}		

		$list = $this->cobros_view_model->get_datatables("singarantias",$estado);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = "$".$fila->cantidad_aprobada;
			$row[] = $fila->fecha_entrega;
			$row[] = $fila->fecha_primer_descuento;
			$row[] = $fila->ultimo_pago;
			$row[] = $fila->monto;
			$row[] = $fila->fecha_pendiente;
			$row[] = $fila->compania;			
			$row[] = $fila->estado_cobro;

			$verify_moroso = $this->cobros_view_model->verify_morosos($fila->id,date('Y-m-d'));
			$moroso = '';
			if ($verify_moroso) {
				$moroso = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_morosos('."'".$fila->id."'".')"><i class="fa fa-envelope-o"></i> Moroso </a>';
			}

			if ($this->roluser == 1 ){
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="remove_payment('."'".$fila->id."'".')"><i class="fa fa-usd"></i> Pagos </a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->clientes_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>'.$moroso .
					'<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->clientes_id."'".')"><i class="fa fa-file"></i> Notas</a>';
			} else {
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>'.$moroso . 
					'<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->clientes_id."'".')"><i class="fa fa-file"></i> Notas</a>';
			}
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cobros_view_model->count_all("singarantias"),
			"recordsFiltered" => $this->cobros_view_model->count_filtered("singarantias"),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_list_mes_actual() {
		$hoy = date('Y-m');
		$_POST['search']['value'] = $hoy;		

		$list = $this->cobros_view_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = "$".$fila->cantidad_aprobada;
			$row[] = $fila->fecha_entrega;
			$row[] = $fila->fecha_primer_descuento;
			$row[] = $fila->ultimo_pago;
			$row[] = $fila->monto;
			$row[] = $fila->fecha_pendiente;
			$row[] = $fila->compania;			
			$row[] = $fila->estado_cobro;
			if ($this->roluser == 1 ){
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="remove_payment('."'".$fila->id."'".')"><i class="fa fa-usd"></i> Pagos </a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->clientes_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';
			} else {
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>';
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cobros_view_model->count_all(),
			"recordsFiltered" => $this->cobros_view_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}


	public function ajax_list_by_empresas($compania_id = NULL)	{

		if ($compania_id != NULL) {
			$compania = $this->companias_model->get_by_id($compania_id);
			$list = $this->cobros_view_model->get_by_company($compania->nombre);
		} else {
			$list = $this->cobros_view_model->get_all();
		}
		
		$data = array();
		$num = -1;
		foreach ($list as $fila) {
			$row = array();
			$num++;

			$row[] = "<center><input type='checkbox' id='cobro".$num."' value='".$fila->id."' onclick='onecheck()'></center>";

			$row[] = $fila->cliente;

			$dato = $this->return_verify_cobro($fila->id);
			if (isset($dato['status'])) {
				if ($dato['status'] == TRUE) {
					$this->status_cobro_empresa($fila->id);
				}
			}
			$fec = date("d-m-Y", strtotime($dato['fecha']));

			$row[] = $fec;

			$hoy = date('d-m-Y');

			$principal = $dato['deuda'];
			$interes = $dato['interes'];
			$tasa = $dato['tasa'];
			$cargo = $dato['cargo'];
			$total = $dato['total'];

			$row[] = "$" . number_format($principal,2);
			$row[] = "$" . number_format($interes,2);
			$row[] = "%" . number_format($tasa,2);
			$row[] = "$" . number_format($cargo,2);
			$row[] = "$" . number_format($total,2);

			$cotizacion = $this->cobro_calculos($fila->id);
			$row[] = "$" . number_format($cotizacion['data'][1]['total'],2);

			$data[] = $row;

		}
		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_list_by_empresas_mes()	{

		$list = $this->cobros_view_model->get_all();
		$data = array();
		$num = -1;
		$num1 = 0;
		foreach ($list as $fila) {

			$date = date('d-m-Y');
			$date = explode('-', $date);
			$mesactual = $date[1];
			$anhoactual = $date[2];

			$cantcobros = $this->cobros_model->get_by_aprobacion_id($fila->id);

			$dato = $this->return_verify_cobro($fila->id);
			$fec0 = date("d-m-Y", strtotime($dato['fecha']));

			$date1 = explode('-', $fec0);
			$mescobrar = $date1[1];
			$anhocobrar = $date1[2];

			if ($mesactual == $mescobrar && $anhoactual == $anhocobrar) {
				$row = array();
				$num++;
				$num1++;

				$row[] = "<center><input type='checkbox' id='cobro".$num."' value='".$fila->id."' onclick='onecheck()'></center>";

				$row[] = $fila->cliente;

				$fec = date("d-m-Y", strtotime($dato['fecha']));
				$row[] = $fec;

				$hoy = date('d-m-Y');

				$principal = $dato['deuda'];
				$interes = $dato['interes'];
				$tasa = $dato['tasa'];
				$cargo = $dato['cargo'];
				$total = $dato['total'];

				$row[] = "$" . number_format($principal,2);
				$row[] = "$" . number_format($interes,2);
				$row[] = "%" . number_format($tasa,2);
				$row[] = "$" . number_format($cargo,2);
				$row[] = "$" . number_format($total,2);
				
				$cotizacion = $this->cobro_calculos($fila->id);
				$row[] = "$" . number_format($cotizacion['data'][1]['total'],2);

				$data[] = $row;
			}

		}
		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	/*	
	public function ajax_list_desempleados()	{
		
		$list = $this->desempleados_view_model->get_all();
		$data = array();
		foreach ($list as $fila) {
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = "$".$fila->cantidad_aprobada;

			$cantcobros = $this->cobros_model->get_by_aprobacion_id($fila->id);
			$fec = date("d-m-Y", strtotime("$fila->fecha_primer_descuento + $cantcobros months"));
			$row[] = $fec;

			$row[] = $fila->compania;
			$row[] = $fila->estado_cobro;
			if ($this->roluser == 1 ){
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="remove_payment('."'".$fila->id."'".')"><i class="fa fa-usd"></i> Pagos </a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';
			} else {
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>';
			}
			$data[] = $row;
		}
		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}*/

	public function ajax_list_desempleados(){		
		$list = $this->desempleados_view_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = "$".$fila->cantidad_aprobada;
			$row[] = $fila->fecha_entrega;
			$row[] = $fila->fecha_primer_descuento;
			$row[] = $fila->ultimo_pago;
			$row[] = $fila->monto;
			$row[] = $fila->fecha_pendiente;
			$row[] = $fila->compania;			
			$row[] = $fila->estado_cobro;
			if ($this->roluser == 1 ){
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="remove_payment('."'".$fila->id."'".')"><i class="fa fa-usd"></i> Pagos </a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->clientes_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>' .
					'<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->clientes_id."'".')"><i class="fa fa-file"></i> Notas</a>';
			} else {
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>' . 
					'<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->clientes_id."'".')"><i class="fa fa-file"></i> Notas</a>';
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->desempleados_view_model->count_all(),
			"recordsFiltered" => $this->desempleados_view_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
	

	public function ajax_list_completos()	{
		$list = $this->cobrados_view_model->get_all();
		$data = array();
		foreach ($list as $fila) {
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = $fila->producto;
			$row[] = '$'.$fila->cantidad_aprobada;
			$row[] = $this->cobros_model->cant_cobros_by_aprobacion_id($fila->id);
			$row[] = '$'.$this->cobros_model->suma_cobros_by_aprobacion_id($fila->id);
			$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="info_collection('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Cotización </a>
				<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>
				<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="remove_payment('."'".$fila->id."'".')"><i class="fa fa-usd"></i> Pagos </a>
				<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>
				<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
				<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="confirm_cancelado('."'".$fila->id."'".')"><i class="fa fa-envelope-o"></i> Prestamo Cancelado</a>' . 
				'<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>';

			$data[] = $row;	
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

/*	public function ajax_list_mes_actual() {

		$list = $this->cobros_view_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$num = 0;
		foreach ($list as $fila) {

			$date = date('d-m-Y');
			$date = explode('-', $date);
			$mesactual = $date[1];
			$anhoactual = $date[2];

			$cantcobros = $this->cobros_model->get_by_aprobacion_id($fila->id);
			$fec = date("d-m-Y", strtotime("$fila->fecha_primer_descuento + $cantcobros months"));

			$date1 = explode('-', $fec);
			$mescobrar = $date1[1];
			$anhocobrar = $date1[2];

			if ($mesactual == $mescobrar && $anhoactual == $anhocobrar) {
				$num++;
				$no++;
				$row = array();
				$row[] = $fila->cliente;
				$row[] = $fila->cedula;
				$row[] = "$".$fila->cantidad_aprobada;
				$row[] = date("d-m-Y", strtotime("$fila->fecha_primer_descuento + $cantcobros months"));
				$row[] = $fila->compania;
				$row[] = "<span class='label label-success'>". $fila->estado . "<span>";
				$row[] = $fila->estado_cobro;
				if ($this->roluser == 1 ){
					$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="remove_payment('."'".$fila->id."'".')"><i class="fa fa-usd"></i> Pagos </a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';
				} else {
					$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_approval('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="status_payment('."'".$fila->id."'".')"><i class="fa fa-adjust"></i> Estado </a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="payment('."'".$fila->id."'".')"><i class="fa fa-money"></i> Cobro </a>';
				}
				$data[] = $row;
			}

		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $num,
			"recordsFiltered" => $num,
			"data" => $data,
		);
		echo json_encode($output);
	}*/

	public function view($id) {

		$aprobacion = $this->aprobacion_model->get_by_id($id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$data = $this->clientes_model->get_by_id($aprobacion->clientes_id);
		$compania = $this->companias_model->get_by_id($data->companias_id);

		if ($data->fecha_nacimiento) {
			$data->fecha_nacimiento = date('d/m/Y', strtotime(str_replace("/","-",$data->fecha_nacimiento)));
		} else {
			$data->fecha_nacimiento = '';
		}
		if ($compania) { 
			$data->companias_id = $compania->nombre;
		}	
		$user_creator = $this->usuarios_model->get_by_id($data->usuarios_id);
		$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
		$cadena = $user_creator->nombres;
		$nombre = explode(' ',$cadena);
		$cadena1 = $user_creator->apellidos;
		$apellido = explode(' ',$cadena1);
		$data->usuarios_id = $nombre[0] . " " . $apellido[0] . ", <b>" . $rol_creator->descripcion . ".</b>";

		$data->cantidad_aprobada = $aprobacion->cantidad_aprobada;
		$data->cantidad_meses = $aprobacion->cantidad_meses;
		$data->fecha_primer_descuento = $aprobacion->fecha_primer_descuento;
		$data->fecha_entrega = $aprobacion->fecha_entrega;
		$data->porcentaje = $aprobacion->porcentaje;
		
		$data->producto_nombre = $producto->nombre;


		$aprobacion = $this->aprobacion_model->get_entregado_by_clientes_id($data->id);
		if ($aprobacion) {
			$data->aprobacion = TRUE;
			$data->cantidad_aprobada = $aprobacion->cantidad_aprobada;
			$data->cantidad_meses = $aprobacion->cantidad_meses;
			$data->fecha_primer_descuento = $aprobacion->fecha_primer_descuento;
			$data->fecha_entrega = $aprobacion->fecha_entrega;
			$data->archivo = $aprobacion->archivo;
			$data->porcentaje = $aprobacion->porcentaje;
			if ($aprobacion->entregado == 1) {
				$aprobacion->entregado = 'Si';
			} else {
				$aprobacion->entregado = 'No';
			}
			$data->entregado = $aprobacion->entregado;
			if ($aprobacion->cobrado == 1) {
				$aprobacion->cobrado = 'Si';
			} else {
				$aprobacion->cobrado = 'No';
			}
			$data->cobrado = $aprobacion->cobrado;
			$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
			$data->productos_id = $producto->nombre;
		}

		echo json_encode($data);
	}

	public function payment_view($id) {
		$data = $this->return_payment_view($id);
		echo json_encode($data);
	}

	public function return_payment_view($id) {    // En reportes tambien esta
		$data = $this->aprobacion_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($data->clientes_id);
		$producto = $this->productos_model->get_by_id($data->productos_id);
		$cantcobros = $this->cobros_model->get_cobros_adelantados_by_aprobacion_id($data->id);
		$cotizacion = $this->cobro_calculos($data->id);
		$compania = $this->companias_model->get_by_id($cliente->companias_id);

		$data->nombre_producto = $producto->nombre;
		$data->nombre_cliente = $cliente->nombre . ' ' . $cliente->apellido;
		$data->total_cobros = $cantcobros;
		if (count($cotizacion['data']) > 1) {
			$data->total_mensual = round($cotizacion['data'][1]['total'],2);
		} else {
			$data->total_mensual = round($cotizacion['data'][0]['total'],2);
		}
		$data->compania = $compania->nombre;

		$cobros = $this->cobros_model->cobros_by_aprobacion_id($data->id);
		$totalcobros = $this->cobros_model->cant_cobros_by_aprobacion_id($data->id);
		
		if ($cobros) {
			$arraycobro = array();
			foreach ($cobros as $row) {
				$arraycobro[] = $row->id;
			}
			$idcobro = max($arraycobro);
			$cobro = $this->cobros_model->get_by_id($idcobro);

			$monto = $cobro->monto * 1;
			$interes = $cobro->interes * 1;
			$deuda = $cobro->deuda * 1;
			$cargo_administrativo = $cobro->cargo_administrativo * 1;
			$interes_mora = $cobro->interes_mora * 1;
			$plazo = $data->cantidad_meses * 1;
			$porcentaje = $data->porcentaje * 1;

			//Ultimos datos:

			$monto = $cobro->monto * 1;
			$deuda = $cobro->deuda * 1;
			$abono = $cobro->abono * 1;
			$deuda = $deuda - $abono;
			if (count($cotizacion['data']) > 1) {
				if ($porcentaje == 50 && $totalcobros == $plazo) {
					$tasa = $cotizacion['data'][0]['tasa'] / 100;
				} else {
					$tasa = $cotizacion['data'][1]['tasa'] / 100;
				}				
			} else {
				$tasa = $cotizacion['data'][0]['tasa'] / 100;
			}				
			$cargopendiente = $cobro->cargo_restante * 1;
			$interesmora = $cobro->interes_mora * 1;
			$regresar = 0;

			$interes = $cobro->interes;
			$interespendiente = $cobro->interes_restante;
			$deuda = $deuda + $interespendiente;
			$interes = $deuda * $tasa;

			$cargofaltante = $plazo - $totalcobros;

			if ($cargofaltante < 0) {
				$cargofaltante = 0;
			}

			if (count($cotizacion['data']) > 1) {
				if ($porcentaje == 50 && $totalcobros <= $plazo) {
					$cargofaltante = ($cargofaltante * $cotizacion['data'][1]['cargo']) + $cotizacion['data'][0]['cargo'];
				} else {
					$cargofaltante = $cargofaltante * $cotizacion['data'][1]['cargo'];
				}
			} else {
				$cargofaltante = $cargofaltante * $cotizacion['data'][0]['cargo'];
			}
			
			$cargopendiente = $cargopendiente + $cargofaltante;

			$interesmora = $monto - $interesmora;
			if ($interesmora < 0) {
				$interesmora = -1 * $interesmora;
			} else {
				$interesmora = 0;
			}

			$pendiente = $deuda + $interes + $cargopendiente + $interesmora;

			//Condiciones:

			if ($monto >= $pendiente) {

				if ($deuda < 0) {
					$regresar = -1 * $deuda;
					$deuda = 0;
					$interes = 0;
					$cargopendiente = 0;
					$interesmora = 0;
				}

				$data->deuda = number_format($deuda,2);
				$data->interes = number_format($interes,2);
				$data->cargo_administrativo = number_format($cargopendiente,2);
				$data->interes_mora = number_format($interesmora,2);
				$data->pendiente = number_format($deuda + $interes + $cargopendiente + $interesmora,2);
				$data->regresar = number_format($regresar,2);
				$data->pendiente1 = $pendiente;
				$data->cargo_administrativo1 = $cargopendiente;

			} else {

				$data->deuda = number_format($deuda,2);
				$data->interes = number_format($interes,2);
				$data->cargo_administrativo = number_format($cargopendiente,2);
				$data->interes_mora = number_format($interesmora,2);
				$data->pendiente = number_format($deuda + $interes + $cargopendiente + $interesmora,2);
				$data->regresar = number_format($regresar,2);
				$data->pendiente1 = $pendiente;
				$data->cargo_administrativo1 = $cargopendiente;
			}

		} else {
			//Cuando no hay cobros:
			$deuda =  $cotizacion['suma'];
			$interes =  $cotizacion['totalintereses'];
			$cargo_administrativo =  $cotizacion['totalcargo'];
			$interes_mora = 0;
			$pendiente = $deuda + $interes + $cargo_administrativo + $interes_mora;
			$regresar = 0;

			$data->deuda = number_format($deuda,2);
			$data->interes = number_format($interes,2);
			$data->cargo_administrativo = number_format($cargo_administrativo,2);
			$data->interes_mora = number_format($interes_mora,2);
			$data->pendiente = number_format($pendiente,2);
			$data->regresar = number_format($regresar,2);
			$data->pendiente1 = $pendiente;
			$data->cargo_administrativo1 = $cargo_administrativo;

		}		

		return $data;
	}


	public function verify_cobro($id){
		$data = $this->return_verify_cobro($id);
		echo json_encode($data);
	}

	public function return_verify_cobro($id){
		$aprobacion = $this->aprobacion_model->get_by_id($id);
		$cobros = $this->cobros_model->cobros_by_aprobacion_id($aprobacion->id);
		$cantcobros = $this->cobros_model->get_by_aprobacion_id($aprobacion->id);
		$cotizacion = $this->cobro_calculos($aprobacion->id);

		//var_dump($cotizacion);

		$idcobro = array();
		if ($cobros) {
			$idcobro = array();
			$arraycargos = array();
			foreach ($cobros as $row) {
				$idcobro[] = $row->id;
				$arraycargos[] = $row->cargo_administrativo;
			}
			$cobroid = max($idcobro);
			$cobro = $this->cobros_model->get_by_id($cobroid);

			$deuda = round($cobro->deuda,2);
			$fecha = $cobro->fecha_correspondiente;

			$feA = date("d-m-Y", strtotime($fecha));
			$mesA = explode('-', $feA);

			if ($mesA[0] == 15) {
				if ($mesA[1] == 1) {
					$fe = date("d-m-Y", strtotime("$fecha + 15 days"));
					$mesS = explode('-', $fe);
					if ($mesS[1] > 2) {
						$diaT = $mesS[0];
						$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
					}
				} elseif ($mesA[1] == 2) {
					$fe = date("d-m-Y", strtotime("$fecha + 15 days"));
					$mesS = explode('-', $fe);
					if ($mesS[0] > 15 && $mesS[0] < 30) {
						$diaF = 30 - $mesS[0];
						$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
					}
				} else {
					$fe = date("d-m-Y", strtotime("$fecha + 15 days"));
				}
			} else {
				if ($mesA[1] == 1) {
					$fe = date("d-m-Y", strtotime("$fecha + 1 months"));
					$mesS = explode('-', $fe);
					if ($mesS[1] > 2) {
						$diaT = $mesS[0];
						$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
					}
				} elseif ($mesA[1] == 2) {
					$fe = date("d-m-Y", strtotime("$fecha + 1 months"));
					$mesS = explode('-', $fe);
					if ($mesS[0] > 15 && $mesS[0] < 30) {
						$diaF = 30 - $mesS[0];
						$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
					}
				} else {
					$fe = date("d-m-Y", strtotime("$fecha + 1 months"));
				}
			}

			$principal = round($cobro->principal,2);
			$interes = round($cobro->interes,2);
			/*			
			if ($cantcobros == 1) {
				if ($aprobacion->porcentaje == 50) {
					$tasa = round($cotizacion['data'][$cantcobros]['tasa'],2);
				} else {
					$tasa = round($cobro->tasa_interes,2);
				}
			} else {
				$tasa = round($cobro->tasa_interes,2);
			}*/

			$cargo = round($cobro->cargo_administrativo,2);

			$plazo = $aprobacion->cantidad_meses * 1;

			if ($aprobacion->porcentaje == 50 && $cantcobros == $plazo) {
				$tasa = round($cotizacion['data'][0]['tasa'],2);
			} else {
				$tasa = round($cotizacion['data'][1]['tasa'],2);
			}

			if ($cantcobros < $plazo) {

				$interes_mora = $cobro->interes_mora;
				$pago = $cobro->monto * 1;
				$cargototal = $cotizacion['totalcargo'];
				$cargocobrado = array_sum($arraycargos);

				$faltantecargos = $cobro->cargo_restante + $cargo;
				$sumatoria = $interes_mora + $faltantecargos + $interes + $deuda;

				if ($pago >= $sumatoria) {

					$pago = $pago - $interes_mora;
					$interes_mora = 0;
					$pago = $pago - $cobro->cargo_administrativo;

					$fechapago = $cobro->fecha_pago;
					$fe1 = date("d-m-Y", strtotime("$fecha - 1 months"));
					$fe2 = date("d-m-Y", strtotime($fechapago));

					$compfe = $this->comparar_fechas($fe,$fe2);
					if ($compfe == 'mayor') {
						$diasf = $this->dias_transcurridos($fe1,$fe2);

						if ($diasf < 30) {
							$tasadiaria = ($tasa / 100) / 30;
							$tasad = $diasf * $tasadiaria;
						} else {							
							$tasat = round($tasa,2);
							$tasad = $tasat/100;
						}

						$interes = $deuda * $tasad;
						$pago = $pago - $interes;
						$deuda = $deuda - $pago;

						$data = array(
							'deuda' => $deuda,
							'fecha' => $fe,
							'principal' => 0,
							'interes' => 0,
							'tasa' => 0,
							'cargo' => 0,
							'imora' => 0,
							'total' => 0,
							'cuotaperiodica' => $cotizacion['cuotaperiodica'],
							'status' => TRUE
						);

					} else {
						$pago = $pago - $interes;
						$deuda = $deuda - $pago;

						$data = array(
							'deuda' => $deuda,
							'fecha' => $fe,
							'principal' => 0,
							'interes' => 0,
							'tasa' => 0,
							'cargo' => 0,
							'imora' => 0,
							'total' => 0,
							'cuotaperiodica' => $cotizacion['cuotaperiodica'],
							'status' => TRUE
						);
					}

				} else {
					
					$pago = $pago - $interes_mora;
					if ($pago < 0) {
						$interes_mora = -1 * $pago;

						$deuda = $deuda + $interes;
						$interes = $deuda * ($cotizacion['data'][$cantcobros]['tasa']/100);
						$principal = $cotizacion['cuotaperiodica'];
						$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

					} else {
						$interes_mora = 0;
						$pago = $pago - $cargo;
						if ($pago < 0) {
							$cargo = -1 * $pago;

							$deuda = $deuda + $interes;
							$interes = $deuda * ($cotizacion['data'][$cantcobros]['tasa']/100);
							$principal = $cotizacion['cuotaperiodica'];


						} else {
							$cargo = 0;
							$pago = $pago - $interes;
							if ($pago < 0) {
								$interes = -1 * $pago;
								$principal = $principal - $interes;

								$deuda = $deuda + $interes;
								$interes = $deuda * ($cotizacion['data'][$cantcobros]['tasa']/100);
								$principal = $cotizacion['cuotaperiodica'];


							} else {	
								$deuda = $deuda - $pago;

								$interes = $deuda * ($cotizacion['data'][$cantcobros]['tasa']/100);
								$principal = $cotizacion['cuotaperiodica'] - $interes;

							}
						}
					}

					$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

					$total = $principal + $interes + $cargo;

					if ($deuda > 0) {
						$data = array(
							'deuda' => $deuda,
							'fecha' => $fe,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => 0,
							'total' => $total,
							'cuotaperiodica' => $cotizacion['cuotaperiodica']
						);
					} else {
						$data = array(
							'deuda' => $deuda,
							'fecha' => $fe,
							'principal' => 0,
							'interes' => 0,
							'tasa' => 0,
							'cargo' => 0,
							'imora' => 0,
							'total' => 0,
							'cuotaperiodica' => $cotizacion['cuotaperiodica'],
							'status' => TRUE
						);
					}
				}

			} else {

				$interes_mora = $cobro->interes_mora;
				$pago = $cobro->monto;
				$pago = $pago - $interes_mora;
				if ($pago < 0) {
					$interes_mora = -1 * $pago;

					$deuda = $deuda + $interes;
					if ($aprobacion->porcentaje == 50 && $cantcobros == $plazo) {
						$interes = $deuda * ($cotizacion['data'][0]['tasa']/100);
					} else {
						$interes = $deuda * ($cotizacion['data'][1]['tasa']/100);
					}

					$principal = $cotizacion['cuotaperiodica'];

					if ($aprobacion->porcentaje == 50) {
						$cargo = $cargo + $cotizacion['data'][0]['cargo'];
					} else {
						$cargo = $cargo;
					}
					
				} else {
					$interes_mora = 0;
					$pago = $pago - $cargo;
					if ($pago < 0) {
						$cargo = -1 * $pago;

						$deuda = $deuda + $interes;
						if ($aprobacion->porcentaje == 50 && $cantcobros == $plazo) {
							$interes = $deuda * ($cotizacion['data'][0]['tasa']/100);
						} else {
							$interes = $deuda * ($cotizacion['data'][1]['tasa']/100);
						}						
						$principal = $cotizacion['cuotaperiodica'];
					} else {
						$cargo = 0;
						$pago = $pago - $interes;
						if ($pago < 0) {
							$intere = -1 * $pago;
							$principal = $principal - $interes;

							$deuda = $deuda + $interes;
							if ($aprobacion->porcentaje == 50 && $cantcobros == $plazo) {
								$interes = $deuda * ($cotizacion['data'][0]['tasa']/100);
							} else {
								$interes = $deuda * ($cotizacion['data'][1]['tasa']/100);
							}
							
							$principal = $cotizacion['cuotaperiodica'];
						} else {	
							$deuda = $deuda - $pago;

							$interes = $deuda * ($cotizacion['data'][1]['tasa']/100);
							$principal = $cotizacion['cuotaperiodica'] - $interes;

						}
					}
				}


				if ($cantcobros >= $plazo) {
					if ($aprobacion->porcentaje == 50 && $cantcobros == $plazo) {
						$cargo = $cargo + $cotizacion['data'][0]['cargo'];
						if ($cantcobros == $plazo) {
							$interes = $interes / 2;
							//$tasa = $tasa / 2;
						}
					} else {
						$cargo = $cargo;
					}	
				} else {
					if (($aprobacion->porcentaje == 50 && $cantcobros == 0) || ($aprobacion->porcentaje == 50 && $cantcobros == $plazo)) {
						$cargo = $cargo + $cotizacion['data'][0]['cargo'];
					} else {
						$cargo = $cargo + $cotizacion['data'][1]['cargo'];
					}
				}


				$total = $principal + $interes + $cargo;

				if ($total < $cotizacion['data'][$plazo-2]['total']) {
					if ($aprobacion->porcentaje == 50 && $cantcobros == $plazo) {
						$total = $cotizacion['data'][0]['total'];
					} else {
						$total = $cotizacion['data'][1]['total'];
					}					
				}

				if ($deuda > 0) {
					$data = array(
						'deuda' => $deuda,
						'fecha' => $fe,
						'principal' => $principal,
						'interes' => $interes,
						'tasa' => $tasa,
						'cargo' => $cargo,
						'imora' => 0,
						'total' => $total,
						'cuotaperiodica' => $cotizacion['cuotaperiodica']
					);
				} else {
					//$data = array('status' => TRUE);
					$data = array(
						'deuda' => $deuda,
						'fecha' => $fe,
						'principal' => 0,
						'interes' => 0,
						'tasa' => 0,
						'cargo' => 0,
						'imora' => 0,
						'total' => 0,
						'cuotaperiodica' => $cotizacion['cuotaperiodica'],
						'status' => TRUE
					);
				}
				
			}
		} else {
			$data = array(
				'deuda' => $cotizacion['data'][$cantcobros]['capital'],
				'fecha' => $cotizacion['data'][$cantcobros]['fecha'],
				'principal' => $cotizacion['data'][$cantcobros]['principal'],
				'interes' => $cotizacion['data'][$cantcobros]['interes'],
				'tasa' => $cotizacion['data'][$cantcobros]['tasa'],
				'cargo' => $cotizacion['data'][$cantcobros]['cargo'],
				'imora' => 0,
				'total' => $cotizacion['data'][$cantcobros]['total'],
				'cuotaperiodica' => $cotizacion['cuotaperiodica']
			);
		}
		return $data;
	}


/*	public function preparar_database(){ //No tocar

		$result = $this->cobros_model->preparar_database();

		$array = array();
		foreach ($result as $row) {
			$fecha = date('d-m-Y', strtotime(str_replace("/","-",$row->fecha_pago)));
			$aprobacion = $this->aprobacion_model->get_by_id($row->aprobacion_id);
			$dato = $this->return_verify_fecha_cobro($fecha,$aprobacion);

			if (isset($dato['arreglo'])) {
				$num = count($dato['arreglo']);
				for ($i=0; $i < $num; $i++) { 
					$data1 = array(
						'deuda' => $dato['arreglo'][$i]['deuda'],
						'fecha_correspondiente' => date('Y-m-d', strtotime(str_replace("/","-",$dato['arreglo'][$i]['fecha']))),
						'principal' => $dato['arreglo'][$i]['principal'],
						'interes' => $dato['arreglo'][$i]['interes'],
						'tasa_interes' => $dato['arreglo'][$i]['tasa'],
						'cargo_administrativo' => $dato['arreglo'][$i]['cargo'],
						'interes_mora' => $dato['arreglo'][$i]['imora'],
						'total' => $dato['arreglo'][$i]['total'],
						'aprobacion_id' => $row->aprobacion_id,
						'usuarios_id' => $row->usuarios_id
					);
					if ($i == ($num - 1)) {
						$data1['fecha_pago'] = date('Y-m-d', strtotime(str_replace("/","-",$row->fecha_pago)));
						$data1['forma_pago'] = $row->forma_pago;
						$data1['nota'] = $row->nota;
						$data1['archivo'] = $row->archivo;
						$data1['interes_mora'] = $dato['arreglo'][$i-1]['imora'];						
						$data1['total'] = $dato['arreglo'][$i]['principal'] + $dato['arreglo'][$i]['interes'] + $dato['arreglo'][$i]['cargo'] + $dato['arreglo'][$i-1]['imora'];
						$data1['monto'] = $row->monto;
					} else {
						$data1['fecha_pago'] = date('0000-00-00');
						$data1['forma_pago'] = '';
						$data1['nota'] = '';
						$data1['monto'] = 0;
					}
					$insert = $this->cobros_model->save($data1);
				}
			} else {
				$data1 = array(
					'deuda' => $dato['data']['deuda'],
					'fecha_correspondiente' => date('Y-m-d', strtotime(str_replace("/","-",$dato['data']['fecha']))),
					'principal' => $dato['data']['principal'],
					'interes' => $dato['data']['interes'],
					'tasa_interes' => $dato['data']['tasa'],
					'cargo_administrativo' => $dato['data']['cargo'],
					'interes_mora' => $dato['data']['imora'],
					'total' => $dato['data']['total'],
					'fecha_pago' => date('Y-m-d', strtotime(str_replace("/","-",$row->fecha_pago))),
					'forma_pago' => $row->forma_pago,
					'nota' => $row->nota,
					'monto' => $row->monto,
					'archivo' => $row->archivo,
					'aprobacion_id' => $row->aprobacion_id,
					'usuarios_id' => $row->usuarios_id
				);

				$insert = $this->cobros_model->save($data1);
			}

		}

		redirect('http://localhost/Prestamos911/admin');

	}*/

	public function verify_fecha_cobro(){
		$fecha = date('d-m-Y', strtotime(str_replace("/","-",$this->input->post('fecha'))));
		$aprobacion = $this->aprobacion_model->get_by_id($this->input->post('aprobacion_id'));
		$data = $this->return_verify_fecha_cobro($fecha,$aprobacion);
		echo json_encode($data);
	}

	public function return_verify_fecha_cobro($fecha,$aprobacion){

		$cantcobros = $this->cobros_model->get_by_aprobacion_id($aprobacion->id);

		$porcentaje = $aprobacion->porcentaje;

		$dato = $this->return_verify_cobro($aprobacion->id);
		$fec = date("d-m-Y", strtotime($dato['fecha']));

		$comparar = $this->comparar_fechas($fecha,$fec);
		$cotizacion = $this->cobro_calculos($aprobacion->id);

		$dias = $this->dias_transcurridos($fecha,$fec);
		$cobros = $this->cobros_model->cobros_by_aprobacion_id($aprobacion->id);

		$plazo = $aprobacion->cantidad_meses*1;

		if ($cantcobros > 0 && $cantcobros < $plazo) {

			$numcargos = $plazo - $cantcobros;

			if ($comparar == 'mayor') {

				$fe = $dato['fecha'];
				$fec = date("d-m-Y", strtotime($fe));

				$comprobarquince = explode('-', $fec);
				if ($comprobarquince[0] == 15) {
					$fec = date("d-m-Y", strtotime("$fec - 15 days"));
				}

				$mesest = $this->meses_transcurridos($fec,$fecha);

				if ($mesest > 1) {

					$arreglo[] = array(
						'deuda' => $dato['deuda'],
						'fecha' => $dato['fecha'],
						'principal' => $dato['principal'],
						'interes' => $dato['interes'],
						'tasa' => $dato['tasa'],
						'cargo' => $dato['cargo'],
						'imora' => $dato['imora'],
						'total' => $dato['total']
					);


					$deuda = round($dato['deuda'] + $dato['interes'],2);
					$fecha1 = $dato['fecha'];
					$principal = round($dato['principal'],2);
					$interes = round(($dato['deuda'] + $dato['interes']) * ($dato['tasa']/100),2);
					$tasa = $dato['tasa'];
					//$cargo = round($dato['cargo'] + $cotizacion['data'][$cantcobros]['cargo'],2);

					if ($numcargos > 1) {
						$cargo = round($dato['cargo'] + $cotizacion['data'][$cantcobros]['cargo'],2);
					} else {
						$cargo = $dato['cargo'];
					}

					$mesdiciembre = explode('-', $fecha1);
					if ($mesdiciembre[1] == 01) {
						$imora = round(0,2);
					} else {
						$imora = round($cotizacion['data'][$cantcobros]['total'] * (2/100),2);
					}

					$total = $principal + $interes + $cargo + $imora;

					$feA = date("d-m-Y", strtotime($fecha1));
					$mesA = explode('-', $feA);

					if ($mesA[0] == 15) {
						if ($mesA[1] == 1) {
							$fecha1 = date("d-m-Y", strtotime("$fecha1 + 15 days"));
							$mesS = explode('-', $fecha1);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fecha1 = date("d-m-Y", strtotime("$fecha1 - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fecha1 = date("d-m-Y", strtotime("$fecha1 + 15 days"));
							$mesS = explode('-', $fecha1);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fecha1 = date("d-m-Y", strtotime("$fecha1 + $diaF days"));
							}
						} else {
							$fecha1 = date("d-m-Y", strtotime("$fecha1 + 15 days"));
						}
					} else {
						if ($mesA[1] == 1) {
							$fecha1 = date("d-m-Y", strtotime("$fecha1 + 1 months"));
							$mesS = explode('-', $fecha1);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fecha1 = date("d-m-Y", strtotime("$fecha1 - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fecha1 = date("d-m-Y", strtotime("$fecha1 + 1 months"));
							$mesS = explode('-', $fecha1);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fecha1 = date("d-m-Y", strtotime("$fecha1 + $diaF days"));
							}
						} else {
							$fecha1 = date("d-m-Y", strtotime("$fecha1 + 1 months"));
						}
					}

					$arreglo[] = array(
						'deuda' => $deuda,
						'fecha' => $fecha1,
						'principal' => $cotizacion['cuotaperiodica'],
						'interes' => $interes,
						'tasa' => $tasa,
						'cargo' => $cargo,
						'imora' => $imora,
						'total' => $total
					);

					$fe = $arreglo[1]['fecha'];

					$num = $mesest - 1;
					if ($num < $numcargos - 1) {
						for ($i=0; $i < $num; $i++) {
							$deuda = round($deuda + $interes,2);
							$interes = round($deuda * ($tasa/100),2);
							$principal = round($cotizacion['cuotaperiodica'],2);
							$cargo = round($cargo + $cotizacion['data'][$cantcobros]['cargo'],2);
							
							//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

							$feA = date("d-m-Y", strtotime($fe));
							$mesA = explode('-', $feA);
							if ($mesA[1] == 1) {
								$fe = date("d-m-Y", strtotime("$fe + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[1] > 2) {
									$diaT = $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
								}
							} elseif ($mesA[1] == 2) {
								$fe = date("d-m-Y", strtotime("$fe + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[0] > 15 && $mesS[0] < 30) {
									$diaF = 30 - $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
								}
							} else {
								$fe = date("d-m-Y", strtotime("$fe + 1 months"));
							}

							$mesdiciembre = explode('-', $fe);
							if ($mesdiciembre[1] == 01) {
								$imora = round($imora,2);
							} else {
								$imora = round($imora + round($cotizacion['data'][$cantcobros]['total'] * (2/100),2),2);
							}

							$total = round($principal + $interes + $cargo + $imora,2);

							$arreglo[] = array(
								'deuda' => $deuda,
								'fecha' => $fe,
								'principal' => $principal,
								'interes' => $interes,
								'tasa' => $tasa,
								'cargo' => $cargo,
								'imora' => $imora,
								'total' => $total
							);
						}
					} else {
						$cargt = round($numcargos * $cotizacion['data'][$cantcobros]['cargo'],2);
						for ($i=0; $i < $num; $i++) {
							$deuda = round($deuda + $interes,2);
							$interes = round($deuda * ($tasa/100),2);
							$principal = round($cotizacion['cuotaperiodica'],2);
							$cargo = round($cargt,2);
							
							//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

							$feA = date("d-m-Y", strtotime($fe));
							$mesA = explode('-', $feA);
							if ($mesA[1] == 1) {
								$fe = date("d-m-Y", strtotime("$fe + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[1] > 2) {
									$diaT = $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
								}
							} elseif ($mesA[1] == 2) {
								$fe = date("d-m-Y", strtotime("$fe + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[0] > 15 && $mesS[0] < 30) {
									$diaF = 30 - $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
								}
							} else {
								$fe = date("d-m-Y", strtotime("$fe + 1 months"));
							}

							$mesdiciembre = explode('-', $fe);
							if ($mesdiciembre[1] == 01) {
								$imora = round($imora,2);
							} else {
								$imora = round($imora + round($cotizacion['data'][$cantcobros]['total'] * (2/100),2),2);
							}

							$total = round($principal + $interes + $cargo + $imora,2);

							$arreglo[] = array(
								'deuda' => $deuda,
								'fecha' => $fe,
								'principal' => $principal,
								'interes' => $interes,
								'tasa' => $tasa,
								'cargo' => $cargo,
								'imora' => $imora,
								'total' => $total
							);
						}

					}

					$deuda = round($deuda + $interes,2);
					$interes = round($deuda * ($tasa/100),2);
					$principal = round($cotizacion['cuotaperiodica'],2);
					if ($porcentaje == 50 && $num >= $numcargos) {
						$interes = $interes / 2;
						$tasa = $tasa / 2;
						$cargo = round($cargo + $cotizacion['data'][0]['cargo'],2);
					} else {
						//$cargo = round($cargo + $cotizacion['data'][$cantcobros]['cargo'],2);				

						if ($numcargos > 1) {
							if (($porcentaje == 50) && ($num == $numcargos - 1)) {
								$cargo = round($cargo + $cotizacion['data'][0]['cargo'],2);
							} else {
								$cargo = round($cargo + $cotizacion['data'][$cantcobros]['cargo'],2);
							}
						} else {
							$cargo = $cargo;
						}

					}
					
					//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

					$feA = date("d-m-Y", strtotime($fe));
					$mesA = explode('-', $feA);
					if ($mesA[1] == 1) {
						$fe = date("d-m-Y", strtotime("$fe + 1 months"));
						$mesS = explode('-', $fe);
						if ($mesS[1] > 2) {
							$diaT = $mesS[0];
							$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
						}
					} elseif ($mesA[1] == 2) {
						$fe = date("d-m-Y", strtotime("$fe + 1 months"));
						$mesS = explode('-', $fe);
						if ($mesS[0] > 15 && $mesS[0] < 30) {
							$diaF = 30 - $mesS[0];
							$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
						}
					} else {
						$fe = date("d-m-Y", strtotime("$fe + 1 months"));
					}

					$mesdiciembre = explode('-', $fe);
					if ($mesdiciembre[1] == 01) {
						$imora = round($imora,2);
					} else {
						if ($porcentaje == 50 && $num > $numcargos) {
							$imora = round($imora + round($cotizacion['data'][0]['total'] * (2/100),2),2);
						} else {
							$imora = round($imora + round($cotizacion['data'][$cantcobros]['total'] * (2/100),2),2);
						}
					}

					$total = round($principal + $interes + $cargo + $imora,2);

					$arreglo[] = array(
						'deuda' => $deuda,
						'fecha' => $fe,
						'principal' => $principal,
						'interes' => $interes,
						'tasa' => $tasa,
						'cargo' => $cargo,
						'imora' => $imora,
						'total' => $total
					);

					$data['arreglo'] = $arreglo;
					$cantarreglo = count($arreglo) - 1;

					$data['data'] = array(
						'deuda' => $arreglo[$cantarreglo]['deuda'],
						'fecha' => $arreglo[$cantarreglo]['fecha'],
						'principal' => $arreglo[$cantarreglo]['principal'],
						'interes' => $arreglo[$cantarreglo]['interes'],
						'tasa' => $arreglo[$cantarreglo]['tasa'],
						'cargo' => $arreglo[$cantarreglo]['cargo'],
						'imora' => $arreglo[$cantarreglo]['imora'],
						'total' => $arreglo[$cantarreglo]['total']
					);

				} elseif ($mesest == 1) {
					$fechaa = $dato['fecha'];
					//$fechad = date("d-m-Y", strtotime("$fechaa + 1 months"));

					$feA = date("d-m-Y", strtotime($fechaa));
					$mesA = explode('-', $feA);
					if ($mesA[1] == 1) {
						$fechad = date("d-m-Y", strtotime("$fechaa + 1 months"));
						$mesS = explode('-', $fechad);
						if ($mesS[1] > 2) {
							$diaT = $mesS[0];
							$fechad = date("d-m-Y", strtotime("$fechad - $diaT days"));
						}
					} elseif ($mesA[1] == 2) {
						$fechad = date("d-m-Y", strtotime("$fechaa + 1 months"));
						$mesS = explode('-', $fechad);
						if ($mesS[0] > 15 && $mesS[0] < 30) {
							$diaF = 30 - $mesS[0];
							$fechad = date("d-m-Y", strtotime("$fechad + $diaF days"));
						}
					} else {
						$fechad = date("d-m-Y", strtotime("$fechaa + 1 months"));
					}

					$diasp = $this->dias_transcurridos($fechad,$fecha);
					if ($diasp > 0) {

						$deuda = $dato['deuda'];
						$fech = $dato['fecha'];
						$principal = $dato['principal'];
						$interes = $dato['interes'];
						$tasa = $cotizacion['data'][$cantcobros]['tasa'];
						$cargo = $dato['cargo'];
						$imora = $dato['imora'];
						$total = $dato['total'];
						$cuotaperiodica = $dato['cuotaperiodica'];

						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => $fech,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);

						$deuda = $deuda + $interes;
						$interes = $deuda * ($tasa/100);
						$principal = $cuotaperiodica;
						//$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

						if ($cantcobros < $plazo) {
							$numcargos = $plazo - $cantcobros;
						} else {
							$numcargos = 0;
						}						

						if ($numcargos > 1) {
							$cargo = $cargo + $cotizacion['data'][1]['cargo'];
						} else {
							$cargo = $cargo;
						}

						//mas de un mes...

						$mesdiciembre = explode('-', $fech);
						if ($mesdiciembre[1] == 01) {
							$imora = 0;
						} else {
							$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
						}
						//$imora = round(0,2);
						
						$total = $principal + $interes + $cargo + $imora;

						$fecha2 = $arreglo[0]['fecha'];
						$feA = date("d-m-Y", strtotime($fecha2));
						$mesA = explode('-', $feA);
						if ($mesA[1] == 1) {
							$fecha2 = date("d-m-Y", strtotime("$fecha2 + 1 months"));
							$mesS = explode('-', $fecha2);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fecha2 = date("d-m-Y", strtotime("$fecha2 - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fecha2 = date("d-m-Y", strtotime("$fecha2 + 1 months"));
							$mesS = explode('-', $fecha2);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fecha2 = date("d-m-Y", strtotime("$fecha2 + $diaF days"));
							}
						} else {
							$fecha2 = date("d-m-Y", strtotime("$fecha2 + 1 months"));
						}

						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => $fecha2,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);

						$deuda = round($arreglo[1]['deuda'] + $arreglo[1]['interes'],2);
						$fecha2 = $arreglo[1]['fecha'];
						$principal = round($arreglo[1]['principal'],2);
						$interes = round(($arreglo[1]['deuda'] + $arreglo[1]['interes']) * ($arreglo[1]['tasa']/100),2);
						$tasa = $arreglo[1]['tasa'];


						if (($porcentaje == 50) && ($mesest == $numcargos - 1)) {
							$cargo = round($arreglo[1]['cargo'] + $cotizacion['data'][0]['cargo'],2);
						} else {
							//$cargo = round($arreglo[1]['cargo'] + $cotizacion['data'][$cantcobros]['cargo'],2);

							if ($cantcobros < $plazo) {
								$numcargos = $plazo - $cantcobros;
							} else {
								$numcargos = 0;
							}						

							if ($numcargos > 1) {
								$cargo = round($arreglo[1]['cargo'] + $cotizacion['data'][1]['cargo'],2);
							} else {
								$cargo = $cargo;
							}

						}	

						// mas de un mess...

						$mesdiciembre = explode('-', $fecha2);
						if ($mesdiciembre[1] == 01) {
							$imora = round(0,2);
						} else {
							$imora = round($imora + ($cotizacion['data'][$cantcobros]['total'] * (2/100)),2);
						}
						//$imora = round(0,2);

						$total = $principal + $interes + $cargo + $imora;

						$feA = date("d-m-Y", strtotime($fecha2));
						$mesA = explode('-', $feA);
						if ($mesA[1] == 1) {
							$fecha2 = date("d-m-Y", strtotime("$fecha2 + 1 months"));
							$mesS = explode('-', $fecha2);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fecha2 = date("d-m-Y", strtotime("$fecha2 - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fecha2 = date("d-m-Y", strtotime("$fecha2 + 1 months"));
							$mesS = explode('-', $fecha2);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fecha2 = date("d-m-Y", strtotime("$fecha2 + $diaF days"));
							}
						} else {
							$fecha2 = date("d-m-Y", strtotime("$fecha2 + 1 months"));
						}

						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => $fecha2,
							'principal' => $cotizacion['cuotaperiodica'],
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);

						$data['arreglo'] = $arreglo;

						$num = count($arreglo) - 1;
						$data['data'] = array(
							'deuda' => $arreglo[$num]['deuda'],
							'fecha' => $arreglo[$num]['fecha'],
							'principal' => $arreglo[$num]['principal'],
							'interes' => $arreglo[$num]['interes'],
							'tasa' => $arreglo[$num]['tasa'],
							'cargo' => $arreglo[$num]['cargo'],
							'imora' => $arreglo[$num]['imora'],
							'total' => $arreglo[$num]['total']
						);

					} else {
						$deuda = $dato['deuda'];
						$fech = $dato['fecha'];
						$principal = $dato['principal'];
						$interes = $dato['interes'];
						$tasa = $cotizacion['data'][$cantcobros]['tasa'];
						$cargo = $dato['cargo'];
						$imora = $dato['imora'];
						$total = $dato['total'];
						$cuotaperiodica = $dato['cuotaperiodica'];

						$deuda = $deuda + $interes;
						$interes = $deuda * ($tasa/100);
						$principal = $cuotaperiodica;
						//$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];


						if ($cantcobros < $plazo) {
							$numcargos = $plazo - $cantcobros;
						} else {
							$numcargos = 0;
						}						

						if ($numcargos > 1) {
							$cargo = $cargo + $cotizacion['data'][1]['cargo'];
						} else {
							$cargo = $cargo;
						}

						//mas de un mes...

						$mesdiciembre = explode('-', $fech);
						if ($mesdiciembre[1] == 01) {
							$imora = 0;
						} else {
							$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
						}
						//$imora = round(0,2);
						
						$total = $principal + $interes + $cargo + $imora;

						$data['data'] = array(
							'deuda' => $deuda,
							'fecha' => $fech,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);
					}
				} else {
					if ($dias > 0) {
						$deuda = $dato['deuda'];
						$fech = $dato['fecha'];
						$principal = $dato['principal'];
						$interes = $dato['interes'];
						$tasa = $cotizacion['data'][1]['tasa'];
						$cargo = $dato['cargo'];
						$imora = $dato['imora'];
						$total = $dato['total'];
						$cuotaperiodica = $dato['cuotaperiodica'];

						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => date("d-m-Y", strtotime($fech)),
							'principal' => $cotizacion['cuotaperiodica'],
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total
						);

						$deuda = $deuda + $interes;
						$interes = $deuda * ($tasa/100);
						$principal = $cuotaperiodica;

						if ($cantcobros < $plazo) {
							$numcargos = $plazo - $cantcobros;
						} else {
							$numcargos = 0;
						}						

						if ($numcargos > 1) {
							$cargo = $cargo + $cotizacion['data'][1]['cargo'];
						} else {
							$cargo = $cargo;
						}

						$feA = date("d-m-Y", strtotime($fech));
						$mesA = explode('-', $feA);
						if ($mesA[1] == 1) {
							$fe = date("d-m-Y", strtotime("$fech + 1 months"));
							$mesS = explode('-', $fe);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fe = date("d-m-Y", strtotime("$fech + 1 months"));
							$mesS = explode('-', $fe);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
							}
						} else {
							$fe = date("d-m-Y", strtotime("$fech + 1 months"));
						}

						//Mas de uno...... Ojo con este no esta bien definido...

						$mesdiciembre = explode('-', $fe);
						if ($mesdiciembre[1] == 01 || $mesdiciembre[1] == 02) {
							$imora = 0;
						} else {
							$imora = $cotizacion['data'][1]['total'] * (2/100);							
						}
						//$imora = round(0,2);
						
						$total = $principal + $interes + $cargo + $imora;

						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => $fe,
							'principal' => $cotizacion['cuotaperiodica'],
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total
						);

						$data['arreglo'] = $arreglo;

						$data['data'] = array(
							'deuda' => $deuda,
							'fecha' => $fech,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);
					} else {
						$data['data'] = array(
							'deuda' => $dato['deuda'],
							'fecha' => $dato['fecha'],
							'principal' => $dato['principal'],
							'interes' => $dato['interes'],
							'tasa' => $cotizacion['data'][$cantcobros]['tasa'],
							'cargo' => $dato['cargo'],
							'imora' => $dato['imora'],
							'total' => $dato['total'],
							'cuotaperiodica' => $dato['cuotaperiodica']
						);
					}
				}
			} else {
				$data['data'] = array(
					'deuda' => $dato['deuda'],
					'fecha' => $dato['fecha'],
					'principal' => $dato['principal'],
					'interes' => $dato['interes'],
					'tasa' => $cotizacion['data'][$cantcobros]['tasa'],
					'cargo' => $dato['cargo'],
					'imora' => $dato['imora'],
					'total' => $dato['total'],
					'cuotaperiodica' => $dato['cuotaperiodica']
				);
			}

		} else {
			if ($comparar == 'mayor' && $cantcobros == 0 && $dias > 46) {
				$fech = $dato['fecha'];
				$fec = date("d-m-Y", strtotime("$fech + 46 days"));

				$fm = explode('-', $fec);
				if ($fm[0] == 16 || $fm[0] == 31) {
					$fec = date("d-m-Y", strtotime("$fech + 45 days"));
				} elseif ($fm[0] == 14) {
					$fec = date("d-m-Y", strtotime("$fech + 47 days"));
				} elseif ($fm[0] == 17) {
					$fec = date("d-m-Y", strtotime("$fech + 44 days"));
				}

				$fech1 = date("d-m-Y", strtotime($fech));
				$t1 = explode('-', $fech1);
				if ($t1[0] == 15 && $t1[1] == 1) {
					$fech2 = date("d-m-Y", strtotime($fec));
					$t2 = explode('-', $fech2);
					if ($t2[1] > 2) {
						$diaF = $t2[0];
						$fec = date("d-m-Y", strtotime("$fec - $diaF days"));
					}
				}

				$fmb = explode('-', $fech);
				if ($fmb[1] == 2 && $fm[0] < 15) {
					$fm1 = explode('-', $fec);
					$mdia = $fm1[0];
					$fec = date("d-m-Y", strtotime("$fec - $mdia days"));
					$fm2 = explode('-', $fec);
					if ($fm2[0] == 31) {
						$fec = date("d-m-Y", strtotime("$fec - 1 days"));
					}
				}

				$comprobarquince = explode('-', $fec);
				if ($comprobarquince[0] == 15) {
					$fec = date("d-m-Y", strtotime("$fec - 15 days"));
				}

				$mesest = $this->meses_transcurridos($fec,$fecha);

				$deuda = $dato['deuda'];
				$fech = $dato['fecha'];
				$principal = $dato['principal'];
				$interes = $dato['interes'];
				$tasa = $dato['tasa'];
				$cargo = $dato['cargo'];
				$imora = $dato['imora'];
				$total = $dato['total'];
				$cuotaperiodica = $dato['cuotaperiodica'];

				$fe = date("d-m-Y", strtotime("$fech + 46 days"));
				$fm = explode('-', $fe);
				if ($fm[0] == 16 || $fm[0] == 31) {
					$fe = date("d-m-Y", strtotime("$fech + 45 days"));
				} elseif ($fm[0] == 14) {
					$fe = date("d-m-Y", strtotime("$fech + 47 days"));
				} elseif ($fm[0] == 17) {
					$fe = date("d-m-Y", strtotime("$fech + 44 days"));
				}

				$fech1 = date("d-m-Y", strtotime($fech));
				$t1 = explode('-', $fech1);
				if ($t1[0] == 15 && $t1[1] == 1) {
					$fech2 = date("d-m-Y", strtotime($fe));
					$t2 = explode('-', $fech2);
					if ($t2[1] > 2) {
						$diaF = $t2[0];
						$fe = date("d-m-Y", strtotime("$fe - $diaF days"));
					}
				}

				$fmb = explode('-', $fech);
				if ($fmb[1] == 2 && $fm[0] < 15) {
					$fm1 = explode('-', $fe);
					$mdia = $fm1[0];
					$fe = date("d-m-Y", strtotime("$fe - $mdia days"));
					$fm2 = explode('-', $fe);
					if ($fm2[0] == 31) {
						$fe = date("d-m-Y", strtotime("$fe - 1 days"));
					}
				}

				$arreglo[] = array(
					'deuda' => $deuda,
					'fecha' => $fe,
					'principal' => $principal,
					'interes' => $interes,
					'tasa' => $tasa,
					'cargo' => $cargo,
					'imora' => $imora,
					'total' => $total,
					'cuotaperiodica' => $cuotaperiodica
				);

				if ($mesest > 1) {

					$feA = date("d-m-Y", strtotime($fe));
					$mesA = explode('-', $feA);

					if ($mesA[0] == 15) {
						if ($mesA[1] == 1) {
							$fe = date("d-m-Y", strtotime("$fe + 15 days"));
							$mesS = explode('-', $fe);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fe = date("d-m-Y", strtotime("$fe + 15 days"));
							$mesS = explode('-', $fe);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
							}
						} else {
							$fe = date("d-m-Y", strtotime("$fe + 15 days"));
						}
					} else {
						if ($mesA[1] == 1) {
							$fe = date("d-m-Y", strtotime("$fe + 1 months"));
							$mesS = explode('-', $fe);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fe = date("d-m-Y", strtotime("$fe + 1 months"));
							$mesS = explode('-', $fe);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
							}
						} else {
							$fe = date("d-m-Y", strtotime("$fe + 1 months"));
						}
					}

					if ($porcentaje == 50) {
						$deuda = $deuda + $interes;
						$interes = $deuda * ($tasa/100);
						$principal = $cuotaperiodica / 2;
						$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

						$mesdiciembre = explode('-', $fe);
						if ($mesdiciembre[1] == 01) {
							$imora = 0;
						} else {
							$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
						}

						$total = $principal + $interes + $cargo + $imora;
					} else {
						$deuda = $deuda + $interes;
						$interes = $deuda * ($tasa/100);
						$principal = $cuotaperiodica;
						$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

						$mesdiciembre = explode('-', $fe);
						if ($mesdiciembre[1] == 01) {
							$imora = 0;
						} else {
							$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
						}

						$total = $principal + $interes + $cargo + $imora;
					}

					$arreglo[] = array(
						'deuda' => $deuda,
						'fecha' => $fe,
						'principal' => $principal,
						'interes' => $interes,
						'tasa' => $tasa,
						'cargo' => $cargo,
						'imora' => $imora,
						'total' => $total,
						'cuotaperiodica' => $cuotaperiodica
					);

					$fecht = $arreglo[0]['fecha'];

					$deuda = $deuda + $interes;
					//$fe = date("d-m-Y", strtotime("$fecht + 1 months"));

					$feA = date("d-m-Y", strtotime($fe));
					$mesA = explode('-', $feA);

					if ($mesA[0] == 15) {
						if ($mesA[1] == 1) {
							$fe = date("d-m-Y", strtotime("$fe + 15 days"));
							$mesS = explode('-', $fe);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fe = date("d-m-Y", strtotime("$fe + 15 days"));
							$mesS = explode('-', $fe);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
							}
						} else {
							$fe = date("d-m-Y", strtotime("$fe + 15 days"));
						}
					} else {
						if ($mesA[1] == 1) {
							$fe = date("d-m-Y", strtotime("$fe + 1 months"));
							$mesS = explode('-', $fe);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fe = date("d-m-Y", strtotime("$fe + 1 months"));
							$mesS = explode('-', $fe);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
							}
						} else {
							$fe = date("d-m-Y", strtotime("$fe + 1 months"));
						}
					}

					$principal = $cuotaperiodica;
					$interes = $deuda * ($cotizacion['data'][$cantcobros+1]['tasa']/100);
					$tasa = $cotizacion['data'][$cantcobros+1]['tasa'];
					$cargo = $cargo + $cotizacion['data'][$cantcobros+1]['cargo'];

					$mesdiciembre = explode('-', $fe);
					if ($mesdiciembre[1] == 01) {
						$imora = $imora;
					} else {
						$imora = $imora + ($cotizacion['data'][$cantcobros+1]['total']*(2/100));
					}					

					$deuda = $deuda;
					$fe = $fe;
					$principal = $principal;
					$interes = $interes;
					$tasa = $tasa;
					$cargo = $cargo;
					$imora = $imora;
					$total = $principal + $interes + $cargo + $imora;

					$arreglo[] = array(
						'deuda' => $deuda,
						'fecha' => $fe,
						'principal' => $principal,
						'interes' => $interes,
						'tasa' => $tasa,
						'cargo' => $cargo,
						'imora' => $imora,
						'total' => $total,
						'cuotaperiodica' => $cuotaperiodica
					);

					$numcargos = $plazo;

					if ($mesest > 2) {

						$fech0 = $arreglo[0]['fecha'];
						$fet = date("d-m-Y", strtotime("$fech0 + $mesest months"));

						$diast = $this->dias_transcurridos($fet,$fecha);

						if ($diast > 0) {
							$mesest = $mesest + 1;
							$num = $mesest - 2;

							if ($num < $numcargos - 2) {
								for ($i=0; $i < $num; $i++) {
									$deuda = round($deuda + $interes,2);
									$interes = round($deuda * ($tasa/100),2);
									$principal = round($cotizacion['cuotaperiodica'],2);
									$cargo = round($cargo + $cotizacion['data'][$cantcobros+1]['cargo'],2);

									//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

									$feA = date("d-m-Y", strtotime($fe));
									$mesA = explode('-', $feA);

									if ($mesA[0] == 15) {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										}
									} else {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										}
									}

									$mesdiciembre = explode('-', $fe);
									if ($mesdiciembre[1] == 01) {
										$imora = round($imora,2);
									} else {
										$imora = round($imora + round($cotizacion['data'][$cantcobros+1]['total'] * (2/100),2),2);
									}
									
									$total = round($principal + $interes + $cargo + $imora,2);

									$arreglo[] = array(
										'deuda' => $deuda,
										'fecha' => $fe,
										'principal' => $principal,
										'interes' => $interes,
										'tasa' => $tasa,
										'cargo' => $cargo,
										'imora' => $imora,
										'total' => $total
									);
								}

								//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

								$feA = date("d-m-Y", strtotime($fe));
								$mesA = explode('-', $feA);

								if ($mesA[0] == 15) {
									if ($mesA[1] == 1) {
										$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										$mesS = explode('-', $fe);
										if ($mesS[1] > 2) {
											$diaT = $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
										}
									} elseif ($mesA[1] == 2) {
										$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										$mesS = explode('-', $fe);
										if ($mesS[0] > 15 && $mesS[0] < 30) {
											$diaF = 30 - $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
										}
									} else {
										$fe = date("d-m-Y", strtotime("$fe + 15 days"));
									}
								} else {
									if ($mesA[1] == 1) {
										$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										$mesS = explode('-', $fe);
										if ($mesS[1] > 2) {
											$diaT = $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
										}
									} elseif ($mesA[1] == 2) {
										$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										$mesS = explode('-', $fe);
										if ($mesS[0] > 15 && $mesS[0] < 30) {
											$diaF = 30 - $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
										}
									} else {
										$fe = date("d-m-Y", strtotime("$fe + 1 months"));
									}
								}

							} else {
								$carg = round($numcargos * $cotizacion['data'][$cantcobros+1]['cargo'],2);
								for ($i=0; $i < $num; $i++) {
									$deuda = round($deuda + $interes,2);
									$interes = round($deuda * ($tasa/100),2);
									$principal = round($cotizacion['cuotaperiodica'],2);
									$cargo = round($carg,2);

									//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

									$feA = date("d-m-Y", strtotime($fe));
									$mesA = explode('-', $feA);

									if ($mesA[0] == 15) {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										}
									} else {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										}
									}

									$mesdiciembre = explode('-', $fe);
									if ($mesdiciembre[1] == 01) {
										$imora = round($imora,2);
									} else {
										$imora = round($imora + round($cotizacion['data'][$cantcobros+1]['total'] * (2/100),2),2);
									}
									
									$total = round($principal + $interes + $cargo + $imora,2);

									$arreglo[] = array(
										'deuda' => $deuda,
										'fecha' => $fe,
										'principal' => $principal,
										'interes' => $interes,
										'tasa' => $tasa,
										'cargo' => $cargo,
										'imora' => $imora,
										'total' => $total
									);
								}

								$feA = date("d-m-Y", strtotime($fe));
								$mesA = explode('-', $feA);

								if ($mesA[0] == 15) {
									if ($mesA[1] == 1) {
										$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										$mesS = explode('-', $fe);
										if ($mesS[1] > 2) {
											$diaT = $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
										}
									} elseif ($mesA[1] == 2) {
										$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										$mesS = explode('-', $fe);
										if ($mesS[0] > 15 && $mesS[0] < 30) {
											$diaF = 30 - $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
										}
									} else {
										$fe = date("d-m-Y", strtotime("$fe + 15 days"));
									}
								} else {
									if ($mesA[1] == 1) {
										$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										$mesS = explode('-', $fe);
										if ($mesS[1] > 2) {
											$diaT = $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
										}
									} elseif ($mesA[1] == 2) {
										$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										$mesS = explode('-', $fe);
										if ($mesS[0] > 15 && $mesS[0] < 30) {
											$diaF = 30 - $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
										}
									} else {
										$fe = date("d-m-Y", strtotime("$fe + 1 months"));
									}
								}
								
							}
						} else {
							$num = $mesest - 2;
							if ($num < $numcargos - 2) {  //listo
								for ($i=0; $i < $num; $i++) {
									$deuda = round($deuda + $interes,2);
									$interes = round($deuda * ($tasa/100),2);
									$principal = round($cotizacion['cuotaperiodica'],2);
									$cargo = round($cargo + $cotizacion['data'][$cantcobros+1]['cargo'],2);

									//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

									$feA = date("d-m-Y", strtotime($fe));
									$mesA = explode('-', $feA);

									if ($mesA[0] == 15) {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										}
									} else {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										}
									}

									$mesdiciembre = explode('-', $fe);
									if ($mesdiciembre[1] == 01) {
										$imora = round($imora,2);
									} else {
										$imora = round($imora + round($cotizacion['data'][$cantcobros+1]['total'] * (2/100),2),2);
									}
									
									$total = round($principal + $interes + $cargo + $imora,2);

									$arreglo[] = array(
										'deuda' => $deuda,
										'fecha' => $fe,
										'principal' => $principal,
										'interes' => $interes,
										'tasa' => $tasa,
										'cargo' => $cargo,
										'imora' => $imora,
										'total' => $total
									);
								}
							} else {  //listo
								$carg = round($numcargos * $cotizacion['data'][$cantcobros+1]['cargo'],2);
								for ($i=0; $i < $num; $i++) {
									$deuda = round($deuda + $interes,2);
									$interes = round($deuda * ($tasa/100),2);
									$principal = round($cotizacion['cuotaperiodica'],2);
									$cargo = round($carg,2);

									//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

									$feA = date("d-m-Y", strtotime($fe));
									$mesA = explode('-', $feA);

									if ($mesA[0] == 15) {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										}
									} else {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										}
									}

									$mesdiciembre = explode('-', $fe);
									if ($mesdiciembre[1] == 01) {
										$imora = round($imora,2);
									} else {
										$imora = round($imora + round($cotizacion['data'][$cantcobros+1]['total'] * (2/100),2),2);
									}
									
									$total = round($principal + $interes + $cargo + $imora,2);

									$arreglo[] = array(
										'deuda' => $deuda,
										'fecha' => $fe,
										'principal' => $principal,
										'interes' => $interes,
										'tasa' => $tasa,
										'cargo' => $cargo,
										'imora' => $imora,
										'total' => $total
									);
								}
							}
						}

					} elseif ($mesest == 2) {
						$fech0 = $arreglo[0]['fecha'];
						$fet = date("d-m-Y", strtotime("$fech0 + $mesest months"));
						$diast = $this->dias_transcurridos($fet,$fecha);
						if ($diast > 0) {
							$deuda = round($deuda + $interes,2);
							$interes = round($deuda * ($tasa/100),2);
							$principal = round($cotizacion['cuotaperiodica'],2);
							$cargo = round($cargo + $cotizacion['data'][$cantcobros+1]['cargo'],2);

							//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

							$feA = date("d-m-Y", strtotime($fe));
							$mesA = explode('-', $feA);

							if ($mesA[0] == 15) {
								if ($mesA[1] == 1) {
									$fe = date("d-m-Y", strtotime("$fe + 15 days"));
									$mesS = explode('-', $fe);
									if ($mesS[1] > 2) {
										$diaT = $mesS[0];
										$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
									}
								} elseif ($mesA[1] == 2) {
									$fe = date("d-m-Y", strtotime("$fe + 15 days"));
									$mesS = explode('-', $fe);
									if ($mesS[0] > 15 && $mesS[0] < 30) {
										$diaF = 30 - $mesS[0];
										$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
									}
								} else {
									$fe = date("d-m-Y", strtotime("$fe + 15 days"));
								}
							} else {
								if ($mesA[1] == 1) {
									$fe = date("d-m-Y", strtotime("$fe + 1 months"));
									$mesS = explode('-', $fe);
									if ($mesS[1] > 2) {
										$diaT = $mesS[0];
										$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
									}
								} elseif ($mesA[1] == 2) {
									$fe = date("d-m-Y", strtotime("$fe + 1 months"));
									$mesS = explode('-', $fe);
									if ($mesS[0] > 15 && $mesS[0] < 30) {
										$diaF = 30 - $mesS[0];
										$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
									}
								} else {
									$fe = date("d-m-Y", strtotime("$fe + 1 months"));
								}
							}

							$mesdiciembre = explode('-', $fe);
							if ($mesdiciembre[1] == 01) {
								$imora = round($imora,2);
							} else {
								$imora = round($imora + round($cotizacion['data'][$cantcobros+1]['total'] * (2/100),2),2);
							}
							
							$total = round($principal + $interes + $cargo + $imora,2);

							$arreglo[] = array(
								'deuda' => $deuda,
								'fecha' => $fe,
								'principal' => $principal,
								'interes' => $interes,
								'tasa' => $tasa,
								'cargo' => $cargo,
								'imora' => $imora,
								'total' => $total
							);
						}
					}

					$data['arreglo'] = $arreglo;

					$cantarreglo = count($arreglo) - 1;
					$data['data'] = array(
						'deuda' => $arreglo[$cantarreglo]['deuda'],
						'fecha' => $arreglo[$cantarreglo]['fecha'],
						'principal' => $arreglo[$cantarreglo]['principal'],
						'interes' => $arreglo[$cantarreglo]['interes'],
						'tasa' => $arreglo[$cantarreglo]['tasa'],
						'cargo' => $arreglo[$cantarreglo]['cargo'],
						'imora' => $arreglo[$cantarreglo]['imora'],
						'total' => $arreglo[$cantarreglo]['total']
					);

				} elseif ($mesest == 1) {
					//$fect = date("d-m-Y", strtotime("$fec + 1 months"));

					$feA = date("d-m-Y", strtotime($fec));
					$mesA = explode('-', $feA);

					if ($mesA[0] == 15) {
						if ($mesA[1] == 1) {
							$fect = date("d-m-Y", strtotime("$fec + 15 days"));
							$mesS = explode('-', $fect);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fect = date("d-m-Y", strtotime("$fect - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fect = date("d-m-Y", strtotime("$fec + 15 days"));
							$mesS = explode('-', $fect);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fect = date("d-m-Y", strtotime("$fect + $diaF days"));
							}
						} else {
							$fect = date("d-m-Y", strtotime("$fec + 15 days"));
						}
					} else {
						if ($mesA[1] == 1) {
							$fect = date("d-m-Y", strtotime("$fec + 1 months"));
							$mesS = explode('-', $fect);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fect = date("d-m-Y", strtotime("$fect - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fect = date("d-m-Y", strtotime("$fec + 1 months"));
							$mesS = explode('-', $fect);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fect = date("d-m-Y", strtotime("$fect + $diaF days"));
							}
						} else {
							$fect = date("d-m-Y", strtotime("$fec + 1 months"));
						}
					}

					$diast = $this->dias_transcurridos($fect,$fecha);

					if ($diast > 0) {

						$deuda = $dato['deuda'];
						$fech = $dato['fecha'];
						$principal = $dato['principal'];
						$interes = $dato['interes'];
						$tasa = $dato['tasa'];
						$cargo = $dato['cargo'];
						$imora = $dato['imora'];
						$total = $dato['total'];
						$cuotaperiodica = $dato['cuotaperiodica'];

						$fe = date("d-m-Y", strtotime("$fech + 46 days"));
						$fm = explode('-', $fe);
						if ($fm[0] == 16 || $fm[0] == 31) {
							$fe = date("d-m-Y", strtotime("$fech + 45 days"));
						} elseif ($fm[0] == 14) {
							$fe = date("d-m-Y", strtotime("$fech + 47 days"));
						} elseif ($fm[0] == 17) {
							$fe = date("d-m-Y", strtotime("$fech + 44 days"));
						}

						$fech1 = date("d-m-Y", strtotime($fech));
						$t1 = explode('-', $fech1);
						if ($t1[0] == 15 && $t1[1] == 1) {
							$fech2 = date("d-m-Y", strtotime($fe));
							$t2 = explode('-', $fech2);
							if ($t2[1] > 2) {
								$diaF = $t2[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaF days"));
							}
						}

						$fmb = explode('-', $fech);
						if ($fmb[1] == 2 && $fm[0] < 15) {
							$fm1 = explode('-', $fe);
							$mdia = $fm1[0];
							$fe = date("d-m-Y", strtotime("$fe - $mdia days"));
							$fm2 = explode('-', $fe);
							if ($fm2[0] == 31) {
								$fe = date("d-m-Y", strtotime("$fe - 1 days"));
							}
						}

						//mas de un mes...

						if ($porcentaje == 50) {
							$deuda = $deuda + $interes;
							$interes = $deuda * ($tasa/100);
							$principal = $cuotaperiodica / 2;
							$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

							$mesdiciembre = explode('-', $fe);
							if ($mesdiciembre[1] == 01) {
								$imora = 0;
							} else {
								$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
							}
							//$imora = round(0,2);
							
							$total = $principal + $interes + $cargo + $imora;
						} else {
							$deuda = $deuda + $interes;
							$interes = $deuda * ($tasa/100);
							$principal = $cuotaperiodica;
							$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

							$mesdiciembre = explode('-', $fe);
							if ($mesdiciembre[1] == 01) {
								$imora = 0;
							} else {
								$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
							}
							//$imora = round(0,2);
							
							$total = $principal + $interes + $cargo + $imora;
						}

						$fecht = $arreglo[0]['fecha'];

						$feA = date("d-m-Y", strtotime($fec));
						$mesA = explode('-', $feA);

						if ($mesA[0] == 15) {
							if ($mesA[1] == 1) {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
								$mesS = explode('-', $fe);
								if ($mesS[1] > 2) {
									$diaT = $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
								}
							} elseif ($mesA[1] == 2) {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
								$mesS = explode('-', $fe);
								if ($mesS[0] > 15 && $mesS[0] < 30) {
									$diaF = 30 - $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
								}
							} else {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
							}
						} else {
							if ($mesA[1] == 1) {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[1] > 2) {
									$diaT = $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
								}
							} elseif ($mesA[1] == 2) {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[0] > 15 && $mesS[0] < 30) {
									$diaF = 30 - $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
								}
							} else {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
							}
						}

						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => $fe,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);

						$fecht = $arreglo[1]['fecha'];

						$deuda = $deuda + $interes;
						//$fe = date("d-m-Y", strtotime("$fecht + 1 months"));

						$feA = date("d-m-Y", strtotime($fec));
						$mesA = explode('-', $feA);

						if ($mesA[0] == 15) {
							if ($mesA[1] == 1) {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
								$mesS = explode('-', $fe);
								if ($mesS[1] > 2) {
									$diaT = $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
								}
							} elseif ($mesA[1] == 2) {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
								$mesS = explode('-', $fe);
								if ($mesS[0] > 15 && $mesS[0] < 30) {
									$diaF = 30 - $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
								}
							} else {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
							}
						} else {
							if ($mesA[1] == 1) {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[1] > 2) {
									$diaT = $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
								}
							} elseif ($mesA[1] == 2) {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[0] > 15 && $mesS[0] < 30) {
									$diaF = 30 - $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
								}
							} else {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
							}
						}


						$principal = $cuotaperiodica;
						$interes = $deuda * ($cotizacion['data'][$cantcobros+1]['tasa']/100);
						$tasa = $cotizacion['data'][$cantcobros+1]['tasa'];
						$cargo = $cargo + $cotizacion['data'][$cantcobros+1]['cargo'];

						//mas de un mes...

						$mesdiciembre = explode('-', $fe);
						if ($mesdiciembre[1] == 01) {
							$imora = $imora;
						} else {
							$imora = $imora + ($cotizacion['data'][$cantcobros+1]['total']*(2/100));
						}
						//$imora = round($imora,2);					

						$deuda = $deuda;
						$fe = $fe;
						$principal = $principal;
						$interes = $interes;
						$tasa = $tasa;
						$cargo = $cargo;
						$imora = $imora;
						$total = $principal + $interes + $cargo + $imora;

						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => $fe,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);

						$data['arreglo'] = $arreglo;

						$num = count($arreglo) - 1 ;

						$data['data'] = array(
							'deuda' => $arreglo[$num]['deuda'],
							'fecha' => $arreglo[$num]['fecha'],
							'principal' => $arreglo[$num]['principal'],
							'interes' => $arreglo[$num]['interes'],
							'tasa' => $arreglo[$num]['tasa'],
							'cargo' => $arreglo[$num]['cargo'],
							'imora' => $arreglo[$num]['imora'],
							'total' => $arreglo[$num]['total'],
							'cuotaperiodica' => $cuotaperiodica
						);
					} else {
						$deuda = $dato['deuda'];
						$fech = $dato['fecha'];
						$principal = $dato['principal'];
						$interes = $dato['interes'];
						$tasa = $dato['tasa'];
						$cargo = $dato['cargo'];
						$imora = $dato['imora'];
						$total = $dato['total'];
						$cuotaperiodica = $dato['cuotaperiodica'];

						$fe = date("d-m-Y", strtotime("$fech + 46 days"));
						$fm = explode('-', $fe);
						if ($fm[0] == 16 || $fm[0] == 31) {
							$fe = date("d-m-Y", strtotime("$fech + 45 days"));
						} elseif ($fm[0] == 14) {
							$fe = date("d-m-Y", strtotime("$fech + 47 days"));
						} elseif ($fm[0] == 17) {
							$fe = date("d-m-Y", strtotime("$fech + 44 days"));
						}

						$fech1 = date("d-m-Y", strtotime($fech));
						$t1 = explode('-', $fech1);
						if ($t1[0] == 15 && $t1[1] == 1) {
							$fech2 = date("d-m-Y", strtotime($fe));
							$t2 = explode('-', $fech2);
							if ($t2[1] > 2) {
								$diaF = $t2[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaF days"));
							}
						}

						$fmb = explode('-', $fech);
						if ($fmb[1] == 2 && $fm[0] < 15) {
							$fm1 = explode('-', $fe);
							$mdia = $fm1[0];
							$fe = date("d-m-Y", strtotime("$fe - $mdia days"));
							$fm2 = explode('-', $fe);
							if ($fm2[0] == 31) {
								$fe = date("d-m-Y", strtotime("$fe - 1 days"));
							}
						}

						if ($porcentaje == 50) {
							$deuda = $deuda + $interes;
							$interes = $deuda * ($tasa/100);
							$principal = $cuotaperiodica / 2;
							$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

							var_dump(8888);

/*							$mesdiciembre = explode('-', $fe);
							if ($mesdiciembre[1] == 01) {
								$imora = 0;
							} else {
								$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
							}*/
							$imora = round(0,2);
							
							$total = $principal + $interes + $cargo + $imora;
						} else {
							$deuda = $deuda + $interes;
							$interes = $deuda * ($tasa/100);
							$principal = $cuotaperiodica;
							$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

							var_dump(9999);

/*							$mesdiciembre = explode('-', $fe);
							if ($mesdiciembre[1] == 01) {
								$imora = 0;
							} else {
								$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
							}*/
							$imora = round(0,2);

							$total = $principal + $interes + $cargo + $imora;
						}

						$fecht = $arreglo[0]['fecha'];

						$feA = date("d-m-Y", strtotime($fec));
						$mesA = explode('-', $feA);

						if ($mesA[0] == 15) {
							if ($mesA[1] == 1) {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
								$mesS = explode('-', $fe);
								if ($mesS[1] > 2) {
									$diaT = $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
								}
							} elseif ($mesA[1] == 2) {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
								$mesS = explode('-', $fe);
								if ($mesS[0] > 15 && $mesS[0] < 30) {
									$diaF = 30 - $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
								}
							} else {
								$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
							}
						} else {
							if ($mesA[1] == 1) {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[1] > 2) {
									$diaT = $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
								}
							} elseif ($mesA[1] == 2) {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
								$mesS = explode('-', $fe);
								if ($mesS[0] > 15 && $mesS[0] < 30) {
									$diaF = 30 - $mesS[0];
									$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
								}
							} else {
								$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
							}
						}

						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => $fe,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);

						$data['arreglo'] = $arreglo;
						$num = count($arreglo) - 1 ;

						$data['data'] = array(
							'deuda' => $arreglo[$num]['deuda'],
							'fecha' => $arreglo[$num]['fecha'],
							'principal' => $arreglo[$num]['principal'],
							'interes' => $arreglo[$num]['interes'],
							'tasa' => $arreglo[$num]['tasa'],
							'cargo' => $arreglo[$num]['cargo'],
							'imora' => $arreglo[$num]['imora'],
							'total' => $arreglo[$num]['total'],
							'cuotaperiodica' => $cuotaperiodica
						);


					}
				} else {
					$deuda = $dato['deuda'];
					$fech = $dato['fecha'];
					$principal = $dato['principal'];
					$interes = $dato['interes'];
					$tasa = $dato['tasa'];
					$cargo = $dato['cargo'];
					$imora = $dato['imora'];
					$total = $dato['total'];
					$cuotaperiodica = $dato['cuotaperiodica'];

					$fe = date("d-m-Y", strtotime("$fech + 46 days"));
					$fm = explode('-', $fe);
					if ($fm[0] == 16 || $fm[0] == 31) {
						$fe = date("d-m-Y", strtotime("$fech + 45 days"));
					} elseif ($fm[0] == 14) {
						$fe = date("d-m-Y", strtotime("$fech + 47 days"));
					} elseif ($fm[0] == 17) {
						$fe = date("d-m-Y", strtotime("$fech + 44 days"));
					}

					$fech1 = date("d-m-Y", strtotime($fech));
					$t1 = explode('-', $fech1);
					if ($t1[0] == 15 && $t1[1] == 1) {
						$fech2 = date("d-m-Y", strtotime($fe));
						$t2 = explode('-', $fech2);
						if ($t2[1] > 2) {
							$diaF = $t2[0];
							$fe = date("d-m-Y", strtotime("$fe - $diaF days"));
						}
					}

					$fmb = explode('-', $fech);
					if ($fmb[1] == 2 && $fm[0] < 15) {
						$fm1 = explode('-', $fe);
						$mdia = $fm1[0];
						$fe = date("d-m-Y", strtotime("$fe - $mdia days"));
						$fm2 = explode('-', $fe);
						if ($fm2[0] == 31) {
							$fe = date("d-m-Y", strtotime("$fe - 1 days"));
						}
					}

					//mas de un mes...

					if ($porcentaje == 50) {
						$deuda = $deuda + $interes;
						$interes = $deuda * ($tasa/100);
						$principal = $cuotaperiodica / 2;
						$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

						$mesdiciembre = explode('-', $fe);
						if ($mesdiciembre[1] == 01) {
							$imora = 0;
						} else {
							$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
						}
						//$imora = round(0,2);
						
						$total = $principal + $interes + $cargo + $imora;
					} else {
						$deuda = $deuda + $interes;
						$interes = $deuda * ($tasa/100);
						$principal = $cuotaperiodica;
						$cargo = $cargo + $cotizacion['data'][$cantcobros]['cargo'];

						$mesdiciembre = explode('-', $fe);
						if ($mesdiciembre[1] == 01) {
							$imora = 0;
						} else {
							$imora = $cotizacion['data'][$cantcobros]['total'] * (2/100);
						}
						$imora = round(0,2);

						$total = $principal + $interes + $cargo + $imora;
					}

					$fecht = $fe;

					$feA = date("d-m-Y", strtotime($fec));
					$mesA = explode('-', $feA);

					if ($mesA[0] == 15) {
						if ($mesA[1] == 1) {
							$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
							$mesS = explode('-', $fe);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
							$mesS = explode('-', $fe);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
							}
						} else {
							$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
						}
					} else {
						if ($mesA[1] == 1) {
							$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
							$mesS = explode('-', $fe);
							if ($mesS[1] > 2) {
								$diaT = $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
							}
						} elseif ($mesA[1] == 2) {
							$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
							$mesS = explode('-', $fe);
							if ($mesS[0] > 15 && $mesS[0] < 30) {
								$diaF = 30 - $mesS[0];
								$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
							}
						} else {
							$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
						}
					}

					$arreglo[] = array(
						'deuda' => $deuda,
						'fecha' => $fe,
						'principal' => $principal,
						'interes' => $interes,
						'tasa' => $tasa,
						'cargo' => $cargo,
						'imora' => $imora,
						'total' => $total,
						'cuotaperiodica' => $cuotaperiodica
					);

					$data['arreglo'] = $arreglo;
					$num = count($arreglo) - 1;

					$data['data'] = array(
						'deuda' => $arreglo[$num]['deuda'],
						'fecha' => $arreglo[$num]['fecha'],
						'principal' => $arreglo[$num]['principal'],
						'interes' => $arreglo[$num]['interes'],
						'tasa' => $arreglo[$num]['tasa'],
						'cargo' => $arreglo[$num]['cargo'],
						'imora' => $arreglo[$num]['imora'],
						'total' => $arreglo[$num]['total'],
						'cuotaperiodica' => $arreglo[$num]['cuotaperiodica']
					);
				}
			} else {
				if ($cantcobros == 0) {

					$fech = $dato['fecha'];
					$fe = date("d-m-Y", strtotime("$fech + 46 days"));
					$fm = explode('-', $fe);
					if ($fm[0] == 16 || $fm[0] == 31) {
						$fe = date("d-m-Y", strtotime("$fech + 45 days"));
					} elseif ($fm[0] == 14) {
						$fe = date("d-m-Y", strtotime("$fech + 47 days"));
					} elseif ($fm[0] == 17) {
						$fe = date("d-m-Y", strtotime("$fech + 44 days"));
					}

					$fech1 = date("d-m-Y", strtotime($fech));
					$t1 = explode('-', $fech1);
					if ($t1[0] == 15 && $t1[1] == 1) {
						$fech2 = date("d-m-Y", strtotime($fe));
						$t2 = explode('-', $fech2);
						if ($t2[1] > 2) {
							$diaF = $t2[0];
							$fe = date("d-m-Y", strtotime("$fe - $diaF days"));
						}
					}

					$fmb = explode('-', $fech);
					if ($fmb[1] == 2 && $fm[0] < 15) {
						$fm1 = explode('-', $fe);
						$mdia = $fm1[0];
						$fe = date("d-m-Y", strtotime("$fe - $mdia days"));
						$fm2 = explode('-', $fe);
						if ($fm2[0] == 31) {
							$fe = date("d-m-Y", strtotime("$fe - 1 days"));
						}
					}
					
					$data['data'] = array(
						'deuda' => $dato['deuda'],
						'fecha' => $fe,
						'principal' => $dato['principal'],
						'interes' => $dato['interes'],
						'tasa' => $dato['tasa'],
						'cargo' => $dato['cargo'],
						'imora' => $dato['imora'],
						'total' => $dato['total'],
						'cuotaperiodica' => $dato['cuotaperiodica']
					);
				} else {

					if ($comparar == 'mayor') {

						$deuda = $dato['deuda'];
						$fecha0 = $dato['fecha'];
						$principal = $dato['principal'];
						$interes = $dato['interes'];
						$tasa = $dato['tasa'];
						$cargo = $dato['cargo'];
						$imora = $dato['imora'];
						$total = $dato['total'];
						$cuotaperiodica = $dato['cuotaperiodica'];

/*						$deuda = $deuda;
						$interes = $interes;
						$principal = $principal;
						$cargo = $cargo;
						$imora = $imora;
						$total = $total;*/

/*						$arreglo[] = array(
							'deuda' => $deuda,
							'fecha' => $fecha0,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total
						);	*/	

						$fecht = $dato['fecha'];
						$feee = date("d-m-Y", strtotime($fecht));

						$mesest = $this->meses_transcurridos($feee,$fecha);
						if ($mesest > 1) {
							$fet = date("d-m-Y", strtotime("$feee + $mesest months"));

							$mesf = explode('-', $fet);
							if ($mesf[1] == 3 && $mesf[0] < 15) {
								$diasd = $mesf[0];
								$fet = date("d-m-Y", strtotime("$fet - $diasd days"));
							}

							$comparar0 = $this->comparar_fechas($fet,$fecha);
							if ($comparar0 == 'menor') {

								$deuda = round($deuda + $interes,2);
								$interes = round($deuda * ($cotizacion['data'][1]['tasa']/100),2);
								$principal = $dato['cuotaperiodica'];
								$cargo = $cargo;

								$mesdiciembre = explode('-', $fecha0);
								if ($mesdiciembre[1] == 01) {
									$imora = round($imora,2);
								} else {
									$numcargos = $plazo - $cantcobros;
									if ($porcentaje == 50 && $numcargos == 0) {
										$imora = round($imora + ($cotizacion['data'][0]['total']*(2/100)),2);
									} else {
										$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
									}
									//$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
								}

								$total = $principal + $interes + $cargo + $imora;

								$arreglo[] = array(
									'deuda' => $deuda,
									'fecha' => $fecha0,
									'principal' => $principal,
									'interes' => $interes,
									'tasa' => $tasa,
									'cargo' => $cargo,
									'imora' => $imora,
									'total' => $total
								);


								$fe = $arreglo[0]['fecha'];
								$num = $mesest;
								for ($i=0; $i < $num; $i++) {
									$deuda = round($deuda + $interes,2);
									$interes = round($deuda * ($tasa/100),2);
									$principal = round($cotizacion['cuotaperiodica'],2);
									$cargo = round($cargo,2);

									//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

									$feA = date("d-m-Y", strtotime($fe));
									$mesA = explode('-', $feA);

									if ($mesA[0] == 15) {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 15 days"));
										}
									} else {
										if ($mesA[1] == 1) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[1] > 2) {
												$diaT = $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
											}
										} elseif ($mesA[1] == 2) {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											$mesS = explode('-', $fe);
											if ($mesS[0] > 15 && $mesS[0] < 30) {
												$diaF = 30 - $mesS[0];
												$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
											}
										} else {
											$fe = date("d-m-Y", strtotime("$fe + 1 months"));
										}
									}

									$mesdiciembre = explode('-', $fe);
									if ($mesdiciembre[1] == 01) {
										$imora = round($imora,2);
									} else {
										$numcargos = $plazo - $cantcobros;
										if ($porcentaje == 50 && $numcargos == 0) {
											$imora = round($imora + round($cotizacion['data'][0]['total'] * (2/100),2),2);
										} else {
											$imora = round($imora + round($cotizacion['data'][1]['total'] * (2/100),2),2);
										}
										//$imora = round($imora + round($cotizacion['data'][1]['total'] * (2/100),2),2);
									}
									
									$total = round($principal + $interes + $cargo + $imora,2);

									$arreglo[] = array(
										'deuda' => $deuda,
										'fecha' => $fe,
										'principal' => $principal,
										'interes' => $interes,
										'tasa' => $tasa,
										'cargo' => $cargo,
										'imora' => $imora,
										'total' => $total
									);
								}

								$fe = date("d-m-Y", strtotime("$fe + 1 months"));

								$arreglo[] = array(
									'deuda' => $deuda,
									'fecha' => $fe,
									'principal' => $principal,
									'interes' => $interes,
									'tasa' => $tasa,
									'cargo' => $cargo,
									'imora' => $imora,
									'total' => $total
								);

								$data['arreglo'] = $arreglo;

							} else {
								$deuda = round($deuda + $interes,2);
								$interes = round($deuda * ($cotizacion['data'][1]['tasa']/100),2);
								$principal = $dato['cuotaperiodica'];
								$cargo = $cargo;

								$mesdiciembre = explode('-', $fet);
								if ($mesdiciembre[1] == 01) {
									$imora = round($imora,2);
								} else {
									$numcargos = $plazo - $cantcobros;
									if ($porcentaje == 50 && $numcargos == 0) {
										$imora = round($imora + ($cotizacion['data'][0]['total']*(2/100)),2);
									} else {
										$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
									}
									//$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
								}
								
								$total = $principal + $interes + $cargo + $imora;

								$arreglo[] = array(
									'deuda' => $deuda,
									'fecha' => $fet,
									'principal' => $principal,
									'interes' => $interes,
									'tasa' => $tasa,
									'cargo' => $cargo,
									'imora' => $imora,
									'total' => $total,
									'cuotaperiodica' => $cuotaperiodica
								);

								$fecht = $arreglo[0]['fecha'];

								$deuda = $deuda + $interes;
								//$fe = date("d-m-Y", strtotime("$fecht + 1 months"));

								$feA = date("d-m-Y", strtotime($fecht));
								$mesA = explode('-', $feA);

								if ($mesA[0] == 15) {
									if ($mesA[1] == 1) {
										$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
										$mesS = explode('-', $fe);
										if ($mesS[1] > 2) {
											$diaT = $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
										}
									} elseif ($mesA[1] == 2) {
										$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
										$mesS = explode('-', $fe);
										if ($mesS[0] > 15 && $mesS[0] < 30) {
											$diaF = 30 - $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
										}
									} else {
										$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
									}
								} else {
									if ($mesA[1] == 1) {
										$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
										$mesS = explode('-', $fe);
										if ($mesS[1] > 2) {
											$diaT = $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
										}
									} elseif ($mesA[1] == 2) {
										$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
										$mesS = explode('-', $fe);
										if ($mesS[0] > 15 && $mesS[0] < 30) {
											$diaF = 30 - $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
										}
									} else {
										$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
									}
								}

								$principal = $cuotaperiodica;
								$interes = $deuda * ($cotizacion['data'][1]['tasa']/100);
								$tasa = $cotizacion['data'][1]['tasa'];
								$cargo = $cargo;

								$mesdiciembre = explode('-', $fe);
								if ($mesdiciembre[1] == 01) {
									$imora = $imora;
								} else {
									$numcargos = $plazo - $cantcobros;
									if ($porcentaje == 50 && $numcargos == 0) {
										$imora = round($imora + ($cotizacion['data'][0]['total']*(2/100)),2);
									} else {
										$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
									}
									//$imora = $imora + ($cotizacion['data'][1]['total']*(2/100));
								}								

								$deuda = $deuda;
								$fe = $fe;
								$principal = $principal;
								$interes = $interes;
								$tasa = $tasa;
								$cargo = $cargo;
								$imora = $imora;
								$total = $principal + $interes + $cargo + $imora;

								if ($mesest > 2) {
									$num = $mesest - 2;
									for ($i=0; $i < $num; $i++) {
										$deuda = round($deuda + $interes,2);
										$interes = round($deuda * ($tasa/100),2);
										$principal = round($cotizacion['cuotaperiodica'],2);
										$cargo = round($cargo,2);

										//$fe = date("d-m-Y", strtotime("$fe + 1 months"));

										$feA = date("d-m-Y", strtotime($fe));
										$mesA = explode('-', $feA);

										if ($mesA[0] == 15) {
											if ($mesA[1] == 1) {
												$fe = date("d-m-Y", strtotime("$fe + 15 days"));
												$mesS = explode('-', $fe);
												if ($mesS[1] > 2) {
													$diaT = $mesS[0];
													$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
												}
											} elseif ($mesA[1] == 2) {
												$fe = date("d-m-Y", strtotime("$fe + 15 days"));
												$mesS = explode('-', $fe);
												if ($mesS[0] > 15 && $mesS[0] < 30) {
													$diaF = 30 - $mesS[0];
													$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
												}
											} else {
												$fe = date("d-m-Y", strtotime("$fe + 15 days"));
											}
										} else {
											if ($mesA[1] == 1) {
												$fe = date("d-m-Y", strtotime("$fe + 1 months"));
												$mesS = explode('-', $fe);
												if ($mesS[1] > 2) {
													$diaT = $mesS[0];
													$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
												}
											} elseif ($mesA[1] == 2) {
												$fe = date("d-m-Y", strtotime("$fe + 1 months"));
												$mesS = explode('-', $fe);
												if ($mesS[0] > 15 && $mesS[0] < 30) {
													$diaF = 30 - $mesS[0];
													$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
												}
											} else {
												$fe = date("d-m-Y", strtotime("$fe + 1 months"));
											}
										}

										$mesdiciembre = explode('-', $fe);
										if ($mesdiciembre[1] == 01) {
											$imora = round($imora,2);
										} else {
											$numcargos = $plazo - $cantcobros;
											if ($porcentaje == 50 && $numcargos == 0) {
												$imora = round($imora + ($cotizacion['data'][0]['total']*(2/100)),2);
											} else {
												$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
											}
											//$imora = round($imora + round($cotizacion['data'][1]['total'] * (2/100),2),2);
										}
										
										$total = round($principal + $interes + $cargo + $imora,2);

										$arreglo[] = array(
											'deuda' => $deuda,
											'fecha' => $fe,
											'principal' => $principal,
											'interes' => $interes,
											'tasa' => $tasa,
											'cargo' => $cargo,
											'imora' => $imora,
											'total' => $total
										);
									}
								} else {
									$arreglo[] = array(
										'deuda' => $deuda,
										'fecha' => $fe,
										'principal' => $principal,
										'interes' => $interes,
										'tasa' => $tasa,
										'cargo' => $cargo,
										'imora' => $imora,
										'total' => $total
									);
								}

								$data['arreglo'] = $arreglo;
							}
						} elseif ($mesest == 1) {							

							$feA = date("d-m-Y", strtotime($feee));
							$mesA = explode('-', $feA);

							if ($mesA[0] == 15) {
								if ($mesA[1] == 1) {
									$fet = date("d-m-Y", strtotime("$feee + 15 days"));
									$mesS = explode('-', $fet);
									if ($mesS[1] > 2) {
										$diaT = $mesS[0];
										$fet = date("d-m-Y", strtotime("$fet - $diaT days"));
									}
								} elseif ($mesA[1] == 2) {
									$fet = date("d-m-Y", strtotime("$fet + 15 days"));
									$mesS = explode('-', $fet);
									if ($mesS[0] > 15 && $mesS[0] < 30) {
										$diaF = 30 - $mesS[0];
										$fet = date("d-m-Y", strtotime("$fet + $diaF days"));
									}
								} else {
									$fet = date("d-m-Y", strtotime("$feee + 15 days"));
								}
							} else {
								if ($mesA[1] == 1) {
									$fet = date("d-m-Y", strtotime("$feee + 1 months"));
									$mesS = explode('-', $fet);
									if ($mesS[1] > 2) {
										$diaT = $mesS[0];
										$fet = date("d-m-Y", strtotime("$fet - $diaT days"));
									}
								} elseif ($mesA[1] == 2) {
									$fet = date("d-m-Y", strtotime("$fet + 1 months"));
									$mesS = explode('-', $fet);
									if ($mesS[0] > 15 && $mesS[0] < 30) {
										$diaF = 30 - $mesS[0];
										$fet = date("d-m-Y", strtotime("$fet + $diaF days"));
									}
								} else {
									$fet = date("d-m-Y", strtotime("$feee + $mesest months"));
								}
							}

							$diasst = $this->dias_transcurridos($fet,$fecha);
							if ($diasst > 0) {

								$arreglo[] = array(
									'deuda' => $deuda,
									'fecha' => $fecha0,
									'principal' => $principal,
									'interes' => $interes,
									'tasa' => $tasa,
									'cargo' => $cargo,
									'imora' => $imora,
									'total' => $total,
									'cuotaperiodica' => $cuotaperiodica
								);

								$deuda = round($deuda + $interes,2);
								$interes = round($deuda * ($cotizacion['data'][1]['tasa']/100),2);
								$principal = $dato['cuotaperiodica'];
								$cargo = $cargo;

								// mas de un mes....

								$mesdiciembre = explode('-', $fet);
								if ($mesdiciembre[1] == 01) {
									$imora = round($imora,2);
								} else {
									$numcargos = $plazo - $cantcobros;
									if ($porcentaje == 50 && $numcargos == 0) {
										$imora = round($imora + ($cotizacion['data'][0]['total']*(2/100)),2);
									} else {
										$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
									}
									//$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
								}
								//$imora = round($imora,2);
								
								$total = $principal + $interes + $cargo + $imora;

								$arreglo[] = array(
									'deuda' => $deuda,
									'fecha' => $fet,
									'principal' => $principal,
									'interes' => $interes,
									'tasa' => $tasa,
									'cargo' => $cargo,
									'imora' => $imora,
									'total' => $total,
									'cuotaperiodica' => $cuotaperiodica
								);

								$fecht = $arreglo[1]['fecha'];

								$deuda = $deuda + $interes;
								//$fe = date("d-m-Y", strtotime("$fecht + 1 months"));

								$feA = date("d-m-Y", strtotime($fecht));
								$mesA = explode('-', $feA);

								if ($mesA[0] == 15) {
									if ($mesA[1] == 1) {
										$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
										$mesS = explode('-', $fe);
										if ($mesS[1] > 2) {
											$diaT = $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
										}
									} elseif ($mesA[1] == 2) {
										$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
										$mesS = explode('-', $fe);
										if ($mesS[0] > 15 && $mesS[0] < 30) {
											$diaF = 30 - $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
										}
									} else {
										$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
									}
								} else {
									if ($mesA[1] == 1) {
										$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
										$mesS = explode('-', $fe);
										if ($mesS[1] > 2) {
											$diaT = $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
										}
									} elseif ($mesA[1] == 2) {
										$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
										$mesS = explode('-', $fe);
										if ($mesS[0] > 15 && $mesS[0] < 30) {
											$diaF = 30 - $mesS[0];
											$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
										}
									} else {
										$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
									}
								}

								$principal = $cuotaperiodica;
								$interes = $deuda * ($cotizacion['data'][1]['tasa']/100);
								$tasa = $cotizacion['data'][1]['tasa'];
								$cargo = $cargo;

								//mas de un mes...

								$mesdiciembre = explode('-', $fe);
								if ($mesdiciembre[1] == 01) {
									$imora = $imora;
								} else {
									$numcargos = $plazo - $cantcobros;
									if ($porcentaje == 50 && $numcargos == 0) {
										$imora = round($imora + ($cotizacion['data'][0]['total']*(2/100)),2);
									} else {
										$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
									}
									//$imora = $imora + ($cotizacion['data'][1]['total']*(2/100));
								}
								//$imora = round($imora,2);							

								$deuda = $deuda;
								$fe = $fe;
								$principal = $principal;
								$interes = $interes;
								$tasa = $tasa;
								$cargo = $cargo;
								$imora = $imora;
								$total = $principal + $interes + $cargo + $imora;

								$arreglo[] = array(
									'deuda' => $deuda,
									'fecha' => $fe,
									'principal' => $principal,
									'interes' => $interes,
									'tasa' => $tasa,
									'cargo' => $cargo,
									'imora' => $imora,
									'total' => $total,
									'cuotaperiodica' => $cuotaperiodica
								);

								$data['arreglo'] = $arreglo;

							} else {
								$deuda = round($deuda + $interes,2);
								$interes = round($deuda * ($cotizacion['data'][1]['tasa']/100),2);
								$principal = $dato['cuotaperiodica'];
								$cargo = $cargo;

								// dia 30 de mes

								$mesdiciembre = explode('-', $fecha0);
								if ($mesdiciembre[1] == 01) {
									$imora = round($imora,2);
								} else {
									$numcargos = $plazo - $cantcobros;
									if ($porcentaje == 50 && $numcargos == 0) {
										$imora = round($imora + ($cotizacion['data'][0]['total']*(2/100)),2);
									} else {
										$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
									}
									//$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
								}
								//$imora = round($imora,2);
								
								$total = $principal + $interes + $cargo + $imora;
							}
						} else {

							$arreglo[] = array(
								'deuda' => $deuda,
								'fecha' => $fecha0,
								'principal' => $principal,
								'interes' => $interes,
								'tasa' => $tasa,
								'cargo' => $cargo,
								'imora' => $imora,
								'total' => $total,
								'cuotaperiodica' => $cuotaperiodica
							);
							

							$diass = $this->dias_transcurridos($feee,$fecha);
							if ($diass > 0) {
								$deuda = round($deuda + $interes,2);
								$interes = round($deuda * ($cotizacion['data'][1]['tasa']/100),2);
								$principal = $dato['cuotaperiodica'];
								$cargo = $cargo;

								// un messs... OJO.....

								$mesdiciembre = explode('-', $fecha0);
								if ($mesdiciembre[1] == 12) {
									$imora = round($imora,2);
								} else {
									$numcargos = $plazo - $cantcobros;
									if ($porcentaje == 50 && $numcargos == 0) {
										$imora = round($imora + ($cotizacion['data'][0]['total']*(2/100)),2);
									} else {
										$imora = round($imora + ($cotizacion['data'][1]['total']*(2/100)),2);
									}
								}
								//$imora = round($imora,2);
								
								$total = $principal + $interes + $cargo + $imora;
							}

							$feA = date("d-m-Y", strtotime($fecha0));
							$mesA = explode('-', $feA);

							if ($mesA[0] == 15) {
								if ($mesA[1] == 1) {
									$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
									$mesS = explode('-', $fe);
									if ($mesS[1] > 2) {
										$diaT = $mesS[0];
										$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
									}
								} elseif ($mesA[1] == 2) {
									$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
									$mesS = explode('-', $fe);
									if ($mesS[0] > 15 && $mesS[0] < 30) {
										$diaF = 30 - $mesS[0];
										$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
									}
								} else {
									$fe = date("d-m-Y", strtotime("$fecht + 15 days"));
								}
							} else {
								if ($mesA[1] == 1) {
									$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
									$mesS = explode('-', $fe);
									if ($mesS[1] > 2) {
										$diaT = $mesS[0];
										$fe = date("d-m-Y", strtotime("$fe - $diaT days"));
									}
								} elseif ($mesA[1] == 2) {
									$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
									$mesS = explode('-', $fe);
									if ($mesS[0] > 15 && $mesS[0] < 30) {
										$diaF = 30 - $mesS[0];
										$fe = date("d-m-Y", strtotime("$fe + $diaF days"));
									}
								} else {
									$fe = date("d-m-Y", strtotime("$fecht + 1 months"));
								}
							}

							$arreglo[] = array(
								'deuda' => $deuda,
								'fecha' => $fe,
								'principal' => $principal,
								'interes' => $interes,
								'tasa' => $tasa,
								'cargo' => $cargo,
								'imora' => $imora,
								'total' => $total,
								'cuotaperiodica' => $cuotaperiodica
							);

							$data['arreglo'] = $arreglo;

						}						

						$data['data'] = array(
							'deuda' => $deuda,
							'fecha' => $fecha0,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);

					} else {

						$deuda = $dato['deuda'];
						$fecha0 = $dato['fecha'];
						$principal = $dato['principal'];
						$interes = $dato['interes'];
						$tasa = $dato['tasa'];
						$cargo = $dato['cargo'];
						$imora = $dato['imora'];
						$total = $dato['total'];
						$cuotaperiodica = $dato['cuotaperiodica'];

						$data['data'] = array(
							'deuda' => $deuda,
							'fecha' => $fecha0,
							'principal' => $principal,
							'interes' => $interes,
							'tasa' => $tasa,
							'cargo' => $cargo,
							'imora' => $imora,
							'total' => $total,
							'cuotaperiodica' => $cuotaperiodica
						);
					}

				}
			}
		}

		return $data;		
	}


	public function verify_cobro_completo(){
		$fecha = date('d-m-Y', strtotime(str_replace("/","-",$this->input->post('fecha'))));
		$aprobacion = $this->aprobacion_model->get_by_id($this->input->post('aprobacion_id'));
		$data = $this->return_verify_cobro_completo($fecha,$aprobacion);
		echo json_encode($data);
	}

	public function return_verify_cobro_completo($fecha,$aprobacion){

		$dato = $this->return_verify_cobro($aprobacion->id);

		$deuda = $dato['deuda'];
		$fecha0 = $dato['fecha'];
		$principal = $dato['principal'];
		$interes = $dato['interes'];
		$tasa = $dato['tasa'];
		$cargo = $dato['cargo'];
		$imora = $dato['imora'];
		$total = $dato['total'];
		$cuotaperiodica = $dato['cuotaperiodica'];

/*		$deuda = $deuda;
		$interes = $interes;
		$principal = $principal;
		$cargo = $cargo;
		$imora = $imora;
		$total = $total;*/

		$data['data'] = array(
			'deuda' => $deuda,
			'fecha' => $fecha0,
			'principal' => $principal,
			'interes' => $interes,
			'tasa' => $tasa,
			'cargo' => $cargo,
			'imora' => $imora,
			'total' => $total,
			'cuotaperiodica' => $cuotaperiodica
		);

		return $data;
					
	}


	function comparar_fechas($fecha_i,$fecha_f)
	{

		$fecha = null;
		if (strtotime($fecha_i) > strtotime($fecha_f)) {
			$fecha = "mayor";
		} elseif (strtotime($fecha_i) < strtotime($fecha_f)) {
			$fecha = "menor";
		} else {
			$fecha = "igual";
		}

		return $fecha;
	}

	function dias_transcurridos($fecha_i,$fecha_f)
	{
		$dias = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
		$dias = abs($dias);
		$dias = floor($dias);		
		return $dias;
	}

	function meses_transcurridos($inicio,$fin)
	{
		$datetime1=new DateTime($inicio);
		$datetime2=new DateTime($fin);
		# obtenemos la diferencia entre las dos fechas
		$interval=$datetime2->diff($datetime1);
		# obtenemos la diferencia en meses
		$intervalMeses=$interval->format("%m");
		# obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
		$intervalAnos = $interval->format("%y")*12;
		return $intervalMeses+$intervalAnos;
	}


	function tiempoTranscurridoFechas($fechaInicio,$fechaFin)
	{
	    $fecha1 = new DateTime($fechaInicio);
	    $fecha2 = new DateTime($fechaFin);
	    $fecha = $fecha1->diff($fecha2);
	    $anhos = 0;
	    $meses = 0;
	    $dias = 0;
	         
	    //años
	    if($fecha->y > 0)
	    {
	        $anhos = $fecha->y;
	    }
	         
	    //meses
	    if($fecha->m > 0)
	    {
	        $meses = $fecha->m;
	    }
	         
	    //dias
	    if($fecha->d > 0)
	    {
	        $dias = $fecha->d;
	    }

	    $tiempo = array(
	    	'anhos' => $anhos,
	    	'meses' => $meses,
	    	'dias' => $dias
	    );
	         
	    return $tiempo;
	}


	public function cobro($id){
		$output = $this->cobro_calculos($id);	
		echo json_encode($output);
	}

	public function cobro_calculos($id)	{ // en refinanciamiento, reportes tambien esta

		$aprobacion_id = $id;

		$approval = $this->aprobacion_model->get_by_id($aprobacion_id);

		$porcentaje = $approval->porcentaje;
		$suma = $approval->cantidad_aprobada;
		$productos_id = $approval->productos_id;
		$plazo = $approval->cantidad_meses;
		$fecha_primer_descuento = $approval->fecha_primer_descuento;

		//$fecha = date("d-m-Y");
		$fecha = date('Y-m-d', strtotime(str_replace("/","-",$fecha_primer_descuento)));

		$tasa = 0.00;
		$cargo = 0.00;

		if ($productos_id) {
			$products = $this->cotizacion_model->get_products_by_id($productos_id);
			foreach ($products as $row) {
				$tasa = $row->tasa_interes_mensual;
				$solocargo = $row->cargo_administrativo;  
				$sologastos = empty($row->gastos_notariales) ? 0 : $row->gastos_notariales;
				$solocomision = empty($row->comision) ? 0 : $row->comision;
				$cargo = $solocargo +  $sologastos +  $solocomision;
			}
		}

		$suma = round($suma,2);
		$tasa = round($tasa,2);
		$cargo = round($cargo,2);

		$interes = $tasa / 100;

		$p = $suma;
		$x = $interes;
		$n = $plazo;

		$m = $p * ( $x / ( 1 - ( ( 1 + $x ) ** - $n ) ) );

		$cuotaperiodica = $m;
		$cuotaperiodica = round($cuotaperiodica,2);

		$cargototal = $cargo * ($suma / 100);
		$cargototal = round($cargototal,2);

		$capital = array();	
		$pagado = array();	
		$interesasignado = array();
		$cargototalarray = array();
		$tasaarray = array();
		$total = array();

		$sum = $suma;
		for ($i=0; $i<$plazo; $i++) {

			$capital[$i] = $sum;

			$interesi = $sum * $interes;
			$interesi = round($interesi,2);
			$valori = $cuotaperiodica - $interesi;
			$valori = round($valori,2);
			$restoi = $sum - $valori;
			$restoi = round($restoi,2);
			$sum = $restoi;
			$pagado[$i] = $valori;
			$interesasignado[$i] = $interesi;
			$cargototalarray[$i] = $cargototal;
			$tasaarray[$i] = $tasa;
		}

		$totalintereses = array_sum($interesasignado);
		$totalintereses = round($totalintereses,2);
		$totalcargo = $cargototal * $plazo;
		$totalcargo = round($totalcargo,2);

		$totalsuma = array();
		$intereses = $totalcargo + array_sum($interesasignado);
		$intereses = round($intereses,2);
		$totalcobrar = array();
		$diferenciasuma = $suma - (array_sum($pagado));
		$diferenciasuma = round($diferenciasuma,2);

		if($diferenciasuma < 0){
			$dec = explode("-0.", $diferenciasuma);
			$num = $dec[1] * 1;
			$m = 0;
			for ($i=0; $i < $num; $i++) {
				$m++;
				$pagado[$plazo-$m] = $pagado[$plazo-$m] - 0.01;
			}
		} elseif($diferenciasuma > 0){
			$dec = explode("0.", $diferenciasuma);
			$num = $dec[1] * 1;
			for ($i=0; $i < $num; $i++) {
				$pagado[$i] = $pagado[$i] + 0.01;
			}
		}

		if ($porcentaje == 50) {

			/*			
			$pagado[0] = $pagado[0] / 2;
			$pagado[$plazo] = $pagado[0];
			$interesasignado[0] = $interesasignado[0] / 2;
			$interesasignado[$plazo] = $interesasignado[0];
			$tasaarray[0] = $tasaarray[0] / 2;
			$tasaarray[$plazo] = $tasaarray[0];
			$cargototalarray[0] = $cargototalarray[0] / 2;
			$cargototalarray[$plazo] = $cargototalarray[0];*/


			$pagado[0] = $pagado[0] / 2;
			$interesasignado[0] = $interesasignado[0] / 2;
			$tasaarray[0] = $tasaarray[0] / 2;
			$cargototalarray[0] = $cargototalarray[0] / 2;

			$capital[$plazo] = 0;
			$pagado[$plazo] = $pagado[0];
			$interesasignado[$plazo] = $interesasignado[0];
			$tasaarray[$plazo] = $tasaarray[0];
			$cargototalarray[$plazo] = $cargototalarray[0];


			$data = array();
			$n = -1;
			$num = 0;
			for ($i=0; $i<$plazo+1; $i++){
				$n++;
				$num++;
				$fe = date("d-m-Y", strtotime("$fecha + $n months"));
				$data[] = array(
					'num' => $num,
					'fecha' => $fe,
					'capital' => $capital[$i],
					'principal' => $pagado[$i],
					'interes' => $interesasignado[$i],
					'tasa' => $tasaarray[$i],
					'cargo' => $cargototalarray[$i],
					'total' => $pagado[$i] + $interesasignado[$i] + $cargototalarray[$i]
				);
				$totalsuma[] = $pagado[$i];
				$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototalarray[$i];
			}
		} else {
			$data = array();
			$n = -1;
			$num = 0;
			for ($i=0; $i<$plazo; $i++){
				$n++;
				$num++;
				$fe = date("d-m-Y", strtotime("$fecha + $n months"));
				$data[] = array(
					'num' => $num,
					'fecha' => $fe,
					'capital' => $capital[$i],
					'principal' => $pagado[$i],
					'interes' => $interesasignado[$i],
					'tasa' => $tasa*1,
					'cargo' => $cargototal,
					'total' => $pagado[$i] + $interesasignado[$i] + $cargototal
				);
				
				$totalsuma[] = $pagado[$i];
				$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototal;
			}
		}


		$finalcapital = $capital[$plazo-1] - $capital[$plazo-1];
		$totalprincipal = array_sum($pagado);
		$totalintereses = array_sum($interesasignado);
		$totalcargo = $cargototal * $plazo;
		

		$cantcobros = $this->cobros_model->get_by_aprobacion_id($aprobacion_id);

		$output = array(
			"data" => $data,
			"suma" => $suma * 1,
			"intereses" => $intereses,
			"totalpagar" => array_sum($totalsuma) + $intereses,
			"finalcapital" => $finalcapital,
			"totalprincipal" => $totalprincipal,
			"totalcargo" => $totalcargo,
			"totalintereses" => $totalintereses,
			"totalcobrar" => array_sum($totalcobrar),
			"cobros" => $cantcobros,
			"plazo" => $plazo,
			"cuotaperiodica" => $cuotaperiodica
		);

		return $output;
	}


	public function pagos(){
		$this->form_validation->set_rules('fecha_pago','fecha de pago','required|trim|xss_clean');
		$this->form_validation->set_rules('forma_pago','forma de pago','required|callback_check_default');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {

			$aprobacion = $this->aprobacion_model->get_by_id($this->input->post('aprobacion_id'));
			$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($aprobacion->clientes_id);
			$this->solicitudes_model->update(array('id' => $solicitud->id), array('usuarios_id' => $this->session->userdata('id_user')));

			$fechad = date('d-m-Y', strtotime(str_replace("/","-",$this->input->post('fecha_pago'))));

			$monto = $this->input->post('monto_pago');

			if ($monto == NULL || $monto == "") {
				$cantcobros = $this->cobros_model->get_by_aprobacion_id($aprobacion->id);
				$cotizacion = $this->cobro_calculos($aprobacion->id);
				if ($cantcobros == 0) {
					$monto = $cotizacion['data'][0]['total'];
				} else {
					$monto = $cotizacion['data'][1]['total'];
				}				
			}

			$nombre_archivo = $this->input->post('nombre_archivo');

			if ($nombre_archivo == null || $nombre_archivo == "") {
		        $filename = NULL;
			} else {
		        $archivo = 'file';
		        $config['upload_path'] = "files/pagos/";
		        $config['file_name'] = $nombre_archivo;
		        $config['allowed_types'] = "*";
		        $config['overwrite'] = FALSE;
		        $this->load->library('upload', $config);	        
		        if (!$this->upload->do_upload($archivo)) {
		            echo json_encode($this->upload->display_errors());
		            return;
		        }
				$data = $this->upload->data();
				$filename = $data['file_name'];
			}

			$payment = $this->return_payment_view($aprobacion->id);
			$pendiente = $payment->pendiente1;
			$cargo_faltante = $payment->cargo_administrativo1;

			$dato = $this->return_verify_fecha_cobro($fechad,$aprobacion);
			if (isset($dato['arreglo'])) {
				$num = count($dato['arreglo']);
				for ($i=0; $i < $num; $i++) { 
					$data1 = array(
						'deuda' => $dato['arreglo'][$i]['deuda'],
						'fecha_correspondiente' => date('Y-m-d', strtotime(str_replace("/","-",$dato['arreglo'][$i]['fecha']))),
						'principal' => $dato['arreglo'][$i]['principal'],
						'interes' => $dato['arreglo'][$i]['interes'],
						'tasa_interes' => $dato['arreglo'][$i]['tasa'],
						'cargo_administrativo' => $dato['arreglo'][$i]['cargo'],
						'interes_mora' => $dato['arreglo'][$i]['imora'],
						'total' => $dato['arreglo'][$i]['total'],
						'aprobacion_id' => $this->input->post('aprobacion_id'),
						'usuarios_id' => $this->session->userdata('id_user')
					);
					if ($i == ($num - 1)) {

						$monto = $monto * 1;
						$deuda = $dato['arreglo'][$i]['deuda'];
						$fecha_correspondiente = date('Y-m-d', strtotime(str_replace("/","-",$dato['arreglo'][$i]['fecha'])));
						$fecha_pago = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_pago'))));
						$cargo_administrativo = $dato['arreglo'][$i]['cargo'];
						$tasa_interes = $dato['arreglo'][$i]['tasa'];
						$interes = $dato['arreglo'][$i]['interes'];

						if ($monto >= $pendiente) {
							$cargo_administrativo = $cargo_faltante;
							$fechaa = date("d-m-Y", strtotime("$fecha_correspondiente - 1 months"));
							$dias = $this->dias_transcurridos($fechaa,$fecha_pago);
							$tasadiaria = ($tasa_interes / 100) / 30;
							if ($dias > 0 && $dias < 30) {
								$tasa = $dias * $tasadiaria;
								$interes = $deuda * $tasa;
							}
						}

						$abono = 0;
						$mora = round($dato['data']['imora'],2);
						$cargo = round($cargo_administrativo,2);
						$interes = round($interes,2);
						$pago = round($monto,2);
						$pago = round($pago - $mora,2);
						$interes_restante = round($interes,2);
						if ($pago > 0) {
							$pago = round($pago - $cargo,2);
							if ($pago > 0) {
								$cargo_restante = 0;
								$pago = round($pago - $interes,2);
								if ($pago > 0) {
									$interes_restante = 0;
									$abono = round($pago,2);
								} else {
									$interes_restante = round(-1 * $pago,2);
								}
							} else {
								$cargo_restante = round(-1 * $pago,2);
							}
						}

						$data1['fecha_pago'] = $fecha_pago;
						$data1['forma_pago'] = $this->input->post('forma_pago');
						$data1['nota'] = $this->input->post('nota');
						$data1['archivo'] = $filename;

						$data1['abono'] = $abono;
						$data1['interes_restante'] = $interes_restante;
						$data1['interes'] = $interes;
						$data1['cargo_restante'] = $cargo_restante;
						$data1['cargo_administrativo'] = $cargo_administrativo;
						$data1['interes_mora'] = $dato['arreglo'][$i]['imora'];					
						$data1['total'] = $dato['arreglo'][$i]['principal'] + $interes + $cargo_administrativo + $dato['arreglo'][$i-1]['imora'];
						$data1['monto'] = $monto;
					} else {
						$data1['abono'] = 0;
						$data1['interes_restante'] = $dato['arreglo'][$i]['interes'];
						$data1['cargo_restante'] = $dato['arreglo'][$i]['cargo'];
						$data1['fecha_pago'] = date('0000-00-00');
						$data1['forma_pago'] = '';
						$data1['nota'] = '';
						$data1['monto'] = 0;
					}
					$insert = $this->cobros_model->save($data1);
				}
			} else {

				$monto = $monto * 1;
				$deuda = $dato['data']['deuda'];
				$fecha_correspondiente = date('Y-m-d', strtotime(str_replace("/","-",$dato['data']['fecha'])));
				$fecha_pago = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_pago'))));
				$cargo_administrativo = $dato['data']['cargo'];
				$tasa_interes = $dato['data']['tasa'];
				$interes = $dato['data']['interes'];

				if ($monto >= $pendiente) {
					$cargo_administrativo = $cargo_faltante;
					$fechaa = date("d-m-Y", strtotime("$fecha_correspondiente - 1 months"));
					$dias = $this->dias_transcurridos($fechaa,$fecha_pago);
					$tasadiaria = ($tasa_interes / 100) / 30;
					if ($dias > 0 && $dias < 30) {
						$tasa = $dias * $tasadiaria;
						$interes = $deuda * $tasa;
					}
				}

				$abono = 0;
				$mora = round($dato['data']['imora'],2);
				$cargo = round($cargo_administrativo,2);
				$interes = round($interes,2);
				$pago = round($monto,2);
				$pago = round($pago - $mora,2);
				$interes_restante = round($interes,2);

				if ($pago > 0) {
					$pago = round($pago - $cargo,2);
					if ($pago > 0) {
						$cargo_restante = 0;
						$pago = round($pago - $interes,2);
						if ($pago > 0) {
							$interes_restante = 0;
							$abono = round($pago,2);
						} else {
							$interes_restante = round(-1 * $pago,2);
						}
					} else {
						$cargo_restante = round(-1 * $pago,2);
					}
				}

				$data1 = array(
					'deuda' => $deuda,
					'fecha_correspondiente' => $fecha_correspondiente,
					'abono' => $abono,
					'principal' => $dato['data']['principal'],
					'interes_restante' => $interes_restante,
					'interes' => $interes,
					'tasa_interes' => $tasa_interes,
					'cargo_restante' => $cargo_restante,
					'cargo_administrativo' => $cargo_administrativo,
					'interes_mora' => $dato['data']['imora'],
					'total' => $dato['data']['total'],
					'fecha_pago' => $fecha_pago,
					'forma_pago' => $this->input->post('forma_pago'),
					'nota' => $this->input->post('nota'),
					'monto' => $monto,
					'archivo' => $filename,
					'aprobacion_id' => $this->input->post('aprobacion_id'),
					'usuarios_id' => $this->session->userdata('id_user')
				);

				$insert = $this->cobros_model->save($data1);


			}		
			echo json_encode(array("status" => TRUE));
		}
	}

	public function status_cobro($id){
		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_aprobacion_id($id);
		$data = array(
			'estados_id' => 8,
			'usuarios_id' => $this->session->userdata('id_user')
		);
		$this->solicitudes_model->update(array('id' => $solicitudes_aprobacion->solicitudes_id), $data);
		$this->aprobacion_model->update(array('id' => $id), array('cobrado' => 1));
		echo json_encode(array("status" => TRUE));
	}

	public function status_cobro_empresa($id){
		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_aprobacion_id($id);
		$data = array(
			'estados_id' => 8,
			'usuarios_id' => $this->session->userdata('id_user')
		);
		$this->solicitudes_model->update(array('id' => $solicitudes_aprobacion->solicitudes_id), $data);
		$this->aprobacion_model->update(array('id' => $id), array('cobrado' => 1));
	}

	public function status_cobro_adelantado($id){
		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_aprobacion_id($id);
		$data = array(
			'estados_id' => 10,
			'usuarios_id' => $this->session->userdata('id_user')
		);
		$this->solicitudes_model->update(array('id' => $solicitudes_aprobacion->solicitudes_id), $data);
		$this->aprobacion_model->update(array('id' => $id), array('cobrado' => 1));
		echo json_encode(array("status" => TRUE));
	}

	public function check_anticipado($id){
		$data = $this->aprobacion_model->get_by_id($id);		
		$cantcobros = $this->cobros_model->get_by_aprobacion_id($id);
		$porcentaje = $data->porcentaje;
		if ($porcentaje == 50) {
			$cobrosfaltantes = ($data->cantidad_meses + 1) - $cantcobros;
		} else {
			$cobrosfaltantes = $data->cantidad_meses - $cantcobros;
		}
		echo json_encode(array("cobros" => $cantcobros, "cobrosfaltantes" => $cobrosfaltantes));
	}

	public function pago_anticipado(){

		$this->form_validation->set_rules('fecha_pago','fecha de pago','required|trim|xss_clean');
		$this->form_validation->set_rules('forma_pago','forma de pago','required|callback_check_default');
		$this->form_validation->set_rules('monto_pago','monto del pago','required|trim|xss_clean');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$aprobacion = $this->aprobacion_model->get_by_id($this->input->post('aprobacion_id'));		
			$cantcobros = $this->cobros_model->get_by_aprobacion_id($this->input->post('aprobacion_id'));

			$fecha = $aprobacion->fecha_primer_descuento;
			//$fe = date("d-m-Y", strtotime("$fecha + $cantcobros months"));

			$porcentaje = $aprobacion->porcentaje;
			if ($porcentaje == 50) {
				//$cant = $cantcobros + 1;
				$fec = date("d-m-Y", strtotime("$fecha + $cantcobros months"));
				$cobrosfaltantes = ($aprobacion->cantidad_meses + 1) - $cantcobros;
			} else {
				$fec = date("d-m-Y", strtotime("$fecha + $cantcobros months"));
				$cobrosfaltantes = $aprobacion->cantidad_meses - $cantcobros;
			}

			$fechas = array();
			for ($i=0; $i < $cobrosfaltantes; $i++) {
				$fechas[] = date("d-m-Y", strtotime("$fec + $i months"));
			}


/*			$array = array();
			for ($j=0; $j < $cobrosfaltantes; $j++) {
				$array[] = array(
					"fecha_correspondiente" => $fechas[$j],
					'fecha_pago' => date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_pago')))),
					'nota' => $this->input->post('nota'),
					'pago_adelantado' => 'Si',
					'aprobacion_id' => $this->input->post('aprobacion_id')
				);
			}*/

			//echo json_encode($array);

			if ($this->input->post('clientes_id') != null) {
				$request = $this->solicitudes_model->get_solicitud_by_clientes_id($this->input->post('clientes_id'));
				$data1 = array('usuarios_id' => $this->session->userdata('id_user'));
				$update = $this->solicitudes_model->update(array('id' => $request->id), $data1);
			}

			$nombre_archivo = $this->input->post('nombre_archivo');

			if ($nombre_archivo == null || $nombre_archivo == "") {
		        $filename = NULL;
			} else {
		        $archivo = 'file';
		        $config['upload_path'] = "files/pagos/";
		        $config['file_name'] = $nombre_archivo;
		        $config['allowed_types'] = "*";
		        $config['overwrite'] = FALSE;
		        $this->load->library('upload', $config);	        
		        if (!$this->upload->do_upload($archivo)) {
		            echo json_encode($this->upload->display_errors());
		            return;
		        }
				$data = $this->upload->data();
				$filename = $data['file_name'];
			}


			for ($j=0; $j < $cobrosfaltantes; $j++) {


				$comparar = $this->comparar_fechas(date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_pago')))),date('Y-m-d', strtotime(str_replace("/","-",$fechas[$j]))));
				$calculos = $this->cobro_calculos($this->input->post('aprobacion_id'));

		/*		$output = array( Para ver que traeee calculosssss------------------
					"data" => $data,$calculos['data']
					"suma" => $suma * 1,
					"intereses" => $intereses,
					"totalpagar" => $totalpagar,
					"totalprincipal" => $totalprincipal,
					"totalcargo" => $totalcargo,
					"totalintereses" => $totalintereses,
					"totalcobrar" => $totalcobrar,
					"cobros" => $cantcobros
				);*/

				$dat = null;
				if ($comparar == "mayor") {

					$cob = $calculos['data'][$j+$cantcobros]['total'];
					$porcentajemora = 2/100;
					$mora = $cob * $porcentajemora;
					$total = $cob + $mora;

					$dat = $total;
				} else {
					$dat = $calculos['data'][$j+$cantcobros]['total'];
				}

				$data = array(
					"fecha_correspondiente" => date('Y-m-d', strtotime(str_replace("/","-",$fechas[$j]))),
					'fecha_pago' => date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_pago')))),
					'forma_pago' => $this->input->post('forma_pago'),
					'nota' => $this->input->post('nota'),
					'monto' => $dat,
					'pago_adelantado' => 'Si',
					"archivo" => $filename,
					'aprobacion_id' => $this->input->post('aprobacion_id'),
					'usuarios_id' => $this->session->userdata('id_user')
				);
				$insert = $this->cobros_model->save($data);
			}
			
			echo json_encode(array("status" => TRUE));

		}
	}

	public function cobros_mes(){
		$data['title'] = "Prestamos 911";
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['estado_cobros'] = $this->cobros_model->get_estado_cobros();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/cobros/cobros_mes',$data);
		$this->load->view('templates/footer',$data);
	}

	public function cobros_empresas(){
		$data['title'] = "Prestamos 911";
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$this->load->view('templates/header',$data);
		$this->load->view('admin/cobros/cobros_empresas',$data);
		$this->load->view('templates/footer',$data);
	}

	public function desempleados(){
		$data['title'] = "Prestamos 911";
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['estado_cobros'] = $this->cobros_model->get_estado_cobros();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/cobros/desempleados',$data);
		$this->load->view('templates/footer',$data);
	}

	public function cobros_completos(){
		$data['title'] = "Prestamos 911";
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$this->load->view('templates/header',$data);
		$this->load->view('admin/cobros/cobros_completos',$data);
		$this->load->view('templates/footer',$data);
	}

	public function sin_garantias(){
		$data['title'] = "Prestamos 911";
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['estado_cobros'] = $this->cobros_model->get_estado_cobros();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/cobros/sin_garantias',$data);
		$this->load->view('templates/footer',$data);
	}

	public function ajax_total_deudas() {
	    $total = $this->deuda_total_model->get_sumary();
	    $totales = $this->deuda_total_model->totals($_POST['estado']);

	    $response = [
	        'neto' => $total->neto,
            'bruto' => $total->bruto,
            'cantidad' => $total->cantidad,
            'total_activos' => $totales['total_activos'],
            'total_desempleados' => $totales['total_desempleados'],
            'total_sin_garantias' => $totales['total_sin_garantias'],
            'total_cancelados' => $totales['total_cancelados'],
        ];

	    echo json_encode($response);
    }

	public function ajax_list_total_deudas()	{
		$data = array();
		$list = $this->deuda_total_model->get_datatables();
		foreach ($list as $fila) {
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->fecha_entrega;
			$row[] = $fila->cantidad_aprobada;
			$row[] = $fila->deuda;
			$row[] = $fila->cargos;
			$row[] = $fila->plazo_faltante;
			$row[] = $fila->deuda_total;
			$data[] = $row;
		}
		$output = array(
			"recordsFiltered" => $this->deuda_total_model->count_filtered(),
			"recordsTotal" => $this->deuda_total_model->count_all(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function total_deudas() {
		$data['title'] = "Prestamos 911";
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		
		// Calculo deuda total...
		// clientes que cumplen esta condicion where cli.estados_cobro_id NOT IN (9,10,11,12,13,14,15) OR cli.estados_cobro_id IS NULL
		//$this->db->select_sum('deuda');
    	//$this->db->from('deuda_clientes');
    	//$query = $this->db->get();
		$data['total_actives'] = 0; //is_null($query->row()->deuda) ? 0 : $query->row()->deuda;
		// Calculo deuda desempleados...
		$this->db->select_sum('cantidad_aprobada');
    	$this->db->from('desempleados_view');
		$query = $this->db->get();
		$data['total_unployed'] = 0; //is_null($query->row()->cantidad_aprobada) ? 0 : $query->row()->cantidad_aprobada;
		// Calculo deuda sin garantia
		// clientes que cumplen esta condicion where cli.estados_cobro_id IN (9,10,11,12,13,14,15)
		$data['total_no_warranty'] = 0;

		$this->load->view('templates/header',$data);
		$this->load->view('admin/cobros/total_deudas',$data);
		$this->load->view('templates/footer',$data);
	}

/*	public function select_cobros(){
		echo json_encode(date('d-m-Y'));
	}*/

	public function save_estado_cobro(){

		$aprobacion = $this->aprobacion_model->get_by_id($this->input->post('id'));

		$agenda = $this->agenda_retiro_model->get_by_aprobacion($aprobacion->id);
		if ($agenda) {
			$this->agenda_retiro_model->delete_by_id($agenda->id);
		}

		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		if ($desempleado) {
			$this->desempleados_model->delete_by_id($desempleado->id);
		}

		$data = array(
			"estados_cobro_id" => $this->input->post('estados_cobro_id')
		);
		$this->clientes_model->update(array('id' => $aprobacion->clientes_id), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function save_estado_cobro_agenda(){
		$this->form_validation->set_rules('fecha_retiro','fecha de retiro','required|trim|xss_clean');
		$this->form_validation->set_rules('hora_retiro','hora de retiro','required|trim|xss_clean');
		$this->form_validation->set_rules('persona_contacto','persona de contacto','required|trim|xss_clean');
		$this->form_validation->set_rules('numero_cheque','numero de cheque','required|trim|xss_clean');
		$this->form_validation->set_rules('monto_cheque','monto del cheque','required|trim|xss_clean');
		$this->form_validation->set_rules('direccion_cobro','dirección','required|trim|xss_clean');
		$this->form_validation->set_rules('telefono_cobro','telefono','required|trim|xss_clean');
		$this->form_validation->set_rules('persona_dirigirse','persona a quien dirigirse','required|trim|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {

			$data = array(
				"estados_cobro_id" => $this->input->post('estado_cobro')
			);
			$this->clientes_model->update(array('id' => $this->input->post('clientes_id_cobro')), $data);

			$data1 = array(
				"fecha_retiro" => date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_retiro')))),
				"hora_retiro" => $this->input->post('hora_retiro'),
				"persona_contacto" => $this->input->post('persona_contacto'),
				"numero_cheque" => $this->input->post('numero_cheque'),
				"monto_cheque" => $this->input->post('monto_cheque'),
				"direccion_cobro" => $this->input->post('direccion_cobro'),
				"telefono_cobro" => $this->input->post('telefono_cobro'),
				"persona_dirigirse" => $this->input->post('persona_dirigirse'),
				"aprobacion_id" => $this->input->post('aprobacion_id_cobro')
			);

			$agenda = $this->agenda_retiro_model->get_by_aprobacion($this->input->post('aprobacion_id_cobro'));
			if ($agenda) {
				$this->agenda_retiro_model->update(array('id' => $agenda->id), $data1);
			} else {
				$insert = $this->agenda_retiro_model->save($data1);
			}
			
			echo json_encode(array("status" => TRUE));
		}
	}

	public function consulta_estado_cobro($id){

		$aprobacion = $this->aprobacion_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);

		$data = array(
			'cliente_id' => $cliente->id,
			'cliente' => $cliente->nombre . " " . $cliente->apellido,
			'estado_cobro' => $cliente->estados_cobro_id
		);

		$cotizacion = $this->cobro_calculos($aprobacion->id);


		$data['info'] = array(
			'cantidad_aprobada' => $aprobacion->cantidad_aprobada,
			'pago_mensual' => round($cotizacion['totalcobrar'] / $cotizacion['plazo'], 2),
			'pago_total' => $cotizacion['totalcobrar']
		);

		$array = array();
		$agenda = $this->agenda_retiro_model->get_by_aprobacion($aprobacion->id);
		if ($agenda) {
			$array = array(
				'fecha_retiro' => date('d/m/Y', strtotime(str_replace("/","-",$agenda->fecha_retiro))),
				'hora_retiro' => $agenda->hora_retiro,
				'persona_contacto' => $agenda->persona_contacto,
				'numero_cheque' => $agenda->numero_cheque,
				'monto_cheque' => $agenda->monto_cheque,
				'direccion_cobro' => $agenda->direccion_cobro,
				'telefono_cobro' => $agenda->telefono_cobro,
				'persona_dirigirse' => $agenda->persona_dirigirse,
			);
			$data['agenda'] = $array;
		}

		$array1 = array();
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		if ($desempleado) {
			$array1 = array(
				'fecha_salida' => date('d/m/Y', strtotime(str_replace("/","-",$desempleado->fecha_salida)))
			);

			$data['desempleado'] = $array1;
		}

		$numcobros = $this->cobros_model->get_by_aprobacion_id($aprobacion->id);
		if ($numcobros) {
			$fecha = $aprobacion->fecha_primer_descuento;
			$cant = $numcobros - 1;
			$fec = date("d-m-Y", strtotime("$fecha + $cant months"));
			$array2 = array(
				'fecha_correspondiente' => date('d/m/Y', strtotime(str_replace("/","-",$fec)))
			);
			$data['cobros'] = $array2;
		} else {
			$array2 = array(
				'fecha_correspondiente' => 'Ninguno'
			);
			$data['cobros'] = $array2;
		}

		echo json_encode($data);
	}

	public function direccion_compania($id){
		$aprobacion = $this->aprobacion_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
		$companias = $this->companias_model->get_by_id($cliente->companias_id);
		echo json_encode($companias->direccion);
	}


	public function verify_fecha_pago(){

		$fecha = date('d-m-Y', strtotime(str_replace("/","-",$this->input->post('fecha'))));
		$aprobacion = $this->aprobacion_model->get_by_id($this->input->post('aprobacion_id'));
		$fecha_primer_descuento = $aprobacion->fecha_primer_descuento;
		$cantcobros = $this->cobros_model->get_by_aprobacion_id($aprobacion->id);
		$fec = date("d-m-Y", strtotime("$fecha_primer_descuento + $cantcobros months"));

		$comparar = $this->comparar_fechas($fecha,$fec);
		$calculos = $this->cobro_calculos($aprobacion->id);

		$data['cliente_id'] = $aprobacion->clientes_id;
		if ($comparar == "mayor") {
			$cob = $calculos['data'][$cantcobros]['total'];
			$porcentajemora = 2/100;
			$mora = $cob * $porcentajemora;
			$total = $cob + $mora;
			$data['monto_pago'] = $total;
		} else {
			$data['monto_pago'] = $calculos['data'][$cantcobros]['total'];
		}		
		echo json_encode($data);
		
	}

	public function verify_fecha_pago_total(){

		$aprobacion = $this->aprobacion_model->get_by_id($this->input->post('aprobacion_id'));
		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_aprobacion_id($aprobacion->id);
		$solicitud = $this->solicitudes_model->get_by_id($solicitudes_aprobacion->solicitudes_id);	
		$cantcobros = $this->cobros_model->get_by_aprobacion_id($aprobacion->id);

		$fecha = $aprobacion->fecha_primer_descuento;

		$porcentaje = $aprobacion->porcentaje;
		if ($porcentaje == 50) {
			$cant = $cantcobros + 1;
			$fec = date("d-m-Y", strtotime("$fecha + $cant months"));
			$cobrosfaltantes = ($aprobacion->cantidad_meses + 1) - $cantcobros;
		} else {
			$fec = date("d-m-Y", strtotime("$fecha + $cantcobros months"));
			$cobrosfaltantes = $aprobacion->cantidad_meses - $cantcobros;
		}

		$fechas = array();
		for ($i=0; $i < $cobrosfaltantes; $i++) {
			$fechas[] = date("d-m-Y", strtotime("$fec + $i months"));
		}

		$fecha = date('d-m-Y', strtotime(str_replace("/","-",$this->input->post('fecha'))));

		$array = array();
		$data['cliente_id'] = $solicitud->clientes_id;
		for ($j=0; $j < $cobrosfaltantes; $j++) {

			$comparar = $this->comparar_fechas($fecha,$fechas[$j]);
			$calculos = $this->cobro_calculos($aprobacion->id);

			if ($comparar == "mayor") {

				$cob = $calculos['data'][$j+$cantcobros]['total'];
				$porcentajemora = 2/100;
				$mora = $cob * $porcentajemora;
				$total = $cob + $mora;

				$array[] = round($total,2);
			} else {
				$array[] = round($calculos['data'][$j+$cantcobros]['total'],2);
			}

		}
		$data['monto_pago'] = array_sum($array);
		echo json_encode($data);		
	}

	public function save_estado_desempleados(){
		$this->form_validation->set_rules('fecha_salida','fecha de salida','required|trim|xss_clean');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {

			$agenda = $this->agenda_retiro_model->get_by_aprobacion($this->input->post('aprobacion_id_cobro'));
			if ($agenda) {
				$this->agenda_retiro_model->delete_by_id($agenda->id);
			}

			$data = array(
				"estados_cobro_id" => $this->input->post('estado_cobro')
			);
			$update = $this->clientes_model->update(array('id' => $this->input->post('clientes_id_cobro')), $data);

			$data1 = array(
				"fecha_salida" => date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_salida')))),
				"aprobacion_id" => $this->input->post('aprobacion_id_cobro')
			);

			$desempleado = $this->desempleados_model->get_by_aprobacion($this->input->post('aprobacion_id_cobro'));
			if ($desempleado) {
				$this->desempleados_model->update(array('id' => $desempleado->id), $data1);
			} else {
				$insert = $this->desempleados_model->save($data1);
			}

			echo json_encode(array("status" => TRUE));

		}

	}

	public function remove_payment($id){

		$aprobacion = $this->aprobacion_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
		$cobros = $this->cobros_model->cobros_by_aprobacion_id($aprobacion->id);
		$cantcobros = $this->cobros_model->get_cobros_adelantados_by_aprobacion_id($aprobacion->id);

		$data = array(
			'nombre_cliente' => $cliente->nombre . " " . $cliente->apellido,
			'cobros_adelantados' => $cantcobros
		);

		if ($cobros) {
			foreach ($cobros as $row) {

				$row->archivo = "<a href='".site_url("files/pagos/".$row->archivo)."' target='_blank'>".$row->archivo."</a>";

				$user_creator = $this->usuarios_model->get_by_id($row->usuarios_id);
				$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
				$cadena = $user_creator->nombres;
				$nombre = explode(' ',$cadena);
				$cadena1 = $user_creator->apellidos;
				$apellido = explode(' ',$cadena1);
				//$row->usuarios_id = $nombre[0] . " " . $apellido[0] . ",<br> <b>" . $rol_creator->descripcion . ".</b>";
				$row->usuarios_id = $nombre[0] . " " . $apellido[0];
			}
			$data['cobros'] = $cobros;
		}

		echo json_encode($data);
	}

	public function remove_payment_to_excel(){
		$id = $this->input->get('id');
		to_excel($this->cobros_model->cobros_by_aprobacion_id_to_excel($id), "cobrosexcel");	
	}

	public function delete_pago($id){
		$cobro = $this->cobros_model->get_by_id($id);
		if ($cobro->archivo != NULL || $cobro->archivo != "") {
			$file = "files/pagos/".$cobro->archivo;
			if ($file) {
				unlink($file);
			}
		}
		$this->cobros_model->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function delete_pago_complet($id){
		$cobro = $this->cobros_model->get_by_id($id);
		$aprobacion_id = $cobro->aprobacion_id;
		if ($cobro->archivo != NULL || $cobro->archivo != "") {
			$file = "files/pagos/".$cobro->archivo;
			if ($file) {
				unlink($file);
			}
		}
		$this->cobros_model->delete_by_id($id);
		$this->aprobacion_model->update(array('id' => $aprobacion_id), array('cobrado' => 0));
		echo json_encode(array("status" => TRUE));
	}

	public function delete_ultimo_pago($id){
		$cobro = $this->cobros_model->cobros_by_aprobacion_id($id);
		if ($cobro) {
			$data = array();
			foreach ($cobro as $row) {
				$data[] = $row->id;
			}
			$data = max($data);
		} else {
			$data = 0;
		}
		if ($data > 0) {
			$cobro = $this->cobros_model->get_by_id($data);
			if ($cobro->archivo != NULL || $cobro->archivo != "") {
				$file = "files/pagos/".$cobro->archivo;
				if ($file) {
					unlink($file);
				}
			}
			$datos = $this->cobros_model->get_by_fecha_pago_and_aprobacion_id($cobro->fecha_pago,$cobro->aprobacion_id);
			if ($datos) {
				foreach ($datos as $fila) {
					$this->cobros_model->delete_by_id($fila->id);
				}
			}
		}
		echo json_encode(array("status" => TRUE));
	}

	public function return_estado($id){
		$aprobacion = $this->aprobacion_model->get_by_id($id);
		$data = array('estado' => "Entregado");
		$this->clientes_model->update(array('id' => $aprobacion->clientes_id), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function view_companies() {
		$data = $this->clientes_model->getCompanies();
		echo json_encode($data);
	}

}