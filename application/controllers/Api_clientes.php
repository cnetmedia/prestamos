<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Api_clientes extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model(array('clientes_model','solicitudes_model','companias_model'));
		$this->load->library(array('form_validation'));
		$this->load->helper(array('url', 'api'));
        $this->load->database('default');
    }
    
    /**
     * @GET: Return the customer created via API
     * 
     * @param integer request_form_id
     */
    public function get_client($requestFormId)
    {
        // Check for a valid GET request....
        $method = $this->input->method();

        if ($method !== 'get') {
            responder(array('errors' => 'Method not allowed'), 405);
        }

        if (!is_numeric($requestFormId)) {
            responder(array('errors' => array('request_form_id' => 'You must pass a valid request form ID')), 422);
        }

        $solicitud = $this->solicitudes_model->get_by_id($requestFormId);

        if (empty($solicitud)) {
            responder(array('errors' => 'request form not found with given ID'), 404);
        }

        $clientId = $solicitud->clientes_id;

        $client = $this->clientes_model->get_by_id($clientId);

        $response = array(
            'data' => array(
                'request_form' => $solicitud,
                'client' => $client
            )
        );

        responder($response, 200);
    }

    /**
     * @POST: Add a new customer via API
     * 
     * @param string nombre
     * @param string apellido
     * @param string ceudla
     */
    public function post_client()
    {
        // Check for a valid POST request....
        $method = $this->input->method();
        if ($method !== 'post') {
            responder(array('errors' => 'Method not allowed'), 405);
        }

        // Translate and clean the request to a POST data object
        $_POST = json_to_post();

        // Validations
        $this->config->load('api_validation_rules');
        $this->form_validation->set_rules($this->config->item('create_client'));

        if ($this->form_validation->run() == FALSE) {
            responder(array('errors' => $this->form_validation->error_array()), 422);
        }

        $gender = $this->input->post('gender');
        if (strtoupper($gender) == 'M') {
            $gender = 'Masculino';
        } else {
            $gender = 'Femenino';
        }

        $documentId = $this->formatDocumentId($this->input->post('documentId'));

        $verifyDocument = $this->clientes_model->get_by_cedula($documentId);

		if ($verifyDocument) {
            responder(array('errors' => array('documentId' => 'Duplicate document')), 422);
        }

        $sourceUrl = $this->input->post('customer_source');

        $formId = 0;

        if ( !empty($this->input->post('id')) ) {
            $sourceUrl .= $this->input->post('id');
            $formId = $this->input->post('id'); // id del formulario al que contiene los datos
        }

        // Get POST data
        $data = array(
            'nombre' => $this->input->post('name'),
            'apellido' => $this->input->post('last_name'),
            'genero' => $gender, // M or F => Masculino o Femenino
            'cedula' => $documentId,
            'estado_civil' => $this->input->post('civil_status'),
            'telefono' => $this->input->post('phone'),
            'correo' => $this->input->post('email'),
            //'fecha_nacimiento' => $this->input->post('birthday', ''),
            'numero_dependientes' => $this->input->post('dependents_quantity'),
            'direccion' => $this->input->post('address'),

            'usuarios_id' => $this->input->post('user_id'), //Usario del sistema que genera la solicitud
            
            'cantidad_solicitada' => $this->input->post('requested_amount'),
            'fuente_cliente' => $sourceUrl,
            'banco' => $this->input->post('bank'),
            'hipotecas' => $this->input->post('mortgage'), // Si / No
            'descuento' => $this->input->post('discount'),
            'tipo_descuento' => $this->input->post('discount_type'),
            'cantidad_descuento' => $this->input->post('discount_quantity'),
            'mes_descuento' => $this->input->post('discount_month'),
            'anho_descuento' => $this->input->post('discount_year'),
            'corregimiento' => $this->input->post('town'),
            'distrito' => $this->input->post('district'),
            'provincia' => $this->input->post('province'),
            'salario' => $this->input->post('salary'),
            'posicion_trabajo' => $this->input->post('work_position'),
            'tiempo_laborando' => $this->input->post('working_time'),
            'nombre_jefe_directo' => $this->input->post('direct_boss_name'),
            'area' => $this->input->post('area'),
            'ministerio' => $this->input->post('ministry'),
            'planilla' => $this->input->post('form'),
            'posicion' => $this->input->post('position'),
            'nombre_referencia' => $this->input->post('reference_name'),
            'apellido_referencia' => $this->input->post('reference_last_name'),
            'parentesco_referencia' => $this->input->post('reference_relationship'),
            'direccion_referencia' => $this->input->post('reference_address'),
            'telefono_referencia' => $this->input->post('reference_phone'),
            'celular_referencia' => $this->input->post('reference_cell_phone'),
            'fecha_creacion' =>  date('Y-m-d H:i:s'),

            'companias_id' => $this->input->post('organization_id')
         );

        // Persist data
        $response = array();
        $statusCode = 200;

        $this->db->trans_begin();

        $clienteId = $this->clientes_model->save($data);
        // crear solicitud con status "Por Aprobar"
        $solicitud = array(
            'clientes_id' => $clienteId,
            'fecha_creacion' =>  date('Y-m-d H:i:s'),
            'estados_id' => 3, // Por Aprobar
            'usuarios_id' => $this->input->post('user_id') // usuario que proceso la solicitud en sistema?
            // fecha_verificacion ?
            // agente ?
            // verificador ?
            // db_access_id ????
        );

        $solicitudId = $this->solicitudes_model->save($solicitud);

        if ($this->db->trans_status() === FALSE) {

            $this->db->trans_rollback();
            $response = array('error' => 'Falló el proceso de creación');
            $statusCode = 400;
        
        } else {            
            $this->db->trans_commit();
            $response = array(
                "data" => array(
                    "request_form_id" => $solicitudId, // id de la solicitud
                    "payload" => $this->input->post(), // data del formulario enviado en el POST
                )
            );
        }

        responder($response, $statusCode);
    }

    /**
     * @GET: Show a list with all availables companies
     * 
     */
    public function get_companies()
    {
        $companies = $this->companias_model->get_companies();
        $total = count($companies);

        $response = array(
            "total" => $total,
            "data" => $companies
        );

        responder($response);
    }

    private function formatDocumentId($val) {
		$patrones = $sustituciones = array();
		$patrones[0] = '/-/';
		$patrones[1] = '/\s/';
		$sustituciones[0] = '';
		$sustituciones[1] = '';

		$val = preg_replace($patrones, $sustituciones, $val);

		if(strlen($val) < 10){
			if(is_numeric($val[0])){
				$val = str_pad($val, 10, "0", STR_PAD_LEFT);				
			}else{
				$aux = substr($val, 1);
				$aux = str_pad($aux, 9, "0", STR_PAD_LEFT);				
				$val = $val[0].$aux;
			}
		}

		if(!preg_match("/^[a-zA-Z0-9]{1}[0-9]{9}$/", $val)){
			return "99-9999-9999";
		}

		$cedula = "";
		$total =strlen($val);
		for ($i=0; $i < $total; $i++) { 
			$cedula .= $val[$i];
			if($i == 1 || $i == 5){
				$cedula .= "-";
			}
		}

		return $cedula;
    }
}