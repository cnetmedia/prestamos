<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Clientes extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('clientes_model','admin_model','usuarios_model','companias_model','aprobacion_model','productos_model','solicitudes_model','estados_model','clientes_view_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('download', 'file', 'url', 'html', 'form'));
		$this->load->database('default');

		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}

	}
	
	public function index()	{
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$inser = FALSE;
		if ($this->admin_model->get_insertar($this->roluser) == 1) { $inser = TRUE; }
		$data['insertar'] = $inser;
		$data['result_tipo_archivos'] = $this->clientes_model->get_tipos_archivos();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/clientes/gestionar_clientes',$data);
		$this->load->view('templates/footer',$data);	
	}

	public function prospectos(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$inser = FALSE;
		if ($this->admin_model->get_insertar($this->roluser) == 1) { $inser = TRUE; }
		$data['insertar'] = $inser;
		$data['result_tipo_archivos'] = $this->clientes_model->get_tipos_archivos();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/clientes/gestionar_prospectos',$data);
		$this->load->view('templates/footer',$data);	
	}

	function check_default($post_string) {
		return $post_string == '0' ? FALSE : TRUE;
	}

	public function ajax_list($filtro = NULL){
		$parameter = $this->input->get('value');
		$list = $this->clientes_view_model->get_datatables($filtro, $parameter);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $client) {

			$no++;
			$row = array();
			$row[] = $client->fecha_creacion;

			if ($client->estado != NULL) {
				if ($client->estado == "Prospecto") {
					$row[] = "<span class='label label-default'>". $client->estado . "</span>";
				} else if ($client->estado == "Por Verificar") {
					$row[] = "<span class='label label-warning'>". $client->estado . "</span>";
				} else if ($client->estado == "Por Aprobar") {
					$row[] = "<span class='label label-info'>". $client->estado . "</span>";
				} else if ($client->estado == "Rechazado" || $client->estado == "Anulado") {
					$row[] = "<span class='label label-danger'>". $client->estado . "</span>";
				} else if ($client->estado == "Por Firmar" || $client->estado == "Por Entregar") {
					$row[] = "<span class='label label-primary'>". $client->estado . "</span>";
				} else if ($client->estado == "Entregado" || $client->estado == "Cobrado" || $client->estado == "Completado" || $client->estado == "Cobro Adelantado") {
					$row[] = "<span class='label label-success'>". $client->estado . "</span>";
				}
			} else {
				if(!empty($client->estados_cobro_id) && $client->estados_cobro_id == 13){
					$row[] = "<span class='label label-info'>Potencial</span>";
				}else{
					$row[] = $client->estado;
				}				
			}			

			$row[] = $client->fuente_cliente;
			$row[] = $client->cliente;
			$row[] = $client->cedula;
			$row[] = '$'.$client->salario;
			$row[] = '$'.$client->cantidad_solicitada;
			$row[] = $client->telefono;
			$row[] = $client->celular;
			$row[] = $client->elegido;

			if ($this->admin_model->get_modificar($this->roluser) == 1 && $this->admin_model->get_eliminar($this->roluser) == 1) {

				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$client->id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="file_client('."'".$client->id."'".')"><i class="fa fa-file"></i> Archivos </a>
					<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_client('."'".$client->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_client('."'".$client->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
					<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$client->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$client->id."'".','."'verificado'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';

			} elseif ($this->admin_model->get_modificar($this->roluser) == 1 && ($this->roluser == 1 || $this->roluser == 2)) {

				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$client->id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="file_client('."'".$client->id."'".')"><i class="fa fa-file"></i> Archivos </a>
					<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_client('."'".$client->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_client('."'".$client->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$client->id."'".','."'verificado'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';

			} elseif ($this->admin_model->get_eliminar($this->roluser) == 1) {

				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$client->id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="file_client('."'".$client->id."'".')"><i class="fa fa-file"></i> Archivos </a>
					<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_client('."'".$client->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$client->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$client->id."'".','."'verificado'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';

			} else {
				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$client->id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="file_client('."'".$client->id."'".')"><i class="fa fa-file"></i> Archivos </a>
					<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_client('."'".$client->id."'".')"><i class="fa fa-folder"></i> Ver </a>';
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->clientes_view_model->count_all($filtro),
			"recordsFiltered" => $this->clientes_view_model->count_filtered($filtro),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_origenes()	{
		$data = $this->clientes_view_model->get_origenes();
		echo json_encode(['data' => $data]);	
	}

	public function ajax_view($id) {
		$data = $this->clientes_model->get_by_id($id);
		if ($data->fecha_nacimiento != NULL) {
			$data->fecha_nacimiento = date('d/m/Y', strtotime(str_replace("/","-",$data->fecha_nacimiento)));
		}
		if ($data->inicio_labores != NULL) {
			$data->inicio_labores = date('d/m/Y', strtotime(str_replace("/","-",$data->inicio_labores)));
		}

		if ($data->genero == NULL) {
			$data->genero = "";
		}
		if ($data->estado_civil == NULL) {
			$data->estado_civil = "";
		}
		if ($data->fecha_nacimiento == NULL) {
			$data->fecha_nacimiento = "";
		}
		if ($data->como_escucho_nosotros == NULL) {
			$data->como_escucho_nosotros = "";
		}
		if ($data->tipo_vivienda == NULL) {
			$data->tipo_vivienda = "";
		}
		if ($data->hipotecas == NULL) {
			$data->hipotecas = "";
		}


		$compania = $this->clientes_model->get_companies_by_id($data->companias_id);
		if ($compania) { $data->companias_id = $compania->nombre; }	
		$user_creator = $this->usuarios_model->get_by_id($data->usuarios_id);
		$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
		$cadena = $user_creator->nombres;
		$nombre = explode(' ',$cadena);
		$cadena1 = $user_creator->apellidos;
		$apellido = explode(' ',$cadena1);
		$data->usuarios_id = $nombre[0] . " " . $apellido[0] . ", " . $rol_creator->descripcion;


		$aprobacion = $this->aprobacion_model->get_entregado_by_clientes_id($data->id);
		if ($aprobacion) {
			$data->aprobacion = TRUE;
			$data->cantidad_aprobada = $aprobacion->cantidad_aprobada;
			$data->cantidad_meses = $aprobacion->cantidad_meses;
			$data->fecha_primer_descuento = $aprobacion->fecha_primer_descuento;
			$data->fecha_entrega = $aprobacion->fecha_entrega;
			$data->archivo = $aprobacion->archivo;
			$data->porcentaje = $aprobacion->porcentaje;
			if ($aprobacion->entregado == 1) {
				$aprobacion->entregado = 'Si';
			} else {
				$aprobacion->entregado = 'No';
			}
			$data->entregado = $aprobacion->entregado;
			if ($aprobacion->cobrado == 1) {
				$aprobacion->cobrado = 'Si';
			} else {
				$aprobacion->cobrado = 'No';
			}
			$data->cobrado = $aprobacion->cobrado;
			$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
			$data->productos_id = $producto->nombre;
		}


		echo json_encode($data);
	}

	public function ajax_edit($id) {
		if ($this->admin_model->get_modificar($this->roluser) == 1) {
			$data = $this->clientes_model->get_by_id($id);
			if ($data->fecha_nacimiento != NULL) {
				$data->fecha_nacimiento = date('d/m/Y', strtotime(str_replace("/","-",$data->fecha_nacimiento)));
			}
			if ($data->inicio_labores != NULL) {
				$data->inicio_labores = date('d/m/Y', strtotime(str_replace("/","-",$data->inicio_labores)));
			}	
			echo json_encode($data);
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar clientes.'));
		}
	}

	public function ajax_add() {
		if ($this->admin_model->get_insertar($this->roluser) == 1) {
			$this->form_validation->set_rules('nombre','nombre','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('apellido','apellido','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('genero','genero','callback_check_default');
			$this->form_validation->set_rules('cedula','cédula','required|trim|min_length[12]|max_length[13]|xss_clean|is_unique[clientes.cedula]');		
			$this->form_validation->set_rules('telefono','teléfono','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('celular','celular','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('estado_civil','estado civil','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('correo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('fecha_nacimiento','fecha de nacimiento','trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('numero_dependientes','numero de dependientes','trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('cantidad_solicitada','cantidad solicitada','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('fuente_cliente','fuente del cliente','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('como_escucho_nosotros','como escucho de nosotros','trim|xss_clean');
			$this->form_validation->set_rules('como_escucho_nosotros_otros','como escucho de nosotros otros','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('tipo_vivienda','tipo de vivienda','callback_check_default');
			$this->form_validation->set_rules('hipotecas','posee hipotecas','callback_check_default');
			$this->form_validation->set_rules('banco','banco de la hipoteca','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('descuento','posee descuento','trim|min_length[2]|xss_clean');
			$this->form_validation->set_rules('anho_descuento','años de descuento','trim|xss_clean');
			$this->form_validation->set_rules('mes_descuento','meses de descuento','trim|xss_clean');
			$this->form_validation->set_rules('cantidad_descuento','cantidad de descuento','trim|xss_clean');
			$this->form_validation->set_rules('tipo_descuento','tipo de descuento','trim|xss_clean');
			$this->form_validation->set_rules('capacidad','capacidad','xss_clean|callback_check_default');
			$this->form_validation->set_rules('corregimiento','corregimiento','trim|min_length[4]|xss_clean');		
			$this->form_validation->set_rules('distrito','distrito','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('provincia','provincia','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('direccion','dirección','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('salario','salario','trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('posicion_trabajo','posición de trabajo','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('inicio_labores','inicio de labores','trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('tiempo_laborando','tiempo laborando','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('nombre_jefe_directo','nombre del jefe directo','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('area','área','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('ministerio','ministerio','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('planilla','planilla','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('posicion','posición','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('companias_id','empresa','required|callback_check_default');

			if ($this->input->post('companias_id') == "") {
				$company_id = null;
			} else {
				$company_id = $this->input->post('companias_id');
			}

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {

				$data = array(
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'genero' => $this->input->post('genero'),
					'cedula' => $this->input->post('cedula'),
					'telefono' => $this->input->post('telefono'),
					'celular' => $this->input->post('celular'),
					'estado_civil' => $this->input->post('estado_civil'),
					'correo' => $this->input->post('correo'),
					'numero_dependientes' => $this->input->post('numero_dependientes'),
					'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
					'fuente_cliente' => empty($this->input->post('fuente_cliente')) ? base_url('/') : $this->input->post('fuente_cliente'),
					'como_escucho_nosotros' => $this->input->post('como_escucho_nosotros'),
					'como_escucho_nosotros_otros' => $this->input->post('como_escucho_nosotros_otros'),
					'tipo_vivienda' => $this->input->post('tipo_vivienda'),
					'hipotecas' => $this->input->post('hipotecas'),
					'banco' => $this->input->post('banco'),
					'descuento' => $this->input->post('descuento'),
					'anho_descuento' => $this->input->post('anho_descuento'),
					'mes_descuento' => $this->input->post('mes_descuento'),
					'cantidad_descuento' => $this->input->post('cantidad_descuento'),
					'tipo_descuento' => $this->input->post('tipo_descuento'),
					'capacidad' => $this->input->post('capacidad'),
					'corregimiento' => $this->input->post('corregimiento'),
					'distrito' => $this->input->post('distrito'),
					'provincia' => $this->input->post('provincia'),
					'direccion' => $this->input->post('direccion'),
					'salario' => $this->input->post('salario'),
					'posicion_trabajo' => $this->input->post('posicion_trabajo'),
					'tiempo_laborando' => $this->input->post('tiempo_laborando'),
					'nombre_jefe_directo' => $this->input->post('nombre_jefe_directo'),
					'area' => $this->input->post('area'),
					'ministerio' => $this->input->post('ministerio'),
					'planilla' => $this->input->post('planilla'),
					'posicion' => $this->input->post('posicion'),
					'nombre_referencia' => $this->input->post('nombre_referencia'),
					'apellido_referencia' => $this->input->post('apellido_referencia'),
					'parentesco_referencia' => $this->input->post('parentesco_referencia'),
					'direccion_referencia' => $this->input->post('direccion_referencia'),
					'telefono_referencia' => $this->input->post('telefono_referencia'),
					'celular_referencia' => $this->input->post('celular_referencia'),
					//'fecha_creacion' => $this->input->post('fecha_creacion'),
					//'fecha_modificacion' => $this->input->post('fecha_modificacion'),
					'companias_id' => $company_id,
					'usuarios_id' => $this->session->userdata('id_user'),
					'fecha_creacion' =>  date('Y-m-d H:i:s')
				);

				if ($this->input->post('fecha_nacimiento') != "" || $this->input->post('fecha_nacimiento') != NULL) {
					$data['fecha_nacimiento'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_nacimiento'))));
				}

				if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
					$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
				}

				$insert = $this->clientes_model->save($data);
				echo json_encode(array("status" => TRUE));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para añadir clientes.'));
		}
	}

	public function ajax_update() {
		if ($this->admin_model->get_modificar($this->roluser) == 1) {

			$ced = $this->clientes_model->get_by_cedula($this->input->post('cedula'));
			if ($ced) {
				if ($this->input->post('id') == $ced->id) {
					$is_unique = '';
				}
			} else {
				$is_unique =  '|is_unique[clientes.cedula]';
			}

			$this->form_validation->set_rules('nombre','nombre','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('apellido','apellido','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('genero','genero','callback_check_default');
			$this->form_validation->set_rules('cedula','cédula','required|trim|min_length[8]|max_length[13]|xss_clean'.$is_unique);		
			$this->form_validation->set_rules('telefono','teléfono','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('celular','celular','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('estado_civil','estado civil','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('correo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('fecha_nacimiento','fecha de nacimiento','trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('numero_dependientes','numero de dependientes','trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('cantidad_solicitada','cantidad solicitada','required|trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('fuente_cliente','fuente del cliente','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('como_escucho_nosotros','como escucho de nosotros','trim|xss_clean');
			$this->form_validation->set_rules('como_escucho_nosotros_otros','como escucho de nosotros otros','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('tipo_vivienda','tipo de vivienda','callback_check_default');
			$this->form_validation->set_rules('hipotecas','posee hipotecas','callback_check_default');
			$this->form_validation->set_rules('banco','banco de la hipoteca','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('descuento','posee descuento','trim|min_length[2]|xss_clean');
			$this->form_validation->set_rules('anho_descuento','años de descuento','trim|xss_clean');
			$this->form_validation->set_rules('mes_descuento','meses de descuento','trim|xss_clean');
			$this->form_validation->set_rules('cantidad_descuento','cantidad de descuento','trim|xss_clean');
			$this->form_validation->set_rules('tipo_descuento','tipo de descuento','trim|xss_clean');
			$this->form_validation->set_rules('capacidad','capacidad','xss_clean|callback_check_default');
			$this->form_validation->set_rules('corregimiento','corregimiento','trim|min_length[4]|xss_clean');		
			$this->form_validation->set_rules('distrito','distrito','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('provincia','provincia','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('direccion','dirección','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('salario','salario','trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('posicion_trabajo','posición de trabajo','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('inicio_labores','inicio de labores','trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('tiempo_laborando','tiempo laborando','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('nombre_jefe_directo','nombre del jefe directo','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('area','área','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('ministerio','ministerio','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('planilla','planilla','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('posicion','posición','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('companias_id','empresa','required|callback_check_default');

			if ($this->input->post('companias_id') == "") {
				$company_id = null;
			} else {
				$company_id = $this->input->post('companias_id');
			}

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'genero' => $this->input->post('genero'),
					'cedula' => $this->input->post('cedula'),
					'telefono' => $this->input->post('telefono'),
					'celular' => $this->input->post('celular'),
					'estado_civil' => $this->input->post('estado_civil'),
					'correo' => $this->input->post('correo'),
					'numero_dependientes' => $this->input->post('numero_dependientes'),
					'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
					'fuente_cliente' => $this->input->post('fuente_cliente'),
					'como_escucho_nosotros' => $this->input->post('como_escucho_nosotros'),
					'como_escucho_nosotros_otros' => $this->input->post('como_escucho_nosotros_otros'),
					'tipo_vivienda' => $this->input->post('tipo_vivienda'),
					'hipotecas' => $this->input->post('hipotecas'),
					'banco' => $this->input->post('banco'),
					'descuento' => $this->input->post('descuento'),
					'anho_descuento' => $this->input->post('anho_descuento'),
					'mes_descuento' => $this->input->post('mes_descuento'),
					'cantidad_descuento' => $this->input->post('cantidad_descuento'),
					'tipo_descuento' => $this->input->post('tipo_descuento'),
					'capacidad' => $this->input->post('capacidad'),
					'corregimiento' => $this->input->post('corregimiento'),
					'distrito' => $this->input->post('distrito'),
					'provincia' => $this->input->post('provincia'),
					'direccion' => $this->input->post('direccion'),
					'salario' => $this->input->post('salario'),
					'posicion_trabajo' => $this->input->post('posicion_trabajo'),
					'tiempo_laborando' => $this->input->post('tiempo_laborando'),
					'nombre_jefe_directo' => $this->input->post('nombre_jefe_directo'),
					'area' => $this->input->post('area'),
					'ministerio' => $this->input->post('ministerio'),
					'planilla' => $this->input->post('planilla'),
					'posicion' => $this->input->post('posicion'),
					'nombre_referencia' => $this->input->post('nombre_referencia'),
					'apellido_referencia' => $this->input->post('apellido_referencia'),
					'parentesco_referencia' => $this->input->post('parentesco_referencia'),
					'direccion_referencia' => $this->input->post('direccion_referencia'),
					'telefono_referencia' => $this->input->post('telefono_referencia'),
					'celular_referencia' => $this->input->post('celular_referencia'),
					'fecha_modificacion' => date('Y-m-d H:i:s'),
					'companias_id' => $company_id,
					);

				if ($this->input->post('fecha_nacimiento') != "" || $this->input->post('fecha_nacimiento') != NULL) {
					$data['fecha_nacimiento'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_nacimiento'))));
				}

				if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
					$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
				}

				$this->clientes_model->update(array('id' => $this->input->post('id')), $data);
				echo json_encode(array("status" => TRUE));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar clientes.'));
		}
	}

	public function ajax_delete($id) {
		if ($this->admin_model->get_eliminar($this->roluser) == 1) {
			$archivo = $this->clientes_model->get_archivos_by_cliente_id($id);
			if ($archivo) {
				foreach ($archivo as $value) {
					$archivoid = $value->id;
				}
		    	$archivos = $this->clientes_model->get_archivos_by_id($archivoid);
		    	foreach ($archivos as $row) {
		    		$name = $row->nombre;
		    		$archivo_id = $row->id;
		    	}	    	
				$file = "files/".$name;
				if ($file) {
					unlink($file);
				}
			}
			$this->clientes_model->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para eliminar.'));
		}

	}

	public function view_direcciones(){
		$data = $this->clientes_model->get_banco();
		$data1 = $this->clientes_model->get_corregimiento();
		$data2 = $this->clientes_model->get_distrito();
		$data3 = $this->clientes_model->get_provincia();
		echo json_encode(array(
			"banco" => $data,
			"corregimiento" => $data1,
			"distrito" => $data2,
			"provincia" => $data3,
		));
	}

	public function view_companies() {
		$data = $this->clientes_model->getCompanies();
		echo json_encode($data);
	}

    public function cargar_archivo() {
		if ($this->admin_model->get_insertar($this->roluser) == 1) {
	    	$nombre_archivo = $this->input->post('nombre_archivo');
	    	$tipos_archivos_id = $this->input->post('tipos_archivos_id');
	    	$descripcion = $this->input->post('descripcion_archivo');
	    	$archivo_original = $this->input->post('archivo_original');

	    	if ($archivo_original == null || $archivo_original == "") {
	    		$archivo_original = 'No';
	    	}
	    	if ($nombre_archivo == null || $nombre_archivo == "") {
	    		echo json_encode(array('permission'=> 'Elija el archivo a subir.'));
	    	} elseif ($tipos_archivos_id == null || $tipos_archivos_id == "") {
	    		echo json_encode(array('permission'=> 'Elija el tipo de archivo.'));
	    	} elseif ($descripcion == null || $descripcion == "") {
	    		echo json_encode(array('permission'=> 'Agrege una descripción.'));
	    	} else {

		    	$cliente_id = $this->input->post('cliente_id');

		        $archivo = 'file';
		        $config['upload_path'] = "files/";
		        $config['file_name'] = $nombre_archivo;
		        $config['allowed_types'] = "*";
		        $config['overwrite'] = FALSE;
		/*        $config['max_size'] = "50000";
		        $config['max_width'] = "2000";
		        $config['max_height'] = "2000";*/

		        $this->load->library('upload', $config);
		        
		        if (!$this->upload->do_upload($archivo)) {
		            echo json_encode($this->upload->display_errors());
		            return;
		        }

				//echo json_encode($this->upload->data());

		/*{"file_name":"picture2.jpg","file_type":"image\/jpeg","file_path":"C:\/AppServ\/www\/htdocs\/Prestamos911
		\/files\/","full_path":"C:\/AppServ\/www\/htdocs\/Prestamos911\/files\/picture2.jpg","raw_name":"picture2"
		,"orig_name":"picture.jpg","client_name":"picture.jpg","file_ext":".jpg","file_size":25.12,"is_image"
		:true,"image_width":220,"image_height":220,"image_type":"jpeg","image_size_str":"width=\"220\" height
		=\"220\""}*/


				$data = $this->upload->data();
				$filename = $data['file_name'];
				$data = array(
					'nombre' => $filename,
					'descripcion' => $descripcion,
					'original' => $archivo_original,
					'tipos_archivos_id' => $tipos_archivos_id,
					'clientes_id' => $cliente_id,
					'usuarios_id' => $this->session->userdata('id_user'),
					'creacion' => date('Y-m-d H:i:s')
				); 
				$this->clientes_model->save_archivos($data);
				echo json_encode(array('status'=> TRUE));
	    	}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para subir archivos.'));
		}
    }

    public function view_files($id){ 	
    	$client = $this->clientes_model->get_cliente_by_id($id);
    	$cliente_nombre = $client->nombre . " " . $client->apellido;
		$list = $this->clientes_model->get_archivos_by_cliente_id($id);
		$data = array();
		foreach ($list as $file) {
			$row = array();
			$user_creator = $this->usuarios_model->get_by_id($file->usuarios_id);
			//$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
			$cadena = $user_creator->nombres;
			$nombre = explode(' ',$cadena);
			$cadena1 = $user_creator->apellidos;
			$apellido = explode(' ',$cadena1);

			$ruta = site_url("files/".$file->nombre);

			$tipo_archivo = $this->clientes_model->get_tipos_archivos_by_id($file->tipos_archivos_id);

			if ($file->original == null || $file->original == "") {
				$file->original = 'No';
			}

			$row[] = $file->nombre;
			$row[] = $tipo_archivo->tipo;
			$row[] = $file->descripcion;
			$row[] = $file->original;
			$row[] = $file->creacion;
			//$row[] = $nombre[0] . " " . $apellido[0] . ",<br> <b>" . $rol_creator->descripcion . ".</b>";
			$row[] = $nombre[0] . " " . $apellido[0];

			if ($this->admin_model->get_eliminar($this->roluser) == 1) {
				$row[] = '<a href='."'".$ruta."'".' target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Ver </a>
					<a href='."'".site_url('clientes/downloads/'.$file->id.'')."'".' class="btn btn-info btn-xs"><i class="fa fa-download"></i> Descargar </a>
		    	 	<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete_file('."'".$file->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
		    } else {
				$row[] = '<a href='."'".$ruta."'".' target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Ver </a>
					<a href='."'".site_url('clientes/downloads/'.$file->id.'')."'".' class="btn btn-info btn-xs"><i class="fa fa-download"></i> Descargar </a>';
		    }
			$data[] = $row;
		}

		$output = array("data" => $data, "cliente" => $cliente_nombre);
		echo json_encode($output);
    }

    public function filtrar_files($tipo_archivo_id){
    	$cliente_id = $this->input->post('cliente_id');	
    	$client = $this->clientes_model->get_cliente_by_id($cliente_id);
    	$cliente_nombre = $client->nombre . " " . $client->apellido;
		$list = $this->clientes_model->get_archivos_by_tipos_archivos_id($tipo_archivo_id,$cliente_id);
		$data = array();
		foreach ($list as $file) {
			$row = array();
			$user_creator = $this->usuarios_model->get_by_id($file->usuarios_id);
			$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
			$cadena = $user_creator->nombres;
			$nombre = explode(' ',$cadena);
			$cadena1 = $user_creator->apellidos;
			$apellido = explode(' ',$cadena1);

			$ruta = site_url("files/".$file->nombre);

			$tipo_archivo = $this->clientes_model->get_tipos_archivos_by_id($file->tipos_archivos_id);

			if ($file->original == null || $file->original == "") {
				$file->original = 'No';
			}

			$row[] = $file->nombre;
			$row[] = $tipo_archivo->tipo;
			$row[] = $file->descripcion;
			$row[] = $file->original;
			$row[] = $file->creacion;
			$row[] = $nombre[0] . " " . $apellido[0] . ",<br> <b>" . $rol_creator->descripcion . ".</b>";

			if ($this->admin_model->get_eliminar($this->roluser) == 1) {
				$row[] = '<a href='."'".$ruta."'".' target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Ver </a>
					<a href='."'".site_url('clientes/downloads/'.$file->id.'')."'".' class="btn btn-info btn-xs"><i class="fa fa-download"></i> Descargar </a>
		    	 	<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete_file('."'".$file->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
		    } else {
				$row[] = '<a href='."'".$ruta."'".' target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Ver </a>
					<a href='."'".site_url('clientes/downloads/'.$file->id.'')."'".' class="btn btn-info btn-xs"><i class="fa fa-download"></i> Descargar </a>';
		    }
			$data[] = $row;
		}

		$output = array("data" => $data, "cliente" => $cliente_nombre);
		echo json_encode($output);

    }

    public function file_delete($file_id){
		if ($this->admin_model->get_eliminar($this->roluser) == 1) {
	    	$archivos = $this->clientes_model->get_archivos_by_id($file_id);
	    	foreach ($archivos as $row) {
	    		$name = $row->nombre;
	    		$archivo_id = $row->id;
	    	}	    	
			$file = "files/".$name;
			if ($file) {
				$do = unlink($file);	 
				if($do != true){
					echo json_encode(array("status" => $do));
				} else {
					$this->clientes_model->delete_archivos_by_id($archivo_id);
					echo json_encode(array("status" => $do));
				}			
			} else {
				echo json_encode(array("error" => "No hay archivos."));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para eliminar.'));
		}
    }

	public function downloads($archivo_id){
        $list = $this->clientes_model->get_archivos_by_id($archivo_id);
		foreach ($list as $row) {
    		$name = $row->nombre;
    	}
    	$file = "files/".$name;    	
        $data = file_get_contents(site_url().$file);
        force_download($name, $data);
	}



	public function add_company() {
		if ($this->admin_model->get_insertar($this->roluser) == 1) {
			$this->form_validation->set_rules('nombre','nombre de la compañia','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('direccion','dirección','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono','teléfono','required|trim|xss_clean');
			$this->form_validation->set_rules('ruc','ruc','trim|xss_clean');
			$this->form_validation->set_rules('sitio_web','sitio web','trim|xss_clean');
			$this->form_validation->set_rules('ano_constitucion','año de constitución','trim|xss_clean');
			$this->form_validation->set_rules('correo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('nombre_rrhh','nombre del recursos humano','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono_rrhh','teléfono del recursos humano','required|trim|xss_clean');
			$this->form_validation->set_rules('nombre_contabilidad','nombre de contabilidad','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono_contabilidad','teléfono de contabilidad','trim|xss_clean');
			$this->form_validation->set_rules('forma_pago','forma de pagos','trim|xss_clean|callback_check_default');
			$this->form_validation->set_rules('cantidad_empleados','cantidad de empleados','trim|xss_clean');
			$this->form_validation->set_rules('rubro','rubro','trim|xss_clean');
			$this->form_validation->set_rules('nombre_firma','nombre firma','trim|xss_clean');
			$this->form_validation->set_rules('posicion_firma','posición firma','trim|xss_clean');

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));

			} else {
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'direccion' => $this->input->post('direccion'),
					'telefono' => $this->input->post('telefono'),
					'ruc' => $this->input->post('ruc'),
					'sitio_web' => $this->input->post('sitio_web'),
					'ano_constitucion' => $this->input->post('ano_constitucion'),
					'correo' => $this->input->post('correo'),
					'nombre_rrhh' => $this->input->post('nombre_rrhh'),
					'telefono_rrhh' => $this->input->post('telefono_rrhh'),
					'nombre_contabilidad' => $this->input->post('nombre_contabilidad'),
					'telefono_contabilidad' => $this->input->post('telefono_contabilidad'),
					'forma_pago' => $this->input->post('forma_pago'),
					'cantidad_empleados' => $this->input->post('cantidad_empleados'),
					'rubro' => $this->input->post('rubro'),
					'nombre_firma' => $this->input->post('nombre_firma'),
					'posicion_firma' => $this->input->post('posicion_firma'),
					'usuarios_id' => $this->session->userdata('id_user'),
				);
				$insert = $this->companias_model->save($data);
				echo json_encode(array("status" => TRUE, "id" => $insert));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para añadir compañias.'));
		}
	}

/*	function calculos_tiempo_laborando($fecha)
	{

		$fecha_actual = date("d-m-Y");
		$partes_fecha_actual = explode("-", $fecha_actual);
		$dia_actual = $partes_fecha_actual[0];
		$mes_actual = $partes_fecha_actual[1];
		$anio_actual = $partes_fecha_actual[2];

		$partes_fecha_recibida = explode("/", $fecha);
		$dia_recibido = $partes_fecha_recibida[0];
		$mes_recibido = $partes_fecha_recibida[1];
		$anio_recibido = $partes_fecha_recibida[2];

		if ($anio_recibido > $anio_actual) {
			return FALSE;
		} else {
			if ($anio_recibido == $anio_actual && $mes_recibido > $mes_actual) {
				return FALSE;
			} else {
				if ($mes_recibido == $mes_actual && $dia_recibido > $dia_actual) {
					return FALSE;
				} else {
					$anhos = $anio_actual - $anio_recibido;
					$meses = $mes_actual - $mes_recibido;
					$dias = $dia_actual - $dia_recibido;

					return [$anhos,$meses,$dias];
				}
			}
		}
	}*/

	function dias_transcurridos($fecha_i,$fecha_f)
	{
		$dias = (strtotime($fecha_i)-strtotime($fecha_f))/86400;
		$dias = abs($dias);
		$dias = floor($dias);		
		return $dias;
	}

	function tiempoTranscurridoFechas($fechaInicio,$fechaFin)
	{
	    $fecha1 = new DateTime($fechaInicio);
	    $fecha2 = new DateTime($fechaFin);
	    $fecha = $fecha1->diff($fecha2);
	    $tiempo = "";
	         
	    //años
	    if($fecha->y > 0)
	    {
	        $tiempo .= $fecha->y;
	             
	        if($fecha->y == 1)
	            $tiempo .= " año, ";
	        else
	            $tiempo .= " años, ";
	    }
	         
	    //meses
	    if($fecha->m > 0)
	    {
	        $tiempo .= $fecha->m;
	             
	        if($fecha->m == 1)
	            $tiempo .= " mes, ";
	        else
	            $tiempo .= " meses, ";
	    }
	         
	    //dias
	    if($fecha->d > 0)
	    {
	        $tiempo .= $fecha->d;
	             
	        if($fecha->d == 1)
	            $tiempo .= " día ";
	        else
	            $tiempo .= " días ";
	    }
	         
/*	    //horas
	    if($fecha->h > 0)
	    {
	        $tiempo .= $fecha->h;
	             
	        if($fecha->h == 1)
	            $tiempo .= " hora, ";
	        else
	            $tiempo .= " horas, ";
	    }
	         
	    //minutos
	    if($fecha->i > 0)
	    {
	        $tiempo .= $fecha->i;
	             
	        if($fecha->i == 1)
	            $tiempo .= " minuto";
	        else
	            $tiempo .= " minutos";
	    }
	    else if($fecha->i == 0) //segundos
	        $tiempo .= $fecha->s." segundos";*/
	         
	    return $tiempo;
	}

	public function calcular_tiempo_laborando(){

		$this->form_validation->set_rules('inicio_labores','inicio de labores','regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$fecha_actual = date("d-m-Y");
			$partes_fecha_actual = explode("-", $fecha_actual);
			$dia_actual = $partes_fecha_actual[0];
			$mes_actual = $partes_fecha_actual[1];
			$anio_actual = $partes_fecha_actual[2];

			$inicio_labores = $this->input->post('inicio_labores');
			$partes_fecha_recibida = explode("/", $inicio_labores);
			$dia_recibido = $partes_fecha_recibida[0];
			$mes_recibido = $partes_fecha_recibida[1];
			$anio_recibido = $partes_fecha_recibida[2];

			$error = '<span class="label label-danger animated shake">Fecha de inicio de labores no valida.</span>';

			if ($anio_recibido > $anio_actual) {
				echo json_encode(array('error' => $error));
			} else {
				if ($anio_recibido == $anio_actual && $mes_recibido > $mes_actual) {
					echo json_encode(array('error' => $error));
				} else {
					if ($anio_recibido == $anio_actual && $mes_recibido == $mes_actual && $dia_recibido > $dia_actual) {
						echo json_encode(array('error' => $error));
					} else {
						$data = $this->tiempoTranscurridoFechas($fecha_actual,date("$dia_recibido-$mes_recibido-$anio_recibido"));
						echo json_encode(array('datos' => $data));
					}
				}
			}
		}

	}


/*    function otros() {

        //paraguardar
		$data = $this->upload->data();
		$filename = $data['file_name'];
		$image_type = $data['image_type'];
		$data = array(
			'imagen' => $filename,
			'tipo_imagen' => $image_type
		); 
		$this->clientes_model->saveArchivos($data);


		$file = "files/" . $filename;
		$do = unlink($file);
		 
		if($do != true){
			echo "Se ha producido un error al intentar eliminar el archivo" . $filename . "<br />";
		}

		$data = file_get_contents("files/american-express.png");

		force_download($filename, $data);

    }*/

}