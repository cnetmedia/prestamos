<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Bitacora extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('admin_model','bitacora_model','usuarios_model','modulos_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}
	}
	
	public function index() {
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->session->userdata('id_rol'));
		$this->load->view('templates/header',$data);
		$this->load->view('admin/bitacora/gestionar_bitacora',$data);
		$this->load->view('templates/footer',$data);
	}

	public function ajax_list()	{

		$list = $this->bitacora_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $binnacle) {

			$usuario = $this->usuarios_model->get_by_id($binnacle->usuarios_id);

			$no++;
			$row = array();
			$row[] = $binnacle->fecha;
			$row[] = $usuario->nombres . " " . $usuario->apellidos;
			$row[] = $binnacle->accion;			
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->bitacora_model->count_all(),
			"recordsFiltered" => $this->bitacora_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function ajax_list_by_cliente($id) {
		$list = $this->bitacora_model->get_by_cliente_id($id);
		$data = array();
		foreach ($list as $binnacle) {
			$row = array();
			$usuario = $this->usuarios_model->get_by_id($binnacle->usuarios_id);
			$row[] = $binnacle->fecha;
			$row[] = $usuario->nombres . " " . $usuario->apellidos;
			$row[] = $binnacle->accion;	
			$data[] = $row;
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}


}