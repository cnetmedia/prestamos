<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Cotizacion extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('cotizacion_model','admin_model','usuarios_model','productos_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');

		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}

	}
	
	public function index()	{
		$data['title'] = "Prestamos 911";		
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		//$data['result_productos'] = $this->cotizacion_model->getProductos();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/cotizacion/gestionar_cotizacion',$data);
		$this->load->view('templates/footer',$data);

	}

	// en reportes, mis_solicitudes tambien esta
	public function ajax_list()	{

		$suma = $this->input->post('suma_prestamo');
		$productos_id = $this->input->post('productos_id');
		$plazo = $this->input->post('plazo');

		$fecha = date("d-m-Y");

		$tasa = 0.00;
		$cargo = 0.00;

		if ($productos_id) {
			$products = $this->cotizacion_model->get_products_by_id($productos_id);
			foreach ($products as $row) {
				$tasa = $row->tasa_interes_mensual;
				$cargo = $row->cargo_administrativo;
				$porccargo = $row->cargo_administrativo;
				$porcgasto = empty($row->gastos_notariales) ? 0 : $row->gastos_notariales;
				$porccomision = empty($row->comision) ? 0 : $row->comision;
			}
		}


		$suma = round($suma,2);
		$tasa = round($tasa,2);
		$cargo = round($cargo,2);

		$interes = $tasa / 100;
		//$interes = round($interes,2);

		//m = mensualidad, p = monto prestado, x = interes mensual, n = cantidad de meses;

		$p = $suma;
		$x = $interes;
		$n = $plazo;

		$m = $p * ( $x / ( 1 - ( ( 1 + $x ) ** - $n ) ) );

		$cuotaperiodica = $m;
		$cuotaperiodica = round($cuotaperiodica,2);

		$cargototal = $cargo * ($suma / 100);
		$cargototal = round($cargototal,2);


		$solocargo = $porccargo * ($suma / 100);
		$solocomision = $porccomision * ($suma / 100);
		$sologasto = $porcgasto * ($suma / 100);

		$solocargo = round($solocargo,2);
		$solocomision = round($solocomision,2);
		$sologasto = round($sologasto,2);

		$pagado = array();	
		$interesasignado = array();

		$sum = $suma;
		for ($i=0; $i<$plazo; $i++) {
			$interesi = $sum * $interes;
			$interesi = round($interesi,2);
			$valori = $cuotaperiodica - $interesi;
			$valori = round($valori,2);
			$restoi = $sum - $valori;
			$restoi = round($restoi,2);
			$sum = $restoi;
			$pagado[$i] = $valori;
			$interesasignado[$i] = $interesi;
		}


		$totalintereses = array_sum($interesasignado);
		$totalintereses = round($totalintereses,2);
		$totalcargo = $cargototal * $plazo;
		$totalcargo = round($totalcargo,2);
		$totalgasto = $sologasto * $plazo;
		$totalgasto = round($totalgasto,2);
		$totalcomision = $solocomision * $plazo;
		$totalcomision = round($totalcomision,2);


		$totalsuma = array();
		$intereses = $totalcargo + $totalgasto + $totalcomision + array_sum($interesasignado);
		$intereses = round($intereses,2);
		$totalcobrar = array();
		$diferenciasuma = $suma - (array_sum($pagado));
		$diferenciasuma = round($diferenciasuma,2);

		if($diferenciasuma < 0){
			$dec = explode("-0.", $diferenciasuma);
			$num = $dec[1] * 1;
			$m = 0;
			for ($i=0; $i < $num; $i++) {
				$m++;
				$pagado[$plazo-$m] = $pagado[$plazo-$m] - 0.01;
			}
		} elseif($diferenciasuma > 0){
			$dec = explode("0.", $diferenciasuma);
			$num = $dec[1] * 1;
			for ($i=0; $i < $num; $i++) {
				$pagado[$i] = $pagado[$i] + 0.01;
			}
		}

		$data = array();
		$n = 0;
		for ($i=0; $i<$plazo; $i++){
			$n++;
			$fe = date("d-m-Y", strtotime("$fecha + $n months"));
			$data[] = array(
				'fecha' => $fe,
				'principal' => $pagado[$i],
				'interes' => $interesasignado[$i],
				'tasa' => $tasa*1,
				'cargo' => $cargototal,				
				'solocargo' => $solocargo,
				'solocomision' => $solocomision,
				'sologasto' => $sologasto,
				'total' => $pagado[$i] + $interesasignado[$i] + $cargototal + $solocomision + $sologasto
			);
			$totalsuma[] = $pagado[$i];
			$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototal + $solocomision + $sologasto;
		}

		$output = array(
			"data" => $data,
			"suma" => $suma,
			"intereses" => $intereses,
			"totalpagar" => array_sum($totalsuma) + $intereses,
			"totalprincipal" => array_sum($totalsuma),
			"totalcargo" => $totalcargo,
			"totalcomision" => $totalcomision,
			"totalgasto" => $totalgasto,
			"totalintereses" => $totalintereses,
			"totalcobrar" => array_sum($totalcobrar),
			"diferenciasuma" => $diferenciasuma
		);

		echo json_encode($output);
	}

	public function view_products() {		
		$data = $this->cotizacion_model->getProductos();
		echo json_encode($data);
	}

	public function plazo_by_products($id) {
		$data = $this->productos_model->get_by_id($id);	

		$product = $this->cotizacion_model->get_plazo_products_by_id($id);
		foreach ($product as $row) {
			$plazomin = $row->plazo_minimo;
			$plazomax = $row->plazo_maximo;
		}
		$n = $plazomin;
		$array = array();
		for ($i=$plazomin; $i <= $plazomax; $i++) {
			$array[] = $n++;
		}

		echo json_encode(array('data' => $data, 'plazo' => $array));
	}

}