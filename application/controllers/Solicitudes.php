<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Solicitudes extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('solicitudes_model','admin_model','clientes_model','usuarios_model','companias_model','aprobacion_model','tipos_reportes_model','db_access_model','desempleados_model','solicitudes_view_model','cobros_model','cotizacion_model','estados_model','datos_verificacion_model','solicitudes_aprobacion_model','notas_clientes_model','notas_model','disposiciones_model','datos_model','paquetes_reportes_model'));
		$this->load->library(array('session','form_validation','Pdf'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');

		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}

	}
	
	public function index()	{
		$data['title'] = "Prestamos 911";		
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$inser = FALSE;		
		if ($this->admin_model->get_insertar($this->roluser) == 1) {
			$inser = TRUE;
		}
		$data['insertar'] = $inser;
		$this->load->view('templates/header',$data);
		$this->load->view('admin/solicitudes/gestionar_solicitudes',$data);
		$this->load->view('templates/footer',$data);		
	}
	
	public function seguro_social()	{
		$data['title'] = "Prestamos 911";		
		$data['roluser'] = $this->roluser;
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$inser = FALSE;		
		if ($this->admin_model->get_insertar($this->roluser) == 1) {
			$inser = TRUE;
		}
		$data['insertar'] = $inser;
		$data['access'] = $this->db_access_model->get_all();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/solicitudes/seguro_social',$data);
		$this->load->view('templates/footer',$data);		
	}

	function check_default($post_string) {
		return $post_string == '0' ? FALSE : TRUE;
	}

	public function ajax_list($estado = NULL,$disposicion = NULL){

		if ($estado > 0) {
			$estado = $this->estados_model->get_by_id($estado);
			$estado = $estado->estado;
		} else {
			$estado = NULL;
		}

		if ($disposicion > 0) {
			$disposicion = $this->disposiciones_model->get_by_id($disposicion);
			$disposicion = $disposicion->nombre;
		} else {
			$disposicion = NULL;
		}

		$list = $this->solicitudes_view_model->get_datatables($estado,$disposicion);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->fecha_registro;
			$row[] = $fila->origen;
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = '$'.$fila->salario;
			$row[] = '$'.$fila->cantidad_solicitada;

			if ($fila->estado == "Prospecto") {
				$row[] = "<span class='label label-default'>". $fila->estado . "</span>";
			} else if ($fila->estado == "Por Verificar") {
				$row[] = "<span class='label label-warning'>". $fila->estado . "</span>";
			} else if ($fila->estado == "Por Aprobar") {
				$row[] = "<span class='label label-info'>". $fila->estado . "</span>";
			} else if ($fila->estado == "Rechazado" || $fila->estado == "Anulado") {
				$row[] = "<span class='label label-danger'>". $fila->estado . "</span>";
			} else if ($fila->estado == "Por Firmar" || $fila->estado == "Por Entregar") {
				$row[] = "<span class='label label-primary'>". $fila->estado . "</span>";
			} else if ($fila->estado == "Entregado" || $fila->estado == "Cobrado" || $fila->estado == "Completado" || $fila->estado == "Cobro Adelantado") {
				$row[] = "<span class='label label-success'>". $fila->estado . "</span>";
			}

			$row[] = $fila->disposicion;

			$row[] = $fila->asignado;


			if ($this->roluser == 1 || $this->roluser == 2) {
				if ($fila->estado == 'Cobrado' || $fila->estado == 'Cobro Adelantado') {

					$estados = $this->estados_model->get();
					$select = '<select class="btn btn-default btn-xs" onchange="verifychangeestado('.$fila->cliente_id.',this.value)">';
					$select .= '<option value="0">Volver a</option>';
					foreach ($estados as $es) {
						if ($es->id != 5 && $es->id < 8) {
							$select .= '<option value="'.$es->id.'">'.$es->estado.'</option>';
						}
					}
					$select .= '</select>';

					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$fila->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_paquete('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar Paquete</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="admindeselegir('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deselegir</a>'.$select;
				} else if ($fila->estado == 'Completado') {
					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="info_collection('."'".$fila->cliente_id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$fila->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_paquete('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar Paquete</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="admindeselegir('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deselegir</a>';
				} else if ($fila->estado == 'Entregado') {

					$estados = $this->estados_model->get();
					$select = '<select class="btn btn-default btn-xs" onchange="verifychangeestado('.$fila->cliente_id.',this.value)">';
					$select .= '<option value="0">Volver a</option>';
					foreach ($estados as $es) {
						if ($es->id != 5 && $es->id < 7) {
							$select .= '<option value="'.$es->id.'">'.$es->estado.'</option>';
						}
					}
					$select .= '</select>';

					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
				  		<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$fila->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_paquete('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar Paquete</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="admindeselegir('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deselegir</a>'.$select;
				} else if ($fila->estado == 'Por Aprobar' || $fila->estado == 'Por Firmar' || $fila->estado == 'Por Entregar') {

					$estados = $this->estados_model->get();
					$select = '<select class="btn btn-default btn-xs" onchange="verifychangeestado('.$fila->cliente_id.',this.value)">';
					$select .= '<option value="0">Volver a</option>';
					foreach ($estados as $es) {
						if ($fila->estado == 'Por Aprobar' && $es->id < 3) {
							$select .= '<option value="'.$es->id.'">'.$es->estado.'</option>';
						}
						if ($fila->estado == 'Por Firmar' && $es->id < 4) {
							$select .= '<option value="'.$es->id.'">'.$es->estado.'</option>';
						}
						if ($fila->estado == 'Por Entregar' && $es->id < 5) {
							$select .= '<option value="'.$es->id.'">'.$es->estado.'</option>';
						}
					}
					$select .= '</select>';

					$sinrespuesta = '';
					if ($fila->disposicion == 'Sin respuesta') {
						$sinrespuesta = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_ventas('."'".$fila->cliente_id."'".')"><i class="fa fa-envelope-o"></i> Sin Respuesta</a>';
					}

					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="assign_request('."'".$fila->id."'".')"><i class="fa fa-hand-rock-o"></i> Elegir </a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_request('."'".$fila->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
				  		<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$fila->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_paquete('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar Paquete</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="admindeselegir('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deselegir</a>'.$select.$sinrespuesta;
				} else if ($fila->estado == 'Anulado' || $fila->estado == 'Rechazado') {
					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="assign_request('."'".$fila->id."'".')"><i class="fa fa-hand-rock-o"></i> Elegir </a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_request('."'".$fila->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
				  		<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$fila->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="activate('."'".$fila->id."'".')"><i class="fa fa-power-off"></i> Activar</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="admindeselegir('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deselegir</a>';
				} elseif ($fila->estado == 'Por Verificar') {

					$estados = $this->estados_model->get();
					$select = '<select class="btn btn-default btn-xs" onchange="verifychangeestado('.$fila->cliente_id.',this.value)">';
					$select .= '<option value="0">Volver a</option>';
					foreach ($estados as $es) {
						if ($es->id < 2) {
							$select .= '<option value="'.$es->id.'">'.$es->estado.'</option>';
						}
					}
					$select .= '</select>';

					$sinrespuesta = '';
					if ($fila->disposicion == 'Sin respuesta') {
						$sinrespuesta = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_ventas('."'".$fila->cliente_id."'".')"><i class="fa fa-envelope-o"></i> Sin Respuesta</a>';
					}

					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="assign_request('."'".$fila->id."'".')"><i class="fa fa-hand-rock-o"></i> Elegir </a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_request('."'".$fila->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
				  		<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$fila->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="admindeselegir('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deselegir</a>'.$select.$sinrespuesta;
				} else {

					$sinrespuesta = '';
					if ($fila->disposicion == 'Sin respuesta') {
						$sinrespuesta = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_ventas('."'".$fila->cliente_id."'".')"><i class="fa fa-envelope-o"></i> Sin Respuesta</a>';
					}
					
					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="assign_request('."'".$fila->id."'".')"><i class="fa fa-hand-rock-o"></i> Elegir </a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_request('."'".$fila->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
				  		<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$fila->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="admindeselegir('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deselegir</a>'.$sinrespuesta;
				}
			} else {
				if ($fila->estado == 'Cobrado' || $fila->estado == 'Cobro Adelantado' || $fila->estado == 'Completado' || $fila->estado == 'Entregado' || $fila->estado == 'Anulado' || $fila->estado == 'Rechazado') {
					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_paquete('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar Paquete</a>';	
				} else {

					$sinrespuesta = '';
					if ($fila->disposicion == 'Sin respuesta') {
						$sinrespuesta = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_ventas('."'".$fila->cliente_id."'".')"><i class="fa fa-envelope-o"></i> Sin Respuesta</a>';
					}

					$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="assign_request('."'".$fila->id."'".')"><i class="fa fa-hand-rock-o"></i> Elegir </a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="history('."'".$fila->cliente_id."'".')"><i class="fa fa-history"></i> Historial</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>
						<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_paquete('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar Paquete</a>'.$sinrespuesta;	
				}
			}

			$data[] = $row;

		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->solicitudes_view_model->count_all($estado,$disposicion),
			"recordsFiltered" => $this->solicitudes_view_model->count_filtered($estado,$disposicion),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function change_estado(){
		$id = $this->input->post('cliente_id');
		$aprobacion = $this->aprobacion_model->get_entregado_by_clientes_id($id);
		if ($aprobacion) {
			$cobros = $this->cobros_model->cobros_by_aprobacion_id($aprobacion->id);
			if ($cobros) {
				foreach ($cobros as $row) {
					$this->cobros_model->delete_by_id($row->id);
				}
			}
			if ($this->input->post('estado_id') == 7) {
				$data = array(
					'entregado' => 1,
					'cobrado' => 0,
				);
			} else {
				$data = array(
					'entregado' => 0,
					'cobrado' => 0,
				);
			}
			$this->aprobacion_model->update(array('id' => $aprobacion->id), $data);
		}
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($id);
		$data1 = array(
			'estados_id' => $this->input->post('estado_id'),
			'usuarios_id' => $this->session->userdata('id_user'),
		);
		$this->solicitudes_model->update(array('id' => $solicitud->id), $data1);
		$data2 = array(
			'usuarios_id' => NULL,
		);
		$this->solicitudes_model->update(array('id' => $solicitud->id), $data2);
		echo json_encode(array('status' => TRUE));
	}


	public function client_by_cedula($cedula){
		$data = $this->solicitudes_model->get_client_by_cedula($cedula);
		if ($data) {
			if ($data->inicio_labores != NULL || $data->inicio_labores != "") {
				$data->inicio_labores = date('d/m/Y', strtotime(str_replace("/","-",$data->inicio_labores)));
			}
		}
		echo json_encode($data);
	}

	public function assign_request($id){

		$request = $this->solicitudes_model->get_by_id($id);
		if ($this->roluser == 1 || $this->roluser == 2) { // sadmin, admin
			if ($request->usuarios_id == null || $request->usuarios_id == "") {
				$data = array(
					'usuarios_id' => $this->session->userdata('id_user'),
				);
				$this->solicitudes_model->update(array('id' => $id), $data);
				echo json_encode(array("status" => TRUE));
			} else {
				echo json_encode(array("permission" => "La solicitud ya esta elegida por un usuario."));
			}
		} elseif ($this->roluser == 3) { // representante
			if ($request->estados_id > 1) {
				echo json_encode(array("permission" => "No tienes permiso para elegir este estado."));
			} else {
				if ($request->usuarios_id == null || $request->usuarios_id == "") {
					$data = array(
						'usuarios_id' => $this->session->userdata('id_user'),
					);
					$this->solicitudes_model->update(array('id' => $id), $data);
					echo json_encode(array("status" => TRUE));
				} else {
					echo json_encode(array("permission" => "La solicitud ya esta elegida por un usuario."));
				}
			}
		} elseif ($this->roluser == 4) { // supervisor
			if ($request->estados_id < 2) {
				echo json_encode(array("permission" => "No tienes permiso para elegir este estado."));
			} else {
				if ($request->usuarios_id == null || $request->usuarios_id == "") {
					$data = array(
						'usuarios_id' => $this->session->userdata('id_user'),
					);
					$this->solicitudes_model->update(array('id' => $id), $data);
					echo json_encode(array("status" => TRUE));
				} else {
					echo json_encode(array("permission" => "La solicitud ya esta elegida por un usuario."));
				}
			}
		}

	}

	public function deselect_request($id){
		$request = $this->solicitudes_model->get_by_id($id);
		if ($request->usuarios_id == null || $request->usuarios_id == "") {
			echo json_encode(array("permission" => "Ya esta deselegido."));
		} else {
			$this->solicitudes_model->update(array('id' => $id), array('usuarios_id' => NULL));
			echo json_encode(array("status" => TRUE));
		}
	}

	public function ajax_view($id) {
		$data = $this->solicitudes_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($data->clientes_id);
		$data->nombre = $cliente->nombre . " " . $cliente->apellido;
		$data->telefono = $cliente->telefono;
		$data->salario = $cliente->salario;
		$data->cantidad_solicitada = $cliente->cantidad_solicitada;

		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		if ($datos_verificados) {
			$data->comentario_css = $datos_verificados->comentario_css;
			$data->archivo_apc = $datos_verificados->archivo_apc;
			$data->comentario_apc = $datos_verificados->comentario_apc;
			$data->comentario_compania = $datos_verificados->comentario_compania;
			$data->archivo_rp = $datos_verificados->archivo_rp;
			$data->comentario_rp = $datos_verificados->comentario_rp;
		}

		echo json_encode($data);
	}

	public function ajax_edit($id) {
		$data = $this->solicitudes_model->get_by_id($id);
		$data1 = $this->clientes_model->get_by_id($data->clientes_id);
		$data1->solicitud_id = $data->id;
		if ($data1->inicio_labores != NULL || $data1->inicio_labores != "") {
			$data1->inicio_labores = date('d/m/Y', strtotime(str_replace("/","-",$data1->inicio_labores)));
		}
		echo json_encode($data1);
	}

	public function ajax_add() {

		$this->form_validation->set_rules('nombre','nombre','required|trim|min_length[3]|max_length[255]|xss_clean');
		$this->form_validation->set_rules('apellido','apellido','required|trim|min_length[3]|max_length[255]|xss_clean');
		$this->form_validation->set_rules('cedula','cédula','required|trim|min_length[8]|max_length[13]|xss_clean|is_unique[clientes.cedula]');	
		$this->form_validation->set_rules('telefono','teléfono','required|trim|min_length[4]|xss_clean');
		$this->form_validation->set_rules('correo','correo electrónico','required|valid_email|trim|xss_clean');
		$this->form_validation->set_rules('cantidad_solicitada','cantidad solicitada','required|trim|min_length[3]|xss_clean');
		$this->form_validation->set_rules('descuento','posee descuento','required|trim|min_length[2]|xss_clean');
		$this->form_validation->set_rules('cantidad_descuento','cantidad de descuento','trim|xss_clean');
		$this->form_validation->set_rules('salario','salario','required|trim|min_length[1]|xss_clean');
		$this->form_validation->set_rules('inicio_labores','inicio de labores','required|trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
		$this->form_validation->set_rules('tiempo_laborando','tiempo laborando','required|trim|min_length[4]|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$data = array(
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'cedula' => $this->input->post('cedula'),
				'telefono' => $this->input->post('telefono'),
				'correo' => $this->input->post('correo'),
				'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
				'descuento' => $this->input->post('descuento'),
				'cantidad_descuento' => $this->input->post('cantidad_descuento'),
				'salario' => $this->input->post('salario'),
				'tiempo_laborando' => $this->input->post('tiempo_laborando'),
				'usuarios_id' => $this->session->userdata('id_user'),
				'fecha_creacion' =>  date('Y-m-d H:i:s')
			);

			if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
				$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
			}

			$insert = $this->clientes_model->save($data);
			if ($insert > 0) {
				$data1 = array(
					'clientes_id' => $insert,
					'fecha_creacion' =>  date('Y-m-d H:i:s'),
					'agente' => $this->session->nombres . ' ' . $this->session->apellidos,
					'estados_id' => 1
				);
				$insert1 = $this->solicitudes_model->save($data1);
				echo json_encode(array("status" => TRUE));
			}			
		}
	}

	public function ajax_update() {
		$this->form_validation->set_rules('nombre','nombre','required|trim|min_length[3]|max_length[255]|xss_clean');
		$this->form_validation->set_rules('apellido','apellido','required|trim|min_length[3]|max_length[255]|xss_clean');		
		$this->form_validation->set_rules('telefono','teléfono','required|trim|min_length[4]|xss_clean');
		$this->form_validation->set_rules('correo','correo electrónico','required|valid_email|trim|xss_clean');
		$this->form_validation->set_rules('cantidad_solicitada','cantidad solicitada','required|trim|min_length[3]|xss_clean');
		$this->form_validation->set_rules('descuento','posee descuento','required|trim|min_length[2]|xss_clean');
		$this->form_validation->set_rules('cantidad_descuento','cantidad de descuento','trim|xss_clean');
		$this->form_validation->set_rules('salario','salario','required|trim|min_length[1]|xss_clean');
		$this->form_validation->set_rules('inicio_labores','inicio de labores','required|trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
		$this->form_validation->set_rules('tiempo_laborando','tiempo laborando','required|trim|min_length[4]|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			if ($this->input->post('solicitud_id') == NULL || $this->input->post('solicitud_id') == "") {
				$solicitud = $this->solicitudes_model->get_solicitud_by_cliente($this->input->post('id'));

				if ($solicitud) {

					$estado = '';
					$array = array();
					foreach ($solicitud as $row) {
						$estado .= $this->estados_model->get_by_id($row->estados_id)->estado . ". ";
						$array[] = $row->estados_id;
					}

					$variable = 'solicitud';
					if (count($solicitud) > 1) {
						$variable = 'solicitudes';
					}

					if (in_array(7, $array) && count($solicitud) < 2) {

						$entregado = $this->solicitudes_model->get_solicitud_by_cliente_and_entregados($this->input->post('id'));
						$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($entregado->id);
						$aprobacion = $this->aprobacion_model->get_by_id($solicitudes_aprobacion->aprobacion_id);
						$mitadmeses = round($aprobacion->cantidad_meses / 2);
						$cantcobros = $this->cobros_model->cant_cobros_by_aprobacion_id($aprobacion->id);

						if ($cantcobros >= $mitadmeses) {
							$botonsi = '<button type="button" class="btn-success btn-xs" onclick="add_sol_ref()"><i class="fa fa-save"></i> Si</button>';
					    	echo json_encode(array("rol" => "Cliente ya posee " . count($solicitud) . " " . $variable . " en estado " . $estado . "Desea refinanciamiento?", "botonsi" =>  $botonsi));
						} else {
					   		echo json_encode(array("rol" => "Cliente ya posee " . count($solicitud) . " " . $variable . " en estado " . $estado . "Aún no aplica para refinanciamiento."));
						}

					} elseif (in_array(8, $array) || in_array(9, $array) || in_array(10, $array)) {
						$data = array(
							'nombre' => $this->input->post('nombre'),
							'apellido' => $this->input->post('apellido'),
							'telefono' => $this->input->post('telefono'),
							'correo' => $this->input->post('correo'),
							'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
							'descuento' => $this->input->post('descuento'),
							'cantidad_descuento' => $this->input->post('cantidad_descuento'),
							'salario' => $this->input->post('salario'),
							'tiempo_laborando' => $this->input->post('tiempo_laborando'),
						);

						if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
							$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
						}

						$this->clientes_model->update(array('id' => $this->input->post('id')), $data);

						$solicitud1 = $this->solicitudes_model->get_solicitud_by_clientes_id($this->input->post('id'));
						$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud1->id);
						if ($solicitudes_aprobacion) {
							$this->solicitudes_aprobacion_model->delete_by_solicitudes_id($solicitudes_aprobacion->solicitudes_id);
						}
						$data1 = array(
							'fecha_creacion' =>  date('Y-m-d H:i:s'),
							'agente' => $this->session->nombres . ' ' . $this->session->apellidos,
							'estados_id' => 1,
							'usuarios_id' => NULL,
						);
						$this->solicitudes_model->update(array('id' => $solicitud1->id), $data1);
						echo json_encode(array("status" => TRUE));
					} else {
						echo json_encode(array("rol" => "Cliente ya posee " . count($solicitud) . " " . $variable . " en estado " . $estado));
					}

					
				} else {

					$data = array(
						'nombre' => $this->input->post('nombre'),
						'apellido' => $this->input->post('apellido'),
						'telefono' => $this->input->post('telefono'),
						'correo' => $this->input->post('correo'),
						'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
						'descuento' => $this->input->post('descuento'),
						'cantidad_descuento' => $this->input->post('cantidad_descuento'),
						'salario' => $this->input->post('salario'),
						'tiempo_laborando' => $this->input->post('tiempo_laborando'),
					);

					if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
						$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
					}

					$this->clientes_model->update(array('id' => $this->input->post('id')), $data);

					$data1 = array(
						'clientes_id' => $this->input->post('id'),
						'fecha_creacion' =>  date('Y-m-d H:i:s'),
						'agente' => $this->session->nombres . ' ' . $this->session->apellidos,
						'estados_id' => 1
					);
					$insert1 = $this->solicitudes_model->save($data1);
					echo json_encode(array("status" => TRUE));
				}
			} else {
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'telefono' => $this->input->post('telefono'),
					'correo' => $this->input->post('correo'),
					'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
					'descuento' => $this->input->post('descuento'),
					'cantidad_descuento' => $this->input->post('cantidad_descuento'),
					'salario' => $this->input->post('salario'),
					'inicio_labores' => $this->input->post('inicio_labores'),
				);

				if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
					$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
				}

				$this->clientes_model->update(array('id' => $this->input->post('id')), $data);
				echo json_encode(array("status" => TRUE));
			}
		}
	}



	public function ajax_refinanciamiento() {
		$this->form_validation->set_rules('nombre','nombre','required|trim|min_length[3]|max_length[255]|xss_clean');
		$this->form_validation->set_rules('apellido','apellido','required|trim|min_length[3]|max_length[255]|xss_clean');		
		$this->form_validation->set_rules('telefono','teléfono','required|trim|min_length[4]|xss_clean');
		$this->form_validation->set_rules('correo','correo electrónico','required|valid_email|trim|xss_clean');
		$this->form_validation->set_rules('cantidad_solicitada','cantidad solicitada','required|trim|min_length[3]|xss_clean');
		$this->form_validation->set_rules('descuento','posee descuento','required|trim|min_length[2]|xss_clean');
		$this->form_validation->set_rules('cantidad_descuento','cantidad de descuento','trim|xss_clean');
		$this->form_validation->set_rules('salario','salario','required|trim|min_length[1]|xss_clean');
		$this->form_validation->set_rules('inicio_labores','inicio de labores','required|trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
		$this->form_validation->set_rules('tiempo_laborando','tiempo laborando','required|trim|min_length[4]|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {

			$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($this->input->post('id'));
			$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);
			if ($solicitudes_aprobacion) {
				$this->solicitudes_aprobacion_model->delete_by_solicitudes_id($solicitudes_aprobacion->solicitudes_id);
			}

			$data = array(
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'telefono' => $this->input->post('telefono'),
				'correo' => $this->input->post('correo'),
				'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
				'descuento' => $this->input->post('descuento'),
				'cantidad_descuento' => $this->input->post('cantidad_descuento'),
				'salario' => $this->input->post('salario'),
				'tiempo_laborando' => $this->input->post('tiempo_laborando'),
			);

			if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
				$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
			}

			$this->clientes_model->update(array('id' => $this->input->post('id')), $data);

			$data1 = array(
				'fecha_creacion' =>  date('Y-m-d H:i:s'),
				'agente' => $this->session->nombres . ' ' . $this->session->apellidos,
				'estados_id' => 1,
				'usuarios_id' => NULL,
			);
			$this->solicitudes_model->update(array('id' => $solicitud->id), $data1);
			echo json_encode(array("status" => TRUE));
		}
	}




	public function ajax_delete($id) {
		//un usuario debe elejir la solicitud para borrarla.
		if($this->admin_model->get_eliminar($this->roluser) == 1) {
			$this->solicitudes_model->update(array('id' => $id), array('usuarios_id' => $this->session->userdata('id_user')));
			$this->solicitudes_model->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para borrar.'));
		}

	}

	public function add_compania() {
		if ($this->admin_model->get_insertar($this->roluser) == 1) {
			$this->form_validation->set_rules('nombre','nombre de la compañia','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('telefono','teléfono','required|trim|xss_clean');

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'direccion' => $this->input->post('direccion'),
					'telefono' => $this->input->post('telefono'),
					'ruc' => $this->input->post('ruc'),
					'sitio_web' => $this->input->post('sitio_web'),
					'ano_constitucion' => $this->input->post('ano_constitucion'),
					'correo' => $this->input->post('correo'),
					'nombre_rrhh' => $this->input->post('nombre_rrhh'),
					'telefono_rrhh' => $this->input->post('telefono_rrhh'),
					'nombre_contabilidad' => $this->input->post('nombre_contabilidad'),
					'telefono_contabilidad' => $this->input->post('telefono_contabilidad'),
					'forma_pago' => $this->input->post('forma_pago'),
					'cantidad_empleados' => $this->input->post('cantidad_empleados'),
					'rubro' => $this->input->post('rubro'),
					'usuarios_id' => $this->session->userdata('id_user'),
				);
				$insert = $this->companias_model->save($data);
				echo json_encode(array("status" => TRUE));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para añadir compañias.'));
		}
	}

	public function completar_cobro($id){
		$cliente = $this->clientes_model->get_by_id($id);
		$data = array(
			'cantidad_solicitada' => 0,
		);
		$this->clientes_model->update(array('id' => $cliente->id), $data);
		$request = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$data1 = array(
			//'usuarios_id' => $this->session->userdata('id_user'),
			'usuarios_id' => NULL
		);
		$update = $this->solicitudes_model->update(array('id' => $request->id), $data1);
		echo json_encode(array("status" => TRUE));
	}

	public function activar($id){
		$data = array(
			'usuarios_id' => $this->session->userdata('id_user'),
			'estados_id' => 1
		);
		$this->solicitudes_model->update(array('id' => $id), $data);
		$this->solicitudes_model->update(array('id' => $id), array('usuarios_id' => NULL));
		echo json_encode(array("status" => TRUE));
	}

	public function tipos_reportes($tipo = NULL){
		$data = $this->tipos_reportes_model->get_all();
		$array = array();
		if ($tipo == 'verificado') {
			foreach ($data as $row) {
				if ($row->id == 1 || $row->id == 12) {
					$array[] = $row;
				}
			}
			$data = $array;
		} else if ($tipo == 'aprobado') {
			foreach ($data as $row) {
				if ($row->id == 1 || $row->id == 2 || $row->id == 3 || $row->id == 4 || $row->id == 5 || $row->id == 6 || $row->id == 7 || $row->id == 8 || $row->id == 9 || $row->id == 10 || $row->id == 11 || $row->id == 12) {
					$array[] = $row;
				}
			}
			$data = $array;
		}
		echo json_encode($data);
	}

	public function paquetes_reportes($tipo = NULL){
		$data = $this->paquetes_reportes_model->get_all();
		echo json_encode($data);
	}


	public function estados_for_select(){
		$data = $this->estados_model->get();
		echo json_encode($data);
	}

	public function disposiciones_for_select(){
		$data = $this->disposiciones_model->get_all();
		echo json_encode($data);
	}

	public function seg_soc(){

		ini_set('memory_limit', '1024M');

    	$cedula = $this->input->post('cedula');
    	$db_access_id = $this->input->post('db_access_id');

		$tipo = $this->myGetType($cedula);
		$cedu = null;
		if ($tipo == "string") {
			$partes = explode('-',$cedula);
			$partes[1] = substr($partes[1],1);
			for ($i=0; $i < count($partes) ; $i++) {
				$cedu .= $partes[$i];
				if ($i == 0) {
					$cedu .= "  ";
				}
			}
		}
		
		$cedu = substr($cedu,1);
    	$tabla = $this->name_db_access($db_access_id);

		$valor = $this->datos_model->get_by_cedula($tabla,$cedula);

		$num = 0;
		if ($valor) {
		    if (count($valor)>0) {
		    	$num = $this->datos_model->numempleados($tabla,$valor[0]['razon_so']);
		    }

		    $array =  '
	            <div class="table-responsive">
	                <table class="table table-striped table-bordered jambo_table" id="datatable-css" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>SEGURO</th>
	                            <th>CEDULA</th>
	                            <th>NOMBRE</th>
	                            <th>RAZON_SO</th>
	                            <th>PATRONO</th>
	                            <th>TEL1</th>
	                            <th>TEL2</th>
	                            <th>FECHA</th>
	                            <th>SALARIO</th>
	                            <th>EMPLEADOS</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							<td> '. $valor[0]->seguro .' </td>
							<td> '. $valor[0]->cedula .' </td>
							<td> '. $valor[0]->nombre .' </td>
							<td> '. $valor[0]->razon_so .' </td>
							<td> '. $valor[0]->patrono .' </td>
							<td> '. $valor[0]->tel1 .' </td>
							<td> '. $valor[0]->tel2 .' </td>
							<td> '. $valor[0]->fecha .' </td>
							<td> '. number_format((int) $valor[0]->salario / 100,2) .' </td>
							<td> '. $num .' </td>
	                    </tbody>
	                </table>
	            </div>

	        ';		

		} else {

			$array = '

				<h2>Datos de CSS</h2>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
					<p class="label label-danger alerta">Datos de CSS No encontrados, Intente con los ultimos 8 valores.</p>
					<span class="label label-danger aviso"></span>
					<div id="resultado"></div>
				</div>

				<form class="form-horizontal form-label-left" id="form_search">
					<input type="hidden" name="db_access" id="db_access" value="'.$db_access_id.'">
					<div class="form-group">
						<label class="col-sm-1 control-label">Cédula</label>
						<div class="col-sm-4">
							<div class="input-group">
								<input type="text" class="form-control" name="buscar_cedula" id="buscar_cedula" onkeyup="searchCedula()" maxlength="8">
								<span class="input-group-btn">
								<button type="button" class="btn btn-primary" onclick="searchCedula()"><i class="fa fa-search"></i> Buscar</button>
								</span>
							</div>
						</div>
					</div>
				</form>

			';

		}

		echo json_encode($array);

	}

    public function name_db_access($id){
    	$access = $this->db_access_model->get_by_id($id);
    	return $access->nombre;
    }

    function myGetType($var)
    {
        if (is_array($var)) return "array";
        if (is_bool($var)) return "boolean";
        if (is_float($var)) return "float";
        if (is_int($var)) return "integer";
        if (is_null($var)) return "NULL";
        if (is_numeric($var)) return "numeric";
        if (is_object($var)) return "object";
        if (is_resource($var)) return "resource";
        if (is_string($var)) return "string";
        return "unknown type";
    }



	public function search_cedula_seg(){

		$cedula = $this->input->post('buscar_cedula');
		$tabla = $this->name_db_access($this->input->post('db_access'));
		$valor = $this->datos_model->get_like_cedula($tabla,$cedula);

		$array = array();
		if ($valor) {
			foreach ($valor as $row) {
				$array[] = array(
					'seguro' => $row->seguro,
					'cedula' => $row->cedula,
					'nombre' => $row->nombre,
					'razon_so' => $row->razon_so,
					'patrono' => $row->patrono,
					'tel1' => $row->tel1,
					'tel2' => $row->tel2,
					'fecha' => $row->fecha,
					'salario' => number_format((int) $row->salario / 100,2),
				);
			}
		}

		$html = '<div class="col-lg-12 col-sm-12 col-12 table-responsive">
	                <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>SEGURO</th>
	                            <th>CEDULA</th>
	                            <th>NOMBRE</th>
	                            <th>RAZON_SO</th>
	                            <th>PATRONO</th>
	                            <th>TEL1</th>
	                            <th>TEL2</th>
	                            <th>FECHA</th>
	                            <th>SALARIO</th>
	                        </tr>
	                    </thead>
	                    <tbody>';
		for ($i=0; $i < count($array); $i++) { 
			$html .= '<tr>
				<td> '. $array[$i]['seguro'] .' </td>
				<td> '. $array[$i]['cedula'] .' </td>
				<td> '. $array[$i]['nombre'] .' </td>
				<td> '. $array[$i]['razon_so'] .' </td>
				<td> '. $array[$i]['patrono'] .' </td>
				<td> '. $array[$i]['tel1'] .' </td>
				<td> '. $array[$i]['tel2'] .' </td>
				<td> '. $array[$i]['fecha'] .' </td>
				<td> '. $array[$i]['salario'] .' </td>
			</tr>';
		}
		$html .= '</tbody>
	                </table>
	            </div>';

		$respuesta = null;
		if (count($array) == 0) {
			$respuesta = '<p class="label label-danger alerta">No hay registro de ese numero.</p>';
		} else {
			$respuesta = $html;
		}

		echo json_encode(array("datos" => $respuesta));
	}

	public function search_by_name(){

		$nombre = $this->input->post('nombre');
		$nombre = strval($nombre);
		$tabla = $this->name_db_access($this->input->post('db_access'));

		$valor = $this->datos_model->get_by_nombre($tabla,$nombre);

		$array = array();
		if ($valor) {
			foreach ($valor as $row) {
				$array[] = array(
					'seguro' => $row->seguro,
					'cedula' => $row->cedula,
					'nombre' => $row->nombre,
					'razon_so' => $row->razon_so,
					'patrono' => $row->patrono,
					'tel1' => $row->tel1,
					'tel2' => $row->tel2,
					'fecha' => $row->fecha,
					'salario' => number_format((int) $row->salario / 100,2),
				);
			}
		}

		$html = '<div class="col-lg-12 col-sm-12 col-12 table-responsive">
	                <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>SEGURO</th>
	                            <th>CEDULA</th>
	                            <th>NOMBRE</th>
	                            <th>RAZON_SO</th>
	                            <th>PATRONO</th>
	                            <th>TEL1</th>
	                            <th>TEL2</th>
	                            <th>FECHA</th>
	                            <th>SALARIO</th>
	                        </tr>
	                    </thead>
	                    <tbody>';
		for ($i=0; $i < count($array); $i++) { 
			$html .= '<tr>
				<td> '. $array[$i]['seguro'] .' </td>
				<td> '. $array[$i]['cedula'] .' </td>
				<td> '. $array[$i]['nombre'] .' </td>
				<td> '. $array[$i]['razon_so'] .' </td>
				<td> '. $array[$i]['patrono'] .' </td>
				<td> '. $array[$i]['tel1'] .' </td>
				<td> '. $array[$i]['tel2'] .' </td>
				<td> '. $array[$i]['fecha'] .' </td>
				<td> '. $array[$i]['salario'] .' </td>
			</tr>';
		}
		$html .= '</tbody>
	                </table>
	            </div>';

		$respuesta = null;
		if (count($array) == 0) {
			$respuesta = '<p class="label label-danger alerta">No hay registro con ese nombre.</p>';
		} else {
			$respuesta = $html;
		}

		echo json_encode(array("datos" => $respuesta));
	}

	public function notas_cliente($id){
    	$cliente = $this->clientes_model->get_by_id($id);
		$nombre_cliente = $cliente->nombre . " " . $cliente->apellido;
		$data = $this->notas_clientes_model->get_by_clientes_id($cliente->id);
		if ($data) {
			foreach ($data as $row) {

				$user_creator = $this->usuarios_model->get_by_id($row->usuarios_id);
				$cadena = $user_creator->nombres;
				$nombre = explode(' ',$cadena);
				$cadena1 = $user_creator->apellidos;
				$apellido = explode(' ',$cadena1);
				$row->usuarios_id = $nombre[0] . " " . $apellido[0];
			}
		}
		echo json_encode(array('nota' => $data, 'nombre_cliente' => $nombre_cliente));
	}

	public function add_notes(){
		$this->form_validation->set_rules('nota','nota','required|trim|xss_clean');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {

			$data = array(
				'fecha_creacion' =>  date('Y-m-d H:i:s'),
				'clientes_id' => $this->input->post('clientes_id'),
				'usuarios_id' => $this->session->userdata('id_user'),
				'nota' => $this->input->post('nota'),
			);
			$insert = $this->notas_clientes_model->save($data);
			echo json_encode(array("status" => TRUE));
			
		}
	}

	public function add_disposicion(){
		$this->form_validation->set_rules('disposiciones_id','disposición','required|trim|xss_clean|callback_check_default');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {

			$solicitud = $this->solicitudes_model->get_by_id($this->input->post('solicitud_id'));
			if ($this->session->userdata('id_user') == $solicitud->usuarios_id) {
				$data = array(
					'disposiciones_id' => $this->input->post('disposiciones_id'),
				);
				$this->solicitudes_model->update(array('id' => $this->input->post('solicitud_id')), $data);
				echo json_encode(array('status' => TRUE));
			} else {
				echo json_encode(array('permission' => '<span class="label label-danger animated shake">No puedes cambiar disposición si no lo tienes elegido.</span>'));
			}
		}
	}

	public function view_notas(){
		$data = $this->notas_model->get_all();
		echo json_encode($data);
	}

	public function view_disposiciones($id){
		$solicitud = $this->solicitudes_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($solicitud->clientes_id);
    	$cliente_nombre = $cliente->nombre . " " . $cliente->apellido;
		$data = $this->disposiciones_model->get_all();
		echo json_encode(array('data' => $data, 'nombre_cliente' => $cliente_nombre));
	}

}