<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Productos extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('productos_model','admin_model','usuarios_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');

		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}

	}

	public function index()	{		
		$insertar = false;	
		if ($this->roluser == 1 || $this->roluser == 2) { $insertar = true; }
		$data['insertar'] = $insertar;
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);		
		$this->load->view('templates/header',$data);
		$this->load->view('admin/productos/gestionar_productos',$data);
		$this->load->view('templates/footer',$data);
	}

	public function ajax_list()	{

		$list = $this->productos_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $products) {

			$no++;
			$row = array();
			$row[] = $products->nombre;
			$row[] = $products->suma_minima . " - " . $products->suma_maxima;
			$row[] = $products->plazo_minimo . " - " . $products->plazo_maximo;
			$row[] = $products->tasa_interes_mensual;
			$row[] = $products->tasa_interes_vencido;
			$row[] = $products->dias_ultimo_plazo_grabacion;
			$row[] = $products->tasa_interes_efectiva;

			//add html for action
			if ($this->roluser == 1 || $this->roluser == 2) {
				if($this->admin_model->get_modificar($this->roluser) == 1 && $this->admin_model->get_eliminar($this->roluser) == 1) {
					$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" title="View" onclick="view_products('."'".$products->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" title="Edit" onclick="edit_products('."'".$products->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
					  <a class="btn btn-danger btn-xs" href="javascript:void(0)" title="Hapus" onclick="confirm_delete('."'".$products->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
				} elseif ($this->admin_model->get_modificar($this->roluser) == 1) {
					$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" title="View" onclick="view_products('."'".$products->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" title="Edit" onclick="edit_products('."'".$products->id."'".')"><i class="fa fa-pencil"></i> Editar</a>';
				} elseif ($this->admin_model->get_eliminar($this->roluser) == 1) {
					$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" title="View" onclick="view_products('."'".$products->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					  <a class="btn btn-danger btn-xs" href="javascript:void(0)" title="Hapus" onclick="confirm_delete('."'".$products->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
				} else {
					$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" title="View" onclick="view_products('."'".$products->id."'".')"><i class="fa fa-folder"></i> Ver </a>';
				}
			} else {
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" title="View" onclick="view_products('."'".$products->id."'".')"><i class="fa fa-folder"></i> Ver </a>';
			}
		
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->productos_model->count_all(),
			"recordsFiltered" => $this->productos_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_add() {
		if(($this->roluser == 1 || $this->roluser == 2) && $this->admin_model->get_insertar($this->roluser) == 1) {
			$this->form_validation->set_rules('nombre','nombre del producto','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('suma_minima','suma mínima','required|trim|xss_clean');
			$this->form_validation->set_rules('suma_maxima','suma máxima','required|trim|xss_clean');
			$this->form_validation->set_rules('plazo_minimo','plazo mínimo','required|trim|xss_clean');
			$this->form_validation->set_rules('plazo_maximo','plazo máximo','required|trim|xss_clean');
			$this->form_validation->set_rules('cargo_administrativo','cargo administrativo','required|trim|xss_clean');
			$this->form_validation->set_rules('tasa_interes_mensual','tasa de interés mensual','required|trim|xss_clean');
			$this->form_validation->set_rules('dias_ultimo_plazo_grabacion','dias de ultimo plazo de grabación','required|trim|xss_clean');
			$this->form_validation->set_rules('tasa_interes_vencido','tasa de intereses vencidos','required|trim|xss_clean');
			$this->form_validation->set_rules('tasa_interes_efectiva','tasa de interes efectiva','required|trim|xss_clean');
			$this->form_validation->set_rules('visible_cliente','visible al cliente','xss_clean');
			
			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$visiblecliente = null;
				if ($this->input->post('visible_cliente') == "" || $this->input->post('visible_cliente') == null){
					$visiblecliente = "No";
				} else {
					$visiblecliente = $this->input->post('visible_cliente');
				}
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'suma_minima' => $this->input->post('suma_minima'),
					'suma_maxima' => $this->input->post('suma_maxima'),
					'plazo_minimo' => $this->input->post('plazo_minimo'),
					'plazo_maximo' => $this->input->post('plazo_maximo'),
					'cargo_administrativo' => $this->input->post('cargo_administrativo'),
					'tasa_interes_mensual' => $this->input->post('tasa_interes_mensual'),
					'dias_ultimo_plazo_grabacion' => $this->input->post('dias_ultimo_plazo_grabacion'),
					'tasa_interes_vencido' => $this->input->post('tasa_interes_vencido'),
					'tasa_interes_efectiva' => $this->input->post('tasa_interes_efectiva'),
					'visible_cliente' => $visiblecliente,
					'usuarios_id' => $this->session->userdata('id_user'),
					'fecha_creacion' =>  date('Y-m-d H:i:s')
				);
				$insert = $this->productos_model->save($data);
				echo json_encode(array("status" => TRUE));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para añadir productos.'));
		}
	}

	public function ajax_view($id) {
		$data = $this->productos_model->get_by_id($id);
		$user_creator = $this->usuarios_model->get_by_id($data->usuarios_id);
		$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
		$cadena = $user_creator->nombres;
		$nombre = explode(' ',$cadena);
		$cadena1 = $user_creator->apellidos;
		$apellido = explode(' ',$cadena1);
		$data->usuarios_id = $nombre[0] . " " . $apellido[0] . ", <b>" . $rol_creator->descripcion . ".</b>";
		echo json_encode($data);
	}

	public function ajax_edit($id) {
		if(($this->roluser == 1 || $this->roluser == 2) && $this->admin_model->get_modificar($this->roluser) == 1) {
			$data = $this->productos_model->get_by_id($id);
			echo json_encode($data);
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar productos.'));
		}
	}

	public function ajax_update() {
		if(($this->roluser == 1 || $this->roluser == 2) && $this->admin_model->get_modificar($this->roluser) == 1) {
			$this->form_validation->set_rules('nombre','nombre del producto','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('suma_minima','suma mínima','required|trim|xss_clean');
			$this->form_validation->set_rules('suma_maxima','suma máxima','required|trim|xss_clean');
			$this->form_validation->set_rules('plazo_minimo','plazo mínimo','required|trim|xss_clean');
			$this->form_validation->set_rules('plazo_maximo','plazo máximo','required|trim|xss_clean');
			$this->form_validation->set_rules('cargo_administrativo','cargo administrativo','required|trim|xss_clean');
			$this->form_validation->set_rules('gastos_notariales','gastos notariales y legales','required|trim|xss_clean');
			$this->form_validation->set_rules('comision','comisión','required|trim|xss_clean');
			$this->form_validation->set_rules('tasa_interes_mensual','tasa de interés mensual','required|trim|xss_clean');
			$this->form_validation->set_rules('dias_ultimo_plazo_grabacion','dias de ultimo plazo de grabación','required|trim|xss_clean');
			$this->form_validation->set_rules('tasa_interes_vencido','tasa de intereses vencidos','required|trim|xss_clean');
			$this->form_validation->set_rules('tasa_interes_efectiva','tasa de interes efectiva','required|trim|xss_clean');
			$this->form_validation->set_rules('visible_cliente','visible al cliente','xss_clean');
			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$visiblecliente = null;
				if ($this->input->post('visible_cliente') == "" || $this->input->post('visible_cliente') == null){
					$visiblecliente = "No";
				} else {
					$visiblecliente = $this->input->post('visible_cliente');
				}
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'suma_minima' => $this->input->post('suma_minima'),
					'suma_maxima' => $this->input->post('suma_maxima'),
					'plazo_minimo' => $this->input->post('plazo_minimo'),
					'plazo_maximo' => $this->input->post('plazo_maximo'),
					'gastos_notariales' => $this->input->post('gastos_notariales'),
					'cargo_administrativo' => $this->input->post('cargo_administrativo'),
					'comision' => $this->input->post('comision'),
					'tasa_interes_mensual' => $this->input->post('tasa_interes_mensual'),
					'dias_ultimo_plazo_grabacion' => $this->input->post('dias_ultimo_plazo_grabacion'),
					'tasa_interes_vencido' => $this->input->post('tasa_interes_vencido'),
					'tasa_interes_efectiva' => $this->input->post('tasa_interes_efectiva'),
					'visible_cliente' => $visiblecliente,
				);
				$this->productos_model->update(array('id' => $this->input->post('id')), $data);
				echo json_encode(array("status" => TRUE));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar productos.'));
		}
	}

	public function ajax_delete($id) {
		if ($this->roluser == 1 && $this->admin_model->get_eliminar($this->roluser) == 1) {
			$this->productos_model->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para eliminar.'));
		}
	}


}