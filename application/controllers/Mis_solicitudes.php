<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Mis_solicitudes extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('solicitudes_model','admin_model','clientes_model','usuarios_model','companias_model','productos_model','aprobacion_model','fechas_firmas_model','db_access_model','solicitudes_view_model','datos_verificacion_model','cotizacion_model','aprobacion_view_model','solicitudes_aprobacion_model','cobros_model','disposiciones_model','datos_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('download', 'file', 'url', 'html', 'form'));
		$this->load->database('default');


		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}
	}
	
	public function index()	{
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['representante'] = $this->session->userdata('nombres') . " " . $this->session->userdata('apellidos');
		$this->load->view('templates/header',$data);
		$this->load->view('admin/solicitudes/gestionar_solicitudes',$data);
		$this->load->view('templates/footer',$data);		
	}

	public function my_list($estado,$disposicion = NULL)	{

		if ($disposicion > 0) {
			$disposicion = $this->disposiciones_model->get_by_id($disposicion);
			$disposicion = $disposicion->nombre;
		} else {
			$disposicion = NULL;
		}

		$list = $this->solicitudes_view_model->get_by_usuarios_id_and_estado($this->session->userdata('id_user'), $estado, $disposicion);
		$data = array();
		foreach ($list as $fila) {
			$row = array();
			$row[] = $fila->fecha_registro;
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = '$'.$fila->salario;
			$row[] = $fila->empresa;
			$row[] = '$'.$fila->cantidad_solicitada;
			if ($fila->estado == "Prospecto") {
				$row[] = "<span class='label label-default'>". $fila->estado . "</span>";
				$row[] = $fila->disposicion;
				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="confirm_deselect_request('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deseleccionar </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="prospectos('."'".$fila->id."'".')"><i class="fa fa-pencil"></i> Editar</a>';
			} else if ($fila->estado == "Por Verificar") {
				$row[] = "<span class='label label-warning'>". $fila->estado . "</span>";
				$row[] = $fila->disposicion;
				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="confirm_deselect_request('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deseleccionar </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="verificacion('."'".$fila->id."'".')"><i class="fa fa-check"></i> Verificar </a>';
			} else if ($fila->estado == "Por Aprobar") {
				$row[] = "<span class='label label-info'>". $fila->estado . "</span>";
				$row[] = $fila->disposicion;
				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="confirm_deselect_request('."'".$fila->id."'".')"><i class="fa fa-hand-paper-o"></i> Deseleccionar </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="aprobacion('."'".$fila->id."'".')"><i class="fa fa-thumbs-o-up"></i> Aprobar </a>';
			}

			$data[] = $row;
		}
		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function edit_client($id){
		if ($this->admin_model->get_modificar($this->roluser) == 1) {
			$solicitud = $this->solicitudes_model->get_by_id($id);
			$data = $this->clientes_model->get_by_id($solicitud->clientes_id);
			if ($data->fecha_nacimiento != NULL) {
				$data->fecha_nacimiento = date('d/m/Y', strtotime(str_replace("/","-",$data->fecha_nacimiento)));
			}
			if ($data->inicio_labores != NULL) {
				$data->inicio_labores = date('d/m/Y', strtotime(str_replace("/","-",$data->inicio_labores)));
			}
			$data->solicitud_id = $solicitud->id;
			echo json_encode($data);
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar clientes.'));
		}
	}

	public function list_aprobados($estado,$disposicion = NULL)	{

		if ($disposicion > 0) {
			$disposicion = $this->disposiciones_model->get_by_id($disposicion);
			$disposicion = $disposicion->nombre;
		} else {
			$disposicion = NULL;
		}

		$list = $this->aprobacion_view_model->get_by_usuarios_id_and_estado($this->session->userdata('id_user'), $estado, $disposicion);
		$data = array();
		foreach ($list as $fila) {
			$row = array();
			$row[] = $fila->fecha_registro;
			$row[] = $fila->cliente;
			$row[] = $fila->cedula;
			$row[] = '$'.$fila->salario;
			$row[] = $fila->empresa;
			$row[] = '$'.$fila->cantidad_aprobada;

			if ($fila->estado == "Por Firmar") {
				$fechas = $this->fechas_firmas_model->get_by_aprobacion_id($fila->id);
				if ($fechas) {
					$firma = " <span class='label label-danger'>Autorización</span>";
					if ($fechas->fecha_autorizacion != "" || $fechas->fecha_autorizacion != NULL) {
						$firma = " <span class='label label-default'>Salida</span>";
					}
					if ($fechas->fecha_salida != "" || $fechas->fecha_salida != NULL) {
						$firma = " <span class='label label-warning'>Empresa</span>";
					}
					if ($fechas->fecha_recibida_empresa != "" || $fechas->fecha_recibida_empresa != NULL) {
						$firma = " <span class='label label-info'>Vuelta</span>";
					}
					if ($fechas->fecha_recibida_vuelta != "" || $fechas->fecha_recibida_vuelta != NULL) {
						$firma = " <span class='label label-success'><i class='fa fa-check'></i></span>";
					}
					$row[] = "<span class='label label-primary'>". $fila->estado . "</span>" . $firma;
				} else {
					$row[] = "<span class='label label-primary'>". $fila->estado . "</span>" . " <span class='label label-danger'>Autorización</span>";
				}

				$row[] = $fila->disposicion;
				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->solicitud_id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="confirm_deselect_request('."'".$fila->solicitud_id."'".')"><i class="fa fa-hand-paper-o"></i> Deseleccionar </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->solicitud_id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="procesar_firma('."'".$fila->solicitud_id."'".')"><i class="fa fa-check"></i> Verificar </a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';
			} elseif ($fila->estado == "Por Entregar") {

				$row[] = "<span class='label label-primary'>". $fila->estado . "</span>";
				$row[] = $fila->disposicion;
				$row[] = '<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="disposiciones('."'".$fila->solicitud_id."'".')"><i class="fa fa-bookmark"></i> Disposiciones</a>
						<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="confirm_deselect_request('."'".$fila->solicitud_id."'".')"><i class="fa fa-hand-paper-o"></i> Deseleccionar </a>
					<a class="btn btn-warning btn-xs" href="javascript:void(0)" onclick="notas_clientes('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Notas</a>
					<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_request('."'".$fila->solicitud_id."'".')"><i class="fa fa-folder"></i> Ver </a>
					<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="entrega('."'".$fila->solicitud_id."'".')"><i class="fa fa-money"></i> Entrega </a>
					<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="file_client('."'".$fila->cliente_id."'".')"><i class="fa fa-file"></i> Documentos </a>
					<a class="btn btn-default btn-xs" href="javascript:void(0)" onclick="generate_pdf('."'".$fila->cliente_id."'".')"><i class="fa fa-file-pdf-o"></i> Generar PDF</a>';
			}

			$data[] = $row;
		}

		$output = array("data" => $data);
		echo json_encode($output);
	}

	public function prospectos(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['representante'] = $this->session->userdata('nombres') . " " . $this->session->userdata('apellidos');
		$data['result_tipo_archivos'] = $this->clientes_model->get_tipos_archivos();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/solicitudes/prospectos',$data);
		$this->load->view('templates/footer',$data);
	}

	public function verificacion(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['representante'] = $this->session->userdata('nombres') . " " . $this->session->userdata('apellidos');
		$data['result_tipo_archivos'] = $this->clientes_model->get_tipos_archivos();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/solicitudes/verificacion',$data);
		//$this->load->view('templates/footer',$data);
	}

	public function aprobacion(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['representante'] = $this->session->userdata('nombres') . " " . $this->session->userdata('apellidos');
		$data['result_tipo_archivos'] = $this->clientes_model->get_tipos_archivos();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/solicitudes/aprobacion',$data);
		$this->load->view('templates/footer',$data);
	}

	public function firma(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$this->load->view('templates/header',$data);
		$this->load->view('admin/solicitudes/firma',$data);
		$this->load->view('templates/footer',$data);
	}

	public function entrega(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['representante'] = $this->session->userdata('nombres') . " " . $this->session->userdata('apellidos');
		$data['result_tipo_archivos'] = $this->clientes_model->get_tipos_archivos();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/solicitudes/entrega',$data);
		$this->load->view('templates/footer',$data);
	}

	function check_default($post_string) {
		return $post_string == '0' ? FALSE : TRUE;
	}

	public function client_save_update() {
		if ($this->admin_model->get_modificar($this->roluser) == 1) {
			$this->form_validation->set_rules('nombre','nombre','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('apellido','apellido','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('genero','genero','callback_check_default');
			$this->form_validation->set_rules('cedula','cédula','trim|min_length[12]|max_length[13]|xss_clean');		
			$this->form_validation->set_rules('telefono','teléfono','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('celular','celular','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('estado_civil','estado civil','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('correo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('fecha_nacimiento','fecha de nacimiento','trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('numero_dependientes','numero de dependientes','trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('cantidad_solicitada','cantidad solicitada','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('fuente_cliente','fuente del cliente','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('como_escucho_nosotros','como escucho de nosotros','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('tipo_vivienda','tipo de vivienda','callback_check_default');
			$this->form_validation->set_rules('hipotecas','posee hipotecas','callback_check_default');
			$this->form_validation->set_rules('banco','banco de la hipoteca','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('descuento','posee descuento','trim|min_length[2]|xss_clean');
			$this->form_validation->set_rules('anho_descuento','años de descuento','trim|xss_clean');
			$this->form_validation->set_rules('mes_descuento','meses de descuento','trim|xss_clean');
			$this->form_validation->set_rules('cantidad_descuento','cantidad de descuento','trim|xss_clean');
			$this->form_validation->set_rules('tipo_descuento','tipo de descuento','trim|xss_clean');
			$this->form_validation->set_rules('capacidad','capacidad','trim|min_length[2]|xss_clean');
			$this->form_validation->set_rules('corregimiento','corregimiento','trim|min_length[4]|xss_clean');		
			$this->form_validation->set_rules('distrito','distrito','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('provincia','provincia','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('direccion','dirección','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('salario','salario','trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('posicion_trabajo','posición de trabajo','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('inicio_labores','inicio de labores','trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('tiempo_laborando','tiempo laborando','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('nombre_jefe_directo','nombre del jefe directo','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('area','área','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('ministerio','ministerio','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('planilla','planilla','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('posicion','posición','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('companias_id','empresa','callback_check_default');

			$company_id = null;
			if ($this->input->post('companias_id') == "") {
				$company_id = null;
			} else {
				$company_id = $this->input->post('companias_id');
			}

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$data = array(
					'nombre' => $this->input->post('nombre'),
					'apellido' => $this->input->post('apellido'),
					'genero' => $this->input->post('genero'),
					'cedula' => $this->input->post('cedula'),
					'telefono' => $this->input->post('telefono'),
					'celular' => $this->input->post('celular'),
					'estado_civil' => $this->input->post('estado_civil'),
					'correo' => $this->input->post('correo'),
					'numero_dependientes' => $this->input->post('numero_dependientes'),
					'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
					'fuente_cliente' => $this->input->post('fuente_cliente'),
					'como_escucho_nosotros' => $this->input->post('como_escucho_nosotros'),
					'tipo_vivienda' => $this->input->post('tipo_vivienda'),
					'hipotecas' => $this->input->post('hipotecas'),
					'banco' => $this->input->post('banco'),
					'descuento' => $this->input->post('descuento'),
					'anho_descuento' => $this->input->post('anho_descuento'),
					'mes_descuento' => $this->input->post('mes_descuento'),
					'cantidad_descuento' => $this->input->post('cantidad_descuento'),
					'tipo_descuento' => $this->input->post('tipo_descuento'),
					'capacidad' => $this->input->post('capacidad'),
					'corregimiento' => $this->input->post('corregimiento'),
					'distrito' => $this->input->post('distrito'),
					'provincia' => $this->input->post('provincia'),
					'direccion' => $this->input->post('direccion'),
					'salario' => $this->input->post('salario'),
					'posicion_trabajo' => $this->input->post('posicion_trabajo'),
					'tiempo_laborando' => $this->input->post('tiempo_laborando'),
					'nombre_jefe_directo' => $this->input->post('nombre_jefe_directo'),
					'area' => $this->input->post('area'),
					'ministerio' => $this->input->post('ministerio'),
					'planilla' => $this->input->post('planilla'),
					'posicion' => $this->input->post('posicion'),
					'nombre_referencia' => $this->input->post('nombre_referencia'),
					'apellido_referencia' => $this->input->post('apellido_referencia'),
					'parentesco_referencia' => $this->input->post('parentesco_referencia'),
					'direccion_referencia' => $this->input->post('direccion_referencia'),
					'telefono_referencia' => $this->input->post('telefono_referencia'),
					'celular_referencia' => $this->input->post('celular_referencia'),
					'fecha_modificacion' => date('Y-m-d,H:i:s'),
					'companias_id' => $company_id,
				);
				if ($this->input->post('fecha_nacimiento') != "" || $this->input->post('fecha_nacimiento') != NULL) {
					$data['fecha_nacimiento'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_nacimiento'))));
				}
				if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
					$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
				}
				$this->clientes_model->update(array('id' => $this->input->post('id')), $data);
				echo json_encode(array("status" => TRUE));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar clientes.'));
		}
	}

	public function client_update() {
		if ($this->admin_model->get_modificar($this->roluser) == 1) {
			$this->form_validation->set_rules('nombre','nombre','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('apellido','apellido','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('genero','genero','callback_check_default');
			$this->form_validation->set_rules('cedula','cédula','required|trim|min_length[12]|max_length[13]|xss_clean');		
			$this->form_validation->set_rules('telefono','teléfono','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('celular','celular','required|trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('estado_civil','estado civil','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('correo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('fecha_nacimiento','fecha de nacimiento','trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('numero_dependientes','numero de dependientes','trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('cantidad_solicitada','cantidad solicitada','required|trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('fuente_cliente','fuente del cliente','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('como_escucho_nosotros','como escucho de nosotros','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('tipo_vivienda','tipo de vivienda','callback_check_default');
			$this->form_validation->set_rules('hipotecas','posee hipotecas','callback_check_default');
			$this->form_validation->set_rules('banco','banco de la hipoteca','trim|min_length[3]|xss_clean');
			$this->form_validation->set_rules('descuento','posee descuento','required|trim|min_length[2]|xss_clean');
			$this->form_validation->set_rules('anho_descuento','años de descuento','trim|xss_clean');
			$this->form_validation->set_rules('mes_descuento','meses de descuento','trim|xss_clean');
			$this->form_validation->set_rules('cantidad_descuento','cantidad de descuento','trim|xss_clean');
			$this->form_validation->set_rules('tipo_descuento','tipo de descuento','trim|xss_clean');
			$this->form_validation->set_rules('capacidad','capacidad','trim|min_length[2]|xss_clean');
			$this->form_validation->set_rules('corregimiento','corregimiento','trim|min_length[4]|xss_clean');		
			$this->form_validation->set_rules('distrito','distrito','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('provincia','provincia','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('direccion','dirección','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('salario','salario','required|trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('posicion_trabajo','posición de trabajo','required|trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('inicio_labores','inicio de labores','required|trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('tiempo_laborando','tiempo laborando','required|trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('nombre_jefe_directo','nombre del jefe directo','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('area','área','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('ministerio','ministerio','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('planilla','planilla','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('posicion','posición','trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('companias_id','empresa','required|callback_check_default');

			if ($this->input->post('companias_id') == "") {
				$company_id = null;
			} else {
				$company_id = $this->input->post('companias_id');
			}

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {

				$archivos = $this->solicitudes_model->get_archivos_by_clientes_id($this->input->post('id'));
				$ar = array();
				if ($archivos) {
					foreach ($archivos as $row) {
						$ar[] = $row->tipos_archivos_id;
					}
				}

				if (count($ar) < 2) {
					echo json_encode(array('permission'=> '<span class="label label-danger animated shake">Faltan archivos, suba al menos dos archivos.</span>'));
				} else {
					$data = array(
						'nombre' => $this->input->post('nombre'),
						'apellido' => $this->input->post('apellido'),
						'genero' => $this->input->post('genero'),
						'cedula' => $this->input->post('cedula'),
						'telefono' => $this->input->post('telefono'),
						'celular' => $this->input->post('celular'),
						'estado_civil' => $this->input->post('estado_civil'),
						'correo' => $this->input->post('correo'),
						'numero_dependientes' => $this->input->post('numero_dependientes'),
						'cantidad_solicitada' => $this->input->post('cantidad_solicitada'),
						'fuente_cliente' => $this->input->post('fuente_cliente'),
						'como_escucho_nosotros' => $this->input->post('como_escucho_nosotros'),
						'tipo_vivienda' => $this->input->post('tipo_vivienda'),
						'hipotecas' => $this->input->post('hipotecas'),
						'banco' => $this->input->post('banco'),
						'descuento' => $this->input->post('descuento'),
						'anho_descuento' => $this->input->post('anho_descuento'),
						'mes_descuento' => $this->input->post('mes_descuento'),
						'cantidad_descuento' => $this->input->post('cantidad_descuento'),
						'tipo_descuento' => $this->input->post('tipo_descuento'),
						'capacidad' => $this->input->post('capacidad'),
						'corregimiento' => $this->input->post('corregimiento'),
						'distrito' => $this->input->post('distrito'),
						'provincia' => $this->input->post('provincia'),
						'direccion' => $this->input->post('direccion'),
						'salario' => $this->input->post('salario'),
						'posicion_trabajo' => $this->input->post('posicion_trabajo'),
						'tiempo_laborando' => $this->input->post('tiempo_laborando'),
						'nombre_jefe_directo' => $this->input->post('nombre_jefe_directo'),
						'area' => $this->input->post('area'),
						'ministerio' => $this->input->post('ministerio'),
						'planilla' => $this->input->post('planilla'),
						'posicion' => $this->input->post('posicion'),
						'nombre_referencia' => $this->input->post('nombre_referencia'),
						'apellido_referencia' => $this->input->post('apellido_referencia'),
						'parentesco_referencia' => $this->input->post('parentesco_referencia'),
						'direccion_referencia' => $this->input->post('direccion_referencia'),
						'telefono_referencia' => $this->input->post('telefono_referencia'),
						'celular_referencia' => $this->input->post('celular_referencia'),
						'fecha_modificacion' => date('Y-m-d H:i:s'),
						'companias_id' => $company_id,
					);

					if ($this->input->post('fecha_nacimiento') != "" || $this->input->post('fecha_nacimiento') != NULL) {
						$data['fecha_nacimiento'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_nacimiento'))));
					}
					if ($this->input->post('inicio_labores') != "" || $this->input->post('inicio_labores') != NULL) {
						$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('inicio_labores'))));
					}
					$this->clientes_model->update(array('id' => $this->input->post('id')), $data);

					$datos = array(
						'usuarios_id' => NULL,
						'estados_id' => 2,
						'fecha_agente' =>  date('Y-m-d H:i:s'),
						'agente' => $this->session->nombres . ' ' . $this->session->apellidos,
					);
					$this->solicitudes_model->update(array('id' => $this->input->post('solicitud_id')), $datos);
					echo json_encode(array("status" => TRUE));
				}
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para editar clientes.'));
		}
	}


	public function verify_customer($id){
		//$id = $_REQUEST["id"];
		$solicitud = $this->solicitudes_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($solicitud->clientes_id);
		$this->getContent($id,$cliente->id,$cliente->cedula);
	}

	public function getContent($solicitud_id,$id,$cedula){

		$step_number = $_REQUEST["step_number"];

		$dats = $this->clientes_model->get_by_id($id);
		$clie = $dats->nombre . ' ' . $dats->apellido;
		$ced = $dats->cedula;
		$tlf = $dats->telefono . ' / ' . $dats->celular;
		$cliente = $clie . ' Cédula: ' . $ced . ' Tlfs: ' . $tlf;

		$html = '';
		if($step_number == 1){
			$html = $this->verify_css($step_number,$cedula,$cliente,$id);
			$html .= '

					<h2>Datos de Prestamos 911</h2>
					<div class="table-responsive">
						<table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
							<thead>
								<tr>
								<th>CEDULA</th>
								<th>NOMBRE</th>
								<th>COMPAÑIA</th>
								<th>TELEFONO</th>
								<th>SALARIO</th>
								<th>EMPLEADOS</th>
								</tr>
							</thead>
							<tbody>
								<td class="modal-cedula"></td>
								<td class="modal-nombre"></td>
								<td class="modal-companias_id"></td>
								<td class="modal-telefono"></td>
								<td class="modal-salario"></td>
								<td class="modal-empleados"></td>
							</tbody>
						</table>
					</div>

					<form class="form-horizontal" id="form_css">

						<input type="hidden" name="solicitud_id" id="solicitud_id" value="'.$solicitud_id.'">
						<input type="hidden" name="clientes_id" id="clientes_id" value="'.$id.'">
						<input type="hidden" name="cedu_access" id="cedu_access">
						<input type="hidden" name="db_access_id" id="db_access_id">

						<div id="msg-error">
						<div class="list-errors animated shake"></div>
						</div>

						<div id="validcedu"></div>

						<label>Comentario</label>
						<textarea id="comentario_css" required="required" class="form-control" name="comentario_css" placeholder="Comentario"></textarea>
						<div class="checkbox">
						<label>
						<input type="checkbox" value="Si" name="valido" id="checkvalido" disabled> Valido
						</label>
						</div>

					</form>

				</div>

				<script type="text/javascript">
					view_client_css('.$solicitud_id.');
					if ($("#cedula_access").val() != null || $("#cedula_access").val() != "") {
						$("#cedu_access").val($("#cedula_access").val());
					}
				</script>

			';
		}elseif($step_number == 2){

/*
					<a class="btn btn-primary btn-xs" href="javascript:history.go(-1)"><i class="fa fa-arrow-left"></i> Atrás </a>
					<a class="btn btn-primary btn-xs" href="javascript:history.go(1)">Adelante <i class="fa fa-arrow-right"></i>  </a>*/

			$html = '<h2 class="StepTitle">Paso '.$step_number.' Verificación de Apc - '.$cliente.'</h2>';
			$html .='

				<center>
					<a class="btn btn-primary btn-xs" onclick="refreshIframe()"><i class="fa fa-refresh"></i> Restablecer</a>
					<div class="ciframe">
						<iframe src="https://online.apc.com.pa/Login/tabid/468/Default.aspx?returnurl=%2fdefault.aspx" width="1000" height="1000" frameborder="0" allowfullscreen> </iframe>
					</div>
				</center>	

				<style>
					#form_apc {overflow-y: hidden;}
				</style>


				<div class="col-lg-12 col-sm-12 col-12">
					<span class="permission-error-archivo label label-danger"></span>
				</div>

				<br><br>

				<form method="POST" id="form_apc" enctype="multipart/form-data">

				<input type="hidden" name="solicitud_id" id="solicitud_id" value="'.$solicitud_id.'">
					<input type="hidden" name="clientes_id_apc" id="clientes_id_apc" value="'.$id.'">

					<div class="col-lg-6 col-sm-6 col-12">                            
						<div class="input-group">
							<label class="input-group-btn">
								<span class="btn btn-primary">
									Examinar&hellip; <input type="file" name="file" style="display: none;" multiple>
								</span>
							</label>
							<input type="text" name="nombre_archivo_apc" class="form-control" readonly>
						</div>
					</div>

					<div class="col-lg-12 col-sm-12 col-12">
						<textarea id="comentario_apc" required class="form-control" name="comentario_apc" placeholder="Comentario de Apc"></textarea>
					</div>                  

				</form>


				<script type="text/javascript">
					view_client_apc('.$solicitud_id.');	
					scriptFile();	  
				</script>

			';
		}elseif($step_number == 3){
			$html = '<h2 class="StepTitle">Paso '.$step_number.' Verificación de Compañia - '.$cliente.'</h2>';
			$html .='

				<style>
					#contenedor {overflow-y: hidden;}
				</style>

				<div id="contenedor">


					<form id="form_compania" class="form-horizontal form-label-left input_mask">

						<input type="hidden" name="clientes_id" id="clientes_id" value="'.$id.'">
						<input type="hidden" value="" name="cid"/>

						<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
							<h2>Información de la Empresa</h2>
                  			<div class="fecha_inicio"></div>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
							<label>Salario:</label>
							<input type="text" name="csalario" required class="form-control has-feedback-left" id="vinputSuccess" placeholder="Salario">
							<span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
							<label>Posición de Trabajo:</label>
							<input type="text" name="cposicion_trabajo" required class="form-control has-feedback-left" id="vinputSuccess2" placeholder="Posición de Trabajo">
							<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback selectcompany">
							<label>Empresa:</label>
							<select required class="form-control" name="ccompanias_id" id="vinputSuccess3" onchange="selectEmpresa()" style="width: 100%"></select>
						</div>

		                <div class="col-md-1 col-sm-1 col-xs-12 form-group has-feedback">
		                  <br>
		                  <button type="button" id="btncompany" class="btn btn-primary" title="Crear Compañia" onclick="add_companies()"><i class="fa fa-building-o"></i></button>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Inicio de Labores</label>
		                  <input type="text" name="cinicio_labores" required class="form-control has-feedback-left date-picker" id="vinputSuccess281" maxlength="10" placeholder="Inicio de Labores">
		                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                </div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
							<label>Tiempo Laborando:</label>
							<input type="text" name="ctiempo_laborando" required class="form-control has-feedback-left" id="vinputSuccess4" placeholder="Tiempo Laborando">
							<span class="fa fa-clock-o form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
							<label>Nombre del Jefe Directo:</label>
							<input type="text" name="cnombre_jefe_directo" required class="form-control has-feedback-left" id="vinputSuccess5" placeholder="Nombre del Jefe Directo">
							<span class="fa fa-black-tie form-control-feedback left" aria-hidden="true"></span>
						</div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                  <label>Tiene Descuento</label>
                  <select required class="form-control has-feedback-left" name="cdescuento" id="inputSuccess18" placeholder="Tiene Descuento" onchange="select_descuento(this.value)">
                    <option disabled selected hidden>Tiene Descuento</option>
                    <option>Si</option>
                    <option selected="">No</option>
                  </select>
                  <span class="fa fa-minus form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                  <label>Tiempo Restante de Descuento</label>                  
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <div class="row">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Años</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" name="canho_descuento" required class="form-control" id="inputSuccess19" placeholder="Años" readonly="" onkeypress="return justNumbers(event);">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                      <div class="row">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Meses</label>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <input type="text" name="cmes_descuento" required class="form-control" id="inputSuccess192" placeholder="Meses" readonly="" onkeypress="return justNumbers(event);">
                        </div>
                      </div>
                    </div>
                  </div>                  
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                  <label>Cantidad de Descuento</label>
                  <input type="text" name="ccantidad_descuento" required class="form-control has-feedback-left" id="oinputSuccess" placeholder="Cantidad de Descuento" readonly="" onkeyup="posee_capacidad()">
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                  <label>Tipo de Descuento</label>
                  <select required class="form-control has-feedback-left" name="ctipo_descuento" id="oinputSuccess1" placeholder="Tipo de Descuento" disabled="" onclick="posee_capacidad()">
                    <option disabled selected hidden>Tipo de Descuento</option>
                    <option>Quincenal</option>
                    <option>Mensual</option>
                  </select>
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
							<label>Documentos:</label><br>
							<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="file_client('.$id.')"><i class="fa fa-file"></i> Documentos </a>
						</div>




						<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
							<h2>Empresa</h2>
							<span class="label label-info crear-compania"></span>
						</div>


						<div id="msg-error_compania" class="col-md-12 col-sm-12 col-xs-12 form-group company has-feedback">
							<div class="list-errors_compania animated shake"></div>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12 form-group company has-feedback">
							<label class="etiqueta">Compañia:</label>
							<input type="text" name="cnombre" required class="form-control has-feedback-left" id="inputSuccess" placeholder="Nombre de la Compañia">
							<span class="fa fa-building-o form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12 form-group company has-feedback">
							<label>Dirección:</label>
							<textarea id="inputSuccess1" required class="form-control" name="cdireccion" placeholder="Dirección"></textarea>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Teléfono:</label>
							<input type="text" name="ctelefono" required class="form-control has-feedback-left" id="inputSuccess2" placeholder="Teléfono">
							<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>RUC:</label>
							<input type="text" name="cruc" required class="form-control has-feedback-left" id="inputSuccess3" placeholder="RUC">
							<span class="fa fa-registered form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Sitio Web:</label>
							<input type="text" name="csitio_web" required class="form-control has-feedback-left" id="inputSuccess4" placeholder="Sitio Web">
							<span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Año de Constitución:</label>
							<input type="text" name="cano_constitucion" required class="form-control has-feedback-left" id="inputSuccess5" placeholder="Año de Constitución">
							<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Correo Electrónico:</label>
							<input type="text" name="ccorreo" required class="form-control has-feedback-left" id="inputSuccess6" placeholder="Correo Electrónico">
							<span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Nombre del Recursos Humano:</label>
							<input type="text" name="cnombre_rrhh" required class="form-control has-feedback-left" id="inputSuccess7" placeholder="Nombre del Recursos Humano">
							<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Teléfono de Recursos Humanos:</label>
							<input type="text" name="ctelefono_rrhh" required class="form-control has-feedback-left" id="inputSuccess8" placeholder="Teléfono de Recursos Humanos">
							<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Nombre de Contabilidad:</label>
							<input type="text" name="cnombre_contabilidad" required class="form-control has-feedback-left" id="inputSuccess9" placeholder="Nombre de Contabilidad">
							<span class="fa fa-bookmark-o form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Teléfono de Contabilidad:</label>
							<input type="text" name="ctelefono_contabilidad" required class="form-control has-feedback-left" id="inputSuccess10" placeholder="Teléfono de Contabilidad">
							<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Forma de Pago:</label>
  		                    <select required class="form-control has-feedback-left" name="cforma_pago" id="inputSuccess11" placeholder="Forma de Pago">
		                      <option disabled selected hidden>Forma de Pago</option>
		                      <option>ACH</option>
		                      <option>Cheque</option>
		                      <option>Efectivo</option>
		                    </select>
		                    <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Cantidad de Empleados:</label>
							<input type="text" name="ccantidad_empleados" required class="form-control has-feedback-left" id="inputSuccess12" placeholder="Cantidad de Empleados">
							<span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12 form-group company has-feedback">
							<label>Rubro:</label>
							<input type="text" name="crubro" required class="form-control has-feedback-left" id="inputSuccess13" placeholder="Rubro">
							<span class="fa fa-bookmark-o form-control-feedback left" aria-hidden="true"></span>
						</div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Nombre Firma:</label>
		                  <input type="text" name="cnombre_firma" required class="form-control has-feedback-left" id="cnombre_firma" placeholder="Nombre Firma">
		                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Posición Firma:</label>
		                  <input type="text" name="cposicion_firma" required class="form-control has-feedback-left" id="cposicion_firma" placeholder="Posición Firma">
		                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>


					<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
						<hr>

							<input type="hidden" name="solicitud_id" id="solicitud_id" value="'.$solicitud_id.'">

							<div id="msg-error-compania">
								<div class="list-errors-compania animated shake"></div>
							</div>

							<label>Comentario:</label>
							<textarea id="comentario_compania" required="required" class="form-control" name="comentario_compania" placeholder="Comentario"></textarea>
							<div class="checkbox">
								<label>
									<input type="checkbox" value="Si" name="valido_compania" id="checkvalidocompania"> Valido
								</label>
							</div>

						</form>
					</div>

				</div>

				<script type="text/javascript">
					view_companies('.$solicitud_id.');
					view_client_com('.$solicitud_id.');
					dateinicio();					 
				</script>

			';
		} else {


/*					<a class="btn btn-primary btn-xs" href="javascript:history.go(-1)"><i class="fa fa-arrow-left"></i> Atrás </a>
					<a class="btn btn-primary btn-xs" href="javascript:history.go(1)">Adelante <i class="fa fa-arrow-right"></i>  </a>*/

/*<iframe src="https://rp.gob.pa/ValidacionDigital.aspx" width="1000" height="750" frameborder="0" allowfullscreen> </iframe>*/


			$html = '<h2 class="StepTitle">Paso '.$step_number.' Verificación de Registro Público - '.$cliente.'</h2>';
			$html .='

				<style>
					#form_rp {overflow-y: hidden;}
				</style>

				<center>
					<a class="btn btn-primary btn-xs" onclick="refreshIframe1()"><i class="fa fa-refresh"></i> Restablecer</a>
					<div class="ciframe1">
						<object type="text/html" data="https://rp.gob.pa/ValidacionDigital.aspx" width="800" height="800" scrolling="yes" style="overflow:hidden; margin-top:-4px; margin-left:-4px; border:none;"></object>
					</div>
				</center>

				<div class="col-lg-12 col-sm-12 col-12">
					<span class="permission-error-archivo label label-danger"></span>
				</div>

				<br><br>

				<form method="POST" id="form_rp" enctype="multipart/form-data">

					<input type="hidden" name="solicitud_id" id="solicitud_id" value="'.$solicitud_id.'">
					<input type="hidden" name="clientes_id_rp" id="clientes_id_rp" value="'.$id.'">

					<div class="col-lg-6 col-sm-6 col-12">                            
						<div class="input-group">
							<label class="input-group-btn">
								<span class="btn btn-primary">
									Examinar&hellip; <input type="file" name="file" style="display: none;" multiple>
								</span>
							</label>
							<input type="text" name="nombre_archivo_rp" class="form-control" readonly>
						</div>
					</div>

					<div class="col-lg-12 col-sm-12 col-12">
						<textarea id="comentario_rp" required class="form-control" name="comentario_rp" placeholder="Comentario de Registro Público"></textarea>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="Si" name="valido_rp" id="checkvalidorp"> Valido
							</label>
						</div>                          
					</div>               

				</form>


				<script type="text/javascript">
					view_client_rp('.$solicitud_id.');
					scriptFile();
				</script>

			';
		}
		echo $html;
	}

    function myGetType($var)
    {
        if (is_array($var)) return "array";
        if (is_bool($var)) return "boolean";
        if (is_float($var)) return "float";
        if (is_int($var)) return "integer";
        if (is_null($var)) return "NULL";
        if (is_numeric($var)) return "numeric";
        if (is_object($var)) return "object";
        if (is_resource($var)) return "resource";
        if (is_string($var)) return "string";
        return "unknown type";
    }

    public function name_db_access($id){
    	$access = $this->db_access_model->get_by_id($id);
    	return $access->nombre;
    }

    public function div_seleccion(){

    	$cedula = $this->input->post('cedula');
    	$db_access_id = $this->input->post('db_access_id');
    	$db = $this->name_db_access($db_access_id);

    	$datos = $this->bycedula($db,$cedula,$db_access_id);
		
		echo json_encode($datos);
    }

	function verify_css($step_number,$cedula,$cliente,$cliente_id){
		$cedu = NULL;
		$datos_verificacion = $this->datos_verificacion_model->get_by_clientes_id($cliente_id);
		if ($datos_verificacion) {
			$cedu = $datos_verificacion->cedula_access;
		} else {

			$tipo = $this->myGetType($cedula);
			if ($tipo == "string") {
				$partes = explode('-',$cedula);
				$partes[1] = substr($partes[1],1);
				for ($i=0; $i < count($partes) ; $i++) {
					$cedu .= $partes[$i];
					if ($i == 0) {
						$cedu .= "  ";
					}
				}
			}
			
			$cedu = substr($cedu,1);
		}

		$access = $this->db_access_model->get_all();
		$option = '';
		if ($access) {
			foreach ($access as $row) {
				$option .= '<option value="'.$row->id.'">'.$row->nombre.'</option>';
			}
		}

		$html = '<form id="form-access" class="form-horizontal">
				  <input type="hidden" name="cedulaaccess" value="'.$cedu.'">
		          <div class="form-group">
		            <label class="control-label col-md-4 col-sm-4 col-xs-12">Seleccione la Base de Datos de Access
		            </label>
		            <div class="col-md-4 col-sm-4 col-xs-12">
                  		<select class="form-control col-md-6 col-xs-12" name="dbaccess" id="dbaccess" onchange="select_access(this.value)">
                  			<option disabled selected hidden>Seleccione Base de Datos Access</option>'. 
                  			$option 
                  		.'</select>
		            </div>
		          </div>
		        </form>
		        <div id="div-seleccion"></div>';

		echo '

			<style type="text/css"> 
				#step2 {
					overflow-y: hidden;
					min-height: 500px;
				}
			</style> 

			<h2 class="StepTitle">Paso '.$step_number.' Verificación Css - '.$cliente.'</h2>

			<div id="step2">'. $html .'

		';

	}

	public function bycedula($tabla,$cedula,$db_access_id){
		$valor = $this->datos_model->get_by_cedula($tabla,$cedula);
		$array = array();
		if ($valor) {
			foreach ($valor as $row) {
				$array[] = array(
					'seguro' => $row->seguro,
					'cedula' => $row->cedula,
					'nombre' => $row->nombre,
					'razon_so' => $row->razon_so,
					'patrono' => $row->patrono,
					'tel1' => $row->tel1,
					'tel2' => $row->tel2,
					'fecha' => $row->fecha,
					'salario' => number_format((int) $row->salario / 100,2),
				);
			}
		}

		$html = '<div class="col-lg-12 col-sm-12 col-12 table-responsive">
	                <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>SEGURO</th>
	                            <th>CEDULA</th>
	                            <th>NOMBRE</th>
	                            <th>RAZON_SO</th>
	                            <th>PATRONO</th>
	                            <th>TEL1</th>
	                            <th>TEL2</th>
	                            <th>FECHA</th>
	                            <th>SALARIO</th>
	                            <th>EMPLEADOS</th>
	                        </tr>
	                    </thead>
	                    <tbody>';
	    $numempleados = 0;
	    if (count($array)>0) {
	    	$numempleados = $this->datos_model->numempleados($tabla,$array[0]['razon_so']);
	    }
		for ($i=0; $i < count($array); $i++) { 
/*			$html .= '<tr>
				<td> '. $array[$i]['seguro'] .' </td>
				<td> '. $array[$i]['cedula'] .' </td>
				<td> '. $array[$i]['nombre'] .' </td>
				<td> '. $array[$i]['razon_so'] .' </td>
				<td> '. $array[$i]['patrono'] .' </td>
				<td> '. $array[$i]['tel1'] .' </td>
				<td> '. $array[$i]['tel2'] .' </td>
				<td> '. $array[$i]['fecha'] .' </td>
				<td> '. $array[$i]['salario'] .' </td>
				<td> <input type="hidden" value="'.$array[$i]['cedula'].'" name="selectCedula'.$i.'" id="selectCedula'.$i.'"/><a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="view_search('.$i.')"><i class="fa fa-check"></i> Seleccionar </a> </td>
			</tr>';*/
			$html .= '<tr>
				<td> '. $array[$i]['seguro'] .' </td>
				<td> '. $array[$i]['cedula'] .' </td>
				<td> '. $array[$i]['nombre'] .' </td>
				<td> '. $array[$i]['razon_so'] .' </td>
				<td> '. $array[$i]['patrono'] .' </td>
				<td> '. $array[$i]['tel1'] .' </td>
				<td> '. $array[$i]['tel2'] .' </td>
				<td> '. $array[$i]['fecha'] .' </td>
				<td> '. $array[$i]['salario'] .' </td>				
				<td> '. $numempleados .' </td>
			</tr>';
		}
		$html .= '</tbody>
	                </table>
	            </div>

				<script>
					$("#checkvalido").removeAttr("disabled");
				</script>

	            ';

		$respuesta = null;
		if (count($array) == 0) {
			$respuesta = '

				<h2>Datos de CSS</h2>

				<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
					<p class="label label-danger alerta">Datos No encontrados, Intente con los ultimos 8 números de la cédula.</p> <br><br>
					<div class="alert alert-info" style="font-size: 14px;">NOTA: En caso de no aparecer con la cédula, debe chequearlo en la sección de Solicitudes -> Seguro Social. Ya que por el Nombre o Apellido se pueden encontrar muchos resultados. Al encontrar el cliente deseado puede copiar sus últimos 8 números de cédula y colocarlos aca si lo desea.
					</div>
					<span class="label label-danger aviso"></span>
					<div id="resultado"></div>
				</div>

				<form class="form-horizontal form-label-left" id="form_search">
					<input type="hidden" name="db_access" id="db_access" value="'.$db_access_id.'">
					<div class="form-group">
						<label class="col-sm-1 control-label">Cédula</label>
						<div class="col-sm-4">
							<div class="input-group">
								<input type="text" class="form-control" name="buscar_cedula" id="buscar_cedula" onkeyup="searchCedula()" maxlength="8">
								<span class="input-group-btn">
								<button type="button" class="btn btn-primary" onclick="searchCedula()"><i class="fa fa-search"></i> Buscar</button>
								</span>
							</div>
						</div>
					</div>
				</form>

				<script>
					$("#checkvalido").removeAttr("disabled");
				</script>
			';


		} else {
			$respuesta = $html;
		}

		return $respuesta;
	}

	public function search_cedula(){

		$cedula = $this->input->post('buscar_cedula');
		$tabla = $this->name_db_access($this->input->post('db_access'));

		$valor = $this->datos_model->get_like_cedula($tabla,$cedula);

		$array = array();
		if ($valor) {
			foreach ($valor as $row) {
				$array[] = array(
					'seguro' => $row->seguro,
					'cedula' => $row->cedula,
					'nombre' => $row->nombre,
					'razon_so' => $row->razon_so,
					'patrono' => $row->patrono,
					'tel1' => $row->tel1,
					'tel2' => $row->tel2,
					'fecha' => $row->fecha,
					'salario' => number_format((int) $row->salario / 100,2),
				);
			}
		}

		$html = '<div class="col-lg-12 col-sm-12 col-12 table-responsive">
	                <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
	                    <thead>
	                        <tr>
	                            <th>SEGURO</th>
	                            <th>CEDULA</th>
	                            <th>NOMBRE</th>
	                            <th>RAZON_SO</th>
	                            <th>PATRONO</th>
	                            <th>TEL1</th>
	                            <th>TEL2</th>
	                            <th>FECHA</th>
	                            <th>SALARIO</th>
	                            <th>ACCION</th>
	                        </tr>
	                    </thead>
	                    <tbody>';
		for ($i=0; $i < count($array); $i++) { 
			$html .= '<tr>
				<td> '. $array[$i]['seguro'] .' </td>
				<td> '. $array[$i]['cedula'] .' </td>
				<td> '. $array[$i]['nombre'] .' </td>
				<td> '. $array[$i]['razon_so'] .' </td>
				<td> '. $array[$i]['patrono'] .' </td>
				<td> '. $array[$i]['tel1'] .' </td>
				<td> '. $array[$i]['tel2'] .' </td>
				<td> '. $array[$i]['fecha'] .' </td>
				<td> '. $array[$i]['salario'] .' </td>
				<td> <input type="hidden" value="'.$array[$i]['cedula'].'" name="selectCedula'.$i.'" id="selectCedula'.$i.'"/><a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="view_search('.$i.')"><i class="fa fa-check"></i> Seleccionar </a> </td>
			</tr>';
		}
		$html .= '</tbody>
	                </table>
	            </div>';

		$respuesta = null;
		if (count($array) == 0) {
			$respuesta = '<p class="label label-danger alerta">No hay registro de ese numero.</p>';
		} else {
			$respuesta = $html;
		}

		echo json_encode(array("datos" => $respuesta));
	}

	public function seleccion_cedula(){

		$cedula = $this->input->post('selectCedula');
		$tabla = $this->name_db_access($this->input->post('selectAccess'));

		$valor = $this->datos_model->get_by_cedula($tabla,$cedula);
		$numempleados = 0;
	    if (count($valor)>0) {
	    	$numempleados = $this->datos_model->numempleados($tabla,$valor[0]->razon_so);
	    }

	    $array =  '
            <div class="table-responsive">
                <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>SEGURO</th>
                            <th>CEDULA</th>
                            <th>NOMBRE</th>
                            <th>RAZON_SO</th>
                            <th>PATRONO</th>
                            <th>TEL1</th>
                            <th>TEL2</th>
                            <th>FECHA</th>
                            <th>SALARIO</th>
                            <th>EMPLEADOS</th>
                        </tr>
                    </thead>
                    <tbody>
						<td> '. $valor[0]->seguro .' </td>
						<td> '. $valor[0]->cedula .' </td>
						<td> '. $valor[0]->nombre .' </td>
						<td> '. $valor[0]->razon_so .' </td>
						<td> '. $valor[0]->patrono .' </td>
						<td> '. $valor[0]->tel1 .' </td>
						<td> '. $valor[0]->tel2 .' </td>
						<td> '. $valor[0]->fecha .' </td>
						<td> '. number_format((int) $valor[0]->salario / 100,2) .' </td>
						<td> '. $numempleados .' </td>


                    </tbody>
                </table>
            </div>

        ';

		echo json_encode(array("datos" => $array));
	}

	public function change_companies($id){
		$data = $this->clientes_model->get_companies_by_id($id);
		echo json_encode($data);
	}

	public function ajax_view($id) {
		$solicitud = $this->solicitudes_model->get_by_id($id);
		$data = $this->clientes_model->get_by_id($solicitud->clientes_id);
		if ($data->inicio_labores != NULL) {
			$data->inicio_labores = date('d/m/Y', strtotime(str_replace("/","-",$data->inicio_labores)));
		}
		$compania = $this->clientes_model->get_companies_by_id($data->companias_id);
		if ($compania) {
			$data->companias_nombre = $compania->nombre;
			$data->companias_direccion = $compania->direccion;
			$data->companias_telefono = $compania->telefono;
			$data->companias_ruc = $compania->ruc;
			$data->companias_sitio_web = $compania->sitio_web;
			$data->companias_ano_constitucion = $compania->ano_constitucion;
			$data->companias_correo = $compania->correo;
			$data->companias_nombre_rrhh = $compania->nombre_rrhh;
			$data->companias_telefono_rrhh = $compania->telefono_rrhh;
			$data->companias_nombre_contabilidad = $compania->nombre_contabilidad;
			$data->companias_telefono_contabilidad = $compania->telefono_contabilidad;
			$data->companias_forma_pago = $compania->forma_pago;
			$data->companias_cantidad_empleados = $compania->cantidad_empleados;
			$data->companias_rubro = $compania->rubro;
			$data->companias_nombre_firma = $compania->nombre_firma;
			$data->companias_posicion_firma = $compania->posicion_firma;
		}	
		$user_creator = $this->usuarios_model->get_by_id($data->usuarios_id);
		$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);
		$cadena = $user_creator->nombres;
		$nombre = explode(' ',$cadena);
		$cadena1 = $user_creator->apellidos;
		$apellido = explode(' ',$cadena1);
		$data->usuarios_id = $nombre[0] . " " . $apellido[0] . ", <b>" . $rol_creator->descripcion . ".</b>";

		$data->db_access_id = $solicitud->db_access_id;

		$datos_verificacion = $this->datos_verificacion_model->get_by_clientes_id($solicitud->clientes_id);
		if ($datos_verificacion) {
			$data->comentario = $datos_verificacion->comentario_css;
			$data->archivoapc = $datos_verificacion->archivo_apc;
			$data->valido = $datos_verificacion->validacion_css;
			$data->comentarioapc = $datos_verificacion->comentario_apc;
			$data->comentariocompania = $datos_verificacion->comentario_compania;
			$data->validocompania = $datos_verificacion->validacion_compania;
			$data->comentariorp = $datos_verificacion->comentario_rp;
			$data->archivorp = $datos_verificacion->archivo_rp;
			$data->validorp = $datos_verificacion->validacion_rp;
			$data->cedula_access = $datos_verificacion->cedula_access;
		}

		$data->solicitud_id = $solicitud->id;

		echo json_encode($data);
	}

	public function add_css() {
		$this->form_validation->set_rules('comentario_css','comentario','required|trim|min_length[3]|max_length[255]|xss_clean');
		$this->form_validation->set_rules('valido','valido','xss_clean');
		$this->form_validation->set_rules('db_access_id','base de datos de access','required|xss_clean|callback_check_default');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$valido = null;
			if ($this->input->post('valido') == "" || $this->input->post('valido') == null){
				$valido = "No";
			} else {
				$valido = $this->input->post('valido');
			}

			$this->solicitudes_model->update(array('id' => $this->input->post('solicitud_id')), array('db_access_id' => $this->input->post('db_access_id')));

			$data = array(
				'comentario_css' => $this->input->post('comentario_css'),
				'validacion_css' => $valido,
				'cedula_access' => $this->input->post('cedu_access'),
				'clientes_id' => $this->input->post('clientes_id')
			);

			$datos_verificacion = $this->datos_verificacion_model->get_by_clientes_id($this->input->post('clientes_id'));
			if ($datos_verificacion) {
				$this->datos_verificacion_model->update(array('id' => $datos_verificacion->id), $data);
			} else {
				$insert = $this->datos_verificacion_model->save($data);
			}
			
			if ($valido == "No") {
				echo json_encode(array("permission" => '<span class="label label-danger animated shake">No validado</span>'));
			} else {
				echo json_encode(array("status" => TRUE));
			}

		}
	}

    public function cargar_archivo_apc() {

    	$solicitud_id = $this->input->post('solicitud_id');
    	$nombre_archivo = $this->input->post('nombre_archivo_apc');
    	$comentario = $this->input->post('comentario_apc');
    	$cliente_id = $this->input->post('clientes_id_apc');

    	if ($comentario == null || $comentario == "") {
    		echo json_encode(array('permission'=> 'Agrege un comentario.'));
    	} else {

    		$solicitud = $this->solicitudes_model->get_by_id($solicitud_id);
    		$datos_verificacion = $this->datos_verificacion_model->get_by_clientes_id($solicitud->clientes_id);

			if ($datos_verificacion->archivo_apc != null || $datos_verificacion->archivo_apc != "") {
				$file = "files/apc/".$datos_verificacion->archivo_apc;
				if ($file) {
					unlink($file);
				}
			}
			if ($nombre_archivo == null || $nombre_archivo == "") {
				$data = array(
					'archivo_apc' => NULL,
					'comentario_apc' => $comentario
				); 
				$this->datos_verificacion_model->update(array('id' => $datos_verificacion->id), $data);
				echo json_encode(array("status" => TRUE));
			} else {
		        $archivo = 'file';
		        $config['upload_path'] = "files/apc/";
		        $config['file_name'] = $nombre_archivo;
		        $config['allowed_types'] = "*";
		        $config['overwrite'] = FALSE;

		        $this->load->library('upload', $config);
		        
		        if (!$this->upload->do_upload($archivo)) {
		            echo json_encode($this->upload->display_errors());
		            return;
		        }

				$data = $this->upload->data();
				$filename = $data['file_name'];
				$data = array(
					'archivo_apc' => $filename,
					'comentario_apc' => $comentario
				); 
				$this->datos_verificacion_model->update(array('id' => $datos_verificacion->id), $data);
				echo json_encode(array("status" => TRUE));
			}

    	}
    }

    public function updateClientCompanias($companias_id){
		//if ($this->admin_model->get_modificar($this->roluser) == 1){
			$this->form_validation->set_rules('salario','salario','trim|min_length[1]|xss_clean');
			$this->form_validation->set_rules('posicion_trabajo','posición de trabajo','trim|min_length[4]|xss_clean');
			$this->form_validation->set_rules('inicio_labores','inicio de labores','trim|regex_match["[0-9]{2}/[0-9]{2}/[0-9]{4}"]|xss_clean');
			$this->form_validation->set_rules('tiempo_laborando','tiempo laborando','trim|min_length[4]|xss_clean');


			$this->form_validation->set_rules('descuento','posee descuento','trim|min_length[2]|xss_clean');
			$this->form_validation->set_rules('anho_descuento','años de descuento','trim|xss_clean');
			$this->form_validation->set_rules('mes_descuento','meses de descuento','trim|xss_clean');
			$this->form_validation->set_rules('cantidad_descuento','cantidad de descuento','trim|xss_clean');
			$this->form_validation->set_rules('tipo_descuento','tipo de descuento','trim|xss_clean');

			$this->form_validation->set_rules('companias_id','empresa','callback_check_default');

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$data = array(
					'salario' => $this->input->post('csalario'),
					'posicion_trabajo' => $this->input->post('cposicion_trabajo'),
					'tiempo_laborando' => $this->input->post('ctiempo_laborando'),
					'nombre_jefe_directo' => $this->input->post('cnombre_jefe_directo'),
					'descuento' => $this->input->post('cdescuento'),
					'anho_descuento' => $this->input->post('canho_descuento'),
					'mes_descuento' => $this->input->post('cmes_descuento'),
					'cantidad_descuento' => $this->input->post('ccantidad_descuento'),
					'tipo_descuento' => $this->input->post('ctipo_descuento'),
					'companias_id' => $companias_id,
				);

				if ($this->input->post('cinicio_labores') != "" || $this->input->post('cinicio_labores') != NULL) {
					$data['inicio_labores'] = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('cinicio_labores'))));
				}

				$this->clientes_model->update(array('id' => $this->input->post('clientes_id')), $data);
				//echo json_encode(array("status" => TRUE));
				$this->valid_compania();
			}
		//} else {
			//echo json_encode(array('permission'=> 'No tienes permiso para editar clientes.'));
		//}
    }

    public function create_companias(){
		//if ($this->admin_model->get_modificar($this->roluser) == 1){
			$this->form_validation->set_rules('cnombre','nombre de la compañia','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('cdireccion','dirección','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('ctelefono','teléfono','required|trim|xss_clean');
			$this->form_validation->set_rules('cruc','ruc','trim|xss_clean');
			$this->form_validation->set_rules('csitio_web','sitio web','trim|xss_clean');
			$this->form_validation->set_rules('cano_constitucion','año de constitución','required|trim|xss_clean');
			$this->form_validation->set_rules('ccorreo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('cnombre_rrhh','nombre del recursos humano','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('ctelefono_rrhh','teléfono del recursos humano','required|trim|xss_clean');
			$this->form_validation->set_rules('cnombre_contabilidad','nombre de contabilidad','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('ctelefono_contabilidad','teléfono de contabilidad','trim|xss_clean');
			$this->form_validation->set_rules('cforma_pago','forma de pagos','required|trim|xss_clean');
			$this->form_validation->set_rules('ccantidad_empleados','cantidad de empleados','required|trim|xss_clean');
			$this->form_validation->set_rules('crubro','rubro','required|trim|xss_clean');
			$this->form_validation->set_rules('cnombre_firma','nombre firma','required|trim|xss_clean');
			$this->form_validation->set_rules('cposicion_firma','posición firma','required|trim|xss_clean');

			if ($this->form_validation->run()==FALSE) {
				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$data = array(
					'nombre' => $this->input->post('cnombre'),
					'direccion' => $this->input->post('cdireccion'),
					'telefono' => $this->input->post('ctelefono'),
					'ruc' => $this->input->post('cruc'),
					'sitio_web' => $this->input->post('csitio_web'),
					'ano_constitucion' => $this->input->post('cano_constitucion'),
					'correo' => $this->input->post('ccorreo'),
					'nombre_rrhh' => $this->input->post('cnombre_rrhh'),
					'telefono_rrhh' => $this->input->post('ctelefono_rrhh'),
					'nombre_contabilidad' => $this->input->post('cnombre_contabilidad'),
					'telefono_contabilidad' => $this->input->post('ctelefono_contabilidad'),
					'forma_pago' => $this->input->post('cforma_pago'),
					'cantidad_empleados' => $this->input->post('ccantidad_empleados'),
					'rubro' => $this->input->post('crubro'),
					'nombre_firma' => $this->input->post('cnombre_firma'),
					'posicion_firma' => $this->input->post('cposicion_firma'),
					'usuarios_id' => $this->session->userdata('id_user'),
				);
				$insert = $this->companias_model->save($data);
				$this->updateClientCompanias($insert);
				//echo json_encode(array("status" => TRUE));
			}
		//} else {
			//echo json_encode(array('permission'=> 'No tienes permiso para crear compañias.'));
		//}
    }

    public function update_companias(){
		//if ($this->admin_model->get_modificar($this->roluser) == 1){
			$this->form_validation->set_rules('cnombre','nombre de la compañia','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('cdireccion','dirección','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('ctelefono','teléfono','required|trim|xss_clean');
			$this->form_validation->set_rules('cruc','ruc','trim|xss_clean');
			$this->form_validation->set_rules('csitio_web','sitio web','trim|xss_clean');
			$this->form_validation->set_rules('cano_constitucion','año de constitución','required|trim|xss_clean');
			$this->form_validation->set_rules('ccorreo','correo electrónico','valid_email|trim|xss_clean');
			$this->form_validation->set_rules('cnombre_rrhh','nombre del recursos humano','required|trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('ctelefono_rrhh','teléfono del recursos humano','required|trim|xss_clean');
			$this->form_validation->set_rules('cnombre_contabilidad','nombre de contabilidad','trim|min_length[3]|max_length[255]|xss_clean');
			$this->form_validation->set_rules('ctelefono_contabilidad','teléfono de contabilidad','trim|xss_clean');
			$this->form_validation->set_rules('cforma_pago','forma de pagos','required|trim|xss_clean');
			$this->form_validation->set_rules('ccantidad_empleados','cantidad de empleados','required|trim|xss_clean');
			$this->form_validation->set_rules('crubro','rubro','required|trim|xss_clean');
			$this->form_validation->set_rules('cnombre_firma','nombre firma','required|trim|xss_clean');
			$this->form_validation->set_rules('cposicion_firma','posición firma','required|trim|xss_clean');

			if ($this->form_validation->run()==FALSE) {

				$data = array(
					'nombre' => $this->input->post('cnombre'),
					'direccion' => $this->input->post('cdireccion'),
					'telefono' => $this->input->post('ctelefono'),
					'ruc' => $this->input->post('cruc'),
					'sitio_web' => $this->input->post('csitio_web'),
					'ano_constitucion' => $this->input->post('cano_constitucion'),
					'correo' => $this->input->post('ccorreo'),
					'nombre_rrhh' => $this->input->post('cnombre_rrhh'),
					'telefono_rrhh' => $this->input->post('ctelefono_rrhh'),
					'nombre_contabilidad' => $this->input->post('cnombre_contabilidad'),
					'telefono_contabilidad' => $this->input->post('ctelefono_contabilidad'),
					'forma_pago' => $this->input->post('cforma_pago'),
					'cantidad_empleados' => $this->input->post('ccantidad_empleados'),
					'rubro' => $this->input->post('crubro'),
					'nombre_firma' => $this->input->post('cnombre_firma'),
					'posicion_firma' => $this->input->post('cposicion_firma'),
				);
				$this->companias_model->update(array('id' => $this->input->post('cid')), $data);

				echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
			} else {
				$data = array(
					'nombre' => $this->input->post('cnombre'),
					'direccion' => $this->input->post('cdireccion'),
					'telefono' => $this->input->post('ctelefono'),
					'ruc' => $this->input->post('cruc'),
					'sitio_web' => $this->input->post('csitio_web'),
					'ano_constitucion' => $this->input->post('cano_constitucion'),
					'correo' => $this->input->post('ccorreo'),
					'nombre_rrhh' => $this->input->post('cnombre_rrhh'),
					'telefono_rrhh' => $this->input->post('ctelefono_rrhh'),
					'nombre_contabilidad' => $this->input->post('cnombre_contabilidad'),
					'telefono_contabilidad' => $this->input->post('ctelefono_contabilidad'),
					'forma_pago' => $this->input->post('cforma_pago'),
					'cantidad_empleados' => $this->input->post('ccantidad_empleados'),
					'rubro' => $this->input->post('crubro'),
					'nombre_firma' => $this->input->post('cnombre_firma'),
					'posicion_firma' => $this->input->post('cposicion_firma'),
				);
				$this->companias_model->update(array('id' => $this->input->post('cid')), $data);
				//echo json_encode(array("status" => TRUE));
				$this->updateClientCompanias($this->input->post('cid'));
			}
		//} else {
			//echo json_encode(array('permission'=> 'No tienes permiso para editar compañias.'));
		//}
    }

	public function valid_compania() {
		$this->form_validation->set_rules('comentario_compania','comentario','required|trim|min_length[3]|xss_clean');
		$this->form_validation->set_rules('valido_compania','valido','xss_clean');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$valido = null;
			if ($this->input->post('valido_compania') == "" || $this->input->post('valido_compania') == null){
				$valido = "No";
			} else {
				$valido = $this->input->post('valido_compania');
			}

			$data = array(
				'comentario_compania' => $this->input->post('comentario_compania'),
				'validacion_compania' => $valido
			);

			$datos_verificacion = $this->datos_verificacion_model->get_by_clientes_id($this->input->post('clientes_id'));
			$this->datos_verificacion_model->update(array('id' => $datos_verificacion->id), $data);

			if ($valido == "No") {
				echo json_encode(array("permission" => '<span class="label label-danger animated shake">No validado</span>'));
			} else {
				echo json_encode(array("status" => TRUE));
			}
		
		}
	}

    public function cargar_archivo_rp() {

    	$solicitud_id = $this->input->post('solicitud_id');
    	$nombre_archivo = $this->input->post('nombre_archivo_rp');
    	$comentario = $this->input->post('comentario_rp');
    	$valido = $this->input->post('valido_rp');
    	$cliente_id = $this->input->post('clientes_id_rp');

    	if ($comentario == null || $comentario == "") {
    		echo json_encode(array('permission'=> 'Agrege un comentario.'));
    	} else {
			$valido = null;
			if ($this->input->post('valido_rp') == "" || $this->input->post('valido_rp') == null){
				$valido = "No";
			} else {
				$valido = $this->input->post('valido_rp');
			}

			$solicitud = $this->solicitudes_model->get_by_id($solicitud_id);
			$datos_verificacion = $this->datos_verificacion_model->get_by_clientes_id($solicitud->clientes_id);

			if ($datos_verificacion->archivo_rp != null || $datos_verificacion->archivo_rp != "") {
				$file = "files/rp/".$datos_verificacion->archivo_rp;
				if ($file) {
					unlink($file);
				}
			}
			if ($nombre_archivo == null || $nombre_archivo == "") {
				$data = array(
					'archivo_rp' => NULL,
					'comentario_rp' => $comentario,
					'validacion_rp' => $valido
				); 
				$this->datos_verificacion_model->update(array('id' => $datos_verificacion->id), $data);
				if ($valido == "No") {
					echo json_encode(array("test" => TRUE));
				} else {
					echo json_encode(array("status" => TRUE));
				}
			} else {
		        $archivo = 'file';
		        $config['upload_path'] = "files/rp/";
		        $config['file_name'] = $nombre_archivo;
		        $config['allowed_types'] = "*";
		        $config['overwrite'] = FALSE;

		        $this->load->library('upload', $config);
		        
		        if (!$this->upload->do_upload($archivo)) {
		            echo json_encode($this->upload->display_errors());
		            return;
		        }

				$data = $this->upload->data();
				$filename = $data['file_name'];
				$data = array(
					'archivo_rp' => $filename,
					'comentario_rp' => $comentario,
					'validacion_rp' => $valido
				); 
				$this->datos_verificacion_model->update(array('id' => $datos_verificacion->id), $data);
				if ($valido == "No") {
					//echo json_encode(array("permission" => '<span class="label label-danger animated shake">No validado</span>'));
					echo json_encode(array("test" => TRUE));
				} else {
					echo json_encode(array("status" => TRUE));
				}
			}
    	}
    }

    public function update_verificado($id){
		$data = array(
			'usuarios_id' => NULL,
			'fecha_verificacion' => date('Y-m-d H:i:s'),
			'verificador' => $this->session->nombres . ' ' . $this->session->apellidos,
			'estados_id' => 3 // por aprobar
		);
		$this->solicitudes_model->update(array('id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}

    public function update_test($id){
		$request = $this->solicitudes_model->get_by_id($id);
		$data = array(
			'usuarios_id' => NULL,
			'estados_id' => 11 // rechazado
		);
		$update = $this->solicitudes_model->update(array('id' => $request->id), $data);
		echo json_encode(array("status" => TRUE));
	}

    public function anular_solicitud($id){
		$data = array(
			'usuarios_id' => NULL,
			'estados_id' => 12 // anulado
		);
		$update = $this->solicitudes_model->update(array('id' => $id), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function info_prestamo($id){
		$solicitud = $this->solicitudes_model->get_by_id($id);
		$data = $this->clientes_model->get_by_id($solicitud->clientes_id);

		$descuento = $data->cantidad_descuento * 1;
		$hipoteca = 0;
		$capacidadmaxima20 = $data->salario * (20 / 100);
		$capacidadmaxima35 = $data->salario * (35 / 100);
		$capacidadmaxima50 = $data->salario * (50 / 100);

		$prestamomaximo6 = ($capacidadmaxima20 - $descuento) * 3.08;
		$prestamomaximo8 = ($capacidadmaxima20 - $descuento) * 3.556;
		$prestamomaximo12 = ($capacidadmaxima20 - $descuento) * 4.64;

		$prestamomaximo6ch = ($capacidadmaxima35 - ($descuento + $hipoteca)) * 3.08;

		$datos = array(
			"descuento" => $descuento,
			"capacidad_maxima_20" => $capacidadmaxima20,
			"capacidad_maxima_35" => $capacidadmaxima35,
			"capacidad_maxima_50" => $capacidadmaxima50,
			"prestamo_maximo_6" => $prestamomaximo6,
			"prestamo_maximo_8" => $prestamomaximo8,
			"prestamo_maximo_12" => $prestamomaximo12,
			"prestamo_maximo_6_ch" => $prestamomaximo6ch
		);

		echo json_encode($datos);
	}

	public function asignar_cotizacion($id){
		$suma = $this->input->post('suma_prestamo');
		$productos_id = $this->input->post('productos_id');
		$plazo = $this->input->post('plazo');

		$solicitud = $this->solicitudes_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($solicitud->clientes_id);
		$producto = $this->productos_model->get_by_id($productos_id);

		$data = array(
			'cantidad_aprobada' => $suma,
			'cantidad_meses' => $plazo,
			'productos_id' => $productos_id,
			'clientes_id' => $cliente->id,
		);

		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);
		if ($solicitudes_aprobacion) {
			$aprobacion = $this->aprobacion_model->get_by_id($solicitudes_aprobacion->aprobacion_id);
			if ($aprobacion) {
				$update = $this->aprobacion_model->update(array('id' => $aprobacion->id), $data);
				$datos = $this->aprobacion_model->get_by_id($aprobacion->id);		
			}
		} else {
			$insert = $this->aprobacion_model->save($data);
			$data1 = array(
				'solicitudes_id' => $solicitud->id,
				'aprobacion_id' => $insert,
			);
			$this->solicitudes_aprobacion_model->save($data1);
			$datos = $this->aprobacion_model->get_by_id($insert);
		}

		$datos->cantidad_solicitada = $cliente->cantidad_solicitada;
		$datos->salario = $cliente->salario;
		$datos->nombre_producto = $producto->nombre;

		echo json_encode($datos);
	}

	public function info_cotizacion($id){

		$solicitud = $this->solicitudes_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($solicitud->clientes_id);
		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);
		if ($solicitudes_aprobacion) {
			$aprobacion = $this->aprobacion_model->get_by_id($solicitudes_aprobacion->aprobacion_id);
			if ($aprobacion) {
				$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
				$datos = $this->aprobacion_model->get_by_id($aprobacion->id);

				$datos->cliente_id = $cliente->id;
				$datos->cantidad_solicitada = $cliente->cantidad_solicitada;
				$datos->salario = $cliente->salario;
				$datos->nombre_producto = $producto->nombre;
				$datos->nombre_cliente = $cliente->nombre . " " . $cliente->apellido;

				$empresa = $this->companias_model->get_by_id($cliente->companias_id);
				$datos->empresa = $empresa->nombre;
				$datos->direccione = $empresa->direccion;
				$datos->telefonoe = $empresa->telefono;
				$datos->ruce = $empresa->ruc;
				$datos->sitiowebe = $empresa->sitio_web;
				$datos->anoconstitucion = $empresa->ano_constitucion;
				$datos->correoe = $empresa->correo;
				$datos->nombrerrhh = $empresa->nombre_rrhh;
				$datos->telefonorrhh = $empresa->telefono_rrhh;
				$datos->nombrecontabilidad = $empresa->nombre_contabilidad;
				$datos->telefonocontabilidad = $empresa->telefono_contabilidad;
				$datos->formapago = $empresa->forma_pago;
				$datos->cantidadempleados = $empresa->cantidad_empleados;
				$datos->rubroe = $empresa->rubro;
				$datos->nombrefirma = $empresa->nombre_firma;
				$datos->posicionfirma = $empresa->posicion_firma;

				$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses);

				$datos->totalpagar = $cotizacion['totalpagar'];
				$datos->pago_mensual = round($cotizacion['totalpagar'] / $aprobacion->cantidad_meses, 2);
				$datos->pago_quincenal = round($datos->pago_mensual / 2, 2);

				$refinanciamiento = $this->aprobacion_model->get_entregado_by_clientes_id($cliente->id);
				if ($refinanciamiento) {

					$datos1 = $this->aprobacion_model->get_by_id($refinanciamiento->id);
					$producto1 = $this->productos_model->get_by_id($datos1->productos_id);
					$datos1->nombre_producto = $producto1->nombre;
					$cotizacion1 = $this->cotizacion($refinanciamiento->cantidad_aprobada,$refinanciamiento->productos_id,$refinanciamiento->cantidad_meses,$refinanciamiento->fecha_primer_descuento,$refinanciamiento->porcentaje);
					$datos1->totalpagar = $cotizacion1['totalpagar'];
					$datos1->pago_mensual = round($cotizacion1['totalpagar'] / $refinanciamiento->cantidad_meses, 2);
					$datos1->pago_quincenal = round($datos1->pago_mensual / 2, 2);

					$cantcobros = $this->cobros_model->cant_cobros_by_aprobacion_id($datos1->id);
					$cotizacion1['cobros'] = $cantcobros;

/*					$cobrado = array();
					for ($i=0; $i < $cantcobros; $i++) { 
						if (isset($cotizacion1['data'][$i]['total'])) {
							$cobrado[] = $cotizacion1['data'][$i]['total'];
						}						
					}*/

/*					$faltante = array();
					$cantfaltante = 0;
					for ($j=$cantcobros; $j < $refinanciamiento->cantidad_meses; $j++) { 
						$faltante[] = $cotizacion1['data'][$j]['total'];
						$cantfaltante++;
					}

					$cotizacion1['cantfaltante'] = $cantfaltante;

					$entregar = $aprobacion->cantidad_aprobada - array_sum($faltante);*/

					$cobrado = $this->cobros_model->suma_cobros_by_aprobacion_id($datos1->id);
					$faltante = $cotizacion1['totalcobrar'] - $cobrado;
					if ($faltante > 0) {
						$entregar = $aprobacion->cantidad_aprobada - $faltante;
					} else {
						$entregar = $aprobacion->cantidad_aprobada;
					}

					$info = array(
						'obligaciontotal' => $cotizacion1['totalcobrar'],
						//'cobrado' => array_sum($cobrado),
						'cobrado' => $cobrado,
						//'faltante' => array_sum($faltante),
						'faltante' => $faltante,
						//'sumcofa' => array_sum($cobrado) + array_sum($faltante),
						'sumcofa' => $cobrado + $faltante,
						'aprobado' => $aprobacion->cantidad_aprobada,
						'entregar' => $entregar
					);

					echo json_encode(array("datos" => $datos, "refinanciamiento" => $datos1, "cobro" => $cotizacion1, "info" => $info));
				} else {
					echo json_encode(array("datos" => $datos));
				}				
			}
		} else {
			echo json_encode(array("cotize" => 'Cotize el préstamo a aprobar.'));
		}
	}

	public function aprobar($id){
		$solicitud = $this->solicitudes_model->get_by_id($id);
		$cliente = $this->clientes_model->get_by_id($solicitud->clientes_id);
		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);
		if ($solicitudes_aprobacion) {
			$aprobacion = $this->aprobacion_model->get_by_id($solicitudes_aprobacion->aprobacion_id);
			if ($aprobacion) {
				$archivos = $this->aprobacion_model->get_archivos_by_clientes_id($cliente->id);

				$tipo_archivos = $this->clientes_model->get_tipos_archivos();

				$tar = array();
				if ($tipo_archivos) {
					foreach ($tipo_archivos as $row2) {
						$tar[] = $row2['tipo'];
					}
				}

				$ar = array();
				if ($archivos) {
					foreach ($archivos as $row) {
						$ar[] = $row->tipos_archivos_id;
						$tipo = $this->clientes_model->get_tipos_archivos_by_id($row->tipos_archivos_id);
						if (($clave = array_search($tipo->tipo, $tar)) !== false) {
						    unset($tar[$clave]);
						}
					}
				}

				$tipoa = '';
				foreach ($tar as $ta) {
					$tipoa .= $ta . '. ';
				}


				if (count($ar) < 5) {
					$num = 5 - count($ar);
					echo json_encode(array('permission'=> 'Faltan ' . $num . ' archivos por cargar. ' . $tipoa));
				} else {
					$data = array(
						'usuarios_id' => NULL,
						'fecha_aprobado' => date('Y-m-d H:i:s'),
						'aprobador' => $this->session->nombres . ' ' . $this->session->apellidos,
						'estados_id' => 4
					);
					$this->solicitudes_model->update(array('id' => $solicitud->id), $data);
					echo json_encode(array("status" => TRUE));
				}
			}
		} else {
			echo json_encode(array("cotizar" => 'Debes cotizar el préstamo a aprobar.'));
		}
	}

	function dias_transcurridos($fecha_i,$fecha_f)
	{
		$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
		$dias 	= abs($dias); $dias = floor($dias);		
		return $dias;
	}

	public function fecha_mayor_menor($fecha){

		$fecha_actual = date("d-m-Y");
		$partes_fecha_actual = explode("-", $fecha_actual);
		$dia_actual = $partes_fecha_actual[0];
		$mes_actual = $partes_fecha_actual[1];
		$anio_actual = $partes_fecha_actual[2];

		$partes_fecha_recibida = explode("/", $fecha);
		$dia_recibido = $partes_fecha_recibida[0];
		$mes_recibido = $partes_fecha_recibida[1];
		$anio_recibido = $partes_fecha_recibida[2];

/*		if ($anio_recibido < $anio_actual) {
			return FALSE;
		} else {
			if ($mes_recibido < $mes_actual) {
				return FALSE;
			} else {
				if ($mes_recibido < $mes_actual && $dia_recibido <= $dia_actual) {
					return FALSE;
				} else {*/
					if ($dia_recibido <= 15) {
						return "100%";
					} else {
						return "50%";
					}
				//}
			//}
		//}
		
	}

	public function fecha_porcentaje(){
		$fecha_primer_descuento = $this->input->post('fecha');
		echo json_encode($this->fecha_mayor_menor($fecha_primer_descuento));
	}

	public function entregar(){

		$this->form_validation->set_rules('fecha_primer_descuento','fecha del primer descuento','required|trim|xss_clean');
		$this->form_validation->set_rules('porcentaje','porcentaje','required|trim|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {

			$solicitud = $this->solicitudes_model->get_by_id($this->input->post('solicitud_id'));
			$refinanciamiento = $this->aprobacion_model->get_entregado_by_clientes_id($solicitud->clientes_id);
			if ($refinanciamiento) {

				$datos1 = $this->aprobacion_model->get_by_id($refinanciamiento->id);
				$cantcobros = $this->cobros_model->get_by_aprobacion_id($datos1->id);
				$cotizacion1 = $this->cotizacion($datos1->cantidad_aprobada,$datos1->productos_id,$datos1->cantidad_meses,$datos1->fecha_primer_descuento,$datos1->porcentaje);

				$fecha = $datos1->fecha_primer_descuento;

				$porcentaje = $datos1->porcentaje;
				if ($porcentaje == 50) {
					$fec = date("d-m-Y", strtotime("$fecha + $cantcobros months"));
					$cobrosfaltantes = ($datos1->cantidad_meses + 1) - $cantcobros;
				} else {
					$fec = date("d-m-Y", strtotime("$fecha + $cantcobros months"));
					$cobrosfaltantes = $datos1->cantidad_meses - $cantcobros;
				}

				$fechas = array();
				for ($i=0; $i < $cobrosfaltantes; $i++) {
					$fechas[] = date("d-m-Y", strtotime("$fec + $i months"));
				}

				$nombre_archivo = $this->input->post('nombre_archivo');

				if ($nombre_archivo == null || $nombre_archivo == "") {
			        $filename = NULL;
				} else {
			        $archivo = 'file';
			        $config['upload_path'] = "files/pagos/";
			        $config['file_name'] = $nombre_archivo;
			        $config['allowed_types'] = "*";
			        $config['overwrite'] = FALSE;
			        $this->load->library('upload', $config);	        
			        if (!$this->upload->do_upload($archivo)) {
			            echo json_encode($this->upload->display_errors());
			            return;
			        }
					$data = $this->upload->data();
					$filename = $data['file_name'];
				}

				$array_mora = array();
				$array = array();
				for ($j=0; $j < $cobrosfaltantes; $j++) {

					$comparar = $this->comparar_fechas(date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_pago')))),date('Y-m-d', strtotime(str_replace("/","-",$fechas[$j]))));
					$calculos = $cotizacion1;

					//$dat = null;
					if ($comparar == "mayor") {
						$cob = $calculos['data'][$j+$cantcobros]['total'];
						$porcentajemora = 2/100;
						$mora = $cob * $porcentajemora;
						$array_mora[] = $mora;
						$total = $cob + $mora;
						//$dat = $total;
						$array[] = $total;
					} else {
						//$dat = $calculos['data'][$j+$cantcobros]['total'];
						$array[] = $calculos['data'][$j+$cantcobros]['total'];
					}

/*					$datos = array(
						"fecha_correspondiente" => date('Y-m-d', strtotime(str_replace("/","-",$fechas[$j]))),
						'fecha_pago' => date('Y-m-d'),
						'forma_pago' => 'Refinanciamiento',
						'monto' => $dat,
						'pago_adelantado' => 'Si',
						"archivo" => $filename,
						'aprobacion_id' => $datos1->id,
						'usuarios_id' => $this->session->userdata('id_user')
					);
					$insert = $this->cobros_model->save($datos);*/
				}

				$datos = array(
					"fecha_correspondiente" => date('Y-m-d', strtotime(str_replace("/","-",$fechas[count($fechas)-1]))),
					'fecha_pago' => date('Y-m-d'),
					'forma_pago' => 'Refinanciamiento',
					//'interes_mora' => array_sum($array_mora),
					'monto' => array_sum($array),
					'pago_adelantado' => 'Si',
					"archivo" => $filename,
					'aprobacion_id' => $datos1->id,
					'usuarios_id' => $this->session->userdata('id_user')
				);
				$insert = $this->cobros_model->save($datos);

				$this->aprobacion_model->update(array('id' => $datos1->id), array('cobrado' => 1));
				
			}

			$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($this->input->post('solicitud_id'));
			$aprobacion = $this->aprobacion_model->get_by_id($solicitudes_aprobacion->aprobacion_id);

			if ($this->input->post('nombre_archivo') != NULL || $this->input->post('nombre_archivo') != "") {
				$nombre_archivo = $this->input->post('nombre_archivo');
		        $archivo = 'file';
		        $config['upload_path'] = "files/entregados/";
		        $config['file_name'] = $nombre_archivo;
		        $config['allowed_types'] = "*";
		        $config['overwrite'] = FALSE;

		        $this->load->library('upload', $config);
		        
		        if (!$this->upload->do_upload($archivo)) {
		            echo json_encode($this->upload->display_errors());
		            return;
		        }

				$data = $this->upload->data();
				$filename = $data['file_name'];

			} else {
				$filename = NULL;
			}

			if ($this->input->post('fecha_entrega') != NULL || $this->input->post('fecha_entrega') != "") {
				$datexplode = explode(" ", $this->input->post('fecha_entrega'));
				$date = date('Y-m-d', strtotime(str_replace("/","-",$datexplode[0])));
				$hour = $datexplode[1] . " " . $datexplode[2];
				$time = date("H:i", strtotime($hour));
				$datef = $date . " " . $time;
				$fecha_entrega = date($datef);
			} else {
				$fecha_entrega = date("Y-m-d H:i:s");
			}

			$data1 = array(
				'numero_cheque' => $this->input->post('numero_cheque'),
				'fecha_primer_descuento' => date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_primer_descuento')))),
				'porcentaje' => $this->input->post('porcentaje'),
				'fecha_entrega' => $fecha_entrega,
				'archivo' => $filename,
				'entregado' => 1,
			);
			$this->aprobacion_model->update(array('id' => $aprobacion->id), $data1);

			$data2 = array(
				'usuarios_id' => NULL,
				'estados_id' => 7
			);
			$this->solicitudes_model->update(array('id' => $this->input->post('solicitud_id')), $data2);
			echo json_encode(array("status" => TRUE));
		}

	}


	function comparar_fechas($fecha_i,$fecha_f)
	{

		$fecha = null;
		if (strtotime($fecha_i) > strtotime($fecha_f)) {
			$fecha = "mayor";
		} elseif (strtotime($fecha_i) < strtotime($fecha_f)) {
			$fecha = "menor";
		} else {
			$fecha = "igual";
		}

		return $fecha;
	}

	function date_valid($date)
	{
		if ($date != "" || $date != NULL) {
			$parts = explode("/", $date);
			if (count($parts) == 3) {      
				if (checkdate($parts[1], $parts[0], $parts[2]))
				{
					return TRUE;
				}
			}
			return FALSE;
		}
	}

	public function fechas_firmas(){

		$this->form_validation->set_rules('fecha_autorizacion','fecha de autorización','callback_date_valid'); 
		$this->form_validation->set_rules('fecha_salida','fecha de salida','callback_date_valid');
		$this->form_validation->set_rules('fecha_recibida_empresa','fecha recibida de la empresa','callback_date_valid');
		$this->form_validation->set_rules('fecha_recibida_vuelta','fecha recibida de vuelta','callback_date_valid');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => '<span class="label label-danger animated shake">Los formatos de fechas deben ser DD/MM/YYYY</span>'));
		} else {

			$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($this->input->post('solicitud_id'));
			$aprobacion = $this->aprobacion_model->get_by_id($solicitudes_aprobacion->aprobacion_id);

			$fecha_autorizacion = $this->input->post('fecha_autorizacion');
			$fecha_salida = $this->input->post('fecha_salida');
			$fecha_recibida_empresa = $this->input->post('fecha_recibida_empresa');
			$fecha_recibida_vuelta = $this->input->post('fecha_recibida_vuelta');

			$data = array(
				'aprobacion_id' => $aprobacion->id,
			);

			if ($fecha_autorizacion != "" || $fecha_autorizacion != NULL) {
				$data['fecha_autorizacion'] = date('Y-m-d', strtotime(str_replace("/","-",$fecha_autorizacion)));
			}
			if ($fecha_salida != "" || $fecha_salida != NULL) {
				$data['fecha_salida'] = date('Y-m-d', strtotime(str_replace("/","-",$fecha_salida)));
			}
			if ($fecha_recibida_empresa != "" || $fecha_recibida_empresa != NULL) {
				$data['fecha_recibida_empresa'] = date('Y-m-d', strtotime(str_replace("/","-",$fecha_recibida_empresa)));
			}
			if ($fecha_recibida_vuelta != "" || $fecha_recibida_vuelta != NULL) {
				$data['fecha_recibida_vuelta'] = date('Y-m-d', strtotime(str_replace("/","-",$fecha_recibida_vuelta)));
			}

			$firmas = $this->fechas_firmas_model->get_by_aprobacion_id($aprobacion->id);
			if ($firmas) {
				$this->fechas_firmas_model->update(array('id' => $firmas->id), $data);
			} else {
				$insert = $this->fechas_firmas_model->save($data);
			}
			echo json_encode(array('status' => TRUE));
		}		
	}

	public function getFechas($id){

		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($id);
		$aprobacion = $this->aprobacion_model->get_by_id($solicitudes_aprobacion->aprobacion_id);
		$fechas = $this->fechas_firmas_model->get_by_aprobacion_id($aprobacion->id);
		if ($fechas) {
			$data = array(
				'id' => $fechas->id,
			);
			if ($fechas->fecha_autorizacion != "" || $fechas->fecha_autorizacion != NULL) {
				$data['fecha_autorizacion'] = date('d/m/Y', strtotime(str_replace("/","-",$fechas->fecha_autorizacion)));
			}
			if ($fechas->fecha_salida != "" || $fechas->fecha_salida != NULL) {
				$data['fecha_salida'] = date('d/m/Y', strtotime(str_replace("/","-",$fechas->fecha_salida)));
			}
			if ($fechas->fecha_recibida_empresa != "" || $fechas->fecha_recibida_empresa != NULL) {
				$data['fecha_recibida_empresa'] = date('d/m/Y', strtotime(str_replace("/","-",$fechas->fecha_recibida_empresa)));
			}
			if ($fechas->fecha_recibida_vuelta != "" || $fechas->fecha_recibida_vuelta != NULL) {
				$data['fecha_recibida_vuelta'] = date('d/m/Y', strtotime(str_replace("/","-",$fechas->fecha_recibida_vuelta)));
			}
		} else {
			$data = array(
				'id' => NULL,
				'fecha_autorizacion' => NULL,
				'fecha_salida' => NULL,
				'fecha_recibida_empresa' => NULL,
				'fecha_recibida_vuelta' => NULL
			);	
		}
		echo json_encode($data);
	}

	public function firmas_procesadas($id){

		$solicitudes_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($id);
		$aprobacion = $this->aprobacion_model->get_by_id($solicitudes_aprobacion->aprobacion_id);
		$fechas = $this->fechas_firmas_model->get_by_aprobacion_id($aprobacion->id);
		if ($fechas) {
			if (($fechas->fecha_autorizacion != "" || $fechas->fecha_autorizacion != NULL) && ($fechas->fecha_salida != "" || $fechas->fecha_salida != NULL) && ($fechas->fecha_recibida_empresa != "" || $fechas->fecha_recibida_empresa != NULL) && ($fechas->fecha_recibida_vuelta != "" || $fechas->fecha_recibida_vuelta != NULL)) {

				$data = array(
					'usuarios_id' => NULL,
					'estados_id' => 6,
					'fecha_firma' => date('Y-m-d H:i:s'),
					'firma' => $this->session->nombres . ' ' . $this->session->apellidos,
				);
				$this->solicitudes_model->update(array('id' => $id), $data);
				echo json_encode(array("status" => TRUE));

			} else {
				echo json_encode(array('validation' => '<span class="label label-danger animated shake">Faltan fechas por registrar</span>'));
			}			
		} else {
			echo json_encode(array('validation' => '<span class="label label-danger animated shake">Faltan fechas por registrar</span>'));
		}
	}

	public function cotizacion($suma,$productos_id,$plazo,$fecha = NULL,$porcentaje = NULL)	{ // en reporte, cotizacion

		if ($fecha != NULL) {
			$fecha = date('Y-m-d', strtotime(str_replace("/","-",$fecha)));
		} else {
			$fecha = date("Y-m-d");
		}

		$tasa = 0.00;
		$cargo = 0.00;

		if ($productos_id) {
			$products = $this->cotizacion_model->get_products_by_id($productos_id);
			foreach ($products as $row) {
				$tasa = $row->tasa_interes_mensual;
				$porccargo = $row->cargo_administrativo;
				$porcgasto = empty($row->gastos_notariales) ? 0 : $row->gastos_notariales;
				$porccomision = empty($row->comision) ? 0 : $row->comision;
				$cargo = $porccargo + $porcgasto + $porccomision;
			}
		}

		$suma = round($suma,2);
		$tasa = round($tasa,2);
		$cargo = round($cargo,2);

		$interes = $tasa / 100;

		$p = $suma;
		$x = $interes;
		$n = $plazo;

		$m = $p * ( $x / ( 1 - ( ( 1 + $x ) ** - $n ) ) );

		$cuotaperiodica = $m;
		$cuotaperiodica = round($cuotaperiodica,2);

		$cargototal = $cargo * ($suma / 100);
		$cargototal = round($cargototal,2);

		$solocargo = $porccargo * ($suma / 100);
		$solocomision = $porccomision * ($suma / 100);
		$sologasto = $porcgasto * ($suma / 100);

		$solocargo = round($solocargo,2);
		$solocomision = round($solocomision,2);
		$sologasto = round($sologasto,2);

		$pagado = array();	
		$interesasignado = array();
		$cargototalarray = array();
		$tasaarray = array();
		$total = array();

		$sum = $suma;
		for ($i=0; $i<$plazo; $i++) {
			$interesi = $sum * $interes;
			$interesi = round($interesi,2);
			$valori = $cuotaperiodica - $interesi;
			$valori = round($valori,2);
			$restoi = $sum - $valori;
			$restoi = round($restoi,2);
			$sum = $restoi;
			$pagado[$i] = $valori;
			$interesasignado[$i] = $interesi;
			$cargototalarray[$i] = $cargototal;
			$tasaarray[$i] = $tasa;
		}

		$totalintereses = array_sum($interesasignado);
		$totalintereses = round($totalintereses,2);
		$totalcargo = $cargototal * $plazo;
		$totalcargo = round($totalcargo,2);

		$totalsuma = array();
		$intereses = $totalcargo + array_sum($interesasignado);
		$intereses = round($intereses,2);
		$totalcobrar = array();
		$diferenciasuma = $suma - (array_sum($pagado));
		$diferenciasuma = round($diferenciasuma,2);

		if($diferenciasuma < 0){
			$dec = explode("-0.", $diferenciasuma);
			$num = $dec[1] * 1;
			$m = 0;
			for ($i=0; $i < $num; $i++) {
				$m++;
				$pagado[$plazo-$m] = $pagado[$plazo-$m] - 0.01;
			}
		} elseif($diferenciasuma > 0){
			$dec = explode("0.", $diferenciasuma);
			$num = $dec[1] * 1;
			for ($i=0; $i < $num; $i++) {
				$pagado[$i] = $pagado[$i] + 0.01;
			}
		}

		if ($porcentaje == 50) {

			$pagado[0] = $pagado[0] / 2;
			$pagado[$plazo] = $pagado[0];
			$interesasignado[0] = $interesasignado[0] / 2;
			$interesasignado[$plazo] = $interesasignado[0];
			$tasaarray[0] = $tasaarray[0] / 2;
			$tasaarray[$plazo] = $tasaarray[0];
			$cargototalarray[0] = $cargototalarray[0] / 2;
			$cargototalarray[$plazo] = $cargototalarray[0];

			$data = array();
			$n = -1;
			$num = 0;
			for ($i=0; $i<=$plazo; $i++){
				$n++;
				$num++;
				$fe = date("d-m-Y", strtotime("$fecha + $n months"));
				$data[] = array(
					'num' => $num,
					'fecha' => $fe,
					'principal' => $pagado[$i],
					'interes' => $interesasignado[$i],
					'tasa' => $tasaarray[$i],
					'cargo' => $cargototalarray[$i],
					'total' => $pagado[$i] + $interesasignado[$i] + $cargototalarray[$i]
				);
				$totalsuma[] = $pagado[$i];
				$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototalarray[$i];
			}
		} else {
			$data = array();
			$n = -1;
			$num = 0;
			for ($i=0; $i<$plazo; $i++){
				$n++;
				$num++;
				$fe = date("d-m-Y", strtotime("$fecha + $n months"));
				$data[] = array(
					'num' => $num,
					'fecha' => $fe,
					'principal' => $pagado[$i],
					'interes' => $interesasignado[$i],
					'tasa' => $tasa*1,
					'cargo' => $cargototal,
					'total' => $pagado[$i] + $interesasignado[$i] + $cargototal
				);
				$totalsuma[] = $pagado[$i];
				$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototal;
			}
		}

		$totalprincipal = array_sum($pagado);
		$totalintereses = array_sum($interesasignado);
		$totalcargo = $cargototal * $plazo;
		
		$output = array(
			"data" => $data,
			"suma" => $suma * 1,
			"intereses" => $intereses,
			"totalpagar" => array_sum($totalsuma) + $intereses,
			"totalprincipal" => $totalprincipal,
			"totalcargo" => $totalcargo,
			"totalintereses" => $totalintereses,
			"totalcobrar" => array_sum($totalcobrar),
			"plazo" => $plazo,
			"solocargo"		 => $solocargo,
			"solocomision"	 => $solocomision,
			"sologasto"		 => $sologasto,
		);

		return $output;
	}

}