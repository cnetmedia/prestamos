<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Web_clientes extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('clientes_model','solicitudes_model','companias_model','documentos_solicitud_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('download', 'file', 'url', 'html', 'form'));
		$this->load->database('default');

		header('Access-Control-Allow-Origin: *');

	}

	function check_default($post_string) {
		return $post_string == '0' ? FALSE : TRUE;
	}


    function myGetType($var)
    {
        if (is_array($var)) return "array";
        if (is_bool($var)) return "boolean";
        if (is_float($var)) return "float";
        if (is_int($var)) return "integer";
        if (is_null($var)) return "NULL";
        if (is_numeric($var)) return "numeric";
        if (is_object($var)) return "object";
        if (is_resource($var)) return "resource";
        if (is_string($var)) return "string";
        return "unknown type";
    }

	public function add_client() {
		$this->form_validation->set_rules('first_name','nombre','required|trim|xss_clean');
		$this->form_validation->set_rules('last_name','apellido','required|trim|xss_clean');
		$this->form_validation->set_rules('00N3600000FKIrK','cédula','required|trim|xss_clean');
		$this->form_validation->set_rules('email','email','required|trim|xss_clean');
		$this->form_validation->set_rules('00N3600000FKLgF','cantidad solicitada','required|trim|xss_clean');
		$this->form_validation->set_rules('00N3600000FKJ49','salario','required|trim|xss_clean');
		$this->form_validation->set_rules('00N3600000FKJ9T','tiempo laborando','required|trim|xss_clean');
		$this->form_validation->set_rules('00N3600000FKJwQ','descuento','required|trim|xss_clean');
		$this->form_validation->set_rules('cargo','cargo que ocupa','required|trim|xss_clean');
		$this->form_validation->set_rules('00N3600000FKJ4E','empresa','required|trim|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			$validationURL = $this->input->post('validationURL');
			if (isset($validationURL)) {
				redirect($validationURL);
			} else {
				echo validation_errors();
			}			
		} else {

			$actualURL = $this->input->post('00N3600000FKIil');
			$retURL = $this->input->post('retURL');

			$cedula = $this->input->post('00N3600000FKIrK');

			$variable = $this->myGetType($cedula);
			$cedula = $this->valida_cedula($cedula);
			
			$verify_cedula = $this->clientes_model->get_by_cedula($cedula);
			if ($verify_cedula) {
				//echo var_dump('Ya aplico por un prestamos, por favor espere que sea procesado.');
				redirect($retURL);
			} else {
				$data = array(
					'nombre' => $this->input->post('first_name'),
					'apellido' => $this->input->post('last_name'),
					'cedula' => $cedula,
					'telefono' => $this->input->post('phone'),
					'correo' => $this->input->post('email'),
					'cantidad_solicitada' => $this->input->post('00N3600000FKLgF'),
					'fuente_cliente' => $actualURL,
					'salario' => $this->input->post('00N3600000FKJ49'),
					'tiempo_laborando' => $this->input->post('00N3600000FKJ9T'),
					'posicion_trabajo' => $this->input->post('cargo'),
					'usuarios_id' => 1,
					'fecha_creacion' =>  date('Y-m-d H:i:s'),
                    'si_app_solicitud' => 1
				);

				if ($this->input->post('00N3600000FKJwQ') == 'Yes') {
					$data['descuento'] = 'Si';
					$data['cantidad_descuento'] = $this->input->post('00N3600000FKK9K');
				} else {
					$data['descuento'] = 'No';
				}

				$insert = $this->clientes_model->save($data);

				if ($insert) {

					$data1 = array(
						'clientes_id' => $insert,
						'fecha_creacion' =>  date('Y-m-d H:i:s'),
						'estados_id' => 1,
                        'si_app_solicitud' => 1
					);
					$insert1 = $this->solicitudes_model->save($data1);

					if ($insert1) {

                        $config = array(
                            'upload_path' => './files/',
                            'overwrite' => false,
                            'max_filename' => 300,
                            'encrypt_name' => true,
                            'remove_spaces' => true,
                            'allowed_types' => 'png|jpg|jpeg',
                            'max_size' => 204800,
                            'xss_clean' => true,
                        );

                        $this->load->library('upload', $config);

						$cedula = $this->getCedula($insert);
						if ($cedula) {
                        	$this->clientes_model->save_archivos($cedula);
						}

						$social = $this->getSeguro($insert);
						if ($social) {
                        	$this->clientes_model->save_archivos($social);
						}

						$trabajo = $this->getTrabajo($insert);
						if ($trabajo) {
							$this->clientes_model->save_archivos($trabajo);
						}
						
                    }

				}

				redirect($retURL);
			}
		}
	}

	public function add_potencial_client() {
		$this->form_validation->set_rules('nombre','Nombre','required|trim|xss_clean');
		$this->form_validation->set_rules('apellido','Apellido','required|trim|xss_clean');
		$this->form_validation->set_rules('cedula','Cédula','required|trim|xss_clean');
		$this->form_validation->set_rules('telefono','Teléfono','required|trim|xss_clean');
		$this->form_validation->set_rules('email','Email','required|trim|xss_clean');
		$this->form_validation->set_rules('direccion','Dirección','required|trim|xss_clean');
		$this->form_validation->set_rules('si_labora','Labora actualmente','required|trim|xss_clean');
		$this->form_validation->set_rules('empresa','Empresa','required|trim|xss_clean');
		$this->form_validation->set_rules('tiempo_laborando','Tiempo laborando','required|trim|xss_clean');
		$this->form_validation->set_rules('salario','Salario','required|trim|xss_clean');
		$this->form_validation->set_rules('cargo','cargo que ocupa','required|trim|xss_clean');
		$this->form_validation->set_rules('si_asiste','Asistirá a la feria','required|trim|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			$data_response['errors'] = validation_errors();
			$data_response['success'] = false;
			echo json_encode($data_response);
			exit();
		} 

		$cedula = $this->input->post('cedula');
		$cedula = $this->valida_cedula($cedula);			
		$verify_cedula = $this->clientes_model->get_by_cedula($cedula);

		if ($verify_cedula) {
			$data_response['errors'] = 'Cliente ya existe en base de datos';
			$data_response['success'] = false;
			echo json_encode($data_response);
			exit();
		} 

		$data = array(
			'nombre' => $this->input->post('nombre'),
			'apellido' => $this->input->post('apellido'),
			'cedula' => $cedula,
			'telefono' => $this->input->post('telefono'),
			'correo' => $this->input->post('email'),
			'direccion' => $this->input->post('direccion'),
			'fuente_cliente' => $this->input->post('fuente_cliente'),
			'si_labora' => $this->input->post('si_labora'),
			'salario' => $this->input->post('salario'),
			'tiempo_laborando' => $this->input->post('tiempo_laborando'),
			'posicion_trabajo' => $this->input->post('cargo'),
			//'companias_id' => $companias_id,
			'usuarios_id' => 1,
			'estados_cobro_id' => 13,
			'si_asiste' => $this->input->post('si_asiste'),
			'fecha_creacion' =>  date('Y-m-d H:i:s')
		);


		$insert = $this->clientes_model->save($data);

		if ($insert) {
			$data_response['msj'] = 'Cliente potencial registrado exitosamente';
			$data_response['success'] = true;
			echo json_encode($data_response);
			exit();
		} 		
	}

	private function valida_cedula($val){
		$patrones = $sustituciones = array();
		$patrones[0] = '/-/';
		$patrones[1] = '/\s/';
		$sustituciones[0] = '';
		$sustituciones[1] = '';

		$val = preg_replace($patrones, $sustituciones, $val);

		if(strlen($val) < 10){
			if(is_numeric($val[0])){
				$val = str_pad($val, 10, "0", STR_PAD_LEFT);				
			}else{
				$aux = substr($val, 1);
				$aux = str_pad($aux, 9, "0", STR_PAD_LEFT);				
				$val = $val[0].$aux;
			}
		}

		if(!preg_match("/^[a-zA-Z0-9]{1}[0-9]{9}$/", $val)){
			return "99-9999-9999";
		}

		$cedula = "";
		$total =strlen($val);
		for ($i=0; $i < $total; $i++) { 
			$cedula .= $val[$i];
			if($i == 1 || $i == 5){
				$cedula .= "-";
			}
		}

		return $cedula;
	}	

	public function register_solicitud() {
		header('Content-type: application/json');
    	$response = array();
		$allInputs = json_decode(trim($this->input->raw_input_stream),true);
		$datosCliente = $allInputs['datosCliente'];
		$datosSolicitud = $allInputs['datosSolicitud'];
		$imagenes = $allInputs['imagenes'];
		$actualURL = $datosSolicitud['url'];	
		$cedula = $datosCliente['cedula'];
		$variable = $this->myGetType($cedula);

		$cedula = $this->valida_cedula($cedula);
		
		$verify_cedula = $this->clientes_model->get_by_cedula($cedula);
		if ($verify_cedula) {
			//echo var_dump('Ya aplico por un prestamos, por favor espere que sea procesado.');
			$response = array(
				'status' => 'Error',
		        'message' => 'Ya posee una solicitud. Espere una respuesta',
		    );
		} else {
			$data = array(
				'nombre' => $datosCliente['nombres'],
				'apellido' => $datosCliente['apellidos'],
				'cedula' => $cedula,
				'telefono' => $datosCliente['telefono'],
				'correo' => $datosCliente['email'],
				'cantidad_solicitada' => $datosCliente['cantidad'],
				'fuente_cliente' => $actualURL,
				'salario' => $datosCliente['salario'],
				'tiempo_laborando' => $datosCliente['tiempoTrabajo'],
				'posicion_trabajo' => $datosCliente['cargo'],
				'usuarios_id' => 1,
				'fecha_creacion' =>  date('Y-m-d H:i:s'),
				'datos_solicitud' => json_encode($datosSolicitud),
				'si_app_solicitud' => 1
			);

			if ($datosCliente['siDescuento'] == 'Yes') {
				$data['descuento'] = 'Si';
				$data['cantidad_descuento'] = $datosCliente['descuento'];
			} else {
				$data['descuento'] = 'No';
			}
			
			$insert = $this->clientes_model->save($data);
			if ($insert) {
				$data1 = array(
					'clientes_id' => $insert,
					'fecha_creacion' =>  date('Y-m-d H:i:s'),
					'estados_id' => 1,
					'si_app_solicitud' => 1
				);
				$insert1 = $this->solicitudes_model->save($data1);

				if($insert1){

					if( !empty($imagenes['cedula']['foto']) || !empty($imagenes['pasaporte']['foto']) || !empty($imagenes['seguro']['foto'])
					 || !empty($imagenes['trabajo']['foto']) || !empty($imagenes['otro']['foto'])){

						$cedula = $this->handlerImage($imagenes['cedula']['foto']);

						if (null !== $cedula) {
						    $payload = $this->buildPayload($cedula, 'Cedula', 2, $insert1);
                            $this->clientes_model->save_archivos($payload);
                        }

						$seguro = $this->handlerImage($imagenes['seguro']['foto']);

						if (null !== $seguro) {
						    $payload = $this->buildPayload($seguro, 'Ficha css', 1, $insert1);
                            $this->clientes_model->save_archivos($payload);
                        }

						$trabajo = $this->handlerImage($imagenes['trabajo']['foto']);

						if (null !== $trabajo) {
						    $payload = $this->buildPayload($trabajo, 'Carta de trabajo', 4, $insert1);
                            $this->clientes_model->save_archivos($payload);
                        }
					}
				}
			}

		    $response = array(
		    	'status' => 'Ok',
		        'message' => 'Solicitud procesada exitosamente',
		    );
		}

		echo json_encode($response);	
	}

	public function get_solicitudes(){
		header('Content-type: application/json');
		header("Access-Control-Allow-Origin: *");

		$user = $this->input->get('usuario', FALSE);

		$lista = $this->solicitudes_model->get_all_solicitudes_app($user);
		$data = array();
		foreach ($lista as $cliente) {
			$row = array();

			$row['datosCliente'] = array();
			$row['datosCliente']['cliente_id'] = $cliente->id_cliente;
			$row['datosCliente']['solicitud_id'] = $cliente->id_solicitud;
			$row['datosCliente']['nombres'] = $cliente->nombre;
			$row['datosCliente']['apellidos'] = $cliente->apellido;
			$row['datosCliente']['cedula'] = $cliente->cedula;
			$row['datosCliente']['telefono'] = $cliente->telefono;
			$row['datosCliente']['email'] = $cliente->correo;
			$row['datosCliente']['lugarTrabajo'] = "";
			$row['datosCliente']['cargo'] = $cliente->posicion_trabajo;
			$row['datosCliente']['tiempoTrabajo'] = $cliente->tiempo_laborando;
			$row['datosCliente']['salario'] = $cliente->salario;
			$row['datosCliente']['cantidad'] = $cliente->cantidad_solicitada;
			$row['datosCliente']['siDescuento'] = $cliente->descuento;
			$row['datosCliente']['descuento'] = $cliente->cantidad_descuento;
			
			$row['imagenes'] = array();
			$row['imagenes']['cedula'] = array();
			if(!empty(json_decode($cliente->doc_cedula))){
				$row['imagenes']['cedula'] = json_decode($cliente->doc_cedula);
			}else{
				$row['imagenes']['cedula']['foto'] = "";
				$row['imagenes']['cedula']['comentario'] = "";
			}
			$row['imagenes']['pasaporte'] = array();
			if(!empty(json_decode($cliente->pasaporte))){
				$row['imagenes']['pasaporte'] = json_decode($cliente->pasaporte);
			}else{
				$row['imagenes']['pasaporte']['foto'] = "";
				$row['imagenes']['pasaporte']['comentario'] = "";
			}

			$row['imagenes']['seguro'] = array();
			if(!empty(json_decode($cliente->seguro))){
				$row['imagenes']['seguro'] = json_decode($cliente->seguro);
			}else{
				$row['imagenes']['seguro']['foto'] = "";
				$row['imagenes']['seguro']['comentario'] = "";
			}

			$row['imagenes']['trabajo'] = array();
			if(!empty(json_decode($cliente->trabajo))){
				$row['imagenes']['trabajo'] = json_decode($cliente->trabajo);
			}else{
				$row['imagenes']['trabajo']['foto'] = "";
				$row['imagenes']['trabajo']['comentario'] = "";
			}

			$row['imagenes']['otro'] = array();
			if(!empty(json_decode($cliente->otro))){
				$row['imagenes']['otro'] = json_decode($cliente->otro);
			}else{
				$row['imagenes']['otro']['foto'] = "";
				$row['imagenes']['otro']['comentario'] = "";
			}

			$row['datosSolicitud'] = array();
			$row['datosSolicitud'] = json_decode($cliente->datos_solicitud);

			array_push($data, $row);
		}

		$response = array(
			"status" => 'Ok',
			"data" => $data,
		);
		echo json_encode($response);
	}

    /**
     * @param $cliente_id
     * @return array|bool
     */
    protected function getCedula($cliente_id)
    {
        if (!$this->upload->do_upload('document_file')) {
            $error = array('error' => $this->upload->display_errors());
			$data = false;
        } else {
            $file = $this->upload->data();
            $data = array(
                'nombre' => $file['file_name'],
                'descripcion' => 'Cedula',
                'original' => 'No',
                'tipos_archivos_id' => 2,
                'clientes_id' => $cliente_id,
                'usuarios_id' => 0,
                'creacion' => date('Y-m-d H:i:s')
            );
        }

        return $data;
    }

    /**
     * @param $cliente_id
     * @return array|bool
     */
    protected function getSeguro($cliente_id)
    {
        if (!$this->upload->do_upload('social_file')) {
            $error = array('error' => $this->upload->display_errors());
			$data = false;
        } else {
            $file = $this->upload->data();
            $data = array(
                'nombre' => $file['file_name'],
                'descripcion' => 'Ficha css',
                'original' => 'No',
                'tipos_archivos_id' => 1,
                'clientes_id' => $cliente_id,
                'usuarios_id' => 0,
                'creacion' => date('Y-m-d H:i:s')
            );
        }

        return $data;
    }

    /**
     * @param $cliente_id
     * @return array|bool
     */
    protected function getTrabajo($cliente_id)
    {
        if (!$this->upload->do_upload('work_file')) {
            $error = array('error' => $this->upload->display_errors());
			$data = false;
        } else {
            $file = $this->upload->data();
            $data = array(
                'nombre' => $file['file_name'],
                'descripcion' => 'Carta de trabajo',
                'original' => 'No',
                'tipos_archivos_id' => 4,
                'clientes_id' => $cliente_id,
                'usuarios_id' => 0,
                'creacion' => date('Y-m-d H:i:s')
            );
        }

        return $data;
    }

    /**
     * @param string $image
     * @return string|null
     */
    protected function handlerImage($image)
    {
        if (!empty($image)) {
            $name = uniqid('doc') . time();
            $file = fopen('./files/' . $name, 'wb');
            $data = explode(',', $image);
            $content = count($data) > 1 ? $data[1] : $data[0];
            fwrite($file, base64_decode($content));
            fclose($file);

            return $name;
        }

        return null;
    }

    /**
     * @param $name
     * @param $description
     * @param $type
     * @param $cliente_id
     * @return array
     */
    protected function buildPayload($name, $description, $type, $cliente_id)
    {
        return array(
            'nombre' => $name,
            'descripcion' => $description,
            'original' => 'No',
            'tipos_archivos_id' => $type,
            'clientes_id' => $cliente_id,
            'usuarios_id' => 0,
            'creacion' => date('Y-m-d H:i:s')
        );
    }
}
