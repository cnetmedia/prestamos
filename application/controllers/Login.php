<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('login_model','bitacora_model'));
		$this->load->library(array('session','form_validation','email'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');
	}

	public function index()	{

		if ($this->session->userdata('id_rol') == '') {
			$data['token'] = $this->token();
			$data['title'] = 'Prestamos 911';
			$data['seven'] = 'Sevenen Corporation';
			$this->load->view('login/login_view',$data);
		} else {
			redirect('admin');
		}
	}

	public function change_password(){
		$data['token'] = $this->token();
		$data['id_user'] = $this->session->userdata('id_user');
		$data['correo'] = $this->session->userdata('correo');
		$data['title'] = 'Prestamos 911';
		$data['seven'] = 'Sevenen Corporation';
		$this->load->view('login/change_password',$data);
	}

	public function process_password(){
		if($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {
			$this->form_validation->set_rules('password', 'nueva contraseña', 'required|trim|min_length[5]|max_length[10]|xss_clean');
			$this->form_validation->set_rules('rpassword', 'repita nueva contraseña', 'required|trim|min_length[5]|max_length[10]|xss_clean');
			if($this->form_validation->run() == FALSE) {
				$this->change_password();
			} else {
				if ($this->input->post('password') == $this->input->post('rpassword')) {
					$update = $this->login_model->updatePasswordIngreso($this->input->post('id'),$this->input->post('password'));
					if ($update) {
						$this->index();
					}
				} else {
					$this->session->set_flashdata('aviso','Las contraseñas no coinciden.');
					$this->change_password();
				}
			}
		} else {
			$this->index();
		}
	}

	public function login_user() {

		if($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token')) {

			$this->form_validation->set_rules('correo', 'usuario', 'required|trim|min_length[3]|max_length[100]|xss_clean');
			$this->form_validation->set_rules('password', 'password', 'required|trim|min_length[5]|max_length[10]|xss_clean');

			if($this->form_validation->run() == FALSE) {
				$this->index();
			} else {
				$correo = $this->input->post('correo');
				$password = sha1($this->input->post('password'));
				$check_user = $this->login_model->loginUser($correo,$password);

				if($check_user == TRUE) {
					if ($check_user->ingreso == 0) {
						$data = array(
							'id_user' 		=> 		$check_user->id,
							'correo' 		=> 		$check_user->correo
						);
						$this->session->set_userdata($data);
						$this->change_password();
					} else {
						$ultima_conexion = date('Y-m-d H:i:s');
						$update = $this->login_model->updateUltimaConexion($check_user->id,$ultima_conexion);
						if($update) {
							$cadena = $check_user->nombres;
							$nombre = explode(' ',$cadena);
							$cadena1 = $check_user->apellidos;
							$apellido = explode(' ',$cadena1);
							$check_rol = $this->login_model->loginRol($check_user->roles_id);
							$data = array(
								'is_logued_in' 	=> 		TRUE,
								'id_user' 		=> 		$check_user->id,
								'id_rol'		=>		$check_user->roles_id,
								'correo' 		=> 		$check_user->correo,
								'nombres' 		=> 		$nombre[0],
								'apellidos' 	=> 		$apellido[0],
								'rol' 			=> 		$check_rol->rol,
								'descripcion' 	=> 		$check_rol->descripcion,
								/*'insertar' 		=> 		$check_rol->insertar,
								'modificar' 	=> 		$check_rol->modificar,
								'eliminar' 		=> 		$check_rol->eliminar,
								'aprobar' 		=> 		$check_rol->aprobar*/
							);
							$this->session->set_userdata($data);
							$this->index();
						}
					}
				}
			}
		} else {
			redirect('login');
		}
	}

	public function token() {
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return $token;
	}

	public function logout_ci() {
		$this->session->sess_destroy();
		if ($this->session->userdata('id_user') > 0) {
			$data = array(
				'accion' => "Cerro Sesión",
				'usuarios_id' => $this->session->userdata('id_user')
			);
			$insert = $this->bitacora_model->save($data);
			if ($insert > 0) {
				redirect('login');
			}
		} else {
			redirect('login');
		}
	}

	public function forgot_password()
	{
		$data['token'] = $this->token();
		$data['title'] = 'Prestamos 911';
		$data['seven'] = 'Sevenen Corporation';
		$this->load->view('login/forgot_password', $data);
	}

	public function restore_password() {

		if($this->input->post('token') && $this->input->post('token') == $this->session->userdata('token'))
		{
			$this->form_validation->set_rules('correo','correo','required|valid_email|trim|xss_clean');

			if($this->form_validation->run()==FALSE){
				$this->forgot_password();
			} else {
				$email = $this->input->post('correo');
				$password = rand();

				$check_user = $this->login_model->verify_email($email);
				if($check_user == TRUE){
					$this->email->from('info@prestamos911.com', 'Prestamos 911');
					$this->email->to($email);
					$this->email->subject('Reinicio de Contraseña, Prestamos 911');
					$this->email->message('Hola, ' . $check_user->nombres . ' ' . $check_user->apellidos . ', tu nueva contraseña es: ' . $password . ' recuerda que puedes cambiarla en tu perfil, al ingresar al Sistema Administrativo. Saludos! ' . 'http://prestamos911.sevenen.com');
					if ($this->email->send()){
						$insert = $this->login_model->resetPassword($check_user->id,$password);
						$this->session->set_flashdata('pass_modificado','Correo enviado.');
					} else {
						$this->session->set_flashdata('pass_modificado','Error.');
					}
					redirect('login/forgot_password');
				} else {
					$this->session->set_flashdata('pass_modificado','Ese correo no esta registrado.');
					$this->forgot_password();
				}
			}
		}
	}



}
