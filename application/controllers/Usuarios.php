<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Usuarios extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('usuarios_model','admin_model','companias_model'));
		$this->load->library(array('session','form_validation','email'));
		$this->load->helper(array('url','form'));
		$this->load->database('default');

		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}

	}
	
	public function index()	{		
		if ($this->roluser == 1 || $this->roluser == 2) {
			$data['title'] = "Prestamos 911";
			$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
			$roles = $this->usuarios_model->getRoles();
			$array = array();
			if ($this->roluser == 1) {
				$array = $roles;
			} else {
				foreach ($roles as $key => $value) {
					if ($value['id'] > $this->roluser){
						$array[] = $value;
					}
				}
			}
			$data['result_roles'] = $array;
			$inser = FALSE;		
			if ($this->roluser == 1 || $this->roluser == 2) {
				if ($this->admin_model->get_insertar($this->roluser) == 1){
					$inser = TRUE;
				}
			}
			$data['insertar'] = $inser;
			$this->load->view('templates/header',$data);
			$this->load->view('admin/usuarios/gestionar_usuarios',$data);
			$this->load->view('templates/footer',$data);
		} else {
			$data['aviso'] = "No tiene permisos para acceder a este modulo.";
			$this->load->view('errors/error_403',$data);
		}			
	}

	function check_default($post_string) {
		return $post_string == '0' ? FALSE : TRUE;
	}

	public function ajax_list()	{
		$list = $this->usuarios_model->get_datatables();

		$lista = array();
		foreach ($list as $fila) {
			if ($fila->id > 0) {
				$lista[] = $fila;
			}			
		}

		$data = array();
		$no = $_POST['start'];
		foreach ($lista as $user) {

			$no++;
			$row = array();
			$row[] = $user->nombres . " " . $user->apellidos;
			$row[] = $user->correo;
			$row[] = $user->extension;
			$row[] = $this->usuarios_model->get_rol_by_id($user->roles_id)->descripcion;
			
			if($user->estado == 'Activo'){
				$row[] = '<label class="label label-success">'.$user->estado.'</label>';
			}else if($user->estado == 'Bloqueado'){
				$row[] = '<label class="label label-danger">'.$user->estado.'</label>';
			}else{
				$row[] = '<label class="label label-default">'.$user->estado.'</label>';	
			}

			$row[] = $user->ultima_conexion;

			$assign = $this->usuarios_model->get_companias_usuarios_by_clientes_id($user->id);
			if($assign){
				$array = array();
				foreach ($assign as $fila) {
					$array[] = $fila->companias_id;
				}
				$company = array();
				for ($i=0; $i < count($array) ; $i++) {
					if ($i > 0) {
						$company[] = "<br>" . json_encode($this->companias_model->get_by_id($array[$i])->nombre);
					} else {
						$company[] = json_encode($this->companias_model->get_by_id($array[$i])->nombre);
					}
				}
				$row[] = $company;
			} else {
				$row[] = "";
			}			

			//add html for action
			if($user->estado == 'Inactivo'){
				$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_user('."'".$user->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
						';
			}else{				
				if ($this->roluser == 1 ){
					if ($this->admin_model->get_modificar($this->roluser) == 1 && $this->admin_model->get_eliminar($this->roluser) == 1) {
						$row[] = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="assign_company('."'".$user->id."'".')"><i class="fa fa-check"></i> Asignar </a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_user('."'".$user->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
						  <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$user->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
					} elseif ($this->admin_model->get_modificar($this->roluser) == 1) {
						$row[] = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="assign_company('."'".$user->id."'".')"><i class="fa fa-check"></i> Asignar </a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_user('."'".$user->id."'".')"><i class="fa fa-pencil"></i> Editar</a>';
					} elseif ($this->admin_model->get_eliminar($this->roluser) == 1) {
						$row[] = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="assign_company('."'".$user->id."'".')"><i class="fa fa-check"></i> Asignar </a>
						<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>
						  <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$user->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
					} else {
						$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>';
					}
				} else {
					if ($this->admin_model->get_modificar($this->roluser) == 1 && $this->admin_model->get_eliminar($this->roluser) == 1 && $this->roluser < $user->roles_id && $this->roluser == 2){
						$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>
							<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_user('."'".$user->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
							  <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$user->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
					} elseif ($this->admin_model->get_modificar($this->roluser) == 1 && $this->roluser < $user->roles_id && $this->roluser == 2) {
						$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>
							<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_user('."'".$user->id."'".')"><i class="fa fa-pencil"></i> Editar</a>';
					} elseif ($this->admin_model->get_eliminar($this->roluser) == 1 && $this->roluser < $user->roles_id && $this->roluser == 2) {
						$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>
							<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$user->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';
					} else {
						$row[] = '<a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="view_user('."'".$user->id."'".')"><i class="fa fa-folder"></i> Ver </a>';
					}
				}			
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->usuarios_model->count_all(),
			"recordsFiltered" => $this->usuarios_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_view($id) {

		$data = $this->usuarios_model->get_by_id($id);
		$rol = $this->usuarios_model->get_rol_by_id($data->roles_id);
		$data->roles_id = $rol->descripcion;
		$user_creator = $this->usuarios_model->get_by_id($data->usuarios_id);
		$rol_creator = $this->usuarios_model->get_rol_by_id($user_creator->roles_id);

		$cadena = $user_creator->nombres;
		$nombre = explode(' ',$cadena);
		$cadena1 = $user_creator->apellidos;
		$apellido = explode(' ',$cadena1);

		$data->usuarios_id = $nombre[0] . " " . $apellido[0] . ", <b>" . $rol_creator->descripcion . ".</b>";
		echo json_encode($data);
	}

	public function ajax_edit($id) {
		$data = $this->usuarios_model->get_by_id($id);
		echo json_encode($data);
	}

	public static function generateRandomString($length = 32) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}

	public function ajax_add() {
		if ($this->roluser == 1 || $this->roluser == 2) {
			if($this->admin_model->get_insertar($this->roluser) == 1) {
				$this->form_validation->set_rules('nombres','nombres','required|trim|min_length[3]|max_length[255]|xss_clean');
				$this->form_validation->set_rules('apellidos','apellidos','required|trim|min_length[3]|max_length[255]|xss_clean');
				$this->form_validation->set_rules('correo','correo','required|valid_email|is_unique[usuarios.correo]|trim|xss_clean');
				$this->form_validation->set_rules('cedula','cédula','required|trim|min_length[12]|max_length[20]|xss_clean');		
				$this->form_validation->set_rules('extension','extensión','required|trim|min_length[3]|xss_clean');
				$this->form_validation->set_rules('roles_id','rol de usuario','required|callback_check_default');
				$this->form_validation->set_rules('estado','estado','required|callback_check_default');
				$this->form_validation->set_rules('descripcion','descripción','required|trim|min_length[3]|max_length[255]|xss_clean');				
				if ($this->form_validation->run()==FALSE) {
					echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
				} else {
					$password = $this->generateRandomString(10);
					$email = $this->input->post('correo');
					if ($this->roluser == 1){
						$data = array(
								'nombres' => $this->input->post('nombres'),
								'apellidos' => $this->input->post('apellidos'),
								'correo' => $email,
								'cedula' => $this->input->post('cedula'),
								'extension' => $this->input->post('extension'),
								'descripcion' => $this->input->post('descripcion'),
								'password' => sha1($password),
								//'ultima_conexion' => $ultima_conexion,
								'estado' => $this->input->post('estado'),
								'roles_id' => $this->input->post('roles_id'),
								'usuarios_id' => $this->session->userdata('id_user'),
								'fecha_creacion' => date('Y-m-d H:i:s'),
								'ingreso' => 0
							);
						$insert = $this->usuarios_model->save($data);
						if ($insert) {
							$this->email->from('social@sevenencorp.com', 'Prestamos 911');
							$this->email->to($email);
							$this->email->subject('Bienvenid@ a Prestamos 911');
							$this->email->message('Hola, ' . $this->input->post('nombres') . ' ' . $this->input->post('apellidos') . ' tu contraseña para ingresar al Sistema Administrativo de Prestamos 911 es: ' . $password . ' puedes cambiarla en tu perfil, luego de ingresar al sistema. Bienvenid@! ' . 'http://prestamos911.sevenen.com');
							if ($this->email->send()){
								echo json_encode(array("status" => TRUE));								
							} else {
								echo json_encode(array("validation" => 'Error al enviar contraseña al correo.'));
							}
						}
					} else {
						if ($this->roluser >= $this->input->post('roles_id')) {
							echo json_encode(array('rol'=> 'No tienes permiso para añadir usuarios con ese rol.'));
						} else {
							//$ultima_conexion = date('Y-m-d H:i:s');

							$data = array(
									'nombres' => $this->input->post('nombres'),
									'apellidos' => $this->input->post('apellidos'),
									'correo' => $email,
									'cedula' => $this->input->post('cedula'),
									'extension' => $this->input->post('extension'),
									'descripcion' => $this->input->post('descripcion'),
									'password' => sha1($password),
									//'ultima_conexion' => $ultima_conexion,
									'estado' => $this->input->post('estado'),
									'roles_id' => $this->input->post('roles_id'),
									'usuarios_id' => $this->session->userdata('id_user'),
									'fecha_creacion' => date('Y-m-d H:i:s'),
									'ingreso' => 0
								);
							$insert = $this->usuarios_model->save($data);
							if ($insert) {
								$this->email->from('social@sevenencorp.com', 'Prestamos 911');
								$this->email->to($email);
								$this->email->subject('Bienvenid@ a Prestamos 911');
								$this->email->message('Hola, ' . $this->input->post('nombres') . ' ' . $this->input->post('apellidos') . ' tu contraseña para ingresar al Sistema Administrativo de Prestamos 911 es: ' . $password . ' puedes cambiarla en tu perfil, luego de ingresar al sistema. Bienvenid@! ' . 'http://prestamos911.sevenen.com');
								if ($this->email->send()){
									echo json_encode(array("status" => TRUE));								
								} else {
									echo json_encode(array("validation" => 'Error al enviar contraseña al correo.'));
								}
							}
						}
					}
				}				
			} else {
				echo json_encode(array('permission'=> 'No tienes permiso para añadir usuarios.'));
			}			
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para este modulo.'));
		}

	}

	public function ajax_update() {
		if ($this->roluser == 1 || $this->roluser == 2) {
			if($this->admin_model->get_modificar($this->roluser) == 1) {
				$this->form_validation->set_rules('nombres','nombres','required|trim|min_length[3]|max_length[255]|xss_clean');
				$this->form_validation->set_rules('apellidos','apellidos','required|trim|min_length[3]|max_length[255]|xss_clean');
				$this->form_validation->set_rules('correo','correo','required|valid_email|trim|xss_clean');
				$this->form_validation->set_rules('cedula','cédula','required|trim|min_length[12]|max_length[20]|xss_clean');		
				$this->form_validation->set_rules('extension','extensión','required|trim|min_length[3]|xss_clean');
				$this->form_validation->set_rules('roles_id','rol de usuario','required|callback_check_default');
				$this->form_validation->set_rules('estado','estado','required|callback_check_default');
				$this->form_validation->set_rules('descripcion','descripción','required|trim|min_length[3]|max_length[255]|xss_clean');
				$this->form_validation->set_rules('resetpassword','resetear contraseña','xss_clean');
				if ($this->form_validation->run()==FALSE) {
					echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
				} else {
					$password = $this->generateRandomString(10);
					$email = $this->input->post('correo');
					if ($this->roluser == 1){
						$data = array(
								'nombres' => $this->input->post('nombres'),
								'apellidos' => $this->input->post('apellidos'),
								'correo' => $email,
								'cedula' => $this->input->post('cedula'),
								'extension' => $this->input->post('extension'),
								'descripcion' => $this->input->post('descripcion'),
								'estado' => $this->input->post('estado'),
								'roles_id' => $this->input->post('roles_id'),
							);
 						if ($this->input->post('resetpassword') == "yes"){
							$data["password"] = sha1($password);
							$data["ingreso"] = 0;							
							$this->email->from('social@sevenencorp.com', 'Prestamos 911');
							$this->email->to($email);
							$this->email->subject('Reinicio de Contraseña, Prestamos 911');
							$this->email->message('Hola, ' . $this->input->post('nombres') . ' ' . $this->input->post('apellidos') . ', tu nueva contraseña es: ' . $password . ' recuerda que puedes cambiarla en tu perfil, al ingresar al Sistema Administrativo. Saludos! ' . 'http://prestamos911.sevenen.com');
							if ($this->email->send()){
								$this->usuarios_model->update(array('id' => $this->input->post('id')), $data);
								echo json_encode(array("status" => TRUE));
							} else {
								echo json_encode(array("validation" => 'Error al enviar contraseña al correo.'));
							}
						} else {
							$this->usuarios_model->update(array('id' => $this->input->post('id')), $data);
							echo json_encode(array("status" => TRUE));
						}
					} else {
						$user = $this->usuarios_model->get_by_id($this->input->post('id'));
						if ($this->roluser >= $user->roles_id) {
							echo json_encode(array('rol'=> 'No tienes permiso para editar este usuario.'));
						} else {
							if ($this->roluser >= $this->input->post('roles_id')) {
								echo json_encode(array('rol'=> 'No tienes permiso para añadir usuarios con ese rol.'));
							} else {
								$data = array(
										'nombres' => $this->input->post('nombres'),
										'apellidos' => $this->input->post('apellidos'),
										'correo' => $email,
										'cedula' => $this->input->post('cedula'),
										'extension' => $this->input->post('extension'),
										'descripcion' => $this->input->post('descripcion'),
										'estado' => $this->input->post('estado'),
										'roles_id' => $this->input->post('roles_id'),
									);
		 						if ($this->input->post('resetpassword') == "yes"){
									$data["password"] = sha1($password);
									$data["ingreso"] = 0;
									$this->email->from('social@sevenencorp.com', 'Prestamos 911');
									$this->email->to($email);
									$this->email->subject('Reinicio de Contraseña, Prestamos 911');
									$this->email->message('Hola, ' . $this->input->post('nombres') . ' ' . $this->input->post('apellidos') . ', tu nueva contraseña es: ' . $password . ' recuerda que puedes cambiarla en tu perfil, al ingresar al Sistema Administrativo. Saludos! ' . 'http://prestamos911.sevenen.com');
									if ($this->email->send()){
										$this->usuarios_model->update(array('id' => $this->input->post('id')), $data);
										echo json_encode(array("status" => TRUE));
									} else {
										echo json_encode(array("validation" => 'Error al enviar contraseña al correo.'));
									}
								} else {
									$this->usuarios_model->update(array('id' => $this->input->post('id')), $data);
									echo json_encode(array("status" => TRUE));
								}
							}
						}
					}
				}				
			} else {
				echo json_encode(array('permission'=> 'No tienes permiso para editar usuarios.'));
			}
		} else {
			echo json_encode(array('permission'=> 'No tienes permiso para este modulo.'));
		}
	}

	public function ajax_delete($id) {
		if ($this->roluser == $id){
			echo json_encode(array('permission'=> 'No es recomendable que elimines tu propia cuenta.'));
		} else {
			if ($this->roluser == 1) {
				if($this->session->userdata('eliminar') == 1) {
					$this->usuarios_model->update(array('id'=>$id), array('estado' => 'Inactivo'));
					echo json_encode(array("status" => TRUE));
				} else {
					echo json_encode(array('permission'=> 'No tienes permiso para borrar usuarios.'));
				}
			} elseif ($this->roluser == 2) {
				echo json_encode(array('permission'=> 'No tienes permiso para eliminar.'));
			} else {
				echo json_encode(array('permission'=> 'No tienes permiso para este modulo.'));
			}
		}
	}

	public function companias_usuarios($id){
		$data = $this->usuarios_model->get_companias_usuarios_by_clientes_id($id);

		$array = array();
		if ($data) {
			foreach ($data as $row) {

				$array[] = array(
					'companias_id' => $row->companias_id,
					'compania' => $this->companias_model->get_by_id($row->companias_id)->nombre
				);
			}
		}

		echo json_encode($array);
	}


	public function add_asign_company(){
		$this->form_validation->set_rules('companias_id','compañia','required|callback_check_default');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$data = array(
				'companias_id' => $this->input->post('companias_id'),
				'usuarios_id' => $this->input->post('userid')
			);
			$insert = $this->usuarios_model->save_companias_usuarios($data);
			echo json_encode(array("status" => TRUE));
		}
	}

	public function disasign_company(){
		$this->usuarios_model->delete_companias_usuarios($this->input->post('usuarios_id'),$this->input->post('companias_id'));
		echo json_encode(array("status" => TRUE));
	}

}