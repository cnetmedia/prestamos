<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Admin extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('admin_model','productos_model','companias_model','clientes_model','solicitudes_model','cobros_model','usuarios_model','roles_model','modulos_model','db_access_model','notas_model','disposiciones_model','aprobacion_model','solicitudes_aprobacion_model','cotizacion_model','datos_model'));
		$this->load->library(array('session','form_validation'));
		$this->load->helper(array('url','form','download','file'));
		$this->load->database('default');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}
	}
	
	public function index() {
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->session->userdata('id_rol'));

		$data['total_usuarios'] = $this->admin_model->totalUsuarios();
		$data['porcetaje_total'] = 100;

		$data['total_productos'] = $this->productos_model->count_all();
		$data['total_companias'] = $this->companias_model->count_all();
		$data['total_clientes'] = $this->clientes_model->count_all();
		$data['total_solicitudes'] = $this->solicitudes_model->count_all();
		$data['total_cobros'] = $this->cobros_model->count_all();

/*		if ($this->session->userdata('id_user') == 0) {
			var_dump($this->session->userdata('id_user'));

			var_dump('Inicio -----');
			$this->importarCliente();
			var_dump('----- Fin');

		}	*/	

/*		if ($this->session->userdata('id_user') == 0) {
			$this->cotiz();
		}*/

		$this->load->view('templates/header',$data);
		$this->load->view('admin/admin_view',$data);
		$this->load->view('templates/footer',$data);
	}

/*	public function importarCliente(){

		$clientes = $this->clientes_model->get_clientes_prueba();

		$array = array();
		foreach ($clientes as $row) {

			$data = array(
				'clientes_id' => $this->clientes_model->save($row),
				'estados_id' => 3,
				'usuarios_id' => 4,
				'disposiciones_id' => 14
			);

			$this->solicitudes_model->save($data);

			$array[] = $data['clientes_id'];

		}

		var_dump('listo!');
		var_dump($array);
		
	}*/

/*	public function cotiz(){


		$aprobaciones = $this->aprobacion_model->get_all();

		if ($aprobaciones) {
			foreach ($aprobaciones as $apro) {


				$suma = $apro->cantidad_aprobada;
				$productos_id = $apro->productos_id;
				$plazo = $apro->cantidad_meses;

				//$fecha = date("d-m-Y");
				$fecha = $apro->fecha_primer_descuento;

				$tasa = 0.00;
				$cargo = 0.00;

				if ($productos_id) {
					$products = $this->cotizacion_model->get_products_by_id($productos_id);
					foreach ($products as $row) {
						$tasa = $row->tasa_interes_mensual;
						$cargo = $row->cargo_administrativo;
					}
				}


				$suma = round($suma,2);
				$tasa = round($tasa,2);
				$cargo = round($cargo,2);

				$interes = $tasa / 100;

				$p = $suma;
				$x = $interes;
				$n = $plazo;

				$m = $p * ( $x / ( 1 - ( ( 1 + $x ) ** - $n ) ) );

				$cuotaperiodica = $m;
				$cuotaperiodica = round($cuotaperiodica,2);

				$cargototal = $cargo * ($suma / 100);
				$cargototal = round($cargototal,2);

				$pagado = array();	
				$interesasignado = array();

				$sum = $suma;
				for ($i=0; $i<$plazo; $i++) {
					$interesi = $sum * $interes;
					$interesi = round($interesi,2);
					$valori = $cuotaperiodica - $interesi;
					$valori = round($valori,2);
					$restoi = $sum - $valori;
					$restoi = round($restoi,2);
					$sum = $restoi;
					$pagado[$i] = $valori;
					$interesasignado[$i] = $interesi;
				}


				$totalintereses = array_sum($interesasignado);
				$totalintereses = round($totalintereses,2);
				$totalcargo = $cargototal * $plazo;
				$totalcargo = round($totalcargo,2);

				$totalsuma = array();
				$intereses = $totalcargo + array_sum($interesasignado);
				$intereses = round($intereses,2);
				$totalcobrar = array();
				$diferenciasuma = $suma - (array_sum($pagado));
				$diferenciasuma = round($diferenciasuma,2);

				if($diferenciasuma < 0){
					$dec = explode("-0.", $diferenciasuma);
					$num = $dec[1] * 1;
					$m = 0;
					for ($i=0; $i < $num; $i++) {
						$m++;
						$pagado[$plazo-$m] = $pagado[$plazo-$m] - 0.01;
					}
				} elseif($diferenciasuma > 0){
					$dec = explode("0.", $diferenciasuma);
					$num = $dec[1] * 1;
					for ($i=0; $i < $num; $i++) {
						$pagado[$i] = $pagado[$i] + 0.01;
					}
				}

				$data = array();
				$n = 0;
				for ($i=0; $i<$plazo; $i++){
					$n++;
					$fe = date("d-m-Y", strtotime("$fecha + $n months"));
					$data[] = array(
						'fecha' => $fe,
						'principal' => $pagado[$i],
						'interes' => $interesasignado[$i],
						'tasa' => $tasa*1,
						'cargo' => $cargototal,
						'total' => $pagado[$i] + $interesasignado[$i] + $cargototal
					);
					$totalsuma[] = $pagado[$i];
					$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototal;
				}

				$output = array(
					"suma" => $suma,
					"cargo" => $totalcargo,
					"interes" => $totalintereses,
					"total" => array_sum($totalcobrar),
					"aprobacion_id" => $apro->id
				);

				$insert = $this->cotizacion_model->save($output);

			}
		}

		var_dump('listo!');


	}*/


	public function profile(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->session->userdata('id_rol'));

		$user = $this->usuarios_model->get_by_id($this->session->userdata('id_user'));
		$datos = array(
			'id' => $user->id,
			'nombres' => $user->nombres,
			'apellidos' => $user->apellidos,
			'correo' => $user->correo,
			'cedula' => $user->cedula,
			'extension' => $user->extension
		);

		$data['dataUser'] = $datos;
		$this->load->view('templates/header',$data);
		$this->load->view('admin/profile_view',$data);
		$this->load->view('templates/footer',$data);
	}

	public function passwordUpdate() {
		$this->form_validation->set_rules('vpassword', 'contraseña actual', 'required|trim|min_length[5]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('password', 'contraseña nueva', 'required|trim|min_length[5]|max_length[10]|xss_clean');
		$this->form_validation->set_rules('rpassword', 'repita contraseña nueva', 'required|trim|min_length[5]|max_length[10]|xss_clean');

		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$vpassword = $this->input->post('vpassword');
			$check_password = $this->admin_model->verify_password($this->session->userdata('id_user'),sha1($vpassword));
			if ($check_password) {
				$password = $this->input->post('password');
				$rpassword = $this->input->post('rpassword');
				if ($rpassword == $password) {
					if ($vpassword == $password) {
						echo json_encode(array("validation" => '<span class="label label-danger animated shake">Contraseña actual es igual a la nueva</span>'));
					} else {
						$data = array('password' => sha1($password));
						$this->usuarios_model->update(array('id' => $this->input->post('idp')), $data);
						echo json_encode(array("status" => TRUE, "aviso" => '<span class="label label-success animated shake">Contraseña actualizada correctamente</span>'));
					}
				} else {
					echo json_encode(array("validation" => '<span class="label label-danger animated shake">Campos de contraseña nueva no coinciden</span>'));
				}
			} else {
				echo json_encode(array("validation" => '<span class="label label-danger animated shake">Contraseña actual incorrecta</span>'));
			}
		}
	}

	public function settings(){
		if ($this->session->userdata('id_rol') == 1) {
			$data['title'] = "Prestamos 911";
			$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->session->userdata('id_rol'));
			$user = $this->usuarios_model->get_by_id($this->session->userdata('id_user'));

			$data['roles'] = $this->admin_model->consultarRoles();


			$this->load->view('templates/header',$data);
			$this->load->view('admin/settings_view',$data);
			$this->load->view('templates/footer',$data);
		} else {
			$data['aviso'] = "No tiene permisos para acceder a este modulo.";
			$this->load->view('errors/error_403',$data);
		}
	}

	public function roles_list()	{

		$list = $this->roles_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $rol) {
			$no++;
			$row = array();
			$row[] = $rol->descripcion;
			if ($rol->insertar == 1) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" name="insertar'.$rol->id.'" id="insertar'.$rol->id.'" value="1" checked onclick="confirmInsertCheck('.$rol->id.')"></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" name="insertar'.$rol->id.'" id="insertar'.$rol->id.'" value="0" onclick="confirmInsertCheck('.$rol->id.')"></div>';
			}

			if ($rol->modificar == 1) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" name="modificar'.$rol->id.'" id="modificar'.$rol->id.'" value="1" checked onclick="confirmEditCheck('.$rol->id.')"></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" name="modificar'.$rol->id.'" id="modificar'.$rol->id.'" value="0" onclick="confirmEditCheck('.$rol->id.')"></div>';
			}

			if ($rol->eliminar == 1) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" name="eliminar'.$rol->id.'" id="eliminar'.$rol->id.'" value="1" checked onclick="confirmDeleteCheck('.$rol->id.')"></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" name="eliminar'.$rol->id.'" id="eliminar'.$rol->id.'" value="0" onclick="confirmDeleteCheck('.$rol->id.')"></div>';
			}

			if ($rol->aprobar == 1) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" name="aprobar'.$rol->id.'" id="aprobar'.$rol->id.'" value="1" checked onclick="confirmAprobarCheck('.$rol->id.')"></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" name="aprobar'.$rol->id.'" id="aprobar'.$rol->id.'" value="0" onclick="confirmAprobarCheck('.$rol->id.')"></div>';
			}
			
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->roles_model->count_all(),
			"recordsFiltered" => $this->roles_model->count_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function rolInsertUpdate(){
		$data = array(
			'insertar' => $this->input->post('insertar')
		);
		$this->roles_model->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function rolEditUpdate(){
		$data = array(
			'modificar' => $this->input->post('modificar')
		);
		$this->roles_model->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function rolDeleteUpdate(){
		$data = array(
			'eliminar' => $this->input->post('eliminar')
		);
		$this->roles_model->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function rolAprobarUpdate(){
		$data = array(
			'aprobar' => $this->input->post('aprobar')
		);
		$this->roles_model->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function grafica(){

		$fechanow = date('Y-m-d');
		$data = array();
		$n = -1;
		$array2 = array();
		for ($i=0; $i<7; $i++){
			$n++;
			$fe = date("Y-m-d", strtotime("$fechanow - $n days"));
			$array2[] = $fe;
			$fec = explode('-',$fe);
			$cantfe = $this->solicitudes_model->get_solicitud_by_fecha($fe);
			$data[] = array(
				'year' => $fec[0],
				'month' => $fec[1],
				'day' => $fec[2],
				'cant' => $cantfe
			);
		}
		
		echo json_encode(array('fechas' => $data));
	}
	

	public function modulos_roles_list()	{

		$list = $this->modulos_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $modulo) {
			$no++;
			$row = array();
			$row[] = $modulo->modulo;

			$sadmin = $this->admin_model->ConsultModuloRol($modulo->id,1);
			if ($sadmin) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="sadmin'.$modulo->id.'" value="'.$sadmin->roles_id.'" onclick="sadmin('.$modulo->id.')" checked></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="sadmin'.$modulo->id.'" value="1" onclick="sadmin('.$modulo->id.')"></div>';
			}

			$admin = $this->admin_model->ConsultModuloRol($modulo->id,2);
			if ($admin) {				
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="admin'.$modulo->id.'" value="'.$admin->roles_id.'" onclick="admin('.$modulo->id.')" checked></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="admin'.$modulo->id.'" value="2" onclick="admin('.$modulo->id.')"></div>';
			}

			$repre = $this->admin_model->ConsultModuloRol($modulo->id,3);
			if ($repre) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="repre'.$modulo->id.'" value="'.$repre->roles_id.'" onclick="repre('.$modulo->id.')" checked></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="repre'.$modulo->id.'" value="3" onclick="repre('.$modulo->id.')"></div>';
			}

			$sup = $this->admin_model->ConsultModuloRol($modulo->id,4);
			if ($sup) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="sup'.$modulo->id.'" value="'.$sup->roles_id.'" onclick="sup('.$modulo->id.')" checked></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="sup'.$modulo->id.'" value="4" onclick="sup('.$modulo->id.')"></div>';
			}

			$cob = $this->admin_model->ConsultModuloRol($modulo->id,5);
			if ($cob) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="cob'.$modulo->id.'" value="'.$cob->roles_id.'" onclick="cob('.$modulo->id.')" checked></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="cob'.$modulo->id.'" value="5" onclick="cob('.$modulo->id.')"></div>';
			}

			$cob1 = $this->admin_model->ConsultModuloRol($modulo->id,6);
			if ($cob1) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="cob1'.$modulo->id.'" value="'.$cob1->roles_id.'" onclick="cob1('.$modulo->id.')" checked></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="cob1'.$modulo->id.'" value="6" onclick="cob1('.$modulo->id.')"></div>';
			}

			$cob2 = $this->admin_model->ConsultModuloRol($modulo->id,7);
			if ($cob2) {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="cob2'.$modulo->id.'" value="'.$cob2->roles_id.'" onclick="cob2('.$modulo->id.')" checked></div>';
			} else {
				$row[] = '<div class="text-center"><input type="checkbox" class="flat" id="cob2'.$modulo->id.'" value="7" onclick="cob2('.$modulo->id.')"></div>';
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->roles_model->count_all(),
			"recordsFiltered" => $this->roles_model->count_filtered(),
			"data" => $data
		);
		echo json_encode($output);
	}

	public function modulorol(){
		$modulos_id = $this->input->post('modulos_id');
		$roles_id = $this->input->post('roles_id');
		$dato = $this->admin_model->ConsultModuloRol($modulos_id,$roles_id);
		if ($dato) {
			$this->admin_model->delete_modulorol($roles_id,$modulos_id);
		} else {
			$data = array(
				'roles_id' => $roles_id,
				'modulos_id' => $modulos_id
			);
			$insert = $this->admin_model->save_modulorol($data);
		}
		echo json_encode(array("status" => TRUE));
	}

/*	public function verificar_archivo(){
		$nombre_archivo = $this->input->post('nombre_archivo');
    	if ($nombre_archivo == null || $nombre_archivo == "") {
    		echo json_encode(array('aviso'=> 'Elija el archivo a subir.'));
    	} else {
			$file = "db/".$nombre_archivo;
			if (file_exists($file)) {
				echo json_encode(array('status'=> true));
			} else {
				echo json_encode(array('status'=> false));
			}
    	}
	}*/

	public function ajax_notas(){
		$list = $this->notas_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $nota) {

			$no++;
			$row = array();
			$row[] = $nota->nombre;
			$row[] = $nota->nota;
			$row[] = '<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_nota('."'".$nota->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
				<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete_nota('."'".$nota->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->notas_model->count_all(),
			"recordsFiltered" => $this->notas_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function view_nota($id){
		$data = $this->notas_model->get_by_id($id);
		echo json_encode($data);
	}

	public function add_notas(){
		$this->form_validation->set_rules('nombre','nombre','required|trim|xss_clean');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$data = array(
				'nombre' => $this->input->post('nombre'),
				'nota' => $this->input->post('nota'),
			);
			$insert = $this->notas_model->save($data);
			echo json_encode(array("status" => TRUE));
		}
	}

	public function update_notas(){
		$this->form_validation->set_rules('nombre','nombre','required|trim|xss_clean');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$data = array(
				'nombre' => $this->input->post('nombre'),
				'nota' => $this->input->post('nota'),
			);
			$this->notas_model->update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
		}
	}

	public function delete_nota($id) {
		$this->notas_model->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}



	public function ajax_disposiciones(){
		$list = $this->disposiciones_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {

			$no++;
			$row = array();
			$row[] = $fila->nombre;
			$row[] = '<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="edit_disposicion('."'".$fila->id."'".')"><i class="fa fa-pencil"></i> Editar</a>
				<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete_disposicion('."'".$fila->id."'".')"><i class="fa fa-trash-o"></i> Borrar</a>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->disposiciones_model->count_all(),
			"recordsFiltered" => $this->disposiciones_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function view_disposiciones($id){
		$data = $this->disposiciones_model->get_by_id($id);
		echo json_encode($data);
	}

	public function add_disposiciones(){
		$this->form_validation->set_rules('nombre','nombre','required|trim|xss_clean');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$data = array(
				'nombre' => $this->input->post('nombre'),
			);
			$insert = $this->disposiciones_model->save($data);
			echo json_encode(array("status" => TRUE));
		}
	}

	public function update_disposiciones(){
		$this->form_validation->set_rules('nombre','nombre','required|trim|xss_clean');
		if ($this->form_validation->run()==FALSE) {
			echo json_encode(array("validation" => validation_errors('<span class="label label-danger animated shake">','</span>')));
		} else {
			$data = array(
				'nombre' => $this->input->post('nombre'),
			);
			$this->disposiciones_model->update(array('id' => $this->input->post('id')), $data);
			echo json_encode(array("status" => TRUE));
		}
	}

	public function delete_disposiciones($id) {
		$this->disposiciones_model->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}



    public function cargar_archivo() { //verificar servidor tenga upload_max_filesize = 1000M ;1GB     post_max_size = 1000M

		$carpeta = 'temp/';
		if (!file_exists($carpeta)) {
		    mkdir($carpeta, 0777, true);
		}

		$roluser = $this->session->userdata('id_rol');
		if ($this->admin_model->get_insertar($roluser) == 1) {
	    	$nombre_archivo = $this->input->post('nombre_archivo');
	        $archivo = 'file';
	        $config['upload_path'] = "temp/";
	        $config['file_name'] = $nombre_archivo;
	        $config['allowed_types'] = "zip";
	        $config['overwrite'] = FALSE;
	        $this->load->library('upload', $config);
	        if (!$this->upload->do_upload($archivo)) {
	            echo json_encode(array('aviso'=> $this->upload->display_errors()));
	        } else {
				$data = $this->upload->data();
				$filename = $data['file_name'];

				$zip = new ZipArchive;
				if ($zip->open('temp/'.$filename) === TRUE) {
				    $zip->extractTo('db/');
				    $nombre = $zip->statIndex(0);
				    $zip->close();

				    $namedbm = explode('.', $nombre['name']);
					$namedbm = $namedbm[0];

				    $db = $this->db_access_model->get_by_nombre($namedbm);
				    if (!$db) {
				    	$insert = $this->db_access_model->save(array('nombre' => $namedbm));
				    }
					$file = "temp/".$filename;
					if ($file) {
						unlink($file);
					}
				    echo json_encode(array('status'=> true));		    
				} else {
				    echo json_encode(array('aviso'=> 'No se pudo subir el archivo intente e nuevo.'));
				}				
			}
		} else {
			echo json_encode(array('aviso'=> 'No tienes permiso para subir archivos.'));
		}
    }

/*    function extraer_archivo($id){

    	$db = $this->db_access_model->get_by_id($id);
    	$archivo = $db->nombre;

		$zip = new ZipArchive;
		if ($zip->open('temp/'.$archivo) === TRUE) {
		    $zip->extractTo('db/');
		    $nombre = $zip->statIndex(0);
		    $zip->close();
		    $update = $this->db_access_model->update(array('id' => $db->id), array('nombre' => $nombre['name']));
		    if ($update) {
				$file = "temp/".$archivo;
				if ($file) {
					unlink($file);
				}
		    }
		    echo json_encode(array('status'=> true));		    
		} else {
		    echo json_encode(array('status'=> false));
		}
    }*/

    public function access_list(){
		$list = $this->db_access_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $access) {
			$no++;
			$row = array();
			$row[] = $access->nombre;
			$row[] = $access->fecha_creacion;
/*			$row[] = '<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$access->id."'".')"><i class="fa fa-trash-o"></i> Eliminar</a>
				<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="descomprimir('."'".$access->id."'".')"><i class="fa fa-trash-o"></i> Descomprimir</a>';*/


			$row[] = '<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="importarCsv('."'".$access->nombre."'".')"><i class="fa fa-upload"></i> Importar</a>
				<a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="confirm_delete('."'".$access->id."'".')"><i class="fa fa-trash-o"></i> Eliminar</a>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->db_access_model->count_all(),
			"recordsFiltered" => $this->db_access_model->count_filtered(),
			"data" => $data
		);
		echo json_encode($output);
    }

	public function delete_access($id) {
		$archivo = $this->db_access_model->get_by_id($id);
		$namedb = $archivo->nombre;
		$file = "db/".$namedb.'.csv';
		if ($file) {
			$delete = $this->db_access_model->delete_by_id($id);
			if ($delete) {
				unlink($file);
				//$sql = "DROP TABLE IF EXISTS $namedb";
				//$data = $this->datos_model->sentenciaSql($sql);
			}
			echo json_encode(array("status" => TRUE));		
		} else {
			echo json_encode(array("status" => TRUE, "respuesta" => "No hay archivo"));
		}	
	}



/*	public function import_datos(){
		$flag = FALSE;
		$url = base_url() . 'db/datos.csv';
		$fila = 1;
		if (($gestor = fopen($url, "r")) !== FALSE) {
             while (($data = fgetcsv($gestor, 1000, ";")) !== FALSE)
             {
             	$fila++;
                $array = array(
                	'seguro' => $data[0],
                	'cedula' => $data[1],
                	'nombre' => $data[2],
                	'razon_so' => $data[3],
                	'patrono' => $data[4],
                	'tel1' => $data[5],
                	'tel2' => $data[6],
                	'fecha' => $data[7],
                	'salario' => $data[8],
                );
                $insert = $this->datos_model->save($array);
                if ($fila == 10) {
                	$flag = TRUE;
                }
             }
             fclose($gestor);
		}
		if ($flag) {
        	echo json_encode(array('status'=> true));
		}

			
	}*/


	public function importcsv($namedb){
		//$sql = LOAD DATA LOCAL INFILE  'C:/Users/Antonio/Desktop/datos.csv' INTO TABLE datos FIELDS TERMINATED BY ';' ENCLOSED BY '"' LINES TERMINATED BY '\n'	IGNORE 1 ROWS;

/*		$namedbm = explode('.', $namedb);
		$namedbm = $namedbm[0];
*/
		$sql = "CREATE TABLE IF NOT EXISTS $namedb (
			seguro varchar(255) NOT NULL,
			cedula varchar(255) NOT NULL,
			nombre varchar(255) NOT NULL,
			razon_so varchar(255) NOT NULL,
			patrono varchar(255) NOT NULL,
			tel1 varchar(255) NOT NULL,
			tel2 varchar(255) NOT NULL,
			fecha varchar(255) NOT NULL,
			salario varchar(255) NOT NULL
		);";
		$data = $this->datos_model->sentenciaSql($sql);
		$verify = $this->datos_model->verifytable($namedb);
		if ($verify > 0) {
			echo json_encode(array('status'=> 'Ya tiene datos'));
		} else {
			$url = base_url() . 'db/'.$namedb.'.csv';
			$sql1 = "LOAD DATA LOCAL INFILE '$url'
				REPLACE INTO TABLE $namedb
				FIELDS TERMINATED BY ';'
				LINES TERMINATED BY '\n'";
			$data1 = $this->datos_model->prueba($sql1);
			if ($data1) {
				$sql2 = "ALTER TABLE $namedb ADD id INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (id);";
				$alter = $this->datos_model->sentenciaSql($sql2);
				if ($alter) {
					echo json_encode(array('status'=> $alter));
				} else {
					echo json_encode(array('import'=> 'error'));
				}				
			} else {
				echo json_encode(array('import'=> 'error'));
			}
		}



	}


	public function table_css($id = NULL){
		if ($id === NULL) {
			echo json_encode(array('data' => []));
		} else {
			$table = $this->db_access_model->get_by_id($id)->nombre;

			$list = $this->datos_model->get_datatables($table);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $fila) {

				$no++;
				$row = array();
				$row[] = $fila->seguro;
				$row[] = $fila->cedula;
				$row[] = $fila->nombre;
				$row[] = $fila->razon_so;
				$row[] = $fila->patrono;
				$row[] = $fila->tel1;
				$row[] = $fila->tel2;
				$row[] = $fila->fecha;
				$row[] = number_format((int) $fila->salario / 100,2);
			
				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->datos_model->count_all($table),
				"recordsFiltered" => $this->datos_model->count_filtered($table),
				"data" => $data,
			);
			echo json_encode($output);
		}


	}

}