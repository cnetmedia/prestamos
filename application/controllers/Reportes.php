<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Reportes extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array('admin_model','clientes_model','companias_model','solicitudes_model','db_access_model','aprobacion_model','desempleados_model','productos_model','cotizacion_model','cobros_model','datos_verificacion_model','solicitudes_aprobacion_model','desembolsos_view_model','movimiento_usuarios_view_model','cantidad_solicitudes_view_model','cobros_proyectados_view_model','cobros_recolectados_view_model','usuarios_model','estados_model','movimientos_view_model','disposiciones_model','correo_cobro_model','cobros_view_model','cli_ap_model','ref_ap_model','paquetes_reportes_model','representante_legal_model','solicitudes_view_model'));
		$this->load->library(array('session','form_validation','Pdf','email','MyPDFMerger'));
		$this->load->helper(array('url','form','mysql_to_excel_helper'));
		$this->load->database('default');

		$this->roluser = $this->session->userdata('id_rol');
		
		if($this->session->userdata('id_rol') == FALSE) {
			redirect(base_url().'login');
		}

		$this->meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
		$this->path = realpath(dirname(__DIR__)) . '/pdfs/';
		$this->files = array();
	}

	public function index()	{		
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);		
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/gestionar_reportes',$data);
		$this->load->view('templates/footer',$data);
	}

	public function desembolsos()	{
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['sumdesembolso'] = number_format($this->desembolsos_view_model->get_sum_desembolso(),2);
		$data['obligacionfecha'] = number_format($this->desembolsos_view_model->get_sum_obligacion_fecha(),2);
		//$data['cantclientes'] = $this->desembolsos_view_model->count_all();
		$data['cantempresas'] = $this->desembolsos_view_model->get_cant_empresas();

		$aprobaciones = $this->desembolsos_view_model->get_all();
		$array = array();
		foreach ($aprobaciones as $row) {
			$array[] = $this->cobro_calculos($row->id)['totalcobrar'];
		}
		$data['totalobligacion'] = number_format(array_sum($array),2);

		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/desembolsos',$data);
		$this->load->view('templates/footer',$data);
	}

	public function movimiento_usuarios(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		//$data['totalregistros'] = $this->movimientos_view_model->count_all();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/movimiento_usuarios',$data);
		$this->load->view('templates/footer',$data);		
	}

	public function clientes(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		//$data['totalregistros'] = $this->movimientos_view_model->count_all();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/clientes',$data);
		$this->load->view('templates/footer',$data);		
	}

	public function cantidad_solicitudes(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['totalregistros'] = $this->cantidad_solicitudes_view_model->count_all();
		$data['solicitudes'] = $this->cantidad_solicitudes_view_model->cant_solicitudes();
		$data['sinsolicitudes'] = $this->cantidad_solicitudes_view_model->cant_sin_solicitudes();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/cantidad_solicitudes',$data);
		$this->load->view('templates/footer',$data);		
	}

	public function cobros_proyectados(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['totalletras'] = number_format($this->cobros_proyectados_view_model->sum_total(),2);
		$data['totalregistros'] = $this->cobros_proyectados_view_model->count_all();
		$data['cantempresas'] = $this->cobros_proyectados_view_model->get_cant_empresas();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/cobros_proyectados',$data);
		$this->load->view('templates/footer',$data);		
	}

	public function cobrosproyectadoscorreos(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['totalletras'] = number_format($this->cobros_proyectados_view_model->sum_total(),2);
		$data['totalregistros'] = $this->cobros_proyectados_view_model->count_all();
		$data['cantempresas'] = $this->cobros_proyectados_view_model->get_cant_empresas();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/cobrosproyectadoscorreos',$data);
		$this->load->view('templates/footer',$data);		
	}

	public function cobros_recolectados(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['totalpago'] = number_format($this->cobros_recolectados_view_model->sum_total(),2);
		$data['totalregistros'] = $this->cobros_recolectados_view_model->count_all();
		$data['cantempresas'] = $this->cobros_recolectados_view_model->get_cant_empresas();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/cobros_recolectados',$data);
		$this->load->view('templates/footer',$data);		
	}

	public function apc()	{
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/apc',$data);
		$this->load->view('templates/footer',$data);
	}

	public function refapc()	{
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/refapc',$data);
		$this->load->view('templates/footer',$data);
	}

	public function detalle_cargos()	{
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/detalle_cargos',$data);
		$this->load->view('templates/footer',$data);
	}

	public function cobros_recolectados_itbms(){
		$data['title'] = "Prestamos 911";
		$data['modulos_roles'] = $this->admin_model->consultarModuloByRol($this->roluser);
		$data['totalpago'] = number_format($this->cobros_recolectados_view_model->sum_total(),2);
		$data['totalregistros'] = $this->cobros_recolectados_view_model->count_all();
		$data['cantempresas'] = $this->cobros_recolectados_view_model->get_cant_empresas();
		$this->load->view('templates/header',$data);
		$this->load->view('admin/reportes/cobros_recolectados_itbms',$data);
		$this->load->view('templates/footer',$data);		
	}

	public function get_users(){
		$data = $this->usuarios_model->getUsuarios();
		$array = array();
		if ($data) {
			foreach ($data as $row) {
				if ($row->id > 0) {
					$array[] = $row;
				}				
			}
		}
		echo json_encode($array);
	}

	public function get_estados(){
		$data = $this->estados_model->get();
		echo json_encode($data);
	}

	public function list_desembolsos($fecha_i = NULL,$fecha_f = NULL){

		if ($fecha_i != NULL && $fecha_f != NULL) {
			$fecha_i = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$fecha_i)));
			$fecha_f = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$fecha_f)));
		}

		$list = $this->desembolsos_view_model->get_datatables($fecha_i,$fecha_f);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->fecha_entrega;
			$row[] = $fila->cliente;
			$row[] = number_format(round($fila->cantidad_aprobada,2),2);
			$row[] = $fila->cantidad_meses;
			$row[] = number_format(round($fila->deuda_fecha,2),2);
			$row[] = number_format(round($this->cobro_calculos($fila->id)['totalcobrar'],2),2);
			$row[] = $fila->empresa;
			$row[] = $fila->fecha_primer_descuento;
			$row[] = [
				round($fila->cantidad_aprobada,2),
				round($fila->deuda_fecha,2),
				round($this->cobro_calculos($fila->id)['totalcobrar'],2)
			];
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->desembolsos_view_model->count_all($fecha_i,$fecha_f),
			"recordsFiltered" => $this->desembolsos_view_model->count_filtered($fecha_i,$fecha_f),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function list_apc(){

		$list = $this->cli_ap_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->apel_paterno;
			$row[] = $fila->apel_materno;
			$row[] = $fila->primer_nom;
			$row[] = $fila->segundo_nom;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cli_ap_model->count_all(),
			"recordsFiltered" => $this->cli_ap_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function list_refapc(){

		$list = $this->ref_ap_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->apel_paterno;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->ref_ap_model->count_all(),
			"recordsFiltered" => $this->ref_ap_model->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function excel_apc() {
		to_excel($this->cli_ap_model->get_for_excel(), "apcexcel");
	}

	public function excel_refapc() {
		to_excel($this->ref_ap_model->get_for_excel(), "refapcexcel");
	}

	public function get_movimiento_by_users(){
		$fecha_i = $this->input->post('fecha_i');
		$fecha_f = $this->input->post('fecha_f');
		$id_user = $this->input->post('id_user');

		$fecha_i = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$fecha_i)));
		$fecha_f = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$fecha_f)));

		if ($id_user > 0) {
			$user = $this->usuarios_model->get_by_id($id_user);
			$user = $user->nombres . ' ' . $user->apellidos;
		} else {
			$user = NULL;
		}

		$array = array();
		$data_fecha_agente = $this->solicitudes_model->get_fecha_agente_by_users($fecha_i, $fecha_f, $user);
		if ($data_fecha_agente) {
			foreach ($data_fecha_agente as $row) {
				$cliente = $this->clientes_model->get_by_id($row->clientes_id);
				$estado = $this->estados_model->get_by_id($row->estados_id);
				$disposicion = $this->disposiciones_model->get_by_id($row->disposiciones_id);
				$row->clientes_id = $cliente->nombre . ' ' . $cliente->apellido;
				$row->estados_id = $estado->estado;
				if ($disposicion) {
					$row->disposiciones_id = $disposicion->nombre;
				} else {
					$row->disposiciones_id = '';
				}
				
			}
		}

		$data_fecha_verificacion = $this->solicitudes_model->get_fecha_verificacion_by_users($fecha_i, $fecha_f, $user);
		if ($data_fecha_verificacion) {
			foreach ($data_fecha_verificacion as $row1) {
				$cliente = $this->clientes_model->get_by_id($row1->clientes_id);
				$estado = $this->estados_model->get_by_id($row1->estados_id);
				$disposicion = $this->disposiciones_model->get_by_id($row1->disposiciones_id);
				$row1->clientes_id = $cliente->nombre . ' ' . $cliente->apellido;
				$row1->estados_id = $estado->estado;
				if ($disposicion) {
					$row1->disposiciones_id = $disposicion->nombre;
				} else {
					$row1->disposiciones_id = '';
				}
				
			}
		}

		$data_fecha_aprobado = $this->solicitudes_model->get_fecha_aprobado_by_users($fecha_i, $fecha_f, $user);
		if ($data_fecha_aprobado) {
			foreach ($data_fecha_aprobado as $row2) {
				$cliente = $this->clientes_model->get_by_id($row2->clientes_id);
				$estado = $this->estados_model->get_by_id($row2->estados_id);
				$disposicion = $this->disposiciones_model->get_by_id($row2->disposiciones_id);
				$row2->clientes_id = $cliente->nombre . ' ' . $cliente->apellido;
				$row2->estados_id = $estado->estado;
				if ($disposicion) {
					$row2->disposiciones_id = $disposicion->nombre;
				} else {
					$row2->disposiciones_id = '';
				}
				
			}
		}

		$data_fecha_firma = $this->solicitudes_model->get_fecha_firma_by_users($fecha_i, $fecha_f, $user);
		if ($data_fecha_firma) {
			foreach ($data_fecha_firma as $row3) {
				$cliente = $this->clientes_model->get_by_id($row3->clientes_id);
				$estado = $this->estados_model->get_by_id($row3->estados_id);
				$disposicion = $this->disposiciones_model->get_by_id($row3->disposiciones_id);
				$row3->clientes_id = $cliente->nombre . ' ' . $cliente->apellido;
				$row3->estados_id = $estado->estado;
				if ($disposicion) {
					$row3->disposiciones_id = $disposicion->nombre;
				} else {
					$row3->disposiciones_id = '';
				}
				
			}
		}

		$array['array_agente'] = $data_fecha_agente;
		$array['array_verificador'] = $data_fecha_verificacion;
		$array['array_aprobador'] = $data_fecha_aprobado;
		$array['array_firma'] = $data_fecha_firma;

		$records = FALSE;
		if (count($array['array_agente']) > 0 || count($array['array_verificador']) > 0 || count($array['array_aprobador']) > 0 || count($array['array_firma']) > 0) {
			$records = TRUE;
		}

		echo json_encode(array('data' => $array, 'records' => $records));
	}

	public function get_clientes_by_estado(){
		$estado = $this->input->post('estado');

		$data_clientes = $this->cobros_view_model->get_clientes_by_estado($estado);

		echo json_encode(array('estado' => $estado, 'data' => $data_clientes ));
	}

	public function get_clientes_by_estado_to_excel(){
		$estado = $this->input->get('estado');
		to_excel($this->cobros_view_model->get_clientes_by_estado_to_excel($estado), 
					"reporteClientes");
	}



	public function list_cantidad_solicitudes($arg = NULL,$fecha_i = NULL,$fecha_f = NULL)	{

		if ($fecha_i != NULL && $fecha_f != NULL) {
			$fecha_i = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$fecha_i)));
			$fecha_f = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$fecha_f)));
		}

		$list = $this->cantidad_solicitudes_view_model->get_datatables($arg,$fecha_i,$fecha_f);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $fila) {
			$no++;
			$row = array();
			$row[] = $fila->cliente;
			$row[] = $fila->fuente_cliente;
			$row[] = $fila->estado;
			$row[] = $fila->fecha_registro;
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->cantidad_solicitudes_view_model->count_all($arg,$fecha_i,$fecha_f),
			"recordsFiltered" => $this->cantidad_solicitudes_view_model->count_filtered($arg,$fecha_i,$fecha_f),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function consulta_cobros_proyectados(){

		$fecha_i = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_i'))));
		$fecha_f = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_f'))));

		$data = $this->cobros_proyectados_view_model->get_cobros_pendientes_by_fechas($fecha_i,$fecha_f);

		$group = NULL;
		if ($data) {
	 		//agrupar por aprobacion   -> dinamicamente 
			foreach($data as $i){
				$aprobacion = $this->aprobacion_model->get_by_id($i->id);
				$datacliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
				$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
				if ($desempleado) {
					$des = 'Desempleado';
				} else {
					$des = 'Activo';
				}
				$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);
				$group[$i->empresa][] = array(
					"aprobacion_id" => $aprobacion->id,
					"cliente" => $i->cliente,
					"aprobado" => $aprobacion->cantidad_aprobada,
					"letra" => $cotizacion['data'][0]['total'],
					"desembolsado" => $aprobacion->fecha_entrega,
					"primer_descuento" => $i->fecha_primer_descuento,
					"fecha_pendiente" => $i->fecha_pendiente,
					"monto" => $i->total_letra,
					"estado" => $des,
					"companias_id" => $datacliente->companias_id
				);
			}

			//sumar columnas - totales de reporte
			$precioFinal = 0;
			foreach($group as $k=> $i){
			    $precioGrupo = 0;
			    foreach($group[$k] as $r){
			        //acumulados
			        $precioGrupo += $r['monto'];                     
			       
			        //totales Finales
			        $precioFinal += $r['monto'];
			    }			     
			   
			    $group[$k]['acumulados'] = array("monto"=>number_format($precioGrupo,2));
			}
			 
			$group['TotalesFinales'] = array("monto"=>number_format($precioFinal,2));
		}

		echo json_encode($group);
	}

	public function recordatorio_pago(){
		$aprobaciones_id = $this->input->post('aprobaciones_id');
		$compania_nombre = NULL;
		$compania_correo = NULL;
		$clientes = array();
		foreach ($aprobaciones_id as $row) {
			$aprobacion = $this->aprobacion_model->get_by_id($row);
			$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
			$compania = $this->companias_model->get_by_id($cliente->companias_id);
			$cobro_proyectado = $this->cobros_proyectados_view_model->get_by_id($aprobacion->id);
			$compania_nombre = $compania->nombre;
			$compania_correo = $compania->correo;
			$clientes[] = $cobro_proyectado->cliente . '  ' . $cliente->cedula . '  B/.' . $cobro_proyectado->total_letra;
		}

		if ($compania_correo == NULL || $compania_correo == '') {
			echo json_encode(array('validation' => TRUE));
		} else {
			$cliente = '';
			if (count($clientes) > 0) {
				foreach ($clientes as $fila) {
					$cliente .= $fila . '<br>';
				}
			}

			$message = 'Señores <br><br>' . $compania_nombre . '<br><br> La presente tiene por objeto desearle éxitos en sus funciones diarias, y a la vez recordarles las retenciones que deben ser efectuadas a su(s) colaborador(es): <br><br>' . $cliente . '<br> Dichos pagos deben ser remitidos a Préstamos 911, S.A. mediante cheque o por ACH a la cuenta corriente de Banistmo no. 0112651568. <br><br> Sin mas que añadir, quedamos ante ustedes. <br><br> Atentamente, <br><br> Departamento de Cobros <br> Préstamos 911, S.A.';

			$this->email->from('cobros@prestamos911.com.', 'Prestamos 911');
			$this->email->to($compania_correo);
			$this->email->bcc('cobros@prestamos911.com');
			$this->email->subject('Recordatorio de Pago Prestamos 911');
			$this->email->message($message);

			$this->email->send();

			echo json_encode(array('status' => TRUE));
		}

	}

	public function detalle_pago(){
		$aprobaciones_id = $this->input->post('aprobaciones_id');
		$compania_nombre = NULL;
		$compania_correo = NULL;
		//$clientes = array();
		foreach ($aprobaciones_id as $row) {
			$aprobacion = $this->aprobacion_model->get_by_id($row);
			$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
			$compania = $this->companias_model->get_by_id($cliente->companias_id);
			//$cobro_proyectado = $this->cobros_proyectados_view_model->get_by_id($aprobacion->id);
			$compania_nombre = $compania->nombre;
			$compania_correo = $compania->correo;
			//$clientes[] = $cobro_proyectado->cliente . '  ' . $cliente->cedula . '  B/.' . $cobro_proyectado->total_letra;
		}

		if ($compania_correo == NULL || $compania_correo == '') {
			echo json_encode(array('validation' => TRUE));
		} else {

			$message = 'Señores <br><br>' . $compania_nombre . '<br><br> La presente tiene por objeto desearle éxito en sus funciones diarias y a la vez solicitarles nos remitan un detalle del pago recibido para así poder aplicar correctamente los pagos a sus colaboradores. <br><br> Agradeciendo de antemano la solicitud, quedamos ante ustedes. <br><br> Atentamente, <br><br> Departamento de Cobros <br> Préstamos 911, S.A.';

			$this->email->from('cobros@prestamos911.com.', 'Prestamos 911');
			$this->email->to($compania_correo);
			$this->email->bcc('cobros@prestamos911.com');
			$this->email->subject('Detalle de Pago Prestamos 911');
			$this->email->message($message);
			$this->email->send();

			echo json_encode(array('status' => TRUE));
		}
	}


	public function prestamo_cancelado(){
		$aprobacion_id = $this->input->post('aprobacion_id');

		$aprobacion = $this->aprobacion_model->get_by_id($aprobacion_id);
		$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
		$compania = $this->companias_model->get_by_id($cliente->companias_id);
		$compania_nombre = $compania->nombre;
		$compania_correo = $compania->correo;
		$nombre_cliente = $cliente->nombre . '  ' . $cliente->apellido;
		$cedula = $cliente->cedula;

		if ($compania_correo == NULL || $compania_correo == '') {
			echo json_encode(array('validation' => TRUE));
		} else {

			$message = 'Señores <br><br>' . $compania_nombre . '<br><br> A solicitud de la parte interesada, certificamos que el préstamo identificado con el No.' . $aprobacion->id . ', a nombre de ' . $nombre_cliente . ', con documento de identidad personal No. ' . $cedula . ', se encuentra cancelado y con saldo cero. <br><br> Esta información es dada exclusivamente a requerimiento del cliente, por lo tanto no con lleva responsabilidad alguna para nuestra institución financiera o alguno de nuestro personal. <br><br> Atentamente, <br><br> Departamento de Cobros <br> Préstamos 911, S.A.';

			$this->email->from('cobros@prestamos911.com.', 'Prestamos 911');
			$this->email->to($compania_correo);
			$this->email->bcc('cobros@prestamos911.com');
			$this->email->subject('Prestamo Cancelado Prestamos 911');
			$this->email->message($message);

			$this->email->send();

			echo json_encode(array('status' => TRUE));
		}

	}

	public function sin_respuesta($id){
		if ($this->roluser == 1 || $this->roluser == 2) {

			$cliente = $this->clientes_model->get_by_id($id);
			$nombre_cliente = $cliente->nombre . ' ' . $cliente->apellido;
			$correo_cliente = $cliente->correo;

			if ($correo_cliente != NULL || $correo_cliente != "") {
				$message = 'Señor(a) <br><br>' . $nombre_cliente . '<br><br> Le saludamos de Préstamos 911. Hemos tratado de comunicarnos con usted en referencia a la aplicación de préstamo que realizo con nosotros. Favor ponerse en contacto con nosotros mediante la línea 377-2911 o al Whatsapp 6619-5911, y así poder completar dicha solicitud. <br><br> Préstamos 911 se enorgullece de su aprobación inmediata y rápido desembolso. Con tan solo 3 meses de estar laborando y capacidad de descuento directo usted esta automáticamente pre-calificado. <br><br> Esperamos escuchar de usted prontamente y poder servirle. <br><br> Atentamente, <br><br> Préstamos 911, S.A.';
				
				$this->email->from('info@prestamos911.com.', 'Prestamos 911');
				$this->email->to($correo_cliente);
				$this->email->bcc('info@prestamos911.com');
				$this->email->subject('Ventas Prestamos 911');
				$this->email->message($message);
				$this->email->send();
			}

			echo json_encode(array('status' => TRUE));
		} else {
			echo json_encode(array('permission' => TRUE));
		}
	}

	public function morosos($id){
		if ($this->roluser == 1 || $this->roluser == 2) {

			$aprobacion = $this->aprobacion_model->get_by_id($id);

			$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
			$nombre_cliente = $cliente->nombre . ' ' . $cliente->apellido;
			$cedula = $cliente->cedula;
			$correo_cliente = $cliente->correo;

			$num_view = $this->aprobacion_model->get_num_view($aprobacion->id);

			if ($correo_cliente != NULL || $correo_cliente != "") {
				$message = 'Señor(a) <br><br>' . $nombre_cliente . '<br>' . $cedula . '<br><br> Le saludamos de Préstamos 911. Hemos tratado de comunicarnos con usted en referencia a la obligación de crédito contraída con nuestra institución la cual se encuentra en estado de morosidad con un saldo a la fecha de B/.' . $num_view->deuda_fecha . ' Favor ponerse en contactos con nuestro departamento de cobros mediante la línea 377-2911 opción 4 o al correo cobros@prestamos911.com. <br><br> Quedamos atentos a escuchar de usted y así ayudarle a solucionar la situación de crédito. <br><br> Atentamente, <br><br> Departamento de Cobros <br> Préstamos 911, S.A.';
				
				$this->email->from('cobros@prestamos911.com.', 'Prestamos 911');
				$this->email->to($correo_cliente);
				$this->email->bcc('cobros@prestamos911.com');
				$this->email->subject('Cobros Moroso Prestamos 911');
				$this->email->message($message);
				$this->email->send();
			}
			echo json_encode(array('status' => TRUE));
		} else {
			echo json_encode(array('permission' => TRUE));
		}

	}

	public function envio_correos_guiaa(){

		$aprobaciones_id = $this->input->post('aprobaciones_id');
		//$cliente_correo = array();
		//$empresa_correo = NULL;
		//$message = NULL;
		//$status = array();
		$status = NULL;
		foreach ($aprobaciones_id as $row) {
			$aprobacion = $this->aprobacion_model->get_by_id($row);
			$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
			$compania = $this->companias_model->get_by_id($cliente->companias_id);
			$cobro_proyectado = $this->cobros_proyectados_view_model->get_by_id($aprobacion->id);

			//$cliente_correo[] = $cliente->correo;		
			//$empresa_correo = $compania->correo;

			//$message = 'Buen día Sr/a. ' . $cobro_proyectado->cliente . ' el presente correo es para recordarles el pago con fecha del ' . date("d-m-Y", strtotime("$cobro_proyectado->fecha_pendiente - 1 months")) . ' por una letra de B/' . $cobro_proyectado->total_letra . ' Gracias por su colaboración. ' . $cliente->correo . ' ' . $compania->correo . ' Prestamos 911.';

			$message = 'Buen día Sr/a. ' . $cobro_proyectado->cliente . ' el presente correo es para recordarles el pago antes de la fecha del ' . date("d-m-Y", strtotime($cobro_proyectado->fecha_pendiente)) . ' por una letra de B/' . $cobro_proyectado->total_letra . ' Gracias por su colaboración. ' . $cliente->correo . ' ' . $compania->correo . ' Prestamos 911.';

			$this->email->from('nombre@correo.com', 'Prestamos 911');
			$this->email->to('nombre@correo.com');
			//$this->email->cc('nombre@correo.com');
			$this->email->bcc('nombre@correo.com');

			$this->email->subject('Recordatorio de Pago Prestamos 911');
			$this->email->message($message);



			if ($this->email->send()) {
				$status = TRUE;
			} else {
				$status = FALSE;
			}

			$data = array(
				'estado' => $status,
				'aprobacion_id' => $cobro_proyectado->id,
			);

			$this->correo_cobro_model->save($data);

		}

		//echo json_encode($status);
		echo json_encode(array('status' => TRUE));



	}

	public function ver_envio_correos(){
		$aprobaciones_id = $this->input->post('aprobaciones_id');

		$array = array();
		foreach ($aprobaciones_id as $row) {
			$correo_cobro = $this->correo_cobro_model->get_by_aprobacion_id($row);
			if ($correo_cobro) {
				foreach ($correo_cobro as $row1) {
					$row1->estado = ($row1->estado > 0) ? "Enviado" : "No enviado";
					$array[] = $row1;
				}
			}
		}		

		echo json_encode($array);
	}

	public function consulta_cobros_recolectados(){
		$fecha_i = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_i'))));
		$fecha_f = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_f'))));

		$data = $this->cobros_model->get_cobros_by_fechas($fecha_i,$fecha_f);

		$group = NULL;
		if ($data) {
	 		//agrupar por aprobacion   -> dinamicamente 
			foreach($data as $i){
				$aprobacion = $this->aprobacion_model->get_by_id($i->aprobacion_id);
				$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
				$empresa = $this->companias_model->get_by_id($cliente->companias_id)->nombre;
				$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
				if ($desempleado) {
					$des = 'Desempleado';
				} else {
					$des = 'Activo';
				}
				$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);
				$group[$empresa][] = array(
					"aprobacion_id" => $i->aprobacion_id,
					"cliente" => $cliente->nombre . ' ' . $cliente->apellido,
					"aprobado" => $aprobacion->cantidad_aprobada,
					"letra" => $cotizacion['data'][0]['total'],
					"cargo" => $cotizacion['cargo'],
					"comision" => $cotizacion['comision'],
					"gasto" => $cotizacion['gasto'],
					"desembolsado" => $aprobacion->fecha_entrega,
					"primer_descuento" => $aprobacion->fecha_primer_descuento,
					"fecha_pago" => $i->fecha_pago,
					"monto" => $i->monto,
					"estado" => $des
				);
			}

			//sumar columnas - totales de reporte
			$precioFinal = 0;
			foreach($group as $k=> $i){
			    $precioGrupo = 0;
			    foreach($group[$k] as $r){
			        //acumulados
			        $precioGrupo += $r['monto'];                     
			       
			        //totales Finales
			        $precioFinal += $r['monto'];
			    }			     
			   
			    $group[$k]['acumulados'] = array("monto"=>number_format($precioGrupo,2));
			}
			 
			$group['TotalesFinales'] = array("monto"=>number_format($precioFinal,2));
		}

		echo json_encode($group);
	}

	public function consulta_cobros_recolectados_itbms(){
		$fecha_i = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_i'))));
		$fecha_f = date('Y-m-d', strtotime(str_replace("/","-",$this->input->post('fecha_f'))));

		$data = $this->cobros_model->get_cobros_by_fechas($fecha_i,$fecha_f);

		$group = NULL;
		if ($data) {
	 		//agrupar por aprobacion   -> dinamicamente 
			foreach($data as $i){
				$aprobacion = $this->aprobacion_model->get_by_id($i->aprobacion_id);
				$cliente = $this->clientes_model->get_by_id($aprobacion->clientes_id);
				$empresa = $this->companias_model->get_by_id($cliente->companias_id)->nombre;
				$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
				if ($desempleado) {
					$des = 'Desempleado';
				} else {
					$des = 'Activo';
				}
				$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

				$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
				$tasa = $producto->tasa_interes_mensual;
				$porccargo = $producto->cargo_administrativo;
				$porcgasto = empty($producto->gastos_notariales) ? 0 : $producto->gastos_notariales;
				$porccomision = empty($producto->comision) ? 0 : $producto->comision;				
				
				/*$totalcargo = round($i->monto - ( $i->monto / (1 + ($porccargo / 100)) ),2);
				$totalgasto = round($i->monto - ( $i->monto / (1 + ($porcgasto / 100)) ),2);
				$totalcomision = round($i->monto - ( $i->monto / (1 + ($porccomision / 100)) ),2);*/
				$totalcargo = round($aprobacion->cantidad_aprobada * ($porccargo / 100),2);
				$totalgasto = round($aprobacion->cantidad_aprobada * ($porcgasto / 100),2);
				$totalcomision = round($aprobacion->cantidad_aprobada * ($porccomision / 100),2);

				$group[$empresa][] = array(
					"aprobacion_id" => $i->aprobacion_id,
					"cliente" => $cliente->nombre . ' ' . $cliente->apellido,
					"aprobado" => $aprobacion->cantidad_aprobada,
					"letra" => $cotizacion['data'][0]['total'],
					"cargo" => $cotizacion['cargo'],
					"comision" => $cotizacion['comision'],
					"gasto" => $cotizacion['gasto'],
					"desembolsado" => $aprobacion->fecha_entrega,
					"primer_descuento" => $aprobacion->fecha_primer_descuento,
					"fecha_pago" => $i->fecha_pago,
					"monto" => $i->monto,
					"estado" => $des,
					"desglose" => array(
									"porccargo" => $porccargo,
									"porcgasto" => $porcgasto,
									"porccomision" => $porccomision,
									"totalcargo" => number_format($totalcargo,2),
									"totalgasto" => number_format($totalgasto,2),
									"totalcomision" => number_format($totalcomision,2),
								),
					"cotizacion" => $cotizacion
				);
			}

			//sumar columnas - totales de reporte
			$precioFinal = 0;
			foreach($group as $k=> $i){
			    $precioGrupo = 0;
			    foreach($group[$k] as $r){
			        //acumulados
			        $precioGrupo += $r['monto'];                     
			       
			        //totales Finales
			        $precioFinal += $r['monto'];
			    }			     
			   
			    $group[$k]['acumulados'] = array("monto"=>number_format($precioGrupo,2));
			}
			 
			$group['TotalesFinales'] = array("monto"=>number_format($precioFinal,2));
		}

		echo json_encode($group);
	}

	public function cobro_calculos($id)	{ // en refinanciamiento, reportes tambien esta

		$aprobacion_id = $id;
		$approval = $this->aprobacion_model->get_by_id($aprobacion_id);

		$porcentaje = $approval->porcentaje;
		$suma = $approval->cantidad_aprobada;
		$productos_id = $approval->productos_id;
		$plazo = $approval->cantidad_meses;
		$fecha_primer_descuento = $approval->fecha_primer_descuento;

		//$fecha = date("d-m-Y");
		$fecha = date('Y-m-d', strtotime(str_replace("/","-",$fecha_primer_descuento)));

		$tasa = 0.00;
		$cargo = 0.00;

		if ($productos_id) {
			$products = $this->cotizacion_model->get_products_by_id($productos_id);
			foreach ($products as $row) {
				$tasa = $row->tasa_interes_mensual;
				$porccargo = $row->cargo_administrativo;
				$porcgasto = empty($row->gastos_notariales) ? 0 : $row->gastos_notariales;
				$porccomision = empty($row->comision) ? 0 : $row->comision;
				$cargo = $porccargo + $porcgasto + $porccomision;
			}
		}

		$suma = round($suma,2);
		$tasa = round($tasa,2);
		$cargo = round($cargo,2);

		$interes = $tasa / 100;

		$p = $suma;
		$x = $interes;
		$n = $plazo;

		$m = $p * ( $x / ( 1 - ( ( 1 + $x ) ** - $n ) ) );

		$cuotaperiodica = $m;
		$cuotaperiodica = round($cuotaperiodica,2);

		$cargototal = $cargo * ($suma / 100);
		$cargototal = round($cargototal,2);

		$pagado = array();	
		$interesasignado = array();

		$sum = $suma;
		for ($i=0; $i<$plazo; $i++) {
			$interesi = $sum * $interes;
			$interesi = round($interesi,2);
			$valori = $cuotaperiodica - $interesi;
			$valori = round($valori,2);
			$restoi = $sum - $valori;
			$restoi = round($restoi,2);
			$sum = $restoi;
			$pagado[$i] = $valori;
			$interesasignado[$i] = $interesi;
		}


		$totalintereses = array_sum($interesasignado);
		$totalintereses = round($totalintereses,2);
		$totalcargo = $cargototal * $plazo;
		$totalcargo = round($totalcargo,2);

		$totalsuma = array();
		$intereses = $totalcargo + array_sum($interesasignado);
		$intereses = round($intereses,2);
		$totalcobrar = array();
		$diferenciasuma = $suma - (array_sum($pagado));
		$diferenciasuma = round($diferenciasuma,2);

		if($diferenciasuma < 0){
			$dec = explode("-0.", $diferenciasuma);
			$num = $dec[1] * 1;
			$m = 0;
			for ($i=0; $i < $num; $i++) {
				$m++;
				$pagado[$plazo-$m] = $pagado[$plazo-$m] - 0.01;
			}
		} elseif($diferenciasuma > 0){
			$dec = explode("0.", $diferenciasuma);
			$num = $dec[1] * 1;
			for ($i=0; $i < $num; $i++) {
				$pagado[$i] = $pagado[$i] + 0.01;
			}
		}

		$data = array();
		$n = 0;
		for ($i=0; $i<$plazo; $i++){
			$n++;
			$fe = date("d-m-Y", strtotime("$fecha + $n months"));
			$data[] = array(
				'fecha' => $fe,
				'principal' => $pagado[$i],
				'interes' => $interesasignado[$i],
				'tasa' => $tasa*1,
				'cargo' => $cargototal,
				'total' => $pagado[$i] + $interesasignado[$i] + $cargototal
			);
			$totalsuma[] = $pagado[$i];
			$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototal;
		}

		$output = array(
			"data" => $data,
			"suma" => $suma,
			"intereses" => $intereses,
			"totalpagar" => array_sum($totalsuma) + $intereses,
			"totalprincipal" => array_sum($totalsuma),
			"totalcargo" => $totalcargo,
			"totalintereses" => $totalintereses,
			"totalcobrar" => array_sum($totalcobrar),
			"diferenciasuma" => $diferenciasuma
		);

		return $output;
	}

	public function cotizacion($suma,$productos_id,$plazo,$fecha = NULL)	{

		if ($fecha == NULL) {
			$fecha = date("d-m-Y");
		} else {
			$fecha = $fecha;
		}

		$tasa = 0.00;
		$cargo = 0.00;

		if ($productos_id) {
			$products = $this->cotizacion_model->get_products_by_id($productos_id);
			foreach ($products as $row) {
				$tasa = $row->tasa_interes_mensual;
				$porccargo = $row->cargo_administrativo;
				$porcgasto = empty($row->gastos_notariales) ? 0 : $row->gastos_notariales;
				$porccomision = empty($row->comision) ? 0 : $row->comision;
				$cargo = $porccargo + $porcgasto + $porccomision;				
			}
		}

		$suma = round($suma,2);
		$tasa = round($tasa,2);
		$cargo = round($cargo,2);

		$interes = $tasa / 100;
		//$interes = round($interes,2);

		//m = mensualidad, p = monto prestado, x = interes mensual, n = cantidad de meses;

		$p = $suma;
		$x = $interes;
		$n = $plazo;

		$m = $p * ( $x / ( 1 - ( ( 1 + $x ) ** - $n ) ) );

		$cuotaperiodica = $m;
		$cuotaperiodica = round($cuotaperiodica,2);

		$cargototal = $cargo * ($suma / 100);
		$cargototal = round($cargototal,2);

		$solocargo = $porccargo * ($suma / 100);
		$solocomision = $porccomision * ($suma / 100);
		$sologasto = $porcgasto * ($suma / 100);

		$solocargo = round($solocargo,2);
		$solocomision = round($solocomision,2);
		$sologasto = round($sologasto,2);
		$cargo = round($solocargo,2);
		$comision = round($solocomision,2);
		$gasto = round($sologasto,2);

		$pagado = array();	
		$interesasignado = array();

		$sum = $suma;
		for ($i=0; $i<$plazo; $i++) {
			$interesi = $sum * $interes;
			$interesi = round($interesi,2);
			$valori = $cuotaperiodica - $interesi;
			$valori = round($valori,2);
			$restoi = $sum - $valori;
			$restoi = round($restoi,2);
			$sum = $restoi;
			$pagado[$i] = $valori;
			$interesasignado[$i] = $interesi;
		}

		$totalintereses = array_sum($interesasignado);
		$totalintereses = round($totalintereses,2);
		$totalcargo = $cargototal * $plazo;
		$totalcargo = round($totalcargo,2);

		$solocargo = round($solocargo * $plazo ,2);
		$solocomision = round($solocomision * $plazo ,2);
		$sologasto = round($sologasto * $plazo ,2);

		$totalsuma = array();
		$intereses = $totalcargo + array_sum($interesasignado);
		$intereses = round($intereses,2);
		$totalcobrar = array();
		$diferenciasuma = $suma - (array_sum($pagado));
		$diferenciasuma = round($diferenciasuma,2);

		if($diferenciasuma < 0){
			$dec = explode("-0.", $diferenciasuma);
			$num = $dec[1] * 1;
			$m = 0;
			for ($i=0; $i < $num; $i++) {
				$m++;
				$pagado[$plazo-$m] = $pagado[$plazo-$m] - 0.01;
			}
		} elseif($diferenciasuma > 0){
			$dec = explode("0.", $diferenciasuma);
			$num = $dec[1] * 1;
			for ($i=0; $i < $num; $i++) {
				$pagado[$i] = $pagado[$i] + 0.01;
			}
		}

		$data = array();
		$n = 0;
		for ($i=0; $i<$plazo; $i++){
			$n++;
			$fe = date("d-m-Y", strtotime("$fecha + $n months"));
			$data[] = array(
				'fecha' => $fe,
				'principal' => $pagado[$i],
				'interes' => $interesasignado[$i],
				'tasa' => $tasa*1,
				'cargo' => $cargototal,
				'total' => $pagado[$i] + $interesasignado[$i] + $cargototal
			);
			$totalsuma[] = $pagado[$i];
			$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototal;
		}

		$output = array(
			"data"			 => $data,
			"suma"			 => $suma,
			"intereses"		 => $intereses,
			"totalpagar"	 => array_sum($totalsuma) + $intereses,
			"totalprincipal" => array_sum($totalsuma),
			"totalcargo"	 => $totalcargo,
			"totalintereses" => $totalintereses,
			"totalcobrar"	 => array_sum($totalcobrar),
			"diferenciasuma" => $diferenciasuma,
			"solocargo"		 => $solocargo,
			"solocomision"	 => $solocomision,
			"sologasto"		 => $sologasto,
			"cargo"		 	 => $cargo,
			"comision"	 	 => $comision,
			"gasto"		 	 => $gasto,
		);

		return $output;
	}
	
	function numtoletras($xcifra,$monetario = TRUE)
	{
	    $xarray = array(0 => "Cero",
	        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
	        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
	        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
	        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
	    );
		//
	    $xcifra = trim($xcifra);
	    $xlength = strlen($xcifra);
	    $xpos_punto = strpos($xcifra, ".");
	    $xaux_int = $xcifra;
	    $xdecimales = "00";
	    if (!($xpos_punto === false)) {
	        if ($xpos_punto == 0) {
	            $xcifra = "0" . $xcifra;
	            $xpos_punto = strpos($xcifra, ".");
	        }
	        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
	        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
	    }
	 
	    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
	    $xcadena = "";
	    for ($xz = 0; $xz < 3; $xz++) {
	        $xaux = substr($XAUX, $xz * 6, 6);
	        $xi = 0;
	        $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
	        $xexit = true; // bandera para controlar el ciclo del While
	        while ($xexit) {
	            if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
	                break; // termina el ciclo
	            }
	 
	            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
	            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
	            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
	                switch ($xy) {
	                    case 1: // checa las centenas
	                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
	                             
	                        } else {
	                            $key = (int) substr($xaux, 0, 3);
	                            if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
	                                $xseek = $xarray[$key];
	                                $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
	                                if (substr($xaux, 0, 3) == 100)
	                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
	                                else
	                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
	                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
	                            }
	                            else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
	                                $key = (int) substr($xaux, 0, 1) * 100;
	                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
	                                $xcadena = " " . $xcadena . " " . $xseek;
	                            } // ENDIF ($xseek)
	                        } // ENDIF (substr($xaux, 0, 3) < 100)
	                        break;
	                    case 2: // checa las decenas (con la misma lógica que las centenas)
	                        if (substr($xaux, 1, 2) < 10) {
	                             
	                        } else {
	                            $key = (int) substr($xaux, 1, 2);
	                            if (TRUE === array_key_exists($key, $xarray)) {
	                                $xseek = $xarray[$key];
	                                $xsub = $this->subfijo($xaux);
	                                if (substr($xaux, 1, 2) == 20)
	                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
	                                else
	                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
	                                $xy = 3;
	                            }
	                            else {
	                                $key = (int) substr($xaux, 1, 1) * 10;
	                                $xseek = $xarray[$key];
	                                if (20 == substr($xaux, 1, 1) * 10)
	                                    $xcadena = " " . $xcadena . " " . $xseek;
	                                else
	                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
	                            } // ENDIF ($xseek)
	                        } // ENDIF (substr($xaux, 1, 2) < 10)
	                        break;
	                    case 3: // checa las unidades
	                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
	                             
	                        } else {
	                            $key = (int) substr($xaux, 2, 1);
	                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
	                            $xsub = $this->subfijo($xaux);
	                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
	                        } // ENDIF (substr($xaux, 2, 1) < 1)
	                        break;
	                } // END SWITCH
	            } // END FOR
	            $xi = $xi + 3;
	        } // ENDDO
	 
	        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
	            $xcadena.= " DE";
	 
	        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
	            $xcadena.= " DE";
	 
	        // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
	        if (trim($xaux) != "") {
	            switch ($xz) {
	                case 0:
	                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
	                        $xcadena.= "UN BILLON ";
	                    else
	                        $xcadena.= " BILLONES ";
	                    break;
	                case 1:
	                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
	                        $xcadena.= "UN MILLON ";
	                    else
	                        $xcadena.= " MILLONES ";
	                    break;
	                case 2:
	                	if ($monetario == TRUE) {
		                    if ($xcifra < 1) {
		                        $xcadena = "CERO DÓLARES CON $xdecimales/100";
		                    }
		                    if ($xcifra >= 1 && $xcifra < 2) {
		                        $xcadena = "UN DÓLAR CON $xdecimales/100";
		                    }
		                    if ($xcifra >= 2) {
		                        $xcadena.= " DÓLARES CON $xdecimales/100"; //
		                    }
		                    break;
	                	}
	            } // endswitch ($xz)
	        } // ENDIF (trim($xaux) != "")
	        // ------------------      en este caso, para México se usa esta leyenda     ----------------
	        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
	        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
	        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
	        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
	        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
	        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
	        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
	    } // ENDFOR ($xz)
	    return trim($xcadena);
	}
	 
	// END FUNCTION
	function subfijo($xx)
	{ // esta función regresa un subfijo para la cifra
	    $xx = trim($xx);
	    $xstrlen = strlen($xx);
	    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
	        $xsub = "";
	    //
	    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
	        $xsub = "MIL";
	    //
	    return $xsub;
	}

	public function generar_paquete($id){		
		$paquetes_id = $this->input->get('paquetes_id');
		$paq = $this->paquetes_reportes_model->get_by_id($paquetes_id);
		$reportes = explode(',', $paq->reportes_ids);
		$copias_reportes = explode(',', $paq->reportes_copias);
		$modo = 'F';
		foreach ($reportes as $key => $value) {
			if ($value == 1) {
				for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "hojaverificacion-".$i.".pdf";
		        	$this->hoja_verificacion($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 2) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "autorizaciondescuento-".$i.".pdf";
		        	$this->autorizacion_descuento($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 3) {
		       	for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "condicionesgenerales-".$i.".pdf";
		        	$this->condiciones_generales($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 4) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "autorizacionrevision-".$i.".pdf";
		        	$this->autorizacion_revision($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 5) {
		    	for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "covermensajeria-".$i.".pdf";
		        	$this->cover_mensajeria($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 6) {
		    	for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "covermensajeriarefinianciamineto-".$i.".pdf";
		        	$this->cover_mensajeria_refinianciamineto($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 7) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "coveraprobacion-".$i.".pdf";
		        	$this->cover_aprobacion($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 8) {
		    	for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "autorizaciondescuentogobierno-".$i.".pdf";
		        	$this->autorizacion_descuento_gobierno($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 9) {
		    	for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "autorizaciondescuentoenblanco-".$i.".pdf";
		        	$this->autorizacion_descuento_en_blanco($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 10) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "contrato-".$i.".pdf";
		        	$this->contrato($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 11) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "pagare-".$i.".pdf";
		        	$this->pagare($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 12) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "nodesembolsado-".$i.".pdf";
		        	$this->no_desembolsado($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 13) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartanuevoempleador-".$i.".pdf";
		        	$this->carta_nuevo_empleador($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 14) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartanuevoempleadorverificardispo-".$i.".pdf";
		        	$this->carta_nuevo_empleador_verificar_dispo($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 15) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartanuevoempleadornodesc-".$i.".pdf";
		        	$this->carta_nuevo_empleador_no_desc($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 16) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartanuevoempleadorcontratodefinido-".$i.".pdf";
		        	$this->carta_nuevo_empleador_contrato_definido($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 17) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartasaldo-".$i.".pdf";
		        	$this->carta_saldo($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 18) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartanojudicialrecordatoriopago-".$i.".pdf";
		        	$this->carta_no_judicial_recordatorio_pago($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 19) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartanojudicialrecordatorioadmin-".$i.".pdf";
		        	$this->carta_no_judicial_recordatorio_admin($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 20) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartanojudicialgerentes-".$i.".pdf";
		        	$this->carta_no_judicial_gerentes($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 21) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "cartacancelacion-".$i.".pdf";
		        	$this->carta_cancelacion($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 22) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "recibopago-".$i.".pdf";
		        	$this->recibo_pago($id,$modo, $nombre_archivo);
		        }
		    } else if ($value == 23) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "constanciadevolucionintereses-".$i.".pdf";
		        	$this->constancia_devolucion_intereses($id,$modo, $nombre_archivo);
		        }
		    }else if ($value == 24) {
		        for ($i=1; $i <= $copias_reportes[$key]; $i++) { 
		        	$nombre_archivo = "condicionesgeneralesnew-".$i.".pdf";
		        	$this->condiciones_generales_new($id,$modo, $nombre_archivo);
		        }
		    }			
		}

		/*print_r($this->files);*/
		
		$pdf = new MyPDFMerger();
		try {
		  	foreach ($this->files as $key => $value) {
		  		$pdf->addPDF($value);		  	
		  	}

			$pdf->merge('browser', 'paquete'.$paquetes_id.'.pdf');
		} catch(Exception $e){
		  echo $e->getMessage();
		}
	}


	public function ver_hoja_verificacion($id){
		$this->hoja_verificacion($id,'I');	
	}

	public function generar_hoja_verificacion($id){
		$this->hoja_verificacion($id,'D');		
	}

	public function hoja_verificacion($id,$flag,$nombre = "hojaverificacion.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);


		if ($db_access) {
			$db_access_name = $db_access->nombre;
			$db_access_name = explode(".", $db_access_name);
			$db_access_name = $db_access_name[0];
		} else {
			$db_access_name = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}

		if ($datos_verificados) {
			$comentario_compania = $datos_verificados->comentario_compania;
			$comentario_css = $datos_verificados->comentario_css;
		} else {
			$comentario_compania = '<br><br><br><br><br><br><br><br>';
			$comentario_css = '<br><br><br><br><br><br><br><br>';
		}

		if ($aprobacion) {
			$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		}

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prestamos 911');
		$pdf->SetTitle('Control de Verificación');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		//$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(0,64,128));
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->setFontSubsetting(true);

		$pdf->SetFont('helvetica', '', 12, '', true);

		$pdf->AddPage();


		$pdf->setCellPaddings(0, 1, 0, 1);
		$pdf->setCellMargins(0, 1, 0, 1);
		$pdf->SetFillColor(255, 255, 127);


		$pdf->MultiCell(100, 5, 'CONTROL DE VERIFICACION', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(75, 5, 'Cliente # '. $cliente->id, 0, 'R', 0, 1, '', '', true);

		$pdf->Ln(4);
		$pdf->SetTextColor(0, 0, 0);
		//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>0, 'blend_mode'=>'Normal'));

		$txt = 'Agente: <u> '.$solicitud->agente.' </u> Fecha: <u> '.date('d/m/Y', strtotime(str_replace("/","-",$solicitud->fecha_verificacion))).' </u> Hora: <u> '.date('H:i a', strtotime(str_replace("/","-",$solicitud->fecha_verificacion))).' </u> Verificador: <u> '.$solicitud->verificador.' </u> Cliente: <u> '. $cliente->nombre . ' '. $cliente->apellido .' </u> Cédula: <u> '. $cliente->cedula .' </u> Celular: <u> '. $cliente->celular .' </u>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);


		$pdf->SetMargins(-1,0,0);		
		$pdf->Ln(4);
		$pdf->SetFillColor(0, 0, 0);
		$pdf->SetTextColor(255,255,255);
		$pdf->setCellPaddings(16, 1, 0, 1);
		$pdf->Cell(0, 0, "Verificación de Empresa", 0, 2, 'L', true);

		$pdf->Ln(4);		
		$pdf->SetTextColor(0, 0, 0);
		$check_false = '<img src="'.site_url('assets/images/check_false.png').'" width="12" height="12">';
		$check_true = '<img src="'.site_url('assets/images/check_true.png').'" width="12" height="12">';
		$deducciones = $cliente->descuento;
		if ($deducciones == "Si") {
			$checkno = $check_false;
			$checksi = $check_true;
			$descuento = ': <u> $'. $cliente->cantidad_descuento .' </u>';
		} else {
			$checkno = $check_true;
			$checksi = $check_false;
			$descuento = '';
		}

		if (isset($desempleado)) {
			$activo = $check_false;
			$noactivo = $check_true;
		} else {
			$noactivo = $check_false;
			$activo = $check_true;
		}

		if ($empresa->ruc == null || $empresa->ruc == "") {
			$empresa->ruc = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$txt1 = 'Nombre de Empresa: <u> '. $empresa->nombre .' </u> Año de constitución: <u> '. $empresa->ano_constitucion .' </u> Número de teléfono: <u> '. $empresa->telefono .' </u> Número patronal: <u> '. $empresa->ruc .' </u>  Cantidad de empleados en planilla: <u> '. $empresa->cantidad_empleados .' </u> Nombre del contacto: <u> '. $empresa->nombre_rrhh .' </u> Posición: <u> '. $cliente->posicion_trabajo .' </u> Inicio de labores: <u> '. date('d/m/Y', strtotime(str_replace("/","-",$cliente->inicio_labores))) .' </u> Salario: <u> $'. $cliente->salario .' </u> Deducciones: '.$checkno.' no '.$checksi.' si'. $descuento;
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, 0, true, 'J', true);
		$txt2 = 'Comentarios: ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'J', true);

		$pdf->SetMargins (15, 0, -1, false);
		$pdf->SetCellPadding(2);
		$pdf->Ln(2);
		$txt3 = $comentario_compania;
		$pdf->writeHTMLCell(0, 0, '', '', $txt3, 1, 1, false, true, 'J', true);

		$pdf->SetMargins(0,0,0);		
		$pdf->Ln(4);
		$pdf->SetFillColor(0, 0, 0);
		$pdf->SetTextColor(255,255,255);
		$pdf->setCellPaddings(16, 1, 0, 1);
		$pdf->Cell(0, 0, "Verificación en Seguro Social", 0, 2, 'L', true);

		$pdf->Ln(4);		
		$pdf->SetTextColor(0, 0, 0);
		$txt4 = 'Meses cotizados: <u> '.$db_access_name.' </u> Monto total cotizado: ____________ <br>Activo en Empresa: '.$noactivo.' no '.$activo.' si';
		$pdf->writeHTMLCell(0, 0, '', '', $txt4, 0, 1, 0, true, 'J', true);
		$txt5 = 'Comentarios: ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt5, 0, 1, 0, true, 'J', true);


		$pdf->SetMargins(15, 0, -1, false);
		$pdf->SetCellPadding(2);
		$pdf->Ln(2);
		$txt6 = $comentario_css;
		$pdf->writeHTMLCell(0, 0, '', '', $txt6, 1, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}

	}

	public function ver_autorizacion_descuento($id){
		$this->autorizacion_descuento($id,'I');	
	}

	public function generar_autorizacion_descuento($id){
		$this->autorizacion_descuento($id,'D');		
	}

	public function autorizacion_descuento($id,$flag,$nombre = "autorizaciondescuento.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);



		$check_false = '<img src="'.site_url('assets/images/check_false.png').'" width="12" height="12">';
		$check_true = '<img src="'.site_url('assets/images/check_true.png').'" width="12" height="12">';

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));


		

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);



		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 10, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();

		$pdf->setCellPaddings(0, 1, 0, 1);
		$pdf->setCellMargins(0, 1, 0, 1);
		$pdf->SetFillColor(255, 255, 127);

		$pdf->Cell(0, 0, 'AUTORIZACION DE DESCUENTO DIRECTO IRREVOCABLE', 0, 1, 'L', 0, '', 0);

		$txt = "Fecha: ".date('d/m/Y')." \nNúmero de Solicitud: ".$solicitud->id." \nEmpresa: ".$empresa->nombre;
		$pdf->MultiCell(71, 0, $txt, 0, 'L', false, 1, 125, 35, true, 0, false, true, 0, 'T', false);

		$pdf->MultiCell(100, 5, 'Estimados Señores :', 0, 'L', 0, 0, '', '', true);
																																																																																																																															
		$pdf->Ln(8);
		$pdf->SetTextColor(0, 0, 0);

		//$cuotas = $aprobacion->cantidad_meses + 1;
		$cuotas = $aprobacion->cantidad_meses;
		$mesmas = number_format(round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2),2);
		
		//$finaltotal = $cotizacion['totalcobrar'] + $mesmas;
		$finaltotal = $cotizacion['totalcobrar'];

		$txt = 'Por medio de la presente yo; '. $cliente->nombre . ' '. $cliente->apellido .', portador(a) de la cédula de identidad personal No. '.$cliente->cedula.', en ejercicio libre de mi voluntad y consentimiento y en pleno uso de mis facultades mentales y legales autorizo a ustedes, se sirvan descontar de mi salario total (incluye sueldo base, comisiones, aumentos, descuento mensual de vacaciones y el descuento correspondiente señalado, por el período de pago, de toda liquidacion que reciba del empleo, por separación temporal o definitiva del trabajo o cualquier otra causa), la suma de '.$this->numtoletras(round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2)).' (US$ '.number_format(round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2),2).') durante '.$cuotas.' mensualidades y remitir dicho descuento a PRÉSTAMOS 911, S.A., dentro de los primeros 5 días de cada mes, para asi cubrir la obligación de crédito comercial que he contraído con esta entidad.';

		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->Ln(2);
		$txt = 'Esta autorización es irrevocable y de forzoso cumplimiento, hasta cancelar la suma de: '.$this->numtoletras($finaltotal).' (US$ '. number_format($finaltotal,2) .')';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->Ln(2);
		$txt = 'Cualquier modificación a la misma requiere de el consentimiento por escrito de PRÉSTAMOS 911, S.A.';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->Ln(2);
		$txt = 'De ustedes y Atentamente,';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->Ln(4);

		$pdf->MultiCell(100, 0, 'Nombre del cliente: '. $cliente->nombre . ' '. $cliente->apellido, 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(80, 0, 'Por Préstamos 911, S.A.', 0, 'R', 0, 1, '', '', true);
		$pdf->MultiCell(100, 0, 'Cédula: '.$cliente->cedula, 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(80, 0, 'RUC155615545-2-2015 DV 8', 0, 'R', 0, 1, '', '', true);	


		$txt='El artículo del Código de Trabajo, modificado por el Decreto de Gabinete No.11 señala que los descuentos de préstamos bancarios y créditos comerciales, que autorice el trabajador son irrevocables y de forzoso cumplimiento para el empleador. El incumplimiento por parte del empleador de esta disposición, viola la norma contenida en el Artículo 128 numeral 20 del Código de Trabajo y acarrea para el infractor las sanciones que establece el Artículo 139 del Código de Trabajo.';
		$pdf->SetCellPadding(2);
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 1, 1, false, true, 'J', true);
		$pdf->Ln(2);

		$pdf->SetLineStyle(array('color' => array(254,000,000)));
		$pdf->SetFillColor(254,000,000);
		$pdf->SetTextColor(255,255,255);
		$pdf->setCellPaddings(0, 0, 0, 0);
		$pdf->setCellMargins(0, 0, 0, 0);
		$pdf->SetCellPadding(2);
		$pdf->Cell(0, 0, "PARA USO EXCLUSIVO DEL EMPLEADOR", 1, 2, 'C', true);


		$pdf->SetTextColor(0,0,0);
		$pdf->SetCellPadding(4);



		$txt = "Confirmamos a PRÉSTAMOS 911, S.A., que este descuento comenzará a efectuarse a partir de la quincena del: _______ de ____________________ de __________, y los pagos se realizaran mediante: <br/>".$check_false." ACH ".$check_false." Cheque <br/><br/> Aceptamos,<br/><br/> Firma: _________________________ <br/><br/> Nombre: _________________________ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   Sello de la empresa <br/><br/> Cargo: _________________________ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp;  &nbsp; &nbsp; No. Patronal:_________________________";

		$pdf->writeHTMLCell(0, 0, '', '', $txt, 1, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}

	}
	
	public function ver_condiciones_generales($id){
		$this->condiciones_generales($id,'I');	
	}

	public function generar_condiciones_generales($id){
		$this->condiciones_generales($id,'D');		
	}

	public function condiciones_generales($id,$flag,$nombre = "cotizacion.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$check_false = '<img src="'.site_url('assets/images/check_false.png').'" width="12" height="12">';
		$check_true = '<img src="'.site_url('assets/images/check_true.png').'" width="12" height="12">';

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prestamos 911');
		$pdf->SetTitle('Cotización');
		//$pdf->SetSubject('TCPDF Tutorial');
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 10, '', true);
		$pdf->AddPage();


		$pdf->setCellPaddings(0, 1, 0, 1);
		$pdf->setCellMargins(0, 1, 0, 1);
		$pdf->SetFillColor(255, 255, 127);

		$fecha = explode('-', date('d-m-Y'));
		$nummes = $fecha[1] - 1;
		$pdf->MultiCell(100, 5, 'Panamá, Rep. de Panamá '.$fecha[0].' de '.$this->meses[$nummes].' de '.$fecha[2], 0, 'L', 0, 0, '', '', true);

		$pdf->Ln(6);
		//$pdf->SetTextColor(0, 0, 0);

		$pdf->writeHTMLCell(0, 0, '', '', 'Señor/a <br/> '.$cliente->nombre . ' '. $cliente->apellido.' <br/> Ciudad: '.$cliente->provincia, 0, 1, false, true, 'L', true);

		$pdf->writeHTMLCell(0, 0, '', '', 'Estimado/a Señor/a: ', 0, 1, 0, true, 'L', true);

		$pdf->writeHTMLCell(0, 0, '', '', 'Nos complace confirmarles que le hemos aprobado un préstamo personal, bajo los siguientes términos y condiciones:', 0, 1, 0, true, 'J', true);

		$pdf->setCellPaddings(0, 0, 0, 0);		
		$pdf->setCellMargins(0, 0, 0, 0);
		$pdf->SetFillColor(255, 255, 127);

		$pdf->MultiCell(140, 5, 'Monto:', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(40, 5, 'USD $ '. $aprobacion->cantidad_aprobada, 0, 'L', 0, 1, '', '', true);

		$pdf->MultiCell(140, 5, 'Plazo:', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(40, 5, $aprobacion->cantidad_meses.' (meses)', 0, 'L', 0, 1, '', '', true);

		$pdf->MultiCell(140, 5, 'Pago Mensuales:', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(40, 5, 'USD $ '.number_format(round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2),2), 0, 'L', 0, 1, '', '', true);

		$pdf->setCellPaddings(5, 1, 0, 1);
		$pdf->setCellMargins(5, 1, 0, 1);

		$pdf->writeHTMLCell(0, 0, '', '', 'El pago mensual por el préstamo personal incluye:', 0, 1, 0, true, 'J', true);

		$pdf->setCellPaddings(5, 0, 0, 0);		
		$pdf->setCellMargins(5, 0, 0, 0);

		$pdf->MultiCell(125, 5, 'Total de Intereses:', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(40, 5, 'USD $ '.number_format($cotizacion['totalintereses'],2), 0, 'L', 0, 1, '', '', true);

		$pdf->MultiCell(125, 5, 'Gastos de Manejo, Legal, Administrativo y Comisión + ITBMS', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(40, 5, 'USD $ '. number_format($cotizacion['totalcargo'],2) , 0, 'L', 0, 1, '', '', true);

		$pdf->MultiCell(125, 5, 'Total', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(40, 5, 'USD $ '.number_format($cotizacion['totalcobrar'],2), 0, 'L', 0, 1, '', '', true);

		$pdf->setCellPaddings(0, 1, 0, 1);
		$pdf->setCellMargins(0, 1, 0, 1);

		$pdf->MultiCell(140, 5, 'Tasa de Interés:', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(40, 5, '% '.number_format($cotizacion['data'][0]['tasa'],2), 0, 'L', 0, 1, '', '', true);

		$pdf->setCellPaddings(0, 0, 0, 0);		
		$pdf->setCellMargins(0, 0, 0, 0);

		$tbl = '
		<style>
		table {
		border-collapse: collapse;
		}

		table,td{
		text-align: justify;



		}
		th.new{

		width: 110px;

		vertical-align: bottom;

		}

		</style>
		<table border="0"  align="center">
		<tr nobr="true">

		<th  id="new" class="new" align="left"></th>

		</tr>
		<tr nobr="true">
		<td  align="left"  >Condiciones:</td>
		<td  colspan="2" align="justify">1. La Financiera se reserva el derecho de cancelar el compromiso de préstamo personal contenido en esta carta, si a su juicio las condiciones y demás términos existentes al momento de su aprobación deterioren.</td>

		</tr>

		<tr nobr="true">
		<td  align="left">Otras Condiciones:</td>
		<td colspan="2" align="justify" >1. Forma de Pago: Descuento Directo. <br/>
		2. Actualización de la evidencia de ingresos presentada inicialmente será requerida en todos los
		casos donde hayan transcurrido 15 días desde la aprobación del préstamo y el préstamo se
		encuentre activo. Por lo antes expuesto, un ejecutivo del Departamento de Préstamo se pondrá
		en contacto con usted para solicitarle dicha documentación.<br/>
		3. Antes de la firma de la carta de términos y condiciones el cliente debe presentar evidencias de
		ingreso.<br/>
		4. Antes del desembolso deben validar que los teléfonos que se capturan son válidos y correctos.
		(Casa, Cel, oficina)</td>

		</tr>

		</table>';
		$pdf->writeHTML($tbl, true, false, false, false, '');


		$txt6 = 'Declará las partes que La Financiera, estará exenta de toda responsabilidad por el desistimiento de la aprobación final
		del préstamo, cuando estos desistimientos se deriven de los siguientes aspectos: La existencia de alguna modificación
		o cambio en las condiciones financieras o referencias de crédito del cliente, según el criterio exclusivo de La Financiera;
		b) Cuando las condiciones iniciales del cliente varían al momento de hacer la evaluación final del crédito. El cliente se
		compromete a no presentar reclamo o demanda alguna y se compromete a reembolsar a La Financiera, los gastos en
		que haya incurrido. <br/> <br/>
		Mucho agradeceremos se sirva firmar y devolver la copia adjunta a esta carta como aceptación a nuestro términos y
		condiciones en un plazo no mayor a 3 días. De no ser así, consideramos que el préstamo no será tomado y dejaremos
		sin efecto la aprobación.';
		$pdf->writeHTMLCell(0, 0, '', '', $txt6, 0, 1, false, true, 'J', true);

		$pdf->Ln(4);
		$txt6 = 'Atentamente,<br/> <br/> <br/> <br/> <br/> <br/> 

		Firma Autorizada &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Aceptado por: <br/> Préstamos 911';
		$pdf->writeHTMLCell(0, 0, '', '', $txt6, 0, 1, false, true, 'J', true);
		$pdf->MultiCell(120, 5, 'Fecha:', 0, 'R', 0, 1, '', '', true);	
		$pdf->SetTextColor(0, 0, 0);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}



	public function ver_autorizacion_revision($id){
		$this->autorizacion_revision($id,'I');	
	}

	public function generar_autorizacion_revision($id){
		$this->autorizacion_revision($id,'D');		
	}

	public function autorizacion_revision($id,$flag,$nombre = "autorizacionrevision.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$nombre_cliente = $cliente->nombre . ' ' . $cliente->apellido;

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prestamos 911');
		$pdf->SetTitle('Autorización Revisión');

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 10, '', true);
		$pdf->AddPage();


		$pdf->setCellPaddings(1, 1, 1, 1);
		$pdf->setCellMargins(1, 1, 1, 1);
		$pdf->SetFillColor(255, 255, 127);



		//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>0, 'blend_mode'=>'Normal'));

		$txt = ' <style>
		h3 {text-align:center;
		font-size: 14px;
		font-weight: bold;}
		p {text-align: justify;}

		</style><h3>AUTORIZACIÓN DE REFERENCIAS DE CRÉDITO</h3><br/> <p>Por este medio, autorizo expresamente y de manera irrevocable a Préstamos 911 y empresas
		afiliadas, sus representantes y/o agentes para consultar, aportar, actualizar y solicitar información
		sobre mi historial de crédito personal y/o de la empresa a la cual represento, en todas y cada una
		de las agencias de información de datos existentes o agentes económicos, en cualquier momento
		y a su entera discreción, sin ser necesaria una autorización expresa cada vez que sea indispensable
		la obtención de dichas referencias. <br/> <br/><br/>

		De igual forma autorizo a intercambiar mi historial de crédito con otros agentes económicos.
		Préstamos 911 queda relevado de toda responsabilidad si la información recibida no se ajusta a la
		realidad o es manejada en forma inapropiada por las personas autorizadas para proporcionarla.
		Igualmente Préstamos 911, no será responsable si la información suministrada es manejada en
		forma inapropiada por la persona que la recibe o solicita. <br/> <br/><br/>

		Queda establecido que los datos sobre el historial de crédito recopilado y consultado serán
		utilizados por Prestamos 911 como parte de la información requerida para evaluar y dar
		seguimiento a facilidad (es) crediticia (s) que actualmente tramito y/o mantengo con este grupo
		de empresas.<br/><br/><br/><br/>
		</p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);

		$pdf->Ln(20);

		$txt = '<table border="0" cellpadding="5">
			<tr>
				<td align="center"><u>&nbsp;&nbsp;&nbsp;'. $nombre_cliente .'&nbsp;&nbsp;&nbsp;</u></td>
				<td align="center">_________________________</td>
			</tr>
			<tr>
				<td align="center">Nombre</td>
				<td align="center">Firma y Nº de Cédula: ' . $cliente->cedula . '</td>
			</tr>
		</table>';

		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);

		$pdf->Ln(30);

		$txt = 'Fundamento Legal: Ley N° 24 del 22 de mayo de 2002, Modificada por la Ley N° 14 de 2006';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}


	public function ver_cover_mensajeria($id){
		$this->cover_mensajeria($id,'I');	
	}

	public function generar_cover_mensajeria($id){
		$this->cover_mensajeria($id,'D');		
	}

	public function cover_mensajeria($id,$flag,$nombre = "covermensajeria.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prestamos 911');
		$pdf->SetTitle('Cover Mensajeria');

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 10, '', true);
		$pdf->AddPage();


		$pdf->setCellPaddings(0, 0, 0, 0);
		$pdf->setCellMargins(0, 0, 0, 0);
		$pdf->SetFillColor(255, 255, 127);

		$pdf->writeHTMLCell(0, 0, '', '', date('d/m/Y'), 0, 1, false, true, 'R', true);
		$pdf->writeHTMLCell(0, 0, '', '', $empresa->nombre_rrhh, 0, 1, false, true, 'L', true);
		$pdf->writeHTMLCell(0, 0, '', '', $empresa->nombre, 0, 1, false, true, 'L', true);
		$pdf->writeHTMLCell(0, 0, '', '', $empresa->direccion, 0, 1, false, true, 'L', true);

		$tbl = '
		<style>
		table {
			border-collapse: collapse;
		}

		table,td{
			text-align: justify;
		}
		th.new{
			width: 110px;
			vertical-align: bottom;
		}

		</style>
		<table border="0"  align="center">
			<tr nobr="true">
				<th  id="new" class="new" align="left"></th>
			</tr>
			<tr nobr="true">
				<td  align="left"  >En referencia a:</td>
				<td  colspan="2" align="left">'.$cliente->nombre . ' ' . $cliente->apellido.' <br/> '.$cliente->cedula . '</td>
			</tr>
		</table>
		';

		$pdf->writeHTML($tbl, true, false, false, false, '');


		$txt2 = 'Estimado/a Sr/a <br/> '. $empresa->nombre_rrhh . '<br/>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'J', true);

		$pdf->setCellPaddings(0, 1, 0, 1);
		$pdf->setCellMargins(0, 1, 0, 1);

		$txt6 = 'Por medio de la presente queremos hacer de su conocimiento el préstamo personal otorgado a su colaborador quien nos
		ha autorizado a procesar los pagos mediante descuento directo. Adjunto encontrara la documentación la cual requiere ser
		completada, en conjunto con su firma y sello de empresa. <br/> <br/>
		Los pagos deben ser efectuados a mas tardar el quinto (5) día de cada mes, mediante cheque a nombre de “PRESTAMOS 911, S.A.”, o por ACH a nuestra cuenta corriente de Banistmo con No. “0112651568”, ruta y tránsito 000000026. <br/> <br/> <br/>
		De utilizar la opción de ACH, favor enviar email con comprobante de transferencia y detalle de pago a
		cobros@prestamos911.com, para asi poder aplicar los pagos correctamente y a tiempo.
		Cualquier consulta que tenga, puede comunicarse con el Departamento de Cobros al teléfono 377-2911 opción 4. <br/> <br/> <br/>
		La copia adjunta de la autorización de descuento directo será retirada en un periodo de tres días.<br/> <br/> <br/>
		Agradecemos su atención a la misma. <br/> <br/> <br/> <br/>
		Atentamente, <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
		Por: Préstamos 911, S.A
		';
		$pdf->writeHTMLCell(0, 0, '', '', $txt6, 0, 1, false, true, 'J', true);


		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}


	public function ver_cover_mensajeria_refinianciamineto($id){
		$this->cover_mensajeria_refinianciamineto($id,'I');
	}

	public function generar_cover_mensajeria_refinianciamineto($id){
		$this->cover_mensajeria_refinianciamineto($id,'D');
	}

	public function cover_mensajeria_refinianciamineto($id,$flag,$nombre = "covermensajeriarefinanciamiento.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);


		$cadenaarray = explode('-', $cliente->cedula);
		$array = array();
		for ($i=0; $i < count($cadenaarray); $i++) { 
			if(strpos($cadenaarray[$i], '0')==0){
				$array[] = substr($cadenaarray[$i],1);				
			}
		}

		$cliente->cedula = $array[0] . '-' . $array[1] . '-' . $array[2];

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prestamos 911');
		$pdf->SetTitle('Cover Mensajeria Refinanciamiento');

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);
		// set header and footer fonts

		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 10, '', true);
		$pdf->AddPage();


		$pdf->setCellPaddings(0, 0, 0, 0);
		$pdf->setCellMargins(0, 0, 0, 0);
		$pdf->SetFillColor(255, 255, 127);



		$pdf->writeHTMLCell(0, 0, '', '', date('d/m/Y'), 0, 1, false, true, 'L', true);
		$pdf->writeHTMLCell(0, 0, '', '', $empresa->nombre_rrhh, 0, 1, false, true, 'L', true);
		$pdf->writeHTMLCell(0, 0, '', '', $empresa->nombre, 0, 1, false, true, 'L', true);
		$pdf->writeHTMLCell(0, 0, '', '', $empresa->direccion, 0, 1, false, true, 'L', true);


		$tbl = '
		<style>
			table {
			    border-collapse: collapse;
			}

			table,td{
				text-align: justify;


			    
			}
			th.new{
				
			 width: 110px;
			 
				vertical-align: bottom;

			}

			</style>
			<table border="0"  align="center">
			 <tr nobr="true">

			  <th  id="new" class="new" align="left"></th>
				
			 </tr>
			 <tr nobr="true">
			  <td  align="left"  >En referencia a:</td>
			  <td  colspan="2" align="left">'.$cliente->nombre . ' ' . $cliente->apellido.'  <br/>  '.$cliente->cedula . '</td>
			  
			 </tr>
			 
			 
			</table>
		';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$txt2 = 'Estimado/a Sr/a <br/>'. $empresa->nombre_rrhh . '<br/>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'J', true);


		$pdf->setCellPaddings(0, 1, 0, 1);
		$pdf->setCellMargins(0, 1, 0, 1);

		$txt6 = 'Por medio de la presente queremos hacer de su conocimiento el refinanciamiento otorgado a su colaborador quien nos ha
			autorizado a procesar los pagos mediante descuento directo. Adjunto encontrara la documentación la cual requiere su firma
			y sello de empresa. <br/> <br/>
			La aceptación de esta carta, será condicionada a que se envié carta de cancelación del préstamo previo que posee el
			trabajador con nuestra institución financiera. <br/> <br/> <br/>
			Los pagos deben ser efectuados a mas tardar el quinto (5) día de cada mes, mediante cheque a nombre de “PRESTAMOS
				911, S.A.”, o por ACH a nuestra cuenta corriente de Banistmo con No. “0112651568”, ruta y tránsito 000000026. <br/> <br/> <br/>
				De utilizar la opción de ACH, favor enviar email con comprobante de transferencia y detalle de pago a
			cobros@prestamos911.com, para asi poder aplicar los pagos correctamente y a tiempo.
			Cualquier consulta que tenga, puede comunicarse con el Departamento de Cobros al teléfono 377-2911 opción 4. <br/> <br/> <br/>
			La copia adjunta de la autorización de descuento directo será retirada en un periodo de tres días.<br/> <br/> <br/>
			Agradecemos su atención a la misma. <br/> <br/> <br/> <br/>
			Atentamente, <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
			Por: Préstamos 911, S.A
			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt6, 0, 1, false, true, 'J', true);


		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}

	public function ver_cover_aprobacion($id){
		$this->cover_aprobacion($id,'I');	
	}

	public function generar_cover_aprobacion($id){
		$this->cover_aprobacion($id,'D');		
	}

	public function cover_aprobacion($id,$flag,$nombre = "coveraprobacion.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prestamos 911');
		$pdf->SetTitle('Cover Aprobación');

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', 'B', 14);

		// add a page
		$pdf->AddPage();
		$pdf->Write(0, 'FORMULARIO DE SOLICITUD DE PRESTAMO', '', 0, 'C', true, 0, false, false, 0);


		$pdf->SetFont('helvetica', '', 10);

		// -----------------------------------------------------------------------------
		    	
		$check_false = '<img src="'.site_url('assets/images/check_false.png').'" width="12" height="12">';
		$check_true = '<img src="'.site_url('assets/images/check_true.png').'" width="12" height="12">';

		if ($cliente->descuento == "Si") {
			$checkno = $check_false;
			$checksi = $check_true;
		} else {
			$checkno = $check_true;
			$checksi = $check_false;
		}


		$tbl = '
			<style>
			 td {
		    	 border: 1px solid black;

		    	font-weight: normal;
			    font-size: 110%;
		    
			}

			th {
			    height: 50px;
			    font-weight: bold;
			    font-size: 110%;
			    background-color: #A9A9A9;
    			color: #FFFFFF;
    			border: 1px solid gray;
			     
			}
						  
			


			</style>
			<table    align="center" cellpadding="2">
			 <tr nobr="true">
			   <th colspan="4"  ><br/> <br/> Datos del Solicitante</th>
			    
    			</tr>
			 <tr nobr="true">
			  <td  colspan="2" align="left"> Nombre: '.$cliente->nombre.' </td>
			  
			  <td  colspan="2" align="left"> Apellido: '.$cliente->apellido.'</td>
			  
			 </tr>
			  <tr nobr="true">
			  <td  align="left"> Cédula: '.$cliente->cedula.'</td>
			  <td  align="left"> Teléfono: '.$cliente->telefono.' </td>
			  <td colspan="2"  align="left"> Estado Civil: '.$cliente->estado_civil.'</td>
			 </tr>
			 <tr nobr="true">
			  <td colspan="4" align="left"> Dirección: '.$cliente->direccion.'</td>
			  </tr>
			 <tr nobr="true">
			  <td colspan="4" align="left" > Lugar de Trabajo: '.$empresa->nombre.'</td>
			  </tr>
			 <tr nobr="true">
			  <td colspan="4" align="left" > Tel. Oficina: '.$empresa->telefono.' &nbsp; &nbsp; &nbsp; Tiempo Laborando: '.$cliente->tiempo_laborando.' &nbsp; &nbsp; &nbsp; Salario: $'.$cliente->salario.'</td>
			  
			 </tr>
			 
			<tr>
			  <td colspan="2" align="left" > Tiene usted algún descuento directo:   Si '.$checksi.' No '.$checkno.' </td>
			   <td colspan="2" align="left" > Monto Mensual: $'.$cliente->cantidad_descuento.'</td>
			  
			 </tr>


			 
			</table>
		';
		$pdf->writeHTML($tbl, true, false, false, false, '');

		// -----------------------------------------------------------------------------

		// NON-BREAKING ROWS (nobr="true")

		$tbl = '
			<style>
			td {
		    	 border: 1px solid black;
		    	font-weight: normal;
			    font-size: 110%;
			}
			th {
			    height: 50px;
			    font-weight: bold;
			    font-size: 110%;
			    background-color: #A9A9A9;
    			color: #FFFFFF;
    			border: 1px solid gray;
			     
			}


			</style>
			<table  align="center" cellpadding="2">
			 <tr nobr="true">
			   <th colspan="4" ><br/> <br/> Referencia Personal</th>
			    
    			</tr>
			 <tr nobr="true">
			  <td  colspan="2" align="left"> Nombre: '.$cliente->nombre_referencia.' </td>
			  
			  <td  colspan="2" align="left"> Apellido: '.$cliente->apellido_referencia.'</td>
			  
			 </tr>
			 
			 <tr nobr="true">
			  <td  colspan="2" align="left"> Parentesco: '.$cliente->parentesco_referencia.'</td>
			  
			  <td  colspan="2" align="left"> Teléfono: '.$cliente->telefono_referencia.'</td>
			  
			 </tr>
			 <tr nobr="true">
			  <td  colspan="4" align="left"> Dirección: '.$cliente->direccion_referencia.'</td>
			  
			 </tr>
			 
			</table>
		';

		$pdf->writeHTML($tbl, true, false, false, false, '');
		$pdf->writeHTML($check_false . ' Certifico que la información dada a la empresa es correcta y de mi entero consentimiento', true, false, false, false, 'C');


		$pdf->Ln(6);
		$pdf->SetFont('helvetica', 'B', 12);

		$hoy = date('d/m/Y');
		

		$pdf->MultiCell(100, 5, 'Firma del Solicitante: _____________________', 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(75, 5, 'Fecha: '. $hoy, 0, 'R', 0, 1, '', '', true);
		$pdf->Ln(8);
		
		$tbl = '
			<style>
			td {
		    	 border: 1px solid black;
		    	 font-weight: normal;
			    font-size: 90%;
		    
			}
			th {
			     height: 50px;
			    font-weight: bold;
			    font-size: 90%;
			    background-color: #A9A9A9;
				color: #FFFFFF;
				border: 1px solid gray;
			     
			}
			td.box{
			     height: 300px;
			    
			     
			}

			</style>
			<table      align="center">
			 <tr nobr="true">
			   <th colspan="4" > <br/> <br/> Para Uso Interno</th>
			    
				</tr>
			 <tr nobr="true">
			  <td  colspan="2" align="left"> Representante: '.$solicitud->agente.'</td>
			  
			  <td  colspan="2" align="left"> Monto de Préstamo: $'.$aprobacion->cantidad_aprobada.'</td>
			  
			 </tr>
			 
			 <tr nobr="true">
			  <td  colspan="2" align="left"> Agente Verificador: '.$solicitud->verificador.'</td>
			  
			  <td  colspan="2" align="left"> Término de Préstamo: '.$aprobacion->cantidad_meses.' meses</td>
			  
			 </tr>
			 <tr nobr="true">
			  <td  colspan="2" align="left"> Aprobado por: '.$solicitud->aprobador.'</td>
			  <td  colspan="2" align="left"> Pago Mensuales: $'. round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2).'</td>
			 
			  
			 </tr>
			 <tr nobr="true">
			  <td class="box" colspan="4" align="left"></td>
			  
			 </tr>
			 
			</table>
		';
			
		$pdf->writeHTML($tbl, true, false, false, false, '');

		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetTextColor(156, 156, 156);

		$pdf->writeHTML('Este documento es propiedad de Préstamos 911, S.A. esta prohibida su reproducción parcial o total', true, false, false, false, 'C');

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}


	public function ver_autorizacion_descuento_gobierno($id){
		$fecha = $this->input->get('fecha');
		$this->autorizacion_descuento_gobierno($id,'I', "autorizaciondescuentogobierno.pdf", $fecha);	
	}

	public function generar_autorizacion_descuento_gobierno($id){
		$fecha = $this->input->get('fecha');
		$this->autorizacion_descuento_gobierno($id,'D', "autorizaciondescuentogobierno.pdf", $fecha);			
	}

	public function autorizacion_descuento_gobierno($id,$flag,$nombre = "autorizaciondescuentogobierno.pdf",$fecha = NULL){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);


		$cadenaarray = explode('-', $cliente->cedula);
		$array = array();
		for ($i=0; $i < count($cadenaarray); $i++) { 
			if(strpos($cadenaarray[$i], '0')==0){
				$array[] = substr($cadenaarray[$i],1);				
			}
		}

		$cliente->cedula = $array[0] . '-' . $array[1] . '-' . $array[2];

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);
		//$pdf=new Pdf('P','pt','LEGAL');

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prestamos 911');
		$pdf->SetTitle('Autorización de Descuento Gobierno');

		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);


		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(10, 12, 15);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 1);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 10);

		// add a page

		$pdf->AddPage('P','LEGAL');

		$tblf1 = '
		<table border="0" width="100%" style="padding:6px;">
			<tr>
				<td align="center"></td>
				<td align="center" style="background-color: #A9A9A9; border: 1px solid black;"><h2>F1</h2></td>
			</tr>
		</table>';


		$tbl = '
		<table border="0" width="100%">
			<tr>
				<td align="center" width="20%">CLAVE <b style="font-size:12px;">M-129</b> <br> NÚM. _____ <br> FECHA '.date('d/m/Y').'</td>
				<td align="center" width="60%">CONTRALORÍA GENERAL DE LA REPÚBLICA <br> DIRECCIÓN DE MÉTODOS Y SISTEMAS DE CONTABILIDAD <br> AUTORIZACIÓN DE DESCUENTOS VOLUNTARIOS </td>
				<td align="right" width="20%" >'.$tblf1.'</td>
			</tr>
		</table>';

		$pdf->writeHTML($tbl, true, false, false, false, '');

		$pdf->writeHTMLCell(0, 0, '', '', '<h2>PRESTAMOS 911</h2>', 0, 1, false, true, 'C', true);
		$pdf->ln(4);

		$pdf->writeHTMLCell(0, 0, '', '', 'Núm. de Doc. Recibido __________', 0, 1, false, true, 'R', true);
		$pdf->ln(4);


		$porc = 3 / 100;
		$serviciodescuento = round($cotizacion['totalcobrar'] * $porc, 2); //(($cotizacion['totalcobrar'] * $porc) + $cotizacion['totalcobrar']) * $porc;
		$comisioncierre = $cotizacion['totalcargo'] + $serviciodescuento;
		$comisioncierre = round($comisioncierre ,2);

		$mgt = $cotizacion['totalintereses'] + $comisioncierre;

		$mot = $aprobacion->cantidad_aprobada + $mgt;

		$quincenal = round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses / 2, 2);
		$plazoxquincena = $aprobacion->cantidad_meses * 2;

		$dias = $aprobacion->cantidad_meses * 30;

		$ultimodescuento = $mot  - ($quincenal * $plazoxquincena);

		$discount = round($mot * $porc, 2);

		$total_cierre = $cotizacion['totalcargo'] + $discount;

		$interes = $mgt - $total_cierre;


		$tab2 = '

		<style>
			.tab2 {
				font-weight: Bold;
			}

			td.area
			{

				width: 6%;
				border: 1px solid black;
			}

			td.min
			{

				width: 6%;
				border: 1px solid black;
			}

			td.pla
			{

				width: 6%;
				border: 1px solid black;
			}

			td.pos
			{

			width: 6%;
			border: 1px solid black;
			}
			td.cedula
			{

				width: 16%;
				border: 1px solid black;
				font-family:Verdana, Geneva, sans-serif;
			}

			td.tipo
			{

				width: 8%;
				border: 1px solid black;
				font-family:Verdana, Geneva, sans-serif;
			}

			td.deuda{

				width: 4%;
				border: 1px solid black;
				font-size: 75%;
			}

			td.general
			{

				width: 40%;
				border: 1px solid black;
				font-family:Verdana, Geneva, sans-serif;
			}

			th.nombre
			{

				width: 25%;
				border: 1px solid black;
				font-family:Verdana, Geneva, sans-serif;
			}

			td.obligacion
			{

				width: 40%;
				border: 1px solid black;
				font-family:Verdana, Geneva, sans-serif;
			}

			td.monto
			{

				width: 16%;
				border: 1px solid black;
				font-family:Verdana, Geneva, sans-serif;
			}

			td.desc
			{

				width: 16%;
				border: 1px solid black;
				font-family:Verdana, Geneva, sans-serif;
			}

			td.fila1
			{

				height:0%;
				border-button:none;
				border-top:none;
			}

			td.funcionario{

				border: 1px solid black;
			}

			td.montocliente
			{

				width: 16%;
				border: 1px solid black;
				font-weight: Normal;
				font-family:Verdana, Geneva, sans-serif;
			}

			td.desccliente
			{

				width: 16%;
				border: 1px solid black;
				font-weight: normal;
				font-family:Verdana, Geneva, sans-serif;
			}

			td.funcionario{
				border: 1px solid black;
				font-weight: normal;
				font-family:Verdana, Geneva, sans-serif;
				width: 25%;

			}

			td.cedulacliente
			{

				width: 16%;
				border: 1px solid black;
				font-weight: Normal;
				font-family:Verdana, Geneva, sans-serif;
			}


		</style>


		<table class="tab2">

			<tr nobr="true">

			<th rowspan="3" align="center" class="nombre"> <br/><br/>   <br/>   <br/>NOMBRE <br/> DEL FUNCIONARIO <br/>
			<br/>   <br/>     </th >	
			<td rowspan="2" align="center" class="general">   <br/>   <br/>GENERALES </td >							 
			<td class="obligacion" align="center" >OBLIGACIÓN</td>
			</tr>
			<tr nobr="true">

			<td class="tipo" align="center" style="background-color: #A9A9A9;"> TIPO</td>
			<td rowspan="2"class="monto" align="center" > <br/> <br/> <br/> <br/> MONTO TOTAL</td>
			<td rowspan="2"class="desc"  align="center" ><br/><br/>  <br/> <br/>DESCUENTO <br/> QUINCENAL</td>

			</tr>
			<tr nobr="true">

			<td  rowspan="" class="area" ><br><br> A<br> R<br> E<br> A </td>						 
			<td class="min" rowspan=""><br><br> M<br> I<br> N</td>
			<td class="pla" rowspan=""><br><br> P<br> L<br> A</td>
			<td class="pos"rowspan=""><br><br> P<br> O<br> S</td>
			<td rowspan="" class="cedula" align="center"> <br/><br/> <br/>CÉDULA</td>
			<td rowspan="" class="deuda"><br><br> D<br> E<br> U<br> D<br> O<br> R</td>
			<td rowspan="" class="deuda"><br><br> F<br> I<br> A<br> D<br> O<br> R</td>
			</tr>
			<tr nobr="true">
			<td class="funcionario">'.$cliente->nombre.' '.$cliente->apellido.'</td>
			<td class="area" style="font-weight: Normal; font-size: 10px; padding-top: 2px;" > '.$cliente->area.' </td >
			<td class="min" style="font-weight: Normal; font-size: 10px; padding-top: 2px;" > '.$cliente->ministerio.'</td>	
			<td class="pla" style="font-weight: Normal; font-size: 10px; padding-top: 2px;" > '.$cliente->planilla.'</td>
			<td class="pos" rowspan="4"  style="font-weight: Normal; font-size: 10px; padding-top: 2px;"> '.$cliente->posicion.'</td>
			<td class="cedulacliente" align="center">'.$cliente->cedula.'</td>
			<td class="deuda">X</td>
			<td class="deuda"> </td>
			<td class="montocliente">B/. '.round($mot,2).'</td>
			<td class="desccliente">B/. '.round($quincenal,2).'</td>


			</tr>

		</table>

		';

		$pdf->SetMargins(11, 15, 15);

		$pdf->writeHTMLCell(0, 0, '', '', $tab2, 0, 1, false, true, 'C', false);
		$pdf->Ln(4);

		$pdf->SetMargins(10, 15, 15);

		$tab3 = '
			<style>
			table.tab3, td.tab3, th.tab3 {
				font-weight: bold;
			    font-size: 90%;
								
			}
			table.tab3{
								border-collapse: collapse;
			}
			th.app
			{
				 
				width: 20%;
			    font-weight: bold;
			    font-size: 90%;
			    background-color: #A9A9A9;
    			color: #000000;
    			border: 1px solid black;
			}
			td.primera
			{
				 
				 width: 6%;
			     border: 1px solid black;
			}
			td.segunda
			{
				 
				 width: 6%;
			     border: 1px solid black;
			}
			td.amba
			{
				 
				 width: 6%;
			     border: 1px solid black;
			}
			
			
			     
			  </style>

			<table class="tab3">
								  <tr>
								    <th align="center" class="app">APLICACION</th>
								    <th align="right">PRIMERA</th>
								     <td class="primera" align="center"></td>
								    <th align="right">SEGUNDA</th>
								     <td class="segunda" align="center"></td>
								    <th align="right">AMBAS</th>
								     <td class="amba" align="center">X</td>
								  </tr>
								  
								  
								  
			</table>

		';
		$pdf->writeHTML($tab3, true, false, false, false, '');

		$pdf->SetMargins(11, 15, 15);
		$fechas = explode('-', $fecha);
		$nummes = $fechas[1] - 1;
		$txt = '  <style> 
					div.deudor{
						text-align:center;

					}
					div.deudor2{
						text-align:right;

					}
		            p {
		            	text-align:justify;
		            	font-size: 90%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            }
		          </style>
		 		<p>Señor Contralor: Para los fines pertinentes y por medio del presente documento, se confiere autorización irrevocable para que se imputen
					financieramente las acciones señaladas al sueldo que devengo y/o devengare a futuro como servidor público, en esta u otra dependencia
					del Gobierno Central, hasta finiquitar la cuantía total. <br/>
					Descuéntese la suma de: '.number_format($quincenal,2).' en: '.$plazoxquincena.' quincenas y un ultimo descuento de '.number_format($ultimodescuento,2).' en: '.$aprobacion->cantidad_meses.' meses a un plazo de: '.$dias.' días y aplíquese el pago efectivo de la obligación que he contraído
				por la suma de B/.'.$mot.' ('.$this->numtoletras($mot).
				//') a partir de la 1ERA quincena del mes de: ____________________
				') a partir de la 1ra quincena del mes de <b>'.$this->meses[$nummes].' de '.$fechas[2].'</b></p>
			 ';
		$pdf->writeHTMLCell(195, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->ln(4);

		$pdf->SetMargins(10, 15, 15);

		$tab4 = '
			<style>
			table.tab4, td.tab4, th.tab4 {
				font-weight: bold;
			    font-size: 80%;
								
			}
			table.tab4{
								border-collapse: collapse;
			}
			th.app
			{
				 
				 width: 20%;
				 height: 5px;
			    font-weight: bold;
			    font-size: 100%;
			    background-color: #A9A9A9;
    			color: #000000;
    			border: 1px solid black;
			}
			td.primera
			{
				 
				 width: 6%;
			     border: 1px solid black;
			}
			td.segunda
			{
				 
				 width: 6%;
			     border: 1px solid black;
			}
			td.amba
			{
				 
				 width: 6%;
			     border: 1px solid black;
			     height: 10px;
			}
			.adelantado{
				 width: 18%;

			}
			
			
			     
			  </style>

			<table class="tab4" >
				  <tr>
				    <th align="center" class="app">MÉTODO <br/> APLICADO</th>
					<th class="adelantado" align="right">DESCONTADO POR ADELANTADO</th>
				     <td class="primera"></td>
				    <th align="right">AGREGADO</th>
				     <td class="segunda"></td>
				    <th align="right">SOBRE <br/>SALDO</th>
				     <td class="amba" align="center"><h1>X</h1></td>
				  </tr>								  
								
			</table>

		';

		$pdf->writeHTML($tab4, true, false, false, false, '');

		$tb5 = '		
		 	<style>
		 				
				td.mensual
				{
				    background-color: #A9A9A9;
	    			color: #000000;
	    			border: 1px solid black;
				}
				.tab5 {
					border-collapse:collapse;
					border: 1pt solid black;
				}
				.tab5 .borde {
					
			    	border-top: 1px solid black;
			    
				}
				tr.borde1 {
					
			    	border-bottom:red;
			    	
			    	

				}
				tr.border2 td{
					
			    	border-left: none;
			    	border-right:  1px solid black;
			    	
			    	

				}
				.vertical{

					color:black;
					font-weight: bold;
				    font-size: 14px;
				    transform: rotate(271deg);
				    position: absolute;
				    display:block;

				}
				p.letra{
				    font-size: 90%;
				    line-height: 60%;
				     

				}
				p.Letra2{
					font-weight: bold;
				    font-size: 120%;
				  
				}
				p.monto{
				    font-size: 90%;
				    line-height: 100%;

				}
				p.monto1{
					font-weight: bold;
				    font-size: 110%;
				    line-height: 30%;

				}
				p.cierre{
					font-weight: bold;
				    font-size: 100%;
				    
				    line-height: 20%;
				    

				}
				p.interes{
					font-weight: bold;
				    font-size: 100%;
				    
				    line-height: 100%;
				}
				.negrita{

				    font-size:90% ;
				    font-weight: bold;
				    color:#993333;
				    

				}
				p.total{
					font-weight: bold;
				    font-size: 100%;
				    
				    line-height: 100%;

				}
				td.anchotd
				{
					 
					 width: 10%;
				     border: 1px solid black;
				}
					     
		</style>

		<table  class="tab5"  border="1"  align="center" width="105%">
			 <tr nobr="true">
				   	<th  > </th>
				    <th class="borde"  colspan="9">  </th>
				    
				    
	    			</tr>

			 <tr nobr="true">
				   <td  class="mensual"  rowspan="26" > <h1 class="vertical">

				  <span><br>S<br>O<br>L<br>O<br> <br>P<br>A<br>R<br>A <br><br>P<br>R<br>É<br>S<br>T<br>A<br>M<br>O<br>S</span>

				   </h1></td>
				    <td > 1</td>
				  <td colspan="6" align="right">  MONTO NETO A RECIBIR POR EL CLIENTE</td>


				  <td  class="mensual" colspan="">B/. '.$aprobacion->cantidad_aprobada.'</td>
				  
				  
			</tr>
				 
		    <tr nobr="true">
				
				<td rowspan=" "></td>
				<td colspan="3" align="right">CANCELACIONES</td>
				<td class="anchotd">Clave</td>
				<td class="anchotd">D.MENSUA</td>
				<td class="anchotd">SALDO</td>
				
		    </tr>

			<tr   class="border1" nobr="true">
			  	  
				 
				  <td   colspan="4" rowspan="5"></td>
				  
				  
				  <td  ></td>
				  <td class="mensual"></td>
				  <td ></td>
				  
			</tr>

			<tr nobr="true">
				  
				  
				  <td  ></td> 
				  <td class="mensual"></td> 
				   <td ></td> 
			 
			  
			 </tr>

			 <tr nobr="true">
				  
				  
				  <td ></td>
				  <td class="mensual" ></td>
				  <td ></td>
				  
			 </tr>

			 <tr nobr="true">
			  		
				 
				  <td ></td>
				  <td class="mensual" ></td>
			  	  <td ></td>
			 
			  
			 </tr>
			  <tr nobr="true">
			  		
				  
				  <td></td>
				  <td  class="mensual"  ></td>
				  <td ></td>
			  
			 
			  
			 </tr>

			 <tr nobr="true">
			  		
				  <td ></td>
				  <td colspan="5" align="right">TOTAL DE CANCELACIONES</td>
				  <td   class="mensual" ></td>
			  
			 
			  
			 </tr>
			  <tr nobr="true">
			  		
				  <td ></td>
				  <td colspan="5" align="right">REFINANCIAMIENTO VALOR PRESENTE </td>
					<td  class="mensual" ></td>
				
			 
			  
			 </tr>

			  <tr nobr="true">
			  		
				  <td >2</td>
				  <td  colspan="6" align="right">TOTAL DE CANCELACIONES Y REFINANCIAMIENTOS</td>
				  <td  class="mensual"  ></td>
				  
			 
			  
			 </tr>
			  <tr nobr="true">
			  		
				  <td  class="mensual" align="center" > <p   class="monto1">MNT</p> <p class="monto">=1+2 </p></td>
				  <td colspan="7"  align="right"> <p class="Letra2">MONTO NETO DE LA TRANSACCIÓN </p></td>
				   <td  class="mensual"  ><br><br>B/. '.$aprobacion->cantidad_aprobada.'</td>
			  
			 
			  
			 </tr>

			  <tr nobr="true">
			  		
				  <td >3</td>
				  <td colspan="6" align="right"><p class="interes">INTERESES</p></td>
				  <td  class="mensual"  >B/. '.number_format($interes, 2).'</td>
				  
			  
			 
			  
			 </tr>
			  <tr class="border2"  >
			  		
				  <td colspan="9"></td>
				  
			 
			  
			 </tr>

			  <tr nobr="true">
			  		
				  <td ></td>
				  <td colspan="5" align="right" >SERVICIO DE DESCUENTO (3%)</td>
				  <td  class="mensual"  >B/. '.$discount.'</td>
			  
			 
			  
			 </tr>
			  <tr nobr="true">
			  		
				  <td ></td>
				  <td colspan="5"  align="right">TIMBRES Y SELLOS FISCALES</td>
				  <td  class="mensual" ></td>
			  
			 
			  
			 </tr>
			  <tr nobr="true">
			  		
				
				<td ></td>
				  <td colspan="5" align="right" >COMISION</td>
				  <td  class="mensual" >B/. '.number_format($cotizacion['totalcargo'], 2).'</td>
			 
			  
			 </tr>

			 <tr nobr="true">
			  		
				
				<td >4</td>
				  <td colspan="6" align="right" > <p class= "cierre"> COMISION DE CIERRE </p></td>
				  <td  class="mensual" >B/. ' . number_format($total_cierre, 2) . '</td>
			 
			  
			 </tr>
			 <tr nobr="true">
			  		
				
				<td ></td>
				  
			  
			 </tr>
			 <tr nobr="true">
			  		
				  <td ></td>
				  <td colspan="5" align="right" >
				<b>F</b>ECI </td>
				  <td  class="mensual" ></td>
			  
			 
			  
			 </tr>
			  <tr nobr="true">
			  		
				  <td ></td>
				  <td colspan="5" align="right" >NOTARIA</td>
				  <td  class="mensual" ></td>
			  
			 
			  
			 </tr>
			 <tr nobr="true">
			  		
				  <td ></td>
				  <td colspan="5" align="right" >SEGURO</td>
				  <td  class="mensual" ></td>
			  
			 
			  
			 </tr>
			  <tr nobr="true">
			  		
				  <td >5</td>
				  <td colspan="6" align="right"><p class="total"> TOTAL/FNS </p></td>
			  		<td  class="mensual" ></td>
			 
			  
			 </tr>
			  <tr nobr="true">
			  		
				  <td colspan="9"></td>
				  
			 
			  
			 </tr>
			 <tr nobr="true">
			  		
				  <td  class="mensual" ><p class="Letra2">MGT  </p> <p class="letra"> =3+4+5 </p></td>
				  <td colspan="7" align="right" > <p class="Letra2"> MONTO DE LOS GASTOS DE LA TRANSACCIÓN</p></td>
				  
			     <td  class="mensual" ><br><br>'.$mgt.'</td>
			 
			  
			 </tr>
			 <tr nobr="true">
			  		
				  <td colspan="9" ></td>
				  
			 
			  
			 </tr>
			 <tr nobr="true">
			  		
				  <td  class="mensual"><p class="Letra2"> MTO=</p><p class="letra">MNT+MGT </p></td>
				  <td colspan="7" align="right"  >  <p class="Letra2">MONTO TOTAL DE LA OBLIGACIÓN </p></td>
				  <td  class="mensual" ><br><br>'.$mot.'</td>
			  
			 
			  
			 </tr>
		 
		</table>'
		;

		$pdf->SetMargins(7.6,15,15);
	
		$pdf->writeHTMLCell(0, 5, '', '', $tb5, 0, 1, false, true, 'C', true);
		$pdf->ln(2);

		$pdf->SetMargins(7.6,15,3.6);


 		$tab6 = '
			  
			 			<style>
			 			tr.top  th{
							
					    	
					    
					    		border-collapse:collapse;
							border-bottom: 1pt solid black;

							
					    
						}
						 
			   			table {
						  border-collapse: separate;
						  border-spacing: 12px 0;
						  font-size: 80%;
					      font-weight: normal;
						}

						td {
						  padding: 10px 0;
						  border: 1px solid black;
			  			  width: 34%;
						
						
			 			</style>
				

						<table  class="tab6"   align="center">
										 <tr class="top" nobr="true">
											    <th colspan="4">

											     </th>
										      
							      
							    
						</tr>
						
						<tr   nobr="true">
							    <td ><br/> <br/><h2>'.$cliente->nombre.' '.$cliente->apellido.'</h2></td>
							    <td ><br/> <br/>NOMBRE AUTORIZADO POR LA ENTIDAD CREDITICIA
								<h2>________________________________</h2>
								</td>
							    <td ><br/> <br/>FECHA, SELLO Y FIRMA
								CONTRALORÍA GENERAL </td>
						</tr>
						<tr  nobr="true">
							    <td> NOMBRE DEL SOLICITANTE <h3>'.$cliente->cedula.'</h3></td>
							    <td rowspan="2" > </td>
							    <td rowspan="3"></td>
						</tr>
						<tr  nobr="true">
							    <td > CEDULA <br/> <br/></td>
							    
							   
						</tr>
						<tr   nobr="true">
							    <td > FIRMA </td>
							    <td > FIRMA </td>
							    
						</tr>
		   </table>
			 
		';
		$pdf->writeHTML($tab6, true, false, false, false, '');



		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}


	public function ver_autorizacion_descuento_en_blanco($id){
		$this->autorizacion_descuento_en_blanco($id,'I');	
	}

	public function generar_autorizacion_descuento_en_blanco($id){
		$this->autorizacion_descuento_en_blanco($id,'D');		
	}

	public function autorizacion_descuento_en_blanco($id,$flag,$nombre = "autorizaciondescuentoenblanco.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$check_false = '<img src="'.site_url('assets/images/check_false.png').'" width="12" height="12">';
		$check_true = '<img src="'.site_url('assets/images/check_true.png').'" width="12" height="12">';

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);



		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 10, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$pdf->AddPage();

		$pdf->setCellPaddings(0, 1, 0, 1);
		$pdf->setCellMargins(0, 1, 0, 1);
		$pdf->SetFillColor(255, 255, 127);

		$pdf->Cell(0, 0, 'AUTORIZACION DE DESCUENTO DIRECTO IRREVOCABLE', 0, 1, 'L', 0, '', 0);

		$txt = "Fecha:  \nNúmero de Solicitud: ".$solicitud->id." \nEmpresa: ";
		$pdf->MultiCell(71, 0, $txt, 0, 'L', false, 1, 125, 35, true, 0, false, true, 0, 'T', false);

		$pdf->MultiCell(100, 5, 'Estimados Señores :', 0, 'L', 0, 0, '', '', true);
																																																																																																																															
		$pdf->Ln(8);
		$pdf->SetTextColor(0, 0, 0);

		$txt = 'Por medio de la presente yo; '. $cliente->nombre . ' '. $cliente->apellido .', portador(a) de la cédula de identidad personal No. '.$cliente->cedula.', en ejercicio libre de mi voluntad y consentimiento y en pleno uso de mis facultades mentales y legales autorizo a ustedes, se sirvan descontar de mi salario total (incluye sueldo base, comisiones, aumentos, descuento mensual de vacaciones y el descuento correspondiente señalado, por el período de pago, de toda liquidacion que reciba del empleo, por separación temporal o definitiva del trabajo o cualquier otra causa), la suma de __________________________________________________________________________________________ (US$ __________ ) durante _____ mensualidades y remitir dicho descuento a PRÉSTAMOS 911, S.A., dentro de los primeros ____ días de cada mes, para asi cubrir la obligación de crédito comercial que he contraído con esta entidad.';

		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->Ln(2);
		$txt = 'Esta autorización es irrevocable y de forzoso cumplimiento, hasta cancelar la suma de: __________________________________________________________________________________________ (US$ __________ )';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->Ln(2);
		$txt = 'Cualquier modificación a la misma requiere de el consentimiento por escrito de PRÉSTAMOS 911, S.A.';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->Ln(2);
		$txt = 'De ustedes y Atentamente,';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);
		$pdf->Ln(4);

		$pdf->MultiCell(100, 0, 'Nombre del cliente: '. $cliente->nombre . ' '. $cliente->apellido, 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(80, 0, 'Por Préstamos 911, S.A.', 0, 'R', 0, 1, '', '', true);
		$pdf->MultiCell(100, 0, 'Cédula: '.$cliente->cedula, 0, 'L', 0, 0, '', '', true);
		$pdf->MultiCell(80, 0, 'RUC155615545-2-2015 DV 8', 0, 'R', 0, 1, '', '', true);	


		$txt='El artículo del Código de Trabajo, modificado por el Decreto de Gabinete No.11 señala que los descuentos de préstamos bancarios y créditos comerciales, que autorice el trabajador son irrevocables y de forzoso cumplimiento para el empleador. El incumplimiento por parte del empleador de esta disposición, viola la norma contenida en el Artículo 128 numeral 20 del Código de Trabajo y acarrea para el infractor las sanciones que establece el Artículo 139 del Código de Trabajo.';
		$pdf->SetCellPadding(2);
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 1, 1, false, true, 'J', true);
		$pdf->Ln(2);

		$pdf->SetLineStyle(array('color' => array(254,000,000)));
		$pdf->SetFillColor(254,000,000);
		$pdf->SetTextColor(255,255,255);
		$pdf->setCellPaddings(0, 0, 0, 0);
		$pdf->setCellMargins(0, 0, 0, 0);
		$pdf->SetCellPadding(2);
		$pdf->Cell(0, 0, "PARA USO EXCLUSIVO DEL EMPLEADOR", 1, 2, 'C', true);


		$pdf->SetTextColor(0,0,0);
		$pdf->SetCellPadding(4);

		//$primer_descuento = explode('-', $aprobacion->fecha_primer_descuento);
		//$nummes = $primer_descuento[1] - 1;


		$txt = "Confirmamos a PRÉSTAMOS 911, S.A., que este descuento comenzará a efectuarse a partir de la quincena del: ____ de ____________________ de ________, y los pagos se realizaran mediante: ".$check_false." ACH ".$check_false." Cheque <br/><br/> Aceptamos,<br/><br/><br/>  Firma: _________________________ <br/> <br/><br/> Nombre: _________________________ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   Sello de la empresa <br/><br/><br/>  Cargo: _________________________ &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp;  &nbsp; &nbsp; No. Patronal:_________________________";
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 1, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}


    function myGetType($var)
    {
        if (is_array($var)) return "array";
        if (is_bool($var)) return "boolean";
        if (is_float($var)) return "float";
        if (is_int($var)) return "integer";
        if (is_null($var)) return "NULL";
        if (is_numeric($var)) return "numeric";
        if (is_object($var)) return "object";
        if (is_resource($var)) return "resource";
        if (is_string($var)) return "string";
        return "unknown type";
    }


	public function ver_contrato($id){
		$this->contrato($id,'I');
	}

	public function generar_contrato($id){
		$this->contrato($id,'D');
	}

	public function contrato($id,$flag,$nombre = "contrato.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$usuario = $this->usuarios_model->getUsuario($this->session->userdata('id_user'));
		$agente_cedula = $usuario->cedula;
		$agente = $usuario->nombres . ' ' . $usuario->apellidos;

		$representante = $this->representante_legal_model->get_by_id(1); 

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 23);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 12, '', true);
		$pdf->AddPage('P','LEGAL');


		$pdf->setCellPaddings(1, 1, 1, 1);
		$pdf->setCellMargins(1, 1, 1, 1);
		$pdf->SetFillColor(255, 255, 127);


		$txt = 'CONTRATO DE FINANCIAMIENTO';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, 0, true, 'C', true);

		$tasa = $producto->tasa_interes_mensual * 1;
		$tipo = $this->myGetType($tasa);

		if ($tipo == "float") {
			$tasa_interes = explode('.', $tasa);
			$tasai = $tasa_interes[0];
			$tasad = $tasa_interes[1];
			$tasad = (string) $tasad;
			$lentasad = strlen($tasad);
			$decimal = '';
			if ($tasad[0] == 0) {
				for ($i=0; $i < $lentasad; $i++) { 
					if ($this->numtoletras($tasad[$i],FALSE) == "") {
						$decimal .= "CERO ";
					}
				}				
			}
			$decimal .= $this->numtoletras($tasad,FALSE);
			$tasainteres = $this->numtoletras($tasai,FALSE).' PUNTO '.$decimal;
		} else {
			$tasainteres = $this->numtoletras($tasa,FALSE);
		}

		$totalinteres = $cotizacion["totalintereses"];		
		$totalinteres = (float) $totalinteres;
		$tipointeres = $this->myGetType($totalinteres);
		if ($tipointeres == "float") {
			$total_interes = explode('.', $totalinteres);
			if (count($total_interes) < 2) {
				$totalinteres = $totalinteres . ".00";
			} else {
				$interesd = $total_interes[1];
				$interesd = (string) $interesd;
				$leninteresd = strlen($interesd);
				if ($leninteresd < 2) {
					$totalinteres = $totalinteres . "0";
				}
			}
		}

		$totalcargo = $cotizacion["solocargo"];
		$totalcargo = (float) $totalcargo;

		$totalGastosNotariales = $cotizacion["sologasto"];
		$totalCargoAdministrativo = $cotizacion["solocargo"];
		$totalComision = $cotizacion["solocomision"];

		$tipocargo = $this->myGetType($totalGastosNotariales);
		if ($tipocargo == "float") {
			$total_cargo = explode('.', $totalGastosNotariales);
			if (count($total_cargo) < 2) {
				$totalGastosNotariales = $totalGastosNotariales . ".00";
			} else {
				$cargod = $total_cargo[1];
				$cargod = (string) $cargod;
				$lencargod = strlen($cargod);
				if ($lencargod < 2) {
					$totalGastosNotariales = $totalGastosNotariales . "0";
				}
			}
		}

		$tipocargo = $this->myGetType($totalCargoAdministrativo);
		if ($tipocargo == "float") {
			$total_cargo = explode('.', $totalCargoAdministrativo);
			if (count($total_cargo) < 2) {
				$totalCargoAdministrativo = $totalCargoAdministrativo . ".00";
			} else {
				$cargod = $total_cargo[1];
				$cargod = (string) $cargod;
				$lencargod = strlen($cargod);
				if ($lencargod < 2) {
					$totalCargoAdministrativo = $totalCargoAdministrativo . "0";
				}
			}
		}

		$tipocargo = $this->myGetType($totalComision);
		if ($tipocargo == "float") {
			$total_cargo = explode('.', $totalComision);
			if (count($total_cargo) < 2) {
				$totalComision = $totalComision . ".00";
			} else {
				$cargod = $total_cargo[1];
				$cargod = (string) $cargod;
				$lencargod = strlen($cargod);
				if ($lencargod < 2) {
					$totalComision = $totalComision . "0";
				}
			}
		}
		
		/*if ($aprobacion->fecha_primer_descuento == NULL || $aprobacion->fecha_primer_descuento == "") {
			$aprobacion->fecha_primer_descuento = date("d-m-Y", strtotime("$aprobacion->fecha_primer_descuento + 46 days"));
		}*/

		$txt1 = '  <style> 
		p {
		text-align:justify;
		font-size: 90%;
		font-weight: normal;
		font-family: Arial, Helvetica, Verdana
		}
		</style>
		<p>
		PRÉSTAMO: N° '.$aprobacion->id.' FECHA: '.date('d/m/Y').' <br/> <br/>

		Entre los suscritos a saber, el señor '.strtoupper($representante->nombre_apellido).', varón, de nacionalidad '.$representante->nacionalidad.', mayor de edad, domiciliado
		en la ciudad de Panamá, portador de '.$representante->tipo_documento.' número '.$representante->documento.', con domicilio en Plaza Regency
		Via España Piso 17, Corregimiento de Bella Vista, Ciudad de Panamá, actuando en su condición de Presidente y
		Representante Legal de la entidad jurídica denominada PRÉSTAMOS 911, S.A., sociedad anónima debidamente
		inscrita a la Folio Electrónica 155615545, Asiento 1 de la Sección de Personas Mercantiles del Registro Público de
		Panamá, y que en adelante se denominará EL ACREEDOR, por una parte, y por la otra '.$cliente->nombre.' '.$cliente->apellido.', mayor de edad, con cédula de identidad personal No. '.$cliente->cedula.', con domicilio en '.$cliente->direccion.' Provincia de '.$cliente->provincia.', actuando en nombre y representación propia, quien en adelante se denominará EL DEUDOR, y que en conjunto se denominarán LAS PARTES, celebran el presente
		Contrato, mediante el método sobre saldo, en los siguientes términos y condiciones: <br/> <br/>

		PRIMERA: En el presente acto EL ACREEDOR otorga a EL DEUDOR un préstamo por la suma de '.$this->numtoletras($aprobacion->cantidad_aprobada).' (US$ '.$aprobacion->cantidad_aprobada.'), a un plazo de '.$this->numtoletras($aprobacion->cantidad_meses,FALSE).' MESES ('.$aprobacion->cantidad_meses.'), y una tasa nominal de '.$tasainteres.' por ciento ('.$producto->tasa_interes_mensual.'%) mensual, tasa efectiva de '.$this->numtoletras($producto->tasa_interes_efectiva,FALSE).' por ciento ('.$producto->tasa_interes_efectiva.'%). <br/> <br/>

		SEGUNDA: EL DEUDOR pagará a EL ACREEDOR en concepto de intereses, la suma de '.$this->numtoletras($cotizacion["totalintereses"]).' (US$ '.$totalinteres.'), La devolución de intereses por cancelación anticipada se calculará mediante el método Suma de Años Dígitos, de acuerdo en lo
		contemplado en el artículo No. 31 de la Ley 42 de 2001. <br/> <br/>

		TERCERA: EL DEUDOR pagará por la comisión la suma de '.$this->numtoletras( $totalComision ).' (US$ '.$totalComision.'), por Gastos
		Notariales y Legales ' . $this->numtoletras($totalGastosNotariales) .  ' (US$ '.$totalGastosNotariales.') y por gastos administrativos '.$this->numtoletras( $totalCargoAdministrativo ).' (US$ '.$totalCargoAdministrativo.') <br/> <br/>
		
		Principal US$ ' .$aprobacion->cantidad_aprobada.'<br/> <br/>
		Intereses '.$producto->tasa_interes_mensual.'% a '.$aprobacion->cantidad_meses.' meses US$ '.$totalinteres.'<br/> <br/>		
		Gastos notariales y Legales US$ '.$totalGastosNotariales.'<br/> <br/>
		Gastos administrativos US$ '.$totalCargoAdministrativo.'<br/> <br/>
		Comisión US$ '.$totalComision.'<br/> <br/>
		OBLIGACIÓN TOTAL US$'.($aprobacion->cantidad_aprobada+$totalinteres+$totalGastosNotariales+$totalCargoAdministrativo+$totalComision).'<br/> <br/>
		CUARTA: EL DEUDOR se obliga a pagar en las oficinas de EL ACREEDOR, la totalidad del monto del préstamo	otorgado en un plazo de '.$this->numtoletras($aprobacion->cantidad_meses,FALSE).' ('.$aprobacion->cantidad_meses.') meses de financiamiento, mediante abonos mensuales de '.$this->numtoletras(round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2)).' (US$ '.round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2).'), cada uno a partir de '.
			/*date('d/m/Y', strtotime(str_replace("/","-",$aprobacion->fecha_primer_descuento)))*/
			'_________________'
			.', siendo el primer pago, y de ahí en adelante el día 30 de cada mes. <br/> <br/>
		QUINTA: EL ACREEDOR y EL DEUDOR acuerdan que en caso de mora en el pago de cualquier abono, EL DEUDOR
		deberá pagar AL ACREEDOR un recargo por mora equivalente de 2% mensual sobre los pagos de plazo vencido no
		capitalizable y en proporción al tiempo de dicho vencimiento. <br/> <br/>
		SEXTA: Queda aceptado por EL DEUDOR que todo los pagos que haga por virtud de este Contrato deberán ser
		efectuados en dólares, moneda legal de los Estados Unidos de América. EL DEUDOR acepta que las obligaciones
		contraídas por él, no se consideran liberadas o satisfechas si efectúa un pago en otra moneda distinta al dólar, aun
		cuando dicho pago hubiese sido ordenado por sentencia, o se hubiese hecho por cualquier otra causa. No obstante lo
		anterior, EL ACREEDOR se reserva el derecho de aceptar o no, pagos en cualquier otra forma, para lo cual EL DEUDOR
		deberá contar con el consentimiento previo y por escrito de EL ACREEDOR. <br/> <br/>
		SEPTIMA: Declara EL DEUDOR que ha dejado firmadas en blanco la de cantidad de 3 Autorizaciones de descuento,
		que serán entregadas y destruidas al llegar el término o vencimiento del préstamo. En caso de que del descuento directo
		autorizado sea interrumpido por cambio de trabajo, en cualquiera que sea el caso, y EL ACREEDOR se vea forzado a
		gestionar un descuento directo con el nuevo empleador,EL DEUDOR pagará un cargo adicional de manejo por la suma de US$100.00. En caso que EL DEUDOR gestione por si solo el descuento directo con el nuevo empleador en un lapso
		no mayor a 60 días calendarios de la fecha de inicio de labores, o si EL DEUDOR ha seguido realizando sus pagos
		mínimos requeridos, y el préstamo no esta en estatus de morosidad, dicha penalización no será aplicada. <br/> <br/>
		Además, Para garantizar el presente Contrato, EL DEUDOR se obliga a firmar un pagaré a favor de EL ACREEDOR,
		cuyo texto declara conocer y aceptar, y que el mismo forma parte integral del presente Contrato, que dicho pagaré es
		para garantizar la suma adeudada, recargos por morosidad y gastos de cobranza. Es entendido que la expedición del
		pagaré no produce novación alguna de la obligación principal. <br/> <br/>

		OCTAVA: EL DEUDOR será personalmente responsable del pago de las obligaciones contraídas en virtud del presente
		Contrato y el Pagaré, por lo que, su patrimonio podrá ser perseguido en caso de producirse un incumplimiento. <br/> <br/>
		NOVENA: EL ACREEDOR podrá declarar de plazo vencido todas las obligaciones aquí contraídas por EL DEUDOR en
		cualquiera de los siguientes casos: a) Si antes de haber pagado la mitad del monto de la obligación, EL DEUDOR dejare
		de efectuar un (1) pago mensual consecutivo a la fecha de su vencimiento; b) Si después de haber pagado la mitad del
		monto de la obligación, EL DEUDOR dejare de hacer, a la fecha de su vencimiento dos (2) pagos mensuales estipulados
		en este Contrato o dejare de hacer el último pago; c) Si EL DEUDOR resultare secuestrado, embargado o sus bienes
		fueran en cualquier forma perseguidos por terceros; d) Si EL DEUDOR incumpliere cualesquiera de las obligaciones
		asumidas mediante este documento. <br/> <br/>
		En caso de producirse cualquiera de los eventos de incumplimiento antes señalado, EL DEUDOR acepta que se tendrá
		como saldo correcto y verdadero del Pagaré garantizado mediante el presente Contrato aquel que conste en los libros
		de EL ACREEDOR, según la propia declaración de este, aun cuando dicho saldo fuera superior a los limites aquí
		estipulados. <br/> <br/>
		DECIMA: EL ACREEDOR podrá ceder, traspasar o enajenar a cualquier título, a favor de terceras personas, el crédito
		de que trata este Contrato de Préstamo, sin el previo consentimiento de EL DEUDOR y sin que ello implique novación
		de las obligaciones de EL DEUDOR. <br/>
		<br/>
		DECIMA PRIMERA: El hecho de que EL ACREEDOR permita una o varias veces que EL DEUDOR incumpla con sus
		obligaciones o las cumpla imperfectamente o en forma distinta a la pactada, o no insista en el cumplimiento exacto de
		sus obligaciones, o no ejerza oportunamente los derechos contractuales o legales que le corresponden, no se refutará
		ni equivaldrá a modificación del presente Contrato y no opta en ningún caso para que EL ACREEDOR en el futuro insista
		en el cumplimiento fiel y específico de las obligaciones que corren a cargo de EL DEUDOR, o ejerza los derechos
		convencionales o legales de que es titular o coparticipe. Se hace constar que para los efectos legales, <br/> <br/>
		EL DEUDOR renuncia al domicilio, a la presentación del documento y desde ahora releva a EL ACREEDOR de la
		obligación de prestar fianza de costas en relación con cualquier ejecución o juicio referente a esta obligación, y acepta
		que correrán por su cuenta los pagos y costas que ocasione o motive esta obligación, ya sea judicial o
		extrajudicialmente. Aceptan LAS PARTES así mismo, que de tiempo en tiempo, y sin previo aviso a EL DEUDOR, el
		plazo de esta obligación podrá ser prorrogada por EL ACREEDOR, si a bien lo tiene, en todo o en parte, EL DEUDOR
		autoriza a EL ACREEDOR para que si a bien lo tiene, al vencimiento del plazo original o alguna de sus prórrogas, le
		cobre el saldo que se le deba por la presente obligación. <br/> <br/>

		DECIMA SEGUNDA: LAS PARTES convienen en que si alguna de las estipulaciones del presente Contrato resultare
		nula según las leyes de la República de Panamá, tal nulidad no invalidará el Contrato en su totalidad, sino que éste se
		interpretará como si no incluyera la estipulación o estipulaciones que declaren nulas y los derechos y obligaciones de
		LAS PARTES serán interpretadas y observadas en la forma en que en derecho proceda. </p><br/> <br/> 

		<table  align="center">
		<tr nobr="true">
		<th colspan="3"></th>
		</tr>
		<tr nobr="true">
		<td  align="left"> <b>POR: EL ACREEDOR</b> <br/><br/><br/>
		_________________________<br/><br/>
		PRESTAMOS 911<br/>
		Nombre: '.$agente.'<br/>
		Cédula No. '.$agente_cedula.'
		</td>
		<td align="left"> <b></b> <br/> <br/>
		<br/>

		</td>
		<td align="left"><b>POR: EL DEUDOR</b> <br/> <br/><br/>
		_________________________<br/><br/>
		Nombre: '.$cliente->nombre.' '.$cliente->apellido.'<br/>
		Cédula No. '.$cliente->cedula.'
		</td>
		</tr>

		</table>

		';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);


		$pdf->lastPage();


		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}





	public function ver_pagare($id){
		$this->pagare($id,'I');
	}

	public function generar_pagare($id){
		$this->pagare($id,'D');
	}

	public function pagare($id,$flag,$nombre = "pagare.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);
		$representante = $this->representante_legal_model->get_by_id(1); 

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$usuario = $this->usuarios_model->getUsuario($this->session->userdata('id_user'));
		$agente_cedula = $usuario->cedula;
		$agente = $usuario->nombres . ' ' . $usuario->apellidos;

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 11, '', true);
		$pdf->AddPage();


		$pdf->setCellPaddings(1, 1, 1, 1);
		$pdf->setCellMargins(1, 1, 1, 1);
		$pdf->SetFillColor(255, 255, 127);


		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		if ($aprobacion->fecha_primer_descuento == NULL || $aprobacion->fecha_primer_descuento == "") {
			$aprobacion->fecha_primer_descuento = date("d-m-Y", strtotime("$aprobacion->fecha_primer_descuento + 46 days"));
		}
		
		$txt = '  <style> 
					div.deudor{
						text-align:center;

					}
					div.deudor2{
						text-align:right;

					}
		            p {
		            	text-align:justify;
		            	font-size: 90%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            }
		          </style>
		 		<p>PAGARE No: '.$aprobacion->id.'<br/> <br/>
				Por USD$'.$cotizacion['totalcobrar'].' <br/> <br/>
				
			Conste por el presente documento negociable lo siguiente que:
			<br/> <br/>
			 Yo, '.$cliente->nombre.' '.$cliente->apellido.' mayor de edad, con documento de identidad personal No. '.$cliente->cedula.', con domicilio en '.$cliente->direccion.', declaro libre de todo apremio y de todo vicio que anule el consentimiento y en pleno usos de mis facultades legales y mentales, en adelante me constituyo en <b> « DEUDOR » </b> de PRESTAMOS 911, S.A., sociedad anónima organizada de acuerdo con las leyes de la República de Panamá e inscrita en el Registro Publico a Folio No. 155615545, con domicilio en la Vía España frente al Supermercado Rey, Edificio Plaza Regency, piso 17, teléfono 377-2911, cuyo presidente y representante legal <b>'.strtoupper($representante->nombre_apellido).'</b>, varón, '.$representante->nacionalidad.', mayor de edad, portador de '.$representante->tipo_documento.' de identidad personal No. '.$representante->documento.', (en adelante <b>EL ACREEDOR</b>) o <b>« PRESTAMOS 911, S.A. </b> », y por lo tanto por este medio reconozco y acepto que debo pagar irrevocable e incondicionalmente a PRESTAMOS 911,S.A, o a su orden la suma de	'.$this->numtoletras($cotizacion['totalcobrar']).' (USD$'.$cotizacion['totalcobrar'].'), moneda de curso legal de los Estados Unidos de
			América, en '.$aprobacion->cantidad_meses.' mensualidades consecutivas a capital, intereses con pagos de '.$this->numtoletras(round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2)).' (USD$'.round($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2).') y F.E.C.I. (si lo hubiese de $0.00) moneda de curso legal de los Estados Unidos de América cada una comenzando el '.date('d/m/Y', strtotime(str_replace("/","-",$aprobacion->fecha_primer_descuento))).' hasta cancelar la totalidad de la deuda.  <br/> <br/> 
			Los pagos serán efectuados en las oficinas de PRESTAMOS 911, S.A., o en el lugar que establezca el tenedor de este Pagaré y solamente si el préstamo no es cancelado dentro de los treinta y cinco (35) días, contados desde el día del desembolso. <br/> <br/> 
			EL DEUDOR pagará mensualmente al Acreedor su interés del de '.$producto->tasa_interes_mensual.'% mensual sobre los saldos diarios del préstamo adeudado al ACREEDOR, más la correspondiente sobretasa del (F.E.C.I. si los hubiere). Los intereses serán calculados con el método de interés sobre saldo, que consiste en calcular los intereses sobre el saldo capital prestado por el tiempo transcurrido. Para efectos del artículo 30 de la Ley 42 del 23 de julio de 2001, la tasa de interés efectiva aplicada es de '.$producto->tasa_interes_efectiva.' anual.  <br/> <br/> 

			El ACREEDOR y el DEUDOR acuerdan, que el DEUDOR, en adición al interés corriente pactado en este instrumento,
			deberá pagar al ACREEDOR a quien lo represente, un interés por mora, sin necesidad de aviso o intimación o
			requerimiento por parte del ACREEDOR, del DOS (2%) por ciento mensual sobre las cuotas de plazo vencido no
			pagadas y en proporción al tiempo de dicho vencimiento hasta que las mismas sean canceladas, sin perjuicio de los efectos de tal incumplimiento de este contrato al tenor de la ley, si el deudor no cancela la totalidad del préstamo a mas tardar cinco (5) días después de fecha de vencimiento del préstamo. PRESTAMOS 911, S.A., o cualquier tenedor subsiguiente podrá declarar la deuda representada por este pagaré de plazo vencido, líquida y exigir el pago de la totalidad de las sumas que entonces se le adeuden, mas las costas y gastos de cobranza, tanto judiciales como extrajudiciales, si a ello hubiera lugar, en cualquier de los siguientes casos:<br/>
			1.- Por mora en el pago de cualquiera de los abonos que debe hacer el DEUDOR a tenor de este Pagaré.
			2.- Si se inician uno o varios juicios en contra del DEUDOR o se dictare mandamiento de secuestro o embargo en contra
			de los bienes del DEUDOR. <br/>
			3.- Si al DEUDOR le siguiere juicio de curso de acreedores, juicio de quiebra y fuere declarado en quiebra o si hiciere
			cesión general de sus bienes o fuere intervenido por autoridades competentes.<br/>
			4.- Por insolvencia, liquidación total o parcial, disolución, administración judicial o extrajudicial del DEUDOR.<br/>
			5.- Si el DEUDOR dejare de pagar sus impuestos, tasas o tributos nacionales o municipales o las cuotas del seguro
			social.<br/>
			6.- Si PRESTAMOS 911, S.A. o cualquier otro tenedor de este pagaré lo juzgan necesario por cualquier razón para
			proteger sus intereses y ante razones fundadas.<br/>
			7.- En caso de muerte del Deudor.<br/>
			8.- Si el Deudor incumpliere cualquiera de las obligaciones contraídas a favor del ACREEDOR en el presente pagaré, o incumpla otras obligaciones contraídas, ahora o en el futuro, para con el ACREEDOR presumiéndose verídico lo aseverado por el Acreedor con respecto a la obligación incumplida. <br/>
			En caso de que EL DEUDOR haya autorizado el pago a través de un descuento directo, el mismo se obliga de ser
			necesario, realizar los pagos adeidados directamente a PRESTAMOS 911, S.A., si surgiera algún inconveniente, sea o no atribuible al mismo, que pueda afectar o interrumpir el sistema de descuento directo originalmente autorizado. De no hacerlo así, el ACREEDOR declara la totalidad de la deuda de plazo vencido, líquida y exigir el pago de la totalidad	de las sumas adeudadas, para lo cual el DEUDOR acepta desde ya irrevocablemente el estado de cuenta o balance de saldo que presente el ACREEDOR. <br/>
			Todos los pagos que el Deudor deba efectuar a tenor de este pagaré se harán en dólares, moneda de curso legal de los Estados Unidos de América, en las oficinas del Acreedor PRESTAMOS 911, S.A., arriba mencionada, salvo que el ACREEDOR indique que dichos pagos deban hacerse en otro lugar, y deberán ser hechos sin deducciones, retenciones o compensaciones de ningún índole. De estar algunos de estos pagos sujetos ahora o en el futuro a cualquier deducción o retenciones, entonces el DEUDOR pagará a PRESTAMOS 911, S.A. las cantidades adicionales, que sean necesarias	para que PRESTAMOS 911, S.A. reciba el pago total. <br/>
			Para todos los efectos legales, el Deudor renuncia expresamente al domicilio, a la presentación de este documento, al protesto, al beneficio de excusión, y al señalamientos de bienes al aviso de haber sido desatendido este documento, al requerimiento o intimación de pago en caso de mora, así como a todas las demás diligencias conservatorias y se acoge al proceso de cobro ejecutivo, por lo que para todos los efectos legales este documentos presta mérito ejecutivo. El DEUDOR de este pagaré pagará todas las obligaciones asumidas en este pagaré. El Deudor se comprometen expresamente a pagar a PRESTAMOS 911, S.A. o a su orden, y acepta que en caso de mora, el Acreedor pueda exigir el pago de la deuda sin previamente requerir el pago. <br/> <br/>
			Los timbres fiscales que deban adherirse a este documento serán de cuenta del DEUDOR podrá anular las estampillas correspondientes, en cuyo caso el DEUDOR reembolsará las sumas pagadas al ACREEDOR. El presente pagaré se	rige por las Leyes de la República de Panamá. <br/>
			El DEUDOR acepta anticipadamente y se da por notificado de cualquier prórroga que pueda conceder PRESTAMOS
			911 S.A. o cualquier otro tenedor del pagaré del DEUDOR, quedando entendido que dicha medida no extinguirá ninguna de las obligaciones del Deudor en virtud de este Pagaré. <br/>
			El Deudor reconoce y acepta expresamente que el presente pagaré ha sido cedido en garantía a favor de PRESTAMOS
			911, S.A. <br/>
			Todas las declaraciones, aceptaciones y renuncias del DEUDOR se entienden hechas en virtud de este Pagaré.
			Otorgado en la Ciudad de Panamá el día '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].'. </p><br/> <br/> <br/> <br/>

			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);

		$pdf->ln(10);

		$txt2 = '<table  align="center">
						 <tr nobr="true">
						  <th colspan="3"></th>
						 </tr>
						 <tr nobr="true">
						  <td  align="left"> <b>ACREEDOR</b> <br/>
							PRESTAMOS 911 <br/>
							NOMBRE: '.$agente.'<br/>
							CEDULA '.$agente_cedula.'<br/>
						   </td>
						   <td align="left"> <b>EL DEUDOR</b> <br/> <br/>
							NOMBRE: '.$cliente->nombre.' '.$cliente->apellido.'<br/>
							CEDULA: '.$cliente->cedula.'
						   </td>
						   <td align="left"><b> EL DEUDOR</b> <br/> <br/>
							NOMBRE: <br/>
							CEDULA:
						   </td>
						 </tr>

					</table>
			  			<br/> <br/> <br/>  <br/>  <br/>
			  			<b>TESTIGO </b> <br/>
							NOMBRE: <br/>
							CEDULA:

			 ';


		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}





	public function ver_no_desembolsado($id){
		$this->no_desembolsado($id,'I');
	}

	public function generar_no_desembolsado($id){
		$this->no_desembolsado($id,'D');
	}

	public function no_desembolsado($id,$flag,$nombre = "nodesembolsado.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);


		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 12, '', true);
		$pdf->AddPage();


		$pdf->setCellPaddings(1, 1, 1, 1);
		$pdf->setCellMargins(5, 1, 1, 1);
		$pdf->SetFillColor(255, 255, 127);

		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$genero = $cliente->genero;
		if ($genero == 'Masculino') {
			$genero = 'el Sr.';
		} else if ($genero == 'Femenino') {
			$genero = 'la Sra.';
		} else {
			$genero = 'el Sr/a.';
		}
		
		$txt = '

			<style>
			
			p {
				text-align:justify;
		            	font-size: 100%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	 

			}
			
			</style>
			<p>
			Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].'<br/> <br/> <br/>

			Señores
			<br/> <br/>
			'.$empresa->nombre.'
			<br/> <br/>
			E.S.M
			<br/> <br/><br/> <br/>
			Estimados Señores:
			<br/> <br/> <br/> 

			La presente es para informarle que, '.$genero.' '.$cliente->nombre.' '.$cliente->apellido.' con documento de identidad personal No. '.$cliente->cedula.', estuvo realizando un trámite de préstamo personal con nuestra entidad el cual no fue desembolsado. <br/> <br/> <br/>
				Por tal motivo le pedimos encarecidamente no tomar en cuenta la autorización de descuento aprobada y se
					deshaga de la misma. <br/> <br/> <br/> <br/>
			Atentamente, <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
			Por: Préstamos 911, S.A </p>
			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}





	public function ver_carta_nuevo_empleador($id){
		$this->carta_nuevo_empleador($id,'I');
	}

	public function generar_carta_nuevo_empleador($id){
		$this->carta_nuevo_empleador($id,'D');
	}

	public function carta_nuevo_empleador($id,$flag,$nombre = "cartanuevoempleador.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}

		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);


		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 12, '', true);
		$pdf->AddPage();
		//$pdf->setCellMargins(25, 1, 1, 1);

		// set some text to print

		//$pdf->Ln(20);


		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$genero = $cliente->genero;
		if ($genero == 'Masculino') {
			$gen = 'el Sr.';
		} else if ($genero == 'Femenino') {
			$gen = 'la Sra.';
		} else {
			$gen = 'el Sr/a.';
		}

		$txt1 = '  

					 <style> 
					
		            p.ex1{
		            	text-align:justify;
		            	font-size: 95%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	
					     margin-left: 5cm;
		            }
		          </style>



		          	
		          	<p class="ex1">
					Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].' <br/> <br/> <br/> 
					Señores<br/> <br/>
					'.$empresa->nombre.' <br/> <br/>  <br/>
					Estimados Señores: <br/>  <br/>
					 La presente tiene por objeto comunicarle, que requerimos acción inmediata de la situación
					de débito que registra su trabajador '.$cliente->nombre.' '.$cliente->apellido.', mayor de edad, portadora de la cédula de identidad personal número '.$cliente->cedula.', con nuestra empresa denominada PRÉSTAMOS 911, S.A. <br/> <br/> <br/>
					Esto en virtud, de la autorización de descuento directo que '.$gen.' '.$cliente->apellido.' suscribió, a
					razón del contrato de préstamo que mantiene con nuestra institución financiera y sin abonos desde
					el '.date('d/m/Y', strtotime(str_replace("/","-",$aprobacion->fecha_primer_descuento))).'
					<br/> <br/> <br/>
				Además, les recordamos lo siguiente: <br/>
				“Artículo 161 del Código de Trabajo. Solamente podrán realizarse las siguientes retenciones y descuentos: <br/>
				11. Las sumas que el trabajador autorice le sean descontadas para cubrir préstamos bancarios y créditos
				comerciales, hasta por un veinte por ciento de su salario; estas autorizaciones de descuento son
				irrevocables y <u>de forzoso cumplimiento por parte del empleador</u> …”
				<br/>
				<br/>
				Agradeciendo de antemano su colaboración, quedo ante ustedes sin mas que añadir.

				<br/>
				<br/>
				<br/>
				<br/>

				

				Atentamente, <br/> <br/> <br/>

				Por: PRESTAMOS 911, S.A.
				<br/> <br/> <br/> <br/>  
				_____________________________ 
				<br/>  <br/> <br/> <br/> <br/>   </p>
			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);

			$txt2 = ' 
				<style> 
					
		            p {
		            	text-align:center;
		            	font-size: 90%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            }
		          </style> <p>
				Adjunto. Copia simple de la carta de autorización de descuento directo debidamente firmada: </p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'C', true);





		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}




	public function ver_carta_nuevo_empleador_verificar_dispo($id){
		$this->carta_nuevo_empleador_verificar_dispo($id,'I');
	}

	public function generar_carta_nuevo_empleador_verificar_dispo($id){
		$this->carta_nuevo_empleador_verificar_dispo($id,'D');
	}

	public function carta_nuevo_empleador_verificar_dispo($id,$flag,$nombre = "cartanuevoempleadorverificardispo.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}

		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 12);

		// add a page
		$pdf->AddPage();
		// set some text to print

		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$genero = $cliente->genero;
		if ($genero == 'Masculino') {
			$gen = 'el Sr.';
		} else if ($genero == 'Femenino') {
			$gen = 'la Sra.';
		} else {
			$gen = 'el Sr/a.';
		}


		$txt1 = '
			<style> 
			
            p{
            	text-align:justify;
            	font-size: 95%;
            	font-weight: normal;
            	font-family: Arial, Helvetica, Verdana
            	
			    
            }
          </style>
          	<p>
			Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].' <br/> <br/> <br/> 
			Señores<br/> <br/>
			'.$empresa->nombre.'<br/> <br/> <br/>
			Estimados Señores: <br/>  <br/>
			La presente tiene por objeto comunicarle, que requerimos acción inmediata de la situación
			de débito que registra su trabajador '.$cliente->nombre.' '.$cliente->apellido.', mayor de edad, portadora de la cédula de identidad personal número '.$cliente->cedula.', con nuestra empresa denominada PRÉSTAMOS 911, S.A. <br/> <br/> <br/>
			Esto en virtud, de la autorización de descuento directo que '.$gen.' '.$cliente->apellido.'
			suscribió, a razón del contrato de préstamo que mantiene con nuestra institución financiera
			desde el '.date('d/m/Y', strtotime(str_replace("/","-",$aprobacion->fecha_primer_descuento))).'. <br/> <br/> <br/>
			Les solicitamos evaluar la autorización de descuento adjuntada, la cual esta debidamente
			firmada por '.$gen.' '.$cliente->apellido.' y notificarnos si mantiene la capacidad para dicho
			descuento. <br/> <br/> <br/>
			Además, les recordamos lo siguiente: <br/>
			“Artículo 161 del Código de Trabajo. Solamente podrán realizarse las siguientes
			retenciones y descuentos: <br/> 
			11. Las sumas que el trabajador autorice le sean descontadas para cubrir préstamos
			bancarios y créditos comerciales, hasta por un veinte por ciento de su salario; <b>estas
			autorizaciones de descuento son irrevocables y  <u>de forzoso cumplimiento por parte
			del empleador </u></b>…” <br/> <br/> <br/>

			Sin mas que añadir, quedo ante ustedes. <br/> <br/> <br/>

			Atentamente, <br/> <br/> <br/>

			Por: PRESTAMOS 911, S.A. 
			<br/> <br/> <br/> <br/> <br/> <br/> <br/>
			</p>
				

			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);

			$txt2 = ' 
				<style> 
					
		            p {
		            	text-align:center;
		            	font-size: 90%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            }
		          </style> <p>
				Adjunto. Copia simple de la carta de autorización de descuento directo debidamente firmada: </p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, '', true);


		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}




	public function ver_carta_nuevo_empleador_no_desc($id){
		$this->carta_nuevo_empleador_no_desc($id,'I');
	}

	public function generar_carta_nuevo_empleador_no_desc($id){
		$this->carta_nuevo_empleador_no_desc($id,'D');
	}

	public function carta_nuevo_empleador_no_desc($id,$flag,$nombre = "cartanuevoempleadornodesc.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}

		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 12);

		// add a page
		$pdf->AddPage();

		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$genero = $cliente->genero;
		if ($genero == 'Masculino') {
			$gen = 'el Sr.';
		} else if ($genero == 'Femenino') {
			$gen = 'la Sra.';
		} else {
			$gen = 'el Sr/a.';
		}

		$txt1 = '   <style> 
					
		            p{
		            	text-align:justify;
		            	font-size: 95%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	
					     
		            }
		          </style>
		          	<p>
					Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].'<br/> <br/> <br/> 
					Señor<br/> <br/> 
					'.$empresa->nombre_rrhh.' <br/> <br/> 
					'.$empresa->nombre.'<br/> <br/> <br/>
					Estimados Señor: <br/>  <br/>
					La presente tiene por objeto comunicarle, que requerimos acción inmediata de la situación
					de débito que registra su trabajador '.$cliente->nombre.' '.$cliente->apellido.', mayor de edad, portadora de la cédula de identidad personal número
					Numero de cedula, con nuestra empresa denominada PRÉSTAMOS 911, S.A. <br/> <br/> <br/>
					Esto en virtud, de la autorización de descuento directo que '.$gen.' '.$cliente->apellido.' suscribió, a razón del contrato de préstamo que mantiene con nuestra institución financiera con fecha de el '.date('d/m/Y', strtotime(str_replace("/","-",$aprobacion->fecha_primer_descuento))).'.<br/> <br/> <br/>
					Además, les recordamos lo siguiente: <br/>
					“Artículo 161 del Código de Trabajo. Solamente podrán realizarse las siguientes retenciones y descuentos:<br/>
					11. Las sumas que el trabajador autorice le sean descontadas para cubrir préstamos bancarios y créditos
					comerciales, hasta por un veinte por ciento de su salario; estas autorizaciones de descuento son
					irrevocables y <u> <b>de forzoso cumplimiento por parte del empleador </b></u>…”<br/>
					Por lo cual, la negativa de realizar dicho descuento al trabajador, es incumplimiento de la norma antes
					citada.
					
					</b>…” <br/> <br/> <br/>

					Agradeciendo de antemano su colaboración, quedo ante usted sin mas que añadir. <br/> <br/> <br/>

					Atentamente, <br/> <br/> <br/>

					Por: PRESTAMOS aaa 911, S.A.
					<br/> <br/> <br/> <br/> <br/> <br/>   </p>

				

			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);

			$txt2 = ' 
				<style> 
					
		            p {
		            	text-align:center;
		            	font-size: 90%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            }
		          </style> <p>
				Adjunto. Copia simple de la carta de autorización de descuento directo debidamente firmada: </p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'C', true);


		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}





	public function ver_carta_nuevo_empleador_contrato_definido($id){
		$this->carta_nuevo_empleador_contrato_definido($id,'I');
	}

	public function generar_carta_nuevo_empleador_contrato_definido($id){
		$this->carta_nuevo_empleador_contrato_definido($id,'D');
	}

	public function carta_nuevo_empleador_contrato_definido($id,$flag,$nombre = "cartanuevoempleadornodesc.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}

		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);




		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 12);

		// add a page
		$pdf->AddPage();
		// set some text to print

		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$txt1 = '  

					 <style> 
					
		            p.ex1{
		            	text-align:justify;
		            	font-size: 95%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	
					     
		            }
		          </style>
		          	<p class="ex1">
					Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].' <br/> <br/> <br/> 
					Señores<br/> <br/>'.$empresa->nombre.' <br/> <br/>  <br/>
					Estimados Señores: <br/>  <br/>
					 La presente tiene por objeto comunicarle, que requerimos acción inmediata de la situación
					de débito que registra su trabajador '.$cliente->nombre.' '.$cliente->apellido.', mayor de edad, portadora de la cédula de identidad personal número '.$cliente->cedula.', con nuestra empresa denominada PRÉSTAMOS 911, S.A. <br/> <br/> <br/>
					Esto en virtud, de la autorización de descuento directo que el Sr/a. '.$cliente->apellido.' suscribió, a
					razón del contrato de préstamo que mantiene con nuestra institución financiera y sin abonos desde
					el '.date('d/m/Y', strtotime(str_replace("/","-",$aprobacion->fecha_primer_descuento))).'.</u>
					<br/> <br/> <br/>
				Además, les recordamos lo siguiente: <br/>
				“Artículo 161 del Código de Trabajo. Solamente podrán realizarse las siguientes retenciones y descuentos:<br/>
				11. Las sumas que el trabajador autorice le sean descontadas para cubrir préstamos bancarios y créditos
				comerciales, hasta por un veinte por ciento de su salario; estas autorizaciones de descuento son
				irrevocables y <u> <b>de forzoso cumplimiento por parte del empleador </b></u>…”
				Por lo cual, la negativa de realizar dicho descuento al trabajador, es incumplimiento de la norma antes
				citada. 
				
				 <br/> <br/> <br/>

				

				Atentamente, <br/> <br/> <br/>

				Por: PRESTAMOS 911, S.A.
				<br/> <br/> <br/> <br/> <br/> 
				_____________________________ 
				<br/>  <br/> <br/> <br/>   </p>
			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);

			$txt2 = ' 
				<style> 
					
		            p {
		            	text-align:center;
		            	font-size: 90%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            }
		          </style> <p>
				Adjunto. Copia simple de la carta de autorización de descuento directo debidamente firmada: </p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'C', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}




	public function ver_carta_saldo($id){
		$this->carta_saldo($id,'I');
	}

	public function generar_carta_saldo($id){
		$this->carta_saldo($id,'D');
	}

	public function carta_saldo($id,$flag,$nombre = "cartasaldo.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}

		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$num_view = $this->aprobacion_model->get_num_view_new($aprobacion->id);
		if (!$num_view) {
			print_r('Solicitud debe estar entregada para ver este documento.'); // saber el saldo actual
			return;
		}

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);



		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 12, '', true);
		$pdf->AddPage();

		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;


		$txt = ' <style>
			
			p {
				text-align:justify;
		            	font-size: 110%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	 margin-left: 10em;

			}
			
			</style>
					<p>
					Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].'<br/> <br/> <br/>  

					Señor/a<br/> <br/>'.$cliente->nombre.' '.$cliente->apellido.'<br/>  <br/>
				
					E.S.M.<br/> <br/> 
					<br/>  

					Estimados Señor/a <br/> <br/> <br/>

					A solicitud de la parte interesada, certificamos que el préstamo identificado con el No.
					'.$aprobacion->id.', a nombre de '.$cliente->nombre.' '.$cliente->apellido.', con documento de
					identidad personal No. '.$cliente->cedula.' mantiene un saldo a la fecha de US$ '.$num_view->deuda_fecha.' <br/> <br/> <br/>
					Esta información es dada exclusivamente a requerimiento del cliente, por lo tanto no
					conlleva responsabilidad alguna para nuestra institución financiera o alguno de nuestro
					personal. <br/> <br/> <br/> <br/> <br/>
					Atentamente, <br/> 

				Por: PRESTAMOS 911, S.A.
			 </p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);


		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}




	public function ver_carta_no_judicial_recordatorio_pago($id){
		$this->carta_no_judicial_recordatorio_pago($id,'I');
	}

	public function generar_carta_no_judicial_recordatorio_pago($id){
		$this->carta_no_judicial_recordatorio_pago($id,'D');
	}

	public function carta_no_judicial_recordatorio_pago($id,$flag,$nombre = "cartanojudicialrecordatoriopago.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}

		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);


		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 12);

		// add a page
		$pdf->AddPage();

		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$genero = $cliente->genero;
		if ($genero == 'Masculino') {
			$gen = 'el Sr.';
		} else if ($genero == 'Femenino') {
			$gen = 'la Sra.';
		} else {
			$gen = 'el Sr/a.';
		}

		$txt1 = '  

					 <style> 
					
		            p.ex1{
		            	text-align:justify;
		            	font-size: 95%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	
					     margin-left: 5cm;
		            }
		          </style>



		          	
		          	<p class="ex1">
					Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].' <br/> <br/> <br/>  <br/>

					Señores<br/> '.$empresa->nombre.' <br/> 
				
					'.$empresa->direccion.'<br/> <br/> 
					<br/>  

					Adeudado US$'.number_format($cotizacion['totalcobrar'], 2).' desde el '.date('d/m/Y', strtotime(str_replace("/","-",$aprobacion->fecha_primer_descuento))).' </u>

						<br/> <br/> <br/> 

					Estimados <br/>  <br/> <br/> <br/>  
					 La presente tiene por objeto recordarle las retenciones que deben ser afectuadas a '.$gen.' '.$cliente->nombre.' '.$cliente->apellido.', con cedula '.$cliente->cedula.', por un monto mensual de $'.number_format($cotizacion['totalcobrar'] / $aprobacion->cantidad_meses, 2).', empezando '.date('d/m/Y', strtotime(str_replace("/","-",$aprobacion->fecha_primer_descuento))).'. <br/> <br/> <br/>

					 Dichos pagos deben ser remitidos a Prestamos 911, S.A. mediante cheque o por ACH a la cuenta corriente de banistmo no. 0112651568.
					 <br/> <br/> <br/>

			Sin mas que añadir, quedo ante ustedes
				 <br/> <br/> <br/>
				
				

				Atentamente, <br/> <br/> <br/>

				Por: PRESTAMOS 911, S.A.
				 <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> 
				 </p>
				 </p>
			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);

			$txt2 = ' 
				<style> 
					
		            p {
		            	text-align:center;
		            	font-size: 90%;
		            	font-weight: normal;
		            	font-family: Verdana, Geneva, sans-serif;
		            }
		          </style> <p>
				Adjunto. Copia simple de la carta de autorización de descuento directo debidamente firmada: </p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'C', true);



		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}









	public function ver_carta_no_judicial_recordatorio_admin($id){
		$this->carta_no_judicial_recordatorio_admin($id,'I');
	}

	public function generar_carta_no_judicial_recordatorio_admin($id){
		$this->carta_no_judicial_recordatorio_admin($id,'D');
	}

	public function carta_no_judicial_recordatorio_admin($id,$flag,$nombre = "cartanojudicialrecordatorioadmin.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);
		$catidad_cobro = $this->cobros_model->cant_cobros_by_aprobacion_id($aprobacion->id);

		$deuda_total = $cotizacion["totalpagar"];
		if ($catidad_cobro > 0) {
			$pagos_array = array();
			for ($i=0; $i < $catidad_cobro; $i++) {
				$pagos_array[] = $cotizacion["data"][$i]["total"];
			}
			$pagos = array_sum($pagos_array);
		} else {
			$pagos = 0;
		}

		$deuda = $deuda_total - $pagos;

		$fecha_ultimo_pago = $aprobacion->fecha_primer_descuento;
		$ultimopago = $catidad_cobro - 1;
		$fecha_ultimo_pago = date("Y-m-d", strtotime("$fecha_ultimo_pago + $ultimopago months"));
		$fecha_ultimo_pago = explode('-', $fecha_ultimo_pago);
		$ultimonummes = $fecha_ultimo_pago[1] - 1;

		//$fecha_habil = date('d-m-Y');
		//$fecha_habil = date("d-m-Y", strtotime("$fecha_habil + 10 days"));

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('dejavusans', '', 10);

		// add a page
		$pdf->AddPage();

		// set some text to print
		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$genero = $cliente->genero;
		if ($genero == 'Masculino') {
			$gen = 'el Sr.';
		} else if ($genero == 'Femenino') {
			$gen = 'la Sra.';
		} else {
			$gen = 'el Sr/a.';
		}

		$txt1 = '  

					 <style> 
					
		            p.ex1{
		            	text-align:justify;
		            	font-size: 95%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	
					     
		            }
		          </style>



		          	
		          	<p class="ex1">
					Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].' <br/> <br/> <br/>  <br/>

					Señor<br/> 
					??? <br/> 
				
					Direccion<br/> <br/> 
					<br/>  

					Adeudado US$'.$deuda.' desde  '.$fecha_ultimo_pago[2]." de ".$this->meses[$ultimonummes]." de ".$fecha_ultimo_pago[0].'

						<br/> <br/> <br/> 

					Estimados Sr/a: ?????? <br/>  <br/> <br/> <br/>  
					 La presente tiene por objeto comunicarle, que requerimos la regularización inmediata de la situación
					de débito que registra su empresa con la empresa denominada PRÉSTAMOS 911, S.A. <br/> <br/> <br/>
					Estaremos remitiendo a nuestros abogados esta deuda para que la misma sea finiquitada por
					medios judiciales; a menos que antes de los 10 dias habiles a la fecha de esta carta, se reciba el total
						de la deuda.
					<br/> <br/> 
				Esto en virtud, de la autorización de descuento directo que su persona suscribió como Persona Autorizada de la empresa '.$empresa->nombre.' '.$empresa->ruc.', empleador de el señor '.$cliente->nombre.' '.$cliente->apellido.', mayor de edad, portador de la cédula de identidad personal	número '.$cliente->cedula.', quien suscribió contrato de préstamo con nuestra institución financiera.

				<br/>
				<br/>
				<br/>
				
				Lamentamos haber adoptado esta postura y esperamos no tener que acudir a la intervención
				judicial. <br/> <br/> <br/>
				

				Atentamente, <br/> <br/> <br/>

				Por: PRESTAMOS 911, S.A.
				<br/> <br/> <br/> <br/> <br/> 
				_____________________________ 
				<br/>  <br/> <br/> <br/> <br/> <br/> 
				 </p>
			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);


		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}



	public function ver_carta_no_judicial_gerentes($id){
		$this->carta_no_judicial_gerentes($id,'I');
	}

	public function generar_carta_no_judicial_gerentes($id){
		$this->carta_no_judicial_gerentes($id,'D');
	}

	public function carta_no_judicial_gerentes($id,$flag,$nombre = "cartanojudicialgerentes.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);
		$catidad_cobro = $this->cobros_model->cant_cobros_by_aprobacion_id($aprobacion->id);


		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('dejavusans', '', 12);

		// add a page
		$pdf->AddPage();


		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$primer_descuento = explode('-', $aprobacion->fecha_primer_descuento);
		$nummes = $primer_descuento[1] - 1;

		$genero = $cliente->genero;
		if ($genero == 'Masculino') {
			$gen = 'el Sr.';
		} else if ($genero == 'Femenino') {
			$gen = 'la Sra.';
		} else {
			$gen = 'el Sr/a.';
		}

		// set some text to print

		$txt1 = '  

					 <style> 
					
		            p.ex1{
		            	text-align:justify;
		            	font-size: 95%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	
					    
		            }
		          </style>



		          	
		          	<p class="ex1">
					Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].' <br/> <br/> <br/>  <br/>

					Señor<br/> <br/>
					'.$empresa->nombre_rrhh.'<br/>  
					'.$empresa->nombre.'<br/> 
					'.$empresa->direccion.'<br/> <br/> 
					<br/> <br/> 

					Adeudado US$'.$cotizacion['totalcobrar'].' desde '.$primer_descuento[2]." de ".$this->meses[$nummes]." de ".$primer_descuento[0].'

						<br/> <br/> <br/> 

					Estimados Sr/a: '.$empresa->nombre_rrhh.' <br/>  <br/> <br/> 
					 La presente tiene por objeto comunicarle, que requerimos la regularización inmediata de la situación
					de débito que registra su empresa con la empresa denominada PRÉSTAMOS 911, S.A. <br/> <br/> <br/>
					Estaremos remitiendo a nuestros abogados esta deuda para que la misma sea finiquitada por
					medios judiciales; a menos que antes de los 10 dias despues de la fecha de la carta, se reciba el total	de la deuda.
					<br/> <br/> 
				Esto en virtud, de la autorización de descuento directo que el Sr. '.$empresa->nombre_firma.', suscribió como '.$empresa->posicion_firma.' de la empresa '.$empresa->nombre.' '.$empresa->ruc.', empleador del Sr/a. '.$cliente->nombre.' '.$cliente->apellido.', mayor de edad, portador de la cédula de identidad personal número '.$cliente->cedula.', quien suscribió contrato de préstamo con nuestra institución financiera.

				<br/>
				<br/>
				
				Lamentamos haber adoptado esta postura y esperamos no tener que acudir a la intervención
				judicial. <br/> <br/> <br/>
				

				Atentamente, <br/> <br/> <br/>

				Por: PRESTAMOS 911, S.A.
				<br/> <br/> <br/> <br/>   <br/> <br/>  
				 </p>
			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);

			$txt2 = ' 
				<style> 
					
		            p {
		            	text-align:center;
		            	font-size: 90%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            }
		          </style> <p>
				Adjunto. Copia simple de la carta de autorización de descuento directo debidamente firmada: </p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'C', true);



		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}






	public function ver_carta_cancelacion($id){
		$this->carta_cancelacion($id,'I');
	}

	public function generar_carta_cancelacion($id){
		$this->carta_cancelacion($id,'D');
	}

	public function carta_cancelacion($id,$flag,$nombre = "cartacancelacion.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);
		$catidad_cobro = $this->cobros_model->cant_cobros_by_aprobacion_id($aprobacion->id);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(20, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('dejavusans', '', 12);

		// add a page
		$pdf->AddPage();


		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		$primer_descuento = explode('-', $aprobacion->fecha_primer_descuento);
		$nummes = $primer_descuento[1] - 1;


		$txt = ' <style>
			
			p {
				text-align:justify;
		            	font-size: 100%;
		            	font-weight: normal;
		            	font-family: Arial, Helvetica, Verdana
		            	 margin-left: 10em;

			}
			
			</style>
					<p>
					Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].' <br/> <br/> <br/>  

					Señor/a<br/> <br/>
					'.$empresa->nombre.'<br/>  <br/>
				
					E.S.M.<br/> <br/> 
					<br/>  

					Estimados Señores <br/> <br/> <br/>

					A solicitud de la parte interesada, certificamos que el préstamo identificado con el No.
					'.$aprobacion->id.', a nombre de '.$cliente->nombre.' '.$cliente->apellido.', con documento de
					identidad personal No. '.$cliente->cedula.', se encontro cancelado y con saldo cero. <br/> <br/> <br/>
					Esta información es dada exclusivamente a requerimiento del cliente, por lo tanto no
					con lleva responsabilidad alguna para nuestra institución financiera o alguno de nuestro
					personal. <br/> <br/> <br/> <br/> <br/>
					Atentamente, <br/>  <br/> 

				Por: PRESTAMOS 911, S.A.
				<br/> <br/> <br/> <br/> <br/> <br/>

			 </p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}




	public function ver_recibo_pago($id){
		$this->carta_recibo_pago($id,'I');
	}

	public function generar_recibo_pago($id){
		$this->carta_recibo_pago($id,'D');
	}

	public function carta_recibo_pago($id,$flag,$nombre = "cartarecibopago.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);
		$catidad_cobro = $this->cobros_model->cant_cobros_by_aprobacion_id($aprobacion->id);

		$pagos = array();
		if ($catidad_cobro > 0) {
			for ($i=0; $i < $catidad_cobro; $i++) { 
				$pagos[] = $cotizacion['data'][$i]['total'];
			}
		}

		$monto = array_sum($pagos);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);


		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 12);

		// add a page
		$pdf->AddPage();

		$date = date('d-m-y');
		$date = explode('-', $date);

		$saldo_anterior = $cotizacion["totalpagar"];
		$pago_recibido = $monto;
		$nuevo_saldo = $saldo_anterior - $monto;


		// -----------------------------------------------------------------------------
		$pdf->SetMargins(25,0,0);


		$pdf->MultiCell(150, 5, 'RECIBO #', 0, 'R', 0, 1, '', '', true);

		$txt = '				
			<style>
							td.day
						{
							 
							 width: 7%;
							 font-size: 60%;
						     border-right: 1px solid white;
						     background-color: #000000;	
						      border: 1px solid black;						
    			            color: #FFFFFF;
						}
							td.month
						{
							 
							 width: 7%;
							 font-size: 60%;
						     
						     border-right: 1px solid white;
						     background-color: #000000;	
						      border: 1px solid black;						
    			            color: #FFFFFF;
						}
							td.year
						{
							 
							 width: 7%;
							 font-size: 60%;
						     border: 1px solid black;
						      
						     background-color: #000000;							
    			            color: #FFFFFF;
						}
						.derecha{
								   
							 width: 64%;
							 
						}
						td.borde
						{

						      border: 1px solid black;						
    			           
						}
					
							</style>
							
							<table align="right" cellpadding="3" >

							
							<tr>
							<td class="derecha">
							</td>
							<td class="day" align="center">DIA </td>
							<td class="month" align="center">MES</td>
							<td class="year" align="center">AÑO </td>
							</tr>
							<tr>
							<td class="derecha">
							</td>
							<td class="borde" align="center">'.$date[0].'</td>
							<td class="borde" align="center">'.$date[1].'</td>
							<td class="borde" align="center">'.$date[2].'</td>
								</tr>
							</table>
							
							
							
			 ';
		$pdf->writeHTMLCell(0, 5, '', '', $txt, 0, 1, false, true, 'R', true);
			
		$pdf->Ln(2);
		$tbl ='
			<style>
							td {
						    	 border: 1px solid black;
						    	font-weight: bold;
							    
							    border-collapse: collapse;
							}
							.nombre{
								  
							 
							  border: 1px solid black;
				  			 font-size: 70%;
							 width: 85%;
							 font-family:Verdana, Geneva, sans-serif;
							}
							.suma{
								  
							 
							  border: 1px solid black;
				  			 font-size: 70%;
							 
							 font-family:Verdana, Geneva, sans-serif;
							}
							.pago{
								  
							 
							  border: 1px solid black;
				  			 font-size: 70%;
							 width: 85%;
							 font-family:Verdana, Geneva, sans-serif;
							}
							.monto{
								  
							 
							  border: 1px solid black;
				  			 font-size: 70%;
							 
							 font-family:Verdana, Geneva, sans-serif;
							}


							</style>
							<table   cellpadding="10"  align="center">
							
							 <tr nobr="true">
							  <td class="nombre" colspan="4" align="left"> &nbsp; Recibimos de: '.$cliente->nombre.' '.$cliente->apellido.' </td>
							  
							  
							  
							 </tr>
							 
							 <tr nobr="true">
							  <td class="suma" colspan="3" align="left">&nbsp; La suma de: '.$this->numtoletras($monto).'</td>
							  
							  <td  class="monto"  align="left">&nbsp; $: '.$monto.'</td>
							  
							 </tr>
							 <tr nobr="true">
							  <td  class="pago"colspan="4" align="left">&nbsp; En concepto de pago a préstamo #: '.$aprobacion->id.'</td>
							  
							 
							  
							 </tr>
							 <tr nobr="true">
							  <td  colspan="4" align="left"> </td>
							  
							  
							  
							 </tr>

							  


							 
							</table>';
	
			$pdf->writeHTMLCell(0, 0, '', '', $tbl, 0, 1, false, true, 'C', false);
			$pdf->Ln(2);
		
					$tb2 = '
								<style>
					 			
								 
					   			.separar {
								 
								  font-size: 80%;
							      font-weight: normal;
							       width: 37%;
							     
							     
								}
								.space {
								  
								  font-size: 80%;
							      font-weight: Bold;
							      font-style: italic;
							      
							       width: 14%;
								}

								.ancho{
									border-right: 1px solid black;
								   font-weight: Bold;
								  border: 1px solid black;
					  			 font-size: 70%;
								 width: 14%;
								}
								.tamano{
									  
								 
								  border: 1px solid black;
					  			 font-size: 70%;
								 width: 18%;
								}
					 			td.caja {
								 
								  font-size: 80%;
							      font-weight: normal;
							      border: 1px solid black;
							       width: 3%;
								}
								.recibido {
									
								  font-size: 50%;
							      font-weight: bold;
							        
							     		      
								}
								
					 			table.tab2 {
					 			text-align:center; 
							    
							  }
							  

							  </style>
								
					      
							<table  class="tab2" cellpadding="3" style="margin-top:10px; margin-left:300px;" width="100%"  >
								
								<tr nobr="true">
								  <td class="ancho"  align="left"> MONTO </td>
								  <td class="tamano"   align="left"> $'.$saldo_anterior.' </td>
								   <td class="space" rowspan="3" align="left"> Pagado en </td>
								   <td  class="caja" align="left"></td>
								    <td class="separar"   align="left"> En Efectivo _______________________ </td>
								  
								  
								 </tr>
								 <tr>
								 <td class="ancho"  align="left" > ABONO </td>
								  <td  class="tamano" align="left"> $'.$pago_recibido.'</td>
								   <td  class="caja" align="left"></td>
								   <td class="separar"  align="left"> Cheque N° _______________________ </td>
								  
								 </tr>
									 <tr nobr="true">
									  <td  class="ancho" rowspan="" align="left"> POR PAGAR </td>
								  <td class="tamano" rowspan=""  align="left"> $'.$nuevo_saldo.'</td>
								   <td  class="caja" align="left"></td>
								    <td class="separar"  align="left"> Transferencia _____________________</td>
								  
								  </tr>	
								   <tr nobr="true">
								     <td class="recibido" colspan="6" align="right" >  &nbsp; <br/>Recibido por:___________________________________________________________</td>
								       

								  
								  </tr>		


							   </table>
		   ';



	
			$pdf->writeHTMLCell(0, 0, '', '', $tb2, 0, 1, false, true, 'J', true);
			$pdf->SetFont('helvetica', '', 0);
			$pdf->SetMargins(-1,0,0);		
			$pdf->Ln(20);
			$pdf->SetFillColor(47,79,79);
			$pdf->setCellPaddings(1, 1, 1, 1);
			$pdf->Cell(0, 0, "", 0, 2, 'L', true);
			$pdf->Ln(4);
			$pdf->SetFont('helvetica', '', 12);
			$pdf->SetMargins(25,0,0);		
			$pdf->Ln(20);
			 


			$pdf->MultiCell(150, 5, 'RECIBO #', 0, 'R', 0, 1, '', '', true);
			$txt1 = '				
									
						<style>
						td.day
						{
							 
							 width: 7%;
							 font-size: 60%;
						     border-right: 1px solid white;
						     background-color: #000000;	
						      border: 1px solid black;						
    			            color: #FFFFFF;
						}
							td.month
						{
							 
							 width: 7%;
							 font-size: 60%;
						     
						     border-right: 1px solid white;
						     background-color: #000000;	
						      border: 1px solid black;						
    			            color: #FFFFFF;
						}
							td.year
						{
							 
							 width: 7%;
							 font-size: 60%;
						     border: 1px solid black;
						      
						     background-color: #000000;							
    			            color: #FFFFFF;
						}
						.derecha{
								   
							 width: 64%;
							 
						}
						td.borde
						{

						      border: 1px solid black;						
    			           
						}
					
							</style>
							
							<table align="right" cellpadding="3" >

							
							<tr>
							<td class="derecha">
							</td>
							<td class="day" align="center">DIA </td>
							<td class="month" align="center">MES</td>
							<td class="year" align="center">AÑO </td>
							</tr>
							<tr>
							<td class="derecha">
							</td>
							<td class="borde" align="center">'.$date[0].'</td>
							<td class="borde" align="center">'.$date[1].'</td>
							<td class="borde" align="center">'.$date[2].'</td>
								</tr>
							</table>
							
							
			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'R', true);
		$pdf->Ln(4);
		$tbl3 ='
							<style>
							td {
						    	 border: 1px solid black;
						    	font-weight: bold;
							    
							    border-collapse: collapse;
							}
							.nombre{
								  
							 
							  border: 1px solid black;
				  			 font-size: 70%;
							 width: 85%;
							 font-family:Verdana, Geneva, sans-serif;
							}
							.suma{
								  
							 
							  border: 1px solid black;
				  			 font-size: 70%;
							 
							 font-family:Verdana, Geneva, sans-serif;
							}
							.pago{
								  
							 
							  border: 1px solid black;
				  			 font-size: 70%;
							 width: 85%;
							 font-family:Verdana, Geneva, sans-serif;
							}
							.monto{
								  
							 
							  border: 1px solid black;
				  			 font-size: 70%;
							 
							 font-family:Verdana, Geneva, sans-serif;
							}


							</style>
							<table   cellpadding="10"  align="center">
							
							 <tr nobr="true">
							  <td class="nombre" colspan="4" align="left"> &nbsp; Recibimos de: '.$cliente->nombre.' '.$cliente->apellido.' </td>
							  
							  
							  
							 </tr>
							 
							 <tr nobr="true">
							  <td class="suma" colspan="3" align="left">&nbsp; La suma de: '.$this->numtoletras($monto).'</td>
							  
							  <td  class="monto"  align="left">&nbsp; $: '.$monto.'</td>
							  
							 </tr>
							 <tr nobr="true">
							  <td  class="pago"colspan="4" align="left">&nbsp; En concepto de pago a préstamo #: '.$aprobacion->id.'</td>
							  
							 
							  
							 </tr>
							 <tr nobr="true">
							  <td  colspan="4" align="left"> </td>
							  
							  
							  
							 </tr>

							  


							 
							</table>';
	
			$pdf->writeHTMLCell(0, 0, '', '', $tbl3, 0, 1, false, true, 'C', false);
			$pdf->Ln(2);

		$tb4 = '
								<style>
					 			
								 
					   			.separar {
								 
								  font-size: 80%;
							      font-weight: normal;
							       width: 37%;
							     
							     
								}
								.space {
								  
								  font-size: 80%;
							      font-weight: Bold;
							      font-style: italic;
							      
							       width: 14%;
								}

								.ancho{
									border-right: 1px solid black;
								   font-weight: Bold;
								  border: 1px solid black;
					  			 font-size: 70%;
								 width: 14%;
								}
								.tamano{
									  
								 
								  border: 1px solid black;
					  			 font-size: 70%;
								 width: 18%;
								}
					 			td.caja {
								 
								  font-size: 80%;
							      font-weight: normal;
							      border: 1px solid black;
							       width: 3%;
								}
								.recibido {
									
								  font-size: 50%;
							      font-weight: bold;
							        
							     		      
								}
								
					 			table.tab2 {
					 			text-align:center; 
							    
							  }
							  

							  </style>
								
					      
							<table  class="tab2" cellpadding="3" style="margin-top:10px; margin-left:300px;" width="100%"  >
								
								<tr nobr="true">
								  <td class="ancho"  align="left"> MONTO </td>
								  <td class="tamano"   align="left"> $'.$saldo_anterior.' </td>
								   <td class="space" rowspan="3" align="left"> Pagado en </td>
								   <td  class="caja" align="left"></td>
								    <td class="separar"   align="left"> En Efectivo _______________________ </td>
								  
								  
								 </tr>
								 <tr>
								 <td class="ancho"  align="left" > ABONO </td>
								  <td  class="tamano" align="left"> $'.$pago_recibido.'</td>
								   <td  class="caja" align="left"></td>
								   <td class="separar"  align="left"> Cheque N° _______________________ </td>
								  
								 </tr>
									 <tr nobr="true">
									  <td  class="ancho" rowspan="" align="left"> POR PAGAR </td>
								  <td class="tamano" rowspan=""  align="left"> $'.$nuevo_saldo.'</td>
								   <td  class="caja" align="left"></td>
								    <td class="separar"  align="left"> Transferencia _____________________</td>
								  
								  </tr>	
								   <tr nobr="true">
								     <td class="recibido" colspan="6" align="right" >  &nbsp; <br/>Recibido por:___________________________________________________________</td>
								       

								  
								  </tr>		


							   </table>
							   ';



	
			$pdf->writeHTMLCell(0, 0, '', '', $tb4, 0, 1, false, true, 'J', true);
			
			$foto='
				<table>
				<tr>
				<td><img src="assets/images/logo.png" />
				</td>
				</tr>

				</table>

			 ';
		$pdf->writeHTMLCell(50, 50, 20, 145, $foto);  
		$pdf->writeHTMLCell(50, 50, 20, 10, $foto);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}




	public function ver_constancia_devolucion_intereses($id){
		$this->constancia_devolucion_intereses($id,'I');	
	}

	public function generar_constancia_devolucion_intereses($id){
		$this->constancia_devolucion_intereses($id,'D');		
	}

	public function constancia_devolucion_intereses($id,$flag,$nombre = "constanciadevolucionintereses.pdf"){

		$cliente = $this->clientes_model->get_by_id($id);
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);
		$catidad_cobro = $this->cobros_model->cant_cobros_by_aprobacion_id($aprobacion->id);

		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prestamos 911');
		$pdf->SetTitle('Constancia Devolución Intereses');

		// set default header data
		$pdf->SetHeaderData('logo.png', NULL, NULL, NULL, array(0,64,255), array(255,255,255));
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$pdf->SetFont('helvetica', '', 10, '', true);
		$pdf->AddPage();


		$pdf->setCellPaddings(1, 1, 1, 1);
		$pdf->setCellMargins(1, 1, 1, 1);
		$pdf->SetFillColor(255, 255, 127);

		$date = explode('-', date('Y-m-d'));
		$nummes = $date[1] - 1;

		
		$txt2 = 'Vía España, Plaza Regency <br/>
				Edif. 177, Piso# 17, Oficina # 2 <br/>
				Tel: 377-2911 Piso #17';
		$pdf->writeHTMLCell(0, 0, '', '', $txt2, 0, 1, 0, true, 'R', true);
		$pdf->Ln(20);
		
		$txt = '  Panamá, '.$date[2]." de ".$this->meses[$nummes]." de ".$date[0].' <br/> <br/> <br/> <br/>  <br/> <br/>
				La presente nota es para hacer constar que le estamos haciendo entrega de la devolución
				de los intereses del Préstamo # '.$aprobacion->id.', por la suma de Suma de intereses
				a devolver ???????, a la Sr/a. '.$cliente->nombre.' '.$cliente->apellido.'<br/> <br/> <br/><br/> <br/> <br/><br/> <br/> <br/> <br/> <br/><br/> <br/> <br/> <br/> <br/><br/> <br/>
				Recibido por: __________________________________

			 ';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}



	public function ver_condiciones_generales_new($id){
		$this->condiciones_generales_new($id,'I');	
	}

	public function generar_condiciones_generales_new($id){
		$this->condiciones_generales_new($id,'D');		
	}

	public function condiciones_generales_new($id,$flag,$nombre = "condiciones_generales.pdf"){
		$cliente = $this->clientes_model->get_by_id($id);
		$celular = empty($cliente->celular) ? '' : ' / ' .$cliente->celular;
		$solicitud = $this->solicitudes_model->get_solicitud_by_clientes_id($cliente->id);
		$solicitud_aprobacion = $this->solicitudes_aprobacion_model->get_by_solicitudes_id($solicitud->id);

		if (!$solicitud_aprobacion) {
			print_r('Solicitud debe estar aprobada para ver este documento.');
			return;
		}

		$usuario = $this->usuarios_model->getUsuario($this->session->userdata('id_user'));
		$agente_cedula = $usuario->cedula;
		$agente = $usuario->nombres . ' ' . $usuario->apellidos;
		
		$aprobacion = $this->aprobacion_model->get_by_id($solicitud_aprobacion->aprobacion_id);
		$empresa = $this->companias_model->get_by_id($cliente->companias_id);
		$db_access = $this->db_access_model->get_by_id($solicitud->db_access_id);
		$datos_verificados = $this->datos_verificacion_model->get_by_clientes_id($cliente->id);
		$desempleado = $this->desempleados_model->get_by_aprobacion($aprobacion->id);
		$producto = $this->productos_model->get_by_id($aprobacion->productos_id);
		$cotizacion = $this->cotizacion($aprobacion->cantidad_aprobada,$aprobacion->productos_id,$aprobacion->cantidad_meses,$aprobacion->fecha_primer_descuento);

		$pdf = new Pdf('P', 'mm', 'LEGAL', true, 'UTF-8', false);

		// set document information

		// set default header data
		$pdf->SetHeaderData('logo-lema.png', 80, NULL, NULL, array(0,64,255), array(255,255,255));
		$pdf->setNombreReporte('condiciones_generales');
		//$pdf->setFooterData(array(0,64,0), array(0,64,128));
		$pdf->setPrintFooter(false);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 23);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting(true);
		$pdf->SetFont('helvetica', '', 12, '', true);
		$pdf->AddPage('P','LEGAL');


		$pdf->setCellPaddings(1, 1, 1, 1);
		$pdf->setCellMargins(1, 1, 1, 1);
		$pdf->SetFillColor(255, 255, 127);		

		$m = $aprobacion->cantidad_meses;
		$t = $aprobacion->cantidad_meses;
		$totaldevolucion = ((($m * $m) + $m) / (($t * $t) + $t)) * $cotizacion["totalintereses"];

		$txt = '<p><b>PRÉSTAMO: N° '.$aprobacion->id.' FECHA: '.date('d/m/Y').'</b></p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, 0, true, 'L', true);

		$txt = '<p><b>CONDICIONES GENERALES ACORDADAS PARA LA FORMALIZACION DE PRÉSTAMOS:</b></p>';
		$pdf->writeHTMLCell(0, 0, '', '', $txt, 0, 1, 0, true, 'C', true);

		$txt1 = '  <style> 
			p {
			text-align:justify;
			font-size: 90%;
			font-weight: normal;
			font-family: Arial, Helvetica, Verdana
			}
			</style>
			<p>
			En la Ciudad de Panamá, República de Panamá comparecen, '.$agente.' varón/mujer, panameño/a, mayor de edad, portador de la cédula de identidad personal No. '.$agente_cedula.', actuando en nombre y representación de PRESTAMOS 911, S.A., sociedad anónima organizada de acuerdo con las leyes de la República de Panamá e inscrita en el Registro Publico a Folio No. 155615545, con domicilio en la Vía España frente a, Supermercado Rey, Edificio Plaza Regency, piso 17, teléfono 377-2911, (en adelante <b>EL ACREEDOR</b>), le ofrece a '.$cliente->nombre.' '.$cliente->apellido.', mujer (varón), mayor de edad, con cédula de identidad personal '.$cliente->cedula.' y con domicilio en '.$cliente->direccion.', Provincia de Panamá, República de Panamá, teléfono No. '. $cliente->telefono. $celular .', (en adelante <b>EL DEUDOR</b>), préstamo bajo los siguientes términos, condiciones y voluntariamente aceptados por ambas partes con relación a su solicitud de préstamo que ha sido objeto de aprobación.<br/> <br/>

			<b>PRIMERO:</b> El DEUDOR acepta que recibirá la suma de '.$this->numtoletras($aprobacion->cantidad_aprobada).' (USD$ '.$aprobacion->cantidad_aprobada.') antes de la cancelación y refinanciamiento, así mismo acepta que el monto total de la obligación producto del préstamo recibido es de '.$this->numtoletras($cotizacion['totalpagar']).' (USD$ '.number_format($cotizacion['totalpagar'],2,'.',',').'). Las Partes aceptan que todas las referencia en "dólares" o "USD$ ", se refieren a la moneda de curso legal de los Estados Unidos de América, de curso legal en la República de Panamá.<br/> <br/>

			<b>SEGUNDO:</b> EL DEUDOR pagará sobre los saldos diarios de préstamos adeudados a EL ACREEDOR una tasa de interés mensual de '.$producto->tasa_interes_mensual.'% Los intereses serán calculados por el método de interés en cuenta sobre saldo, que consiste en calcular los intereses sobre el saldo del capital prestado por el tiempo transcurrido. Para calcular los intereses se tomará en cuenta el número de días calendarios transcurridos y utilizando como base '. strtolower($this->numtoletras($aprobacion->cantidad_meses,FALSE)).' ('.$aprobacion->cantidad_meses.') meses de '.$this->numtoletras(($aprobacion->cantidad_meses*30),FALSE).' ('.($aprobacion->cantidad_meses*30).') días. Siempre y cuando se efectúen todos los pagos en la fecha de vencimiento de los mismos, EL DEUDOR pagará a EL ACREEDOR durante la vigencia del contrato de préstamo, en concepto de intereses la suma total de '.$this->numtoletras((float)$cotizacion['intereses']).' (US$ '.number_format($cotizacion['intereses'],2,'.',',').') y en concepto de F.E.C.I., la cantidad total de US$ 0.00 (si lo hubiere).<br/> <br/>

			<b>TERCERO:</b> La tasa de interés efectiva aplicada será 199 % anual.<br/> <br/>

			<b>CUARTO:</b> EL ACREEDOR retendrá del capital inicial para sí, (I) una comisión de cierre y de gastos de '.$this->numtoletras($cotizacion["totalcargo"]).' (USD$'.$cotizacion["totalcargo"].'), calculada sobre el saldo bruto del préstamo y (II) '.$this->numtoletras((float)$cotizacion['intereses']).' (USD$'.number_format($cotizacion['intereses'],2,'.',',').') en concepto de intereses. En el evento que el DEUDOR efectué el pago de la obligación en un plazo no mayor de tres (3) días hábiles después de la expiración de la obligación, se le hará una devolución de '.$this->numtoletras($totaldevolucion).' (USD$ '.number_format($totaldevolucion,2,'.',',').').<br/>
			Así también el ACREEDOR retendrá del capital inicial un suma equivalente a, 0.00% sobre el saldo bruto del préstamo, y será efectivo, si e, patrono o empleador del DEUDOR, cobra alguna suma por manejos administrativos de los descuentos a efectuar, caso contrario, se devolverá si la misma ha sido efectivamente retenida.<br/>
			El ACREEDOR declara que no retendrá suma alguna del capital inicial para comisiones y gastos cobrados y destinados a terceros. Cualquier cantidad aquí establecida serán retenidas del capital inicial dentro del monto total de la obligación.<br/> <br/>

			<b>QUINTO:</b> EL DEUDOR deberá pagar a EL ACREEDOR el monto total de la obligación establecida en la cláusula primera de este documento en un término de 35 días calendarios, mediante un sólo pago en dicho plazo. Este monto incluirá capital, intereses, gastos de cierre y FECI si los hubiera, y de existir un saldo pendiente o insoluto el DEUDOR deberá cancelar el mismo en un pago final dentro de los tres (3) días hábiles adicionales a la fecha de expiración del término de la obligación. En caso que el deudor realice el pago dentro del plazo ordinario de la obligación o dentro de los tres (3) días hábiles extraordinarios, se devolverá la suma de '.$this->numtoletras($totaldevolucion).' (USD$'.number_format($totaldevolucion,2,'.',',').').<br/> 
			En el caso que EL DEUDOR, no cancele la totalidad de la obligación dentro de los plazos aquí señalados, el mismo previa firma de autorización de descuento directo, tendrá un plazo de '.$this->numtoletras($aprobacion->cantidad_meses,FALSE).' meses para realizar la cancelación del monto adeudado a razón de '.($aprobacion->cantidad_meses * 2).' pagos quincenales por un monto de '.$this->numtoletras(round($cotizacion['totalpagar']/($aprobacion->cantidad_meses * 2),2)).' (USD$ '.number_format(($cotizacion['totalpagar']/($aprobacion->cantidad_meses * 2)),2,'.',',').') cada uno. Acuerda el ACREEDOR que si el DEUDOR cancela la totalidad del Préstamo en un periodo de '.$aprobacion->cantidad_meses.' meses, la obligación no se considerará como mora y el DEUDOR no será reportado a la Asociación Panameña de Crédito APC. <br/> <br/>
			<b>SEXTO:</b> Por instrucciones del DEUDOR el ACREEDOR cancelará directamente con el producto del préstamo, los compromisos crediticios, sujetos a la presentación de las respectivas cartas de saldos, de acuerdo a lo señalado a continuación:<br/>
			1. xxxxxxxxx<br/>
			2. xxxxxxxxx <br/> <br/>
			<b>SEPTIMO:</b> El Contrato de Préstamo y el Pagaré a la orden serán ambos firmados por EL DEUDOR y se consideran parte integral, para todos los efectos legales, en cuanto al cumplimiento de la obligación. <br/> <br/>
			<b>OCTAVO:</b> EL DEUDOR autoriza a EL ACREEDOR de ahora en adelante a recopilar, transmitir, verificar, o reportar de tiempo en tiempo a cualquier agencia de información de datos, instituciones financieras o cualesquiera agentes económicos de la localidad o del extranjero toda la información sobre el historial de crédito que hemos mantenido, mantenemos o lleguemos a mantener con cualquiera de ellos y con EL ACREEDOR. <br/> <br/>
			<b>NOVENO:</b> Declaran LAS PARTES que aceptan los términos y condiciones en su totalidad, este documento mantendrá una vigencia de (7) días calendarios a partir de su firma. Las Condiciones Generales aquí presentadas están sujetas a la aprobación del crédito y a la verificación de la información y de las referencias de crédito del Deudor y del Codeudor si lo hubiere. En caso de que exista CODEUDOR, debe incluirse sus generales tanto en el contrato de préstamo y en el pagaré. <br/> <br/>
			<b>DECIMO:</b> Declara el señor(a) __________________________________ con cédula de identidad personal _________________ y con domicilio en ___________________________________________________ Teléfono _________________, en libre ejercicio de su consentimiento y en plenos uso de sus facultades mentales y legales, que acepta ser el <b>CODEUDOR</b>, de la obligación aquí contraída por parte de EL DEUDOR, y se obliga de manera solidaria, con todas las obligaciones que surgen dentro del contrato de Préstamo, o bien cualquier obligación que mantenga EL DEUDOR con EL ACREEDOR.<br/> <br/>
			En FE DE LO CUAL, LAS PARTES suscriben el presente contrato en dos (2) ejemplares del mismo tenor y efecto, en la Ciudad de Panamá, a los '.date('d').' de '.date('m').' del '.date('Y').'.<br/> <br/>


			<table  align="center">
			<tr nobr="true">
			<th colspan="3"></th>
			</tr>
			<tr >
			<td  align="left"> <b>ACREEDOR</b>  <br/> <br/><br/>
			_________________________<br/><br/>
			PRESTAMOS 911<br/>
			Nombre: '.$agente.' <br/>
			Cédula No. '. $agente_cedula.'
			</td>
			
			<td align="left"><b>EL DEUDOR</b> <br/> <br/><br/>
			_________________________<br/><br/>
			Nombre: '.$cliente->nombre.' '.$cliente->apellido.'<br/>
			Cédula No. '.$cliente->cedula.'
			</td>
			<td align="left"><b>EL CODEUDOR</b> <br/> <br/><br/>
			_________________________<br/><br/>
			Nombre: <br/>
			Cédula No. 
			</td>
			</tr>

			</table>

		';
		$pdf->writeHTMLCell(0, 0, '', '', $txt1, 0, 1, false, true, 'J', true);

		$pdf->lastPage();

		$nombre_archivo = utf8_decode($nombre);
		if($flag == 'I' || $flag == 'D'){
			$pdf->Output($nombre_archivo, $flag);			
		}else{
			$this->files[] = $this->path . $nombre_archivo;
			$pdf->Output( $this->path . $nombre_archivo, $flag);
		}
	}

	public function get_detalle_cargos(){
		$fecha_i = $this->input->post('fecha_i');
		$fecha_f = $this->input->post('fecha_f');
		$fecha_i = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$fecha_i)));
		$fecha_f = date('Y-m-d H:i:s', strtotime(str_replace("/","-",$fecha_f)));
		$list = $this->solicitudes_view_model->get_all_by_fechas($fecha_i,$fecha_f);

		$data = array();

		foreach ($list as $key => $value) {
			$cotizacion = $this->cotizacion($value->cantidad_aprobada,$value->productos_id,$value->cantidad_meses,$value->fecha_primer_descuento);
			$data[$key] = (array)$value;
			$data[$key]['cotizacion'] = $cotizacion;
			$data[$key]['calculos']['intereses'] = (string)number_format($cotizacion['intereses'],2);
			$data[$key]['calculos']['notariales'] = (string)number_format($cotizacion['sologasto'],2);
			$data[$key]['calculos']['comision'] = (string)number_format($cotizacion['solocomision'],2);
			$data[$key]['calculos']['cargo'] = (string)number_format($cotizacion['solocargo'],2);
			$data[$key]['calculos']['itbms'] = (string)number_format(round(($cotizacion['solocomision'] * 7) / 100,2),2);
		}

		echo json_encode(array('records' => $data));
	}
}