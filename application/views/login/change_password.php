<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo site_url('gentelella-master/vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo site_url('gentelella-master/vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo site_url('gentelella-master/vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo site_url('gentelella-master/vendors/animate.css/animate.min.css') ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo site_url('gentelella-master/build/css/custom.min.css') ?>" rel="stylesheet">
  </head>

  <body class="login">
    <div>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">

            <?php echo form_open(base_url().'login/process_password'); ?>    

              <h1>Actualización</h1>

              <p>Aquí podras cambiar la contraseña enviada, por una a tu gusto.</p>    

              <?=form_hidden('id',$id_user)?>

              <div>
				        <input name="correo" required type="email" class="form-control" placeholder="Correo Electrónico"  value="<?php echo set_value('correo'); ?>"/>
              </div>
              <div>
                <input name="password" required type="password" class="form-control" placeholder="Ingresa tu nueva contraseña"/>
              </div>
              <div>
                <input name="rpassword" required type="password" class="form-control" placeholder="Repite tu nueva contraseña"/>
              </div>

              <?php if (form_error('correo')) { echo "<div class='alert alert-danger'>". form_error('correo') ."</div>"; } ?>
              <?php if (form_error('password')) { echo "<div class='alert alert-danger'>". form_error('password') ."</div>"; } ?>
              <?php if (form_error('rpassword')) { echo "<div class='alert alert-danger'>". form_error('rpassword') ."</div>"; } ?>

			       <?php if($this->session->flashdata('aviso')) {  echo "<div class='alert alert-danger'>". $this->session->flashdata('aviso') ."</div>"; } ?>

              <div>
              	<?=form_hidden('token',$token)?>
                <input name="submit" type="submit" class="btn btn-default" value="Actualizar contraseña">
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <!-- <h1><i class="fa fa-money"></i> <?= $title ?></h1> -->
                  <img src="<?php echo site_url('assets/images/logo_prestamos.png') ?>">
                  <p>© 2016 Todos los derechos reservados. <br> <?= $seven ?></p>
                </div>
              </div>
            <?php echo form_close()?>
          </section>
        </div>

      </div>
    </div>
  </body>
</html>
