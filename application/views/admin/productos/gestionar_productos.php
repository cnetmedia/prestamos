<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gestionar Productos <small>Listado de Productos</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

          	<?php if ($insertar == true) { ?>
            	<button class="btn btn-success btn-xs" onclick="add_products()"><i class="glyphicon glyphicon-plus"></i> Crear Producto</button>
          	<?php } ?>

            <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
            
            <span class="permission-error label label-danger"></span>

            <br><br>

            <table id="datatable-responsive-productos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Suma ($)</th>
                  <th>Plazo (Meses)</th>
                  <th>Interés (%)</th>
                  <th>Interés Vencido (%)</th>
                  <th>Plazo de Gracia</th>
                  <th>Tasa Efectiva</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
              <tr>
                  <th>Nombre</th>
                  <th>Suma ($)</th>
                  <th>Plazo (Meses)</th>
                  <th>Interés (%)</th>
                  <th>Interés Vencido (%)</th>
                  <th>Plazo de Gracia</th>
                  <th>Tasa Efectiva</th>
                  <th>Acciones</th>
              </tr>
              </tfoot>

            </table>
          </div>
        </div>
      </div>
  </div>
</div>
<!-- /page content -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Productos</h3>
            </div>
            <div class="modal-body form">

              <form id="form" class="form-horizontal form-label-left input_mask">
              <input type="hidden" value="" name="id"/>

                <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <label>Nombre del Producto</label>
					        <input type="text" name="nombre" required class="form-control has-feedback-left" id="inputSuccess" placeholder="Nombre del Producto">
                  	<span class="fa fa-product-hunt form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Suma Mínima</label>
                  <input type="text" name="suma_minima" required class="form-control has-feedback-left" id="inputSuccess1" placeholder="Suma Mínima">
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Suma Máxima</label>
                  <input type="text" name="suma_maxima" required class="form-control" id="inputSuccess2" placeholder="Suma Máxima">
                  <span class="fa fa-usd form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Plazo Mínimo</label>
                  <input type="text" name="plazo_minimo" required class="form-control has-feedback-left" id="inputSuccess3" placeholder="Plazo Mínimo">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Plazo Máximo</label>
                  <input type="text" name="plazo_maximo" required class="form-control" id="inputSuccess4" placeholder="Plazo Máximo">
                  <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Gastos notariales y Legales</label>
                  <input type="text" name="gastos_notariales" required class="form-control has-feedback-left" id="inputSuccess5" placeholder="Gastos notariales y Legales">
                  <span class="fa fa-percent form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Cargo Administrativo</label>
                  <input type="text" name="cargo_administrativo" required class="form-control has-feedback-right" id="inputSuccess12" placeholder="Cargo Administrativo">
                  <span class="fa fa-percent form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Comisión</label>
                  <input type="text" name="comision" required class="form-control has-feedback-left" id="inputSuccess11" placeholder="Comisión">
                  <span class="fa fa-percent form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Tasa de Interés Mensual</label>
                  <input type="text" name="tasa_interes_mensual" required class="form-control" id="inputSuccess6" placeholder="Tasa de Interés Mensual">
                  <span class="fa fa-percent form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Plazo de Gracia</label>
                  <input type="text" name="dias_ultimo_plazo_grabacion" required class="form-control has-feedback-left" id="inputSuccess7" placeholder="Plazo de Gracia">
                  <span class="fa fa-sun-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Tasa de Intereses Vencidos</label>
                  <input type="text" name="tasa_interes_vencido" required class="form-control" id="inputSuccess8" placeholder="Tasa de Intereses Vencidos">
                  <span class="fa fa-percent form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Tasa de Interes Efectiva</label>
                  <input type="text" name="tasa_interes_efectiva" required class="form-control has-feedback-left" id="inputSuccess9" placeholder="Tasa de Interes Efectiva">
                  <span class="fa fa-percent form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"  name="visible_cliente" id="inputSuccess10" value="Si"> Visible al Cliente
                    </label>
                  </div>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->



<!-- view modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información del Producto</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary">Nombres</h2></u></strong>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-suma_minima">
                      <i class="fa fa-usd"></i> Suma Mínima:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-suma_maxima">
                      <i class="fa fa-usd"></i> Suma Máxima:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-plazo_minimo">
                      <i class="fa fa-calendar-o"></i> Plazo Mínimo:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-plazo_maximo">
                      <i class="fa fa-calendar-o"></i> Plazo Máximo:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-gastos_notariales">
                      <i class="fa fa-percent"></i> Gastos notariales y Legales:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-cargo_administrativo">
                      <i class="fa fa-percent"></i> Cargo Administrativo:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-comision">
                      <i class="fa fa-percent"></i> Comisión:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-tasa_interes_mensual">
                      <i class="fa fa-percent"></i> Tasa de Interés Mensual:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-dias_ultimo_plazo_grabacion">
                      <i class="fa fa-sun-o"></i> Plazo de Gracia:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-tasa_interes_vencido">
                      <i class="fa fa-percent"></i> Tasa de interés Vencido:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-tasa_interes_efectiva">
                      <i class="fa fa-percent"></i> Tasa de interés Efectiva:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha_creacion">
                      <i class="fa fa-calendar-o"></i> Fecha de Creación:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-visible_cliente">
                      <i class="fa fa-eye"></i> Visible al Cliente:
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-usuarios_id">
                      <i class="fa fa-pencil"></i> Creado Por:
                    </div>
                </div> 
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End view modal -->

<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea eliminar el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->


<!-- jQuery -->
<script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">
var save_method; //for save method string
var table;
$(document).ready(function() {
    //datatables
    table = $('#datatable-responsive-productos').DataTable( {
        "order": [[ 0, "asc" ]],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('productos/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
	        { 
	            "targets": [ -1 ], //last column
	            "orderable": false, //set not orderable
	        },
        ],

    } );
});

function add_products()
{
    save_method = 'add';
    $('.permission-error').hide();
    $('#msg-error').hide();
	 $("#inputSuccess10").attr("checked",false);
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    //$('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Añadir Productos'); // Set Title to Bootstrap modal title
}

function justNumbers(e)
{
   var keynum = window.event ? window.event.keyCode : e.which;
   if ((keynum == 8) || (keynum == 46))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    //$('#btnSave').text('procesando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    if(save_method == 'add') {
        url = "<?php echo site_url('productos/ajax_add')?>";
    } else {
        url = "<?php echo site_url('productos/ajax_update')?>";
    }
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('#msg-error').hide();
            }
            else if (data.validation)
            {

                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            }
            else if (data.rol)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
            }
            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function view_products(id)
{
    $('.permission-error').hide();
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('productos/ajax_view/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {            
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información del Producto');
            $('.modal-nombre').text(data.nombre);
            $('.modal-suma_minima').html('<strong><i class="fa fa-usd"></i> Suma Mínima: </strong>$' + data.suma_minima);
            $('.modal-suma_maxima').html('<strong><i class="fa fa-usd"></i> Suma Máxima: </strong>$' + data.suma_maxima);
            $('.modal-plazo_minimo').html('<strong><i class="fa fa-calendar-o"></i> Plazo Mínimo: </strong>' + data.plazo_minimo + ' Meses');
            $('.modal-plazo_maximo').html('<strong><i class="fa fa-calendar-o"></i> Plazo Máximo: </strong>' + data.plazo_maximo + ' Meses');
            $('.modal-gastos_notariales').html('<strong><i class="fa fa-percent"></i> Gastos notariales y Legales: </strong>' + data.gastos_notariales + '%'); 
            $('.modal-cargo_administrativo').html('<strong><i class="fa fa-percent"></i> Cargo Administrativo: </strong>' + data.cargo_administrativo + '%');
            $('.modal-comision').html('<strong><i class="fa fa-percent"></i> Comisión: </strong>' + data.comision + '%');
            $('.modal-tasa_interes_mensual').html('<strong><i class="fa fa-percent"></i> Tasa de Interés Mensual: </strong>' + data.tasa_interes_mensual + '%');
            $('.modal-dias_ultimo_plazo_grabacion').html('<strong><i class="fa fa-sun-o"></i> Plazo de Gracia: </strong>' + data.dias_ultimo_plazo_grabacion + ' Dias');
            $('.modal-tasa_interes_vencido').html('<strong><i class="fa fa-percent"></i> Tasa de Interés Vencido: </strong>' + data.tasa_interes_vencido + '%');
            $('.modal-tasa_interes_efectiva').html('<strong><i class="fa fa-percent"></i> Tasa de Interés Efectiva: </strong>' + data.tasa_interes_efectiva + '%');
            $('.modal-visible_cliente').html('<strong><i class="fa fa-eye"></i> Visible al Cliente: </strong>' + data.visible_cliente);
            $('.modal-fecha_creacion').html('<strong><i class="fa fa-calendar-o"></i> Fecha de Creación: </strong>' + data.fecha_creacion);
            $('.modal-usuarios_id').html('<strong><i class="fa fa-pencil"></i> Creado Por: </strong>' + data.usuarios_id);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function edit_products(id)
{
	$('.permission-error').hide();
    $('#msg-error').hide();
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    //$('.help-block').empty(); // clear error string
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('productos/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
			if (data.permission) {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            } else {
	            $('[name="id"]').val(data.id);
	            $('[name="nombre"]').val(data.nombre);
	            $('[name="suma_minima"]').val(data.suma_minima);
	            $('[name="suma_maxima"]').val(data.suma_maxima);
	            $('[name="plazo_minimo"]').val(data.plazo_minimo);
	            $('[name="plazo_maximo"]').val(data.plazo_maximo);
              $('[name="gastos_notariales"]').val(data.gastos_notariales);
              $('[name="cargo_administrativo"]').val(data.cargo_administrativo);
	            $('[name="comision"]').val(data.comision);
	            $('[name="tasa_interes_mensual"]').val(data.tasa_interes_mensual);
	            $('[name="dias_ultimo_plazo_grabacion"]').val(data.dias_ultimo_plazo_grabacion);
	            $('[name="tasa_interes_vencido"]').val(data.tasa_interes_vencido);
              $('[name="tasa_interes_efectiva"]').val(data.tasa_interes_efectiva);
	            //$('[name="visible_cliente"]').val(data.visible_cliente);
	            if (data.visible_cliente == "Si") {
	            	$("#inputSuccess10").attr("checked",true);
	            } else {
	            	$("#inputSuccess10").attr("checked",false);
	            }
	            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	            $('.modal-title').text('Editar Productos'); // Set title to Bootstrap modal title
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function confirm_delete(id) {
    $('.permission-error').hide();
    $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal").modal('hide');     // dismiss the dialog
        });
    });
    $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });
    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" title="Hapus" onclick="delete_products('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function delete_products(id)
{
    if(id) {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('productos/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
                if(data.permission)
                {
                    $('.permission-error').show();
                    $('.permission-error').html(data.permission);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
        $("#myModal").modal('hide');
    }
}


</script>

