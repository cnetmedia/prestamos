  <style>
    #div_carga{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      /*background: url(<?php echo site_url('assets/images/gris.png') ?>) repeat;*/
      display:none;
      z-index:1;
    }

    #cargador{
      position:absolute;
      top:50%;
      left: 50%;
      margin-top: -25px;
      margin-left: -25px;
    }

    #cargador_aviso{
      position:absolute;
      top:40%;
      left: 47%;
      margin-top: -25px;
      margin-left: -25px;
    }
  </style>

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gestionar Cotización <small>Cotización de Prestamos</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div id="div_carga">
              <p id="cargador_aviso">Por favor espere...</p>
              <img id="cargador" src="<?php echo site_url('assets/images/ajax-loader.gif') ?>"/>
            </div>

<!--             <div class="alert alert-info">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong>Nota:</strong> Para cotizar debes ingresar primero el monto o suma de prestamo. El sistema reflejara los productos disponibles con esa cantidad para que luego puedas desplegarlos, al seleccionar el producto se reflejaran los plazos disponibles del mismo.
            </div> -->

            <button class="btn btn-default btn-xs" onclick="reiniciar()"><i class="glyphicon glyphicon-refresh"></i> Reiniciar</button>

            <div id="aviso" class="label label-danger"></div>

            <div class="row">
              <form id="form" class="form-horizontal form-label-left">
              <input type="hidden" value="" name="id"/>

                <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors animated shake"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="text" name="suma_prestamo" required class="form-control has-feedback-left" id="inputSuccess" placeholder="Suma de Prestamo" onkeyup="suma(this.value)">
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                  <select required class='form-control has-feedback-left' name='productos_id' id='inputSuccess2' placeholder='Producto' onchange='producto(this.value)'><option disabled selected hidden>Producto</option><!-- Opciones Ajax --></select>
                  <span class="fa fa-product-hunt form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                  <select required class='form-control has-feedback-left' name='plazo' id='inputSuccess3' placeholder='Plazo' onchange="meses(this.value)"><option disabled selected hidden>Plazo</option><!-- Opciones Ajax --></select>
                  <span class="fa fa-sun-o form-control-feedback left" aria-hidden="true"></span>
                </div>

              </form>
            </div>

            <div class="text-center resultado">
              <!-- Para resultado -->
            </div>

            <table class="table table-striped table-bordered dt-responsive nowrap" id="datatable-cotizacion" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Fecha de Pagos</th>
                  <th>Principal</th>
                  <th>Interés</th>
                  <th>Tasa de Interés</th>
                  <th>Cargo Administrativo</th>
                  <th>Gastos notariales</th>
                  <th>Comision</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody class="filas">
              <tfoot>
              <tr>
                <th>Fecha de Pagos</th>
                <th>Principal</th>
                <th>Interés</th>
                <th>Tasa de Interés</th>
                <th>Cargo Administrativo</th>
                <th>Gastos notariales</th>
                <th>Comision</th>
                <th>Total</th>
              </tr>
              </tfoot>
              </tbody>
            </table>

          </div>
        </div>
      </div>
  </div>
</div>
<!-- /page content -->

<!-- jQuery -->
<script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>


<script type="text/javascript">
  var table;
  $(document).ready(function() {
      //datatables
      table = $('#datatable-cotizacion').dataTable( {
        "paging": false,
        "ordering": false,
        "info": false,
        "bFilter": false,
        "language": {
            "zeroRecords": "Esperando cotización...",
            "infoEmpty": "No hay registros disponibles",
            "processing": "Procesando..."
        }
      });

      view_products();
  });

/*function suma(val) {
    //$('#aviso').hide();
    reiniciar();
    //var patron = /^(\d|-)?(\d|,)*\.?\d*$/; Para numero decimales positivos y negativos
    var patron = /^(\d)*\.?\d*$/;
    var validar = patron.test(val);
    if (validar) {
      $("#inputSuccess").val(val);
      if ($("#inputSuccess").val().length > 0 && $("#inputSuccess").val() > 0) {
        view_products();
        //cotiza();
      }
    } else {
      $("#inputSuccess").val(null);
    }
}*/

function suma(val) {
    var patron = /^(\d)*\.?\d*$/;
    var validar = patron.test(val);
    if (validar) {
      $("#inputSuccess").val(val);
      if ($("#inputSuccess").val().length > 0 && $("#inputSuccess").val() > 0) {
        cotiza();
      }
    } else {
      $("#inputSuccess").val(null);
    }
}

/*function producto(val){
    $('#aviso').hide();
    $('.resultado').html('');
    $('.filas').empty();
    $('.filas').append('<tr><td colspan="6" align="center">Esperando cotización...</td></tr>');
    if (parseInt(val)) {
      $('#div_carga').show();
      $.ajax({
          url : "<?php echo site_url('cotizacion/plazo_by_products')?>",
          type: "POST",
          data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {
              $('#inputSuccess3').empty();
              var trHTML = "<option disabled selected hidden>Plazo</option>";
              if (data.length > 0) {
                  $.each(data, function (i, item) {
                      trHTML += "<option value='" + item + "'>" + item + " Meses</option>";
                  });
              }
              $('#inputSuccess3').append(trHTML);
              $('#div_carga').hide();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $('#div_carga').hide();
              alert('Error get data from ajax');
          }
      });
    } else {
      if (monto_disponible == false) {
        view_products();
      } else if ($('#inputSuccess').val() == "" || $('#inputSuccess').val() == null) {
        $('#aviso').show().text('Ingrese suma del prestamo a cotizar.');
      }
    }
}*/




function producto(id){
    $('#aviso').hide();
    $('.resultado').html('');
    $('.filas').empty();
    $('.filas').append('<tr><td colspan="8" align="center">Esperando cotización...</td></tr>');
    if (parseInt(id)) {
      $('#div_carga').show();
      $.ajax({
          url : "<?php echo site_url('cotizacion/plazo_by_products/')?>" + id,
          type: "POST",
          //data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {
              if (data.data) {
                $('#aviso').show().text('El monto mínimo disponible es de $' + data.data.suma_minima + ' y el máximo es de $' + data.data.suma_maxima);
              }
              $('#inputSuccess3').empty();
              var trHTML = "<option disabled selected hidden>Plazo</option>";
              if (data.plazo.length > 0) {
                  $.each(data.plazo, function (i, item) {
                      trHTML += "<option value='" + item + "'>" + item + " Meses</option>";
                  });
              }
              $('#inputSuccess3').append(trHTML);
              $('#div_carga').hide();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $('#div_carga').hide();
              alert('Error get data from ajax');
          }
      });
    }
}



function meses(val) {
    if (parseInt(val)) {
      cotiza();
    }
}

/*function meses(val) {
    var patron = /^(\d)*$/;
    var validar = patron.test(val);
    if (validar) {      
      $("#inputSuccess3").val(val);
      if ($("#inputSuccess3").val().length > 0 && $("#inputSuccess3").val() > 0) {
        cotiza();
      } else {
        $('.resultado').html('');
        $('.filas').empty();
        $('.filas').append('<tr><td colspan="6" align="center">Esperando cotización...</td></tr>');
      }
    } else {
      $("#inputSuccess3").val(null);
    }
}*/

function reiniciar(){
  //$('#aviso').hide();
  $('#form')[0].reset();
  $('#inputSuccess2').empty();
  $('#inputSuccess2').append("<option disabled selected hidden>Producto</option>");
  $('#inputSuccess3').empty();
  $('#inputSuccess3').append("<option disabled selected hidden>Plazo</option>");
  $('.resultado').html('');
  $('.filas').empty();
  $('.filas').append('<tr><td colspan="8" align="center">Esperando cotización...</td></tr>');
  view_products();
}

//var monto_disponible = true;
/*function view_products()
{    
    //$('#aviso').hide();
    $('#div_carga').show();
    $.ajax({
        url : "<?php echo site_url('cotizacion/view_products')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            $('#inputSuccess2').empty();
            var trHTML = "<option disabled selected hidden>Producto</option>";
            if (data.data.length > 0) {
                $.each(data.data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
                monto_disponible = true;
                $('#aviso').hide();
            } else {    
                monto_disponible = false;            
                $('#aviso').show().text('El monto mínimo disponible es de $' + data.min + ' y el máximo es de $' + data.max);
            }
            $('#inputSuccess2').append(trHTML);
            $('#div_carga').hide();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#div_carga').hide();
            alert('Error get data from ajax');
        }
    });
}*/

function view_products()
{    
    //$('#aviso').hide();
    $('#div_carga').show();
    $.ajax({
        url : "<?php echo site_url('cotizacion/view_products')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            $('#inputSuccess2').empty();
            var trHTML = "<option disabled selected hidden>Producto</option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
                $('#aviso').hide();
            }
            $('#inputSuccess2').append(trHTML);
            $('#div_carga').hide();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#div_carga').hide();
            alert('Error get data from ajax');
        }
    });
}

function cotiza(){

    var suma,producto,plazo,flag,flag1,flag2;

    suma = $("#inputSuccess").val();
    producto = $("#inputSuccess2").val();
    plazo = $("#inputSuccess3").val();

    if (suma == null || suma == "" || suma == 0) {flag = false;} else {flag = true;}
    if (producto == null) {flag1 = false;} else {flag1 = true;}
    if (plazo == null || plazo == "" || plazo == 0) {flag2 = false;} else {flag2 = true;}

    if (flag == true && flag1 == true && flag2 == true) {
      $('#div_carga').show();
      $.ajax({
          url : "<?php echo site_url('cotizacion/ajax_list')?>",
          type: "POST",
          data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {
              $('.filas').empty();
              var trHTML = '';
              $.each(data.data, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>' + item.fecha + '</td>' +
                    '<td>$' + item.principal.toFixed(2) + '</td>' +
                    '<td>$' + item.interes.toFixed(2) + '</td>' +
                    '<td>%' + item.tasa.toFixed(2) + '</td>' +
                    '<td>$' + item.cargo.toFixed(2) + '</td>' +
                    '<td>$' + item.sologasto.toFixed(2) + '</td>' +
                    '<td>$' + item.solocomision.toFixed(2) + '</td>' +
                    '<td>$' + item.total.toFixed(2) + '</td>' +
                  '</tr>';
              });
              trHTML += '<tr><td></td><td><span class="label label-warning">$'+ data.totalprincipal.toFixed(2) +'</span></td><td><span class="label label-info">$'+ data.totalintereses.toFixed(2) +'</span></td><td></td><td><span class="label label-primary">$'+ data.totalcargo.toFixed(2) +'</span></td><td><span class="label label-primary">$'+ data.totalgasto.toFixed(2) +'</span></td><td><span class="label label-primary">$'+ data.totalcomision.toFixed(2) +'</span></td><td><span class="label label-success">$'+ data.totalcobrar.toFixed(2) +'</span></td></tr>';
              $('.filas').append(trHTML);
              $('.resultado').html("<h1>" + data.suma.toFixed(2) + " + " + data.intereses.toFixed(2) + " = " + data.totalpagar.toFixed(2) + "</h1>" + "<p>Prestamos + Suma de Intereses = Total</p>");
              $('#div_carga').hide();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $('#div_carga').hide();
              alert('Error adding / update data');
          }
      });
    }
}

</script>