<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Gestionar Compañias <small>Listado de Compañias</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <?php if ($insertar == true) { ?>
              <button class="btn btn-success btn-xs" onclick="add_companies()"><i class="glyphicon glyphicon-plus"></i> Crear Compañia</button>
            <?php } ?>

            <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
            
            <span class="permission-error label label-danger"></span>

            <br><br>

            <table id="datatable-responsive-companias" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>RUC</th>
                  <th>Teléfono</th>
                  <th>Correo</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
              <tr>
                  <th>Nombre</th>
                  <th>RUC</th>
                  <th>Teléfono</th>
                  <th>Correo</th>
                  <th>Acciones</th>
              </tr>
              </tfoot>

            </table>
          </div>
        </div>
      </div>
  </div>
</div>
<!-- /page content -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Compañias</h3>
            </div>
            <div class="modal-body form">

              <form id="form" class="form-horizontal form-label-left input_mask">
              <input type="hidden" value="" name="id"/>

                <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <label>Nombre de la Compañia</label>
                  <input type="text" name="nombre" required class="form-control has-feedback-left" id="inputSuccess" placeholder="Nombre de la Compañia">
                  <span class="fa fa-building-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <label>Dirección</label>
                  <textarea id="inputSuccess1" required class="form-control" name="direccion" placeholder="Dirección"></textarea>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Teléfono</label>
                  <input type="text" name="telefono" required class="form-control has-feedback-left" id="inputSuccess2" placeholder="Teléfono">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>RUC</label>
                  <input type="text" name="ruc" required class="form-control" id="inputSuccess3" placeholder="RUC">
                  <span class="fa fa-registered form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Sitio Web</label>
                  <input type="text" name="sitio_web" required class="form-control has-feedback-left" id="inputSuccess4" placeholder="Sitio Web">
                  <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Año de Constitución</label>
                  <input type="text" name="ano_constitucion" required class="form-control" id="inputSuccess5" placeholder="Año de Constitución">
                  <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Correo Electrónico</label>
                  <input type="text" name="correo" required class="form-control has-feedback-left" id="inputSuccess6" placeholder="Correo Electrónico">
                  <span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Nombre del Recursos Humano</label>
                  <input type="text" name="nombre_rrhh" required class="form-control" id="inputSuccess7" placeholder="Nombre del Recursos Humano">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Teléfono de Recursos Humanos</label>
                  <input type="text" name="telefono_rrhh" required class="form-control has-feedback-left" id="inputSuccess8" placeholder="Teléfono de Recursos Humanos">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Nombre de Contabilidad</label>
                  <input type="text" name="nombre_contabilidad" required class="form-control" id="inputSuccess7" placeholder="Nombre de Contabilidad">
                  <span class="fa fa-bookmark-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Teléfono de Contabilidad</label>
                  <input type="text" name="telefono_contabilidad" required class="form-control has-feedback-left" id="inputSuccess8" placeholder="Teléfono de Contabilidad">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Forma de Pago</label>
                  <select required class="form-control has-feedback-left" name="forma_pago" id="inputSuccess9" placeholder="Forma de Pago">
                    <option disabled selected hidden>Forma de Pago</option>
                    <option>ACH</option>
                    <option>Cheque</option>
                    <option>Efectivo</option>
                  </select>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Cantidad de Empleados</label>
                  <input type="text" name="cantidad_empleados" required class="form-control has-feedback-left" id="inputSuccess10" placeholder="Cantidad de Empleados">
                  <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Rubro</label>
                  <input type="text" name="rubro" required class="form-control" id="inputSuccess11" placeholder="Rubro">
                  <span class="fa fa-bookmark-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Nombre Firma</label>
                  <input type="text" name="nombre_firma" required class="form-control has-feedback-left" id="nombre_firma" placeholder="Nombre Firma">
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Posición Firma</label>
                  <input type="text" name="posicion_firma" required class="form-control" id="posicion_firma" placeholder="Posición Firma">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


<!-- view modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información de la Compañia</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- Espacio para info --></h2></u></strong>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-direccion">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-ruc">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-sitio_web">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-ano_constitucion">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-correo">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-nombre_rrhh">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono_rrhh">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-nombre_contabilidad">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono_contabilidad">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-forma_pago">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-cantidad_empleados">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-rubro">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-nombre-firma">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-posicion-firma">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha_creacion">
                      <!-- Espacio para info -->
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-usuarios_id">
                      <!-- Espacio para info -->
                    </div>
                </div> 
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End view modal-->

<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea eliminar el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="cid"/>

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

              <div class="table-responsive notes_datos">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Fecha</th>
                      <th class="column-title">Nota</th>
                      <th class="column-title">Usuario</th>
                    </tr>
                  </thead>

                  <tbody class="datos_companias">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- jQuery -->
<script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">
var save_method; //for save method string
var table;
$(document).ready(function() {
    //datatables
    table = $('#datatable-responsive-companias').DataTable( {
        //"order": [[ 0, "asc" ]],
        "order": [],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('companias/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          { 
              "targets": [ -1 ], //last column
              "orderable": false, //set not orderable
          },
        ],

    } );
});

function add_companies()
{
    save_method = 'add';
    $('.permission-error').hide();
    $('#msg-error').hide();
    $("#inputSuccess10").attr("checked",false);
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    //$('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Añadir Compañias'); // Set Title to Bootstrap modal title
}

function save()
{
    //$('#btnSave').text('procesando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    if(save_method == 'add') {
        url = "<?php echo site_url('companias/ajax_add')?>";
    } else {
        url = "<?php echo site_url('companias/ajax_update')?>";
    }
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('#msg-error').hide();
            }
            else if (data.validation)
            {

                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            }
            else if (data.rol)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
            }
            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax 
}

function view_companies(id)
{
    $('.permission-error').hide();
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('companias/ajax_view/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información de la Compañia');
            $('.modal-nombre').text(data.nombre);
            $('.modal-direccion').html('<strong>Dirección: </strong>' + data.direccion);
            $('.modal-telefono').html('<strong>Teléfono: </strong>' + data.telefono);
            $('.modal-ruc').html('<strong>RUC: </strong>' + data.ruc);
            $('.modal-sitio_web').html('<strong>Sitio Web: </strong>' + data.sitio_web);
            $('.modal-ano_constitucion').html('<strong>Año de Constitución: </strong>' + data.ano_constitucion);
            $('.modal-correo').html('<strong>Correo Electrónico: </strong>' + data.correo);
            $('.modal-nombre_rrhh').html('<strong>Nombre del Recursos Humanos: </strong>' + data.nombre_rrhh);
            $('.modal-telefono_rrhh').html('<strong>Teléfono de Recurso Humano: </strong>' + data.telefono_rrhh);
            $('.modal-nombre_contabilidad').html('<strong>Nombre de Contabilidad: </strong>' + data.nombre_contabilidad);
            $('.modal-telefono_contabilidad').html('<strong>Teléfono de Contabilidad: </strong>' + data.telefono_contabilidad);
            $('.modal-forma_pago').html('<strong>Forma de Pago: </strong>' + data.forma_pago);
            $('.modal-cantidad_empleados').html('<strong>Cantidad de Empleados: </strong>' + data.cantidad_empleados);
            $('.modal-rubro').html('<strong>Rubro: </strong>' + data.rubro);
            $('.modal-nombre-firma').html('<strong>Nombre Firma: </strong>' + data.nombre_firma);
            $('.modal-posicion-firma').html('<strong>Posición Firma: </strong>' + data.posicion_firma);
            $('.modal-fecha_creacion').html('<strong>Fecha de Creación: </strong>' + data.fecha_creacion);
            $('.modal-usuarios_id').html('<strong>Creado Por: </strong>' + data.usuarios_id);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function edit_companies(id)
{
    $('.permission-error').hide();
    $('#msg-error').hide();
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    //$('.help-block').empty(); // clear error string
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('companias/ajax_edit/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            if (data.permission) {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            } else {
                $('[name="id"]').val(data.id);
                $('[name="nombre"]').val(data.nombre);
                $('[name="direccion"]').val(data.direccion);
                $('[name="telefono"]').val(data.telefono);
                $('[name="ruc"]').val(data.ruc);
                $('[name="sitio_web"]').val(data.sitio_web);
                $('[name="ano_constitucion"]').val(data.ano_constitucion);
                $('[name="correo"]').val(data.correo);
                $('[name="nombre_rrhh"]').val(data.nombre_rrhh);
                $('[name="telefono_rrhh"]').val(data.telefono_rrhh);
                $('[name="nombre_contabilidad"]').val(data.nombre_contabilidad);
                $('[name="telefono_contabilidad"]').val(data.telefono_contabilidad);
                $('[name="forma_pago"]').val(data.forma_pago);
                $('[name="cantidad_empleados"]').val(data.cantidad_empleados);
                $('[name="rubro"]').val(data.rubro);
                $('[name="nombre_firma"]').val(data.nombre_firma);
                $('[name="posicion_firma"]').val(data.posicion_firma);

                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Editar Compañias'); // Set title to Bootstrap modal title
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function confirm_delete(id) {
    $('.permission-error').hide();
    $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal").modal('hide');     // dismiss the dialog
        });
    });
    $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });
    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" title="Hapus" onclick="delete_companies('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function delete_companies(id)
{
    if(id) {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('companias/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
                if(data.permission)
                {
                    $('.permission-error').show();
                    $('.permission-error').html(data.permission);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
        $("#myModal").modal('hide');
    }
}

function notes_companies(id){
  $('#modal_form_notas').modal('show');
  $('.modal-title').text('Añadir Nota');
  $('#form_notas')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="cid"]').val(id);
  datos_companias(id);
}

function save_notes(){
  $.ajax({
      url : "<?php echo site_url('companias/add_notes')?>",
      type: "POST",
      data: $('#form_notas').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            notes_companies($('[name="cid"]').val());
          } else if (data.validation) {
            $('#msg-error-notas').show();
            $('.list-errors-notas').html('<div class="animated shake">'+data.validation+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function datos_companias(id){
  $.ajax({
      url : "<?php echo site_url('companias/notas_compania/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

          $('.datos_companias').empty();
          var trHTML = '';
          if (data.length > 0) {
              $('.notes_datos').show();
              $.each(data, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>'+item.fecha_creacion+'</td>' +
                    '<td>'+item.nota+'</td>' +
                    '<td>'+item.usuarios_id+'</td>' +
                  '</tr>'
              });
          } else {
            $('.notes_datos').hide();
          }
          $('.datos_companias').append(trHTML);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

</script>