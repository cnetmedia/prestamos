        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Gestionar Usuarios <small>Listado de Usuarios</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <?php if ($insertar == true) { ?>
                      <button class="btn btn-success btn-xs" onclick="add_user()"><i class="glyphicon glyphicon-plus"></i> Crear Usuario</button>
                    <?php } ?>

                    <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
                    
                    <span class="permission-error label label-danger"></span>

                    <br><br>

                    <table id="datatable-responsive-usuarios" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Nombre Completo</th>
                          <th>Correo</th>
                          <th>Ext</th>
                          <th>Rol</th>
                          <th>Estado</th>
                          <th>Ultima Conexion</th>
                          <th>Compañia Asignada</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>

                      <tfoot>
                      <tr>
                          <th>Nombre Completo</th>
                          <th>Correo</th>
                          <th>Ext</th>
                          <th>Rol</th>
                          <th>Estado</th>
                          <th>Ultima Conexion</th>
                          <th>Compañia Asignada</th>
                          <th>Acciones</th>
                      </tr>
                      </tfoot>

                    </table>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- /page content -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Usuario</h3>
            </div>
            <div class="modal-body form">

              <form id="form" class="form-horizontal form-label-left input_mask">
              <input type="hidden" value="" name="id"/>

                <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors animated shake"></div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Nombres</label>
                  <input type="text" name="nombres" required class="form-control has-feedback-left" id="inputSuccess2" placeholder="Nombres">
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Apellidos</label>
                  <input type="text" name="apellidos" required class="form-control" id="inputSuccess3" placeholder="Apellidos">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Correo Electrónico</label>
                  <input type="email" name="correo" required class="form-control has-feedback-left" id="inputSuccess4" placeholder="Correo Electrónico">
                  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Cédula</label>
                  <input type="text" name="cedula" required class="form-control" id="inputSuccess5" placeholder="Cédula" maxlength="13" data-toggle="tooltip" data-placement="top" title="Formato: XX-XXXX-XXXXX" onkeyup="validarCedula(this.value)">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Extensión</label>
                  <input type="text" name="extension" required class="form-control has-feedback-left" id="inputSuccess6" placeholder="Extensión" onkeypress="return justNumbers(event);">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Rol de Usuario</label>
                  <?php echo "<select required class='select2_group form-control' name='roles_id' id='roles_id' placeholder='Rol de Usuario'><option disabled selected hidden>Rol de Usuario</option>";
                  if (count($result_roles)) {   
                    foreach ($result_roles as $row) {
                      echo "<option value='". $row['id'] . "'>" . $row['descripcion'] . "</option>";
                    }
                  }
                  echo "</select>";?> 
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Estado</label>
                  <select required class="form-control" name="estado" id="estado" placeholder="Estado">
                    <option disabled selected hidden>Estado</option>
                    <option>Bloqueado</option>
                    <option>Inactivo</option>
                    <option>Activo</option>
                  </select>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label><br></label>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" class="flat" name="resetpassword" id="resetpassword" value="yes"> Resetear Contraseña
                    </label>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <label for="descripcion">Descripción :</label>
                  <textarea id="descripcion" required="required" class="form-control" name="descripcion"></textarea>
                </div>

              </form>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea eliminar el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información del Usuario</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombres text-primary">Nombres</h2></u></strong>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-correo">
                      <i class="fa fa-envelope"></i> Correo:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-cedula">
                      <i class="fa fa-user"></i> Cedula:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-extension">
                      <i class="fa fa-phone"></i> Extensión:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha_creacion">
                      <i class="fa fa-calendar"></i> Fecha de Creación:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-ultima_conexion">
                      <i class="fa fa-sign-in"></i> Ultima Conexión:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-estado">
                      <i class="fa fa-adjust"></i> Estado:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-roles_id">
                      <i class="fa fa-black-tie"></i> Rol de Usuario:
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-usuarios_id">
                      <i class="fa fa-pencil"></i> Creado Por:
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-descripcion">
                      <i class="fa fa-file-text-o"></i> Descripción:
                    </div>
                </div> 
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_assign_company" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Asignar Compañia</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <strong><u><h2 class="modal-nombres-assign text-primary">Nombres</h2></u></strong>
              </div>            

              <form id="form_assign" class="form-horizontal form-label-left input_mask">

                <input type="hidden" name="userid"/>

                <div id="msg-error-assign" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-assign animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback selectcompany">

                  <select required class='form-control' name='companias_id' id='companias_id' style="width: 100%"></select>

                </div>

              </form>


              <div class="table-responsive company_user">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th>
                        <input type="checkbox" id="check-all" onchange="allcheck()">
                      </th>
                      <th class="column-title">Compañia </th>
                      </th>
                      <th class="bulk-actions" colspan="1">
                        <a class="antoo" style="color:#fff; font-weight:500;">Acciones ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                      </th>
                    </tr>
                  </thead>

                  <tbody class="datos_companias_usuarios">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success btn-xs" onclick="save_asign_company()">
                <i class="fa fa-check"></i> Asignar
              </button>
              <button type="button" class="btn btn-primary btn-xs" id="btnDeleteAsign" onclick="delete_asign_company()">
                <i class="fa fa-check"></i> Desasignar
              </button>
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>



<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#datatable-responsive-usuarios').DataTable( {
        "order": [[ 0, "asc" ]],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('usuarios/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    } );

    $("#inputSuccess5").inputmask();
});

function validarCedula(val)
{
    var pvalor = null;
    if (val.charAt(0).toUpperCase() == "P" || val.charAt(0).toUpperCase() == "E" || val.charAt(0).toUpperCase() == "N") {
      pvalor = val.charAt(0).toUpperCase() + val.slice(1);
      $("#inputSuccess5").val(pvalor);
    }
    if (val.charAt(1).toUpperCase() == "P" || val.charAt(1).toUpperCase() == "E" || val.charAt(1).toUpperCase() == "N") {
      pvalor = val.charAt(0).toUpperCase() + val.charAt(1).toUpperCase() + val.slice(1);
      $("#inputSuccess5").val(pvalor);
    }
    if (val.charAt(0).toUpperCase() == "-") {
      $("#inputSuccess5").val(null);
    }
    //$("#inputSuccess3").inputmask("Regex", { regex: "[pPeEnN]{0,2}[0-9]{0,1}-[0-9]*" }); 
    $("#inputSuccess5").inputmask("Regex", { regex: "[pPeEnN0-9]{2}-[0-9]{4}-[0-9]*" });
}

function justNumbers(e)
{
   var keynum = window.event ? window.event.keyCode : e.which;
   if ((keynum == 8) || (keynum == 46))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}


function add_user()
{    
    $('.checkbox').hide();
    $('.permission-error').hide();
    $('#msg-error').hide();
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    //$('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Añadir Usuario'); // Set Title to Bootstrap modal title
}

function view_user(id)
{

    $('.permission-error').hide();
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('usuarios/ajax_view/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información del Usuario');
            $('.modal-nombres').text(data.nombres + ' ' + data.apellidos);

            $('.modal-correo').html('<strong><i class="fa fa-envelope"></i> Correo: </strong>' + data.correo);
            $('.modal-cedula').html('<strong><i class="fa fa-user"></i> Cedula: </strong>' + data.cedula);
            $('.modal-extension').html('<strong><i class="fa fa-phone"></i> Extensión: </strong>' + data.extension);
            $('.modal-descripcion').html('<strong><i class="fa fa-file-text-o"></i> Descripción: </strong>' + data.descripcion);
            $('.modal-fecha_creacion').html('<strong><i class="fa fa-calendar"></i> Fecha de Creación: </strong>' + data.fecha_creacion);
            $('.modal-ultima_conexion').html('<strong><i class="fa fa-sign-in"></i> Ultima Conexión: </strong>' + data.ultima_conexion);
            $('.modal-estado').html('<strong><i class="fa fa-adjust"></i> Estado: </strong>' + data.estado);
            $('.modal-roles_id').html('<strong><i class="fa fa-black-tie"></i> Rol de Usuario: </strong>' + data.roles_id);
            $('.modal-usuarios_id').html('<strong><i class="fa fa-pencil"></i> Creado Por: </strong>' + data.usuarios_id);


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });

}

function edit_user(id)
{
    $('.checkbox').show();
    $('.permission-error').hide();
    $('#msg-error').hide();
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    //$('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('usuarios/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="nombres"]').val(data.nombres);
            $('[name="apellidos"]').val(data.apellidos);
            $('[name="correo"]').val(data.correo);
            $('[name="cedula"]').val(data.cedula);
            $('[name="extension"]').val(data.extension);
            $('[name="roles_id"]').val(data.roles_id);
            $('[name="estado"]').val(data.estado);
            $('[name="descripcion"]').val(data.descripcion);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Usuario'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{

    //$('#btnSave').text('procesando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('usuarios/ajax_add')?>";
    } else {
        url = "<?php echo site_url('usuarios/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('#msg-error').hide();
            }
            else if (data.validation)
            {

                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            }
            else if (data.rol)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
            }

            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_user(id)
{
    //if(confirm('Are you sure delete this data?'))

    if(id)
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('usuarios/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();

                if(data.permission)
                {
                    $('.permission-error').show();
                    $('.permission-error').html(data.permission);
                }


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

        $("#myModal").modal('hide');

    }
}

function confirm_delete(id) {

    $('.permission-error').hide();

    $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal").modal('hide');     // dismiss the dialog
        });
    });

    $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });
    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_user('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}

function assign_company(id){
  $('#msg-error-assign').hide();
  $('#btnDeleteAsign').hide();
  $.ajax({
      url : "<?php echo site_url('usuarios/ajax_view/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
          
          $('#modal_assign_company').modal('show');
          $('.modal-title').text('Asignar Compañia');               
          $('.modal-nombres-assign').text(data.nombres + ' ' + data.apellidos);
          $('[name="userid"]').val(data.id);
          view_companies();
          datos_companias_usuarios(id);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {;
          alert('Error get data from ajax');
      }
  });
}

function view_companies()
{
    $.ajax({
        url : "<?php echo site_url('clientes/view_companies')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#companias_id').empty();
            var trHTML = "<option></option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#companias_id').append(trHTML);

            $("#companias_id").select2({
              placeholder: "Empresa",
              allowClear: false,
              language: "es",
              theme: "classic",
              dropdownParent: $(".selectcompany")
            });            

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function save_asign_company(){
  var clientid = $('[name="userid"]').val();
  $.ajax({
      url : "<?php echo site_url('usuarios/add_asign_company')?>",
      type: "POST",
      data: $('#form_assign').serialize(),
      dataType: "JSON",
      success: function(data)
      {

          if (data.status)
          {
              //$('#modal_assign_company').modal('hide');
              datos_companias_usuarios(clientid);
              reload_table();
          }
          else if (data.validation)
          {
              $('#msg-error-assign').show();
              $('.list-errors-assign').html('<div class="animated shake">'+data.validation+'</div>');
          }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

var numcompanies;
function datos_companias_usuarios(id){
    $.ajax({
        url : "<?php echo site_url('usuarios/companias_usuarios/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            //alert(data);  

            $('.datos_companias_usuarios').empty();
            var trHTML = '';
            if (data.length > 0) {
                $('.company_user').show();
                $.each(data, function (i, item) {
                    trHTML += '<tr class="even pointer">' +
                      '<td class="a-center ">' +
                        '<input type="checkbox" class="flat" name="compania'+item.companias_id+'" id="compania'+i+'" value="'+item.companias_id+'" onchange="onecheck()">' +
                      '</td>' +
                      '<td class=" ">'+item.compania+'</td>' +
                      '</td>' +
                    '</tr>'
                });
                numcompanies = data.length;
            } else {
              $('.company_user').hide();
            }
            $('.datos_companias_usuarios').append(trHTML);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function allcheck(){
  if ($('#check-all').prop('checked') == true) {
    for (var i = 0; i < numcompanies; i++) {
      $('#compania'+i).prop('checked',true);
    }
    $('#btnDeleteAsign').show();
  } else {
    for (var i = 0; i < numcompanies; i++) {
      $('#compania'+i).prop('checked',false);
    }
    $('#btnDeleteAsign').hide();
  }
}

function onecheck(){
  var flag = false;
  for (var i = 0; i < numcompanies; i++) {
    if ($('#compania'+i).prop('checked')) {
      flag = true;
    }
  }
  if (flag == true) {
    $('#btnDeleteAsign').show();
  } else {
    $('#btnDeleteAsign').hide();
  }
  

}

function delete_asign_company(){

  var clientid = $('[name="userid"]').val();

  for (var i = 0; i < numcompanies; i++) {
    if ($('#compania'+i).prop('checked')) {
      //alert($('#compania'+i).val());

        $.ajax({
            url : "<?php echo site_url('usuarios/disasign_company')?>",
            type: "POST",
            data: {"usuarios_id":clientid,"companias_id":$('#compania'+i).val()},
            dataType: "JSON",
            success: function(data)
            {

                if (data.status)
                {
                    datos_companias_usuarios(clientid);
                    $('#btnDeleteAsign').hide();
                    reload_table();
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });

    }
  }
}

</script>