<!-- page content -->
<div class="right_col" role="main">


    <div class="">
      <div class="row top_tiles">
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-user"></i></div>
            <div class="count"><?= $totalregistros ?></div>
            <h3>Registros</h3>
            <p>Total de registros</p>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="tile-stats">
            <p class="most"></p>
          </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Reportes de Movimientos de Usuarios <small>Listado de Reportes</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="row col-md-2 col-sm-2 col-xs-12 selectrol">
              <select class='form-control' name="filtrar_rol" id="filtrar_rol"></select>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-12 selectusers">
              <select class='form-control' name="filtrar_usuarios" id="filtrar_usuarios"></select>
            </div>

            <div class="row col-md-2 col-sm-2 col-xs-12 selectestado">
              <select class='form-control' name="filtrar_estados" id="filtrar_estados"></select>
            </div>

            <div class="clearfix"></div><br>

            <div class="row col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
              <div id="reportrange_mov" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                Por Movimiento:
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span>Por Movimiento</span> <b class="caret"></b>
              </div>
            </div>
          
            <div class="row col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
              <div id="reportrange" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                Por Verificar:
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span>Por Verificar</span> <b class="caret"></b>
              </div>
            </div>

            <div class="row col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
              <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                Por Aprobar:
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span>Por Aprobar</span> <b class="caret"></b>
              </div>
            </div>

            <div class="clearfix"></div>

            <h2 class="tituloconsulta"></h2>

            <div class="clearfix"></div>

            <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Refrescar</button>
            <button class="btn btn-default btn-xs" onclick="reload_all()"><i class="fa fa-undo"></i> Mostrar Todos</button>

             <table id="datatable-movimiento-usuarios" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Fecha Movimiento</th>
                    <th>Cliente</th>
                    <th>Agente</th>
                    <th>Envio a Por Verificar</th>
                    <th>Verificador</th>
                    <th>Envio a Por Aprobar</th>
                    <th>Aprobador</th>
                    <th>Envio a Por Firmar</th>
                    <th>Usuario</th>
                    <th>Envio a Por Entregar</th>
                    <th>Estado Actual</th>
                    <th>Elegido Actualmente</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th>Fecha Movimiento</th>
                    <th>Cliente</th>
                    <th>Agente</th>
                    <th>Envio a Por Verificar</th>
                    <th>Verificador</th>
                    <th>Envio a Por Aprobar</th>
                    <th>Aprobador</th>
                    <th>Envio a Por Firmar</th>
                    <th>Usuario</th>
                    <th>Envio a Por Entregar</th>
                    <th>Estado Actual</th>
                    <th>Elegido Actualmente</th>
                </tr>
                </tfoot>
              </table>

          </div>
        </div>
      </div>
  </div>




</div>
<!-- /page content -->




    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

var table;

$(document).ready(function() {

    table = $('#datatable-movimiento-usuarios').DataTable( {
      "order": [],
      "bRetrieve": true,
      //"bPaginate": false,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "ajax": {
          "url": "<?php echo site_url('reportes/list_movimiento_usuarios')?>",
          "type": "POST",
          "complete": function(response) {
            var prospectos = 0;
            var verificar = 0;
            var aprobar = 0;
            var firmar = 0;
            var entregar = 0;
            var entregado = 0;
            var cobrado = 0;
            var rechazado = 0;
            var anulado = 0;
            $.each(response.responseJSON.data, function (i, item) {
              if (item[10] == 'Prospecto') {
                prospectos++;
              }
              if (item[10] == 'Por Verificar') {
                verificar++;
              }
              if (item[10] == 'Por Aprobar') {
                aprobar++;
              }  
              if (item[10] == 'Por Firmar') {
                firmar++;
              }  
              if (item[10] == 'Por Entregar') {
                entregar++;
              }  
              if (item[10] == 'Entregado') {
                entregado++;
              }  
              if (item[10] == 'Cobrado') {
                cobrado++;
              }  
              if (item[10] == 'Rechazado') {
                rechazado++;
              }  
              if (item[10] == 'Anulado') {
                anulado++;
              }            
            });
            $('.most').html(prospectos + ' Prospecto, ' + verificar + ' Por Verificar, ' + aprobar + ' Por Aprobar, ' + firmar + ' Por Firmar, ' + entregar + ' Por Entregar, ' + entregado + ' Entregado, ' + cobrado + ' Cobrado, ' + rechazado + ' Rechazado, ' + anulado + ' Anulado. De ' + response.responseJSON.data.length + ' Mostrados.');
          }
      },
      "columnDefs": [{ 
          "targets": [ ],
          "orderable": false,
      },],
    } );

    filtros_select();

});


function reload_table()
{
    table.ajax.reload(null,false);
}

function reload_all()
{
  filtros_select();
  $('.tituloconsulta').html('');
  table.search( '' ).columns().search( '' ).draw();
  table.ajax.url("<?php echo site_url('reportes/list_movimiento_usuarios')?>").load();
}


function filtros_select()
{
    $('#filtrar_rol').empty();
    var trHTML = "<option></option>" +
        "<option value='0'>Todos</option>" +
        "<option value='1'>Agente</option>" +
        "<option value='2'>Verificador</option>" +
        "<option value='3'>Aprobador</option>";
    $('#filtrar_rol').append(trHTML);
    $("#filtrar_rol").select2({
      placeholder: "Filtrar por Rol",
      allowClear: false,
      language: "es",
      theme: "classic",
      dropdownParent: $(".selectrol")
    });
    $.ajax({
        url : "<?php echo site_url('reportes/get_users')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#filtrar_usuarios').empty();
            var trHTML = "<option></option>";
            trHTML += "<option value='0'>Todas</option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombres + " " + item.apellidos + "</option>";
                });
            }
            $('#filtrar_usuarios').append(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    $("#filtrar_usuarios").select2({
      placeholder: "Filtrar por Usuarios",
      allowClear: false,
      language: "es",
      theme: "classic",
      dropdownParent: $(".selectusers")
    });
    $.ajax({
        url : "<?php echo site_url('reportes/get_estados')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#filtrar_estados').empty();
            var trHTML = "<option></option>";
            trHTML += "<option value='0'>Todas</option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.estado + "</option>";
                });
            }
            $('#filtrar_estados').append(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    $("#filtrar_estados").select2({
      placeholder: "Filtrar por Estados",
      allowClear: false,
      language: "es",
      theme: "classic",
      dropdownParent: $(".selectestado")
    });
}

</script>

<!-- bootstrap-daterangepicker -->
<script>

$('.tituloconsulta').html('');
movimiento();
aprobacion();
verificacion();

function movimiento(){
  $(document).ready(function() {
    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
/*      alert(start.toISOString());
      alert(end.toISOString());
      alert(label);*/
      $('#reportrange_mov span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

      table.search( '' ).columns().search( '' ).draw();

      var rol = 0;
      var user = 0;
      var status = 0;

      if ($('#filtrar_rol').val() > 0 || $('#filtrar_rol').val() > 0) {
        rol = $('#filtrar_rol').val();
      }
      if ($('#filtrar_usuarios').val() > 0  || $('#filtrar_usuarios').val() > 0 ) {
        user = $('#filtrar_usuarios').val();
      }
      if ($('#filtrar_estados').val() > 0  || $('#filtrar_estados').val() > 0 ) {
        status = $('#filtrar_estados').val();
      }

      var filtros_url = rol + ',' + user + ',' + status;
      var filtros = encodeURIComponent(filtros_url);

      table.ajax.url("<?php echo site_url('reportes/list_movimiento_usuarios/movimiento/')?>" + filtros + '/' + start.toISOString() + '/' + end.toISOString()).load();

      $('.tituloconsulta').html('Consulta por Fecha de Movimientos:');
      aprobacion();
      verificacion();

    };

    var f = new Date();

    var optionSet1 = {
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      minDate: '01/01/2016',
      maxDate: (f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear(),
      dateLimit: {
        days: 90
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Último 7 Días': [moment().subtract(6, 'days'), moment()],
        'Último 30 Días': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'right',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'MM/DD/YYYY',
      separator: ' to ',
      locale: {
        applyLabel: 'Enviar',
        cancelLabel: 'Cerrar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Personalizado',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
      }
    };

    $('#reportrange_mov span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $('#reportrange_mov').daterangepicker(optionSet1, cb);

    $('#reportrange_mov').on('show.daterangepicker', function() {
      console.log("show event fired");
    });
    $('#reportrange_mov').on('hide.daterangepicker', function() {
      console.log("hide event fired");
    });
    $('#reportrange_mov').on('apply.daterangepicker', function(ev, picker) {
      console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    });
    $('#reportrange_mov').on('cancel.daterangepicker', function(ev, picker) {
      console.log("cancel event fired");
    });

    $('#options1').click(function() {
      $('#reportrange_mov').data('daterangepicker').setOptions(optionSet1, cb);
    });

    $('#options2').click(function() {
      $('#reportrange_mov').data('daterangepicker').setOptions(optionSet2, cb);
    });

    $('#destroy').click(function() {
      $('#reportrange_mov').data('daterangepicker').remove();
    });

  });
}

function aprobacion(){
  $(document).ready(function() {
    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
/*      alert(start.toISOString());
      alert(end.toISOString());
      alert(label);*/
      $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

      table.search( '' ).columns().search( '' ).draw();

      var rol = 0;
      var user = 0;
      var status = 0;

      if ($('#filtrar_rol').val() > 0 || $('#filtrar_rol').val() > 0) {
        rol = $('#filtrar_rol').val();
      }
      if ($('#filtrar_usuarios').val() > 0  || $('#filtrar_usuarios').val() > 0 ) {
        user = $('#filtrar_usuarios').val();
      }
      if ($('#filtrar_estados').val() > 0  || $('#filtrar_estados').val() > 0 ) {
        status = $('#filtrar_estados').val();
      }

      var filtros_url = rol + ',' + user + ',' + status;
      var filtros = encodeURIComponent(filtros_url);

      table.ajax.url("<?php echo site_url('reportes/list_movimiento_usuarios/aprobacion/')?>" + filtros + '/' + start.toISOString() + '/' + end.toISOString()).load();

       $('.tituloconsulta').html('Consulta por Fecha de Registros:');
       movimiento();
       verificacion();

    };

    var f = new Date();

    //alert(f.format('m/d/Y'));
    //(f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear()

    var optionSet1 = {
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      minDate: '01/01/2016',
      maxDate: (f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear(),
      dateLimit: {
        days: 90
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Último 7 Días': [moment().subtract(6, 'days'), moment()],
        'Último 30 Días': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'right',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'MM/DD/YYYY',
      separator: ' to ',
      locale: {
        applyLabel: 'Enviar',
        cancelLabel: 'Cerrar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Personalizado',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
      }
    };

    $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $('#reportrange_right').daterangepicker(optionSet1, cb);

    $('#reportrange_right').on('show.daterangepicker', function() {
      console.log("show event fired");
    });
    $('#reportrange_right').on('hide.daterangepicker', function() {
      console.log("hide event fired");
    });
    $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
      console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    });
    $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
      console.log("cancel event fired");
    });

    $('#options1').click(function() {
      $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
    });

    $('#options2').click(function() {
      $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
    });

    $('#destroy').click(function() {
      $('#reportrange_right').data('daterangepicker').remove();
    });

  });
}

function verificacion(){
  $(document).ready(function() {
    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
/*      alert(start.toISOString());
      alert(end.toISOString());
      alert(label);*/
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

      table.search( '' ).columns().search( '' ).draw();

      var rol = 0;
      var user = 0;
      var status = 0;

      if ($('#filtrar_rol').val() > 0 || $('#filtrar_rol').val() > 0) {
        rol = $('#filtrar_rol').val();
      }
      if ($('#filtrar_usuarios').val() > 0  || $('#filtrar_usuarios').val() > 0 ) {
        user = $('#filtrar_usuarios').val();
      }
      if ($('#filtrar_estados').val() > 0  || $('#filtrar_estados').val() > 0 ) {
        status = $('#filtrar_estados').val();
      }

      var filtros_url = rol + ',' + user + ',' + status;
      var filtros = encodeURIComponent(filtros_url);

      table.ajax.url("<?php echo site_url('reportes/list_movimiento_usuarios/verificacion/')?>" + filtros + '/' + start.toISOString() + '/' + end.toISOString()).load();

       $('.tituloconsulta').html('Enviados a por Verificar:');
       movimiento();
       aprobacion();

    };

    var f = new Date();

    //alert(f.format('m/d/Y'));
    //(f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear()

    var optionSet1 = {
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      minDate: '01/01/2016',
      maxDate: (f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear(),
      dateLimit: {
        days: 90
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Último 7 Días': [moment().subtract(6, 'days'), moment()],
        'Último 30 Días': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'right',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'MM/DD/YYYY',
      separator: ' to ',
      locale: {
        applyLabel: 'Enviar',
        cancelLabel: 'Cerrar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Personalizado',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
      }
    };

    $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $('#reportrange').daterangepicker(optionSet1, cb);

    $('#reportrange').on('show.daterangepicker', function() {
      console.log("show event fired");
    });
    $('#reportrange').on('hide.daterangepicker', function() {
      console.log("hide event fired");
    });
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    });
    $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
      console.log("cancel event fired");
    });

    $('#options1').click(function() {
      $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
    });

    $('#options2').click(function() {
      $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
    });

    $('#destroy').click(function() {
      $('#reportrange').data('daterangepicker').remove();
    });

  });
}


</script>

<!-- End bootstrap-daterangepicker -->