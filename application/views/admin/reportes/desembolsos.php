<!-- page content -->
<div class="right_col" role="main">


    <div class="">
      <div class="row top_tiles">
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <!-- <div class="icon"><i class="fa fa-money"></i></div> -->
            <div class="count desem"></div>
            <h3>Desembolsos</h3>
            <p class="mostrados"></p>
          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <!-- <div class="icon"><i class="fa fa-comments-o"></i></div> -->
            <div class="count obli"></div>
            <h3>Saldo</h3>
            <p class="mostrados"></p>
          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <!-- <div class="icon"><i class="fa fa-user"></i></div> -->
            <div class="count oblit"></div>
            <h3>Obligación</h3>
            <p class="mostrados"></p>

          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-building-o"></i></div>
            <div class="count cante"></div>
            <h3>Empresas</h3>
            <p class="mostrados"></p>
          </div>
        </div>
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="tile-stats">
            <div>
              <p class="prod"></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Reportes de Desembolsos <small>Listado de Reportes</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
              <span>Rango de fechas</span> <b class="caret"></b>
            </div>

            <div class="clearfix"></div><br>

            <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Refrescar</button>

	           <table id="datatable-desembolsos" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	              <thead>
	                <tr>
                    <th>Entregado</th>
	                  <th>Cliente</th>
	                  <th>Aprobado</th>
                    <th>Plazo</th>
	                  <th>Saldo</th>
                    <th>Obligación</th>
	                  <th>Empresa</th>
	                  <th>Primer descuento</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	              <tfoot>
	              <tr>
                    <th>Entregado</th>
	                  <th>Cliente</th>
	                  <th>Aprobado</th>
                    <th>Plazo</th>
                    <th>Saldo</th>
                    <th>Obligación</th>
	                  <th>Empresa</th>
	                  <th>Primer descuento</th>
	              </tr>
	              </tfoot>
	            </table>

          </div>
        </div>
      </div>
  </div>


  <div class="">
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <!-- <div class="icon"><i class="fa fa-money"></i></div> -->
          <div class="count">$<?= $sumdesembolso ?></div>
          <h3>Desembolsos</h3>
          <p>Total préstamos desembolsados</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <!-- <div class="icon"><i class="fa fa-comments-o"></i></div> -->
          <div class="count">$<?= $obligacionfecha ?></div>
          <h3>Saldo</h3>
          <p>Total deuda a la fecha</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <!-- <div class="icon"><i class="fa fa-user"></i></div> -->
          <div class="count">$<?= $totalobligacion ?></div>
          <h3>Obligación</h3>
          <p>Total de obligación</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-building-o"></i></div>
          <div class="count"><?= $cantempresas ?></div>
          <h3>Empresas</h3>
          <p>Total de empresas</p>
        </div>
      </div>
    </div>
  </div>


</div>
<!-- /page content -->




    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

var fd;
var f_inicio;
var f_fin;
var table;

$(document).ready(function() {

    fd = new Date();
    f_inicio = fd.format('d-m-Y');
    f_fin = (fd.getDate()+1) + "-" + (fd.getMonth() +1) + "-" + fd.getFullYear();

    table = $('#datatable-desembolsos').DataTable( {
      "order": [],
      "bRetrieve": true,
      "bPaginate": false,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "ajax": {
          "url": "<?php echo site_url('reportes/list_desembolsos/')?>" + f_inicio + '/' + f_fin,
          "type": "POST",
          "complete": function(response) {

/*            alert(response);
            alert(response.readyState);
            alert(response.responseText);              
            alert(response.responseJSON);              
            alert(response.responseJSON.data);        
            alert(response.responseJSON.data.length);*/

            //alert(response.responseJSON.data); 

            var desembolso = 0;
            var obligacion = 0;
            var obligaciont = 0;
            var empresas = [];
            var productos = [];
            var ron = 0;
            $.each(response.responseJSON.data, function (i, item) {
                //alert(i + ': ' + item[1]);
                //alert(item[8]);
/*                desembolso += parseFloat(item[2]);
                obligacion += parseFloat(item[4]);
                obligaciont += parseFloat(item[5]);*/
                desembolso += parseFloat(item[8][0]);
                obligacion += parseFloat(item[8][1]);
                obligaciont += parseFloat(item[8][2]);
                empresas.push(item[6]);
                productos.push({'a':item[3],'b':ron++});
            });

            Array.prototype.unique=function(a){
              return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
            });

            empresas = empresas.unique();

            //alert(desembolso);

            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
              minimumFractionDigits: 2, /* this might not be necessary */
            });

            $('.desem').html(formatter.format(desembolso));
            $('.obli').html(formatter.format(obligacion));
            $('.oblit').html(formatter.format(obligaciont));

            $('.mostrados').html('De ' + response.responseJSON.data.length + ' Mostrados');

            $('.cante').html(empresas.length);

            var matriz = {};

            productos.forEach(function(registro) {
              var num = registro["a"];
              matriz[num] = matriz[num] ? (matriz[num] + 1) : 1;
            });

            // luego puedes usar la primera matriz, para crear el arreglo con tu formato necesitado.
            matriz = Object.keys(matriz).map(function(num) {
               return { plazo: num, cant: matriz[num] };
            });

            //var b = []
            var html = '';
            for (var i = 0; i < matriz.length; i++) {
              //b.push({'plazo' : matriz[i].plazo, 'cant' : matriz[i].cant});
              var mes;
              if (matriz[i].plazo > 1) {
                mes = ' Meses';
              } else {
                mes = ' Mes';
              }
              var reg;
              if (matriz[i].cant > 1) {
                reg = ' registros';
              } else {
                reg = ' registro';
              }
              html += 'De ' + matriz[i].plazo + mes + ' hay ' + matriz[i].cant + reg + '. ';
            }

            $('.prod').html(html);


          }
      },
      "columnDefs": [{ 
          "targets": [ -1 ],
          "orderable": false,
      },],
    } );

    //consulta(f_inicio, f_fin);
});


function reload_table()
{
    table.ajax.reload(null,false);
}

function consulta(f_inicio, f_fin)
{
    table.ajax.url("<?php echo site_url('reportes/list_desembolsos/')?>" + f_inicio + '/' + f_fin).load();
}

</script>

<!-- bootstrap-daterangepicker -->
<script>

  $(document).ready(function() {

    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
/*      alert(start.toISOString());
      alert(end.toISOString());
      alert(label);*/
      //$('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#reportrange_right span').html('Desde: ' + start.format('DD/MM/YYYY') + ' Hasta: ' + end.format('DD/MM/YYYY'));

       //table.ajax.url("<?php echo site_url('reportes/list_desembolsos/')?>" + start.toISOString() + '/' + end.toISOString()).load();

       consulta(start.toISOString(), end.toISOString());

    };

    var f = new Date();

    //alert(f.format('m/d/Y'));
    //(f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear()

    var optionSet1 = {
      //startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      minDate: '01/01/2016',
      maxDate: (f.getMonth() +1) + "/" + (f.getDate()+1) + "/" + f.getFullYear(),
/*      dateLimit: {
        days: 90
      },*/
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Último 7 Días': [moment().subtract(6, 'days'), moment()],
        'Último 30 Días': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'right',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'MM/DD/YYYY',
      separator: ' to ',
      locale: {
        applyLabel: 'Enviar',
        cancelLabel: 'Cerrar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Personalizado',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
      }
    };

    //$('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    $('#reportrange_right span').html('Desde: ' + moment().format('DD/MM/YYYY') + ' Hasta: ' + moment().format('DD/MM/YYYY'));

    $('#reportrange_right').daterangepicker(optionSet1, cb);

    $('#reportrange_right').on('show.daterangepicker', function() {
      console.log("show event fired");
    });
    $('#reportrange_right').on('hide.daterangepicker', function() {
      console.log("hide event fired");
    });
    $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
      console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    });
    $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
      console.log("cancel event fired");
    });

    $('#options1').click(function() {
      $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
    });

    $('#options2').click(function() {
      $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
    });

    $('#destroy').click(function() {
      $('#reportrange_right').data('daterangepicker').remove();
    });

    //setTimeout(consulta(f_inicio, f_fin),5000);

  });
</script>

<!-- End bootstrap-daterangepicker -->