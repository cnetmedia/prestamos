<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Reportes de Clientes <small>Listado de Reportes</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="col-md-4 col-sm-4 col-xs-12 selectestado">
              <select class='form-control' name="filtrar_clientes" id="filtrar_clientes" 
                onchange="clickEstado(this.value)">
                <option value='0'>Todos</option>
                <option value='aldia'>Al dia</option>
                <option value='morosos'>Morosos</option>
                <option value='desempleado'>Desempleado</option>
                <option value='completado'>Completado</option>
              </select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 boton-excel pull-right">
              <!-- 
              <a class="btn btn-success btn-sm pull-right" 
                href="<?php echo site_url('reportes/get_clientes_by_estado_to_excel')?>">
                <i class="fa fa-file-excel-o"></i> Descargar excel
              </a> -->
              <button class="btn btn-success btn-sm pull-right" 
                id="btn-excel" data-estado="0"
                onclick="clickBoton()">
                <i class="fa fa-file-excel-o"></i> Descargar excel
              </button>  
              </a>
            </div>
            <div class="clearfix"></div>
            <div class="records"></div>
            <div class="clientes"></div>
          </div>
        </div>
      </div>
  </div>

</div>
<!-- /page content -->
<!-- jQuery -->
<script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>
<script type="text/javascript">

$(document).ready(function() {
  $("#filtrar_clientes").select2({
    placeholder: "Filtrar por estado",
    allowClear: false,
    language: "es",
    theme: "classic",
    dropdownParent: $(".selectestado")
  });
  
  $("#btn-excel").on("click", function() {
    
  });
});

function clickBoton(){
  var value = $('#btn-excel').attr('data-estado'); 
  console.log(value);
  var href = "<?php echo site_url('reportes/get_clientes_by_estado_to_excel')?>/?estado="+value;
  var a = $('<a />');
  a.attr('href',href);
  a.attr('download',true);
  a.attr('id',"descarga-btn-excel");
  $('.boton-excel').append(a);
  //$('#descarga-btn-excel').click();
  document.getElementById('descarga-btn-excel').click();
}

function clickEstado(estado){
  $('#btn-excel').attr('data-estado',estado);
  console.log(estado)
  consulta(estado);
}

function consulta(estado){
  $.ajax({
    url : "<?php echo site_url('reportes/get_clientes_by_estado')?>",
    type: "POST",
    data: {
      'estado' : estado
    },
    dataType: "JSON",
    success: function(data)
    {
      $('.clientes').empty();
      if (data.data.length > 0) {
        var html = '<div class="table-responsive">' +
            '<table class="table table-striped table-bordered jambo_table" id="datatable-recolectados" cellspacing="0" width="100%">' +
              '<caption><h2>Movimiento de Firma</h2></caption>' +
              '<thead>' +
                '<tr>' +
                '<th>Cliente</th>' +
                '<th>Documento de identidad</th>' +
                '<th>Celular</th>' +
                '<th>Telefono</th>' +
                '<th>Correo electronico</th>' +
                '<th>Estado Actual</th>' +
                '</tr>' +
              '</thead>';
        $.each(data.data, function(i,item){
          //alert(i + ': ' + item);
          var etiqueta = '<label class="label label-info">Al dia</label>';
          if(item.estado == 'morosos'){
            etiqueta = '<label class="label label-warning">Moroso</label>';
          }

          if(item.estado == 'completado'){
            etiqueta = '<label class="label label-success">Completado</label>';
          }

          if(item.estado == 'desempleado'){
            etiqueta = '<label class="label label-danger">Desempleado</label>';
          }

          html += '<tbody>'+
              '<td>'+item.cliente+'</td>' +
              '<td>'+item.cedula+'</td>' +
              '<td>'+item.celular+'</td>' +
              '<td>'+item.telefono+'</td>' +
              '<td>'+item.correo+'</td>' +
              '<td>'+etiqueta+'</td>' +
            '</tbody>';
        });
        html += '</table>' +
            '</div>';
        $('.clientes').html(html);
      }

      $('.records').empty();
      if (data.records == false) {
        $('.records').html('<p>Lo siento no hay registros con el estado seleccionado.</p>');
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error get data from ajax');
    }
  });
}
consulta('');

</script>


<!-- End bootstrap-daterangepicker -->