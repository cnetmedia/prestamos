<!-- page content -->
<div class="right_col" role="main">


    <div class="">
      <div class="row top_tiles">
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <!-- <div class="icon"><i class="fa fa-money"></i></div> -->
            <div class="count total"></div>
            <h3>Total</h3>
            <p class="mostrados"></p>
          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-building-o"></i></div>
            <div class="count cante"></div>
            <h3>Empresas</h3>
            <p class="mostrados"></p>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Reportes de Cobros Recolectados <small>Listado de Reportes</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <div class="row col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
              <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span>Fechas por registros</span> <b class="caret"></b>
              </div>
            </div>

            <div class="clearfix"></div>

            <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Refrescar</button>
            <button class="btn btn-default btn-xs" onclick="reload_all()"><i class="fa fa-undo"></i> Mostrar Todos</button>

	           <table id="datatable-cobros-recolectados" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	              <thead>
	                <tr>
                    <th>Cliente</th>
	                  <th>Empresa</th>
	                  <th>Último pago</th>
                    <th>Total pagado</th>
	                </tr>
	              </thead>
	              <tbody>
	              </tbody>
	              <tfoot>
	              <tr>
                    <th>Cliente</th>
                    <th>Empresa</th>
                    <th>Último pago</th>
                    <th>Total pagado</th>
	              </tr>
	              </tfoot>
	            </table>

          </div>
        </div>
      </div>
  </div>


  <div class="">
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <!-- <div class="icon"><i class="fa fa-money"></i></div> -->
          <div class="count">$<?= $totalpago ?></div>
          <h3>Total</h3>
          <p>Total de pagos</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-user"></i></div>
          <div class="count"><?= $totalregistros ?></div>
          <h3>Clientes</h3>
          <p>Total de clientes</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-building-o"></i></div>
          <div class="count"><?= $cantempresas ?></div>
          <h3>Empresas</h3>
          <p>Total de empresas</p>
        </div>
      </div>
    </div>
  </div>





</div>
<!-- /page content -->




    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

var table;

$(document).ready(function() {

    table = $('#datatable-cobros-recolectados').DataTable( {
      "order": [],
      "bRetrieve": true,
      "bPaginate": false,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "ajax": {
          "url": "<?php echo site_url('reportes/list_cobros_recolectados')?>",
          "type": "POST",
          "complete": function(response) {

            var total = 0;
            var empresas = [];
            $.each(response.responseJSON.data, function (i, item) {
                total += parseFloat(item[3]);
                empresas.push(item[1]);
            });

            Array.prototype.unique=function(a){
              return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
            });

            empresas = empresas.unique();

            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'USD',
              minimumFractionDigits: 2, /* this might not be necessary */
            });

            $('.total').html(formatter.format(total));
            $('.cante').html(empresas.length);
            $('.mostrados').html('De ' + response.responseJSON.data.length + ' Mostrados');
          }
      },
      "columnDefs": [{ 
          "targets": [ ],
          "orderable": false,
      },],
    } );


});


function reload_table()
{
    table.ajax.reload(null,false);
}

function reload_all()
{
  registros();
  table.search( '' ).columns().search( '' ).draw();
  table.ajax.url("<?php echo site_url('reportes/list_cobros_recolectados')?>").load();
}


registros();

function registros(){
  $(document).ready(function() {
    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
/*      alert(start.toISOString());
      alert(end.toISOString());
      alert(label);*/
      $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

       table.ajax.url("<?php echo site_url('reportes/list_cobros_recolectados/')?>" + start.toISOString() + '/' + end.toISOString()).load();

    };

    var f = new Date();

    //alert(f.format('m/d/Y'));
    //(f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear()

    var optionSet1 = {
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      minDate: '01/01/2016',
      maxDate: (f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear(),
      dateLimit: {
        days: 90
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Último 7 Días': [moment().subtract(6, 'days'), moment()],
        'Último 30 Días': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'right',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'MM/DD/YYYY',
      separator: ' to ',
      locale: {
        applyLabel: 'Enviar',
        cancelLabel: 'Cerrar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Personalizado',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
      }
    };

    $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

    $('#reportrange_right').daterangepicker(optionSet1, cb);

    $('#reportrange_right').on('show.daterangepicker', function() {
      console.log("show event fired");
    });
    $('#reportrange_right').on('hide.daterangepicker', function() {
      console.log("hide event fired");
    });
    $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
      console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    });
    $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
      console.log("cancel event fired");
    });

    $('#options1').click(function() {
      $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
    });

    $('#options2').click(function() {
      $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
    });

    $('#destroy').click(function() {
      $('#reportrange_right').data('daterangepicker').remove();
    });

  });
}


</script>

<!-- End bootstrap-daterangepicker -->