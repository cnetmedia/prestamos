<!-- page content -->
<div class="right_col" role="main">



    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Reportes de Movimientos de Usuarios <small>Listado de Reportes</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <div class="col-md-4 col-sm-4 col-xs-12 selectusers">
              <select class='form-control' name="filtrar_usuarios" id="filtrar_usuarios" onchange="clickUser(this.value)"></select>
            </div>

            <div class="row col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
              <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span>Consultar Fecha</span> <b class="caret"></b>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="array_agente"></div>
            <div class="array_verificador"></div>
            <div class="array_aprobador"></div>
            <div class="array_firma"></div>
            <div class="records"></div>


          </div>
        </div>
      </div>
  </div>




</div>
<!-- /page content -->




    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

  
var fd;
var f_inicio;
var f_fin;

$(document).ready(function() {

  fd = new Date();
  f_inicio = fd.format('d/m/Y');
  f_fin = (fd.getDate()+1) + "/" + (fd.getMonth() +1) + "/" + fd.getFullYear();

  var cb = function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);


    f_inicio = start.toISOString();
    f_fin = end.toISOString();

    //$('#reportrange_right span').html('Desde: ' + start.format('MMMM D, YYYY') + ' Hasta: ' + end.format('MMMM D, YYYY'));
    $('#reportrange_right span').html('Desde: ' + start.format('DD/MM/YYYY') + ' Hasta: ' + end.format('DD/MM/YYYY'));

    var user = 0;
    if ($('#filtrar_usuarios').val() > 0  || $('#filtrar_usuarios').val() > 0 ) {
      user = $('#filtrar_usuarios').val();
    }

    consulta(start.toISOString(),end.toISOString(),user);

  };

  var f = new Date();

  //alert(f.format('m/d/Y'));
  //(f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear()

  var optionSet1 = {
    //startDate: moment().subtract(29, 'days'),
    startDate: moment(),
    endDate: moment(),
    minDate: '01/01/2016',
    maxDate: (f.getMonth() +1) + "/" + (f.getDate()+1) + "/" + f.getFullYear(),
    dateLimit: {
      days: 90
    },
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: true,
    ranges: {
      'Hoy': [moment(), moment()],
      'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Último 7 Días': [moment().subtract(6, 'days'), moment()],
      'Último 30 Días': [moment().subtract(29, 'days'), moment()],
      'Este Mes': [moment().startOf('month'), moment().endOf('month')],
      'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    buttonClasses: ['btn btn-default'],
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small',
    format: 'MM/DD/YYYY',
    separator: ' to ',
    locale: {
      applyLabel: 'Enviar',
      cancelLabel: 'Cerrar',
      fromLabel: 'Desde',
      toLabel: 'Hasta',
      customRangeLabel: 'Personalizado',
      daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      firstDay: 1
    }
  };

  //$('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
  $('#reportrange_right span').html('Desde: ' + moment().format('DD/MM/YYYY') + ' Hasta: ' + moment().format('DD/MM/YYYY'));

  $('#reportrange_right').daterangepicker(optionSet1, cb);

  $('#reportrange_right').on('show.daterangepicker', function() {
    console.log("show event fired");
    //alert("show event fired");
  });
  $('#reportrange_right').on('hide.daterangepicker', function() {
    console.log("hide event fired");
    //alert("hide event fired");
  });
  $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
    console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    //alert("apply event fired");
    //alert(picker.startDate.format('MMMM D, YYYY'));
  });
  $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
    console.log("cancel event fired");
    //alert("cancel event fired");
  });

  $('#options1').click(function() {
    $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
    //alert("ange_right");
  });

  $('#options2').click(function() {
    $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
  });

  $('#destroy').click(function() {
    $('#reportrange_right').data('daterangepicker').remove();
    //alert('destroy');
  });

  filtrarUsuarios();
  consulta(f_inicio, f_fin, 0);

});

function filtrarUsuarios(){
  $.ajax({
    url : "<?php echo site_url('reportes/get_users')?>",
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      $('#filtrar_usuarios').empty();
      var trHTML = "<option></option>";
      trHTML += "<option value='0'>Todos</option>";
      if (data.length > 0) {
        $.each(data, function (i, item) {
          trHTML += "<option value='" + item.id + "'>" + item.nombres + " " + item.apellidos + "</option>";
        });
      }
      $('#filtrar_usuarios').append(trHTML);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error get data from ajax');
    }
  });
  $("#filtrar_usuarios").select2({
    placeholder: "Filtrar Usuario",
    allowClear: false,
    language: "es",
    theme: "classic",
    dropdownParent: $(".selectusers")
  });
}

function clickUser(id_user){
  consulta(f_inicio, f_fin, id_user);
}

function consulta(fecha_i, fecha_f, id_user){
  $.ajax({
    url : "<?php echo site_url('reportes/get_movimiento_by_users')?>",
    type: "POST",
    data: {
      'fecha_i' : fecha_i,
      'fecha_f' : fecha_f,
      'id_user' : id_user
    },
    dataType: "JSON",
    success: function(data)
    {
      //alert(data);
      $('.array_agente').empty();
      if (data.data.array_agente.length > 0) {
        var html = '<div class="table-responsive">' +
            '<table class="table table-striped table-bordered jambo_table" id="datatable-recolectados" cellspacing="0" width="100%">' +
              '<caption><h2>Movimiento de Agentes</h2></caption>' +
              '<thead>' +
                '<tr>' +
                '<th>Agente</th>' +
                '<th>Envio a Verificación</th>' +
                '<th>Cliente</th>' +
                '<th>Estado Actual</th>' +
                '<th>Disposición</th>' +
                '</tr>' +
              '</thead>';
        $.each(data.data.array_agente, function(i,item){
          //alert(i + ': ' + item);
          html += '<tbody>'+
              '<td>'+item.agente+'</td>' +
              '<td>'+item.fecha_agente+'</td>' +
              '<td>'+item.clientes_id+'</td>' +
              '<td>'+item.estados_id+'</td>' +
              '<td>'+item.disposiciones_id+'</td>' +
            '</tbody>';
        });
        html += '</table>' +
            '</div>';
        $('.array_agente').html(html);
      }

      $('.array_verificador').empty();
      if (data.data.array_verificador.length > 0) {
        var html = '<div class="table-responsive">' +
            '<table class="table table-striped table-bordered jambo_table" id="datatable-recolectados" cellspacing="0" width="100%">' +
              '<caption><h2>Movimiento de Verificador</h2></caption>' +
              '<thead>' +
                '<tr>' +
                '<th>Verificador</th>' +
                '<th>Envio a Aprobación</th>' +
                '<th>Cliente</th>' +
                '<th>Estado Actual</th>' +
                '<th>Disposición</th>' +
                '</tr>' +
              '</thead>';
        $.each(data.data.array_verificador, function(i,item){
          //alert(i + ': ' + item);
          html += '<tbody>'+
              '<td>'+item.verificador+'</td>' +
              '<td>'+item.fecha_verificacion+'</td>' +
              '<td>'+item.clientes_id+'</td>' +
              '<td>'+item.estados_id+'</td>' +
              '<td>'+item.disposiciones_id+'</td>' +
            '</tbody>';
        });
        html += '</table>' +
            '</div>';
        $('.array_verificador').html(html);
      }

      $('.array_aprobador').empty();
      if (data.data.array_aprobador.length > 0) {
        var html = '<div class="table-responsive">' +
            '<table class="table table-striped table-bordered jambo_table" id="datatable-recolectados" cellspacing="0" width="100%">' +
              '<caption><h2>Movimiento de Aprobador</h2></caption>' +
              '<thead>' +
                '<tr>' +
                '<th>Aprobador</th>' +
                '<th>Envio a Por Firmar</th>' +
                '<th>Cliente</th>' +
                '<th>Estado Actual</th>' +
                '<th>Disposición</th>' +
                '</tr>' +
              '</thead>';
        $.each(data.data.array_aprobador, function(i,item){
          //alert(i + ': ' + item);
          html += '<tbody>'+
              '<td>'+item.aprobador+'</td>' +
              '<td>'+item.fecha_aprobado+'</td>' +
              '<td>'+item.clientes_id+'</td>' +
              '<td>'+item.estados_id+'</td>' +
              '<td>'+item.disposiciones_id+'</td>' +
            '</tbody>';
        });
        html += '</table>' +
            '</div>';
        $('.array_aprobador').html(html);
      }

      $('.array_firma').empty();
      if (data.data.array_firma.length > 0) {
        var html = '<div class="table-responsive">' +
            '<table class="table table-striped table-bordered jambo_table" id="datatable-recolectados" cellspacing="0" width="100%">' +
              '<caption><h2>Movimiento de Firma</h2></caption>' +
              '<thead>' +
                '<tr>' +
                '<th>Verificador de firma</th>' +
                '<th>Envio a Por Entregar</th>' +
                '<th>Cliente</th>' +
                '<th>Estado Actual</th>' +
                '<th>Disposición</th>' +
                '</tr>' +
              '</thead>';
        $.each(data.data.array_firma, function(i,item){
          //alert(i + ': ' + item);
          html += '<tbody>'+
              '<td>'+item.firma+'</td>' +
              '<td>'+item.fecha_firma+'</td>' +
              '<td>'+item.clientes_id+'</td>' +
              '<td>'+item.estados_id+'</td>' +
              '<td>'+item.disposiciones_id+'</td>' +
            '</tbody>';
        });
        html += '</table>' +
            '</div>';
        $('.array_firma').html(html);
      }

      $('.records').empty();
      if (data.records == false) {
        $('.records').html('<p>Lo siento no hay registros sobre la fecha seleccionada.</p>');
      }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error get data from ajax');
    }
  });
}

</script>


<!-- End bootstrap-daterangepicker -->