<!-- page content -->
<div class="right_col" role="main">


    <div class="recolect">
      <div class="row top_tiles">
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <!-- <div class="icon"><i class="fa fa-money"></i></div> -->
            <div class="count total"></div>
            <h3>Total Recolectado</h3>
            <p class="mostrados"></p>
          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-building-o"></i></div>
            <div class="count cante"></div>
            <h3>Empresas</h3>
            <p class="mostrados"></p>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Reportes de Cobros Recolectados <small>Listado de Reportes</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <div class="row col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
              <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span>Fechas por registros</span> <b class="caret"></b>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="view_cobrados"></div>

          </div>
        </div>
      </div>
  </div>


  <div class="">
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <!-- <div class="icon"><i class="fa fa-money"></i></div> -->
          <div class="count">$<?= $totalpago ?></div>
          <h3>Total</h3>
          <p>Total de pagos</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-user"></i></div>
          <div class="count"><?= $totalregistros ?></div>
          <h3>Clientes</h3>
          <p>Total de clientes</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-building-o"></i></div>
          <div class="count"><?= $cantempresas ?></div>
          <h3>Empresas</h3>
          <p>Total de empresas</p>
        </div>
      </div>
    </div>
  </div>



    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_pagos" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Pagos</h3>
                </div>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="modal-nombre text-info"><!-- info --></h2>
                    </div>
                  </div>

                  <div class="table-responsive company_user">
                    <table class="table table-striped table-bordered jambo_table bulk_action" width="100%">
                      <thead>
                        <tr>
                          <!-- <th style="text-align: center"><input type="checkbox" id="totalpagos" onclick="totalpagos()"></th> -->
                          <th>Deuda</th>
                          <th>Fecha tope</th>
                          <th>Abono</th>
                          <th>Principal</th>
                          <th>Interés pendiente</th>
                          <th>Interés</th>
                          <th>Tasa interés</th>
                          <th>Cargo pendiente</th>
                          <th>Cargo administrativo</th>
                          <th>Cargo mora</th>
                          <th>Total</th>
                          <th>Fecha de pago</th>
                          <th>Forma de pago</th>
                          <th>Monto</th>
                          <th>Archivo</th>
                          <th>Nota</th>
                          <th>Registrado por</th>
                        </tr>
                      </thead>
                      <tbody class="filaspagos">
                      </tbody>
                    </table>
                  </div>

                </div>
                <div class="modal-footer">
                  <button class="btn btn-success btn-xs" 
                    id="btn-excel" data-id=""
                    onclick="clickBoton()">
                    <i class="fa fa-file-excel-o"></i> Descargar excel
                  </button> 
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


</div>
<!-- /page content -->



    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

var fd;
var f_inicio;
var f_fin;

$(document).ready(function() {

  fd = new Date();
  f_inicio = fd.format('d/m/Y');
  f_fin = (fd.getDate()+1) + "/" + (fd.getMonth() +1) + "/" + fd.getFullYear();

  var cb = function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
/*      alert(start.toISOString());
    alert(end.toISOString());
    alert(label);*/
    //$('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    $('#reportrange_right span').html('Desde: ' + start.format('DD/MM/YYYY') + ' Hasta: ' + end.format('DD/MM/YYYY'));

    consulta(start.toISOString(), end.toISOString());

  };

  var f = new Date();

  //alert(f.format('m/d/Y'));
  //(f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear()

  var optionSet1 = {
    //startDate: moment().subtract(29, 'days'),
    endDate: moment(),
    minDate: '01/01/2016',
    maxDate: (f.getMonth() +1) + "/" + (f.getDate()+1) + "/" + f.getFullYear(),
    dateLimit: {
      days: 90
    },
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: true,
    ranges: {
      'Hoy': [moment(), moment()],
      'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Último 7 Días': [moment().subtract(6, 'days'), moment()],
      'Último 30 Días': [moment().subtract(29, 'days'), moment()],
      'Este Mes': [moment().startOf('month'), moment().endOf('month')],
      'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    buttonClasses: ['btn btn-default'],
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small',
    format: 'MM/DD/YYYY',
    separator: ' to ',
    locale: {
      applyLabel: 'Enviar',
      cancelLabel: 'Cerrar',
      fromLabel: 'Desde',
      toLabel: 'Hasta',
      customRangeLabel: 'Personalizado',
      daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      firstDay: 1
    }
  };

  //$('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
  $('#reportrange_right span').html('Desde: ' + moment().format('DD/MM/YYYY') + ' Hasta: ' + moment().format('DD/MM/YYYY'));

  $('#reportrange_right').daterangepicker(optionSet1, cb);

  $('#reportrange_right').on('show.daterangepicker', function() {
    console.log("show event fired");
  });
  $('#reportrange_right').on('hide.daterangepicker', function() {
    console.log("hide event fired");
  });
  $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
    console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
  });
  $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
    console.log("cancel event fired");
  });

  $('#options1').click(function() {
    $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
  });

  $('#options2').click(function() {
    $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
  });

  $('#destroy').click(function() {
    $('#reportrange_right').data('daterangepicker').remove();
  });

  $('.recolect').hide();
  consulta(f_inicio, f_fin);

});

function consulta(fecha_i,fecha_f){
  $.ajax({
      url : "<?php echo site_url('reportes/consulta_cobros_recolectados')?>",
      type: "POST",
      data: {
        'fecha_i' : fecha_i,
        'fecha_f' : fecha_f
      },
      dataType: "JSON",
      success: function(data)
      {

        if (data) {
          $('.recolect').show();

          var html = '';
          var cant = 0;
          $.each(data, function(i,item){
            cant++;
            if (i != 'TotalesFinales') {
              html += '<div class="table-responsive">' +
                  '<table class="table table-striped table-bordered jambo_table" id="datatable-recolectados" cellspacing="0" width="100%">' +
                    '<caption><h2>'+i+'</h2></caption>' +
                    '<thead>' +
                      '<tr>' +
                      '<th>Cliente</th>' +
                      '<th>Aprobado</th>' +
                      '<th>Letra</th>' +
                      '<th>Desembolsado</th>' +
                      '<th>Primer Descuento</th>' +
                      '<th>Fecha Pago</th>' +
                      '<th>Monto</th>' +
                      '<th>Estado</th>' +
                      '</tr>' +
                    '</thead>';                    
                    
            } else {
              $('.total').html('$' + item.monto);
            }

            $.each(item, function(j,jtem){ 

              if (j >= 0) {
                html += '<tbody>'+
                    '<td><a href="javascript:void(0)" onclick="remove_payment('+jtem.aprobacion_id+')">'+jtem.cliente+' </a></td>' +
                    '<td>$'+jtem.aprobado+'</td>' +
                    '<td>$'+jtem.letra+'</td>' +
                    '<td>'+jtem.desembolsado+'</td>' +
                    '<td>'+jtem.primer_descuento+'</td>' +
                    '<td>'+jtem.fecha_pago+'</td>' +
                    '<td>$'+jtem.monto+'</td>' +
                    '<td>'+jtem.estado+'</td>' +
                  '</tbody>';
              }

              if (j == 'acumulados') {
                html += '<tbody>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td>$'+jtem.monto+'</td>' +
                    '<td></td>' +
                  '</tbody>';             
              }

            });

            html += '</table>' +
                '</div>';

          });

          if (cant > 0) {
            cant = cant - 1;
          }
          $('.cante').html(cant);
          
          $('.view_cobrados').html(html);

        } else {
          $('.recolect').hide();
          $('.view_cobrados').html('Lo siento, no hay registros.');
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}


function remove_payment(id){
  $('#modal_form_pagos').modal('show');
  $('#modal_form_pagos #btn-excel').attr('data-id',id);
  table_payment(id);
}

function clickBoton(){
  var id = $('#btn-excel').attr('data-id'); 
  console.log(id);
  var href = "<?php echo site_url('cobros/remove_payment_to_excel')?>/?id="+id;
  var a = $('<a />');
  a.attr('href',href);
  a.attr('download',true);
  a.attr('id',"descarga-btn-excel");
  $('#btn-excel').append(a);
  //$('#descarga-btn-excel').click();
  document.getElementById('descarga-btn-excel').click();
  $("#descarga-btn-excel").remove();
}

//var numpagos;
var aprobaId;
function table_payment(id){
  aprobaId = id;
  $.ajax({
      url : "<?php echo site_url('cobros/remove_payment/')?>" + aprobaId,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        
        $('.modal-nombre').text(data.nombre_cliente);

        if (data.cobros) {
          $('.filaspagos').empty();
          var trHTML = '';
          var abono = 0;
          var monto = 0;
          $.each(data.cobros, function (i, item) {
              trHTML += '<tr>' +
/*                  '<td style="text-align: center"><input type="checkbox" id="pago'+i+'" value="'+item.id+'" disabled></td>' +*/
                  '<td>$'+item.deuda+'</td>' +
                  '<td>'+item.fecha_correspondiente+'</td>' +
                  '<td>$'+item.abono+'</td>' +
                  '<td>$'+item.principal+'</td>' +
                  '<td>$'+item.interes_restante+'</td>' +
                  '<td>$'+item.interes+'</td>' +
                  '<td>%'+item.tasa_interes+'</td>' +
                  '<td>$'+item.cargo_restante+'</td>' +
                  '<td>$'+item.cargo_administrativo+'</td>' +
                  '<td>$'+item.interes_mora+'</td>' +
                  '<td>$'+item.total+'</td>' +
                  '<td>'+item.fecha_pago+'</td>' +
                  '<td>'+item.forma_pago+'</td>' +
                  '<td>$'+item.monto+'</td>' +
                  '<td>'+item.archivo+'</td>' +
                  '<td>'+item.nota+'</td>' +
                  '<td>'+item.usuarios_id+'</td>' +
                '</tr>';
                abono += parseFloat(item.abono);
                monto += parseFloat(item.monto);
          });
          trHTML += '<tr>' +
                  /*'<td></td>' +*/
                  '<td></td>' +
                  '<td></td>' +
                  '<td>$'+abono.toFixed(2)+'</td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td>$'+monto.toFixed(2)+'</td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                '</tr>';
          $('.filaspagos').append(trHTML);
/*          numpagos = data.cobros.length;

          var num = numpagos - 1;
          $('#pago'+num).removeAttr('disabled');*/

        } else {
          $('.filaspagos').empty();
          //var trHTML = '<tr><td colspan="15" style="text-align: center">Lo siento, no hay registros</td></tr>';
          var trHTML = '<tr><td colspan="14" style="text-align: center">Lo siento, no hay registros</td></tr>';
          $('.filaspagos').append(trHTML);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

</script>

<!-- End bootstrap-daterangepicker -->