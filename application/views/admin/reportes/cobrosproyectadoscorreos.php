<!-- page content -->
<div class="right_col" role="main">


    <div class="proyect">
      <div class="row top_tiles">
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <!-- <div class="icon"><i class="fa fa-money"></i></div> -->
            <div class="count total"></div>
            <h3>Total Proyectados</h3>
            <p class="mostrados"></p>
          </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-building-o"></i></div>
            <div class="count cante"></div>
            <h3>Empresas</h3>
            <p class="mostrados"></p>
          </div>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Reportes de Cobros Proyectados <small>Listado de Reportes</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            <div class="row col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
              <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span>Fechas por registros</span> <b class="caret"></b>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="view_proyectados"></div>

          </div>
        </div>
      </div>
  </div>


  <div class="">
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <!-- <div class="icon"><i class="fa fa-money"></i></div> -->
          <div class="count">$<?= $totalletras ?></div>
          <h3>Total</h3>
          <p>Total de letras</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-user"></i></div>
          <div class="count"><?= $totalregistros ?></div>
          <h3>Clientes</h3>
          <p>Total de clientes</p>
        </div>
      </div>
      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
          <div class="icon"><i class="fa fa-building-o"></i></div>
          <div class="count"><?= $cantempresas ?></div>
          <h3>Empresas</h3>
          <p>Total de empresas</p>
        </div>
      </div>
    </div>
  </div>



<!-- view modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información de la Compañia</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- Espacio para info --></h2></u></strong>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-direccion">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-ruc">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-sitio_web">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-ano_constitucion">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-correo">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-nombre_rrhh">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono_rrhh">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-nombre_contabilidad">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono_contabilidad">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-forma_pago">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-cantidad_empleados">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-rubro">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-nombre-firma">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-posicion-firma">
                      <!-- Espacio para info -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha_creacion">
                      <!-- Espacio para info -->
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-usuarios_id">
                      <!-- Espacio para info -->
                    </div>
                </div> 
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End view modal-->



    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_pagos" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Pagos</h3>
                </div>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="modal-nombre text-info"><!-- info --></h2>
                    </div>
                  </div>

                  <div class="table-responsive company_user">
                    <table class="table table-striped table-bordered jambo_table bulk_action" width="100%">
                      <thead>
                        <tr>
                          <!-- <th style="text-align: center"><input type="checkbox" id="totalpagos" onclick="totalpagos()"></th> -->
                          <th>Deuda</th>
                          <th>Fecha tope</th>
                          <th>Abono</th>
                          <th>Principal</th>
                          <th>Interés pendiente</th>
                          <th>Interés</th>
                          <th>Tasa interés</th>
                          <th>Cargo pendiente</th>
                          <th>Cargo administrativo</th>
                          <th>Cargo mora</th>
                          <th>Total</th>
                          <th>Fecha de pago</th>
                          <th>Forma de pago</th>
                          <th>Monto</th>
                          <th>Archivo</th>
                          <th>Nota</th>
                          <th>Registrado por</th>
                        </tr>
                      </thead>
                      <tbody class="filaspagos">
                      </tbody>
                    </table>
                  </div>

                </div>
                <div class="modal-footer">
<!--                   <button type="button" class="btn btn-primary btn-xs" onclick="confirm_remove()" id="btnProcesarCobro">
                    <i class="fa fa-trash-o"></i> Remover
                  </button> -->
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->



    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_envios" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Envios de Correo</h3>
                </div>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="modal-empresa text-info"><!-- info --></h2>
                    </div>
                  </div>

                  <div class="table-responsive">
                    <table class="table table-striped table-bordered jambo_table bulk_action" width="100%">
                      <thead>
                        <tr>
                          <th>Fecha de Envio</th>
                          <th>Estado</th>
                          <th>Tipo</th>
                          <th>Sujeto</th>
                        </tr>
                      </thead>
                      <tbody class="filasenvios">
                      </tbody>
                    </table>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


<!-- Modal para eliminar -->
<div id="modalCorreo" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea enviar el correo?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->


</div>
<!-- /page content -->




    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

var fd;
var f_inicio;
var f_fin;

$(document).ready(function() {

  fd = new Date();
  f_inicio = fd.format('d/m/Y');
  f_fin = (fd.getDate()+1) + "/" + (fd.getMonth() +1) + "/" + fd.getFullYear();

  var cb = function(start, end, label) {
    console.log(start.toISOString(), end.toISOString(), label);
/*      alert(start.toISOString());
    alert(end.toISOString());
    alert(label);*/
    //$('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    $('#reportrange_right span').html('Desde: ' + start.format('DD/MM/YYYY') + ' Hasta: ' + end.format('DD/MM/YYYY'));

    consulta(start.toISOString(), end.toISOString());

  };

  var f = new Date();

  //alert(f.format('m/d/Y'));
  //(f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear()

  var dias = daysInMonth((f.getMonth() +2),f.getFullYear());

  var optionSet1 = {
    //startDate: moment().subtract(29, 'days'),
    endDate: moment(),
    minDate: '01/01/2016',
    maxDate: (f.getMonth() + 6) + "/" + dias + "/" + f.getFullYear(),
    dateLimit: {
      days: 90
    },
    showDropdowns: true,
    showWeekNumbers: true,
    timePicker: false,
    timePickerIncrement: 1,
    timePicker12Hour: true,
    ranges: {
      'Hoy': [moment(), moment()],
      'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Último 7 Días': [moment().subtract(6, 'days'), moment()],
      'Último 30 Días': [moment().subtract(29, 'days'), moment()],
      'Este Mes': [moment().startOf('month'), moment().endOf('month')],
      'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    opens: 'right',
    buttonClasses: ['btn btn-default'],
    applyClass: 'btn-small btn-primary',
    cancelClass: 'btn-small',
    format: 'MM/DD/YYYY',
    separator: ' to ',
    locale: {
      applyLabel: 'Enviar',
      cancelLabel: 'Cerrar',
      fromLabel: 'Desde',
      toLabel: 'Hasta',
      customRangeLabel: 'Personalizado',
      daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      firstDay: 1
    }
  };

  //$('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
  $('#reportrange_right span').html('Desde: ' + moment().format('DD/MM/YYYY') + ' Hasta: ' + moment().format('DD/MM/YYYY'));

  $('#reportrange_right').daterangepicker(optionSet1, cb);

  $('#reportrange_right').on('show.daterangepicker', function() {
    console.log("show event fired");
  });
  $('#reportrange_right').on('hide.daterangepicker', function() {
    console.log("hide event fired");
  });
  $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
    console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
  });
  $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
    console.log("cancel event fired");
  });

  $('#options1').click(function() {
    $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
  });

  $('#options2').click(function() {
    $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
  });

  $('#destroy').click(function() {
    $('#reportrange_right').data('daterangepicker').remove();
  });

  $('.proyect').hide();
  consulta(f_inicio, f_fin);

});

function consulta(fecha_i,fecha_f){
  $.ajax({
      url : "<?php echo site_url('reportes/consulta_cobros_proyectados')?>",
      type: "POST",
      data: {
        'fecha_i' : fecha_i,
        'fecha_f' : fecha_f
      },
      dataType: "JSON",
      success: function(data)
      {

        // <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="ver_envio('+item[0].companias_id+')"><i class="fa fa-envelope-o"></i> Ver Envios </a>

        if (data) {
          $('.proyect').show();

          var html = '';
          var cant = 0;
          $.each(data, function(i,item){
            cant++;
            if (i != 'TotalesFinales') {
              html += '<form id="'+item[0].companias_id+'"><div class="table-responsive">' +
                  '<table class="table table-striped table-bordered jambo_table" id="datatable-proyectados" cellspacing="0" width="100%">' +
                    '<caption><h2><a href="javascript:void(0)" onclick="view_companies('+item[0].companias_id+')">'+i+'</a></h2> <a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_recordatorio('+item[0].companias_id+')"><i class="fa fa-envelope-o"></i> Recordatorio de Pago </a><a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_detalle('+item[0].companias_id+')"><i class="fa fa-envelope-o"></i> Detalle de Pago </a> <span class="label label-danger" id="msg'+item[0].companias_id+'"></span></caption>' +
                    '<thead>' +
                      '<tr>' +
                      '<th>Cliente</th>' +
                      '<th>Aprobado</th>' +
                      '<th>Letra</th>' +
                      '<th>Desembolsado</th>' +
                      '<th>Primer Descuento</th>' +
                      '<th>Fecha Pendiente</th>' +
                      '<th>Monto</th>' +
                      '<th>Estado</th>' +
                      '</tr>' +
                    '</thead>';                    
                    
            } else {
              $('.total').html('$' + item.monto);
            }

            $.each(item, function(j,jtem){ 

              if (j >= 0) {
                html += '<tbody>'+
                    '<td><input type="hidden" name="aprobaciones_id[]" value="'+jtem.aprobacion_id+'"><a href="javascript:void(0)" onclick="remove_payment('+jtem.aprobacion_id+')">'+jtem.cliente+' </a></td>' +
                    '<td>$'+jtem.aprobado+'</td>' +
                    '<td>$'+jtem.letra+'</td>' +
                    '<td>'+jtem.desembolsado+'</td>' +
                    '<td>'+jtem.primer_descuento+'</td>' +
                    '<td>'+jtem.fecha_pendiente+'</td>' +
                    '<td>$'+jtem.monto+'</td>' +
                    '<td>'+jtem.estado+'</td>' +
                  '</tbody>';
              }

              if (j == 'acumulados') {
                html += '<tbody>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td>$'+jtem.monto+'</td>' +
                    '<td></td>' +
                  '</tbody>';             
              }

            });

            html += '</table>' +
                '</div></form>';

          });

          if (cant > 0) {
            cant = cant - 1;
          }
          $('.cante').html(cant);
          
          $('.view_proyectados').html(html);

        } else {
          $('.proyect').hide();
          $('.view_proyectados').html('Lo siento, no hay registros.');
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function daysInMonth(humanMonth, year) {
  return new Date(year || new Date().getFullYear(), humanMonth, 0).getDate();
}

function view_companies(id)
{
    $.ajax({
        url : "<?php echo site_url('companias/ajax_view/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información de la Compañia');
            $('.modal-nombre').text(data.nombre);
            $('.modal-direccion').html('<strong>Dirección: </strong>' + data.direccion);
            $('.modal-telefono').html('<strong>Teléfono: </strong>' + data.telefono);
            $('.modal-ruc').html('<strong>RUC: </strong>' + data.ruc);
            $('.modal-sitio_web').html('<strong>Sitio Web: </strong>' + data.sitio_web);
            $('.modal-ano_constitucion').html('<strong>Año de Constitución: </strong>' + data.ano_constitucion);
            $('.modal-correo').html('<strong>Correo Electrónico: </strong>' + data.correo);
            $('.modal-nombre_rrhh').html('<strong>Nombre del Recursos Humanos: </strong>' + data.nombre_rrhh);
            $('.modal-telefono_rrhh').html('<strong>Teléfono de Recurso Humano: </strong>' + data.telefono_rrhh);
            $('.modal-nombre_contabilidad').html('<strong>Nombre de Contabilidad: </strong>' + data.nombre_contabilidad);
            $('.modal-telefono_contabilidad').html('<strong>Teléfono de Contabilidad: </strong>' + data.telefono_contabilidad);
            $('.modal-forma_pago').html('<strong>Forma de Pago: </strong>' + data.forma_pago);
            $('.modal-cantidad_empleados').html('<strong>Cantidad de Empleados: </strong>' + data.cantidad_empleados);
            $('.modal-rubro').html('<strong>Rubro: </strong>' + data.rubro);
            $('.modal-nombre-firma').html('<strong>Nombre Firma: </strong>' + data.nombre_firma);
            $('.modal-posicion-firma').html('<strong>Posición Firma: </strong>' + data.posicion_firma);
            $('.modal-fecha_creacion').html('<strong>Fecha de Creación: </strong>' + data.fecha_creacion);
            $('.modal-usuarios_id').html('<strong>Creado Por: </strong>' + data.usuarios_id);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function remove_payment(id){
  $('#modal_form_pagos').modal('show');
  //$('#totalpagos').prop('checked',false);
  table_payment(id);
}

//var numpagos;
var aprobaId;
function table_payment(id){
  aprobaId = id;
  $.ajax({
      url : "<?php echo site_url('cobros/remove_payment/')?>" + aprobaId,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        
        $('.modal-nombre').text(data.nombre_cliente);

        if (data.cobros) {
          $('.filaspagos').empty();
          var trHTML = '';
          var abono = 0;
          var monto = 0;
          $.each(data.cobros, function (i, item) {
              trHTML += '<tr>' +
/*                  '<td style="text-align: center"><input type="checkbox" id="pago'+i+'" value="'+item.id+'" disabled></td>' +*/
                  '<td>$'+item.deuda+'</td>' +
                  '<td>'+item.fecha_correspondiente+'</td>' +
                  '<td>$'+item.abono+'</td>' +
                  '<td>$'+item.principal+'</td>' +
                  '<td>$'+item.interes_restante+'</td>' +
                  '<td>$'+item.interes+'</td>' +
                  '<td>%'+item.tasa_interes+'</td>' +
                  '<td>$'+item.cargo_restante+'</td>' +
                  '<td>$'+item.cargo_administrativo+'</td>' +
                  '<td>$'+item.interes_mora+'</td>' +
                  '<td>$'+item.total+'</td>' +
                  '<td>'+item.fecha_pago+'</td>' +
                  '<td>'+item.forma_pago+'</td>' +
                  '<td>$'+item.monto+'</td>' +
                  '<td>'+item.archivo+'</td>' +
                  '<td>'+item.nota+'</td>' +
                  '<td>'+item.usuarios_id+'</td>' +
                '</tr>';
                abono += parseFloat(item.abono);
                monto += parseFloat(item.monto);
          });
          trHTML += '<tr>' +
                  /*'<td></td>' +*/
                  '<td></td>' +
                  '<td></td>' +
                  '<td>$'+abono.toFixed(2)+'</td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td>$'+monto.toFixed(2)+'</td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                '</tr>';
          $('.filaspagos').append(trHTML);
/*          numpagos = data.cobros.length;

          var num = numpagos - 1;
          $('#pago'+num).removeAttr('disabled');*/

        } else {
          $('.filaspagos').empty();
          //var trHTML = '<tr><td colspan="15" style="text-align: center">Lo siento, no hay registros</td></tr>';
          var trHTML = '<tr><td colspan="14" style="text-align: center">Lo siento, no hay registros</td></tr>';
          $('.filaspagos').append(trHTML);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function confirm_recordatorio(id) {
    $("#modalCorreo").modal("show");
    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="recordatorio_pago('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function confirm_detalle(id){
    $("#modalCorreo").modal("show");
    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="detalle_pago('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}


function recordatorio_pago(id){
  $('#msg'+id).text('');
  $.ajax({
      url : "<?php echo site_url('reportes/recordatorio_pago')?>",
      type: "POST",
      data: $('#'+id).serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            $('#msg'+id).removeClass('label label-danger').addClass('label label-success');
            $('#msg'+id).text('Recordatorio de pago enviado.');
          }
          if (data.validation) {
            $('#msg'+id).removeClass('label label-success').addClass('label label-danger');
            $('#msg'+id).text('Empresa sin correo. Registrelo e intente de nuevo.');
          }
          $("#modalCorreo").modal("hide");
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function detalle_pago(id){
  $('#msg'+id).text('');
  $.ajax({
      url : "<?php echo site_url('reportes/detalle_pago')?>",
      type: "POST",
      data: $('#'+id).serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            $('#msg'+id).removeClass('label label-danger').addClass('label label-success');
            $('#msg'+id).text('Solicitud detalle de pago enviado.');
          }
          if (data.validation) {
            $('#msg'+id).removeClass('label label-success').addClass('label label-danger');
            $('#msg'+id).text('Empresa sin correo. Registrelo e intente de nuevo.');
          }
          $("#modalCorreo").modal("hide");
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}


/*function ver_envio(id){
  $('#modal_form_envios').modal('show');
  $.ajax({
      url : "<?php echo site_url('reportes/ver_envio_correos')?>",
      type: "POST",
      data: $('#'+id).serialize(),
      dataType: "JSON",
      success: function(data)
      {

        $('.filasenvios').empty();
        var trHTML = '';
        if (data.length > 0) {
          $.each(data, function (i, item) {
              //alert(i + ': ' + item);
              trHTML += '<tr>' +
                  '<td>'+item.fecha_envio+'</td>' +
                  '<td>'+item.estado+'</td>' +
                  '<td>'+item.tipo+'</td>' +
                  '<td>'+item.sujeto+'</td>' +
                '</tr>';
          });
          $('.filasenvios').append(trHTML);
        } else {
          trHTML += '<tr><td colspan="4" style="text-align: center">Lo siento, no hay registros</td></tr>';
          $('.filasenvios').append(trHTML);
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}*/


</script>

<!-- End bootstrap-daterangepicker -->