<!-- page content -->
<div class="right_col" role="main">

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Reportes de APC clientes <small>Listado de Reportes</small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Refrescar</button>

            <button class="btn btn-default btn-xs"  onClick='<?php redirect(base_url() . 'reportes/excel_apc'); ?>'><i class="glyphicon glyphicon-refresh"></i> Exportar Excel</button>

            <div class="col-lg-12 col-sm-12 col-12 table-responsive">
              <table class="table table-striped table-bordered jambo_table" id="datatable-apc" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th>Primer Apellido</th>
                          <th>Segundo Apellido</th>
                          <th>Primer Nombre</th>
                          <th>Segundo Nombre</th>
                      </tr>
                  </thead>
                  <tbody>
                  <tfoot>
                      <tr>
                          <th>Primer Apellido</th>
                          <th>Segundo Apellido</th>
                          <th>Primer Nombre</th>
                          <th>Segundo Nombre</th>
                      </tr>
                  </tfoot>
                  </tbody>
              </table>
            </div>

          </div>
        </div>
      </div>
  </div>


</div>
<!-- /page content -->




    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

var fd;
var f_inicio;
var f_fin;
var table;

$(document).ready(function() {
    //datatables
    table = $('#datatable-apc').DataTable( {
        //"order": [[ 0, "asc" ]],
        "order": [],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('reportes/list_apc')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          { 
              "targets": [], //last column
              "orderable": false, //set not orderable
          },
        ],

    } );
});


function reload_table()
{
    table.ajax.reload(null,false);
}


</script>
