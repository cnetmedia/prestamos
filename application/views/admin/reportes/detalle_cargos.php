<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Reportes Desglose Cargos e ITBMS sobre la comisión <small>Listado de Reportes</small></h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="row col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
            <div id="reportrange_right" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
              <span>Consultar Fecha</span> <b class="caret"></b>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="records"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->




<!-- jQuery -->
<script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">
  var fd;
  var f_inicio;
  var f_fin;

  $(document).ready(function() {
    fd = new Date();
    f_inicio = fd.format('d/m/Y');
    f_fin = (fd.getDate()+1) + "/" + (fd.getMonth() +1) + "/" + fd.getFullYear();

    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);

      f_inicio = start.toISOString();
      f_fin = end.toISOString();

      //$('#reportrange_right span').html('Desde: ' + start.format('MMMM D, YYYY') + ' Hasta: ' + end.format('MMMM D, YYYY'));
      $('#reportrange_right span').html('Desde: ' + start.format('DD/MM/YYYY') + ' Hasta: ' + end.format('DD/MM/YYYY'));

      consulta(start.toISOString(),end.toISOString());

    };

    var f = new Date();

    //alert(f.format('m/d/Y'));
    //(f.getMonth() +1) + "/" + f.getDate() + "/" + f.getFullYear()

    var optionSet1 = {
      //startDate: moment().subtract(29, 'days'),
      startDate: moment(),
      endDate: moment(),
      minDate: '01/01/2016',
      maxDate: (f.getMonth() +1) + "/" + (f.getDate()+1) + "/" + f.getFullYear(),
      dateLimit: {
        days: 90
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Último 7 Días': [moment().subtract(6, 'days'), moment()],
        'Último 30 Días': [moment().subtract(29, 'days'), moment()],
        'Este Mes': [moment().startOf('month'), moment().endOf('month')],
        'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'right',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'MM/DD/YYYY',
      separator: ' to ',
      locale: {
        applyLabel: 'Enviar',
        cancelLabel: 'Cerrar',
        fromLabel: 'Desde',
        toLabel: 'Hasta',
        customRangeLabel: 'Personalizado',
        daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        firstDay: 1
      }
    };

    //$('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    $('#reportrange_right span').html('Desde: ' + moment().format('DD/MM/YYYY') + ' Hasta: ' + moment().format('DD/MM/YYYY'));

    $('#reportrange_right').daterangepicker(optionSet1, cb);

    $('#reportrange_right').on('show.daterangepicker', function() {
      console.log("show event fired");
      //alert("show event fired");
    });
    $('#reportrange_right').on('hide.daterangepicker', function() {
      console.log("hide event fired");
      //alert("hide event fired");
    });
    $('#reportrange_right').on('apply.daterangepicker', function(ev, picker) {
      console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
      //alert("apply event fired");
      //alert(picker.startDate.format('MMMM D, YYYY'));
    });
    $('#reportrange_right').on('cancel.daterangepicker', function(ev, picker) {
      console.log("cancel event fired");
      //alert("cancel event fired");
    });

    $('#options1').click(function() {
      $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
      //alert("ange_right");
    });

    $('#options2').click(function() {
      $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
    });

    $('#destroy').click(function() {
      $('#reportrange_right').data('daterangepicker').remove();
      //alert('destroy');
    });

    consulta(f_inicio, f_fin);

  });

  function consulta(fecha_i, fecha_f){
    $.ajax({
      url : "<?php echo site_url('reportes/get_detalle_cargos')?>",
      type: "POST",
      data: {
        'fecha_i' : fecha_i,
        'fecha_f' : fecha_f
      },
      dataType: "JSON",
      success: function(data)
      {
        $('.records').empty();
        if (data.records == false) {
          $('.records').html('<p>Lo siento no hay registros sobre la fecha seleccionada.</p>');
        }else{
          var html = '<div class="table-responsive">' +
            '<table class="table table-striped table-bordered jambo_table" id="datatable-recolectados" cellspacing="0" width="100%">' +
              '<caption><h2>Préstamos</h2></caption>' +
              '<thead>' +
                '<tr>' +
                '<th>Cliente</th>' +
                '<th>Cédula</th>' +
                '<th>Estado Actual</th>' +
                '<th>Producto</th>' +
                '<th>Fecha Aprobación</th>' +
                '<th>Monto</th>' +
                '<th>Intereses</th>' +
                '<th>Gastos notariales</th>' +
                '<th>Gastos administrativos</th>' +
                '<th>Comisión</th>' +
                '<th>ITBMS</th>' +
                '</tr>' +
              '</thead><tbody>';
          $.each(data.records, function(i,item){
          //alert(i + ': ' + item);
          html +=    '<tr><td>'+item.cliente+'</td>' +
              '<td>'+item.cedula+'</td>' +
              '<td>'+item.estado+'</td>' +
              '<td>'+item.producto+'</td>' +
              '<td>'+item.fecha_aprobado+'</td>' +
              '<td style="text-align:right;">'+item.cantidad_aprobada+'</td>' +
              '<td style="text-align:right;">'+item.calculos.intereses+'</td>' +
              '<td style="text-align:right;">'+item.calculos.notariales+'</td>' +
              '<td style="text-align:right;">'+item.calculos.cargo+'</td>' +
              '<td style="text-align:right;">'+item.calculos.comision+'</td>' +
              '<td style="text-align:right;">'+item.calculos.itbms+'</td></tr>';
          });

          html += '</tbody></table>' +
            '</div>';
          $('.records').html(html);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }

</script>


<!-- End bootstrap-daterangepicker -->