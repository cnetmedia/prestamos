  <style>
    #div_carga{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      /*background: url(<?php echo site_url('assets/images/gris.png') ?>) repeat;*/
      display:none;
      z-index:1;
    }

    #cargador{
      position:absolute;
      top:50%;
      left: 50%;
      margin-top: -25px;
      margin-left: -25px;
    }

    #cargador_aviso{
      position:absolute;
      top:40%;
      left: 47%;
      margin-top: -25px;
      margin-left: -25px;
    }
  </style>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Ajustes del Sistema</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Roles de usuario</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table id="roles-list" class="table table-striped table-bordered dt-responsive jambo_table" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Rol</th>
                          <th style="text-align: center">Inserta</th>
                          <th style="text-align: center">Modifica</th>
                          <th style="text-align: center">Elimina</th>
                          <th style="text-align: center">Aprueba</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Modulos por Rol</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table id="modulos-roles-list" class="table table-striped table-bordered dt-responsive jambo_table" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Modulo</th>
                          <?php
                            if ($roles) {
                              foreach ($roles as $row) {
                                echo "<th style='text-align: center'>".$row->descripcion."</th>";
                              }
                            }
                          ?>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>


<!--             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Disposiciones o Notas</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                  <button class="btn btn-success btn-xs" onclick="add_nota()"><i class="glyphicon glyphicon-plus"></i> Crear Nota</button>


                  <table id="datatable-notas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Nota</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>

                    <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Nota</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>

                  </table>


                  </div>
                </div>
              </div>
            </div> -->


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Disposiciones</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                  <button class="btn btn-success btn-xs" onclick="add_disposicion()"><i class="glyphicon glyphicon-plus"></i> Crear </button>


                  <table id="datatable-disposicion" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                    </tfoot>
                  </table>


                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <!-- <h2>Subir Archivos Access (Comprimidos .zip) Recuerde que deben ser Access 2002 - 2003.</h2> -->
                    <h2>Subir Archivos CSV (Comprimidos .zip) <b>Nota:</b> Asegurese de que el CSV contenga el nombre del mes y el año. Ejemplo: enero17.</h2>
                    <h2>Luego de subirlo importelo a su base de datos.</h2>

                    <div id="div_carga">
                      <p id="cargador_aviso">Por favor espere...</p>
                      <img id="cargador" src="<?php echo site_url('assets/images/ajax-loader.gif') ?>"/>
                    </div>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <div class="label label-danger aviso-archivo"></div><br>
                    </div>

                    <div class="clearfix"></div>

                    <div class="progress" style="margin:10px">
                      <div id="bar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <span class="sr-only">0% Complete</span>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                     <form method="POST" class="myForm" enctype="multipart/form-data">
                          
                          <input type="hidden" value="" name="cliente_id" id="client"/>

                          <div class="col-lg-6 col-sm-6 col-12">                            
                              <div class="input-group">
                                  <label class="input-group-btn">
                                      <span class="btn btn-primary">
                                          Examinar&hellip; <input type="file" name="file" style="display: none;" multiple>
                                      </span>
                                  </label>
                                  <input type="text" name="nombre_archivo" class="form-control" readonly>
                              </div>
                          </div>

                          <div class="col-lg-2 col-sm-2 col-12">
                              <button type="button" class="btn btn-success" onclick="submitFile();" id="bto">
                                  <i class="fa fa-upload"></i> Subir
                              </button>
                          </div>             

                      </form>


                      <table id="access-list" class="table table-striped table-bordered dt-responsive jambo_table" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Fecha de Creación</th>
                            <th>Acción</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>


                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->



<!-- Bootstrap modal -->
<!-- <div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="id"/>

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="text" name="nombre" class="form-control" placeholder="Nombre">
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div> -->
<!-- End Bootstrap modal -->



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_disposicion" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Disposición</h3>
            </div>
            <div class="modal-body">

              <form id="form_disposicion" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="id"/>

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="text" name="nombre" class="form-control" placeholder="Nombre">
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_disposicion()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


        <!-- Modal para eliminar -->
        <div id="myModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                Seguro de realizar el cambio?
              </div>
              <div id="info" class="modal-footer"><!-- Botones --></div>
            </div>
          </div>
        </div>
        <!-- Fin de modal eliminar -->

        <!-- Modal para eliminar -->
        <div id="myModal1" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                Seguro de eliminar el archivo?
              </div>
              <div id="info1" class="modal-footer"><!-- Botones --></div>
            </div>
          </div>
        </div>
        <!-- Fin de modal eliminar -->

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>
    <script src="<?php echo site_url('assets/js/jquery.form.js') ?>"></script>

<script type="text/javascript">

var table;
var table1;
var table2;
//var table3;
var table4;

$(document).ready(function() {
  table = $('#roles-list').DataTable( {
    "paging": false,
    "ordering": false,
    "info": false,
    "bFilter": false,
      "order": [[ 0, "asc" ]],
      "destroy": true,
      "bRetrieve": true,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
          "url":  "<?php echo site_url('admin/roles_list')?>",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [ -1 ],
          "orderable": false,
      },
      ],
  } );




  table1 = $('#modulos-roles-list').DataTable( {
    "paging": false,
    "ordering": false,
    "info": false,
    "bFilter": false,
      "order": [[ 0, "asc" ]],
      "destroy": true,
      "bRetrieve": true,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
          "url":  "<?php echo site_url('admin/modulos_roles_list')?>",
          "type": "POST"
      }
  } );

  table2 = $('#access-list').DataTable( {
    "paging": false,
    "ordering": false,
    "info": false,
    "bFilter": false,
      "order": [[ 0, "asc" ]],
      "destroy": true,
      "bRetrieve": true,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
          "url":  "<?php echo site_url('admin/access_list')?>",
          "type": "POST"
      }
  } );

/*  table3 = $('#datatable-notas').DataTable( {
      "destroy": true,
      "bRetrieve": true,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url('admin/ajax_notas')?>",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [ -1 ],
          "orderable": false,
      },
      ],
  } );*/

  table4 = $('#datatable-disposicion').DataTable( {
      "destroy": true,
      "bRetrieve": true,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url('admin/ajax_disposiciones')?>",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [ -1 ],
          "orderable": false,
      },
      ],
  } );

});

function reload_table()
{
  table.ajax.reload(null,false);
}

function reload_table1()
{
  table1.ajax.reload(null,false);
}

function reload_table2()
{
  table2.ajax.reload(null,false);
}

function reload_table3()
{
  table3.ajax.reload(null,false);
}

function reload_table4()
{
  table4.ajax.reload(null,false);
}

function confirmInsertCheck(id) {
    $("#myModal").on("show", function() {
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");
            $("#myModal").modal('hide');
        });
    });
    $("#myModal").on("hide", function() {
        $("#myModal a.btn").off("click");
    });    
    $("#myModal").on("hidden", function() {
        $("#myModal").remove();
    });    
    $("#myModal").modal({
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true
    });
    $("#info").html('<a class="btn btn-success btn-xs" onclick="insertarCheck('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal" onclick="reload_table()"><i class="fa fa-close"></i> No</a>');
}

function insertarCheck(id){
  var insertar = $('#insertar'+id).val();
  if (insertar == 1) { insertar = 0 } else { insertar = 1 }
  $.ajax({
      url : "<?php echo site_url('admin/rolInsertUpdate')?>",
      type: "POST",
      data: {"id":id,"insertar":insertar},
      dataType: "JSON",
      success: function(data)
      {
          if (data.status)
          {
              $('#myModal').modal('hide');
              reload_table();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function confirmEditCheck(id) {
    $("#myModal").on("show", function() {
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");
            $("#myModal").modal('hide');
        });
    });
    $("#myModal").on("hide", function() {
        $("#myModal a.btn").off("click");
    });    
    $("#myModal").on("hidden", function() {
        $("#myModal").remove();
    });    
    $("#myModal").modal({
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true
    });
    $("#info").html('<a class="btn btn-success btn-xs" onclick="editarCheck('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal" onclick="reload_table()"><i class="fa fa-close"></i> No</a>');
}

function editarCheck(id){
  var modificar = $('#modificar'+id).val();
  if (modificar == 1) { modificar = 0 } else { modificar = 1 }
  $.ajax({
      url : "<?php echo site_url('admin/rolEditUpdate')?>",
      type: "POST",
      data: {"id":id,"modificar":modificar},
      dataType: "JSON",
      success: function(data)
      {
          if (data.status)
          {
              $('#myModal').modal('hide');
              reload_table();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function confirmDeleteCheck(id) {
    $("#myModal").on("show", function() {
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");
            $("#myModal").modal('hide');
        });
    });
    $("#myModal").on("hide", function() {
        $("#myModal a.btn").off("click");
    });    
    $("#myModal").on("hidden", function() {
        $("#myModal").remove();
    });    
    $("#myModal").modal({
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true
    });
    $("#info").html('<a class="btn btn-success btn-xs" onclick="borrarCheck('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal" onclick="reload_table()"><i class="fa fa-close"></i> No</a>');
}

function borrarCheck(id){
  var eliminar = $('#eliminar'+id).val();
  if (eliminar == 1) { eliminar = 0 } else { eliminar = 1 }
  $.ajax({
      url : "<?php echo site_url('admin/rolDeleteUpdate')?>",
      type: "POST",
      data: {"id":id,"eliminar":eliminar},
      dataType: "JSON",
      success: function(data)
      {
          if (data.status)
          {
              $('#myModal').modal('hide');
              reload_table();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function confirmAprobarCheck(id) {
    $("#myModal").on("show", function() {
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");
            $("#myModal").modal('hide');
        });
    });
    $("#myModal").on("hide", function() {
        $("#myModal a.btn").off("click");
    });    
    $("#myModal").on("hidden", function() {
        $("#myModal").remove();
    });    
    $("#myModal").modal({
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true
    });
    $("#info").html('<a class="btn btn-success btn-xs" onclick="aprobarCheck('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal" onclick="reload_table()"><i class="fa fa-close"></i> No</a>');
}

function aprobarCheck(id){
  var aprobar = $('#aprobar'+id).val();
  if (aprobar == 1) { aprobar = 0 } else { aprobar = 1 }
  $.ajax({
      url : "<?php echo site_url('admin/rolAprobarUpdate')?>",
      type: "POST",
      data: {"id":id,"aprobar":aprobar},
      dataType: "JSON",
      success: function(data)
      {
          if (data.status)
          {
              $('#myModal').modal('hide');
              reload_table();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function sadmin(modulos_id){
  var roles_id = $('#sadmin'+modulos_id).val();
  $.ajax({
      url : "<?php echo site_url('admin/modulorol')?>",
      type: "POST",
      data: {"modulos_id":modulos_id,"roles_id":roles_id},
      dataType: "JSON",
      success: function(data)
      {
          reload_table1();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function admin(modulos_id){
  var roles_id = $('#admin'+modulos_id).val();
  $.ajax({
      url : "<?php echo site_url('admin/modulorol')?>",
      type: "POST",
      data: {"modulos_id":modulos_id,"roles_id":roles_id},
      dataType: "JSON",
      success: function(data)
      {
          reload_table1();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function repre(modulos_id){
  var roles_id = $('#repre'+modulos_id).val();
  $.ajax({
      url : "<?php echo site_url('admin/modulorol')?>",
      type: "POST",
      data: {"modulos_id":modulos_id,"roles_id":roles_id},
      dataType: "JSON",
      success: function(data)
      {
          reload_table1();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function sup(modulos_id){
  var roles_id = $('#sup'+modulos_id).val();
  $.ajax({
      url : "<?php echo site_url('admin/modulorol')?>",
      type: "POST",
      data: {"modulos_id":modulos_id,"roles_id":roles_id},
      dataType: "JSON",
      success: function(data)
      {
          reload_table1();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function cob(modulos_id){
  var roles_id = $('#cob'+modulos_id).val();
  $.ajax({
      url : "<?php echo site_url('admin/modulorol')?>",
      type: "POST",
      data: {"modulos_id":modulos_id,"roles_id":roles_id},
      dataType: "JSON",
      success: function(data)
      {
          reload_table1();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function cob1(modulos_id){
  var roles_id = $('#cob1'+modulos_id).val();
  $.ajax({
      url : "<?php echo site_url('admin/modulorol')?>",
      type: "POST",
      data: {"modulos_id":modulos_id,"roles_id":roles_id},
      dataType: "JSON",
      success: function(data)
      {
          reload_table1();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function cob2(modulos_id){
  var roles_id = $('#cob2'+modulos_id).val();
  $.ajax({
      url : "<?php echo site_url('admin/modulorol')?>",
      type: "POST",
      data: {"modulos_id":modulos_id,"roles_id":roles_id},
      dataType: "JSON",
      success: function(data)
      {
          reload_table1();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

$('.permission-error-archivo').hide();
$('.progress').hide();

function submitFile(){
    $('.aviso-archivo').hide();
    $('.progress').show();
    var formData = new FormData($('.myForm')[0]);
    var id = $('#client').val();
    $.ajax({
        xhr: function() {

            if(window.XMLHttpRequest) {
              var xhr = new window.XMLHttpRequest();
            }else if(window.ActiveXObject) {
              var xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }

/*            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.responseText) {
                        $('.aviso-archivo').show().html(data.aviso);
                    }
                }
            };*/

/*            xhr.addEventListener("load", function (evt) {
                alert(evt);
            }, false);*/

            if(xhr.upload){
              //Upload progress
              xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                  var percentComplete = (evt.loaded / evt.total) * 100;
                  //Do something with upload progress
                  //alert(percentComplete + ' 1');
                  $('#bar').css('width', percentComplete + '%');
                }
              }, false);
              //Download progress
              xhr.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                  var percentComplete = (evt.loaded / evt.total) * 100;
                  //Do something with download progress
                  //alert(percentComplete + ' 2');        
                  $('#bar').css('width', percentComplete + '%');
                }
              }, false);
              return xhr;
            }

        },
        url: "<?php echo site_url('admin/cargar_archivo')?>",
        type: 'POST',
        data: formData,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
            if (data.status) {
                reload_table2();
            } else if (data.aviso) {
                $('.aviso-archivo').show().html(data.aviso);
            }
            $('.progress').hide();
            $('.myForm')[0].reset();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
}


$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {

    $('.aviso-archivo').hide();

    var file = this.files[0];
    name = file.name;
    size = file.size;
    //type = file.type;

    var ext = name.split('.')[1];
    if (ext != 'zip') {
      $('.aviso-archivo').show().html('El archivo debe ser formato .zip.<br><br>');
      $('.myForm')[0].reset();
      return;
    }

    if (size > 400000000) {
      $('.aviso-archivo').show().html('El archivo supera el tamaño limite permitido.<br><br>');
      $('.myForm')[0].reset();
      return;
    }

    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
});


function confirm_delete(id) {

    $("#myModal1").modal('show');

    $("#info1").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" title="Hapus" onclick="delete_access('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}

function delete_access(id)
{
    $.ajax({
        url : "<?php echo site_url('admin/delete_access')?>/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            if (data.status) {
                reload_table2();
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error deleting data');
        }
    });
    $("#myModal1").modal('hide');
}

/*function descomprimir(id){
  $.ajax({
      url : "<?php echo site_url('admin/extraer_archivo/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
              reload_table2();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}*/


/*function add_nota()
{
    $('.list-errors-notas').empty();
    save_method = 'add';
    $('#form_notas')[0].reset();
    $('.form-group').removeClass('has-error');
    $('#modal_form_notas').modal('show');
    $('.modal-title').text('Añadir Nota');
}


function save_notes()
{

    $('#btnSave').attr('disabled',true); 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('admin/add_notas')?>";
    } else {
        url = "<?php echo site_url('admin/update_notas')?>";
    }

    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_notas').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if (data.status)
            {
                $('#modal_form_notas').modal('hide');
                reload_table3();
            }

            if (data.validation) {
              $('.list-errors-notas').html(data.validation);
            }

            $('#btnSave').attr('disabled',false);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').attr('disabled',false);

        }
    });
}

function edit_nota(id){
    $('.list-errors-notas').empty();
    save_method = 'update';
    $('#form_notas')[0].reset();
    $.ajax({
        url : "<?php echo site_url('admin/view_nota/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="nota"]').val(data.nota);
            $('#modal_form_notas').modal('show');
            $('.modal-title').text('Editar Nota');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}



function confirm_delete_nota(id) {

    $("#myModal1").modal("show");

    $("#info1").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" title="Hapus" onclick="delete_nota('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}

function delete_nota(id)
{
  $.ajax({
      url : "<?php echo site_url('admin/delete_nota/')?>"+id,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
          $('#modal_form_notas').modal('hide');
          reload_table3();

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
  $("#myModal1").modal('hide');
}*/





function add_disposicion()
{
    $('.list-errors-notas').empty();
    save_method = 'add';
    $('#form_disposicion')[0].reset();
    $('.form-group').removeClass('has-error');
    $('#modal_form_disposicion').modal('show');
    $('.modal-title').text('Añadir Disposición');
}


function save_disposicion()
{

    $('#btnSave').attr('disabled',true); 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('admin/add_disposiciones')?>";
    } else {
        url = "<?php echo site_url('admin/update_disposiciones')?>";
    }

    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_disposicion').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if (data.status)
            {
                $('#modal_form_disposicion').modal('hide');
                reload_table4();
            }

            if (data.validation) {
              $('.list-errors-notas').html(data.validation);
            }

            $('#btnSave').attr('disabled',false);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').attr('disabled',false);

        }
    });
}

function edit_disposicion(id){
    $('.list-errors-notas').empty();
    save_method = 'update';
    $('#form_disposicion')[0].reset();
    $.ajax({
        url : "<?php echo site_url('admin/view_disposiciones/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('#modal_form_disposicion').modal('show');
            $('.modal-title').text('Editar Disposición');
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}



function confirm_delete_disposicion(id) {

    $("#myModal1").modal("show");

    $("#info1").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" title="Hapus" onclick="delete_disposicion('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}

function delete_disposicion(id)
{
  $.ajax({
      url : "<?php echo site_url('admin/delete_disposiciones/')?>"+id,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
          $('#modal_form_disposicion').modal('hide');
          reload_table4();

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
  $("#myModal1").modal('hide');
}

function importarCsv(nombre) {
  $('.aviso-archivo').hide();
  $('#div_carga').show();
  $.ajax({
      url : "<?php echo site_url('admin/importcsv/')?>" + nombre,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        if (data.status) {
          $('.aviso-archivo').show().html('Tabla importada en la Base de Datos Correctamente!.');
        } else {
          $('.aviso-archivo').show().html('Error al importar tabla.');
        }
        $('#div_carga').hide();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}


</script>