<style type="text/css">
  .checkboxStyle{
    background-color: #26B99A;
  }
</style>


        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cobros del Mes <small>Listado de Cobros</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row col-lg-2 col-sm-2 col-12">
                      <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
                    </div>

                    <div class="row col-lg-2 pull-right selectcompany">
                      <select class='form-control' name="filtrar_empresa" id="filtrar_empresa" onchange="empresas(this.value)"></select>
                    </div>   
                    
                    <span class="permission-error label label-danger"></span>

                    <br><br>

                    <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Cliente</th>
                          <th>Teléfono</th>
                          <th>Estado</th>
                          <th>Monto Aprobado</th>
                          <th>Fecha a Cobrar</th>
                          <th>Compañia</th>
                          <th>Estado de Cobro</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Cliente</th>
                          <th>Teléfono</th>
                          <th>Estado</th>
                          <th>Monto Aprobado</th>
                          <th>Fecha a Cobrar</th>
                          <th>Compañia</th>
                          <th>Estado de Cobro</th>
                          <th>Acciones</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- /page content -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_view" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Información del Usuario</h3>
                </div>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-xs-12 bottom">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <strong><u><h2 class="modal-nombre text-primary"><!-- info --></h2></u></strong>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <h2 class="text-info">Datos Personales</h2>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-genero">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-cedula">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-telefono">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-celular">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-estado_civil">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-correo">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_nacimiento">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-numero_dependientes">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-cantidad_solicitada">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-estado">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fuente_cliente">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-como_escucho_nosotros">
                          <!-- info -->
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <h2 class="text-info">Información Adicional</h2>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-tipo_vivienda">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-hipotecas">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-banco">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-descuento">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-tiempo_restante_descuento">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-capacidad">
                          <!-- info -->
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <h2 class="text-info">Dirección del Cliente</h2>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-corregimiento">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-distrito">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-provincia">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-direccion">
                          <!-- info -->
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <h2 class="text-info">Información de la Empresa</h2>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-salario">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-posicion_trabajo">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-companias_id">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-tiempo_laborando">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-nombre_jefe_directo">
                          <!-- info -->
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <h2 class="text-info">Referencia Personal</h2>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-nombre_referencia">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-apellido_referencia">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-parentesco_referencia">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-direccion_referencia">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-telefono_referencia">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-celular_referencia">
                          <!-- info -->
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <h2 class="text-info">Información del Sistema</h2>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_creacion">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_modificacion">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-usuarios_id">
                          <!-- info -->
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <h2 class="text-info">Prestamo Otorgado</h2>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-cantidad_aprobada">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-producto_ap">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-cantidad_meses">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-pago_mensual">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-pago_quincenal">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-pago_total">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_primer_descuento">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_entrega">
                          <!-- info -->
                        </div>


                    </div> 
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_cotizar" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Cobros</h3>
                </div>
                <div class="modal-body">

                <input type="hidden" name="cliente_id" id="cliente_id">
                  <input type="hidden" name="aprobacion_id" id="aprobacion_id">
                  <input type="hidden" name="numero_cobro" id="numero_cobro">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- info --></h2></u></strong>
                    </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-suma_prestamo">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-productos_id">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-plazo">
                        <!-- info -->
                      </div>


                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-titulo-pago">
                        <!-- info -->
                      </div>


                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_pago">
                        <!-- info -->
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback modal-forma_pago formapago">
                        <!-- info -->
                      </div>
                      <div class="col-md-5 col-sm-5 col-xs-12 form-group has-feedback modal-validacion-fecha_pago">
                        <!-- info -->
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-nota">
                        <!-- info -->
                      </div>
                  </div>

                  <table class="table table-striped table-bordered dt-responsive nowrap" id="datatable-cotizacion" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th style="text-align: center"><input type="checkbox" id="numtotal" onclick="numtotal()"></th>
                        <th>Fecha de Pagos</th>
                        <th>Principal</th>
                        <th>Interés</th>
                        <th>Tasa de Interés</th>
                        <th>Cargo Administrativo</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody class="filas">
                    </tbody>
                  </table>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-success btn-xs" onclick="procesarCobro()" id="btnProcesarCobro">
                    <i class="fa fa-check"></i> Aceptar
                  </button>
                  <button type="button" class="btn btn-success btn-xs" onclick="pagoAnticipado()" id="btnPagoAnticipado">
                    <i class="fa fa-money"></i> Cobrar Todo
                  </button>
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->



    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_estado_cobros" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Estado de Cobro</h3>
                </div>
                <div class="modal-body">

                  <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback aviso-estado-cobro"></div>

                    <form id="form_estado_cobro">

                      <input type="hidden" name="clientes_id_cobro">            

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <select class='form-control' name="estado_cobro" id="estado_cobro" onchange="estadocobro(this.value)">
                          <option disabled selected hidden>Estado de cobro</option>
                          <?php if (count($estado_cobros)) {
                            foreach ($estado_cobros as $row) {
                              echo "<option value='". $row['id'] . "'>" . $row['estado'] . "</option>";
                            }
                          } ?>
                        </select>
                      </div>

                      <div class="clearfix"></div>

                      <div class="modal-div-cobro">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha-retiro">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-hora-retiro">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-persona-contacto">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-numero-cheque">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-monto-cheque">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono-cobro">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-direccion-cobro">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-nombre-dirigirse">
                          <!-- info -->
                        </div>
                      </div>

                      <div class="modal-div-cobro1">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha-salida">
                          <!-- info -->
                        </div>
                      </div>

                    </form> 


                  </div>


                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-success btn-xs" onclick="save_estado_cobro()">
                    <i class="fa fa-save"></i> Guardar
                  </button>
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>



<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#datatable-responsive-solicitudes').DataTable( {
        "order": [[ 0, "asc" ]],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('cobros/ajax_list_mes_actual')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    } );

    view_companies();
    view_select_cobros();

});

function view_select_cobros()
{

    $('#filtrar_cobro').empty();
    var trHTML = "<option></option>";
    trHTML += "<option value='Todos'>Todos</option>";
    trHTML += "<option value='Mes actual'>Mes actual</option>";
    $('#filtrar_cobro').append(trHTML);

    $("#filtrar_cobro").select2({
      placeholder: "Filtrar Cobros",
      allowClear: false,
      language: "es",
      theme: "classic",
      minimumResultsForSearch: -1,
      dropdownParent: $(".selectcobros")
    });


/*    $.ajax({
        url : "<?php echo site_url('cobros/select_cobros')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {


            $('#filtrar_cobro').empty();
            var trHTML = "<option></option>";
            trHTML += "<option value='0'>Todos</option>";
            trHTML += "<option value='" + data + "'>" + data + "</option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#filtrar_cobro').append(trHTML);

            $("#filtrar_cobro").select2({
              placeholder: "Filtrar Cobros",
              allowClear: false,
              language: "es",
              theme: "classic",
              dropdownParent: $(".selectcobros")
            });
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });*/
}


function view_companies()
{
    $.ajax({
        url : "<?php echo site_url('clientes/view_companies')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#filtrar_empresa').empty();
            var trHTML = "<option></option>";
            trHTML += "<option value='0'>Todas</option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#filtrar_empresa').append(trHTML);

/*            $("#filtrar_empresa").select2({
              placeholder: "Filtrar por Empresa",
              allowClear: false,
              language: "es",
              theme: "classic",
              dropdownParent: $(".selectcompany")
            });*/
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    $("#filtrar_empresa").select2({
      placeholder: "Filtrar por Empresa",
      allowClear: false,
      language: "es",
      theme: "classic",
      dropdownParent: $(".selectcompany")
    });
}

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax 
}

function view_approval(id)
{

    $('.permission-error').hide();
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('cobros/view/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            
            $('#modal_form_view').modal('show');
            //$('.modal-title').text('Información del Cliente');
            $('.modal-nombre').text(data.nombre + ' ' + data.apellido);

            $('.modal-genero').html('<strong><i class="fa fa-venus-mars"></i> Género: </strong>' + data.genero);
            $('.modal-cedula').html('<strong><i class="fa fa-user"></i> Cédula: </strong>' + data.cedula);
            $('.modal-telefono').html('<strong><i class="fa fa-phone"></i> Teléfono: </strong>' + data.telefono);
            $('.modal-celular').html('<strong><i class="fa fa-mobile"></i> Celular: </strong>' + data.celular);
            $('.modal-estado_civil').html('<strong><i class="fa fa-spinner"></i> Estado Civil: </strong>' + data.estado_civil);
            $('.modal-correo').html('<strong><i class="fa fa-envelope-o"></i> Correo Electrónico: </strong>' + data.correo);
            $('.modal-fecha_nacimiento').html('<strong><i class="fa fa-birthday-cake"></i> Fecha de Nacimiento: </strong>' + data.fecha_nacimiento);
            $('.modal-numero_dependientes').html('<strong><i class="fa fa-users"></i> Numero de Dependientes: </strong>' + data.numero_dependientes);
            $('.modal-cantidad_solicitada').html('<strong><i class="fa fa-usd"></i> Catidad Solicitada: </strong>' + data.cantidad_solicitada);
            $('.modal-estado').html('<strong><i class="fa fa-adjust"></i> Estado o Status: </strong>' + data.estado);
            $('.modal-fuente_cliente').html('<strong><i class="fa fa-phone"></i> Fuente del Cliente: </strong>' + data.fuente_cliente);
            $('.modal-como_escucho_nosotros').html('<strong><i class="fa fa-file-text-o"></i> Como Escucho de Nosotros: </strong>' + data.como_escucho_nosotros);
            $('.modal-tipo_vivienda').html('<strong><i class="fa fa-home"></i> Tipo de Vivienda: </strong>' + data.tipo_vivienda);
            $('.modal-hipotecas').html('<strong><i class="fa fa-credit-card"></i> Tiene Hipotecas: </strong>' + data.hipotecas);
            $('.modal-banco').html('<strong><i class="fa fa-university"></i> Banco de la Hipoteca: </strong>' + data.banco);
            $('.modal-descuento').html('<strong><i class="fa fa-minus"></i> Tiene Descuento: </strong>' + data.descuento);
            $('.modal-tiempo_restante_descuento').html('<strong><i class="fa fa-clock-o"></i> Tiempo Restante del Descuento: </strong>' + data.tiempo_restante_descuento);
            $('.modal-capacidad').html('<strong><i class="fa fa-check"></i> Capacidad: </strong>' + data.capacidad);
            $('.modal-corregimiento').html('<strong><i class="fa fa-map-signs"></i> Corregimiento: </strong>' + data.corregimiento);
            $('.modal-distrito').html('<strong><i class="fa fa-file-map-o"></i> Distrito: </strong>' + data.distrito);
            $('.modal-provincia').html('<strong><i class="fa fa-map-marker"></i> Provincia: </strong>' + data.provincia);
            $('.modal-direccion').html('<strong><i class="fa fa-sign-in"></i> Dirección: </strong>' + data.direccion);
            $('.modal-salario').html('<strong><i class="fa fa-usd"></i> Salario: </strong>' + data.salario);
            $('.modal-posicion_trabajo').html('<strong><i class="fa fa-user"></i> Posición de Trabajo: </strong>' + data.posicion_trabajo);
            $('.modal-companias_id').html('<strong><i class="fa fa-building-o"></i> Empresa: </strong>' + data.companias_id);
            $('.modal-tiempo_laborando').html('<strong><i class="fa fa-clock-o"></i> Tiempo Laborando: </strong>' + data.tiempo_laborando);
            $('.modal-nombre_jefe_directo').html('<strong><i class="fa fa-black-tie"></i> Nombre del Jefe Directo: </strong>' + data.nombre_jefe_directo);
            $('.modal-nombre_referencia').html('<strong><i class="fa fa-user"></i> Nombre de Referencia: </strong>' + data.nombre_referencia);
            $('.modal-apellido_referencia').html('<strong><i class="fa fa-user"></i> Apellido de Referencia: </strong>' + data.apellido_referencia);
            $('.modal-parentesco_referencia').html('<strong><i class="fa fa-user"></i> Parentesco de Referencia: </strong>' + data.parentesco_referencia);
            $('.modal-direccion_referencia').html('<strong><i class="fa fa-sign-in"></i> Dirección de Referencia: </strong>' + data.direccion_referencia);
            $('.modal-telefono_referencia').html('<strong><i class="fa fa-phone"></i> Teléfono de Referencia: </strong>' + data.telefono_referencia);
            $('.modal-celular_referencia').html('<strong><i class="fa fa-mobile"></i> Celular de Referencia: </strong>' + data.celular_referencia);
            $('.modal-fecha_creacion').html('<strong><i class="fa fa-calendar-o"></i> Fecha de Creación: </strong>' + data.fecha_creacion);
            $('.modal-fecha_modificacion').html('<strong><i class="fa fa-calendar-o"></i> Fecha de Modificación: </strong>' + data.fecha_modificacion);
            $('.modal-usuarios_id').html('<strong><i class="fa fa-pencil"></i> Creado Por: </strong>' + data.usuarios_id);


            $('.modal-cantidad_aprobada').html('<strong><i class="fa fa-usd"></i> Préstamo aprobado: </strong>' + data.cantidad_aprobada);
            $('.modal-cantidad_meses').html('<strong><i class="fa fa-calendar-o"></i> Termino aprobado: </strong>' + data.cantidad_meses);
            $('.modal-pago_mensual').html('<strong><i class="fa fa-usd"></i> Pago mensual: </strong>' + data.pago_mensual);
            $('.modal-pago_quincenal').html('<strong><i class="fa fa-usd"></i> Pago quincenal: </strong>' + data.pago_mensual / 2);
            $('.modal-pago_total').html('<strong><i class="fa fa-usd"></i> Obligación Total: </strong>' + data.pago_total);
            $('.modal-cantidad_solicitada_ap').html('<strong><i class="fa fa-usd"></i> Cantidad solicitada: </strong>' + data.cantidad_solicitada);
            $('.modal-salario_ap').html('<strong><i class="fa fa-usd"></i> Salario devengado: </strong>' + data.salario);
            $('.modal-producto_ap').html('<strong><i class="fa fa-product-hunt"></i> Producto: </strong>' + data.producto_nombre);
            $('.modal-fecha_primer_descuento').html('<strong><i class="fa fa-calendar-o"></i> Fecha de primer descuento: </strong>' + data.fecha_primer_descuento);
            $('.modal-fecha_entrega').html('<strong><i class="fa fa-calendar-o"></i> Fecha de entrega: </strong>' + data.fecha_entrega);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });

}

function payment(id){
  $("#numtotal").prop("checked", false);
  $('#btnPagoAnticipado').hide();
  $.ajax({
      url : "<?php echo site_url('cobros/payment_view/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('#modal_form_cotizar').modal('show');
        $('.modal-nombre').text(data.nombre_cliente);
        $('.modal-suma_prestamo').html('<strong><i class="fa fa-usd"></i> Aprobado: </strong>' + data.cantidad_aprobada);
        $('.modal-productos_id').html('<strong><i class="fa fa-product-hunt"></i> Producto: </strong>' + data.nombre_producto);
        $('.modal-plazo').html('<strong><i class="fa fa-sun-o"></i> Plazo: </strong>' + data.cantidad_meses + " Meses a " + data.porcentaje + "%" );
        cotiza(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function cotiza(datos){
    $('.modal-titulo-pago').empty();
    $('.modal-fecha_pago').empty();
    $('.modal-forma_pago').empty();
    $('.modal-nota').empty();
    $('.modal-validacion-fecha_pago').empty();
    $.ajax({
        url : "<?php echo site_url('cobros/cobro')?>",
        type: "POST",
        data: datos,
        dataType: "JSON",
        success: function(data)
        {
            $('.filas').empty();
            var totalm = null;
            var trHTML = '';
            $.each(data.data, function (i, item) {
                trHTML += '<tr>' +
                  '<td align="center"><input type="checkbox" value="' + item.num + '" id="numcobro' + item.num + '" onclick="numcobro(' + item.num + ')" disabled></td>' +
                  '<td>' + item.fecha + '</td>' +
                  '<td>$' + item.principal.toFixed(2) + '</td>' +
                  '<td>$' + item.interes.toFixed(2) + '</td>' +
                  '<td align="center">%' + item.tasa.toFixed(2) + '</td>' +
                  '<td align="center">$' + item.cargo.toFixed(2) + '</td>' +
                  '<td>$' + item.total.toFixed(2) + '</td>' +
                '</tr>';
                totalm = item.total;
                $('#numero_cobro').val(i);
            });            
            trHTML += '<tr><td></td><td></td><td><span class="label label-warning">$'+ data.totalprincipal.toFixed(2) +'</span></td><td><span class="label label-info">$'+ data.totalintereses.toFixed(2) +'</span></td><td></td><td align="center"><span class="label label-primary">$'+ data.totalcargo.toFixed(2) +'</span></td><td><span class="label label-success">$'+ data.totalcobrar.toFixed(2) +'</span></td></tr>';
            $('.filas').append(trHTML);

            $('[name="cliente_id"]').val(datos.clientes_id);
            $('[name="aprobacion_id"]').val(datos.id);

            var num = data.cobros + 1;
            $('#numcobro'+num).removeAttr('disabled');

            if (data.cobros > 0) {
              for (var i = 1; i < num; i++) {
                $("#numcobro"+i).attr("checked", true);
              }
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
    
}

function numcobro(num){

  if($('#numcobro'+num).prop('checked')) {
    $('.modal-titulo-pago').html('<h2>Cobro Individual</h2>');
    $('.modal-fecha_pago').html('<input type="text" name="fecha_pago" id="fecha_pago" class="form-control has-feedback-left date-picker" placeholder="Fecha de Pago"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>');

    $('.modal-forma_pago').html('<select class="form-control" name="forma_pago" id="forma_pago"><option disabled selected hidden>Forma de pago</option><option value="Efectivo">Efectivo</option><option value="Cheque">Cheque</option><option value="Transferencia">Transferencia</option></select>');

    $('.modal-nota').html('<textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>');

    $(document).ready(function() {
      $('#fecha_pago').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: "DD/MM/YYYY",
      }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
      });
    });

/*    $("#forma_pago").select2({
      placeholder: "Forma de pago",
      allowClear: false,
      language: "es",
      theme: "classic",
      minimumResultsForSearch: -1,
      dropdownParent: $(".formapago")
    });*/

  } else {
    $('.modal-titulo-pago').empty();
    $('.modal-fecha_pago').empty();
    $('.modal-forma_pago').empty();
    $('.modal-nota').empty();
    $('.modal-validacion-fecha_pago').empty();
/*    $("#numtotal").prop("checked", false);
    numtotal();*/
  }
  
}

function procesarCobro(){
  var clienteid = $('[name="cliente_id"]').val();
  var aprobacionid = $('[name="aprobacion_id"]').val();
  var fechapago =  $('#fecha_pago').val();
  var formapago =  $('#forma_pago').val();
  var nota =  $('#nota').val();
  var num = 0;
  var flag = false;

  for (var i = 1; i < parseInt($('#numero_cobro').val()) + 2; i++) {
    if($('#numcobro'+i).prop('checked')) {
        num++;
    }
    if(!$('#numcobro'+i).prop('disabled')) {
      if($('#numcobro'+i).prop('checked')) {
        flag = true;
      } else {
        flag = false;
      }
    }

  }

/*  alert("cobros: " + parseInt($('#numero_cobro').val()));
  alert("numero: " + num);*/

  if (flag == true) {
    $.ajax({
      url : "<?php echo site_url('cobros/pagos')?>",
      type: "POST",
      data: {"fecha_pago":fechapago,"forma_pago":formapago,"nota":nota,"aprobacion_id":aprobacionid,"clientes_id":clienteid},
      dataType: "JSON",
      success: function(data)
      {
        if (data.status) {
          $('#modal_form_cotizar').modal('hide');
          reload_table();
          if (num > parseInt($('#numero_cobro').val())) {
            finishCobro(clienteid);
          }
        } else if (data.validation) {
          $('.modal-validacion-fecha_pago').html(data.validation);
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      }
    });
  } else {
    $('.modal-fecha_pago').html('<span class="label label-danger animated shake">Seleccione el cobro</span>');
    $('.modal-forma_pago').empty();
    $('.modal-validacion-fecha_pago').empty();
    $('.modal-nota').empty();
  }

}

function finishCobro(id){  
  $.ajax({
    url : "<?php echo site_url('cobros/status_cobro/')?>/" + id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      if (data.status) {
        reload_table();
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}

function finishCobroAdelantado(id){  
  $.ajax({
    url : "<?php echo site_url('cobros/status_cobro_adelantado/')?>/" + id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      if (data.status) {
        reload_table();
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}

function numtotal(){
/*  if($('#numtotal').prop('checked')){
    for (var i = 1; i < parseInt($('#numero_cobro').val()) + 2; i++) {
      $("#numcobro"+i).prop("checked", true);
    }
  } else {
    for (var i = 1; i < parseInt($('#numero_cobro').val()) + 2; i++) {
      $("#numcobro"+i).prop("checked", false);
    }
  }*/

  var aprobacion_id = $('#aprobacion_id').val();
  $.ajax({
    url : "<?php echo site_url('cobros/check_anticipado/')?>/" + aprobacion_id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      if($('#numtotal').prop('checked')){
        for (var i = data.cobros+1; i <= data.cobrosfaltantes + data.cobros; i++) {
          $("#numcobro"+i).prop("checked", true);
          //$("#numcobro"+i).addClass("flat");
          $("#numcobro"+i).parent().addClass("checkboxStyle");
          if(!$('#numcobro'+i).prop('disabled')) {
            $('#numcobro'+i).attr('disabled', true);
          }
        }
        $('#btnPagoAnticipado').show();
        $('#btnProcesarCobro').hide();


        $('.modal-titulo-pago').html('<h2>Cobro Total</h2>');
        $('.modal-fecha_pago').html('<input type="text" name="fecha_pago" id="fecha_pago" class="form-control has-feedback-left date-picker" placeholder="Fecha de Pago"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>');

        $('.modal-forma_pago').html('<select class="form-control" name="forma_pago" id="forma_pago"><option disabled selected hidden>Forma de pago</option><option value="Efectivo">Efectivo</option><option value="Cheque">Cheque</option><option value="Transferencia">Transferencia</option></select>');

        $('.modal-nota').html('<textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>');

        $(document).ready(function() {
          $('#fecha_pago').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: "DD/MM/YYYY",
          }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
          });
        });

/*        $("#forma_pago").select2({
          placeholder: "Forma de pago",
          allowClear: false,
          language: "es",
          theme: "classic",
          dropdownParent: $(".formapago")
        });*/

      } else {
        for (var i = data.cobros+1; i <= data.cobrosfaltantes + data.cobros; i++) {
          $("#numcobro"+i).prop("checked", false);
          $("#numcobro"+i).parent().removeClass("checkboxStyle");
        }
        var numc = data.cobros+1;
        $('#numcobro'+numc).removeAttr('disabled');
        $('#btnPagoAnticipado').hide();
        $('#btnProcesarCobro').show();

        $('.modal-titulo-pago').empty();
        $('.modal-fecha_pago').empty();
        $('.modal-forma_pago').empty();
        $('.modal-nota').empty();
        $('.modal-validacion-fecha_pago').empty();

        
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}

function pagoAnticipado(){

  $('.modal-validacion-fecha_pago').empty();

  var clienteid = $('[name="cliente_id"]').val();
  var aprobacionid = $('[name="aprobacion_id"]').val();
  var fechapago =  $('#fecha_pago').val();
  var formapago =  $('#forma_pago').val();
  var nota =  $('#nota').val();

  $.ajax({
    url : "<?php echo site_url('cobros/pago_anticipado')?>",
    type: "POST",
    data: {"fecha_pago":fechapago,"forma_pago":formapago,"nota":nota,"aprobacion_id":aprobacionid,"clientes_id":clienteid},
    dataType: "JSON",
    success: function(data)
    {

/*      for (var i = 0; i <= data.cobrosfaltantes + data.cobros; i++) {
        $("#numcobro"+i).attr("checked", true);
      }*/

      if (data.status) {
        $('#modal_form_cotizar').modal('hide');
        finishCobroAdelantado(clienteid);
      }
      else if (data.validation) {
        $('.modal-validacion-fecha_pago').html(data.validation);
      }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });

}

function empresas(compania){
  if (compania == "0") {
    table.ajax.url("<?php echo site_url('cobros/ajax_list')?>").load();
  } else {
    table.ajax.url("<?php echo site_url('cobros/ajax_list_empresa/')?>/" + compania).load();
  }
  view_select_cobros();
}


function status_payment(id){
  $('#modal_estado_cobros').modal('show');
  $('#form_estado_cobro')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.aviso-estado-cobro').hide();
  $('.modal-div-cobro').hide();  
  $('.modal-div-cobro1').hide();
  $('[name="clientes_id_cobro"]').val(id);

  $.ajax({
    url : "<?php echo site_url('cobros/consulta_estado_cobro/')?>/" + id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      //alert(data.estado_cobro);
      if (data.estado_cobro != null) {
        $('[name="estado_cobro"]').val(data.estado_cobro);
        estadocobro(data.estado_cobro);
      }

      if (data.agenda) {
        $('[name="fecha_retiro"]').val(data.agenda.fecha_retiro);
        $('[name="hora_retiro"]').val(data.agenda.hora_retiro);
        $('[name="persona_contacto"]').val(data.agenda.persona_contacto);
        $('[name="numero_cheque"]').val(data.agenda.numero_cheque);
        $('[name="monto_cheque"]').val(data.agenda.monto_cheque);
        $('[name="direccion_cobro"]').val(data.agenda.direccion_cobro);
        $('[name="telefono_cobro"]').val(data.agenda.telefono_cobro);
        $('[name="persona_dirigirse"]').val(data.agenda.persona_dirigirse);
      }

      if (data.desempleado) {
        $('[name="fecha_salida"]').val(data.desempleado.fecha_salida);
      }
      
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });



}

function estadocobro(id){
  if (id == 8) {    
    $('.modal-div-cobro1').hide();
    $('.modal-div-cobro').show();
    $('.modal-fecha-retiro').html('<label>Fecha de Retiro</label><input type="text" name="fecha_retiro" id="fecha_retiro" class="form-control has-feedback-left date-picker" placeholder="Fecha de Retiro"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-hora-retiro').html('<label>Hora de Retiro</label><input type="text" name="hora_retiro" id="hora_retiro" class="form-control has-feedback-left" placeholder="Hora de Retiro"><span class="fa fa-clock-o form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-persona-contacto').html('<label>Persona de Contacto</label><input type="text" name="persona_contacto" id="persona_contacto" class="form-control has-feedback-left" placeholder="Persona de Contacto"><span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-numero-cheque').html('<label>Numero de Cheque</label><input type="text" name="numero_cheque" id="numero_cheque" class="form-control has-feedback-left" placeholder="Numero de Cheque"><span class="fa fa-bank form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-monto-cheque').html('<label>Monto del Cheque</label><input type="text" name="monto_cheque" id="monto_cheque" class="form-control has-feedback-left" placeholder="Monto del Cheque"><span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-telefono-cobro').html('<label>Telefono</label><input type="text" name="telefono_cobro" id="telefono_cobro" class="form-control has-feedback-left" placeholder="Telefono"><span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-direccion-cobro').html('<label>Dirección</label><textarea name="direccion_cobro" id="direccion_cobro" class="form-control" placeholder="Dirección"></textarea>');
    $('.modal-nombre-dirigirse').html('<label>Nombre a quien dirigirse</label><input type="text" name="persona_dirigirse" id="persona_dirigirse" class="form-control has-feedback-left" placeholder="Nombre a quien dirigirse"><span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>');


    $(document).ready(function() {
      $('#fecha_retiro').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: "DD/MM/YYYY",
      }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
      });
    });


  } else if (id == 9) {
    $('.modal-div-cobro').hide();
    $('.modal-div-cobro1').show();
    $('.modal-fecha-salida').html('<label>Fecha de Salida</label><input type="text" name="fecha_salida" id="fecha_salida" class="form-control has-feedback-left date-picker" placeholder="Fecha de Salida"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>');

    $(document).ready(function() {
      $('#fecha_salida').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: "DD/MM/YYYY",
      }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
      });
    });

  } else {
    $('.modal-div-cobro').hide();
    $('.modal-div-cobro1').hide();
  }
}

function save_estado_cobro(){
  var cliente_id = $('[name="clientes_id_cobro"]').val();
  var estados_cobro_id = $('#estado_cobro').val(); 
  if (estados_cobro_id == null) {
     $('.aviso-estado-cobro').html('<span class="label label-danger">Seleccione estado</span>').show();
  } else {
    var url;
    var datos;
    if (estados_cobro_id == 8) {
      url = "<?php echo site_url('cobros/save_estado_cobro_agenda')?>";
      datos = $('#form_estado_cobro').serialize();
    } else if (estados_cobro_id == 9) {
      url = "<?php echo site_url('cobros/save_estado_desempleados')?>";
      datos = $('#form_estado_cobro').serialize();
    } else {
      url = "<?php echo site_url('cobros/save_estado_cobro')?>";
      datos = {"estados_cobro_id":estados_cobro_id,"id": cliente_id};
    }
    $.ajax({
      url : url,
      type: "POST",
      data: datos,
      dataType: "JSON",
      success: function(data)
      {
        if (data.status) {
          $('#modal_estado_cobros').modal('hide');
          reload_table();
        } else if (data.validation) {
          $('.aviso-estado-cobro').html(data.validation).show();
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      }
    });
  }
}

</script>


