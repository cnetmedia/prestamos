<style type="text/css">
  .checkboxStyle{
    background-color: #26B99A;
  }
</style>

<!-- page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
            <h2>Total Deudas<small>Listado de Cobros</small></h2>
            <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div class="row">

                    <div class="col-md-4 col-md-offset-5">
                        <div id="reportrange"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 28em;">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                            <span>Rango de fechas</span> <b class="caret"></b>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <select class='form-control' name="filtrar_estado_cliente" id="filtrar_estado_cliente" onchange="">
                            <option value="">Todos</option>
                            <option value="9">Activo</option>
                            <option value="10">Desempleado</option>
                            <option value="11">Sin Garantías</option>
                            <option value="0">Cancelados</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Filtrar</button>
                    </div>
                </div>

            <hr/>
            <span class="permission-error label label-danger"></span>
            <span class="msg-correo"></span>
            <h2 id="msg-analisis-contexto"></h2>
            <hr/>

                <div>
                    <div class="row top_tiles">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <!--div class="icon"><i class="fa fa-money"></i></div-->
                                <div class="count" id="total_bruto" style="font-size:25px">0</div>
                                <h3>Deuda</h3>
                                <p>Total de la deuda de todos los clientes</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <!--div class="icon"><i class="fa fa-money"></i></div-->
                                <div class="count" id="total_neto"  style="font-size:25px">0</div>
                                <h3>Deuda Principal</h3>
                                <p>Total de la deuda principal de todos los clientes</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <!--div class="icon"><i class="fa fa-user"></i></div-->
                                <div class="count" id="total"  style="font-size:25px">0</div>
                                <h3>Prestamos</h3>
                                <p>Cantidad de préstamos</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="counts" style="display:block;">
                    <div class="row top_tiles">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <!--div class="icon"><i class="fa fa-user"></i></div-->
                                <div class="count" id="total_activos" style="font-size:25px">0</div>
                                <h3>Activos</h3>
                                <p>Cantidad de préstamos</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <!--div class="icon"><i class="fa fa-user"></i></div-->
                                <div class="count" id="total_desempleados"  style="font-size:25px">0</div>
                                <h3>Desempleados</h3>
                                <p>Cantidad de préstamos</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <!--div class="icon"><i class="fa fa-user"></i></div-->
                                <div class="count" id="total_sin_garantias"  style="font-size:25px">0</div>
                                <h3>Sin Garantías</h3>
                                <p>Cantidad de préstamos</p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <!--div class="icon"><i class="fa fa-user"></i></div-->
                                <div class="count" id="total_cancelados"  style="font-size:25px">0</div>
                                <h3>Cancelados</h3>
                                <p>Cantidad de préstamos</p>
                            </div>
                        </div>                        
                    </div>
                </div>



                <hr/>

            <table id="datatable-responsive-totaldeudas" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Cliente</th>
                    <th>Fecha desembolso</th>
                    <th>Monto original</th>
                    <th>Total de la deuda *</th>
                    <th>Cargos</th>
                    <th>Cant. de cuotas pendientes</th>
                    <th>Total adeudado **</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th>Cliente</th>
                    <th>Fecha desembolso</th>
                    <th>Monto original</th>
                    <th>Total de la deuda *</th>
                    <th>Cargos</th>
                    <th>Cant. de cuotas pendientes</th>
                    <th>Total adeudado **</th>
                </tr>
                </tfoot>
            </table>

            <div class="row col-xs-12" style="padding: 20px 0 20px 15px;">
                <p><strong>* Solo el principal, sin intereses ni comisiones</strong></p>
                <p><strong>** Con intereses y comisiones</strong></p>
            </div>

            </div>
        </div>
        </div>
    </div>
</div>
<!-- /page content -->


<!-- jQuery -->
<script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

var table;
var from;
var to;

function reload_table()
{
    $('.permission-error').hide();
    refreshData();
    table.ajax.reload(null,false); //reload datatable ajax
}

function refreshData()
{
    $.ajax({
        url: '<?php echo site_url('cobros/ajax_total_deudas')?>',
        method: 'POST',
        dataType: 'json',
        data: {
            from: from,
            to: to,
            estado: $("#filtrar_estado_cliente option:selected").text()
        }
    })
        .done(function (response) {
            showData(response);
        });

    var todos =  $("#filtrar_estado_cliente option:selected").text();
    if  (todos === "Todos") {
        $('#msg-analisis-contexto').html(title_mensaje_todos);
        $('#counts').show();
    } else {
        $('#msg-analisis-contexto').html('Análisis del total de la deuda a partir del estado <b>' + todos + '</b>, desde <b>' + from + '</b> hasta <b>' + to + '</b>.');
        $('#counts').hide();
    }
}

function showData(response) {
    $('#total_neto').text('$' + formatMoney(response.neto));
    $('#total_bruto').text('$' + formatMoney(response.bruto));
    $('#total').text(formatMoney(response.cantidad, 0));
    $('#total_activos').text(formatMoney(response.total_activos, 0));
    $('#total_desempleados').text(formatMoney(response.total_desempleados, 0));
    $('#total_sin_garantias').text(formatMoney(response.total_sin_garantias, 0));
    $('#total_cancelados').text(formatMoney(response.total_cancelados, 0));
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

$(document).ready(function() {

    from = moment().subtract(1, 'months').format('DD/MM/YYYY');
    to = moment().format('DD/MM/YYYY');
    title_mensaje_todos = 'Análisis del total de la deuda a partir de <b>todos</b> los estados, desde <b>' + from + '</b> hasta <b>' + to + '</b>.';

    $('#msg-analisis-contexto').html(title_mensaje_todos);

    $("#filtrar_estado_cliente").select2();

    $.ajax({
        url: '<?php echo site_url('cobros/ajax_total_deudas')?>',
        method: 'POST',
        dataType: 'json',
        data: {
            from: from,
            to: to,
            estado: $("#filtrar_estado_cliente option:selected").text()
        }
    })
        .done(function (response) {
            showData(response);
        });

    //datatables
    table = $('#datatable-responsive-totaldeudas').DataTable( {
        "order": [[ 0, "asc" ]],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('cobros/ajax_list_total_deudas')?>",
            "type": "POST",
            data: function (d) {
                d.estado = $("#filtrar_estado_cliente option:selected").text();
                d.from = from;
                d.to = to;
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    } );


    var cb = function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html('Desde: ' + start.format('DD/MM/YYYY') + ' Hasta: ' + end.format('DD/MM/YYYY'));
    };

    var f = new Date();

    var optionSet1 = {
        endDate: moment(),
        minDate: '01/01/2002',
        maxDate: (f.getMonth() +1) + "/" + (f.getDate()+1) + "/" + f.getFullYear(),
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Último 7 Días': [moment().subtract(6, 'days'), moment()],
            'Último 30 Días': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD/MM/YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Aplicar',
            cancelLabel: 'Cerrar',
            fromLabel: 'Desde',
            toLabel: 'Hasta',
            customRangeLabel: 'Personalizado',
            daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            firstDay: 1
        }
    };

    $('#reportrange span').html('Desde: ' + moment().subtract(1, 'months').format('DD/MM/YYYY') + ' Hasta: ' + moment().format('DD/MM/YYYY'));

    $('#reportrange').daterangepicker(optionSet1, cb);

    $('#reportrange').on('show.daterangepicker', function() {
        console.log("show event fired");
    });
    $('#reportrange').on('hide.daterangepicker', function() {
        console.log("hide event fired");
    });
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
        from = picker.startDate.format('DD/MM/YYYY');
        to = picker.endDate.format('DD/MM/YYYY');
    });

    $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
        console.log("cancel event fired");
    });

    $('#options1').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
    });

    $('#options2').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
    });

    $('#destroy').click(function() {
        $('#reportrange').data('daterangepicker').remove();
    });

});

</script>