<style type="text/css">
  .checkboxStyle{
    background-color: #26B99A;
  }
</style>


        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Gestionar Cobros <small>Listado de Cobros</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row col-lg-8 col-sm-8 col-12">
                      <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>

                      <!-- <a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_morosos()"><i class="fa fa-envelope-o"></i> Morosos </a> -->
                    </div>

                    <div class="row col-lg-2 pull-right selectcompany">
                      <select class='form-control' name="filtrar_empresa" id="filtrar_empresa" onchange="empresas(this.value)"></select>
                    </div>

                    <div class="col-lg-2 pull-right selectcobros">
                      <select class='form-control' name="filtrar_cobro" id="filtrar_cobro" onchange="cobrosxmes(this.value)"></select>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>
                    <span class="msg-correo"></span>

                    <br><br>

                    <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Aprobado</th>
                          <th>Fecha Desembolso</th>
                          <th>Primer Descuento</th>
                          <th>Ultimo Pago</th>
                          <th>Monto</th>
                          <th>Fecha a Cobrar</th>
                          <th>Compañia</th>
                          <th>Estado de Cobro</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Aprobado</th>
                          <th>Fecha Desembolso</th>
                          <th>Primer Descuento</th>
                          <th>Ultimo Pago</th>
                          <th>Monto</th>
                          <th>Fecha a Cobrar</th>
                          <th>Compañia</th>
                          <th>Estado de Cobro</th>
                          <th>Acciones</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- /page content -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_view" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Información del Usuario</h3>
                </div>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-xs-12 bottom">
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <h2 class="modal-nombre text-info"><!-- info --></h2>
                      </div>

                      <div class="clearfix"></div>

                      <div id="tabla-view"></div>

                      <div class="clearfix"></div>

                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <hr>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_creacion">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_modificacion">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-usuarios_id">
                        <!-- info -->
                      </div>
                    </div> 
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_cotizar" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Cobros</h3>
                </div>
                <div class="modal-body">

                  <div class="row">

                    <div class="clearfix"></div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <div class="tabla-info-cobro"></div>
                      <div class="datocobro"></div>
                      <div class="datomoroso"></div>
                      <!-- <div class="infoformcobro"></div> -->
                    </div>
                    <div class="clearfix"></div>

                    <form method="POST" id="formcobro" enctype="multipart/form-data">
                      <input type="hidden" name="aprobacion_id" id="aprobacion_id">
                      <input type="hidden" name="numero_cobro" id="numero_cobro">

                      <!--<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-titulo-pago"></div>-->
                      <div class="col-md-12 col-sm-12 col-xs-12 modal-validacion-fecha_pago"></div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" name="fecha_pago" id="fecha_pago" class="form-control has-feedback-left date-picker" placeholder="Fecha de Pago"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <select class="form-control" name="forma_pago" id="forma_pago"><option disabled selected hidden>Forma de pago</option><option value="Efectivo">Efectivo</option><option value="Cheque">Cheque</option><option value="Transferencia">Transferencia</option></select>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" name="monto_pago" id="monto_pago" class="form-control has-feedback-left date-picker" placeholder="Monto del Pago"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <div class="input-group"><label class="input-group-btn"><span class="btn btn-primary"> Examinar&hellip; <input type="file" name="file" style="display: none;" multiple></span></label><input type="text" name="nombre_archivo" class="form-control" readonly></div>
                      </div>
                    </form>

                  </div>

                  <div class="hidden">
                    <h2>Tabla Referencial de la Cotización</h2>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered dt-responsive nowrap" id="datatable-cotizacion" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th style="text-align: center" hidden><input type="checkbox" id="numtotal" onclick="numtotal()"></th>
                            <th>Fecha de Pagos</th>
                            <th>Capital</th>
                            <th>Principal</th>
                            <th>Interés</th>
                            <th>Tasa de Interés</th>
                            <th>Cargo Administrativo</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody class="filas">
                        </tbody>
                      </table>
                    </div>
                  </div>



                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-success btn-xs" onclick="procesarCobro()" id="btnProcesarCobro">
                    <i class="fa fa-check"></i> Aceptar
                  </button>
<!--                   <button type="button" class="btn btn-success btn-xs" onclick="pagoAnticipado()" id="btnPagoAnticipado">
                    <i class="fa fa-money"></i> Cobrar Todo
                  </button> -->
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->



    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_estado_cobros" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Estado de Cobro</h3>
                </div>
                <div class="modal-body">

                  <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- info --></h2></u></strong>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback aviso-estado-cobro"></div>

                    <form id="form_estado_cobro">

                      <input type="hidden" name="clientes_id_cobro"> 
                      <input type="hidden" name="aprobacion_id_cobro">           

                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <select class='form-control' name="estado_cobro" id="estado_cobro" onchange="estadocobro(this.value)">
                          <option disabled selected hidden>Estado de cobro</option>
                          <?php if (count($estado_cobros)) {
                            foreach ($estado_cobros as $row) {
                              echo "<option value='". $row['id'] . "'>" . $row['estado'] . "</option>";
                            }
                          } ?>
                        </select>
                      </div>

                      <div class="clearfix"></div>

                      <div class="modal-div-cobro">


                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-cantidad-aprobada">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-pago-mensual">
                          <!-- info -->
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-pago-total">
                          <!-- info -->
                        </div>


                        <div class="clearfix"></div>



                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha-retiro">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-hora-retiro">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-persona-contacto">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-numero-cheque">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-monto-cheque">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono-cobro">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-direccion-cobro">
                          <!-- info -->
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-nombre-dirigirse">
                          <!-- info -->
                        </div>
                      </div>

                      <div class="modal-div-cobro1">
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-ultimo-pago">
                          <label>Ultimo pago recibido</label><input type="text" name="ultimo_pago" id="ultimo_pago" class="form-control has-feedback-left" placeholder="Ultimo pago recibido" readonly=""><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha-salida">
                          <!-- info -->
                        </div>
                      </div>

                    </form> 


                  </div>


                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-success btn-xs" onclick="save_estado_cobro()">
                    <i class="fa fa-save"></i> Guardar
                  </button>
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_pagos" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Pagos</h3>
                </div>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="modal-nombre text-info"><!-- info --></h2>
                    </div>
                  </div>

                  <div class="table-responsive company_user">
                    <table class="table table-striped table-bordered jambo_table bulk_action" width="100%">
                      <thead>
                        <tr>
                          <th style="text-align: center"><input type="checkbox" id="totalpagos" onclick="totalpagos()"></th>
                          <th>Deuda</th>
                          <th>Fecha tope</th>
                          <th>Abono</th>
                          <th>Principal</th>
                          <th>Interés pendiente</th>
                          <th>Interés</th>
                          <th>Tasa interés</th>
                          <th>Cargo pendiente</th>
                          <th>Cargo administrativo</th>
                          <th>Cargo mora</th>
                          <th>Total referencial</th>
                          <th>Fecha de pago</th>
                          <th>Forma de pago</th>
                          <th>Monto del pago</th>
                          <th>Archivo</th>
                          <th>Nota</th>
                          <th>Registrado por</th>
                        </tr>
                      </thead>
                      <tbody class="filaspagos">
                      </tbody>
                    </table>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary btn-xs" onclick="confirm_remove()" id="btnProcesarCobro">
                    <i class="fa fa-trash-o"></i> Remover
                  </button>
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea remover el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_impresiones" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Impresiones</h3>
                </div>
                <div class="modal-body">

                  <input type="hidden" name="clieid">
                  <span class="label label-danger aviso"></span>
                  <select class="form-control" name="impresion" id="impresion" onchange="seleccion_reporte(this.value)">
                  </select>
                  <div style="margin-top:" class="fade" id="fecha_descuento">
                    <label>Fecha Primer descuento</label>
                    <input type="text" name="fecha_descuento" class="date-picker" 
                      id="inputSuccess88" placeholder="Seleccione fecha" maxlength="10">
                  </div>

                </div>
                <div class="modal-footer botones_pdf">
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


<!-- Modal para eliminar -->
<div id="modalCorreo" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea enviar los correos?
      </div>
      <div id="infocorreo" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-nota text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="clientes_id"/>

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

              <div class="table-responsive notes_datos">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Fecha</th>
                      <th class="column-title">Nota</th>
                      <th class="column-title">Registrado por</th>
                    </tr>
                  </thead>

                  <tbody class="datos_notas">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>



<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {
    $('#inputSuccess88').datetimepicker({
        format: "DD-MM-YYYY",
    });

    $('#inputSuccess88').on("dp.change",function (e) {
        var fecha = $('#inputSuccess88').val();
        console.log('fecha',fecha);
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '?fecha='+ fecha +'" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '?fecha='+ fecha +'"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    });
    
    //datatables
    table = $('#datatable-responsive-solicitudes').DataTable( {
        "order": [[ 0, "asc" ]],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('cobros/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    } );

    view_companies();
    view_select_cobros();

});

function view_select_cobros()
{
    $('#filtrar_cobro').empty();
    var trHTML = "<option></option>";
    trHTML += "<option value='Todos'>Todos</option>";
    trHTML += "<option value='Mes actual'>Mes actual</option>";
    $('#filtrar_cobro').append(trHTML);

    $("#filtrar_cobro").select2({
      placeholder: "Filtrar Cobros",
      allowClear: false,
      language: "es",
      theme: "classic",
      minimumResultsForSearch: -1,
      dropdownParent: $(".selectcobros")
    });
}


function view_companies()
{
    $.ajax({
        url : "<?php echo site_url('cobros/view_companies')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#filtrar_empresa').empty();
            var trHTML = "<option></option>";
            trHTML += "<option value='0'>Todas</option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#filtrar_empresa').append(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    $("#filtrar_empresa").select2({
      placeholder: "Filtrar por Empresa",
      allowClear: false,
      language: "es",
      theme: "classic",
      dropdownParent: $(".selectcompany")
    });
}

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax 
}

function view_approval(id)
{

    $('.permission-error').hide();
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('cobros/view/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información del Cliente');
            $('.modal-nombre').text(data.nombre + ' ' + data.apellido);

            var htmla = '';
            if (data.aprobacion) {
              htmla = '<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">Aprobación</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td>Cantidad aprobada: $'+data.cantidad_aprobada+'</td>'+
                      '<td>Plazo: '+data.cantidad_meses+' meses</td>'+
                      '<td>Producto: '+data.productos_id+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Fecha de entrega: '+data.fecha_entrega+'</td>'+
                      '<td>Fecha de primer descuento: '+data.fecha_primer_descuento+'</td>'+
                      '<td>Archivo: <a href="files/entregados/'+data.archivo+'" target="_blank">'+data.archivo+'</a></td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Porcentaje: %'+data.porcentaje+'</td>'+
                      '<td>Entregado: '+data.entregado+'</td>'+
                      '<td>Cobrado: '+data.cobrado+'</td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>';
            }

            $('#tabla-view').html('<div class="table-responsive">'+
              '<table class="table table-striped">'+
                '<thead>'+
                  '<tr class="headings">'+
                    '<th colspan="3"><h2 class="text-info">Datos Personales</h2></th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                '</tbody>'+
                  '<tr>'+
                    '<td>Género: '+data.genero+'</td>'+
                    '<td>Cédula: '+data.cedula+'</td>'+
                    '<td>Teléfono: '+data.telefono+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td>Celular: '+data.celular+'</td>'+
                    '<td>Estado Civil: '+data.estado_civil+'</td>'+
                    '<td>Correo Electrónico: '+data.correo+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td>Fecha de Nacimiento: '+data.fecha_nacimiento+'</td>'+
                    '<td>Numero de Dependientes: '+data.numero_dependientes+'</td>'+
                    '<td>Catidad Solicitada: $'+data.cantidad_solicitada+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td>Fuente del Cliente: '+data.fuente_cliente+'</td>'+
                    '<td>Como Escucho de Nosotros: '+data.como_escucho_nosotros+'</td>'+
                    '<td></td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>'+
            '<div class="table-responsive">'+
              '<table class="table table-striped">'+
                '<thead>'+
                  '<tr class="headings">'+
                    '<th colspan="3"><h2 class="text-info">Información Adicional</h2></th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                '</tbody>'+
                  '<tr>'+
                    '<td>Tipo de Vivienda: '+data.tipo_vivienda+'</td>'+
                    '<td>Tiene Hipotecas: '+data.hipotecas+'</td>'+
                    '<td>Banco de la Hipoteca: '+data.banco+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td>Tiene Descuento: '+data.descuento+'</td>'+
                    '<td>Tiempo Restante del Descuento: '+ data.anho_descuento + ' años, ' + data.mes_descuento + ' meses.</td>'+
                    '<td>Capacidad: '+data.capacidad+'</td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>'+
            '<div class="table-responsive">'+
              '<table class="table table-striped">'+
                '<thead>'+
                  '<tr class="headings">'+
                    '<th colspan="3"><h2 class="text-info">Dirección del Cliente</h2></th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                '</tbody>'+
                  '<tr>'+
                    '<td>Corregimiento: '+data.corregimiento+'</td>'+
                    '<td>Distrito: '+data.distrito+'</td>'+
                    '<td>Provincia: '+data.provincia+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td>Dirección: '+data.direccion+'</td>'+
                    '<td></td>'+
                    '<td></td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>'+
            '<div class="table-responsive">'+
              '<table class="table table-striped">'+
                '<thead>'+
                  '<tr class="headings">'+
                    '<th colspan="3"><h2 class="text-info">Información de la Empresa</h2></th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                '</tbody>'+
                  '<tr>'+
                    '<td>Salario: $'+data.salario+'</td>'+
                    '<td>Posición de Trabajo: '+data.posicion_trabajo+'</td>'+
                    '<td>Empresa: '+data.companias_id+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td>Inicio Labores: '+data.inicio_labores+'</td>'+
                    '<td>Tiempo Laborando: '+data.tiempo_laborando+'</td>'+
                    '<td>Nombre del Jefe Directo: '+data.nombre_jefe_directo+'</td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>'+
            '<div class="table-responsive">'+
              '<table class="table table-striped">'+
                '<thead>'+
                  '<tr class="headings">'+
                    '<th colspan="3"><h2 class="text-info">Referencia Personal</h2></th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                '</tbody>'+
                  '<tr>'+
                    '<td>Nombre: '+data.nombre_referencia+'</td>'+
                    '<td>Apellido: '+data.apellido_referencia+'</td>'+
                    '<td>Parentesco: '+data.parentesco_referencia+'</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td>Dirección: '+data.direccion_referencia+'</td>'+
                    '<td>Teléfono: '+data.telefono_referencia+'</td>'+
                    '<td>Celular: '+data.celular_referencia+'</td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>' + htmla);

            $('.modal-fecha_creacion').html('Fecha de Creación: ' + data.fecha_creacion);
            $('.modal-fecha_modificacion').html('Fecha de Modificación: ' + data.fecha_modificacion);
            $('.modal-usuarios_id').html('Creado Por: ' + data.usuarios_id);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });

}

function payment(id){
  //$("#numtotal").prop("checked", false);
  //$('#btnPagoAnticipado').hide();
  //$('#btnProcesarCobro').show();
  $('#formcobro')[0].reset();
  $('.datocobro').empty();
  $('.datomoroso').empty();
  $.ajax({
      url : "<?php echo site_url('cobros/payment_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('#modal_form_cotizar').modal('show');        
        $('.tabla-info-cobro').html('<div class="table-responsive">'+
          '<table class="table table-striped">'+
            '<thead>'+
              '<tr class="headings">'+
                '<th colspan="3"><h2 class="text-info">'+data.nombre_cliente+'</h2></th>'+
              '</tr>'+
            '</thead>'+
            '<tbody>'+
            '</tbody>'+
              '<tr>'+
                '<td>Aprobado: $'+data.cantidad_aprobada+'</td>'+
                '<td>Producto: '+data.nombre_producto+'</td>'+
                '<td>Plazo: '+data.cantidad_meses+' Meses a '+data.porcentaje+' %</td>'+
              '</tr>'+
              '<tr>'+
                '<td>Primer descuento: '+data.fecha_primer_descuento+'</td>'+
                '<td>Pago mensual: $'+data.total_mensual+'</td>'+
                '<td>Compañia: '+data.compania+'</td>'+
              '</tr>'+
              '<tr>'+
                '<td>Deuda pendiente: $'+data.deuda+'</td>'+
                '<td>Interés: $'+data.interes+'</td>'+
                '<td>Cargo administrativo: $'+data.cargo_administrativo+'</td>'+
              '</tr>'+
              '<tr>'+
                '<td>Cargo por Mora: $'+data.interes_mora+'</td>'+
                '<td>Total Pendiente: $'+data.pendiente+'</td>'+
                '<td>Total Sobrante: $'+data.regresar+'</td>'+
              '</tr>'+
            '</tbody>'+
          '</table>'+
        '</div>');
        datocobro(data.id);
        cotiza(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });

  $('#fecha_pago').datetimepicker({
      format: "DD/MM/YYYY"
  });
  $("#fecha_pago").on("dp.change",function (e) {
     verify_fecha_cobro();
  });

}

function datocobro(id){
    $('.datocobro').empty();
    $('.datomoroso').empty();
    $.ajax({
        url : "<?php echo site_url('cobros/verify_cobro/')?>" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {

          if (data.status) {
            $('#formcobro').hide();
            finishCobro(id);
          } else {
            $('#formcobro').show();
          }
          
          $('.datocobro').html('<h2>Cobro Pendiente</h2>'+
            '<div class="table-responsive">'+
              '<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">'+
                '<thead>'+
                  '<tr>'+
                    '<th>Deuda</th>'+
                    '<th>Fecha Correspondiente</th>'+
                    '<th>Principal</th>'+
                    '<th>Interés</th>'+
                    '<th>Tasa de Interés</th>'+
                    '<th>Cargo Administrativo</th>'+
                    '<th>Total</th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                  '<tr>' +
                    '<td align="center">$' + data.deuda.toFixed(2) + '</td>' +
                    '<td align="center">' + data.fecha + '</td>' +
                    '<td>$' + data.principal.toFixed(2) + '</td>' +
                    '<td>$' + data.interes.toFixed(2) + '</td>' +
                    '<td align="center">%' + data.tasa.toFixed(2) + '</td>' +
                    '<td align="center">$' + data.cargo.toFixed(2) + '</td>' +
                    '<td>$' + data.total.toFixed(2) + '</td>' +
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
}

function cotiza(datos){
    $('.modal-titulo-pago').empty();
    //$('#formcobro').hide();
    $('.modal-validacion-fecha_pago').empty();
    $.ajax({
        url : "<?php echo site_url('cobros/cobro/')?>" + datos.id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            $('.filas').empty();
            var totalm = null;
            var trHTML = '';
            $.each(data.data, function (i, item) {
                trHTML += '<tr>' +
                  '<td align="center" hidden><input type="checkbox" value="' + item.num + '" id="numcobro' + item.num + '" onclick="numcobro(' + item.num + ')" disabled></td>' +
                  '<td align="center">' + item.fecha + '</td>' +
                  '<td>$' + item.capital.toFixed(2) + '</td>' +
                  '<td>$' + item.principal.toFixed(2) + '</td>' +
                  '<td>$' + item.interes.toFixed(2) + '</td>' +
                  '<td align="center">%' + item.tasa.toFixed(2) + '</td>' +
                  '<td align="center">$' + item.cargo.toFixed(2) + '</td>' +
                  '<td>$' + item.total.toFixed(2) + '</td>' +
                '</tr>';
                totalm = item.total;
                $('#numero_cobro').val(i);
            });            
            trHTML += '<tr><td hidden></td><td></td><td><span class="label label-warning">$'+ data.finalcapital.toFixed(2) +'</span></td><td><span class="label label-warning">$'+ data.totalprincipal.toFixed(2) +'</span></td><td><span class="label label-info">$'+ data.totalintereses.toFixed(2) +'</span></td><td></td><td align="center"><span class="label label-primary">$'+ data.totalcargo.toFixed(2) +'</span></td><td><span class="label label-success">$'+ data.totalcobrar.toFixed(2) +'</span></td></tr>';
            $('.filas').append(trHTML);

            $('[name="aprobacion_id"]').val(datos.id);

            var num = data.cobros + 1;
            $('#numcobro'+num).removeAttr('disabled');

            if (data.cobros > 0) {
              for (var i = 1; i < num; i++) {
                $("#numcobro"+i).attr("checked", true);
              }
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
    
}

function numcobro(num){

  $('#formcobro')[0].reset();
  if($('#numcobro'+num).prop('checked')) {
    $('.modal-titulo-pago').html('<h2>Cobro Individual</h2>');

    $('#formcobro').show();

    $(document).ready(function() {
      $('#fecha_pago').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: "DD/MM/YYYY",
      }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        verify_fecha_pago();
      });
    });

    $(function() {
      $(document).on('change', ':file', function() {
        var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
      });
      $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {
          var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;
          if( input.length ) {
            input.val(log);
          } else {
            if( log ) alert(log);
          }
        });
      });
    });

  } else {
    $('.modal-titulo-pago').empty();
    //$('#formcobro').hide();
    $('.modal-validacion-fecha_pago').empty();
  }
  
}

/*function procesarCobro(){
  var num = 0;
  var flag = false;

  for (var i = 1; i < parseInt($('#numero_cobro').val()) + 2; i++) {
    if($('#numcobro'+i).prop('checked')) {
        num++;
    }
    if(!$('#numcobro'+i).prop('disabled')) {
      if($('#numcobro'+i).prop('checked')) {
        flag = true;
      } else {
        flag = false;
      }
    }
  }

  if (flag == true) {
    var formData = new FormData($('#formcobro')[0]);
    $.ajax({
      url : "<?php echo site_url('cobros/pagos')?>",
      type: "POST",
      data: formData,
      mimeType: "multipart/form-data",
      contentType: false,
      cache: false,
      processData: false,
      dataType: "JSON",
      success: function(data)
      {
        if (data.status) {
          $('#modal_form_cotizar').modal('hide');
          reload_table();
          if (num > parseInt($('#numero_cobro').val())) {
            finishCobro($('#aprobacion_id').val());
          }
        } else if (data.validation) {
          $('.modal-validacion-fecha_pago').html(data.validation);
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      }
    });
  } else {
    $('.infoformcobro').html('<span class="label label-danger animated shake">Seleccione el cobro</span>');
    $('.modal-validacion-fecha_pago').empty();
  }

}*/

function procesarCobro(){
  var formData = new FormData($('#formcobro')[0]);
  $.ajax({
    url : "<?php echo site_url('cobros/pagos')?>",
    type: "POST",
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    dataType: "JSON",
    success: function(data)
    {
      if (data.status) {
        //$('#modal_form_cotizar').modal('hide');
        payment($('#aprobacion_id').val());
        reload_table();
      } else if (data.validation) {
        $('.modal-validacion-fecha_pago').html(data.validation + '<br><br>');
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}

function finishCobro(id){  
  $.ajax({
    url : "<?php echo site_url('cobros/status_cobro/')?>" + id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      if (data.status) {
        reload_table();
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}

function finishCobroAdelantado(id){  
  $.ajax({
    url : "<?php echo site_url('cobros/status_cobro_adelantado/')?>" + id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      if (data.status) {
        reload_table();
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}

function numtotal(){
  $('#formcobro')[0].reset();
  var aprobacion_id = $('#aprobacion_id').val();
  $.ajax({
    url : "<?php echo site_url('cobros/check_anticipado/')?>" + aprobacion_id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      if($('#numtotal').prop('checked')){
        for (var i = data.cobros+1; i <= data.cobrosfaltantes + data.cobros; i++) {
          $("#numcobro"+i).prop("checked", true);
          //$("#numcobro"+i).addClass("flat");
          $("#numcobro"+i).parent().addClass("checkboxStyle");
          if(!$('#numcobro'+i).prop('disabled')) {
            $('#numcobro'+i).attr('disabled', true);
          }
        }
        $('#btnPagoAnticipado').show();
        $('#btnProcesarCobro').hide();


        $('.modal-titulo-pago').html('<h2>Cobro Total</h2>');
        $('#formcobro').show();

        $(document).ready(function() {
          $('#fecha_pago').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: "DD/MM/YYYY",
          }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            verify_fecha_pago_total();
          });
        });

        $(function() {
          $(document).on('change', ':file', function() {
            var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
          });
          $(document).ready( function() {
            $(':file').on('fileselect', function(event, numFiles, label) {
              var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;
              if( input.length ) {
                input.val(log);
              } else {
                if( log ) alert(log);
              }
            });
          });
        });

      } else {
        for (var i = data.cobros+1; i <= data.cobrosfaltantes + data.cobros; i++) {
          $("#numcobro"+i).prop("checked", false);
          $("#numcobro"+i).parent().removeClass("checkboxStyle");
        }
        var numc = data.cobros+1;
        $('#numcobro'+numc).removeAttr('disabled');
        $('#btnPagoAnticipado').hide();
        $('#btnProcesarCobro').show();

        $('.modal-titulo-pago').empty();
        //$('#formcobro').hide();
        $('.modal-validacion-fecha_pago').empty();

      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}

function pagoAnticipado(){

  var formData = new FormData($('#formcobro')[0]);
  $('.modal-validacion-fecha_pago').empty();

  $.ajax({
    url : "<?php echo site_url('cobros/pago_anticipado')?>",
    type: "POST",
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    dataType: "JSON",
    success: function(data)
    {

      if (data.status) {
        $('#modal_form_cotizar').modal('hide');
        finishCobroAdelantado($('#aprobacion_id').val());
      }
      else if (data.validation) {
        $('.modal-validacion-fecha_pago').html(data.validation);
      }

    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });

}

function empresas(compania_id){
  if (compania_id == "0") {
    table.ajax.url("<?php echo site_url('cobros/ajax_list')?>").load();
  } else {
    table.ajax.url("<?php echo site_url('cobros/ajax_list/')?>" + compania_id).load();
  }
  view_select_cobros();
}

function cobrosxmes(dato){
  if (dato == "Todos") {
    table.ajax.url("<?php echo site_url('cobros/ajax_list')?>").load();
  } else {
    table.ajax.url("<?php echo site_url('cobros/ajax_list_mes_actual')?>").load();
  }
  view_companies();
}

var idAprobacion;
function status_payment(id){
  idAprobacion = id;
  $('#modal_estado_cobros').modal('show');
  $('#form_estado_cobro')[0].reset();
  $('.form-group').removeClass('has-error');
  $('.aviso-estado-cobro').hide();
  $('.modal-div-cobro').hide();  
  $('.modal-div-cobro1').hide();
  $('[name="aprobacion_id_cobro"]').val(id);  

  $.ajax({
    url : "<?php echo site_url('cobros/consulta_estado_cobro/')?>" + id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {

      $('[name="clientes_id_cobro"]').val(data.cliente_id);
      $('.modal-nombre').text(data.cliente);

      if (data.estado_cobro != null) {
        $('[name="estado_cobro"]').val(data.estado_cobro);
        estadocobro(data.estado_cobro);
      }

      if (data.info) {
        $('.modal-cantidad-aprobada').html('<strong><i class="fa fa-usd"></i> Préstamo: </strong>' + data.info.cantidad_aprobada);
        $('.modal-pago-mensual').html('<strong><i class="fa fa-usd"></i> Pago Mensual: </strong>' + data.info.pago_mensual);
        $('.modal-pago-total').html('<strong><i class="fa fa-usd"></i> Pago Total: </strong>' + data.info.pago_total);
      }

      if (data.cobros) {
        $('[name="ultimo_pago"]').val(data.cobros.fecha_correspondiente);
      }


    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });



}

function estadocobro(id){
  if (id == 8) {    
    $('.modal-div-cobro1').hide();
    $('.modal-div-cobro').show();
    $('.modal-fecha-retiro').html('<label>Fecha de Retiro</label><input type="text" name="fecha_retiro" id="fecha_retiro" class="form-control has-feedback-left date-picker" placeholder="Fecha de Retiro"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-hora-retiro').html('<label>Hora de Retiro</label><input type="text" name="hora_retiro" id="hora_retiro" class="form-control has-feedback-left" placeholder="Hora de Retiro"><span class="fa fa-clock-o form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-persona-contacto').html('<label>Persona de Contacto</label><input type="text" name="persona_contacto" id="persona_contacto" class="form-control has-feedback-left" placeholder="Persona de Contacto"><span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-numero-cheque').html('<label>Numero de Cheque</label><input type="text" name="numero_cheque" id="numero_cheque" class="form-control has-feedback-left" placeholder="Numero de Cheque"><span class="fa fa-bank form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-monto-cheque').html('<label>Monto del Cheque</label><input type="text" name="monto_cheque" id="monto_cheque" class="form-control has-feedback-left" placeholder="Monto del Cheque"><span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-telefono-cobro').html('<label>Telefono</label><input type="text" name="telefono_cobro" id="telefono_cobro" class="form-control has-feedback-left" placeholder="Telefono"><span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>');
    $('.modal-direccion-cobro').html('<label>Dirección</label><textarea name="direccion_cobro" id="direccion_cobro" class="form-control" placeholder="Dirección"></textarea>');
    $('.modal-nombre-dirigirse').html('<label>Nombre a quien dirigirse</label><input type="text" name="persona_dirigirse" id="persona_dirigirse" class="form-control has-feedback-left" placeholder="Nombre a quien dirigirse"><span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>');


    $(document).ready(function() {
      $('#fecha_retiro').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: "DD/MM/YYYY",
      }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
      });
    });

    $.ajax({
      url : "<?php echo site_url('cobros/consulta_estado_cobro/')?>" + idAprobacion,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

        if (data.agenda) {
          $('[name="fecha_retiro"]').val(data.agenda.fecha_retiro);
          $('[name="hora_retiro"]').val(data.agenda.hora_retiro);
          $('[name="persona_contacto"]').val(data.agenda.persona_contacto);
          $('[name="numero_cheque"]').val(data.agenda.numero_cheque);
          $('[name="monto_cheque"]').val(data.agenda.monto_cheque);
          $('[name="direccion_cobro"]').val(data.agenda.direccion_cobro);
          $('[name="telefono_cobro"]').val(data.agenda.telefono_cobro);
          $('[name="persona_dirigirse"]').val(data.agenda.persona_dirigirse);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      }
    });


    $.ajax({
      url : "<?php echo site_url('cobros/direccion_compania/')?>" + idAprobacion,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('[name="direccion_cobro"]').val(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      }
    });

  } else if (id == 9) {
    $('.modal-div-cobro').hide();
    $('.modal-div-cobro1').show();

    $('.modal-fecha-salida').html('<label>Fecha de Salida</label><input type="text" name="fecha_salida" id="fecha_salida" class="form-control has-feedback-left date-picker" placeholder="Fecha de Salida"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>');

    $(document).ready(function() {
      $('#fecha_salida').daterangepicker({
        singleDatePicker: true,
        calender_style: "picker_4",
        format: "DD/MM/YYYY",
      }, function(start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
      });
    });

    $.ajax({
      url : "<?php echo site_url('cobros/consulta_estado_cobro/')?>" + idAprobacion,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

        if (data.desempleado) {
          $('[name="fecha_salida"]').val(data.desempleado.fecha_salida);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      }
    });

  } else {
    $('.modal-div-cobro').hide();
    $('.modal-div-cobro1').hide();
  }
}

function save_estado_cobro(){
  var aprobacion_id = $('[name="aprobacion_id_cobro"]').val();
  var estados_cobro_id = $('#estado_cobro').val(); 
  if (estados_cobro_id == null) {
     $('.aviso-estado-cobro').html('<span class="label label-danger">Seleccione estado</span>').show();
  } else {
    var url;
    var datos;
    if (estados_cobro_id == 8) {
      url = "<?php echo site_url('cobros/save_estado_cobro_agenda')?>";
      datos = $('#form_estado_cobro').serialize();
    } else if (estados_cobro_id == 9) {
      url = "<?php echo site_url('cobros/save_estado_desempleados')?>";
      datos = $('#form_estado_cobro').serialize();
    } else {
      url = "<?php echo site_url('cobros/save_estado_cobro')?>";
      datos = {"estados_cobro_id":estados_cobro_id,"id": aprobacion_id};
    }
    $.ajax({
      url : url,
      type: "POST",
      data: datos,
      dataType: "JSON",
      success: function(data)
      {
        if (data.status) {
          $('#modal_estado_cobros').modal('hide');
          reload_table();
        } else if (data.validation) {
          $('.aviso-estado-cobro').html(data.validation).show();
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      }
    });
  }
}


function verify_fecha_cobro(){
  var  aprobacion_id = $('[name="aprobacion_id"]').val();
  $('.datomoroso').empty();
  $.ajax({
      url : "<?php echo site_url('cobros/verify_fecha_cobro')?>",
      type: "POST",
      data: {
        "aprobacion_id":aprobacion_id,
        "fecha": $('#fecha_pago').val()
      },
      dataType: "JSON",
      success: function(data)
      {
          if (data.data) {
            $('.datomoroso').html('<h2>Cobro a Realizar</h2>'+
              '<div class="table-responsive">'+
                '<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">'+
                  '<thead>'+
                    '<tr>'+
                      '<th>Deuda</th>'+
                      '<th>Fecha Correspondiente</th>'+
                      '<th>Principal</th>'+
                      '<th>Interés</th>'+
                      '<th>Tasa de Interés</th>'+
                      '<th>Cargo Administrativo</th>'+
                      '<th>Cargo por Mora</th>'+
                      '<th>Total</th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                    '<tr>' +
                      '<td align="center">$' + data.data.deuda.toFixed(2) + '</td>' +
                      '<td align="center">' + data.data.fecha + '</td>' +
                      '<td>$' + data.data.principal.toFixed(2) + '</td>' +
                      '<td>$' + data.data.interes.toFixed(2) + '</td>' +
                      '<td align="center">%' + data.data.tasa.toFixed(2) + '</td>' +
                      '<td align="center">$' + data.data.cargo.toFixed(2) + '</td>' +
                      '<td align="center">$' + data.data.imora.toFixed(2) + '</td>' +
                      '<td>$' + data.data.total.toFixed(2) + '</td>' +
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function verify_fecha_pago(){
  var  aprobacion_id = $('[name="aprobacion_id"]').val();
  $.ajax({
      url : "<?php echo site_url('cobros/verify_fecha_pago')?>",
      type: "POST",
      data: {
        "aprobacion_id":aprobacion_id,
        "fecha": $('#fecha_pago').val()
      },
      dataType: "JSON",
      success: function(data)
      {
          $('[name="monto_pago"]').val(data.monto_pago.toFixed(2));
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function verify_fecha_pago_total(){
  var  aprobacion_id = $('[name="aprobacion_id"]').val();
  $.ajax({
      url : "<?php echo site_url('cobros/verify_fecha_pago_total')?>",
      type: "POST",
      data: {
        "aprobacion_id":aprobacion_id,
        "fecha": $('#fecha_pago').val()
      },
      dataType: "JSON",
      success: function(data)
      {
          $('[name="monto_pago"]').val(data.monto_pago.toFixed(2));
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function remove_payment(id){
  $('#modal_form_pagos').modal('show');
  $('#totalpagos').prop('checked',false);
  table_payment(id);
}

var numpagos;
var aprobaId;
function table_payment(id){
  aprobaId = id;
  $.ajax({
      url : "<?php echo site_url('cobros/remove_payment/')?>" + aprobaId,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        
        $('.modal-nombre').text(data.nombre_cliente);

        if (data.cobros) {
          $('.filaspagos').empty();
          var trHTML = '';
          var abono = 0;
          var monto = 0;
          $.each(data.cobros, function (i, item) {
              trHTML += '<tr>' +
                  '<td style="text-align: center"><input type="checkbox" id="pago'+i+'" value="'+item.id+'" disabled></td>' +
                  '<td>$'+item.deuda+'</td>' +
                  '<td>'+item.fecha_correspondiente+'</td>' +
                  '<td>$'+item.abono+'</td>' +
                  '<td>$'+item.principal+'</td>' +
                  '<td>$'+item.interes_restante+'</td>' +
                  '<td>$'+item.interes+'</td>' +
                  '<td>%'+item.tasa_interes+'</td>' +
                  '<td>$'+item.cargo_restante+'</td>' +
                  '<td>$'+item.cargo_administrativo+'</td>' +
                  '<td>$'+item.interes_mora+'</td>' +
                  '<td>$'+item.total+'</td>' +
                  '<td>'+item.fecha_pago+'</td>' +
                  '<td>'+item.forma_pago+'</td>' +
                  '<td>$'+item.monto+'</td>' +
                  '<td>'+item.archivo+'</td>' +
                  '<td>'+item.nota+'</td>' +
                  '<td>'+item.usuarios_id+'</td>' +
                '</tr>';
                abono += parseFloat(item.abono);
                monto += parseFloat(item.monto);
          });
          trHTML += '<tr>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td>$'+abono.toFixed(2)+'</td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td>$'+monto.toFixed(2)+'</td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                '</tr>';
          $('.filaspagos').append(trHTML);
          numpagos = data.cobros.length;

          var num = numpagos - 1;
          $('#pago'+num).removeAttr('disabled');

        } else {
          $('.filaspagos').empty();
          var trHTML = '<tr><td colspan="15" style="text-align: center">Lo siento, no hay registros</td></tr>';
          $('.filaspagos').append(trHTML);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}

function totalpagos(){
  if ($('#totalpagos').prop('checked') == true) {
    for (var i = 0; i < numpagos; i++) {
      $('#pago'+i).prop('checked',true);
    }
  } else {
    for (var i = 0; i < numpagos; i++) {
      $('#pago'+i).prop('checked',false);
    }
  }
}

function removerpagos(){
  for (var i = 0; i < numpagos; i++) {
    if ($('#pago'+i).prop('checked')) {
      $.ajax({
          url : "<?php echo site_url('cobros/delete_pago/')?>" + $('#pago'+i).val(),
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              if (data.status)
              {
                  table_payment(aprobaId);
                  reload_table();
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });
    }
  }
  $("#myModal").modal('hide');
}

function confirm_remove() {

   var flag = false;
   for (var i = 0; i < numpagos; i++) {
      if ($('#pago'+i).prop('checked')) {
        flag = true;
      }
    }

    if (flag == true) {
      $('.permission-error').hide();

      $("#myModal").on("show", function() {
          $("#myModal a.btn").on("click", function(e) {
              console.log("button pressed");
              $("#myModal").modal('hide');
          });
      });

      $("#myModal").on("hide", function() {
          $("#myModal a.btn").off("click");
      });
      
      $("#myModal").on("hidden", function() {
          $("#myModal").remove();
      });
      
      $("#myModal").modal({
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true 
      });

      $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="removerpagos()"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
    }

}

/*Modal dentro de modal*/
$(document).ready(function () {
    $('#openBtn').click(function () {
        $('#modal_form').modal()
    });

    $('.modal')
        .on({
            'show.bs.modal': function() {
                var idx = $('.modal:visible').length;
                $(this).css('z-index', 1040 + (10 * idx));
            },
            'shown.bs.modal': function() {
                var idx = ($('.modal:visible').length) - 1;
                $('.modal-backdrop').not('.stacked')
                .css('z-index', 1039 + (10 * idx))
                .addClass('stacked');
            },
            'hidden.bs.modal': function() {
                if ($('.modal:visible').length > 0) {
                    setTimeout(function() {
                        $(document.body).addClass('modal-open');
                    }, 0);
                }
            }
        });
});
/*End Modal dentro de modal*/


function generate_pdf(id,tipo = null){
  $("#modal_impresiones").modal('show');
  $('[name="clieid"]').val(id);
  $('.aviso').text('');
  $('.botones_pdf').empty();
  $.ajax({
    url : "<?php echo site_url('solicitudes/tipos_reportes/')?>" + tipo,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
        $('#impresion').empty();
        var trHTML = "<option disabled selected hidden>Tipos de Reportes</option>";
        $.each(data, function (i, item) {
            trHTML += '<option value="'+item.id+'">'+item.nombre+'</option>';
        });
        $('#impresion').append(trHTML);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error adding / update data');
    }
  });
}

function seleccion_reporte(id){
    $('.botones_pdf').empty();
    if(id==8){
      $('#fecha_descuento').removeClass('fade');
    }else{
      $('#fecha_descuento').addClass('fade');     
    }
    if (id == 1) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_hoja_verificacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_hoja_verificacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 2) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 3) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_condiciones_generales/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_condiciones_generales/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 4) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_revision/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_revision/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 5) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_mensajeria/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_mensajeria/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 6) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_mensajeria_refinianciamineto/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_mensajeria_refinianciamineto/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 7) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_aprobacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_aprobacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 8) {
        /*$('.botones_pdf').html('<a class="btn btn-default btn-xs" id="ver_descuento_gobierno" href="<?=base_url()?>reportes/ver_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" id="des_descuento_gobierno" href="<?=base_url()?>reportes/generar_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>'); */       
    } else if (id == 9) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento_en_blanco/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento_en_blanco/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 10) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_contrato/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_contrato/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 11) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_pagare/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_pagare/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 12) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_no_desembolsado/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_no_desembolsado/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 13) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 14) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_verificar_dispo/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_verificar_dispo/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 15) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_no_desc/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_no_desc/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 16) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_contrato_definido/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_contrato_definido/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 17) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_saldo/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_saldo/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 18) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_recordatorio_pago/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_recordatorio_pago/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 19) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_recordatorio_admin/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_recordatorio_admin/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 20) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_gerentes/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_gerentes/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 21) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_cancelacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_cancelacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 22) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_recibo_pago/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_recibo_pago/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 23) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_constancia_devolucion_intereses/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_constancia_devolucion_intereses/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    }else if (id == 24) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_condiciones_generales_new/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_condiciones_generales_new/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    }
}

$(function() {
  $(document).on('change', ':file', function() {
    var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });
  $(document).ready( function() {
    $(':file').on('fileselect', function(event, numFiles, label) {
      var input = $(this).parents('.input-group').find(':text'),
      log = numFiles > 1 ? numFiles + ' files selected' : label;
      if( input.length ) {
        input.val(log);
      } else {
        if( log ) alert(log);
      }
    });
  });
});



function confirm_morosos(id){
    $("#modalCorreo").modal("show");
    $("#infocorreo").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="morosos('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function morosos(id){
  $('.msg-correo').text('');
  $.ajax({
      url : "<?php echo site_url('reportes/morosos/')?>" + id,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {

          if (data.status) {
            $('.msg-correo').removeClass('label label-danger').addClass('label label-success');
            $('.msg-correo').text('Correo enviado.');
          }
/*          if (data.validation) {
            $('.msg-correo').removeClass('label label-success').addClass('label label-danger');
            $('.msg-correo').text('Empresa sin correo. Registrelo e intente de nuevo.');
          }*/
          if (data.permission) {
            $('.msg-correo').removeClass('label label-success').addClass('label label-danger');
            $('.msg-correo').text('No tienes permisos para enviar estos correos.');
          }
          $("#modalCorreo").modal("hide");
          window.scrollTo(0,0);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function notas_clientes(id){
  $('#modal_form_notas').modal('show');
  $('.modal-title').text('Añadir Nota');
  $('#form_notas')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="clientes_id"]').val(id);
  $('#msg-error-notas').hide();
  //$('.nombrenota').hide();
  //view_notas();
  datos_notas(id);
}

function datos_notas(id){
  $.ajax({
      url : "<?php echo site_url('solicitudes/notas_cliente/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

          $('.modal-nombre-nota').text(data.nombre_cliente);

          $('.datos_notas').empty();
          var trHTML = '';
          if (data.nota.length > 0) {
              $('.notes_datos').show();
              $.each(data.nota, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>'+item.fecha_creacion+'</td>' +
                    '<td>'+item.nota+'</td>' +
                    '<td>'+item.usuarios_id+'</td>' +
                  '</tr>'
              });
          } else {
            $('.notes_datos').hide();
          }
          $('.datos_notas').append(trHTML);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function save_notes(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_notes')?>",
      type: "POST",
      data: $('#form_notas').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            notas_clientes($('[name="clientes_id"]').val());
            $('#msg-error-notas').hide();
          } else if (data.validation) {
            $('#msg-error-notas').show();
            $('.list-errors-notas').html('<div class="animated shake">'+data.validation+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

</script>


