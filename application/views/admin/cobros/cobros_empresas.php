
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Gestionar Cobros <small>Listado de Cobros</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row col-lg-6 col-sm-6 col-12">
                      <button class="btn btn-success btn-xs" onclick="cobrar()"><i class="glyphicon glyphicon-ok"></i> Procesar</button>
                      <?php if ($this->session->userdata('id_rol') == 1) { ?>
                        <button class="btn btn-warning btn-xs" onclick="removerultimopago()"><i class="glyphicon glyphicon-trash"></i> Remover ultimo pago</button>
                      <?php } ?>
                      <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
                    </div>

                    <div class="row col-lg-2 pull-right selectcompany">
                      <select class='form-control' name="filtrar_empresa" id="filtrar_empresa" onchange="empresas(this.value)"></select>
                    </div>

                    <div class="col-lg-2 pull-right selectcobros">
                      <select class='form-control' name="filtrar_cobro" id="filtrar_cobro" onchange="cobrosxmes(this.value)"></select>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>   

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback validation-error"></div>

                    <div class="clearfix"></div><hr>                    

                    <form method="POST" id="form-pago" enctype="multipart/form-data" class="row">
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_pago">
                        <input type="text" name="fecha_pago" id="fecha_pago" class="form-control has-feedback-left date-picker" placeholder="Fecha de Pago"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-forma_pago formapago">
                        <select class="form-control" name="forma_pago" id="forma_pago"><option disabled selected hidden>Forma de pago</option><option value="Efectivo">Efectivo</option><option value="Cheque">Cheque</option><option value="Transferencia">Transferencia</option></select>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-monto_pago">
                        <input type="text" name="monto_pago" id="monto_pago" class="form-control has-feedback-left date-picker" placeholder="Monto del Pago"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-nota">
                        <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-archivo">
                        <div class="input-group">
                          <label class="input-group-btn">
                            <span class="btn btn-primary">
                            Examinar&hellip; <input type="file" name="file" style="display: none;" multiple></span>
                          </label>
                          <input type="text" name="nombre_archivo" class="form-control" readonly>
                        </div>
                      </div>
                    </form>

                    <div class="clearfix"></div>

                    <!-- <br><br> -->

                    <div class="table-responsive">
                      <!-- <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered bulk_action" cellspacing="0" width="100%"> -->
                      <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered bulk_action">
                        <thead>
                          <tr>
                            <th style="text-align: center"><input type="checkbox" id="check-all" onclick="checkall()"></th>
                            <th>Cliente</th>
                            <th>Fecha de Pago</th>
                            <th>Principal</th>
                            <th>Interés</th>
                            <th>Tasa Interés</th>
                            <th>Cargo Administrativo</th>
                            <th>Total Acumulado</th>
                            <th>Pago Mensual</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th style="text-align: center"><input type="checkbox" id="check-all2" onclick="checkall2()"></th>
                            <th>Cliente</th>
                            <th>Fecha de Pago</th>
                            <th>Principal</th>
                            <th>Interés</th>
                            <th>Tasa Interés</th>
                            <th>Cargo Administrativo</th>
                            <th>Total Acumulado</th>
                            <th>Pago Mensual</th>
                        </tr>
                        </tfoot>
                      </table>
                    </div>

                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- /page content -->


    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>



<script type="text/javascript">

var save_method;
var table;
var numfilas;

$(document).ready(function() {

    //datatables
    table = $('#datatable-responsive-solicitudes').DataTable( {
        "order": [],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true,/*
        "serverSide": true,
        "order": [],*/

        "ajax": {
            "url": "<?php echo site_url('cobros/ajax_list_by_empresas')?>",
            "type": "POST",

/*            "data": function (data) {
              console.log(data);

            },*/
            "complete": function(response) {

/*              console.log(response);
              console.log(response.readyState);
              console.log(response.responseText);              
              console.log(response.responseJSON);              
              console.log(response.responseJSON.data);*/         
              //console.log(response.responseJSON.data.length);

              numfilas = response.responseJSON.data.length;

            }
        },
        "columnDefs": [{ 
            "targets": [ 0 ],
            "orderable": false,
        },],
    } );


/*    table.on('draw.dt', function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_flat-green'
      });
    });*/

    $('#form-pago')[0].reset();
    $('#form-pago').hide();

/*    $('#fecha_pago').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_4",
      format: "DD/MM/YYYY",
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
      verify_fecha_pago();
    });*/

    $('#fecha_pago').datetimepicker({
        format: "DD/MM/YYYY"
    });
    $("#fecha_pago").on("dp.change",function (e) {
       //verify_fecha_pago();
    });

    view_companies();
    view_select_cobros();
    $('.validation-error').hide();
    $('#form-pago').hide();

});

$(function() {
  $(document).on('change', ':file', function() {
    var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });
  $(document).ready( function() {
    $(':file').on('fileselect', function(event, numFiles, label) {
      var input = $(this).parents('.input-group').find(':text'),
      log = numFiles > 1 ? numFiles + ' files selected' : label;
      if( input.length ) {
        input.val(log);
      } else {
        if( log ) alert(log);
      }
    });
  });
});

function reload_table()
{
    $('.validation-error').hide();
    $('#form-pago')[0].reset();
    $('#form-pago').hide();
    $('#check-all').prop('checked', false);
    $('#check-all2').prop('checked', false);
    table.ajax.reload(null,false); //reload datatable ajax 
}

function checkall(){
  $('.validation-error').hide();
  if ($('#check-all').prop('checked') == true) {
    $('#check-all2').prop('checked', true);
    $('#form-pago').show();
    $('[name="monto_pago"]').hide();
  } else {
    $('#check-all2').prop('checked', false);
    $('#form-pago')[0].reset();
    $('#form-pago').hide();
    $('[name="monto_pago"]').show();
  }
  checktotal();
  window.scrollTo(0, 0); 
}

function checkall2(){
  $('.validation-error').hide();
  if ($('#check-all2').prop('checked') == true) {
    $('#check-all').prop('checked', true);
    $('#form-pago').show();
    $('[name="monto_pago"]').hide();
  } else {
    $('#check-all').prop('checked', false);
    $('#form-pago')[0].reset();
    $('#form-pago').hide();
    $('[name="monto_pago"]').show();
  }
  checktotal();
  window.scrollTo(0, 0);
}

var numcheck = 0;
function checktotal(){
  numcheck = 0;
  $('.validation-error').hide();
  if ($('#check-all').prop('checked') == true || $('#check-all2').prop('checked') == true) {
    for (var i = 0; i < numfilas; i++) {
      $('#cobro'+i).prop('checked', true);
      if ($('#cobro'+i).prop('checked') == true) {
        numcheck++;
      }
    }
  } else {
    for (var i = 0; i < numfilas; i++) {
      $('#cobro'+i).prop('checked', false);
    }
  }
}

function onecheck(){
  $('.validation-error').hide();
  var flag = false;
  for (var i = 0; i < numfilas; i++) {
    if ($('#cobro'+i).prop('checked') == true) {
      flag = true;
    }
  }
  if (flag == true) {
    $('#form-pago').show();
  } else {
    $('#form-pago')[0].reset();
    $('#form-pago').hide();
  }
  //verify_fecha_pago();
}

function cobrar(){
  var num = 0;
  for (var i = 0; i < numfilas; i++) {
    if ($('#cobro'+i).prop('checked') == true) {
      var formData = null;
      formData = new FormData($('#form-pago')[0]);
      formData.append('aprobacion_id', $('#cobro'+i).val());
      $.ajax({
        url : "<?php echo site_url('cobros/pagos')?>",
        type: "POST",
        data: formData,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {
          //console.log(data);
          if (data.status) {
            num++;
            if (num == numcheck) {
              reload_table();
            }
          } else if (data.validation) {
            $('.validation-error').show();
            $('.validation-error').html(data.validation);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error adding / update data');
        }
      });      
    } else {
      $('.validation-error').show();
      $('.validation-error').html('<span class="label label-danger animated shake">Seleccione el cobro</span>');
    }
  }
}


function verify_fecha_pago(){  
  if ($('#fecha_pago').val()) {
    var sum = 0;
    for (var i = 0; i < numfilas; i++) {
      if ($('#cobro'+i).prop('checked') == true) {
        $.ajax({
            url : "<?php echo site_url('cobros/verify_fecha_pago')?>",
            type: "POST",
            data: {
              "aprobacion_id":$('#cobro'+i).val(),
              "fecha": $('#fecha_pago').val()
            },
            dataType: "JSON",
            success: function(data)
            {
              sum += parseFloat(data.monto_pago.toFixed(2));
              $('[name="monto_pago"]').val(sum);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
      }
    }
    
  } else {
    $('#form-pago')[0].reset();
    //console.log('vacio');
  } 

}

function empresas(id){
  $('.validation-error').hide();
  $('#form-pago')[0].reset();
  $('#form-pago').hide();
  $('#check-all').prop('checked', false);
  $('#check-all2').prop('checked', false);
  if (id == "0") {
    table.ajax.url("<?php echo site_url('cobros/ajax_list_by_empresas')?>").load();
  } else {
    table.ajax.url("<?php echo site_url('cobros/ajax_list_by_empresas/')?>" + id).load();
  }
  view_select_cobros();
}

function view_select_cobros()
{
    $('#filtrar_cobro').empty();
    var trHTML = "<option></option>";
    trHTML += "<option value='Todos'>Todos</option>";
    trHTML += "<option value='Mes actual'>Mes actual</option>";
    $('#filtrar_cobro').append(trHTML);

    $("#filtrar_cobro").select2({
      placeholder: "Filtrar Cobros",
      allowClear: false,
      language: "es",
      theme: "classic",
      minimumResultsForSearch: -1,
      dropdownParent: $(".selectcobros")
    });
}


function cobrosxmes(dato){
  $('.validation-error').hide();
  $('#form-pago')[0].reset();
  $('#form-pago').hide();
  $('#check-all').prop('checked', false);
  $('#check-all2').prop('checked', false);
  if (dato == "Todos") {
    table.ajax.url("<?php echo site_url('cobros/ajax_list_by_empresas')?>").load();
  } else {
    table.ajax.url("<?php echo site_url('cobros/ajax_list_by_empresas_mes')?>").load();
  }
  view_companies();
}

function view_companies()
{
    $.ajax({
        url : "<?php echo site_url('clientes/view_companies')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#filtrar_empresa').empty();
            var trHTML = "<option></option>";
            trHTML += "<option value='0'>Todas</option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#filtrar_empresa').append(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    $("#filtrar_empresa").select2({
      placeholder: "Filtrar por Empresa",
      allowClear: false,
      language: "es",
      theme: "classic",
      dropdownParent: $(".selectcompany")
    });
}

function removerultimopago(){
  var num = 0;
  for (var i = 0; i < numfilas; i++) {
    if ($('#cobro'+i).prop('checked')) {
      $.ajax({
        url : "<?php echo site_url('cobros/delete_ultimo_pago/')?>" + $('#cobro'+i).val(),
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          if (data.status)
          {
            num++;
            if (num == numcheck) {
              reload_table();
            }
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
      });
    }
  }
}

</script>