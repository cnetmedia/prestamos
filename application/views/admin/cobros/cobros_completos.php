<style type="text/css">
  .checkboxStyle{
    background-color: #26B99A;
  }
</style>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cobros Completados <small>Listado de Cobros</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row col-lg-2 col-sm-2 col-12">
                      <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>

                    <br><br>

                    <table id="datatable-responsive-cobros" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Producto</th>
                          <th>Prestamo</th>
                          <th>Pagos</th>
                          <th>Total</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Producto</th>
                          <th>Prestamo</th>
                          <th>Pagos</th>
                          <th>Total</th>
                          <th>Acciones</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- /page content -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_cotizar_info" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Información del Cobro</h3>
                </div>
                <div class="modal-body">

                <input type="hidden" name="cliente_id" id="cliente_id">
                  <input type="hidden" name="aprobacion_id" id="aprobacion_id">
                  <input type="hidden" name="numero_cobro" id="numero_cobro">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- info --></h2></u></strong>
                    </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-suma_prestamo">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-productos_id">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-plazo">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_pago">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-validacion-fecha_pago">
                        <!-- info -->
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-nota">
                        <!-- info -->
                      </div>
                  </div>

                  <table class="table table-striped table-bordered dt-responsive nowrap" id="datatable-cotizacion" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th class="hidden">Cobro</th>
                        <th>Fecha de Pagos</th>
                        <th>Principal</th>
                        <th>Interés</th>
                        <th>Tasa de Interés</th>
                        <th>Cargo Administrativo</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody class="filas">
                    </tbody>
                  </table>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_history" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Historial</h3>
                </div>
                <div class="modal-body">

                    <table id="datatable-responsive-bitacora" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Usuario</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Fecha</th>
                          <th>Usuario</th>
                          <th>Acción</th>
                      </tr>
                      </tfoot>
                    </table>



                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_pagos" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Pagos</h3>
                </div>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="modal-nombre text-info"><!-- info --></h2>
                    </div>
                  </div>

                  <div class="table-responsive company_user">
                    <table class="table table-striped table-bordered jambo_table bulk_action" width="100%">
                      <thead>
                        <tr>
                          <th style="text-align: center"><input type="checkbox" id="totalpagos" onclick="totalpagos()"></th>
                          <th>Deuda</th>
                          <th>Fecha tope</th>
                          <th>Abono</th>
                          <th>Principal</th>
                          <th>Interés pendiente</th>
                          <th>Interés</th>
                          <th>Tasa interés</th>
                          <th>Cargo pendiente</th>
                          <th>Cargo administrativo</th>
                          <th>Cargo mora</th>
                          <th>Total</th>
                          <th>Fecha de pago</th>
                          <th>Forma de pago</th>
                          <th>Monto</th>
                          <th>Archivo</th>
                          <th>Nota</th>
                          <th>Registrado por</th>
                        </tr>
                      </thead>
                      <tbody class="filaspagos">
                      </tbody>
                    </table>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary btn-xs" onclick="confirm_remove()" id="btnProcesarCobro">
                    <i class="fa fa-trash-o"></i> Remover
                  </button>
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->



    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_cotizar" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Cobros</h3>
                </div>
                <div class="modal-body">

                  <div class="row">

                    <div class="clearfix"></div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <div class="tabla-info-cobro"></div>
                      <div class="datocobro"></div>
                      <div class="datomoroso"></div>
                      <!-- <div class="infoformcobro"></div> -->
                    </div>
                    <div class="clearfix"></div>

                    <form method="POST" id="formcobro" enctype="multipart/form-data">
                      <input type="hidden" name="aprobacion_id" id="aprobacion_id">
                      <input type="hidden" name="numero_cobro" id="numero_cobro">

                      <!--<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-titulo-pago"></div>-->
                      <div class="col-md-12 col-sm-12 col-xs-12 modal-validacion-fecha_pago"></div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" name="fecha_pago" id="fecha_pago" class="form-control has-feedback-left date-picker" placeholder="Fecha de Pago"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <select class="form-control" name="forma_pago" id="forma_pago"><option disabled selected hidden>Forma de pago</option><option value="Efectivo">Efectivo</option><option value="Cheque">Cheque</option><option value="Transferencia">Transferencia</option></select>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <input type="text" name="monto_pago" id="monto_pago" class="form-control has-feedback-left date-picker" placeholder="Monto del Pago"><span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <div class="input-group"><label class="input-group-btn"><span class="btn btn-primary"> Examinar&hellip; <input type="file" name="file" style="display: none;" multiple></span></label><input type="text" name="nombre_archivo" class="form-control" readonly></div>
                      </div>
                    </form>

                  </div>

                  <div class="hidden">
                    <h2>Tabla Referencial de la Cotización</h2>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered dt-responsive nowrap" id="datatable-cotizacion" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th style="text-align: center" hidden><input type="checkbox" id="numtotal" onclick="numtotal()"></th>
                            <th>Fecha de Pagos</th>
                            <th>Capital</th>
                            <th>Principal</th>
                            <th>Interés</th>
                            <th>Tasa de Interés</th>
                            <th>Cargo Administrativo</th>
                            <th>Total</th>
                          </tr>
                        </thead>
                        <tbody class="filas">
                        </tbody>
                      </table>
                    </div>
                  </div>



                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-success btn-xs" onclick="procesarCobro()" id="btnProcesarCobro">
                    <i class="fa fa-check"></i> Aceptar
                  </button>
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->



<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea remover el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->



<!-- Modal para eliminar -->
<div id="modalCorreo" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea enviar el correo?
      </div>
      <div id="infocorreo" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_impresiones" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Impresiones</h3>
            </div>
            <div class="modal-body">

              <input type="hidden" name="clieid">
              <span class="label label-danger aviso"></span>
              <select class="form-control" name="impresion" id="impresion" onchange="seleccion_reporte(this.value)">
                
              </select>
              <div style="margin-top:" class="fade" id="fecha_descuento">
                <label>Fecha Primer descuento</label>
                <input type="text" name="fecha_descuento" class="date-picker" 
                  id="inputSuccess88" placeholder="Seleccione fecha" maxlength="10">
              </div>
            </div>
            <div class="modal-footer botones_pdf">
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-nota text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="clientes_id"/>

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

              <div class="table-responsive notes_datos">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Fecha</th>
                      <th class="column-title">Nota</th>
                      <th class="column-title">Registrado por</th>
                    </tr>
                  </thead>

                  <tbody class="datos_notas">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->
    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

var table;
var tablehistory;

$(document).ready(function() {
    $('#inputSuccess88').datetimepicker({
        format: "DD-MM-YYYY",
    });

    $('#inputSuccess88').on("dp.change",function (e) {
        var fecha = $('#inputSuccess88').val();
        console.log('fecha',fecha);
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '?fecha='+ fecha +'" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '?fecha='+ fecha +'"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    });

    table = $('#datatable-responsive-cobros').DataTable( {
        "order": [],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true,
        "ajax": {
            "url": "<?php echo site_url('cobros/ajax_list_completos')?>",
            "type": "POST"
        }
    } );

    tablehistory = $('#datatable-responsive-bitacora').DataTable( {
        "order": [[ 0, "desc" ]],
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true,
    } );


});

function generate_pdf(id,tipo = null){
  $("#modal_impresiones").modal('show');
  $('[name="clieid"]').val(id);
  $('.aviso').text('');
  $('.botones_pdf').empty();
  $.ajax({
    url : "<?php echo site_url('solicitudes/tipos_reportes/')?>" + tipo,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
        $('#impresion').empty();
        var trHTML = "<option disabled selected hidden>Tipos de Reportes</option>";
        $.each(data, function (i, item) {
            trHTML += '<option value="'+item.id+'">'+item.nombre+'</option>';
        });
        $('#impresion').append(trHTML);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error adding / update data');
    }
  });
}

function seleccion_reporte(id){
    $('.botones_pdf').empty();
    if(id==8){
      $('#fecha_descuento').removeClass('fade');
    }else{
      $('#fecha_descuento').addClass('fade');     
    }
    if (id == 1) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_hoja_verificacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_hoja_verificacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 2) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 3) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_condiciones_generales/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_condiciones_generales/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 4) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_revision/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_revision/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 5) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_mensajeria/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_mensajeria/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 6) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_mensajeria_refinianciamineto/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_mensajeria_refinianciamineto/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 7) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_aprobacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_aprobacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 8) {
        /*$('.botones_pdf').html('<a class="btn btn-default btn-xs" id="ver_descuento_gobierno" href="<?=base_url()?>reportes/ver_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" id="des_descuento_gobierno" href="<?=base_url()?>reportes/generar_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>'); */       
    } else if (id == 9) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento_en_blanco/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento_en_blanco/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 10) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_contrato/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_contrato/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 11) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_pagare/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_pagare/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 12) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_no_desembolsado/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_no_desembolsado/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 13) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 14) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_verificar_dispo/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_verificar_dispo/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 15) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_no_desc/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_no_desc/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 16) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_contrato_definido/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_contrato_definido/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 17) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_saldo/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_saldo/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 18) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_recordatorio_pago/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_recordatorio_pago/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 19) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_recordatorio_admin/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_recordatorio_admin/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 20) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_gerentes/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_gerentes/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 21) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_cancelacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_cancelacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 22) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_recibo_pago/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_recibo_pago/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 23) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_constancia_devolucion_intereses/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_constancia_devolucion_intereses/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    }else if (id == 24) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_condiciones_generales_new/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_condiciones_generales_new/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    }
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function info_collection(id){
  $.ajax({
      url : "<?php echo site_url('cobros/payment_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('#modal_form_cotizar_info').modal('show');
        $('.modal-nombre').text(data.nombre_cliente);
        $('.modal-suma_prestamo').html('<strong><i class="fa fa-usd"></i> Aprobado: </strong>' + data.cantidad_aprobada);
        $('.modal-productos_id').html('<strong><i class="fa fa-product-hunt"></i> Producto: </strong>' + data.nombre_producto);
        $('.modal-plazo').html('<strong><i class="fa fa-sun-o"></i> Plazo: </strong>' + data.cantidad_meses + " Meses a " + data.porcentaje + "%" );


        $('.modal-fecha_pago').html('<label class="label label-success">Ultimos ' + data.total_cobros + ' Pagos adelantados</label>');

        cotiza(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function cotiza(datos){
    //$('.modal-fecha_pago').empty();
    $('.modal-nota').empty();
    $('.modal-validacion-fecha_pago').empty();
    $.ajax({
        url : "<?php echo site_url('cobros/cobro/')?>" + datos.id,
        type: "POST",
        //data: datos,
        dataType: "JSON",
        success: function(data)
        {
            $('.filas').empty();
            var totalm = null;
            var trHTML = '';
            $.each(data.data, function (i, item) {
                trHTML += '<tr>' +
                  '<td align="center" class="hidden"><input type="checkbox" value="' + item.num + '" id="numcobro' + item.num + '" onclick="numcobro(' + item.num + ')" disabled></td>' +
                  '<td>' + item.fecha + '</td>' +
                  '<td>$' + item.principal.toFixed(2) + '</td>' +
                  '<td>$' + item.interes.toFixed(2) + '</td>' +
                  '<td align="center">%' + item.tasa.toFixed(2) + '</td>' +
                  '<td align="center">$' + item.cargo.toFixed(2) + '</td>' +
                  '<td>$' + item.total.toFixed(2) + '</td>' +
                '</tr>';
                totalm = item.total;
                $('#numero_cobro').val(i);
            });            
            trHTML += '<tr><td class="hidden"></td><td></td><td><span class="label label-warning">$'+ data.totalprincipal.toFixed(2) +'</span></td><td><span class="label label-info">$'+ data.totalintereses.toFixed(2) +'</span></td><td></td><td align="center"><span class="label label-primary">$'+ data.totalcargo.toFixed(2) +'</span></td><td><span class="label label-success">$'+ data.totalcobrar.toFixed(2) +'</span></td></tr>';
            $('.filas').append(trHTML);

            $('[name="cliente_id"]').val(datos.clientes_id);
            $('[name="aprobacion_id"]').val(datos.id);

            var num = data.cobros + 1;
            $('#numcobro'+num).removeAttr('disabled');

            if (data.cobros > 0) {
              for (var i = 1; i < num; i++) {
                $("#numcobro"+i).attr("checked", true);
              }
            }

            if (datos.total_cobros > 1) {
              var ini = num - datos.total_cobros;
              for (var i = ini; i < num; i++) {
                $("#numcobro"+i).parent().addClass("checkboxStyle");
              }
            } else {
              $('.modal-fecha_pago').empty();
            }            

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
}

function history(id){
  tablehistory.ajax.url("<?php echo site_url('bitacora/ajax_list_by_cliente/')?>/" + id).load();
  $('#modal_history').modal('show');
  tablehistory.ajax.reload(null,false);
}


function remove_payment(id){
  $('#modal_form_pagos').modal('show');
  $('#totalpagos').prop('checked',false);
  table_payment(id);
}

function totalpagos(){
  if ($('#totalpagos').prop('checked') == true) {
    for (var i = 0; i < numpagos; i++) {
      $('#pago'+i).prop('checked',true);
    }
  } else {
    for (var i = 0; i < numpagos; i++) {
      $('#pago'+i).prop('checked',false);
    }
  }
}

var numpagos;
var aprobaId;
function table_payment(id){
  aprobaId = id;
  $.ajax({
      url : "<?php echo site_url('cobros/remove_payment/')?>" + aprobaId,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        
        $('.modal-nombre').text(data.nombre_cliente);

        if (data.cobros) {
          $('.filaspagos').empty();
          var trHTML = '';
          var abono = 0;
          var monto = 0;
          $.each(data.cobros, function (i, item) {
              trHTML += '<tr>' +
                  '<td style="text-align: center"><input type="checkbox" id="pago'+i+'" value="'+item.id+'" disabled></td>' +
                  '<td>$'+item.deuda+'</td>' +
                  '<td>'+item.fecha_correspondiente+'</td>' +
                  '<td>$'+item.abono+'</td>' +
                  '<td>$'+item.principal+'</td>' +
                  '<td>$'+item.interes_restante+'</td>' +
                  '<td>$'+item.interes+'</td>' +
                  '<td>%'+item.tasa_interes+'</td>' +
                  '<td>$'+item.cargo_restante+'</td>' +
                  '<td>$'+item.cargo_administrativo+'</td>' +
                  '<td>$'+item.interes_mora+'</td>' +
                  '<td>$'+item.total+'</td>' +
                  '<td>'+item.fecha_pago+'</td>' +
                  '<td>'+item.forma_pago+'</td>' +
                  '<td>$'+item.monto+'</td>' +
                  '<td>'+item.archivo+'</td>' +
                  '<td>'+item.nota+'</td>' +
                  '<td>'+item.usuarios_id+'</td>' +
                '</tr>';
                abono += parseFloat(item.abono);
                monto += parseFloat(item.monto);
          });
          trHTML += '<tr>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td>$'+abono.toFixed(2)+'</td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td>$'+monto.toFixed(2)+'</td>' +
                  '<td></td>' +
                  '<td></td>' +
                  '<td></td>' +
                '</tr>';
          $('.filaspagos').append(trHTML);
          numpagos = data.cobros.length;

          var num = numpagos - 1;
          $('#pago'+num).removeAttr('disabled');

        } else {
          $('.filaspagos').empty();
          var trHTML = '<tr><td colspan="15" style="text-align: center">Lo siento, no hay registros</td></tr>';
          $('.filaspagos').append(trHTML);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}


function confirm_remove() {

   var flag = false;
   for (var i = 0; i < numpagos; i++) {
      if ($('#pago'+i).prop('checked')) {
        flag = true;
      }
    }

    if (flag == true) {
      $('.permission-error').hide();
      $("#myModal").modal("show");
      $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="removerpagos()"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
    }

}

function removerpagos(){
  for (var i = 0; i < numpagos; i++) {
    if ($('#pago'+i).prop('checked')) {
      $.ajax({
          url : "<?php echo site_url('cobros/delete_pago_complet/')?>" + $('#pago'+i).val(),
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              if (data.status)
              {
                  table_payment(aprobaId);
                  reload_table();
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });
    }
  }
  $("#myModal").modal('hide');
}



function payment(id){
  //$("#numtotal").prop("checked", false);
  //$('#btnPagoAnticipado').hide();
  //$('#btnProcesarCobro').show();
  //$('#formcobro')[0].reset();
  $('#formcobro')[0].reset();
  $('.datocobro').empty();
  $('.datomoroso').empty();
  $.ajax({
      url : "<?php echo site_url('cobros/payment_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('#modal_form_cotizar').modal('show');        
        $('.tabla-info-cobro').html('<div class="table-responsive">'+
          '<table class="table table-striped">'+
            '<thead>'+
              '<tr class="headings">'+
                '<th colspan="3"><h2 class="text-info">'+data.nombre_cliente+'</h2></th>'+
              '</tr>'+
            '</thead>'+
            '<tbody>'+
            '</tbody>'+
              '<tr>'+
                '<td>Aprobado: $'+data.cantidad_aprobada+'</td>'+
                '<td>Producto: '+data.nombre_producto+'</td>'+
                '<td>Plazo: '+data.cantidad_meses+' Meses a '+data.porcentaje+' %</td>'+
              '</tr>'+
              '<tr>'+
                '<td>Primer descuento: '+data.fecha_primer_descuento+'</td>'+
                '<td>Pago mensual: $'+data.total_mensual+'</td>'+
                '<td>Compañia: '+data.compania+'</td>'+
              '</tr>'+
              '<tr>'+
                '<td>Deuda pendiente: $'+data.deuda+'</td>'+
                '<td>Interés: $'+data.interes+'</td>'+
                '<td>Cargo administrativo: $'+data.cargo_administrativo+'</td>'+
              '</tr>'+
              '<tr>'+
                '<td>Cargo por Mora: $'+data.interes_mora+'</td>'+
                '<td>Total Pendiente: $'+data.pendiente+'</td>'+
                '<td>Total Sobrante: $'+data.regresar+'</td>'+
              '</tr>'+
            '</tbody>'+
          '</table>'+
        '</div>');
        datocobro(data.id);
        cotiza(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });

  $('#fecha_pago').datetimepicker({
      format: "DD/MM/YYYY"
  });
  $("#fecha_pago").on("dp.change",function (e) {
     verify_fecha_cobro();
  });

}

function datocobro(id){
    $('.datocobro').empty();
    $('.datomoroso').empty();
    $.ajax({
        url : "<?php echo site_url('cobros/verify_cobro/')?>" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {

         
          $('.datocobro').html('<h2>Cobro Pendiente</h2>'+
            '<div class="table-responsive">'+
              '<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">'+
                '<thead>'+
                  '<tr>'+
                    '<th>Deuda</th>'+
                    '<th>Fecha Correspondiente</th>'+
                    '<th>Principal</th>'+
                    '<th>Interés</th>'+
                    '<th>Tasa de Interés</th>'+
                    '<th>Cargo Administrativo</th>'+
                    '<th>Total</th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody>'+
                  '<tr>' +
                    '<td align="center">$' + data.deuda.toFixed(2) + '</td>' +
                    '<td align="center">' + data.fecha + '</td>' +
                    '<td>$' + data.principal.toFixed(2) + '</td>' +
                    '<td>$' + data.interes.toFixed(2) + '</td>' +
                    '<td align="center">%' + data.tasa.toFixed(2) + '</td>' +
                    '<td align="center">$' + data.cargo.toFixed(2) + '</td>' +
                    '<td>$' + data.total.toFixed(2) + '</td>' +
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
}


function procesarCobro(){
  var formData = new FormData($('#formcobro')[0]);
  $.ajax({
    url : "<?php echo site_url('cobros/pagos')?>",
    type: "POST",
    data: formData,
    mimeType: "multipart/form-data",
    contentType: false,
    cache: false,
    processData: false,
    dataType: "JSON",
    success: function(data)
    {
      if (data.status) {
        //$('#modal_form_cotizar').modal('hide');
        payment($('#aprobacion_id').val());
        reload_table();
      } else if (data.validation) {
        $('.modal-validacion-fecha_pago').html(data.validation + '<br><br>');
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}


function verify_fecha_cobro(){
  var  aprobacion_id = $('[name="aprobacion_id"]').val();
  $('.datomoroso').empty();
  $.ajax({
      url : "<?php echo site_url('cobros/verify_cobro_completo')?>",
      type: "POST",
      data: {
        "aprobacion_id":aprobacion_id,
        "fecha": $('#fecha_pago').val()
      },
      dataType: "JSON",
      success: function(data)
      {
          if (data.data) {
            $('.datomoroso').html('<h2>Cobro a Realizar</h2>'+
              '<div class="table-responsive">'+
                '<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">'+
                  '<thead>'+
                    '<tr>'+
                      '<th>Deuda</th>'+
                      '<th>Fecha Correspondiente</th>'+
                      '<th>Principal</th>'+
                      '<th>Interés</th>'+
                      '<th>Tasa de Interés</th>'+
                      '<th>Cargo Administrativo</th>'+
                      '<th>Cargo por Mora</th>'+
                      '<th>Total</th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                    '<tr>' +
                      '<td align="center">$' + data.data.deuda.toFixed(2) + '</td>' +
                      '<td align="center">' + data.data.fecha + '</td>' +
                      '<td>$' + data.data.principal.toFixed(2) + '</td>' +
                      '<td>$' + data.data.interes.toFixed(2) + '</td>' +
                      '<td align="center">%' + data.data.tasa.toFixed(2) + '</td>' +
                      '<td align="center">$' + data.data.cargo.toFixed(2) + '</td>' +
                      '<td align="center">$' + data.data.imora.toFixed(2) + '</td>' +
                      '<td>$' + data.data.total.toFixed(2) + '</td>' +
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}


function confirm_cancelado(id){
    $("#modalCorreo").modal("show");
    $("#infocorreo").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="prestamo_cancelado('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}


function prestamo_cancelado(id){
  $('.permission-error').hide();
  $('.permission-error').text('');
  $.ajax({
      url : "<?php echo site_url('reportes/prestamo_cancelado')?>",
      type: "POST",
      data: {'aprobacion_id' : id},
      dataType: "JSON",
      success: function(data)
      {

          $('.permission-error').show();
          if (data.status) {
            $('.permission-error').removeClass('label label-danger').addClass('label label-success');
            $('.permission-error').text('Correo enviado.');
          }
          if (data.validation) {
            $('.permission-error').removeClass('label label-success').addClass('label label-danger');
            $('.permission-error').text('Empresa sin correo. Registrelo e intente de nuevo.');
          }
          $("#modalCorreo").modal("hide");
          window.scrollTo(0,0);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function notas_clientes(id){
  $('#modal_form_notas').modal('show');
  $('.modal-title').text('Añadir Nota');
  $('#form_notas')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="clientes_id"]').val(id);
  $('#msg-error-notas').hide();
  //$('.nombrenota').hide();
  //view_notas();
  datos_notas(id);
}

function datos_notas(id){
  $.ajax({
      url : "<?php echo site_url('solicitudes/notas_cliente/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

          $('.modal-nombre-nota').text(data.nombre_cliente);

          $('.datos_notas').empty();
          var trHTML = '';
          if (data.nota.length > 0) {
              $('.notes_datos').show();
              $.each(data.nota, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>'+item.fecha_creacion+'</td>' +
                    '<td>'+item.nota+'</td>' +
                    '<td>'+item.usuarios_id+'</td>' +
                  '</tr>'
              });
          } else {
            $('.notes_datos').hide();
          }
          $('.datos_notas').append(trHTML);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function save_notes(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_notes')?>",
      type: "POST",
      data: $('#form_notas').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            notas_clientes($('[name="clientes_id"]').val());
            $('#msg-error-notas').hide();
          } else if (data.validation) {
            $('#msg-error-notas').show();
            $('.list-errors-notas').html('<div class="animated shake">'+data.validation+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

</script>