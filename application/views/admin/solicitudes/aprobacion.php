  <style>
    #div_carga{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      /*background: url(<?php echo site_url('assets/images/gris.png') ?>) repeat;*/
      display:none;
      z-index:1;
    }

    #cargador{
      position:absolute;
      top:50%;
      left: 50%;
      margin-top: -25px;
      margin-left: -25px;
    }

    #cargador_aviso{
      position:absolute;
      top:40%;
      left: 47%;
      margin-top: -25px;
      margin-left: -25px;
    }
  </style>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row" id="list-aprobacion">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Solicitudes Por Aprobar <small>Listado de Solicitudes</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>

                    <div class="row col-lg-3 pull-right">
                      <select class='form-control' name="filtrar_disposiciones" id="filtrar_disposiciones" onchange="filtrar_disposiciones(this.value)">
                      </select>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>

                    <br><br>

                    <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Compañia</th>
                          <th>Solicitado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Compañia</th>
                          <th>Solicitado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Acciones</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
          </div>



            <div class="row" id="div-aprobacion">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Solicitudes <small>Por Aprobar</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <button class="btn btn-default btn-xs" onclick="refrescar_datos()"><i class="glyphicon glyphicon-refresh"></i> Refrescar Datos</button>

                    <input type="hidden" value="" name="clientes_id"/>
                    <input type="hidden" value="" name="solicitud_id"/>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- info --></h2></u></strong>
                    </div>

                    <div class="clearfix"></div>

                    <div id="tabla-view"></div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <hr>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_creacion">
                      <!-- info -->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_modificacion">
                      <!-- info -->
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-usuarios_id">
                      <!-- info -->
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <hr>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="text-info">Información del Prestamo</h2>
                    </div>

                    <div class="clearfix"></div>

                    <div class="info-prestamo"></div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <hr>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="text-info">Verificación del Cliente</h2>
                    </div>

                    
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                          <thead>
                            <tr>
                              <th>COMENTARIO CSS</th>
                              <th>ARCHIVO APC</th>
                              <th>COMENTARIO APC</th>
                              <th>COMENTARIO COMPAÑIA</th>
                              <th>ARCHIVO RP</th>
                              <th>COMENTARIO RP</th>
                            </tr>
                          </thead>
                          <tbody class="view_request_table">
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="text-info">Aprobación de Prestamo</h2>
                    </div>

                    <div class="clearfix"></div>

                    <div class="apro-prestamo"></div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-msg">
                      <!-- espacio -->
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <hr>
                    </div>

                  </div>


                  <button type="button" onclick="rechazar()" class="btn btn-warning btn-xs pull-right"><i class="fa fa-close"></i> Rechazar</button>
                  <button type="button" onclick="cancelar()" class="btn btn-danger btn-xs pull-right"><i class="fa fa-close"></i> Cancelar</button>
                  <button type="button" onclick="file_client()" class="btn btn-primary btn-xs pull-right"><i class="fa fa-file"></i> Archivos</button>
                  <button type="button" onclick="cotizacion()" class="btn btn-info btn-xs pull-right"><i class="fa fa-usd"></i> Cotizar</button>
                  <button type="button" onclick="confirmar_aprobacion()" class="btn btn-success btn-xs pull-right"><i class="fa fa-check"></i> Aprobar</button>
                  <button type="button" onclick="notas_clientes_form()" class="btn btn-warning btn-xs pull-right"><i class="fa fa-file"></i> Notas</button>
                  <button type="button" onclick="disposiciones_clientes_form()" class="btn btn-warning btn-xs pull-right"><i class="fa fa-bookmark"></i> Disposiciones</button>

                </div>
              </div>
            </div>




        </div>
        <!-- /page content -->

<!-- Bootstrap modal -->
<!-- <div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Solicitud</h3>
            </div>
            <div class="modal-body form">

              <form id="form" class="form-horizontal form-label-left input_mask">
              <input type="hidden" value="" name="id" id="idc" />
              <input type="hidden" value="" name="solicitud_id" id="ids" />

                <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors animated shake"></div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="nombre" required class="form-control has-feedback-left" id="inputSuccess" placeholder="Nombre" disabled>
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="apellido" required class="form-control" id="inputSuccess1" placeholder="Apellido" disabled>
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="email" name="correo" required class="form-control has-feedback-left" id="inputSuccess2" placeholder="Correo Electrónico" disabled>
                  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="telefono" required class="form-control has-feedback-left" id="inputSuccess3" placeholder="Teléfono" onkeypress="return justNumbers(event);" disabled>
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="cedula" required class="form-control" id="inputSuccess4" placeholder="Cédula"  onkeyup="validarCedula(this.value)" maxlength="11" data-toggle="tooltip" data-placement="top" title="Cédula. Ejemplo: 0-00000000, P-00000000, E-00000000, N-0000000, PE-00000000">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="salario" required class="form-control has-feedback-left" id="inputSuccess5" placeholder="Salario" disabled>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="inicio_labores" required class="form-control has-feedback-left date-picker" id="inputSuccess281" placeholder="Inicio de Labores">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="tiempo_laborando" required class="form-control" id="inputSuccess6" placeholder="Tiempo Laborando" readonly="">
                  <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <select required class="form-control has-feedback-left" name="descuento" id="inputSuccess7" placeholder="Tiene Descuento" disabled>
                    <option disabled selected hidden>Tiene Descuento</option>
                    <option>Si</option>
                    <option>No</option>
                  </select>
                  <span class="fa fa-minus form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="text" name="cantidad_solicitada" required class="form-control has-feedback-left" id="inputSuccess8" placeholder="Cantidad Solicitada" disabled>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

              </form>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div> -->
<!-- End Bootstrap modal -->

<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea eliminar el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información de la Solicitud</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- Nombre --></h2></u></strong>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha-creacion">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-salario">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-cantidad-solicitada">
                      <!-- dato -->
                    </div>

                </div> 
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_cotizar" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Cotización</h3>
            </div>
            <div class="modal-body">

              <div id="div_carga">
                <p id="cargador_aviso">Por favor espere...</p>
                <img id="cargador" src="<?php echo site_url('assets/images/ajax-loader.gif') ?>"/>
              </div>

<!--               <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Nota:</strong> Para cotizar debes ingresar primero el monto o suma de prestamo. El sistema reflejara los productos disponibles con esa cantidad para que luego puedas desplegarlos, al seleccionar el producto se reflejaran los plazos disponibles del mismo.
              </div> -->

              <button class="btn btn-default btn-xs" onclick="reiniciar()"><i class="glyphicon glyphicon-refresh"></i> Reiniciar</button>

              <div id="aviso" class="label label-danger"></div>

              <div class="row">
                <form id="form_cotizacion" class="form-horizontal form-label-left">
                  <input type="hidden" value="" name="id"/>

                  <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                    <div class="list-errors animated shake"></div>
                  </div>

                  <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                    <input type="text" name="suma_prestamo" required class="form-control has-feedback-left" id="cinputSuccess" placeholder="Suma de Prestamo" onkeyup="suma(this.value)">
                    <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                    <select required class='form-control has-feedback-left' name='productos_id' id='cinputSuccess2' placeholder='Producto' onchange='producto(this.value)'><option disabled selected hidden>Producto</option><!-- Opciones Ajax --></select>
                    <span class="fa fa-product-hunt form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-4 col-sm-12 col-xs-12 form-group has-feedback">
                    <select required class='form-control has-feedback-left' name='plazo' id='cinputSuccess3' placeholder='Plazo' onchange="meses(this.value)"><option disabled selected hidden>Plazo</option><!-- Opciones Ajax --></select>
                    <span class="fa fa-sun-o form-control-feedback left" aria-hidden="true"></span>
                  </div>

                </form>
              </div>

              <div class="text-center resultado">
                <!-- Para resultado -->
              </div>

              <table class="table table-striped table-bordered dt-responsive nowrap" id="datatable-cotizacion" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Fecha de Pagos</th>
                    <th>Principal</th>
                    <th>Interés</th>
                    <th>Tasa de Interés</th>
                    <th>Cargo Administrativo</th>
                    <th>Gastos notariales</th>
                    <th>Comision</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody class="filas">
                <tfoot>
                  <tr>
                    <th>Fecha de Pagos</th>
                    <th>Principal</th>
                    <th>Interés</th>
                    <th>Tasa de Interés</th>
                    <th>Cargo Administrativo</th>
                    <th>Gastos notariales</th>
                    <th>Comision</th>
                    <th>Total</th>
                  </tr>
                </tfoot>
                </tbody>
              </table>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success btn-xs" onclick="aceptar_cotizacion()" id="btnAceptarCotizacion" data-dismiss="modal">
                <i class="fa fa-check"></i> Aceptar
              </button>
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Modal para aprobar -->
<div id="myModal2" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Estas seguro de aprobar esta solicitud?
      </div>
      <div class="modal-footer">
        <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="aprobar()" data-dismiss="modal"><i class="fa fa-check"></i> Si</a>
        <a class="btn btn-danger btn-xs" href="javascript:void(0)" data-dismiss="modal"><i class="fa fa-close"></i> No</a>
      </div>
    </div>
  </div>
</div>
<!-- Fin de modal aprobar -->

<!-- Upload modal -->
<div class="modal fade" id="modal_form_files" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Archivos del Cliente</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre-archivo text-primary"><!-- Nombre Cliente --></h2></u></strong>
                    </div>

                     <form method="POST" class="myForm" enctype="multipart/form-data">
                          
                          <input type="hidden" value="" name="cliente_id" id="client"/>

                          <div class="row col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                            <div class="row checkbox">
                              <label>
                                <input type="checkbox" class="flat" name="archivo_original" value="Si"> Archivo Original
                              </label>
                            </div>
                          </div>

                          <div class="col-lg-6 col-sm-6 col-12">                            
                              <div class="input-group">
                                  <label class="input-group-btn">
                                      <span class="btn btn-primary">
                                          Examinar&hellip; <input type="file" name="file" style="display: none;" multiple>
                                      </span>
                                  </label>
                                  <input type="text" name="nombre_archivo" class="form-control" readonly>
                              </div>
                          </div>

                          <div class="col-lg-4 col-sm-4 col-12">
                            <?php echo "<select required class='form-control' name='tipos_archivos_id' id='tipos_archivos_id' placeholder='Tipo de Archivo'><option disabled selected hidden>Tipo de Archivo</option>";
                            if (count($result_tipo_archivos)) {
                              foreach ($result_tipo_archivos as $row) {
                                echo "<option value='". $row['id'] . "'>" . $row['tipo'] . "</option>";
                              }
                            }
                            echo "</select>";?> 
                          </div>


                          <div class="col-lg-2 col-sm-2 col-12">
                              <button type="button" class="btn btn-success" onclick="submitFile();">
                                  <i class="fa fa-upload"></i> Subir
                              </button>
                          </div> 

                          <div class="col-lg-12 col-sm-12 col-12">
                              <textarea id="descripcion_archivo" required class="form-control" name="descripcion_archivo" placeholder="Descripción"></textarea>
                          </div>                  

                      </form>

                    <div class="col-lg-12 col-sm-12 col-12">
                        <span class="permission-error-archivo label label-danger"></span>
                    </div>

                    <hr>

                    <div class="col-lg-3 col-sm-3 col-12">
                      <select required class='form-control' name='filtrar_tipos_id' id='filtrar_tipos_id' placeholder='Filtrar por' onchange="filtrar_archivos($('#client').val())"><option disabled selected hidden>Filtrar por</option>
                        <?php if (count($result_tipo_archivos)) {  
                          foreach ($result_tipo_archivos as $row) {
                            echo "<option value='". $row['id'] . "'>" . $row['tipo'] . "</option>";
                          }
                        } ?>
                      </select>
                    </div>

                    <div class="col-lg-3 col-sm-3 col-12">
                      <button class="btn btn-default" onclick="view_files($('#client').val())"><i class="glyphicon glyphicon-refresh"></i> Reiniciar</button>
                    </div>

                    <div class="col-lg-12 col-sm-12 col-12 table-responsive">
                        <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Archivo</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Original</th>
                                    <th>Ultima Modificación</th>
                                    <th>Creado Por</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="filas1">
                            <tfoot>
                                <tr>
                                    <th>Archivo</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Original</th>
                                    <th>Ultima Modificación</th>
                                    <th>Creado Por</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>

                </div> 
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End Upload modal-->

<!-- Modal para rechazo -->
<div id="myModal3" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div id="test"><!-- Texto --></div>
      </div>
      <div id="info2" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal rechazo -->




<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-nota text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="clientes_id" id="clientes_id" />

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

<!--                 <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback notas_id">
                  <select class="form-control" name="notas_id" id="notas_id" style="width: 100%" onchange="notas(this.value)"></select>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback nombrenota">
                  <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre">
                </div> -->

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

              <div class="table-responsive notes_datos">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Fecha</th>
                      <!-- <th class="column-title">Nombre</th> -->
                      <th class="column-title">Nota</th>
                      <th class="column-title">Registrado por</th>
                    </tr>
                  </thead>

                  <tbody class="datos_notas">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


<!-- Modal para deselegir -->
<div id="myModal4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea deseleccionar la solicitud?
      </div>
      <div id="info4" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal deselegir -->




<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_disposiciones" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Disposición</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-disposicion text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_disposiciones" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="solicitud_id"/>
                <input type="hidden" value="" name="bandera"/>

                <div id="msg-error-disposiciones" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-disposiciones animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback disposiciones_id">
                  <select class="form-control" name="disposiciones_id" id="disposiciones_id" style="width: 100%"></select>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_disposicion()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Asignar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->



    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>



<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#datatable-responsive-solicitudes').DataTable( {
        "order": [],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true,
        "ajax": {
            "url": "<?php echo site_url('mis_solicitudes/my_list/Por Aprobar')?>",
            "type": "POST"
        }
    } );

    $("#inputSuccess4").inputmask();


    $('#inputSuccess281').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_4",
      format: "DD/MM/YYYY",
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
      calcular_tiempo_laborando();
    });


    $("#filtrar_disposiciones").select2({
      placeholder: "Filtrar por Disposición",
      allowClear: false,
      language: "es",
      theme: "classic",
    });

    disposiciones_for_select();


});

function calcular_tiempo_laborando(){
  $('.fecha_inicio').text('');
  $.ajax({
      url : "<?php echo site_url('clientes/calcular_tiempo_laborando')?>",
      type: "POST",
      data: {"inicio_labores": $('[name="inicio_labores"]').val()},
      dataType: "JSON",
      success: function(data)
      {
          if (data.datos) {
            $('#inputSuccess28').val(data.datos);
          } else {
            $('.fecha_inicio').text(data.error);
            $('#inputSuccess28').val(null);
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

/*function validarCedula(val)
{
    var pvalor = null;
    if (val.charAt(0).toUpperCase() == "P" || val.charAt(0).toUpperCase() == "E" || val.charAt(0).toUpperCase() == "N") {
      pvalor = val.charAt(0).toUpperCase() + val.slice(1);
      $("#inputSuccess4").val(pvalor);
    }
    if (val.charAt(1).toUpperCase() == "P" || val.charAt(1).toUpperCase() == "E" || val.charAt(1).toUpperCase() == "N") {
      pvalor = val.charAt(0).toUpperCase() + val.charAt(1).toUpperCase() + "-" + val.slice(1);
      $("#inputSuccess4").val(pvalor);
    }
    if (val.charAt(0).toUpperCase() == "-") {
      $("#inputSuccess4").val(null);
    }
    $("#inputSuccess4").inputmask("Regex", { regex: "[pPeEnN]{0,2}[0-9]{0,1}-[0-9]*" }); 
    fcedula($("#inputSuccess4").val());
}


function fcedula(val){
    //var patron = /^(\d){2}\-\d{4}\-\d{4}$/;
    var patron = /^[PEN]{0,2}\d{0,1}\-\d{8,10}$/;
    var validar = patron.test(val);
    if (validar) {
      $.ajax({
          url : "<?php echo site_url('solicitudes/client_by_cedula/')?>/" + val,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
            if (data != null) {
              $('[name="solicitud_id"]').val(null);
                $('[name="id"]').val(data.id);
                $('[name="nombre"]').val(data.nombre);
                $('[name="apellido"]').val(data.apellido);
                $('[name="correo"]').val(data.correo);
                $('[name="telefono"]').val(data.telefono);
                $('[name="salario"]').val(data.salario);
                $('[name="inicio_labores"]').val(data.inicio_labores);
                $('[name="tiempo_laborando"]').val(data.tiempo_laborando);
                $('[name="descuento"]').val(data.descuento);
                $('[name="cantidad_solicitada"]').val(data.cantidad_solicitada);
                save_method = 'update';
            } else {
              $('[name="solicitud_id"]').val(data);
                $('[name="id"]').val(data);
                $('[name="nombre"]').val(data);
                $('[name="apellido"]').val(data);
                $('[name="correo"]').val(data);
                $('[name="telefono"]').val(data);
                $('[name="salario"]').val(data);
                $('[name="inicio_labores"]').val(data);
                $('[name="tiempo_laborando"]').val(data);
                $('[name="descuento"]').val(data);
                $('[name="cantidad_solicitada"]').val(data);
                save_method = 'add';
            }
          $('#inputSuccess').removeAttr("disabled");
          $('#inputSuccess1').removeAttr("disabled");
          $('#inputSuccess2').removeAttr("disabled");
          $('#inputSuccess3').removeAttr("disabled");
          $('#inputSuccess5').removeAttr("disabled");
          $('#inputSuccess6').removeAttr("disabled");
          $('#inputSuccess7').removeAttr("disabled");
          //$('#inputSuccess7').html('<option disabled selected hidden>Tiene Descuento</option>');          
          $('#inputSuccess8').removeAttr("disabled");
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
    } else {
      $('#idc').val(null);
      $('#ids').val(null);
      $('#inputSuccess').val(null);$('#inputSuccess').attr("disabled","disabled");
      $('#inputSuccess1').val(null);$('#inputSuccess1').attr("disabled","disabled");
      $('#inputSuccess2').val(null);$('#inputSuccess2').attr("disabled","disabled");
      $('#inputSuccess3').val(null);$('#inputSuccess3').attr("disabled","disabled");
      $('#inputSuccess5').val(null);$('#inputSuccess5').attr("disabled","disabled");
      $('#inputSuccess6').val(null);$('#inputSuccess6').attr("disabled","disabled");
      $('#inputSuccess7').val(null);$('#inputSuccess7').attr("disabled","disabled");
      $('#inputSuccess8').val(null);$('#inputSuccess8').attr("disabled","disabled");
    }
    $('#msg-error').hide();
}*/


function justNumbers(e)
{
   var keynum = window.event ? window.event.keyCode : e.which;
   if ((keynum == 8) || (keynum == 46))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}


function add_request()
{
    $('#inputSuccess').attr("disabled","disabled");
    $('#inputSuccess1').attr("disabled","disabled");
    $('#inputSuccess2').attr("disabled","disabled");
    $('#inputSuccess3').attr("disabled","disabled");
    $('#inputSuccess4').removeAttr("disabled");
    $('#inputSuccess5').attr("disabled","disabled");
    $('#inputSuccess6').attr("disabled","disabled");
    $('#inputSuccess7').attr("disabled","disabled");
    $('#inputSuccess8').attr("disabled","disabled");
    $('.permission-error').hide();
    $('#msg-error').hide();    
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('#modal_form').modal('show');
    $('.modal-title').text('Añadir Solicitud');
}

function assign_request(id)
{
    $('.permission-error').hide();
    $.ajax({
        url : "<?php echo site_url('solicitudes/assign_request/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          if (data.status) {
              reload_table();
          } else if (data.permission) {
              $('.permission-error').show();
              $('.permission-error').html(data.permission);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function view_request(id)
{

    $('.permission-error').hide();
    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_view/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información de la Solicitud');
            $('.modal-nombre').text(data.nombre);
            $('.modal-fecha-creacion').html('<strong><i class="fa fa-calendar"></i> Fecha de Creación: </strong>' + data.fecha_creacion);
            $('.modal-telefono').html('<strong><i class="fa fa-phone"></i> Teléfono: </strong>' + data.telefono);
            $('.modal-salario').html('<strong><i class="fa fa-usd"></i> Salario: $</strong>' + data.salario);
            $('.modal-cantidad-solicitada').html('<strong><i class="fa fa-usd"></i> Cantidad Solicitada: $</strong>' + data.cantidad_solicitada);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });

}

/*function edit_request(id)
{
    $('#inputSuccess4').attr("disabled","disabled");
    $('.permission-error').hide();
    $('#msg-error').hide();
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    //$('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

          $('[name="solicitud_id"]').val(data.solicitud_id);
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="apellido"]').val(data.apellido);
            $('[name="correo"]').val(data.correo);
            $('[name="telefono"]').val(data.telefono);
            $('[name="cedula"]').val(data.cedula);
            $('[name="salario"]').val(data.salario);
            $('[name="inicio_labores"]').val(data.inicio_labores);
            $('[name="tiempo_laborando"]').val(data.tiempo_laborando);
            $('[name="descuento"]').val(data.descuento);
            $('[name="cantidad_solicitada"]').val(data.cantidad_solicitada);

        $('#inputSuccess').removeAttr("disabled");
        $('#inputSuccess1').removeAttr("disabled");
        $('#inputSuccess2').removeAttr("disabled");
        $('#inputSuccess3').removeAttr("disabled");
        $('#inputSuccess5').removeAttr("disabled");
        $('#inputSuccess6').removeAttr("disabled");
        $('#inputSuccess7').removeAttr("disabled");
        //$('#inputSuccess7').html('<option disabled selected hidden>Tiene Descuento</option>');          
        $('#inputSuccess8').removeAttr("disabled");

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Solicitud'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}*/

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{

    //$('#btnSave').text('procesando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('solicitudes/ajax_add')?>";
    } else {
        url = "<?php echo site_url('solicitudes/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('#msg-error').hide();
            }
            else if (data.validation)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            }
            else if (data.rol)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
            }

            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_request(id)
{
    if(id) {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('solicitudes/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();

                if(data.permission)
                {
                    $('.permission-error').show();
                    $('.permission-error').html(data.permission);
                }


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

        $("#myModal").modal('hide');

    }
}

function confirm_delete(id) {

    $('.permission-error').hide();

    $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal").modal('hide');     // dismiss the dialog
        });
    });

    $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });
    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_request('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}

</script>




<script type="text/javascript">

  $('#div-aprobacion').hide();

  function aprobacion(id){
    $('#list-aprobacion').hide();
    $('#div-aprobacion').show();
    view_client(id);
  }

  function view_client(id)
  {
      $('.permission-error').hide();
      $.ajax({
          url : "<?php echo site_url('mis_solicitudes/ajax_view/')?>" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {

              $('[name="clientes_id"]').val(data.id);
              $('[name="solicitud_id"]').val(data.solicitud_id);
              
              $('.modal-nombre').text(data.nombre + ' ' + data.apellido);

              $('#tabla-view').html('<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">Datos Personales</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td>Género: '+data.genero+'</td>'+
                      '<td>Cédula: '+data.cedula+'</td>'+
                      '<td>Teléfono: '+data.telefono+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Celular: '+data.celular+'</td>'+
                      '<td>Estado Civil: '+data.estado_civil+'</td>'+
                      '<td>Correo Electrónico: '+data.correo+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Fecha de Nacimiento: '+data.fecha_nacimiento+'</td>'+
                      '<td>Numero de Dependientes: '+data.numero_dependientes+'</td>'+
                      '<td>Catidad Solicitada: $'+data.cantidad_solicitada+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Estado: '+data.estado+'</td>'+
                      '<td>Fuente del Cliente: '+data.fuente_cliente+'</td>'+
                      '<td>Como Escucho de Nosotros: '+data.como_escucho_nosotros+'</td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>'+
              '<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">Información Adicional</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td>Tipo de Vivienda: '+data.tipo_vivienda+'</td>'+
                      '<td>Tiene Hipotecas: '+data.hipotecas+'</td>'+
                      '<td>Banco de la Hipoteca: '+data.banco+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Tiene Descuento: '+data.descuento+'</td>'+
                      '<td>Tiempo Restante del Descuento: '+ data.anho_descuento + ' años, ' + data.mes_descuento + ' meses.</td>'+
                      '<td>Capacidad: '+data.capacidad+'</td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>'+
              '<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">Dirección del Cliente</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td>Corregimiento: '+data.corregimiento+'</td>'+
                      '<td>Distrito: '+data.distrito+'</td>'+
                      '<td>Provincia: '+data.provincia+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Dirección: '+data.direccion+'</td>'+
                      '<td></td>'+
                      '<td></td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>'+
              '<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">Información de la Empresa</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td>Salario: $'+data.salario+'</td>'+
                      '<td>Posición de Trabajo: '+data.posicion_trabajo+'</td>'+
                      '<td>Empresa: '+data.companias_nombre+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Inicio Labores: '+data.inicio_labores+'</td>'+
                      '<td>Tiempo Laborando: '+data.tiempo_laborando+'</td>'+
                      '<td>Nombre del Jefe Directo: '+data.nombre_jefe_directo+'</td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>'+
              '<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">Referencia Personal</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td>Nombre: '+data.nombre_referencia+'</td>'+
                      '<td>Apellido: '+data.apellido_referencia+'</td>'+
                      '<td>Parentesco: '+data.parentesco_referencia+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Dirección: '+data.direccion_referencia+'</td>'+
                      '<td>Teléfono: '+data.telefono_referencia+'</td>'+
                      '<td>Celular: '+data.celular_referencia+'</td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>');

              $('.modal-fecha_creacion').html('Fecha de Creación: ' + data.fecha_creacion);
              $('.modal-fecha_modificacion').html('Fecha de Modificación: ' + data.fecha_modificacion);
              $('.modal-usuarios_id').html('Creado Por: ' + data.usuarios_id);


              $('.view_request_table').empty();
              var trHTML = '';
              if (data.comentario == null && data.archivoapc == null && data.comentarioapc == null && data.comentariocompania == null && data.archivorp == null && data.comentariorp == null) {
                trHTML += '<tr>' +
                      '<td colspan="6" align="center">No hay registros...</td>'
                    '</tr>';
              } else {
                if (data.comentario == null) { data.comentario = "" }
                if (data.archivoapc == null) { data.archivoapc = "" }
                if (data.comentarioapc == null) { data.comentarioapc = "" }
                if (data.comentariocompania == null) { data.comentariocompania = "" }
                if (data.archivorp == null) { data.archivorp = "" }
                if (data.comentariorp == null) { data.comentariorp = "" }
                trHTML += '<tr>' +
                      '<td>' + data.comentario + '</td>' +
                      '<td><a href="<?=base_url()?>files/apc/' + data.archivoapc + '" target="_blank">' + data.archivoapc + '</a></td>' +
                      '<td>' + data.comentarioapc + '</td>' +
                      '<td>' + data.comentariocompania + '</td>' +
                      '<td><a href="<?=base_url()?>files/rp/' + data.archivorp + '" target="_blank">' + data.archivorp + '</a></td>' +
                      '<td>' + data.comentariorp + '</td>' +
                    '</tr>';
              }
              $('.view_request_table').append(trHTML);

              info_prestamo(id);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });

  }

  function info_prestamo(id)
  {
      $('.permission-error').hide();
      $.ajax({
          url : "<?php echo site_url('mis_solicitudes/info_prestamo/')?>" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {


              $('.info-prestamo').html('<div class="table-responsive">'+
                  '<table class="table table-striped">'+
                    '<tbody>'+
                      '<tr>'+
                        '<td>Total de descuentos directos: $'+data.descuento.toFixed(2)+'</td>'+
                        '<td>Préstamo máximo a 6 meses: $'+data.prestamo_maximo_6.toFixed(2)+'</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td>Capacidad maxima 20%: $'+data.capacidad_maxima_20.toFixed(2)+'</td>'+
                        '<td>Préstamo máximo a 8 meses: $'+data.prestamo_maximo_8.toFixed(2)+'</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td>Capacidad maxima 35%: $'+data.capacidad_maxima_35.toFixed(2)+'</td>'+
                        '<td>Préstamo máximo a 12 meses: $'+data.prestamo_maximo_12.toFixed(2)+'</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td>Capacidad maxima 50%: $'+data.capacidad_maxima_50.toFixed(2)+'</td>'+
                        '<td>Préstamo máximo a 6 meses con hipoteca: $'+data.prestamo_maximo_6_ch.toFixed(2)+'</td>'+
                      '</tr>'+
                    '</tbody>'+
                  '</table>'+
                '</div>');


              $('.modal-descuento').html('Total de descuentos directos: $' + data.descuento.toFixed(2));


              info_cotizacion(id);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });

  }

  function cancelar(){
    window.location = "<?php site_url('mis_solicitudes/aprobacion') ?>";
  }

  function cotizacion(){
    $('#modal_form_cotizar').modal('show');
    $('#modal-title').text('Cotización');
    reiniciar();
  }

/*  function suma(val) {
      reiniciar();
      var patron = /^(\d)*\.?\d*$/;
      var validar = patron.test(val);
      if (validar) {
        $("#cinputSuccess").val(val);
        if ($("#cinputSuccess").val().length > 0 && $("#cinputSuccess").val() > 0) {
          view_products();
        }
      } else {
        $("#cinputSuccess").val(null);
      }
  }*/


function suma(val) {

    var patron = /^(\d)*\.?\d*$/;
    var validar = patron.test(val);
    if (validar) {
      $("#cinputSuccess").val(val);
      if ($("#cinputSuccess").val().length > 0 && $("#cinputSuccess").val() > 0) {
        cotiza();
      }
    } else {
      $("#cinputSuccess").val(null);
    }
}


  function reiniciar(){
    $('#form_cotizacion')[0].reset();
    $('#cinputSuccess2').empty();
    $('#cinputSuccess2').append("<option disabled selected hidden>Producto</option>");
    $('#cinputSuccess3').empty();
    $('#cinputSuccess3').append("<option disabled selected hidden>Plazo</option>");
    $('.resultado').html('');
    $('.filas').empty();
    $('.filas').append('<tr><td colspan="6" align="center">Esperando cotización...</td></tr>');
    $('#btnAceptarCotizacion').attr('disabled',true);
    view_products();  
  }

/*  var monto_disponible = true;
  function view_products()
  {
      $('#div_carga').show();
      $.ajax({
          url : "<?php echo site_url('cotizacion/view_products')?>",
          type: "POST",
          data: $('#form_cotizacion').serialize(),
          dataType: "JSON",
          success: function(data)
          {
              $('#cinputSuccess2').empty();
              var trHTML = "<option disabled selected hidden>Producto</option>";
              if (data.data.length > 0) {
                  $.each(data.data, function (i, item) {
                      trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                  });
                  monto_disponible = true;
                  $('#aviso').hide();
              } else {    
                  monto_disponible = false;            
                  $('#aviso').show().text('El monto mínimo disponible es de $' + data.min + ' y el máximo es de $' + data.max);
              }
              $('#cinputSuccess2').append(trHTML);
              $('#div_carga').hide();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $('#div_carga').hide();
              alert('Error get data from ajax');
          }
      });
  }*/


function view_products()
{    
    //$('#aviso').hide();
    $('#div_carga').show();
    $.ajax({
        url : "<?php echo site_url('cotizacion/view_products')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            $('#cinputSuccess2').empty();
            var trHTML = "<option disabled selected hidden>Producto</option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
                $('#aviso').hide();
            }
            $('#cinputSuccess2').append(trHTML);
            $('#div_carga').hide();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#div_carga').hide();
            alert('Error get data from ajax');
        }
    });
}

/*  function producto(val){
      $('#aviso').hide();
      $('.resultado').html('');
      $('.filas').empty();
      $('.filas').append('<tr><td colspan="6" align="center">Esperando cotización...</td></tr>');
      if (parseInt(val)) {
        $('#div_carga').show();
        $.ajax({
            url : "<?php echo site_url('cotizacion/plazo_by_products')?>",
            type: "POST",
            data: $('#form_cotizacion').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                $('#cinputSuccess3').empty();
                var trHTML = "<option disabled selected hidden>Plazo</option>";
                if (data.length > 0) {
                    $.each(data, function (i, item) {
                        trHTML += "<option value='" + item + "'>" + item + " Meses</option>";
                    });
                }
                $('#cinputSuccess3').append(trHTML);
                $('#div_carga').hide();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#div_carga').hide();
                alert('Error get data from ajax');
            }
        });
      } else {
        if (monto_disponible == false) {
          view_products();
        } else if ($('#cinputSuccess').val() == "" || $('#cinputSuccess').val() == null) {
          $('#aviso').show().text('Ingrese suma del prestamo a cotizar.');
        }
      }
  }*/


function producto(id){
    $('#aviso').hide();
    $('.resultado').html('');
    $('.filas').empty();
    $('.filas').append('<tr><td colspan="6" align="center">Esperando cotización...</td></tr>');
    if (parseInt(id)) {
      $('#div_carga').show();
      $.ajax({
          url : "<?php echo site_url('cotizacion/plazo_by_products/')?>" + id,
          type: "POST",
          //data: $('#form').serialize(),
          dataType: "JSON",
          success: function(data)
          {
              if (data.data) {
                $('#aviso').show().text('El monto mínimo disponible es de $' + data.data.suma_minima + ' y el máximo es de $' + data.data.suma_maxima);
              }
              $('#cinputSuccess3').empty();
              var trHTML = "<option disabled selected hidden>Plazo</option>";
              if (data.plazo.length > 0) {
                  $.each(data.plazo, function (i, item) {
                      trHTML += "<option value='" + item + "'>" + item + " Meses</option>";
                  });
              }
              $('#cinputSuccess3').append(trHTML);
              $('#div_carga').hide();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $('#div_carga').hide();
              alert('Error get data from ajax');
          }
      });
    }
}

/*  function meses(val) {
      $('#aviso').hide();
      if (parseInt(val)) {
        cotiza();
      } else {
        if ($('#cinputSuccess').val() == "" || $('#cinputSuccess').val() == null) {
          $('#aviso').show().text('Ingrese suma del prestamo a cotizar.');
        } else if ($('#cinputSuccess2').val() == null || $('#cinputSuccess2').val() <= 0) {
          $('#aviso').show().text('Seleccione el producto.');
        }      
      }
  }*/

function meses(val) {
    if (parseInt(val)) {
      cotiza();
    }
}

  function cotiza(){

      var suma,producto,plazo,flag,flag1,flag2;

      suma = $("#cinputSuccess").val();
      producto = $("#cinputSuccess2").val();
      plazo = $("#cinputSuccess3").val();

      if (suma == null || suma == "" || suma == 0) {flag = false;} else {flag = true;}
      if (producto == null) {flag1 = false;} else {flag1 = true;}
      if (plazo == null || plazo == "" || plazo == 0) {flag2 = false;} else {flag2 = true;}

      if (flag == true && flag1 == true && flag2 == true) {
        $('#btnAceptarCotizacion').attr('disabled',false);
        $.ajax({
            url : "<?php echo site_url('cotizacion/ajax_list')?>",
            type: "POST",
            data: $('#form_cotizacion').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                $('.filas').empty();
                var totalm = null;
                var trHTML = '';
                $.each(data.data, function (i, item) {
                    trHTML += '<tr>' +
                      '<td>' + item.fecha + '</td>' +
                      '<td>$' + item.principal.toFixed(2) + '</td>' +
                      '<td>$' + item.interes.toFixed(2) + '</td>' +
                      '<td>%' + item.tasa.toFixed(2) + '</td>' +
                      '<td>$' + item.cargo.toFixed(2) + '</td>' +
                      '<td>$' + item.sologasto.toFixed(2) + '</td>' +
                      '<td>$' + item.solocomision.toFixed(2) + '</td>' +
                      '<td>$' + item.total.toFixed(2) + '</td>' +
                    '</tr>';
                    totalm = item.total;
                });
                trHTML += '<tr><td></td><td><span class="label label-warning">$'+ data.totalprincipal.toFixed(2) +'</span></td><td><span class="label label-info">$'+ data.totalintereses.toFixed(2) +'</span></td><td></td><td><span class="label label-primary">$'+ data.totalcargo.toFixed(2) + '</span></td><td><span class="label label-primary">$'+ data.totalgasto.toFixed(2) +'</span></td><td><span class="label label-primary">$'+ data.totalcomision.toFixed(2) + ' </span></td><td><span class="label label-success">$'+ data.totalcobrar.toFixed(2) +'</span></td></tr>';
                $('.filas').append(trHTML);
                $('.resultado').html("<h1>" + data.suma.toFixed(2) + " + " + data.intereses.toFixed(2) + " = " + data.totalpagar.toFixed(2) + "</h1>" + "<p>Prestamos + Suma de Intereses = Total</p>");

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
      }
  }

  function aceptar_cotizacion(){
    var id = $('[name="solicitud_id"]').val();
    $('.apro-prestamo').html('');
    $.ajax({
        url : "<?php echo site_url('mis_solicitudes/asignar_cotizacion/')?>" + id,
        type: "POST",
        data: $('#form_cotizacion').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            info_cotizacion(id);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
  }

  function info_cotizacion(id)
  {
      $('.permission-error').hide();
      $('.apro-prestamo').html('');
      $.ajax({
          url : "<?php echo site_url('mis_solicitudes/info_cotizacion/')?>" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
            if (data.datos) {
              $('.apro-prestamo').html('<div class="table-responsive">'+
                  '<table class="table table-striped">'+
                    '<tbody>'+
                      '<tr>'+
                        '<td>Salario devengado: $'+data.datos.salario+'</td>'+
                        '<td>Préstamo aprobado: $'+data.datos.cantidad_aprobada+'</td>'+
                        '<td>Pago mensual: $'+data.datos.pago_mensual+'</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td>Cantidad solicitada: $'+data.datos.cantidad_solicitada+'</td>'+
                        '<td>Producto: '+data.datos.nombre_producto+'</td>'+
                        '<td>Pago quincenal: $'+data.datos.pago_quincenal+'</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td></td>'+
                        '<td>Termino aprobado: '+data.datos.cantidad_meses+' meses</td>'+
                        '<td>Obligación Total: $'+data.datos.totalpagar+'</td>'+
                      '</tr>'+
                    '</tbody>'+
                  '</table>'+
                '</div>');
              $('.modal-msg').html('');
            } else if (data.cotize) {
              $('.apro-prestamo').html(data.cotize);
            }

          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });

  }

  function confirmar_aprobacion(){
      $('.permission-error-archivo').hide();

      $("#myModal2").on("show", function() {
          $("#myModal2 a.btn").on("click", function(e) {
              console.log("button pressed");
              $("#myModal2").modal('hide');
          });
      });

      $("#myModal2").on("hide", function() {
          $("#myModal2 a.btn").off("click");
      });
      
      $("#myModal2").on("hidden", function() {
          $("#myModal2").remove();
      });
      
      $("#myModal2").modal({
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true
      });
  }

  function aprobar(){
    var id = $('[name="solicitud_id"]').val();    
    $.ajax({
        url : "<?php echo site_url('mis_solicitudes/aprobar/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          if (data.status) {
            window.location = "<?php site_url('mis_solicitudes/aprobacion') ?>";
          } else if (data.cotizar) {
            $('.modal-msg').html('<span class="label label-danger">'+data.cotizar+'</span>');
          } else if (data.permission) {
            $('.modal-msg').html('<span class="label label-danger">'+data.permission+'</span>');
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
  }

  function file_client(){
      var id = $('[name="clientes_id"]').val();
      $('#filtrar_tipos_id').prop('selectedIndex',0);
      $('.permission-error').hide();
      $('#modal_form_files').modal('show');
      $('.modal-title').text('Archivos del Cliente');
      $('[name="cliente_id"]').val(id);
      view_files(id);
  }

  function view_files(id){
      $('.permission-error-archivo').hide();
      $('.myForm')[0].reset();
      $('#filtrar_tipos_id').prop('selectedIndex',0);
      $.ajax({
          url : "<?php echo site_url('clientes/view_files/')?>" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('.filas1').empty();
              $('.modal-nombre-archivo').text(data.cliente);
              var trHTML = '';
              if (data.data.length > 0) {          
                  $.each(data.data, function (i, item) {
                      trHTML += '<tr>' +
                        '<td>' + item[0] + '</td>' +
                        '<td>' + item[1] + '</td>' +
                        '<td>' + item[2] + '</td>' +
                        '<td>' + item[3] + '</td>' +
                        '<td>' + item[4] + '</td>' +
                        '<td>' + item[5] + '</td>' +
                        '<td>' + item[6] + '</td>' +
                      '</tr>';
                  });                
              } else {
                  trHTML += '<tr>' +
                    '<td colspan="6" align="center">No hay archivos...</td>' +
                  '</tr>';
              }
              $('.filas1').append(trHTML);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });
  }

  function submitFile(){
      var formData = new FormData($('.myForm')[0]);
      var id = $('#client').val();
      $.ajax({
          url: "<?php echo site_url('clientes/cargar_archivo')?>",
          type: 'POST',
          data: formData,
          mimeType: "multipart/form-data",
          contentType: false,
          cache: false,
          processData: false,
          dataType: "JSON",
          success: function(data, textSatus, jqXHR){
              if (data.status) {
                  view_files(id);
              } else if(data.permission) {
                  $('.permission-error-archivo').show();
                  $('.permission-error-archivo').html(data.permission);
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              alert("error");
          }
      });
  }

  function filtrar_archivos(cliente_id){
      var id = $('#filtrar_tipos_id').val();
      $.ajax({
          url : "<?php echo site_url('clientes/filtrar_files/')?>/" + id,
          type: "POST",
          data: {"cliente_id":cliente_id},
          dataType: "JSON",
          success: function(data)
          {
              $('.filas1').empty();
              $('.modal-nombre-archivo').text(data.cliente);
              var trHTML = '';
              if (data.data.length > 0) {          
                  $.each(data.data, function (i, item) {
                      trHTML += '<tr>' +
                        '<td>' + item[0] + '</td>' +
                        '<td>' + item[1] + '</td>' +
                        '<td>' + item[2] + '</td>' +
                        '<td>' + item[3] + '</td>' +
                        '<td>' + item[4] + '</td>' +
                        '<td>' + item[5] + '</td>' +
                        '<td>' + item[6] + '</td>' +
                      '</tr>';
                  });                
              } else {
                  trHTML += '<tr>' +
                    '<td colspan="6" align="center">No hay archivos...</td>' +
                  '</tr>';
              }
              $('.filas1').append(trHTML);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });
  }

  $(function() {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
    });
    
  });

function rechazar() {

    $("#myModal3").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal3 a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal3").modal('hide');     // dismiss the dialog
        });
    });

    $("#myModal3").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal3 a.btn").off("click");
    });
    
    $("#myModal3").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal3").remove();
    });
    
    $("#myModal3").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

    $("#test").html('Esta seguro?. Pasara al estado rechazado.');

    $("#info2").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="acept_test()" data-dismiss="modal"><i class="fa fa-check"></i> Aceptar</a><a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</a>');

}


function acept_test(){
    var id = $('[name="solicitud_id"]').val();
    $.ajax({
        url: "<?php echo site_url('mis_solicitudes/update_test/')?>" + id,
        type: 'POST',
        dataType: "JSON",
        success: function(data, textSatus, jqXHR){
            if (data.status) {
                window.location = "<?php site_url('mis_solicitudes/aprobacion') ?>";
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert("error");
        }
    });
}


function confirm_delete_file(file_id) {

    $('.permission-error-archivo').hide();

    $("#myModal").on("show", function() {
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");
            $("#myModal").modal('hide');
        });
    });

    $("#myModal").on("hide", function() {
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {
        $("#myModal").remove();
    });
    
    $("#myModal").modal({
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true
    });

    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_file('+ file_id +')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}

function delete_file(file_id)
{

    $('.permission-error-archivo').hide();
    var id = $('#client').val();
    $.ajax({
        url : "<?php echo site_url('clientes/file_delete/')?>/" + file_id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {

            if (data.status) {
                view_files(id);
            }
            if(data.error)
            {
                $('.permission-error-archivo').show();
                $('.permission-error-archivo').html(data.error);
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Error deleting data');
            $('.permission-error-archivo').show();
            $('.permission-error-archivo').html('No existe archivo en el directorio.');
        }
    });
    $("#myModal").modal('hide');
}


/*Modal dentro de modal*/
$(document).ready(function () {
    $('#openBtn').click(function () {
        $('#modal_form').modal()
    });

    $('.modal')
        .on({
            'show.bs.modal': function() {
                var idx = $('.modal:visible').length;
                $(this).css('z-index', 1040 + (10 * idx));
            },
            'shown.bs.modal': function() {
                var idx = ($('.modal:visible').length) - 1; // raise backdrop after animation.
                $('.modal-backdrop').not('.stacked')
                .css('z-index', 1039 + (10 * idx))
                .addClass('stacked');
            },
            'hidden.bs.modal': function() {
                if ($('.modal:visible').length > 0) {
                    // restore the modal-open class to the body element, so that scrolling works
                    // properly after de-stacking a modal.
                    setTimeout(function() {
                        $(document.body).addClass('modal-open');
                    }, 0);
                }
            }
        });
});
/*End Modal dentro de modal*/


function disposiciones_clientes_form(){
  var id = $('[name="solicitud_id"]').val();
    $('[name="bandera"]').val(id);
    disposiciones(id);
}

function notas_clientes_form(){
    var id = $('[name="clientes_id"]').val();
    notas_clientes(id);
}

function notas_clientes(id){
  $('#modal_form_notas').modal('show');
  $('.modal-title').text('Añadir Nota');
  $('#form_notas')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="clientes_id"]').val(id);
  $('#msg-error-notas').hide();
  $('.nombrenota').hide();
  //view_notas();
  datos_notas(id);
}


function datos_notas(id){
  $.ajax({
      url : "<?php echo site_url('solicitudes/notas_cliente/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

          $('.modal-nombre-nota').text(data.nombre_cliente);

          $('.datos_notas').empty();
          var trHTML = '';
          if (data.nota.length > 0) {
              $('.notes_datos').show();
              $.each(data.nota, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>'+item.fecha_creacion+'</td>' +
                    '<td>'+item.nota+'</td>' +
                    '<td>'+item.usuarios_id+'</td>' +
                  '</tr>'
              });
          } else {
            $('.notes_datos').hide();
          }
          $('.datos_notas').append(trHTML);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function save_notes(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_notes')?>",
      type: "POST",
      data: $('#form_notas').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            notas_clientes($('[name="clientes_id"]').val());
            $('#msg-error-notas').hide();
          } else if (data.validation) {
            $('#msg-error-notas').show();
            $('.list-errors-notas').html('<div class="animated shake">'+data.validation+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function confirm_deselect_request(id) {
    $('.permission-error-archivo').hide();    
    $("#myModal4").modal("show");
    $("#info4").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="deselect_request('+ id +')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function deselect_request(id){
    $.ajax({
        url : "<?php echo site_url('solicitudes/deselect_request/')?>" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          if (data.status) {
              reload_table();
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    $("#myModal4").modal('hide');
}

function view_notas()
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_notas')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#notas_id').empty();
            var trHTML = "<option></option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            trHTML += "<option value='Otro'>Otro</option>";
            $('#notas_id').append(trHTML);

            $("#notas_id").select2({
              placeholder: "Notas",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".notas_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function notas(id){
  if (id == 'Otro') {
    $('#nota').removeAttr('readonly',false);
    $('[name="nota"]').val(null);
    $('.nombrenota').show();
  } else {
    $('.nombrenota').hide();
    $('#nota').attr('readonly','readonly');
    $.ajax({
        url : "<?php echo site_url('admin/view_nota/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nota"]').val(data.nota);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('[name="nota"]').val(null);
        }
    }); 
  }
}


function refrescar_datos(){
  var id = $('[name="solicitud_id"]').val();
  view_client(id);
}




function disposiciones_for_select(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/disposiciones_for_select')?>",
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        if (data) {
          var trHTML = '<option value="0">Todos<option>';
          $.each(data, function(i,item){
            trHTML += '<option value='+item.id+'>'+item.nombre+'<option>';
          });
          $("#filtrar_disposiciones").html(trHTML);
          $("#filtrar_disposiciones").select2({
            placeholder: "Filtrar por Disposición",
            allowClear: false,
            language: "es",
            theme: "classic",
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function disposiciones(id){
  $('#modal_form_disposiciones').modal('show');
  $('.modal-title').text('Añadir Disposición');
  $('#form_disposiciones')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="solicitud_id"]').val(id);
  $('#msg-error-disposiciones').hide();
  view_disposiciones(id);
}


function view_disposiciones(id)
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_disposiciones/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('.modal-nombre-disposicion').text(data.nombre_cliente);
            
            $('#disposiciones_id').empty();
            var trHTML = "<option></option>";
            if (data.data.length > 0) {
                $.each(data.data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#disposiciones_id').append(trHTML);

            $("#disposiciones_id").select2({
              placeholder: "Disposiciones",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".disposiciones_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save_disposicion(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_disposicion')?>",
      type: "POST",
      data: $('#form_disposiciones').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            $('#modal_form_disposiciones').modal('hide');
            if ($('[name="bandera"]').val() > 0) {
              window.location = "<?php site_url('mis_solicitudes/aprobacion') ?>";
            } else {
              reload_table();
            }
          } else if (data.validation) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.validation+'</div>');
          } else if (data.permission) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.permission+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function filtrar_disposiciones(disposicion){
  table.ajax.url("<?php echo site_url('mis_solicitudes/my_list/Por Aprobar/')?>" + disposicion).load();
}




</script>


