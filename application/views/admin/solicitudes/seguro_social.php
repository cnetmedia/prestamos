        <style>
          #div_carga{
            position:absolute;
            top:0;
            left:0;
            width:100%;
            height:100%;
            display:none;
            z-index:1;
          }

          #cargador{
            position:absolute;
            top:50%;
            left: 50%;
            margin-top: -25px;
            margin-left: -25px;
          }

          #cargador_aviso{
            position:absolute;
            top:40%;
            left: 47%;
            margin-top: -25px;
            margin-left: -25px;
          }
        </style>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row" id="list-firma">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Seguro Social <small>Información</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p>
                      Consulte la Base de Datos que desee. Luego de seleccionarla puede buscar en el campo de busqueda por cualquiera de las columna de la tabla. <br> <b>Nota:</b> Tome en cuenta que los nombres en las base de datos de la CSS pueden variar de orden, es decir, podria estar escrito primero el nombre y luego el apellido o al contrario. El campo de busqueda de la tabla es vulnerable al orden en el cual lo coloque.
                    </p>

                    <div id="div_carga">
                      <p id="cargador_aviso">Por favor espere...</p>
                      <img id="cargador" src="<?php echo site_url('assets/images/ajax-loader.gif') ?>"/>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>

                    <br>

                    <form id="form-access" class="form-horizontal">

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Base de Datos CSS</label>
                        <select class="form-control col-md-6 col-xs-12" name="dbaccess" id="dbaccess" onchange="cargar_tabla(this.value)">
                          <option disabled selected hidden>Seleccione Base de Datos CSS</option>
                          <?php if ($access) {
                            foreach ($access as $row) {
                              echo '<option value="'.$row->id.'">'.$row->nombre.'</option>';
                            }
                          } ?>
                        </select>
                      </div>

                    </form>

                    <table id="datatable-css" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>SEGURO</th>
                          <th>CEDULA</th>
                          <th>NOMBRE</th>
                          <th>RAZON_SO</th>
                          <th>PATRONO</th>
                          <th>TEL1</th>
                          <th>TEL2</th>
                          <th>FECHA</th>
                          <th>SALARIO</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>SEGURO</th>
                          <th>CEDULA</th>
                          <th>NOMBRE</th>
                          <th>RAZON_SO</th>
                          <th>PATRONO</th>
                          <th>TEL1</th>
                          <th>TEL2</th>
                          <th>FECHA</th>
                          <th>SALARIO</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
            </div>

        </div>
        <!-- /page content -->


    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>


<script type="text/javascript">

var table;

$(document).ready(function() {

    table = $('#datatable-css').DataTable( {
      "order": [],
      "bRetrieve": true,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "ajax": {
          "url": "<?php echo site_url('admin/table_css')?>",
          "type": "POST"
      },
      "columnDefs": [{ 
          "targets": [ -1 ],
          "orderable": false,
      },],
    } );

});

function cargar_tabla(id) {
  table.ajax.url("<?php echo site_url('admin/table_css/')?>" + id).load();
}

</script>