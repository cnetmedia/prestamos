  <style>
    #div_carga{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      /*background: url(<?php echo site_url('assets/images/gris.png') ?>) repeat;*/
      display:none;
      z-index:1;
    }

    #cargador{
      position:absolute;
      top:50%;
      left: 50%;
      margin-top: -25px;
      margin-left: -25px;
    }

    #cargador_aviso{
      position:absolute;
      top:40%;
      left: 47%;
      margin-top: -25px;
      margin-left: -25px;
    }
  </style>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row" id="list-verificados">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Solicitudes Por Verificar <small>Listado de Solicitudes</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>

                    <div class="row col-lg-3 pull-right">
                      <select class='form-control' name="filtrar_disposiciones" id="filtrar_disposiciones" onchange="filtrar_disposiciones(this.value)">
                      </select>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>

                    <br><br>

                    <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Compañia</th>
                          <th>Solicitado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Compañia</th>
                          <th>Solicitado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Acciones</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
          </div>



            <div class="row" id="div-verificado">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Solicitudes <small>Verificación</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div id="div_carga">
                      <p id="cargador_aviso">Por favor espere...</p>
                      <img id="cargador" src="<?php echo site_url('assets/images/ajax-loader.gif') ?>"/>
                    </div>

                    <!-- Smart Wizard -->
                    <div id="wizard" class="form_wizard wizard_horizontal">
                      <ul class="wizard_steps">
                        <li>
                          <a href="#step-1">
                            <span class="step_no">1</span>
                            <span class="step_descr">
                                Paso 1<br />
                                <small>Css</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <span class="step_no">2</span>
                            <span class="step_descr">
                                Paso 2<br />
                                <small>Apc</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <span class="step_no">3</span>
                            <span class="step_descr">
                                Paso 3<br />
                                <small>Compañia</small>
                            </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <span class="step_no">4</span>
                            <span class="step_descr">
                                Paso 4<br />
                                <small>RP</small>
                            </span>
                          </a>
                        </li>
                      </ul>
                      <div id="step-1">

                      </div>
                      <div id="step-2">

                      </div>
                      <div id="step-3">

                      </div>
                      <div id="step-4">

                      </div>
                    </div>
                    <!-- End SmartWizard Content -->
                  </div>
                </div>
              </div>
          </div>







        </div>
        <!-- /page content -->

<!-- Bootstrap modal -->
<!-- <div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Solicitud</h3>
            </div>
            <div class="modal-body form">

              <form id="form" class="form-horizontal form-label-left input_mask">
              <input type="hidden" value="" name="id" id="idc" />
              <input type="hidden" value="" name="solicitud_id" id="ids" />

                <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors animated shake"></div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="nombre" required class="form-control has-feedback-left" id="sinputSuccess" placeholder="Nombre" disabled>
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="apellido" required class="form-control" id="sinputSuccess1" placeholder="Apellido" disabled>
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="email" name="correo" required class="form-control has-feedback-left" id="sinputSuccess2" placeholder="Correo Electrónico" disabled>
                  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="telefono" required class="form-control has-feedback-left" id="sinputSuccess3" placeholder="Teléfono" onkeypress="return justNumbers(event);" disabled>
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="cedula" required class="form-control" id="sinputSuccess4" placeholder="Cédula">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="salario" required class="form-control has-feedback-left" id="sinputSuccess5" placeholder="Salario" disabled>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="inicio_labores" required class="form-control has-feedback-left date-picker" id="sinputSuccess281" placeholder="Inicio de Labores" maxlength="10">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="tiempo_laborando" required class="form-control" id="sinputSuccess6" placeholder="Tiempo Laborando" disabled>
                  <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <select required class="form-control has-feedback-left" name="descuento" id="inputSuccess7" placeholder="Tiene Descuento" onchange="select_descuento(this.value)">
                    <option disabled selected hidden>Tiene Descuento</option>
                    <option>Si</option>
                    <option>No</option>
                  </select>
                  <span class="fa fa-minus form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="cantidad_descuento" required class="form-control" id="oinputSuccess" placeholder="Cantidad de Descuento">
                  <span class="fa fa-usd form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="text" name="cantidad_solicitada" required class="form-control has-feedback-left" id="sinputSuccess8" placeholder="Cantidad Solicitada" disabled>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

              </form>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div> -->
<!-- End Bootstrap modal -->

<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea eliminar el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información de la Solicitud</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- Nombre --></h2></u></strong>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha-creacion">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-salario">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-cantidad-solicitada">
                      <!-- dato -->
                    </div>

                </div> 
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


<!-- Modal para rechazo -->
<div id="myModal2" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div id="test"><!-- Texto --></div>
      </div>
      <div id="info2" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal rechazo -->


<!-- Modal para anular solicitudes -->
<div id="myModal3" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        El cliente pasara a estado por aprobar, deseas continuar?
      </div>
      <div class="modal-footer">
        <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="updateVerificado()" data-dismiss="modal"><i class="fa fa-check"></i> Si</a>
        <a class="btn btn-danger btn-xs" href="javascript:void(0)" data-dismiss="modal"><i class="fa fa-close"></i> No</a>
      </div>
    </div>
  </div>
</div>
<!-- Fin de modal anular solicitudes -->


<!-- Upload modal -->
<div class="modal fade" id="modal_form_files" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Archivos del Cliente</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre-archivo text-primary"><!-- Nombre Cliente --></h2></u></strong>
                    </div>

                     <form method="POST" class="myForm" enctype="multipart/form-data">
                          
                        <input type="hidden" value="" name="cliente_id" id="client"/>

                        <div class="row col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <div class="row checkbox">
                            <label>
                              <input type="checkbox" class="flat" name="archivo_original" value="Si"> Archivo Original
                            </label>
                          </div>
                        </div>

                        <div class="col-lg-6 col-sm-6 col-12">                            
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        Examinar&hellip; <input type="file" name="file" style="display: none;" multiple>
                                    </span>
                                </label>
                                <input type="text" name="nombre_archivo" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-12">
                          <?php echo "<select required class='form-control' name='tipos_archivos_id' id='tipos_archivos_id' placeholder='Tipo de Archivo'><option disabled selected hidden>Tipo de Archivo</option>";
                          if (count($result_tipo_archivos)) {  
                            foreach ($result_tipo_archivos as $row) {
                              echo "<option value='". $row['id'] . "'>" . $row['tipo'] . "</option>";
                            }
                          }
                          echo "</select>";?>
                        </div>


                        <div class="col-lg-2 col-sm-2 col-12">
                            <button type="button" class="btn btn-success" onclick="submitFile();">
                                <i class="fa fa-upload"></i> Subir
                            </button>
                        </div> 

                        <div class="col-lg-12 col-sm-12 col-12">
                            <textarea id="descripcion_archivo" required class="form-control" name="descripcion_archivo" placeholder="Descripción"></textarea>
                        </div>                  

                    </form>

                    <div class="col-lg-12 col-sm-12 col-12">
                        <span class="permission-error-archivo label label-danger"></span>
                    </div>

                    <hr>

                    <div class="col-lg-3 col-sm-3 col-12">
                      <select required class='form-control' name='filtrar_tipos_id' id='filtrar_tipos_id' placeholder='Filtrar por' onchange="filtrar_archivos($('#client').val())"><option disabled selected hidden>Filtrar por</option>
                        <?php if (count($result_tipo_archivos)) {  
                          foreach ($result_tipo_archivos as $row) {
                            echo "<option value='". $row['id'] . "'>" . $row['tipo'] . "</option>";
                          }
                        } ?>
                      </select>
                    </div>

                    <div class="col-lg-3 col-sm-3 col-12">
                      <button class="btn btn-default" onclick="view_files($('#client').val())"><i class="glyphicon glyphicon-refresh"></i> Reiniciar</button>
                    </div>

                    <div class="col-lg-12 col-sm-12 col-12 table-responsive">
                        <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Archivo</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Original</th>
                                    <th>Ultima Modificación</th>
                                    <th>Creado Por</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="filas">
                            <tfoot>
                                <tr>
                                    <th>Archivo</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Original</th>
                                    <th>Ultima Modificación</th>
                                    <th>Creado Por</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>

                </div> 
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End Upload modal  -->




<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-nota text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="clientes_id" id="clientes_id" />

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

<!--                 <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback notas_id">
                  <select class="form-control" name="notas_id" id="notas_id" style="width: 100%" onchange="notas(this.value)"></select>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback nombrenota">
                  <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre">
                </div> -->

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

              <div class="table-responsive notes_datos">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Fecha</th>
                      <!-- <th class="column-title">Nombre</th> -->
                      <th class="column-title">Nota</th>
                      <th class="column-title">Registrado por</th>
                    </tr>
                  </thead>

                  <tbody class="datos_notas">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


<!-- Modal para deselegir -->
<div id="myModal4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea deseleccionar la solicitud?
      </div>
      <div id="info4" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal deselegir -->







<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_disposiciones" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Disposición</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-disposicion text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_disposiciones" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="solicitud_id"/>
                <input type="hidden" value="" name="bandera"/>

                <div id="msg-error-disposiciones" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-disposiciones animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback disposiciones_id">
                  <select class="form-control" name="disposiciones_id" id="disposiciones_id" style="width: 100%"></select>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_disposicion()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Asignar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->





        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Desarrollado por <a href="http://www.sevenencorp.com/" target="_blank">Sevenen Corporation</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo site_url('gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo site_url('gentelella-master/vendors/fastclick/lib/fastclick.js') ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo site_url('gentelella-master/vendors/nprogress/nprogress.js') ?>"></script>
    <!-- Chart.js -->
    <script src="<?php echo site_url('gentelella-master/vendors/Chart.js/dist/Chart.min.js') ?>"></script>
    <!-- gauge.js -->
    <script src="<?php echo site_url('gentelella-master/vendors/gauge.js/dist/gauge.min.js') ?>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo site_url('gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>"></script>
    <!-- iCheck -->
    <script src="<?php echo site_url('gentelella-master/vendors/iCheck/icheck.min.js') ?>"></script>
    <!-- Skycons -->
    <script src="<?php echo site_url('gentelella-master/vendors/skycons/skycons.js') ?>"></script>
    <!-- Flot -->
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.pie.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.time.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.stack.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.resize.js') ?>"></script>
    <!-- Flot plugins -->
    <script src="<?php echo site_url('gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/flot.curvedlines/curvedLines.js') ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo site_url('gentelella-master/vendors/DateJS/build/date.js') ?>"></script>
    <!-- JQVMap -->
    <script src="<?php echo site_url('gentelella-master/vendors/jqvmap/dist/jquery.vmap.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo site_url('gentelella-master/production/js/moment/moment.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/production/js/datepicker/daterangepicker.js') ?>"></script>
    <!-- Datatables -->
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons/js/buttons.flash.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') ?>"></script>
<!--     <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-scroller/js/datatables.scroller.min.js') ?>"></script> -->
    <script src="<?php echo site_url('gentelella-master/vendors/jszip/dist/jszip.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/pdfmake/build/pdfmake.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/pdfmake/build/vfs_fonts.js') ?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo site_url('gentelella-master/build/js/custom.min.js') ?>"></script>

    <!-- jquery.inputmask -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') ?>"></script>

    <!-- jQuery autocomplete -->
    <script src="<?php echo site_url('gentelella-master/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') ?>"></script>

    <!-- Dropzone.js -->
    <script src="<?php echo site_url('gentelella-master/vendors/dropzone/dist/min/dropzone.min.js') ?>"></script>

    <!-- assets -->
    <script src="<?php echo site_url('assets/js/jquery.spinner.js') ?>"></script>

    <!-- Select2 -->
    <script src="<?php echo site_url('gentelella-master/vendors/select2/dist/js/select2.full.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/select2/dist/js/i18n/es.js') ?>"></script>
    <!-- jQuery Smart Wizard -->
    <script src="<?php echo site_url('gentelella-master/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js') ?>"></script>





<script type="text/javascript">

var save_method; //for save method string
var table;
var method_compania;

$(document).ready(function() {
    table = $('#datatable-responsive-solicitudes').DataTable( {
        "order": [],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true,
        "ajax": {
            "url": "<?php echo site_url('mis_solicitudes/my_list/Por Verificar')?>",
            "type": "POST"
        }
    } );

    $("#inputSuccess4").inputmask();
    
    disposiciones_for_select();

});


function dateinicio(){
  $(document).ready(function() {
    $('#vinputSuccess281').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_4",
      format: "DD/MM/YYYY",
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
      calcular_tiempo_laborando();
    });
  });
}

function calcular_tiempo_laborando(){
  $('.fecha_inicio').html('');
  $.ajax({
      url : "<?php echo site_url('clientes/calcular_tiempo_laborando')?>",
      type: "POST",
      data: {"inicio_labores": $('[name="cinicio_labores"]').val()},
      dataType: "JSON",
      success: function(data)
      {
          if (data.datos) {
            $('#vinputSuccess4').val(data.datos);
          } else {
            $('.fecha_inicio').html(data.error);
            $('#vinputSuccess4').val(null);
          }
          if (data.validation) {
            $('.fecha_inicio').html(data.validation);
            $('#inputSuccess28').val(null);                
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}


function view_request(id)
{
    $('.permission-error').hide();
    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_view/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {            
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información de la Solicitud');
            $('.modal-nombre').text(data.nombre);
            $('.modal-fecha-creacion').html('<strong><i class="fa fa-calendar"></i> Fecha de Creación: </strong>' + data.fecha_creacion);
            $('.modal-telefono').html('<strong><i class="fa fa-phone"></i> Teléfono: </strong>' + data.telefono);
            $('.modal-salario').html('<strong><i class="fa fa-usd"></i> Salario: $</strong>' + data.salario);
            $('.modal-cantidad-solicitada').html('<strong><i class="fa fa-usd"></i> Cantidad Solicitada: $</strong>' + data.cantidad_solicitada);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function edit_request(id)
{
    $('#sinputSuccess4').attr("disabled","disabled");
    $('.permission-error').hide();
    $('#msg-error').hide();
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');

    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_edit/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="solicitud_id"]').val(data.solicitud_id);
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="apellido"]').val(data.apellido);
            $('[name="correo"]').val(data.correo);
            $('[name="telefono"]').val(data.telefono);
            $('[name="cedula"]').val(data.cedula);
            $('[name="salario"]').val(data.salario);
            $('[name="inicio_labores"]').val(data.inicio_labores);
            $('[name="tiempo_laborando"]').val(data.tiempo_laborando);
            $('[name="descuento"]').val(data.descuento);
            $('[name="cantidad_descuento"]').val(data.cantidad_descuento);
            $('[name="cantidad_solicitada"]').val(data.cantidad_solicitada);

            $('#sinputSuccess').removeAttr("disabled");
            $('#sinputSuccess1').removeAttr("disabled");
            $('#sinputSuccess2').removeAttr("disabled");
            $('#sinputSuccess3').removeAttr("disabled");
            $('#sinputSuccess5').removeAttr("disabled");
            $('#sinputSuccess6').removeAttr("disabled");
            $('#sinputSuccess7').removeAttr("disabled");
            $('#sinputSuccess8').removeAttr("disabled");

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Solicitud'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    //$('#btnSave').text('procesando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_update')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('#msg-error').hide();
            }
            else if (data.validation)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            }
            else if (data.rol)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
            }

            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_request(id)
{
    if(id) {
        $.ajax({
            url : "<?php echo site_url('solicitudes/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $('#modal_form').modal('hide');
                reload_table();
                if(data.permission)
                {
                    $('.permission-error').show();
                    $('.permission-error').html(data.permission);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
        $("#myModal").modal('hide');
    }
}

function confirm_delete(id) {

    $('.permission-error').hide();

    $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal").modal('hide');     // dismiss the dialog
        });
    });

    $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });
    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_request('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}


function porAprobar(id) {

    $('.permission-error').hide();

    $("#myModal2").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal2 a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal2").modal('hide');     // dismiss the dialog
        });
    });

    $("#myModal2").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal2 a.btn").off("click");
    });
    
    $("#myModal2").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal2").remove();
    });
    
    $("#myModal2").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

    $("#test").html('No cumple con los requisitos necesarios. Pasara al estado rechazado.');

    $("#info2").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="acept_test('+id+')" data-dismiss="modal"><i class="fa fa-check"></i> Aceptar</a><a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</a>');

}

  function enviarAprobar() {

      $('.permission-error-archivo').hide();

      $("#myModal3").on("show", function() {
          $("#myModal3 a.btn").on("click", function(e) {
              console.log("button pressed");
              $("#myModal3").modal('hide');
          });
      });

      $("#myModal3").on("hide", function() {
          $("#myModal3 a.btn").off("click");
      });
      
      $("#myModal3").on("hidden", function() {
          $("#myModal3").remove();
      });
      
      $("#myModal3").modal({
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true
      });
  }





  $('#div-verificado').hide();

  function verificacion(id){

    $('#div-verificado').show();
    $('#list-verificados').hide();

    $(document).ready(function() {

      $('#wizard').smartWizard({
        labelNext:'Siguiente',
        labelPrevious:'Anterior',
        labelFinish:'Enviar a aprobar',
        labelCancel:'Cancelar',
        labelNotes:'Notas',
        labelDisposition:'Disposición',
        labelReject:'Rechazar',
        reverseButtonsOrder: false,
        //contentURL:"<?php echo site_url('mis_solicitudes/verify_customer?id=')?>" + id,
        contentURL:"<?php echo site_url('mis_solicitudes/verify_customer/')?>" + id,
        transitionEffect:'slide', // Effect on navigation, none/fade/slide/slideleft
        onFinish:onFinishCallback,
        onLeaveStep:leaveAStepCallback
      });


      $('.buttonNext').addClass('btn btn-primary btn-xs');
      $('.buttonPrevious').addClass('btn btn-info btn-xs');
      $('.buttonFinish').addClass('btn btn-success btn-xs pull-right');
      $('.buttonCancel').addClass('btn btn-danger btn-xs pull-right');
      $('.buttonNotes').addClass('btn btn-warning btn-xs pull-right');
      $('.buttonDisposition').addClass('btn btn-warning btn-xs pull-right');
      $('.buttonReject').addClass('btn btn-danger btn-xs pull-right');

      $('.buttonCancel').click(function() {
          window.location = "<?php site_url('mis_solicitudes/verificacion') ?>";
      });
      $('.buttonNotes').click(function() {
          notas_clientes($('#clientes_id').val());
      });
      $('.buttonDisposition').click(function() {
        var id = $('#solicitud_id').val();
          $('[name="bandera"]').val(id);
          disposiciones(id);
      });
      $('.buttonReject').click(function() {
          porAprobar($('#solicitud_id').val());
      });


    });


    function onFinishCallback(){
      if(validateAllSteps()){
        submitFileRp();
      }
    }

    function leaveAStepCallback(obj){
      var step_num= obj.attr('rel');
      return validateSteps(step_num);
    }

    function validateSteps(step){
      var isStepValid = true;
      if(step == 1){
        if(validateStep1() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','<span class="label label-danger animated shake">Por favor, corrija los errores en el paso ' + step + ' y haga clic en siguiente.</span><br><br>');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
        }else{
          $('#wizard').smartWizard('hideMessage');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
        validateCss();
      }
      else if(step == 2){
        if(validateStep2() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','<span class="label label-danger animated shake">Por favor, corrija los errores en el paso ' + step + ' y haga clic en siguiente.</span><br><br>');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
        }else{
          $('#wizard').smartWizard('hideMessage');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
        submitFileApc();
      }
      else if(step == 3){
        if(validateStep3() == false ){
          isStepValid = false; 
          $('#wizard').smartWizard('showMessage','<span class="label label-danger animated shake">Por favor, corrija los errores en el paso ' + step + ' y haga clic en siguiente.</span><br><br>');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
        }else{
          $('#wizard').smartWizard('hideMessage');
          $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        }
        saveCompania();
      }
      return isStepValid;
    }

    function validateAllSteps(){
      var isStepValid = true;

      if(validateStep1() == false){
        isStepValid = false;
        $('#wizard').smartWizard('setError',{stepnum:1,iserror:true});         
      }else{
        $('#wizard').smartWizard('setError',{stepnum:1,iserror:false});
      }

      if(validateStep2() == false){
        isStepValid = false;
        $('#wizard').smartWizard('setError',{stepnum:2,iserror:true});         
      }else{
        $('#wizard').smartWizard('setError',{stepnum:2,iserror:false});
      }

      if(validateStep3() == false){
        isStepValid = false;
        $('#wizard').smartWizard('setError',{stepnum:3,iserror:true});         
      }else{
        $('#wizard').smartWizard('setError',{stepnum:3,iserror:false});
      }

      if(!isStepValid){
        $('#wizard').smartWizard('showMessage','<span class="label label-danger">Por favor, corrija los errores en los pasos y continue</span><br><br>');
      }

      return isStepValid;
    }

    function validateStep1(){
      var isValid = true;
      var un = $('#comentario_css').val();
      var un1 = $('#checkvalido').prop('checked');
      var un2 = $('#cedu_access').val();
      if(!un && un.length <= 0){
        isValid = false;
      }
      else if(un1 == false){
        isValid = false;
      }
      if(!un2 && un2.length <= 0){
        isValid = false;
        $('#validcedu').show();
        $('#validcedu').html('<span class="label label-danger">Seleccione la Cédula, en el boton Seleccionar de la Tabla de Access</span>');
      }else{
        $('#validcedu').hide();
      }
      return isValid;
    }

    function validateStep2(){
      var isValid = true;
      var un = $('#comentario_apc').val();
      if(!un && un.length <= 0){
        isValid = false;
      }
      return isValid;
    }

    function validateStep3(){
      var isValid = true;

      var vun = $('#vinputSuccess').val();
      var vun1 = $('#vinputSuccess2').val();
      var vun2 = $('#vinputSuccess4').val();

      var un2 = $('#inputSuccess1').val();
      var un3 = $('#inputSuccess2').val();
      var un4 = $('#inputSuccess3').val();
      var un5 = $('#inputSuccess4').val();
      var un6 = $('#inputSuccess5').val();
      var un7 = $('#inputSuccess6').val();
      var un8 = $('#inputSuccess7').val();
      var un9 = $('#inputSuccess8').val();
      var un10 = $('#inputSuccess9').val();
      var un11 = $('#inputSuccess10').val();
      var un12 = $('#inputSuccess11').val();
      var un13 = $('#inputSuccess12').val();
      var un14 = $('#inputSuccess13').val();
      var un15 = $('#cnombre_firma').val();
      var un16 = $('#cposicion_firma').val();

      var un = $('#comentario_compania').val();
      var un1 = $('#checkvalidocompania').prop('checked');


      if(!un2 && un2.length <= 0){
        un1 = false;
      }
      else if(!vun && vun.length <= 0){
        un1 = false;
      }
      else if(!vun1 && vun1.length <= 0){
        un1 = false;
      }
      else if(!vun2 && vun2.length <= 0){
        un1 = false;
      }
      else if(!un3 && un3.length <= 0){
        un1 = false;
      }
/*      else if(!un4 && un4.length <= 0){
        un1 = false;
      }*/
/*      else if(!un5 && un5.length <= 0){
        un1 = false;
      }*/
      else if(!un6 && un6.length <= 0){
        un1 = false;
      }
/*      else if(!un7 && un7.length <= 0){
        un1 = false;
      }*/
      else if(!un8 && un8.length <= 0){
        un1 = false;
      }
      else if(!un9 && un9.length <= 0){
        un1 = false;
      }
/*      else if(!un10 && un10.length <= 0){
        un1 = false;
      }
      else if(!un11 && un11.length <= 0){
        un1 = false;
      }*/
      else if(!un12){
        un1 = false;
      }
      else if(!un13 && un13.length <= 0){
        un1 = false;
      }
      else if(!un14 && un14.length <= 0){
        un1 = false;
      }
      else if(!un15 && un15.length <= 0){
        un1 = false;
      }
      else if(!un16 && un16.length <= 0){
        un1 = false;
      }
      
      if(!un && un.length <= 0){
        isValid = false;
      }
      else if(un1 == false){
        isValid = false;
      }
      return isValid;
    }

  }

  var access_cedula = 0;
  function view_client_css(id)
  {
    $.ajax({
      url : "<?php echo site_url('mis_solicitudes/ajax_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {              
        $('.modal-nombre').text(data.nombre + ' ' + data.apellido);
        $('.modal-cedula').text(data.cedula);
        $('.modal-telefono').text(data.telefono);
        $('.modal-empleados').text(data.companias_cantidad_empleados);
        $('.modal-salario').text('$' + data.salario);
        $('.modal-companias_id').text(data.companias_nombre);
        $('[name="comentario_css"]').val(data.comentario);
        $('[name="dbaccess"]').val(data.db_access_id);

        if (data.db_access_id > 0) {
          select_access(data.db_access_id);
        }

        if (data.valido == "Si") {
        $("#checkvalido").attr("checked",true);
        } else {
        $("#checkvalido").attr("checked",false);
        }

        if (data.cedula_access != null) {
          access_cedula = data.cedula_access;
        }

        

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });

  }

  function obtener_access_cedula(){
    var access_id =  $('[name="dbaccess"]').val();
    if (access_id > 0 && access_cedula.length > 0) {
      $('.alerta').hide();   
      $('.aviso').hide();
      $('#resultado').hide();
      $('#div_carga').show();
      $('#validcedu').hide();
      var selectCedula = access_cedula;
      var selectAccess = access_id;
      $("#cedu_access").val(selectCedula);
      $.ajax({
        url : "<?php echo site_url('mis_solicitudes/seleccion_cedula')?>",
        type: "POST",
        data: {
          "selectCedula":selectCedula,
          "selectAccess":selectAccess
        },
        dataType: "JSON",
        success: function(data)
        {          
          $('#div_carga').hide();
          if (data.datos == "") {
            $('.alerta').show();
          } else {
            $('#resultado').show();
            $('#resultado').html(data.datos);
            $("#checkvalido").removeAttr("disabled");
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax');
        }
      });
    }
  }


  function view_client_apc(id)
  {
    $.ajax({
      url : "<?php echo site_url('mis_solicitudes/ajax_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {              

        $('[name="comentario_apc"]').val(data.comentarioapc);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });

  }

  function view_client_com(id)
  {
    $.ajax({
      url : "<?php echo site_url('mis_solicitudes/ajax_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {              

        if (data.validocompania == "Si") {
        $("#checkvalidocompania").attr("checked",true);
        } else {
        $("#checkvalidocompania").attr("checked",false);
        }

        $('[name="comentario_compania"]').val(data.comentariocompania);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });

  }

  function view_client_rp(id)
  {
    $.ajax({
      url : "<?php echo site_url('mis_solicitudes/ajax_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {              

        if (data.validorp == "Si") {
        $("#checkvalidorp").attr("checked",true);
        } else {
        $("#checkvalidorp").attr("checked",false);
        }

        $('[name="comentario_rp"]').val(data.comentariorp);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });

  }

  function validateCss(){
    $('#msg-error').hide();
    $.ajax({
      url : "<?php echo site_url('mis_solicitudes/add_css')?>",
      type: "POST",
      data: $('#form_css').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        if (data.status) //if success close modal and reload ajax table
        {
          $('#msg-error').hide();
          window.scrollTo(0, 0);
        }
        else if (data.validation)
        {
          $('#msg-error').show();
          $('.list-errors').html(data.validation);
        }
        else if (data.permission)
        {
          $('#msg-error').show();
          $('.list-errors').html(data.permission + "<br><br>");
          porAprobar($('#solicitud_id').val());
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      }
    }); 
  }


  function scriptFile(){
    $(function() {

      // We can attach the `fileselect` event to all file inputs on the page
      $(document).on('change', ':file', function() {
        var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
      });

      // We can watch for our custom `fileselect` event like this
      $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
            input.val(log);
          } else {
            if( log ) alert(log);
          }

        });
      });

    });
  }

  function submitFileApc(){
      var formData = new FormData($('#form_apc')[0]);
      $.ajax({
          url: "<?php echo site_url('mis_solicitudes/cargar_archivo_apc')?>",
          type: 'POST',
          data: formData,
          mimeType: "multipart/form-data",
          contentType: false,
          cache: false,
          processData: false,
          dataType: "JSON",
          success: function(data, textSatus, jqXHR){
              if (data.status) {
                  $('.permission-error-archivo').hide();
                  window.scrollTo(0, 0);
              } else if(data.permission) {
                  $('.permission-error-archivo').show();
                  $('.permission-error-archivo').html(data.permission);
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              alert("error");
          }
      });
  }

  function verify_companies(id)
  {
      $.ajax({
          url : "<?php echo site_url('mis_solicitudes/ajax_view/')?>" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('[name="ccompanias_id"]').val(data.companias_id);
              $('#vinputSuccess3').select2().on("select2-selecting", function(e) {});
              $("#vinputSuccess3").select2({
                placeholder: "Empresa",
                allowClear: false,
                language: "es",
                theme: "classic",
                dropdownParent: $(".selectcompany")
              });           
              $('[name="csalario"]').val(data.salario);
              $('[name="cposicion_trabajo"]').val(data.posicion_trabajo);
              $('[name="cinicio_labores"]').val(data.inicio_labores);
              $('[name="ctiempo_laborando"]').val(data.tiempo_laborando);
              $('[name="cnombre_jefe_directo"]').val(data.nombre_jefe_directo);


            if (data.descuento != null) { $('[name="cdescuento"]').val(data.descuento); }
            if (data.descuento == "Si") {
              $('#inputSuccess19').removeAttr("readonly");
              $('#inputSuccess192').removeAttr("readonly");
              $('#oinputSuccess').removeAttr("readonly");
            }
            else {
              $('#inputSuccess19').attr("readonly","readonly");
              $('#inputSuccess192').attr("readonly","readonly");
              $('#oinputSuccess').attr("readonly","readonly");
            }
            $('[name="canho_descuento"]').val(data.anho_descuento);
            $('[name="cmes_descuento"]').val(data.mes_descuento);
            $('[name="ccantidad_descuento"]').val(data.cantidad_descuento);
            $('[name="ctipo_descuento"]').val(data.tipo_descuento);


              $('[name="cid"]').val(data.companias_id);
              $('[name="cnombre"]').val(data.companias_nombre);
              $('[name="cdireccion"]').val(data.companias_direccion);
              $('[name="ctelefono"]').val(data.companias_telefono);
              $('[name="cruc"]').val(data.companias_ruc);
              $('[name="csitio_web"]').val(data.companias_sitio_web);
              $('[name="cano_constitucion"]').val(data.companias_ano_constitucion);
              $('[name="ccorreo"]').val(data.companias_correo);
              $('[name="cnombre_rrhh"]').val(data.companias_nombre_rrhh);
              $('[name="ctelefono_rrhh"]').val(data.companias_telefono_rrhh);
              $('[name="cnombre_contabilidad"]').val(data.companias_nombre_contabilidad);
              $('[name="ctelefono_contabilidad"]').val(data.companias_telefono_contabilidad);
              $('[name="cforma_pago"]').val(data.companias_forma_pago);
              $('[name="ccantidad_empleados"]').val(data.companias_cantidad_empleados);
              $('[name="crubro"]').val(data.companias_rubro);
              $('[name="cnombre_firma"]').val(data.companias_nombre_firma);
              $('[name="cposicion_firma"]').val(data.companias_posicion_firma);

              method_compania = 'update';
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function change_companies(id)
  {
      $.ajax({
          url : "<?php echo site_url('mis_solicitudes/change_companies/')?>" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('[name="cid"]').val(data.id);
              $('[name="cnombre"]').val(data.nombre);
              $('[name="cdireccion"]').val(data.direccion);
              $('[name="ctelefono"]').val(data.telefono);
              $('[name="cruc"]').val(data.ruc);
              $('[name="csitio_web"]').val(data.sitio_web);
              $('[name="cano_constitucion"]').val(data.ano_constitucion);
              $('[name="ccorreo"]').val(data.correo);
              $('[name="cnombre_rrhh"]').val(data.nombre_rrhh);
              $('[name="ctelefono_rrhh"]').val(data.telefono_rrhh);
              $('[name="cnombre_contabilidad"]').val(data.nombre_contabilidad);
              $('[name="ctelefono_contabilidad"]').val(data.telefono_contabilidad);
              $('[name="cforma_pago"]').val(data.forma_pago);
              $('[name="ccantidad_empleados"]').val(data.cantidad_empleados);
              $('[name="crubro"]').val(data.rubro);
              $('[name="cnombre_firma"]').val(data.nombre_firma);
              $('[name="cposicion_firma"]').val(data.posicion_firma);

              method_compania = 'update';
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function saveCompania() {
    var url;
    if (method_compania == 'update') {
      url = "<?php echo site_url('mis_solicitudes/update_companias')?>";
    } else {
      url = "<?php echo site_url('mis_solicitudes/create_companias')?>";
    }
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_compania').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if (data.status)
            {
                //validateCompania();
                $('#msg-error-compania').hide();
                window.scrollTo(0, 0);
            }
            else if (data.validation)
            {
                $('#msg-error-compania').show();
                $('.list-errors-compania').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#msg-error-compania').show();
                $('.list-errors-compania').html(data.permission);
                porAprobar($('#solicitud_id').val());
            }
            else if (data.rol)
            {
                $('#msg-error-compania').show();
                $('.list-errors-compania').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
            }
            view_companies($('#solicitud_id').val());
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
  }

  function submitFileRp(){
      var formData = new FormData($('#form_rp')[0]);
      var id = $('#solicitud_id').val();
      $.ajax({
          url: "<?php echo site_url('mis_solicitudes/cargar_archivo_rp')?>",
          type: 'POST',
          data: formData,
          mimeType: "multipart/form-data",
          contentType: false,
          cache: false,
          processData: false,
          dataType: "JSON",
          success: function(data, textSatus, jqXHR){
              if (data.status) {
                  $('.permission-error-archivo').hide();
                  enviarAprobar();
              } else if(data.permission) {
                  $('.permission-error-archivo').show();
                  $('.permission-error-archivo').html(data.permission);
              } else if(data.test) {
                  porAprobar(id);
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              alert("error");
          }
      });
  }

  function updateVerificado(){
      var id = $('#solicitud_id').val();
      $.ajax({
          url: "<?php echo site_url('mis_solicitudes/update_verificado/')?>" + id,
          type: 'POST',
          dataType: "JSON",
          success: function(data, textSatus, jqXHR){
              if (data.status) {
                  window.location = "<?php site_url('mis_solicitudes/verificacion') ?>";
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              alert("error");
          }
      });
  }

   function searchCedula(){
    $('.alerta').hide();
    $('.aviso').hide();
    $('#resultado').hide();
    if ($('#buscar_cedula').val().length == 8) {
      $('#div_carga').show();
      $.ajax({
          url : "<?php echo site_url('mis_solicitudes/search_cedula')?>",
          type: "POST",
          data: $('#form_search').serialize(),
          dataType: "JSON",
          success: function(data)
          {
            $('#div_carga').hide();
            if (data.datos == "") {
              $('.alerta').show();
            } else {
              $('#resultado').html(data.datos);
              $('#resultado').show();
            }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
    }
  }

  function view_search(item){ 
    $('.alerta').hide();   
    $('.aviso').hide();
    $('#resultado').hide();
    $('#div_carga').show();
    $('#validcedu').hide();
    var selectCedula = $('#selectCedula'+item).val();
    var selectAccess = $('#dbaccess').val();
    $("#cedu_access").val(selectCedula);
    $.ajax({
      url : "<?php echo site_url('mis_solicitudes/seleccion_cedula')?>",
      type: "POST",
      data: {
        "selectCedula":selectCedula,
        "selectAccess":selectAccess
      },
      dataType: "JSON",
      success: function(data)
      {          
        $('#div_carga').hide();
        if (data.datos == "") {
          $('.alerta').show();
        } else {
          $('#resultado').show();
          $('#resultado').html(data.datos);
          $("#checkvalido").removeAttr("disabled");
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
    });
  }


  function acept_test(id){
      $.ajax({
          url: "<?php echo site_url('mis_solicitudes/update_test/')?>" + id,
          type: 'POST',
          dataType: "JSON",
          success: function(data, textSatus, jqXHR){
              if (data.status) {
                  $('#div-verificado').hide();
                  $('#list-verificados').show();
                  reload_table();
                  window.scrollTo(0, 0);
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              alert("error");
          }
      });
  }

  function view_companies(id)
  {
      $.ajax({
          url : "<?php echo site_url('clientes/view_companies')?>",
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('#vinputSuccess3').empty();
              var trHTML = "<option></option>";
              if (data.length > 0) {
                  $.each(data, function (i, item) {
                      trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                  });
              }
              $('#vinputSuccess3').append(trHTML);
              $("#vinputSuccess3").select2({
                placeholder: "Empresa",
                allowClear: false,
                language: "es",
                theme: "classic",
                dropdownParent: $(".selectcompany")
              });

              if (id > 0) {
                verify_companies(id); 
              }
              
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
  }

  function selectEmpresa(){
    $('.crear-compania').text('');
    var companias_id = $('#vinputSuccess3').val();
    if (companias_id > 0) {
      $('.etiqueta').text("Compañia:");
      change_companies(companias_id);
    }
    
  }

  function add_companies(){
      window.scrollTo(0,500);
      view_companies(null);
      clear_companies();
  }

  function clear_companies(){
    $('.crear-compania').text('Crear nueva compañia');
    $('.etiqueta').text("Nueva Compañia:");
    $('[name="cid"]').val(null);
    $('[name="cnombre"]').val(null);
    $('[name="cdireccion"]').val(null);
    $('[name="ctelefono"]').val(null);
    $('[name="cruc"]').val(null);
    $('[name="csitio_web"]').val(null);
    $('[name="cano_constitucion"]').val(null);
    $('[name="ccorreo"]').val(null);
    $('[name="cnombre_rrhh"]').val(null);
    $('[name="ctelefono_rrhh"]').val(null);
    $('[name="cnombre_contabilidad"]').val(null);
    $('[name="ctelefono_contabilidad"]').val(null);
    $('[name="cforma_pago"]').val(null);
    $('[name="ccantidad_empleados"]').val(null);
    $('[name="crubro"]').val(null);
    $('[name="cnombre_firma"]').val(null);
    $('[name="cposicion_firma"]').val(null);
    method_compania = 'create';
  }


  function select_access(id){
    $('#validcedu').hide();
    $('#msg-error').hide();
    $('[name="db_access_id"]').val(id);
    $('#div-seleccion').hide();
    $('#div_carga').show();
    var cedula = $('[name="cedulaaccess"]').val();
    $("#cedu_access").val(cedula);
    $.ajax({
      url : "<?php echo site_url('mis_solicitudes/div_seleccion')?>",
      type: "POST",
      data: {
        "cedula": cedula,
        "db_access_id": id
      },
      dataType: "JSON",
      success: function(data)
      {          
        $('#div-seleccion').show();
        $('#div-seleccion').html(data);
        $('#div_carga').hide();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        $('#div_carga').hide();
        alert('Error get data from ajax');
      }
    });


  }

  function file_client(id){
      $('#filtrar_tipos_id').prop('selectedIndex',0);
      $('.permission-error').hide();
      $('#modal_form_files').modal('show');
      $('.modal-title').text('Archivos del Cliente');
      $('[name="cliente_id"]').val(id);
      view_files(id);
  }

  function view_files(id){
      $('.permission-error-archivo').hide();
      $('.myForm')[0].reset();
      $('#filtrar_tipos_id').prop('selectedIndex',0);
      $.ajax({
          url : "<?php echo site_url('clientes/view_files/')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('.filas').empty();
              $('.modal-nombre-archivo').text(data.cliente);
              var trHTML = '';
              if (data.data.length > 0) {          
                  $.each(data.data, function (i, item) {
                      trHTML += '<tr>' +
                        '<td>' + item[0] + '</td>' +
                        '<td>' + item[1] + '</td>' +
                        '<td>' + item[2] + '</td>' +
                        '<td>' + item[3] + '</td>' +
                        '<td>' + item[4] + '</td>' +
                        '<td>' + item[5] + '</td>' +
                        '<td>' + item[6] + '</td>' +
                      '</tr>';
                  });                
              } else {
                  trHTML += '<tr>' +
                    '<td colspan="6" align="center">No hay archivos...</td>' +
                  '</tr>';
              }
              $('.filas').append(trHTML);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });
  }

  function submitFile(){
      var formData = new FormData($('.myForm')[0]);
      var id = $('#clientes_id').val();
      $.ajax({
          url: "<?php echo site_url('clientes/cargar_archivo')?>",
          type: 'POST',
          data: formData,
          mimeType: "multipart/form-data",
          contentType: false,
          cache: false,
          processData: false,
          dataType: "JSON",
          success: function(data, textSatus, jqXHR){
              if (data.status) {
                  view_files(id);
              } else if(data.permission) {
                  $('.permission-error-archivo').show();
                  $('.permission-error-archivo').html(data.permission);
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              alert("error");
          }
      });
  }

  function confirm_delete_file(file_id) {

      $('.permission-error-archivo').hide();

      $("#myModal").on("show", function() {
          $("#myModal a.btn").on("click", function(e) {
              console.log("button pressed");
              $("#myModal").modal('hide');
          });
      });

      $("#myModal").on("hide", function() {
          $("#myModal a.btn").off("click");
      });
      
      $("#myModal").on("hidden", function() {
          $("#myModal").remove();
      });
      
      $("#myModal").modal({
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true
      });

      $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_file('+ file_id +')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

  }


  function delete_file(file_id)
  {
      $('.permission-error-archivo').hide();
      var id = $('#clientes_id').val();
      $.ajax({
          url : "<?php echo site_url('clientes/file_delete/')?>/" + file_id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {

              if (data.status) {
                  view_files(id);
              }
              if(data.error)
              {
                  $('.permission-error-archivo').show();
                  $('.permission-error-archivo').html(data.error);
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              //alert('Error deleting data');
              $('.permission-error-archivo').show();
              $('.permission-error-archivo').html('No existe archivo en el directorio.');
          }
      });
      $("#myModal").modal('hide');
  }

  function filtrar_archivos(cliente_id){
      var id = $('#filtrar_tipos_id').val();
      $.ajax({
          url : "<?php echo site_url('clientes/filtrar_files/')?>/" + id,
          type: "POST",
          data: {"cliente_id":cliente_id},
          dataType: "JSON",
          success: function(data)
          {
              $('.filas').empty();
              $('.modal-nombre-archivo').text(data.cliente);
              var trHTML = '';
              if (data.data.length > 0) {          
                  $.each(data.data, function (i, item) {
                      trHTML += '<tr>' +
                        '<td>' + item[0] + '</td>' +
                        '<td>' + item[1] + '</td>' +
                        '<td>' + item[2] + '</td>' +
                        '<td>' + item[3] + '</td>' +
                        '<td>' + item[4] + '</td>' +
                        '<td>' + item[5] + '</td>' +
                        '<td>' + item[6] + '</td>' +
                      '</tr>';
                  });                
              } else {
                  trHTML += '<tr>' +
                    '<td colspan="6" align="center">No hay archivos...</td>' +
                  '</tr>';
              }
              $('.filas').append(trHTML);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });
  }


/*Modal dentro de modal*/
$(document).ready(function () {
    $('#openBtn').click(function () {
        $('#modal_form').modal()
    });

    $('.modal')
        .on({
            'show.bs.modal': function() {
                var idx = $('.modal:visible').length;
                $(this).css('z-index', 1040 + (10 * idx));
            },
            'shown.bs.modal': function() {
                var idx = ($('.modal:visible').length) - 1; // raise backdrop after animation.
                $('.modal-backdrop').not('.stacked')
                .css('z-index', 1039 + (10 * idx))
                .addClass('stacked');
            },
            'hidden.bs.modal': function() {
                if ($('.modal:visible').length > 0) {
                    // restore the modal-open class to the body element, so that scrolling works
                    // properly after de-stacking a modal.
                    setTimeout(function() {
                        $(document.body).addClass('modal-open');
                    }, 0);
                }
            }
        });
});
/*End Modal dentro de modal*/




function notas_clientes(id){
  $('#modal_form_notas').modal('show');
  $('.modal-title').text('Añadir Nota');
  $('#form_notas')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="clientes_id"]').val(id);
  $('#msg-error-notas').hide();
  //$('.nombrenota').hide();
  //view_notas();
  datos_notas(id);
}


function datos_notas(id){
  $.ajax({
      url : "<?php echo site_url('solicitudes/notas_cliente/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

          $('.modal-nombre-nota').text(data.nombre_cliente);

          $('.datos_notas').empty();
          var trHTML = '';
          if (data.nota.length > 0) {
              $('.notes_datos').show();
              $.each(data.nota, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>'+item.fecha_creacion+'</td>' +
                    '<td>'+item.nota+'</td>' +
                    '<td>'+item.usuarios_id+'</td>' +
                  '</tr>'
              });
          } else {
            $('.notes_datos').hide();
          }
          $('.datos_notas').append(trHTML);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function save_notes(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_notes')?>",
      type: "POST",
      data: $('#form_notas').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            notas_clientes($('#clientes_id').val());
            $('#msg-error-notas').hide();
          } else if (data.validation) {
            $('#msg-error-notas').show();
            $('.list-errors-notas').html('<div class="animated shake">'+data.validation+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}


function refreshIframe(){
  $('.ciframe').empty();
  $('.ciframe').html('<iframe src="https://online.apc.com.pa/Login/tabid/468/Default.aspx?returnurl=%2fdefault.aspx" width="1000" height="1000" frameborder="0" allowfullscreen> </iframe>');
}

function refreshIframe1(){
  $('.ciframe1').empty();
  $('.ciframe1').html('<iframe src="https://rp.gob.pa/ValidacionDigital.aspx" width="1000" height="750" frameborder="0" allowfullscreen> </iframe>');
}

function confirm_deselect_request(id) {
    $('.permission-error-archivo').hide();    
    $("#myModal4").modal("show");
    $("#info4").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="deselect_request('+ id +')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function deselect_request(id){
    $.ajax({
        url : "<?php echo site_url('solicitudes/deselect_request/')?>" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          if (data.status) {
              reload_table();
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    $("#myModal4").modal('hide');
}


function view_notas()
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_notas')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#notas_id').empty();
            var trHTML = "<option></option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            trHTML += "<option value='Otro'>Otro</option>";
            $('#notas_id').append(trHTML);

            $("#notas_id").select2({
              placeholder: "Notas",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".notas_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function notas(id){
  if (id == 'Otro') {
    $('#nota').removeAttr('readonly',false);
    $('[name="nota"]').val(null);
    $('.nombrenota').show();
  } else {
    $('.nombrenota').hide();
    $('#nota').attr('readonly','readonly');
    $.ajax({
        url : "<?php echo site_url('admin/view_nota/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nota"]').val(data.nota);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('[name="nota"]').val(null);
        }
    }); 
  }
}




function disposiciones_for_select(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/disposiciones_for_select')?>",
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        if (data) {
          var trHTML = '<option value="0">Todos<option>';
          $.each(data, function(i,item){
            trHTML += '<option value='+item.id+'>'+item.nombre+'<option>';
          });
          $("#filtrar_disposiciones").html(trHTML);
          $("#filtrar_disposiciones").select2({
            placeholder: "Filtrar por Disposición",
            allowClear: false,
            language: "es",
            theme: "classic",
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function disposiciones(id){
  $('#modal_form_disposiciones').modal('show');
  $('.modal-title').text('Añadir Disposición');
  $('#form_disposiciones')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="solicitud_id"]').val(id);
  $('#msg-error-disposiciones').hide();
  view_disposiciones(id);
}


function view_disposiciones(id)
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_disposiciones/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('.modal-nombre-disposicion').text(data.nombre_cliente);
            
            $('#disposiciones_id').empty();
            var trHTML = "<option></option>";
            if (data.data.length > 0) {
                $.each(data.data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#disposiciones_id').append(trHTML);

            $("#disposiciones_id").select2({
              placeholder: "Disposiciones",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".disposiciones_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save_disposicion(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_disposicion')?>",
      type: "POST",
      data: $('#form_disposiciones').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            $('#modal_form_disposiciones').modal('hide');
            if ($('[name="bandera"]').val() > 0) {
               window.location = "<?php site_url('mis_solicitudes/verificacion') ?>";
            } else {
              reload_table();
            }
          } else if (data.validation) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.validation+'</div>');
          } else if (data.permission) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.permission+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function filtrar_disposiciones(disposicion){
  table.ajax.url("<?php echo site_url('mis_solicitudes/my_list/Por Verificar/')?>" + disposicion).load();
}


function select_descuento(val){
    if (val == "Si") {
        $('#inputSuccess19').removeAttr("readonly");
        $('#inputSuccess192').removeAttr("readonly");
        $('#oinputSuccess').removeAttr("readonly");
        $('#oinputSuccess1').removeAttr("disabled");
    } else {
        $('#inputSuccess19').val(null);
        $('#inputSuccess19').attr("readonly","readonly");
        $('#inputSuccess192').val(null);
        $('#inputSuccess192').attr("readonly","readonly");
        $('#oinputSuccess').val(null);
        $('#oinputSuccess').attr("readonly","readonly");
        $('#oinputSuccess1').prop('selectedIndex',0);
        $('#oinputSuccess1').attr("disabled","disabled");
    }
}

function justNumbers(e)
{
   var keynum = window.event ? window.event.keyCode : e.which;
   if ((keynum == 8) || (keynum == 46))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function posee_capacidad(){

    var salario = $('#inputSuccess25').val();
    var descuento = $('#oinputSuccess').val();

    if (salario >= 0 && salario != "") {
        if (descuento > 0) {
            if ($('#oinputSuccess1').val() == "Quincenal") {
                descuento = descuento * 2;
            }
            if (descuento <= (salario * 0.2)) {
                $('#inputSuccess20').prop('selectedIndex',1);
            } else {
                $('#inputSuccess20').prop('selectedIndex',2);
            }
        } else {
            $('#inputSuccess20').prop('selectedIndex',1);
        }
    } else {
        $('#inputSuccess25').val(null);
        $('#inputSuccess20').prop('selectedIndex',0);
    }
}

</script>

