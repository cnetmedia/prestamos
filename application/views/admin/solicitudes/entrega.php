        <style type="text/css">
          .faltante{
            background-color: #1ABB9C;
          }
          .cobrado{
            background-color: #3498DB;
          }
        </style>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row" id="list-entrega">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Solicitudes Por Entregar <small>Listado de Solicitudes</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>

                    <div class="row col-lg-3 pull-right">
                      <select class='form-control' name="filtrar_disposiciones" id="filtrar_disposiciones" onchange="filtrar_disposiciones(this.value)">
                      </select>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>

                    <br><br>

                    <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Compañia</th>
                          <th>Aprobado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Compañia</th>
                          <th>Aprobado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Acciones</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
            </div>

            <div class="row" id="div-entrega">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Solicitudes <small>Entrega</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="text-info">Aprobación de Prestamo</h2>
                    </div>

                    <div class="clearfix"></div>
                    <div class="table_cotiza"></div>
                    <div class="clearfix"></div>
                    <div class="table_refinanciamiento"></div>
                    <div class="table_cobro"></div>
                    <div class="clearfix"></div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <hr>
                    </div>

                    <div class="clearfix"></div>
                    <div class="table_empresa"></div>
                    <div class="clearfix"></div>

                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <hr>
                    </div>

                    <form id="form_entrega" class="form-horizontal form-label-left input_mask" enctype="multipart/form-data">

                      <input type="hidden" name="solicitud_id">
                      <input type="hidden" name="clientes_id">

                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <div id="validacion"></div>
                      </div>

                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <div class="row col-lg-6 col-sm-6 col-12">                            
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        Examinar&hellip; <input type="file" name="file" style="display: none;" multiple>
                                    </span>
                                </label>
                                <input type="text" name="nombre_archivo" class="form-control" readonly>
                            </div>
                        </div>
                      </div>


                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>N° Cheque</label>
                        <input type="text" name="numero_cheque" required class="form-control has-feedback-left date-picker" id="numero_cheque" placeholder="N° Cheque">
                        <span class="fa fa-sticky-note-o form-control-feedback left" aria-hidden="true"></span>
                      </div>


                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Fecha de Primer Descuento</label>
                        <input type="text" name="fecha_primer_descuento" required class="form-control has-feedback-left date-picker" id="inputSuccess" placeholder="Fecha de Primer Descuento">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                      </div>


                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Fecha de Entrega</label>
                        <input type="text" name="fecha_entrega" required class="form-control has-feedback-left date-picker" id="inputSuccess1" placeholder="Fecha de Entrega">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                      </div>

<!--                       <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Metodo de Pago</label>
                        <select required class='form-control' name='metodo_pago' id='metodo_pago' placeholder='Metodo de Pago'>
                          <option disabled selected hidden>Metodo de Pago</option>
                          <option value="Efectivo">Efectivo</option>
                          <option value="Cheque">Cheque</option>
                          <option value="Transferencia">Transferencia</option>
                        </select>
                      </div> -->

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Porcentaje a Pagar</label><br>
                         <input type="radio" name="porcentaje" id="porcentaje50" value="50" disabled /> 50%
                        <input type="radio" name="porcentaje" id="porcentaje100" value="100" disabled /> 100%
 <!--                       <input type="radio" name="porcentaje" id="porcentaje50" value="50" /> 50%
                        <input type="radio" name="porcentaje" id="porcentaje100" value="100" /> 100% -->

                      </div>

                    </form>

                    <button type="button" onclick="rechazar()" class="btn btn-warning btn-xs pull-right"><i class="fa fa-close"></i> Rechazar</button>
                    <button type="button" onclick="cancelar()" class="btn btn-danger btn-xs pull-right"><i class="fa fa-close"></i> Cancelar</button>
                    <button class="btn btn-success btn-xs pull-right" onclick="procesar()"><i class="fa fa-check"></i> Procesar</button>
                    <button class="btn btn-warning btn-xs pull-right" onclick="notas_clientes_form()"><i class="fa fa-file"></i> Notas</button>
                    <button type="button" id="btnDisposiciones" onclick="disposiciones_clientes_form()" class="btn btn-warning btn-xs pull-right"><i class="fa fa-bookmark"></i> Disposiciones</button>

                  </div>
                </div>
              </div>
            </div>

        </div>
        <!-- /page content -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información de la Solicitud</h3>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-xs-12 bottom">

                  <div class="clearfix"></div>
                  <div class="table_view"></div>
                  <div class="clearfix"></div>

                </div> 
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea eliminar el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Modal para aprobar -->
<div id="myModal2" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Esta seguro de entregar esta solicitud?
      </div>
      <div class="modal-footer">
        <a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="entregado()" data-dismiss="modal"><i class="fa fa-check"></i> Si</a>
        <a class="btn btn-danger btn-xs" href="javascript:void(0)" data-dismiss="modal"><i class="fa fa-close"></i> No</a>
      </div>
    </div>
  </div>
</div>
<!-- Fin de modal aprobar -->


<!-- Upload modal -->
<div class="modal fade" id="modal_form_files" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Archivos del Cliente</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre-archivo text-primary"><!-- Nombre Cliente --></h2></u></strong>
                    </div>

                     <form method="POST" class="myForm" enctype="multipart/form-data">
                          
                        <input type="hidden" value="" name="cliente_id" id="client"/>

                        <div class="row col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                          <div class="row checkbox">
                            <label>
                              <input type="checkbox" class="flat" name="archivo_original" value="Si"> Archivo Original
                            </label>
                          </div>
                        </div>

                        <div class="col-lg-6 col-sm-6 col-12">                            
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        Examinar&hellip; <input type="file" name="file" style="display: none;" multiple>
                                    </span>
                                </label>
                                <input type="text" name="nombre_archivo" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-4 col-12">
                          <?php echo "<select required class='form-control' name='tipos_archivos_id' id='tipos_archivos_id' placeholder='Tipo de Archivo'><option disabled selected hidden>Tipo de Archivo</option>";
                          if (count($result_tipo_archivos)) {  
                            foreach ($result_tipo_archivos as $row) {
                              echo "<option value='". $row['id'] . "'>" . $row['tipo'] . "</option>";
                            }
                          }
                          echo "</select>";?>
                        </div>


                        <div class="col-lg-2 col-sm-2 col-12">
                            <button type="button" class="btn btn-success" onclick="submitFile();">
                                <i class="fa fa-upload"></i> Subir
                            </button>
                        </div> 

                        <div class="col-lg-12 col-sm-12 col-12">
                            <textarea id="descripcion_archivo" required class="form-control" name="descripcion_archivo" placeholder="Descripción"></textarea>
                        </div>                  

                    </form>

                    <div class="col-lg-12 col-sm-12 col-12">
                        <span class="permission-error-archivo label label-danger"></span>
                    </div>

                    <hr>

                    <div class="col-lg-3 col-sm-3 col-12">
                      <select required class='form-control' name='filtrar_tipos_id' id='filtrar_tipos_id' placeholder='Filtrar por' onchange="filtrar_archivos($('#client').val())"><option disabled selected hidden>Filtrar por</option>
                        <?php if (count($result_tipo_archivos)) {  
                          foreach ($result_tipo_archivos as $row) {
                            echo "<option value='". $row['id'] . "'>" . $row['tipo'] . "</option>";
                          }
                        } ?>
                      </select>
                    </div>

                    <div class="col-lg-3 col-sm-3 col-12">
                      <button class="btn btn-default" onclick="view_files($('#client').val())"><i class="glyphicon glyphicon-refresh"></i> Reiniciar</button>
                    </div>

                    <div class="col-lg-12 col-sm-12 col-12 table-responsive">
                        <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Archivo</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Original</th>
                                    <th>Ultima Modificación</th>
                                    <th>Creado Por</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="filas">
                            <tfoot>
                                <tr>
                                    <th>Archivo</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Original</th>
                                    <th>Ultima Modificación</th>
                                    <th>Creado Por</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>

                </div> 
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End Upload modal  -->

<!-- Modal para rechazo -->
<div id="myModal3" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div id="test"><!-- Texto --></div>
      </div>
      <div id="info2" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal rechazo -->



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-nota text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="clientes_id" id="clientes_id" />
                <input type="hidden" value="" name="bandera"/>

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

<!--                 <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback notas_id">
                  <select class="form-control" name="notas_id" id="notas_id" style="width: 100%" onchange="notas(this.value)"></select>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback nombrenota">
                  <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre">
                </div> -->

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

              <div class="table-responsive notes_datos">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Fecha</th>
                      <!-- <th class="column-title">Nombre</th> -->
                      <th class="column-title">Nota</th>
                      <th class="column-title">Registrado por</th>
                    </tr>
                  </thead>

                  <tbody class="datos_notas">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


<!-- Modal para deselegir -->
<div id="myModal4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea deseleccionar la solicitud?
      </div>
      <div id="info4" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal deselegir -->



    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_impresiones" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Impresiones</h3>
                </div>
                <div class="modal-body">

                  <input type="hidden" name="clieid">
                  <span class="label label-danger aviso"></span>
                  <select class="form-control" name="impresion" id="impresion" onchange="seleccion_reporte(this.value)">
                    
                  </select>

                </div>
                <div class="modal-footer botones_pdf">
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_disposiciones" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Disposición</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-disposicion text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_disposiciones" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="solicitud_id"/>

                <div id="msg-error-disposiciones" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-disposiciones animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback disposiciones_id">
                  <select class="form-control" name="disposiciones_id" id="disposiciones_id" style="width: 100%"></select>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_disposicion()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Asignar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->
    

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>



<script type="text/javascript">

  $('#div-entrega').hide();

  var save_method;
  var table;

  $(document).ready(function() {
      table = $('#datatable-responsive-solicitudes').DataTable( {
          "order": [],
          "destroy": true,
          "bRetrieve": true,
          "language": {
              "lengthMenu": "Mostrar _MENU_ registros por página",
              "zeroRecords": "Lo siento, no hay registros",
              "info": "Página _PAGE_ de _PAGES_",
              "infoEmpty": "No hay registros disponibles",
              "infoFiltered": "(filtro de _MAX_ registros en total)",
              "sSearch": "Buscar",
              "oPaginate": {
                  "sFirst":    "Primero",
                  "sLast":     "Último",
                  "sNext":     "Siguiente",
                  "sPrevious": "Anterior"
              },
              "processing": "Procesando..."
          },
          "processing": true,
          "ajax": {
              "url": "<?php echo site_url('mis_solicitudes/list_aprobados/Por Entregar')?>",
              "type": "POST"
          }
      } );


    $("#filtrar_disposiciones").select2({
      placeholder: "Filtrar por Disposición",
      allowClear: false,
      language: "es",
      theme: "classic",
    });

    disposiciones_for_select();


  });

  function reload_table()
  {
      $('.permission-error').hide();
      table.ajax.reload(null,false); //reload datatable ajax 
  }

  function view_request(id)
  {
      $('.permission-error').hide();
      $.ajax({
          url : "<?php echo site_url('mis_solicitudes/info_cotizacion/')?>" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('#modal_form_view').modal('show');
              $('.modal-title').text('Información de la Solicitud');

              $('.table_view').html('<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">'+data.datos.nombre_cliente+'</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td>Salario devengado: $'+data.datos.salario+'</td>'+
                      '<td>Producto: '+data.datos.nombre_producto+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Cantidad solicitada: $'+data.datos.cantidad_solicitada+'</td>'+
                      '<td>Préstamo aprobado: $'+data.datos.cantidad_aprobada+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td></td>'+
                      '<td>Termino aprobado: '+data.datos.cantidad_meses+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td></td>'+
                      '<td>Pago quincenal: $'+data.datos.pago_quincenal+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td></td>'+
                      '<td>Pago mensual: $'+data.datos.pago_mensual+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td></td>'+
                      '<td>Obligación Total: $'+data.datos.totalpagar+'</td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>');
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });

  }


  function confirm_delete(id) {

      $('.permission-error').hide();

      $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
          $("#myModal a.btn").on("click", function(e) {
              console.log("button pressed");   // just as an example...
              $("#myModal").modal('hide');     // dismiss the dialog
          });
      });

      $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
          $("#myModal a.btn").off("click");
      });
      
      $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
          $("#myModal").remove();
      });
      
      $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true                     // ensure the modal is shown immediately
      });

      $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_request('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

  }


  function delete_request(id) {
    if(id) {
      $.ajax({
          url : "<?php echo site_url('solicitudes/ajax_delete')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              $('#modal_form').modal('hide');
              reload_table();
              if(data.permission)
              {
                  $('.permission-error').show();
                  $('.permission-error').html(data.permission);
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error deleting data');
          }
      });
      $("#myModal").modal('hide');
    }
  }

  function entrega(id){
    $('#list-entrega').hide();
    $('#div-entrega').show();
    $('#form_entrega')[0].reset();
    $.ajax({
        url : "<?php echo site_url('mis_solicitudes/info_cotizacion/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
          if (data.datos) {

            $('[name="solicitud_id"]').val(id);
            $('[name="clientes_id"]').val(data.datos.cliente_id);

              $('.table_cotiza').html('<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">Cliente: '+data.datos.nombre_cliente+'</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td>Salario devengado: $'+data.datos.salario+'</td>'+
                      '<td>Producto: '+data.datos.nombre_producto+'</td>'+
                      '<td>Pago quincenal: $'+data.datos.pago_quincenal+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Cantidad solicitada: $'+data.datos.cantidad_solicitada+'</td>'+
                      '<td>Préstamo aprobado: $'+data.datos.cantidad_aprobada+'</td>'+
                      '<td>Pago mensual: $'+data.datos.pago_mensual+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td></td>'+
                      '<td>Termino aprobado: '+data.datos.cantidad_meses+'</td>'+
                      '<td>Obligación Total: $'+data.datos.totalpagar+'</td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>');

              $('.table_empresa').html('<div class="table-responsive">'+
                '<table class="table table-striped">'+
                  '<thead>'+
                    '<tr class="headings">'+
                      '<th colspan="3"><h2 class="text-info">Compañia: '+data.datos.empresa+'</h2></th>'+
                    '</tr>'+
                  '</thead>'+
                  '<tbody>'+
                  '</tbody>'+
                    '<tr>'+
                      '<td colspan="3">Dirección: '+data.datos.direccione+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Teléfono: '+data.datos.telefonoe+'</td>'+
                      '<td>Ruc: '+data.datos.ruce+'</td>'+
                      '<td>Sitio Web: '+data.datos.sitiowebe+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Año de Constitución: '+data.datos.anoconstitucion+'</td>'+
                      '<td>Correo: '+data.datos.correoe+'</td>'+
                      '<td>Nombre RRHH: '+data.datos.nombrerrhh+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Teléfono RRHH: '+data.datos.telefonorrhh+'</td>'+
                      '<td>Nombre de Contabilidad: '+data.datos.nombrecontabilidad+'</td>'+
                      '<td>Teléfono Contabilidad: '+data.datos.telefonocontabilidad+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Forma de Pago: '+data.datos.formapago+'</td>'+
                      '<td>Cantidad de Empleados: '+data.datos.cantidadempleados+'</td>'+
                      '<td>Rubro: '+data.datos.rubroe+'</td>'+
                    '</tr>'+
                    '<tr>'+
                      '<td>Nombre Firma: '+data.datos.nombrefirma+'</td>'+
                      '<td>Posición Firma: '+data.datos.posicionfirma+'</td>'+
                      '<td></td>'+
                    '</tr>'+
                  '</tbody>'+
                '</table>'+
              '</div>');

              if (data.refinanciamiento) {
                $('.table_refinanciamiento').html('<hr><div class="table-responsive">'+
                  '<table class="table table-striped">'+
                    '<thead>'+
                      '<tr class="headings">'+
                        '<th colspan="3"><h2 class="text-info">Solicitud Anterior</h2></th>'+
                      '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                    '</tbody>'+
                      '<tr>'+
                        '<td>Salario devengado: $'+data.datos.salario+'</td>'+
                        '<td>Producto: '+data.refinanciamiento.nombre_producto+'</td>'+
                        '<td>Pago quincenal: $'+data.refinanciamiento.pago_quincenal+'</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td>Cantidad solicitada: $'+data.datos.cantidad_solicitada+'</td>'+
                        '<td>Préstamo aprobado: $'+data.refinanciamiento.cantidad_aprobada+'</td>'+
                        '<td>Pago mensual: $'+data.refinanciamiento.pago_mensual+'</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td></td>'+
                        '<td>Termino aprobado: '+data.refinanciamiento.cantidad_meses+'</td>'+
                        '<td>Obligación Total: $'+data.refinanciamiento.totalpagar+'</td>'+
                      '</tr>'+
                    '</tbody>'+
                  '</table>'+
                '</div>');

                var trHTML = '<div class="table-responsive">'+
                  '<table class="table table-striped table-bordered dt-responsive nowrap" id="datatable-cotizacion" cellspacing="0" width="100%">'+
                    '<thead>'+
                      '<tr>'+
                        '<th style="text-align: center; background-color: #9CC2CB;"><input type="checkbox" id="numtotal" disabled checked></th>'+
                        '<th>Fecha de Pagos</th>'+
                        '<th>Principal</th>'+
                        '<th>Interés</th>'+
                        '<th style="text-align:center">Tasa de Interés</th>'+
                        '<th style="text-align:center">Cargo Administrativo</th>'+
                        '<th>Total</th>'+
                      '</tr>'+
                    '</thead>'+
                    '<tbody>';
                $.each(data.cobro.data, function (i, item) {
                    trHTML += '<tr>' +
                      '<td align="center"><input type="checkbox" value="' + item.num + '" id="numcobro' + item.num + '" disabled></td>' +
                      '<td>' + item.fecha + '</td>' +
                      '<td>$' + item.principal.toFixed(2) + '</td>' +
                      '<td>$' + item.interes.toFixed(2) + '</td>' +
                      '<td align="center">%' + item.tasa.toFixed(2) + '</td>' +
                      '<td align="center">$' + item.cargo.toFixed(2) + '</td>' +
                      '<td>$' + item.total.toFixed(2) + '</td>' +
                    '</tr>';
                });

                trHTML += '<tr>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td><span class="label label-warning">$'+ data.cobro.totalprincipal.toFixed(2) +'</span></td>'+
                        '<td><span class="label label-info">$'+ data.cobro.totalintereses.toFixed(2) +'</span></td>'+
                        '<td></td>'+
                        '<td align="center"><span class="label label-primary">$'+ data.cobro.totalcargo.toFixed(2) +'</span></td>'+
                        '<td><span class="label label-success">$'+ data.cobro.totalcobrar.toFixed(2) +'</span></td>'+
                        '</tr>'+
                      '</tbody>'+
                    '</table>'+
                  '</div>'+
                  '<div class="clearfix"></div>'+
                  '<div class="table-responsive">'+
                    '<table class="table table-striped">'+
                      '<tbody>'+
                        '<tr>'+
                          '<td><i class="fa fa-square aero"></i> Obligación Total: $'+data.info.obligaciontotal.toFixed(2)+'</td>'+
                          '<td><i class="fa fa-square blue"></i> Cobrado: $'+data.info.cobrado.toFixed(2)+'</td>'+
                          '<td>Aprobado Actual: $'+data.info.aprobado+'</td>'+
                        '</tr>'+
                        '<tr>'+
                          '<td></td>'+
                          '<td><i class="fa fa-square green"></i> Faltante: $'+data.info.faltante.toFixed(2)+'</td>'+
                          '<td>ENTREGAR SOLO: $'+data.info.entregar.toFixed(2)+'</td>'+
                        '</tr>'+
                      '</tbody>'+
                    '</table>'+
                  '</div>';
                $('.table_cobro').html(trHTML);

                var num = data.cobro.cobros + 1;

                if (data.cobro.cobros > 0) {
                  for (var i = 1; i < num; i++) {
                    $("#numcobro"+i).attr("checked", true);
                    $("#numcobro"+i).parent().addClass("cobrado");
                  }
                }

                for (var i = data.cobro.cobros+1; i <= data.cobro.cantfaltante + data.cobro.cobros; i++) {
                  $("#numcobro"+i).prop("checked", true);
                  $("#numcobro"+i).parent().addClass("faltante");
                }

              }
          } else if (data.cotize) {
            $('.table_cotiza').html(data.cotize);
          }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });


    $('#inputSuccess').datetimepicker({
        format: "DD/MM/YYYY"
    });
    $("#inputSuccess").on("dp.change",function (e) {
       fecha_porcentaje();
    });


    $('#inputSuccess1').datetimepicker({
        format: "DD/MM/YYYY h:mm A",
        maxDate: new Date(),
    });  

  }

  function procesar(){
    $("#myModal2").on("show", function() {
        $("#myModal2 a.btn").on("click", function(e) {
            console.log("button pressed");
            $("#myModal2").modal('hide');
        });
    });

    $("#myModal2").on("hide", function() {
        $("#myModal2 a.btn").off("click");
    });
    
    $("#myModal2").on("hidden", function() {
        $("#myModal2").remove();
    });
    
    $("#myModal2").modal({
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true
    });
  }

  function fecha_porcentaje(){
    $('#validacion').empty();
    $('#porcentaje50').prop('checked', false);
    $('#porcentaje100').prop('checked', false);
    $.ajax({
        url : "<?php echo site_url('mis_solicitudes/fecha_porcentaje')?>",
        type: "POST",
        data: {"fecha": $('#inputSuccess').val()},
        dataType: "JSON",
        success: function(data)
        {

            //alert(data);

            if (data == false) {
                $('#validacion').html('<span class="label label-danger">Fecha no valida</span>');
            } else {
                if (data == "100%") {
                    $('#porcentaje100').prop('checked', true);
                } else {
                    $('#porcentaje50').prop('checked', true);
                }
            }

/*            if (data == "50%") {
              $('#porcentaje50').prop('checked', true);
            } else if (data == "100%") {
              $('#porcentaje100').prop('checked', true);
            } else {
              $('#validacion').html('<span class="label label-danger">Fecha no valida</span>');
            }*/
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
  }

  function entregado(){
    $('#porcentaje50').attr('disabled', false);
    $('#porcentaje100').attr('disabled', false);
    var formData = new FormData($('#form_entrega')[0]);
    $.ajax({
        url : "<?php echo site_url('mis_solicitudes/entregar')?>",
        type: "POST",
        data: formData,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {
            if (data.status) {
              window.location = "<?php site_url('mis_solicitudes/entrega') ?>";
            } else if (data.validation) {
              $('#validacion').html(data.validation);
            }
            $('#porcentaje50').attr('disabled', true);
            $('#porcentaje100').attr('disabled', true);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
            $('#porcentaje50').attr('disabled', true);
            $('#porcentaje100').attr('disabled', true);
        }
    });

  }

  function cancelar(){
    window.location = "<?php site_url('mis_solicitudes/entrega') ?>";
  }

  function file_client(id){
      $('#filtrar_tipos_id').prop('selectedIndex',0);
      $('.permission-error').hide();
      $('#modal_form_files').modal('show');
      $('.modal-title').text('Archivos del Cliente');
      $('[name="cliente_id"]').val(id);
      view_files(id);
  }

  function view_files(id){
      $('.permission-error-archivo').hide();
      $('.myForm')[0].reset();
      $('#filtrar_tipos_id').prop('selectedIndex',0);
      $.ajax({
          url : "<?php echo site_url('clientes/view_files/')?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
              $('.filas').empty();
              $('.modal-nombre-archivo').text(data.cliente);
              var trHTML = '';
              if (data.data.length > 0) {          
                  $.each(data.data, function (i, item) {
                      trHTML += '<tr>' +
                        '<td>' + item[0] + '</td>' +
                        '<td>' + item[1] + '</td>' +
                        '<td>' + item[2] + '</td>' +
                        '<td>' + item[3] + '</td>' +
                        '<td>' + item[4] + '</td>' +
                        '<td>' + item[5] + '</td>' +
                        '<td>' + item[6] + '</td>' +
                      '</tr>';
                  });                
              } else {
                  trHTML += '<tr>' +
                    '<td colspan="6" align="center">No hay archivos...</td>' +
                  '</tr>';
              }
              $('.filas').append(trHTML);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });
  }

  function submitFile(){
      var formData = new FormData($('.myForm')[0]);
      var id = $('#client').val();
      $.ajax({
          url: "<?php echo site_url('clientes/cargar_archivo')?>",
          type: 'POST',
          data: formData,
          mimeType: "multipart/form-data",
          contentType: false,
          cache: false,
          processData: false,
          dataType: "JSON",
          success: function(data, textSatus, jqXHR){
              if (data.status) {
                  view_files(id);
              } else if(data.permission) {
                  $('.permission-error-archivo').show();
                  $('.permission-error-archivo').html(data.permission);
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              alert("error");
          }
      });
  }

  function filtrar_archivos(cliente_id){
      var id = $('#filtrar_tipos_id').val();
      $.ajax({
          url : "<?php echo site_url('clientes/filtrar_files/')?>/" + id,
          type: "POST",
          data: {"cliente_id":cliente_id},
          dataType: "JSON",
          success: function(data)
          {
              $('.filas').empty();
              $('.modal-nombre-archivo').text(data.cliente);
              var trHTML = '';
              if (data.data.length > 0) {          
                  $.each(data.data, function (i, item) {
                      trHTML += '<tr>' +
                        '<td>' + item[0] + '</td>' +
                        '<td>' + item[1] + '</td>' +
                        '<td>' + item[2] + '</td>' +
                        '<td>' + item[3] + '</td>' +
                        '<td>' + item[4] + '</td>' +
                        '<td>' + item[5] + '</td>' +
                        '<td>' + item[6] + '</td>' +
                      '</tr>';
                  });                
              } else {
                  trHTML += '<tr>' +
                    '<td colspan="6" align="center">No hay archivos...</td>' +
                  '</tr>';
              }
              $('.filas').append(trHTML);
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });
  }

  $(function() {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
    });
    
  });

/*  $(document).ready(function() {
    $('#inputSuccess').daterangepicker({
      singleDatePicker: true,
      calender_style: "picker_4",
      format: "DD/MM/YYYY"
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
      fecha_porcentaje();
    });
  });*/

  function rechazar() {

      $("#myModal3").on("show", function() {    // wire up the OK button to dismiss the modal when shown
          $("#myModal3 a.btn").on("click", function(e) {
              console.log("button pressed");   // just as an example...
              $("#myModal3").modal('hide');     // dismiss the dialog
          });
      });

      $("#myModal3").on("hide", function() {    // remove the event listeners when the dialog is dismissed
          $("#myModal3 a.btn").off("click");
      });
      
      $("#myModal3").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
          $("#myModal3").remove();
      });
      
      $("#myModal3").modal({                    // wire up the actual modal functionality and show the dialog
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true                     // ensure the modal is shown immediately
      });

      $("#test").html('Esta seguro?. Pasara al estado rechazado.');

      $("#info2").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="acept_test()" data-dismiss="modal"><i class="fa fa-check"></i> Aceptar</a><a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</a>');

  }


  function acept_test(){
      var id = $('[name="solicitud_id"]').val();
      $.ajax({
          url: "<?php echo site_url('mis_solicitudes/update_test/')?>" + id,
          type: 'POST',
          dataType: "JSON",
          success: function(data, textSatus, jqXHR){
              if (data.status) {
                  window.location = "<?php site_url('mis_solicitudes/entrega') ?>";
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              alert("error");
          }
      });
  }


function disposiciones_clientes_form(){
  var id = $('[name="solicitud_id"]').val();
    $('[name="bandera"]').val(id);
    disposiciones(id);
}

function notas_clientes_form(){
    var id = $('[name="clientes_id"]').val();
    notas_clientes(id);
}

function notas_clientes(id){
  $('#modal_form_notas').modal('show');
  $('.modal-title').text('Añadir Nota');
  $('#form_notas')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="clientes_id"]').val(id);
  $('#msg-error-notas').hide();
  //$('.nombrenota').hide();
  //view_notas();
  datos_notas(id);
}


function datos_notas(id){
  $.ajax({
      url : "<?php echo site_url('solicitudes/notas_cliente/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
          $('.modal-nombre-nota').text(data.nombre_cliente);

          $('.datos_notas').empty();
          var trHTML = '';
          if (data.nota.length > 0) {
              $('.notes_datos').show();
              $.each(data.nota, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>'+item.fecha_creacion+'</td>' +
                    '<td>'+item.nota+'</td>' +
                    '<td>'+item.usuarios_id+'</td>' +
                  '</tr>'
              });
          } else {
            $('.notes_datos').hide();
          }
          $('.datos_notas').append(trHTML);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function save_notes(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_notes')?>",
      type: "POST",
      data: $('#form_notas').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            notas_clientes($('[name="clientes_id"]').val());
            $('#msg-error-notas').hide();
          } else if (data.validation) {
            $('#msg-error-notas').show();
            $('.list-errors-notas').html('<div class="animated shake">'+data.validation+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function confirm_deselect_request(id) {
    $('.permission-error-archivo').hide();    
    $("#myModal4").modal("show");
    $("#info4").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="deselect_request('+ id +')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function deselect_request(id){
    $.ajax({
        url : "<?php echo site_url('solicitudes/deselect_request/')?>" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          if (data.status) {
              reload_table();
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    $("#myModal4").modal('hide');
}

function generate_pdf(id,tipo = null){
  $("#modal_impresiones").modal('show');
  $('[name="clieid"]').val(id);
  $('.aviso').text('');
  $('.botones_pdf').empty();
  $.ajax({
    url : "<?php echo site_url('solicitudes/tipos_reportes/')?>" + tipo,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
        $('#impresion').empty();
        var trHTML = "<option disabled selected hidden>Tipos de Reportes</option>";
        $.each(data, function (i, item) {
            trHTML += '<option value="'+item.id+'">'+item.nombre+'</option>';
        });
        $('#impresion').append(trHTML);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error adding / update data');
    }
  });
}

function seleccion_reporte(id){
    $('.botones_pdf').empty();
    if (id == 1) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_hoja_verificacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_hoja_verificacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 2) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 3) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_condiciones_generales/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_condiciones_generales/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 4) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_revision/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_revision/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 5) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_mensajeria/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_mensajeria/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 6) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_mensajeria_refinianciamineto/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_mensajeria_refinianciamineto/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 7) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_aprobacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_aprobacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 8) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 9) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento_en_blanco/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento_en_blanco/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 10) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_contrato/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_contrato/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 11) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_pagare/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_pagare/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 12) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_no_desembolsado/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_no_desembolsado/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 13) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 14) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_verificar_dispo/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_verificar_dispo/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 15) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_no_desc/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_no_desc/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 16) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_contrato_definido/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_contrato_definido/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 17) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_saldo/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_saldo/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 18) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_recordatorio_pago/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_recordatorio_pago/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 19) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_recordatorio_admin/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_recordatorio_admin/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 20) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_gerentes/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_gerentes/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 21) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_cancelacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_cancelacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 22) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_recibo_pago/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_recibo_pago/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 23) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_constancia_devolucion_intereses/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_constancia_devolucion_intereses/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    }
}


function view_notas()
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_notas')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#notas_id').empty();
            var trHTML = "<option></option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            trHTML += "<option value='Otro'>Otro</option>";
            $('#notas_id').append(trHTML);

            $("#notas_id").select2({
              placeholder: "Notas",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".notas_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function notas(id){
  if (id == 'Otro') {
    $('#nota').removeAttr('readonly',false);
    $('[name="nota"]').val(null);
    $('.nombrenota').show();
  } else {
    $('.nombrenota').hide();
    $('#nota').attr('readonly','readonly');
    $.ajax({
        url : "<?php echo site_url('admin/view_nota/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nota"]').val(data.nota);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('[name="nota"]').val(null);
        }
    }); 
  }
}



function disposiciones_for_select(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/disposiciones_for_select')?>",
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        if (data) {
          var trHTML = '<option value="0">Todos<option>';
          $.each(data, function(i,item){
            trHTML += '<option value='+item.id+'>'+item.nombre+'<option>';
          });
          $("#filtrar_disposiciones").html(trHTML);
          $("#filtrar_disposiciones").select2({
            placeholder: "Filtrar por Disposición",
            allowClear: false,
            language: "es",
            theme: "classic",
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function disposiciones(id){
  $('#modal_form_disposiciones').modal('show');
  $('.modal-title').text('Añadir Disposición');
  $('#form_disposiciones')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="solicitud_id"]').val(id);
  $('#msg-error-disposiciones').hide();
  view_disposiciones(id);
}


function view_disposiciones(id)
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_disposiciones/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('.modal-nombre-disposicion').text(data.nombre_cliente);
            
            $('#disposiciones_id').empty();
            var trHTML = "<option></option>";
            if (data.data.length > 0) {
                $.each(data.data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#disposiciones_id').append(trHTML);

            $("#disposiciones_id").select2({
              placeholder: "Disposiciones",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".disposiciones_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save_disposicion(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_disposicion')?>",
      type: "POST",
      data: $('#form_disposiciones').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            $('#modal_form_disposiciones').modal('hide');
            if ($('[name="bandera"]').val() > 0) {
              window.location = "<?php site_url('mis_solicitudes/entrega') ?>";
            } else {
              reload_table();
            }
          } else if (data.validation) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.validation+'</div>');
          } else if (data.permission) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.permission+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function filtrar_disposiciones(disposicion){
  table.ajax.url("<?php echo site_url('mis_solicitudes/list_aprobados/Por Entregar/')?>" + disposicion).load();
}


</script>
