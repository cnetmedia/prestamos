<style type="text/css">
  .checkboxStyle{
    background-color: #26B99A;
  }
</style>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Gestionar Solicitudes <small>Listado de Solicitudes</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="row col-lg-7 col-sm-7 col-12">

                      <?php if ($insertar == true) { ?>
                        <button class="btn btn-success btn-xs" onclick="add_request()"><i class="glyphicon glyphicon-plus"></i> Crear Solicitud</button>
                      <?php } ?>

                      <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>

                      <!-- <a class="btn btn-primary btn-xs" href="javascript:void(0)" onclick="confirm_ventas()"><i class="fa fa-envelope-o"></i> Sin Respuesta </a> -->

                    </div>

                    <div class="row col-lg-3 pull-right">
                      <select class='form-control' name="filtrar_disposiciones" id="filtrar_disposiciones" onchange="filtrar_disposiciones(this.value)">
                      </select>
                    </div>

                    <div class="col-lg-2 pull-right">
                      <select class='form-control' name="filtrar_estado" id="filtrar_estado" onchange="estados(this.value)">
                      </select>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>
                    <span class="msg-correo"></span>

                    <div class="clearfix"></div>

                    <br>

                    <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Origen</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Solicitado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Elegido por</th>
<!--                           <?php if ($roluser == 1 || $roluser == 2) { ?>
                            <th>Elegido por</th>
                          <?php } ?> -->
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Fecha</th>
                          <th>Origen</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Solicitado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Elegido por</th>
<!--                           <?php if ($roluser == 1 || $roluser == 2) { ?>
                            <th>Elegido por</th>
                          <?php } ?> -->
                          <th>Acciones</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- /page content -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Solicitud</h3>
            </div>
            <div class="modal-body form">

              <form id="form" class="form-horizontal form-label-left input_mask">
              <input type="hidden" value="" name="id" id="idc" />
              <input type="hidden" value="" name="solicitud_id" id="ids" />

                <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors animated shake"></div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Nombre</label>
                  <input type="text" name="nombre" required class="form-control has-feedback-left" id="inputSuccess" placeholder="Nombre" disabled>
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Apellido</label>
                  <input type="text" name="apellido" required class="form-control" id="inputSuccess1" placeholder="Apellido" disabled>
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <label>Correo Electrónico</label>
                  <input type="email" name="correo" required class="form-control has-feedback-left" id="inputSuccess2" placeholder="Correo Electrónico" disabled>
                  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Teléfono</label>
                  <input type="text" name="telefono" required class="form-control has-feedback-left" id="inputSuccess3" placeholder="Teléfono" onkeypress="return justNumbers(event);" disabled>
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Cédula</label>
                  <input type="text" name="cedula" required class="form-control" id="inputSuccess4" placeholder="Cédula"  onkeyup="validarCedula(this.value)" maxlength="13" data-toggle="tooltip" data-placement="top" title="Formato: XX-XXXX-XXXXX">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Salario</label>
                  <input type="text" name="salario" required class="form-control has-feedback-left" id="inputSuccess5" placeholder="Salario" disabled>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Inicio de Labores</label>
                  <input type="text" name="inicio_labores" required class="form-control has-feedback-left date-picker" id="inputSuccess281" placeholder="Inicio de Labores" maxlength="10" disabled>
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Tiempo Laborando</label>
                  <input type="text" name="tiempo_laborando" required class="form-control" id="inputSuccess6" placeholder="Tiempo Laborando" readonly="">
                  <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Tiene Descuento</label>
                  <select required class="form-control has-feedback-left" name="descuento" id="inputSuccess7" placeholder="Tiene Descuento" onchange="select_descuento(this.value)" disabled>
                    <option disabled selected hidden>Tiene Descuento</option>
                    <option>Si</option>
                    <option>No</option>
                  </select>
                  <span class="fa fa-minus form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Cantidad de Descuento</label>
                  <input type="text" name="cantidad_descuento" required class="form-control" id="oinputSuccess" placeholder="Cantidad de Descuento" readonly="">
                  <span class="fa fa-usd form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <label>Cantidad Solicitada</label>
                  <input type="text" name="cantidad_solicitada" required class="form-control has-feedback-left" id="inputSuccess8" placeholder="Cantidad Solicitada" disabled>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

              </form>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea eliminar el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información de la Solicitud</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="modal-nombre text-info"><!-- Nombre --></h2>
                    </div>

                    <div class="clearfix"></div>

                    <div class="tabla_view"></div>

                    <div class="clearfix"></div>

                    <div class="table-responsive">
                      <table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th>COMENTARIO CSS</th>
                            <th>ARCHIVO APC</th>
                            <th>COMENTARIO APC</th>
                            <th>COMENTARIO COMPAÑIA</th>
                            <th>ARCHIVO RP</th>
                            <th>COMENTARIO RP</th>
                          </tr>
                        </thead>
                        <tbody class="view_request_table">
                        </tbody>
                      </table>
                    </div>

                </div> 
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_cotizar" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Información del Cobro</h3>
                </div>
                <div class="modal-body">

                <input type="hidden" name="cliente_id" id="cliente_id">
                  <input type="hidden" name="aprobacion_id" id="aprobacion_id">
                  <input type="hidden" name="numero_cobro" id="numero_cobro">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- info --></h2></u></strong>
                    </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-suma_prestamo">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-productos_id">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-plazo">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-fecha_pago">
                        <!-- info -->
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback modal-validacion-fecha_pago">
                        <!-- info -->
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback modal-nota">
                        <!-- info -->
                      </div>
                  </div>

                  <table class="table table-striped table-bordered dt-responsive nowrap" id="datatable-cotizacion" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>Cobro</th>
                        <th>Fecha de Pagos</th>
                        <th>Principal</th>
                        <th>Interés</th>
                        <th>Tasa de Interés</th>
                        <th>Cargo Administrativo</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody class="filas">
                    </tbody>
                  </table>

                </div>
                <div class="modal-footer for-info">
                  <button type="button" class="btn btn-success btn-xs" onclick="confirmar_cobro()">
                    <i class="fa fa-check"></i> Confirmar
                  </button>
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_history" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Historial</h3>
                </div>
                <div class="modal-body">

                    <table id="datatable-responsive-bitacora" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Usuario</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Fecha</th>
                          <th>Usuario</th>
                          <th>Acción</th>
                      </tr>
                      </tfoot>
                    </table>



                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->



    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form_pagos" role="dialog">
        <!-- <div class="modal-dialog modal-sm"> -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Pagos</h3>
                </div>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- info --></h2></u></strong>
                    </div>
                  </div>

                  <div class="table-responsive company_user">
                    <table class="table table-striped table-bordered jambo_table bulk_action">
                      <thead>
                        <tr>
                          <th style="text-align: center"><input type="checkbox" id="totalpagos" onclick="totalpagos()"></th>
                          <th>Fecha correspondiente</th>
                          <th>Fecha de pago</th>
                          <th>Forma de pago</th>
                          <th>Pago</th>
                          <th>Nota</th>
                          <th>Registrado por</th>
                        </tr>
                      </thead>
                      <tbody class="filaspagos">
                      </tbody>
                    </table>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary btn-xs" onclick="confirm_remove()" id="btnProcesarCobro">
                    <i class="fa fa-trash-o"></i> Remover
                  </button>
                  <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                  </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_impresiones" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Impresiones</h3>
                </div>
                <div class="modal-body">

                  <input type="hidden" name="clieid">
                  <span class="label label-danger aviso"></span>
                  <select class="form-control" name="impresion" id="impresion" onchange="seleccion_reporte(this.value)">
                    
                  </select>
                  <div style="margin-top:" class="fade" id="fecha_descuento">
                    <label>Fecha Primer descuento</label>
                    <input type="text" name="fecha_descuento" class="date-picker" 
                      id="inputSuccess88" placeholder="Seleccione fecha" maxlength="10">
                  </div>
                </div>
                <div class="modal-footer botones_pdf">
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->

        <div class="modal fade" id="modal_impresiones_paquetes" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Impresiones</h3>
                </div>
                <div class="modal-body">

                  <input type="hidden" name="clieid">
                  <span class="label label-danger aviso"></span>
                  <select class="form-control" name="impresion_paq" id="impresion_paq"  onchange="seleccion_paquete(this.value)" >
                    
                  </select>

                </div>
                <div class="modal-footer botones_pdf">
                </div>
            </div>
        </div>
    </div>


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_disposiciones" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Disposición</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-disposicion text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_disposiciones" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="solicitud_id"/>

                <div id="msg-error-disposiciones" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-disposiciones animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback disposiciones_id">
                  <select class="form-control" name="disposiciones_id" id="disposiciones_id" style="width: 100%"></select>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_disposicion()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Asignar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-nota text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="clientes_id"/>

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

              <div class="table-responsive notes_datos">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Fecha</th>
                      <th class="column-title">Nota</th>
                      <th class="column-title">Registrado por</th>
                    </tr>
                  </thead>

                  <tbody class="datos_notas">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->


<!-- Modal para deselegir -->
<div id="myModal4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea deseleccionar la solicitud?
      </div>
      <div id="info4" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal deselegir -->

<!-- Modal para deselegir -->
<div id="myModal5" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea cambiar el estado de la solicitud?
      </div>
      <div id="info5" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal deselegir -->


<!-- Modal para eliminar -->
<div id="modalCorreo" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea enviar los correos?
      </div>
      <div id="infocorreo" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>



<script type="text/javascript">

var save_method; //for save method string
var table;
var tablehistory;

$(document).ready(function() {
    $('#inputSuccess88').datetimepicker({
        format: "DD-MM-YYYY",
    });

    $('#inputSuccess88').on("dp.change",function (e) {
        var fecha = $('#inputSuccess88').val();
        console.log('fecha',fecha);
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '?fecha='+ fecha +'" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '?fecha='+ fecha +'"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    });

    table = $('#datatable-responsive-solicitudes').DataTable( {
      "order": [],
      "bRetrieve": true,
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Lo siento, no hay registros",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No hay registros disponibles",
          "infoFiltered": "(filtro de _MAX_ registros en total)",
          "sSearch": "Buscar",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "processing": "Procesando..."
      },
      "processing": true,
      "serverSide": true,
      "ajax": {
          "url": "<?php echo site_url('solicitudes/ajax_list')?>",
          "type": "POST"
      },
      "columnDefs": [{ 
          "targets": [ -1 ],
          "orderable": false,
      },],
    } );

    tablehistory = $('#datatable-responsive-bitacora').DataTable( {
        "order": [[ 0, "desc" ]],
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true,
    } );

    $("#inputSuccess4").inputmask();

    $("#filtrar_estado").select2({
      placeholder: "Filtrar por Estado",
      allowClear: false,
      language: "es",
      theme: "classic",
    });

    $("#filtrar_disposiciones").select2({
      placeholder: "Filtrar por Disposición",
      allowClear: false,
      language: "es",
      theme: "classic",
    });


    $('#inputSuccess281').datetimepicker({
        format: "DD/MM/YYYY",
        maxDate: new Date()
    });
    $("#inputSuccess281").on("dp.change",function (e) {
       calcular_tiempo_laborando();
    });


    estados_for_select();
    disposiciones_for_select();

});


function estados_for_select(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/estados_for_select')?>",
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        if (data) {
          var trHTML = '<option value="0">Todos<option>';
          $.each(data, function(i,item){
            trHTML += '<option value='+item.id+'>'+item.estado+'<option>';
          });
          $("#filtrar_estado").html(trHTML);
          $("#filtrar_estado").select2({
            placeholder: "Filtrar por Estado",
            allowClear: false,
            language: "es",
            theme: "classic",
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function disposiciones_for_select(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/disposiciones_for_select')?>",
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        if (data) {
          var trHTML = '<option value="0">Todos<option>';
          $.each(data, function(i,item){
            trHTML += '<option value='+item.id+'>'+item.nombre+'<option>';
          });
          $("#filtrar_disposiciones").html(trHTML);
          $("#filtrar_disposiciones").select2({
            placeholder: "Filtrar por Disposición",
            allowClear: false,
            language: "es",
            theme: "classic",
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}


function calcular_tiempo_laborando(){
  $('#msg-error').hide();
  $.ajax({
      url : "<?php echo site_url('clientes/calcular_tiempo_laborando')?>",
      type: "POST",
      data: {"inicio_labores": $('[name="inicio_labores"]').val()},
      dataType: "JSON",
      success: function(data)
      {
          if (data.datos) {
            $('#inputSuccess6').val(data.datos);
          } else {
            $('#msg-error').show();
            $('.list-errors').html(data.error);
            $('#inputSuccess6').val(null);
          }
          if (data.validation) {
            $('#msg-error').show();
            $('.list-errors').html(data.validation);
            $('#inputSuccess28').val(null);                
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function validarCedula(val)
{
    var pvalor = null;
    if (val.charAt(0).toUpperCase() == "P" || val.charAt(0).toUpperCase() == "E" || val.charAt(0).toUpperCase() == "N") {
      pvalor = val.charAt(0).toUpperCase() + val.slice(1);
      $("#inputSuccess4").val(pvalor);
    }
    if (val.charAt(1).toUpperCase() == "P" || val.charAt(1).toUpperCase() == "E" || val.charAt(1).toUpperCase() == "N") {
      //pvalor = val.charAt(0).toUpperCase() + val.charAt(1).toUpperCase() + "-" + val.slice(1);
      pvalor = val.charAt(0).toUpperCase() + val.charAt(1).toUpperCase() + val.slice(1);
      $("#inputSuccess4").val(pvalor);
    }
    if (val.charAt(0).toUpperCase() == "-") {
      $("#inputSuccess4").val(null);
    }
    //$("#inputSuccess4").inputmask("Regex", { regex: "[pPeEnN]{0,2}[0-9]{0,1}-[0-9]*" });
    $("#inputSuccess4").inputmask("Regex", { regex: "[pPeEnN0-9]{2}-[0-9]{4}-[0-9]*" });
    fcedula($("#inputSuccess4").val());
}


function fcedula(val){
    //var patron = /^(\d){2}\-\d{4}\-\d{4}$/;
    //var patron = /^[PEN]{0,2}\d{0,1}\-\d{8,10}$/;
    var patron = /^[pPeEnN]{0,2}\d{0,2}\-\d{4}\-\d{4,5}$/;
    var validar = patron.test(val);
    if (validar) {
	    $.ajax({
	        url : "<?php echo site_url('solicitudes/client_by_cedula/')?>" + val,
	        type: "POST",
	        dataType: "JSON",
	        success: function(data)
	        {
            $('#inputSuccess').removeAttr("disabled");
            $('#inputSuccess1').removeAttr("disabled");
            $('#inputSuccess2').removeAttr("disabled");
            $('#inputSuccess3').removeAttr("disabled");
            $('#inputSuccess5').removeAttr("disabled");
            $('#inputSuccess7').removeAttr("disabled");
            $('#inputSuccess8').removeAttr("disabled");
            $('#inputSuccess281').removeAttr("disabled");
	        	if (data != null) {
	        		  $('[name="solicitud_id"]').val(null);
		            $('[name="id"]').val(data.id);
		            $('[name="nombre"]').val(data.nombre);
		            $('[name="apellido"]').val(data.apellido);
		            $('[name="correo"]').val(data.correo);
		            $('[name="telefono"]').val(data.telefono);
		            if (data.salario > 0) { $('[name="salario"]').val(data.salario) }
                $('[name="inicio_labores"]').val(data.inicio_labores);
		            $('[name="tiempo_laborando"]').val(data.tiempo_laborando);
		            $('[name="descuento"]').val(data.descuento);
                select_descuento(data.descuento);
                $('[name="cantidad_descuento"]').val(data.cantidad_descuento);
                if (data.cantidad_solicitada > 0) { $('[name="cantidad_solicitada"]').val(data.cantidad_solicitada) }
		            save_method = 'update';
	        	} else {
	        		  $('[name="solicitud_id"]').val(data);
		            $('[name="id"]').val(data);
		            $('[name="nombre"]').val(data);
		            $('[name="apellido"]').val(data);
		            $('[name="correo"]').val(data);
		            $('[name="telefono"]').val(data);
		            $('[name="salario"]').val(data);
                $('[name="inicio_labores"]').val(data);
		            $('[name="tiempo_laborando"]').val(data);
		            $('[name="descuento"]').val(data);
                $('#inputSuccess7').append('<option disabled selected hidden>Tiene Descuento</option>');
                select_descuento(data);
                $('[name="cantidad_descuento"]').val(data);
		            $('[name="cantidad_solicitada"]').val(data);
		            save_method = 'add';
	        	}
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error get data from ajax');
	        }
	    });
    } else {
    	$('#idc').val(null);
    	$('#ids').val(null);
	    $('#inputSuccess').val(null);$('#inputSuccess').attr("disabled","disabled");
	    $('#inputSuccess1').val(null);$('#inputSuccess1').attr("disabled","disabled");
	    $('#inputSuccess2').val(null);$('#inputSuccess2').attr("disabled","disabled");
	    $('#inputSuccess3').val(null);$('#inputSuccess3').attr("disabled","disabled");
	    $('#inputSuccess5').val(null);$('#inputSuccess5').attr("disabled","disabled");
      $('#inputSuccess281').val(null);$('#inputSuccess281').attr("disabled","disabled");
	    $('#inputSuccess6').val(null);
	    $('#inputSuccess7').val(null);$('#inputSuccess7').attr("disabled","disabled");
	    $('#inputSuccess8').val(null);$('#inputSuccess8').attr("disabled","disabled");
      $('#inputSuccess7').append('<option disabled selected hidden>Tiene Descuento</option>');
      select_descuento(null);
    }
    $('#msg-error').hide();
}


function justNumbers(e)
{
   var keynum = window.event ? window.event.keyCode : e.which;
   if ((keynum == 8) || (keynum == 46))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function select_descuento(val){
    if (val == "Si") {
        $('#oinputSuccess').removeAttr("readonly");
    } else {
        $('#oinputSuccess').val(null);
        $('#oinputSuccess').attr("readonly","readonly");
    }
}

function add_request()
{
    $('#inputSuccess').attr("disabled","disabled");
    $('#inputSuccess1').attr("disabled","disabled");
    $('#inputSuccess2').attr("disabled","disabled");
    $('#inputSuccess3').attr("disabled","disabled");
    $('#inputSuccess4').removeAttr("disabled");
    $('#inputSuccess5').attr("disabled","disabled");
    $('#inputSuccess7').attr("disabled","disabled");
    $('#inputSuccess8').attr("disabled","disabled");
    $('#inputSuccess281').attr("disabled","disabled");
    $('.permission-error').hide();
    $('#msg-error').hide();    
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('#modal_form').modal('show');
    $('.modal-title').text('Añadir Solicitud');
}

function assign_request(id)
{
    $('.permission-error').hide();
    $.ajax({
        url : "<?php echo site_url('solicitudes/assign_request/')?>" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          if (data.status) {
              reload_table();
          } else if (data.permission) {
              $('.permission-error').show();
              $('.permission-error').html(data.permission);
              window.scrollTo(0,0);
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function view_request(id)
{

    $('.permission-error').hide();
    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_view/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            //console.log(data);            
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información de la Solicitud');
            $('.modal-nombre').text(data.nombre);

            $('.tabla_view').html('<div class="table-responsive">'+
              '<table class="table table-striped">'+
                '<tbody>'+
                  '<tr>'+
                    '<td>Fecha de Creación: '+data.fecha_creacion+'</td>'+
                    '<td>Teléfono: '+data.telefono+'</td>'+
                    '<td>Salario: $'+data.salario+'</td>'+
                    '<td>Cantidad Solicitada: $'+data.cantidad_solicitada+'</td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>');

            $('.view_request_table').empty();
            var trHTML = '';
            if (data.comentario_css == null && data.archivo_apc == null && data.comentario_apc == null && data.comentario_compania == null && data.archivo_rp == null && data.comentario_rp == null) {
              trHTML += '<tr>' +
                    '<td colspan="6" align="center">No hay registros...</td>'
                  '</tr>';
            } else {
              if (data.comentario_css == null) { data.comentario_css = "" }
              if (data.archivo_apc == null) { data.archivo_apc = "" }
              if (data.comentario_apc == null) { data.comentario_apc = "" }
              if (data.comentario_compania == null) { data.comentario_compania = "" }
              if (data.archivo_rp == null) { data.archivo_rp = "" }
              if (data.comentario_rp == null) { data.comentario_rp = "" }
              trHTML += '<tr>' +
                    '<td>' + data.comentario_css + '</td>' +
                    '<td><a href="<?=base_url()?>files/apc/' + data.archivo_apc + '" target="_blank">' + data.archivo_apc + '</a></td>' +
                    '<td>' + data.comentario_apc + '</td>' +
                    '<td>' + data.comentario_compania + '</td>' +
                    '<td><a href="<?=base_url()?>files/rp/' + data.archivo_rp + '" target="_blank">' + data.archivo_rp + '</a></td>' +
                    '<td>' + data.comentario_rp + '</td>' +
                  '</tr>';
            }
            $('.view_request_table').append(trHTML);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });

}

function edit_request(id)
{
    $('#inputSuccess4').attr("disabled","disabled");
    $('.permission-error').hide();
    $('#msg-error').hide();
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');

    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_edit/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

        	  $('[name="solicitud_id"]').val(data.solicitud_id);
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="apellido"]').val(data.apellido);
            $('[name="correo"]').val(data.correo);
            $('[name="telefono"]').val(data.telefono);
            $('[name="cedula"]').val(data.cedula);
            $('[name="salario"]').val(data.salario);
            $('[name="inicio_labores"]').val(data.inicio_labores);
            $('[name="tiempo_laborando"]').val(data.tiempo_laborando);
            $('[name="descuento"]').val(data.descuento);
            $('[name="cantidad_descuento"]').val(data.cantidad_descuento);
            $('[name="cantidad_solicitada"]').val(data.cantidad_solicitada);

    		    $('#inputSuccess').removeAttr("disabled");
    		    $('#inputSuccess1').removeAttr("disabled");
    		    $('#inputSuccess2').removeAttr("disabled");
    		    $('#inputSuccess3').removeAttr("disabled");
    		    $('#inputSuccess5').removeAttr("disabled");
            $('#inputSuccess281').removeAttr("disabled");
    		    $('#inputSuccess7').removeAttr("disabled");
            //$('#inputSuccess7').append('<option disabled selected hidden>Tiene Descuento</option>');			    
    		    $('#inputSuccess8').removeAttr("disabled");

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Solicitud'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax
}


function save()
{
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
        url = "<?php echo site_url('solicitudes/ajax_add')?>";
    } else {
        url = "<?php echo site_url('solicitudes/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('#msg-error').hide();
            }
            else if (data.validation)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            }
            else if (data.rol)
            {
                if (data.botonsi) {
                  $('#msg-error').show();
                  $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span> '+data.botonsi+'</div>');
                } else {
                  $('#msg-error').show();
                  $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
                }
            }
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').attr('disabled',false);

        }
    });
}

function delete_request(id)
{
    if(id) {
        $.ajax({
            url : "<?php echo site_url('solicitudes/ajax_delete/')?>"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $('#modal_form').modal('hide');
                reload_table();
                if(data.permission)
                {
                    $('.permission-error').show();
                    $('.permission-error').html(data.permission);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
        $("#myModal").modal('hide');
    }
}

function confirm_delete(id) {

    $('.permission-error').hide();

    $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal").modal('hide');     // dismiss the dialog
        });
    });

    $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });
    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_request('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}

/*function check_collection(id){
  $.ajax({
      url : "<?php echo site_url('cobros/payment_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('#modal_form_cotizar').modal('show');
        $('.for-info').show();
        $('.modal-nombre').text(data.nombre_cliente);
        $('.modal-suma_prestamo').html('<strong><i class="fa fa-usd"></i> Aprobado: </strong>' + data.cantidad_aprobada);
        $('.modal-productos_id').html('<strong><i class="fa fa-product-hunt"></i> Producto: </strong>' + data.nombre_producto);
        $('.modal-plazo').html('<strong><i class="fa fa-sun-o"></i> Plazo: </strong>' + data.cantidad_meses + " Meses a " + data.porcentaje + "%" );

        $('.modal-fecha_pago').html('<label class="label label-success">Ultimos ' + data.total_cobros + ' Pagos adelantados</label>');

        cotiza(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}*/


function cotiza(datos){
    $('.modal-nota').empty();
    $('.modal-validacion-fecha_pago').empty();
    $.ajax({
        url : "<?php echo site_url('cobros/cobro')?>/" + datos.id,
        type: "POST",
        //data: datos,
        dataType: "JSON",
        success: function(data)
        {
            $('.filas').empty();
            var totalm = null;
            var trHTML = '';
            $.each(data.data, function (i, item) {
                trHTML += '<tr>' +
                  '<td align="center"><input type="checkbox" value="' + item.num + '" id="numcobro' + item.num + '" onclick="numcobro(' + item.num + ')" disabled></td>' +
                  '<td>' + item.fecha + '</td>' +
                  '<td>$' + item.principal.toFixed(2) + '</td>' +
                  '<td>$' + item.interes.toFixed(2) + '</td>' +
                  '<td align="center">%' + item.tasa.toFixed(2) + '</td>' +
                  '<td align="center">$' + item.cargo.toFixed(2) + '</td>' +
                  '<td>$' + item.total.toFixed(2) + '</td>' +
                '</tr>';
                totalm = item.total;
                $('#numero_cobro').val(i);
            });            
            trHTML += '<tr><td></td><td></td><td><span class="label label-warning">$'+ data.totalprincipal.toFixed(2) +'</span></td><td><span class="label label-info">$'+ data.totalintereses.toFixed(2) +'</span></td><td></td><td align="center"><span class="label label-primary">$'+ data.totalcargo.toFixed(2) +'</span></td><td><span class="label label-success">$'+ data.totalcobrar.toFixed(2) +'</span></td></tr>';
            $('.filas').append(trHTML);

            $('[name="cliente_id"]').val(datos.clientes_id);
            $('[name="aprobacion_id"]').val(datos.id);

            var num = data.cobros + 1;
            $('#numcobro'+num).removeAttr('disabled');

            if (data.cobros > 0) {
              for (var i = 1; i < num; i++) {
                $("#numcobro"+i).attr("checked", true);
              }
            }

            if (datos.total_cobros > 1) {
              var ini = num - datos.total_cobros;
              for (var i = ini; i < num; i++) {
                $("#numcobro"+i).parent().addClass("checkboxStyle");
              }
            } else {
              $('.modal-fecha_pago').empty();
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
}

function confirmar_cobro(){
  var id = $('[name="cliente_id"]').val();
  $.ajax({
    url : "<?php echo site_url('solicitudes/completar_cobro/')?>" + id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
      if (data.status) {
        $('#modal_form_cotizar').modal('hide');
        reload_table();
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
    }
  });
}


function info_collection(id){
  $.ajax({
      url : "<?php echo site_url('cobros/payment_view/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('#modal_form_cotizar').modal('show');
        $('.for-info').hide();
        $('.modal-nombre').text(data.nombre_cliente);
        $('.modal-suma_prestamo').html('<strong><i class="fa fa-usd"></i> Aprobado: </strong>' + data.cantidad_aprobada);
        $('.modal-productos_id').html('<strong><i class="fa fa-product-hunt"></i> Producto: </strong>' + data.nombre_producto);
        $('.modal-plazo').html('<strong><i class="fa fa-sun-o"></i> Plazo: </strong>' + data.cantidad_meses + " Meses a " + data.porcentaje + "%" );


        $('.modal-fecha_pago').html('<label class="label label-success">Ultimos ' + data.total_cobros + ' Pagos adelantados</label>');

        cotiza(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function estados(estado){
  var disposicion = $('#filtrar_disposiciones').val();
  table.ajax.url("<?php echo site_url('solicitudes/ajax_list/')?>" + estado + '/' + disposicion).load();
}

function filtrar_disposiciones(disposicion){
  var estado = $('#filtrar_estado').val();
  table.ajax.url("<?php echo site_url('solicitudes/ajax_list/')?>" + estado + '/' + disposicion).load();
}

function history(id){
  tablehistory.ajax.url("<?php echo site_url('bitacora/ajax_list_by_cliente/')?>" + id).load();
  $('#modal_history').modal('show');
  $('.modal-title').text('Historial');
  //tablehistory.ajax.reload(null,false);
}

/*function remove_payment(id){
  $('#modal_form_pagos').modal('show');
  table_payment(id);
}

var numpagos;
var aprobaId;
var cobro_adelantado;
function table_payment(id){
  aprobaId = id;
  $.ajax({
      url : "<?php echo site_url('cobros/remove_payment/')?>" + aprobaId,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        
        $('.modal-nombre').text(data.nombre_cliente);
        cobro_adelantado = data.cobros_adelantados;

        if (data.cobros) {
          $('.filaspagos').empty();
          var trHTML = '';
          $.each(data.cobros, function (i, item) {
              trHTML += '<tr>' +
                  '<td style="text-align: center"><input type="checkbox" id="pago'+i+'" value="'+item.id+'" onclick="pago_adelantado()" disabled></td>' +
                  '<td>'+item.fecha_correspondiente+'</td>' +
                  '<td>'+item.fecha_pago+'</td>' +
                  '<td>'+item.forma_pago+'</td>' +
                  '<td>'+item.monto+'</td>' +
                  '<td>'+item.nota+'</td>' +
                  '<td>'+item.usuarios_id+'</td>' +
                '</tr>'
          });
          $('.filaspagos').append(trHTML);
          numpagos = data.cobros.length;

          var num = numpagos - 1;
          $('#pago'+num).removeAttr('disabled');

        } else {
          $('.filaspagos').empty();
          var trHTML = '<tr><td colspan="7" style="text-align: center">Lo siento, no hay registros</td></tr>';
          $('.filaspagos').append(trHTML);
        }

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
      }
  });
}*/

function totalpagos(){
  if ($('#totalpagos').prop('checked') == true) {
    for (var i = 0; i < numpagos; i++) {
      $('#pago'+i).prop('checked',true);
    }
  } else {
    for (var i = 0; i < numpagos; i++) {
      $('#pago'+i).prop('checked',false);
    }
  }
}

function pago_adelantado(){
  var num = numpagos - 1;
  if ($('#pago'+num).prop('checked') == true) {
    if (cobro_adelantado > 1) {
      var ini = numpagos - cobro_adelantado;
      for (var i = ini; i < numpagos; i++) {
        $('#pago'+i).prop('checked',true);
      }
    }
  } else {
    if (cobro_adelantado > 1) {
      var ini = numpagos - cobro_adelantado;
      for (var i = ini; i < numpagos; i++) {
        $('#pago'+i).prop('checked',false);
      }
    }
  }
}

function confirm_remove() {

   var flag = false;
   for (var i = 0; i < numpagos; i++) {
      if ($('#pago'+i).prop('checked')) {
        flag = true;
      }
    }

    if (flag == true) {
      $('.permission-error').hide();

      $("#myModal").on("show", function() {
          $("#myModal a.btn").on("click", function(e) {
              console.log("button pressed");
              $("#myModal").modal('hide');
          });
      });

      $("#myModal").on("hide", function() {
          $("#myModal a.btn").off("click");
      });
      
      $("#myModal").on("hidden", function() {
          $("#myModal").remove();
      });
      
      $("#myModal").modal({
        "backdrop"  : "static",
        "keyboard"  : true,
        "show"      : true 
      });

      $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="removerpagos()"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
    }

}

function removerpagos(){
  for (var i = 0; i < numpagos; i++) {
    if ($('#pago'+i).prop('checked')) {
      //alert($('#pago'+i).val());

      $.ajax({
          url : "<?php echo site_url('cobros/delete_pago/')?>" + $('#pago'+i).val(),
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
              if (data.status)
              {
                  table_payment(aprobaId);
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
          }
      });

    }
  }  
  return_estado(aprobaId);
}


function return_estado(id){
  $.ajax({
    url : "<?php echo site_url('cobros/return_estado/')?>" + id,
    type: "POST",
    dataType: "JSON",
    success: function(data)
    {
      if (data.status) {
        reload_table();
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error adding / update data');
    }
  });
  $("#myModal").modal('hide');
  $('#modal_form_pagos').modal('hide');
}




/*Modal dentro de modal*/
$(document).ready(function () {
/*    $('#openBtn').click(function () {
        $('#modal_form').modal()
    });*/

    $('.modal')
        .on({
            'show.bs.modal': function() {
                var idx = $('.modal:visible').length;
                $(this).css('z-index', 1040 + (10 * idx));
            },
            'shown.bs.modal': function() {
                var idx = ($('.modal:visible').length) - 1;
                $('.modal-backdrop').not('.stacked')
                .css('z-index', 1039 + (10 * idx))
                .addClass('stacked');
            },
            'hidden.bs.modal': function() {
                if ($('.modal:visible').length > 0) {
                    setTimeout(function() {
                        $(document.body).addClass('modal-open');
                    }, 0);
                }
            }
        });
});
/*End Modal dentro de modal*/

function generate_pdf(id,tipo = null){
  $("#modal_impresiones").modal('show');
  $('.modal-title').text('Impresiones');
  $('[name="clieid"]').val(id);  
  $('.aviso').text('');
  $('.botones_pdf').empty();
  $.ajax({
    url : "<?php echo site_url('solicitudes/tipos_reportes/')?>" + tipo,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
        $('#impresion').empty();
        var trHTML = "<option disabled selected hidden>Tipos de Reportes</option>";
        $.each(data, function (i, item) {
            trHTML += '<option value="'+item.id+'">'+item.nombre+'</option>';
        });
        $('#impresion').append(trHTML);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error adding / update data');
    }
  });
}

function seleccion_reporte(id){
    $('.botones_pdf').empty();
    if(id==8){
      $('#fecha_descuento').removeClass('fade');
    }else{
      $('#fecha_descuento').addClass('fade');     
    }
    if (id == 1) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_hoja_verificacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_hoja_verificacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 2) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 3) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_condiciones_generales/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_condiciones_generales/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 4) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_revision/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_revision/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 5) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_mensajeria/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_mensajeria/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 6) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_mensajeria_refinianciamineto/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_mensajeria_refinianciamineto/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 7) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_cover_aprobacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_cover_aprobacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 8) {
        /*$('.botones_pdf').html('<a class="btn btn-default btn-xs" id="ver_descuento_gobierno" href="<?=base_url()?>reportes/ver_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" id="des_descuento_gobierno" href="<?=base_url()?>reportes/generar_autorizacion_descuento_gobierno/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>'); */       
    } else if (id == 9) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_autorizacion_descuento_en_blanco/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_autorizacion_descuento_en_blanco/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 10) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_contrato/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_contrato/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 11) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_pagare/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_pagare/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 12) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_no_desembolsado/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_no_desembolsado/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 13) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 14) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_verificar_dispo/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_verificar_dispo/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 15) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_no_desc/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_no_desc/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 16) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_nuevo_empleador_contrato_definido/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_nuevo_empleador_contrato_definido/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 17) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_saldo/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_saldo/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 18) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_recordatorio_pago/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_recordatorio_pago/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 19) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_recordatorio_admin/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_recordatorio_admin/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 20) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_no_judicial_gerentes/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_no_judicial_gerentes/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 21) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_carta_cancelacion/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_carta_cancelacion/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 22) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_recibo_pago/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_recibo_pago/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    } else if (id == 23) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_constancia_devolucion_intereses/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_constancia_devolucion_intereses/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    }else if (id == 24) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/ver_condiciones_generales_new/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Ver PDF</a>' +
        '<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_condiciones_generales_new/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Descargar PDF</a>');
    }
}

function generate_paquete(id,tipo = null){
  $("#modal_impresiones_paquetes").modal('show');
  $('.modal-title').text('Paquetes de Impresiones');
  $('[name="clieid"]').val(id);
  $('.aviso').text('');
  $('.botones_pdf').empty();
  $.ajax({
    url : "<?php echo site_url('solicitudes/paquetes_reportes/')?>" + tipo,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
        $('#impresion_paq').empty();
        var trHTML = "<option disabled selected hidden>Paquetes de Reportes</option>";
        $.each(data, function (i, item) {
            trHTML += '<option value="'+item.id+'" data-reportes="'+item.reportes_ids+'">'+item.nombre+'</option>';
        });
        $('#impresion_paq').append(trHTML);
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error adding / update data');
    }
  });
}

function seleccion_paquete(id){
    $('.botones_pdf').empty();
    console.log(id);
    /*if (id == 1) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_paquete/' + $('[name="clieid"]').val() + '" target="_blank"><span class="fa fa-file-pdf-o"></span> Generar PDF</a>');
    } else if (id == 2) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_paquete/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Generar PDF</a>');
    } else if (id == 3) {
        $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_paquete/' + $('[name="clieid"]').val() + '"><span class="fa fa-file-pdf-o"></span> Generar PDF</a>');
    }*/ 
    $('.botones_pdf').html('<a class="btn btn-default btn-xs" href="<?=base_url()?>reportes/generar_paquete/' + $('[name="clieid"]').val() + '/?paquetes_id='+id+'" target="_blank"><span class="fa fa-file-pdf-o"></span> Generar PDF</a>');
}

function activate(id){
  $.ajax({
    url : "<?php echo site_url('solicitudes/activar/')?>" + id,
    type: "POST",
    dataType: "JSON",
    success: function(data)
    {
        if (data.status) {
            reload_table();
        }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
        alert('Error adding / update data');
    }
  });
}


function add_sol_ref(){
    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_refinanciamiento')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if (data.status)
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('#msg-error').hide();
            }
            else if (data.validation)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            }
            else if (data.rol)
            {
                if (data.botonsi) {
                  $('#msg-error').show();
                  $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span> '+data.botonsi+'</div>');
                } else {
                  $('#msg-error').show();
                  $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
                }
            }
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').attr('disabled',false);

        }
    });
}

function disposiciones(id){
  $('#modal_form_disposiciones').modal('show');
  $('.modal-title').text('Añadir Disposición');
  $('#form_disposiciones')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="solicitud_id"]').val(id);
  $('#msg-error-disposiciones').hide();
  view_disposiciones(id);
}



function view_disposiciones(id)
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_disposiciones/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('.modal-nombre-disposicion').text(data.nombre_cliente);

            $('#disposiciones_id').empty();
            var trHTML = "<option></option>";
            if (data.data.length > 0) {
                $.each(data.data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#disposiciones_id').append(trHTML);

            $("#disposiciones_id").select2({
              placeholder: "Disposiciones",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".disposiciones_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}



function notas_clientes(id){
  $('#modal_form_notas').modal('show');
  $('.modal-title').text('Añadir Nota');
  $('#form_notas')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="clientes_id"]').val(id);
  $('#msg-error-notas').hide();
  //$('.nombrenota').hide();
  //view_notas();
  datos_notas(id);
}


function datos_notas(id){
  $.ajax({
      url : "<?php echo site_url('solicitudes/notas_cliente/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

          $('.modal-nombre-nota').text(data.nombre_cliente);

          $('.datos_notas').empty();
          var trHTML = '';
          if (data.nota.length > 0) {
              $('.notes_datos').show();
              $.each(data.nota, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>'+item.fecha_creacion+'</td>' +
                    '<td>'+item.nota+'</td>' +
                    '<td>'+item.usuarios_id+'</td>' +
                  '</tr>'
              });
          } else {
            $('.notes_datos').hide();
          }
          $('.datos_notas').append(trHTML);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function save_disposicion(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_disposicion')?>",
      type: "POST",
      data: $('#form_disposiciones').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            $('#modal_form_disposiciones').modal('hide');
            reload_table();
          } else if (data.validation) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.validation+'</div>');
          } else if (data.permission) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.permission+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function save_notes(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_notes')?>",
      type: "POST",
      data: $('#form_notas').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            notas_clientes($('[name="clientes_id"]').val());
            $('#msg-error-notas').hide();
          } else if (data.validation) {
            $('#msg-error-notas').show();
            $('.list-errors-notas').html('<div class="animated shake">'+data.validation+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function admindeselegir(id){
  $("#myModal4").modal('show');
  $("#info4").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="deselect_request('+ id +')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function deselect_request(id){
  $.ajax({
      url : "<?php echo site_url('solicitudes/deselect_request/')?>" + id,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {
        if (data.status) {
            reload_table();
        }
        else if (data.permission)
        {
            $('.permission-error').show();
            $('.permission-error').html(data.permission);
        }


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
  $("#myModal4").modal('hide');
}

function view_notas()
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_notas')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#notas_id').empty();
            var trHTML = "<option></option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            trHTML += "<option value='Otro'>Otro</option>";
            $('#notas_id').append(trHTML);

            $("#notas_id").select2({
              placeholder: "Notas",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".notas_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function notasd(id){
  if (id == 'Otro') {
    $('#nota').removeAttr('readonly',false);
    $('[name="nota"]').val(null);
    $('.nombrenota').show();
  } else {
    $('.nombrenota').hide();
    $('#nota').attr('readonly','readonly');
    $.ajax({
        url : "<?php echo site_url('admin/view_nota/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nota"]').val(data.nota);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('[name="nota"]').val(null);
        }
    }); 
  }
}

function verifychangeestado(id,e){
    $("#myModal5").modal('show');
    $("#info5").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="changeestado('+id+','+e+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" href="javascript:void(0)" onclick="nochangeestado()"><i class="fa fa-close"></i> No</a>');
}

function changeestado(id,e){
  $.ajax({
      url : "<?php echo site_url('solicitudes/change_estado')?>",
      type: "POST",
      data: {
        cliente_id : id,
        estado_id: e
      },
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            reload_table();
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          $('[name="nota"]').val(null);
      }
  });  
  $("#myModal5").modal('hide');
}

function nochangeestado(){
  $("#myModal5").modal('hide');
  reload_table();
}


function confirm_ventas(id){
    $("#modalCorreo").modal("show");
    $("#infocorreo").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="sin_respuesta('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
}

function sin_respuesta(id){
  $('.msg-correo').text('');
  $.ajax({
      url : "<?php echo site_url('reportes/sin_respuesta/')?>" + id,
      type: "POST",
      dataType: "JSON",
      success: function(data)
      {

          if (data.status) {
            $('.msg-correo').removeClass('label label-danger').addClass('label label-success');
            $('.msg-correo').text('Correo enviado.');
          }
          if (data.permission) {
            $('.msg-correo').removeClass('label label-success').addClass('label label-danger');
            $('.msg-correo').text('No tienes permisos para enviar estos correos.');
          }
          $("#modalCorreo").modal("hide");
          window.scrollTo(0,0);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}


</script>