        <style>
          #div_carga{
            position:absolute;
            top:0;
            left:0;
            width:100%;
            height:100%;
            display:none;
            z-index:1;
          }

          #cargador{
            position:absolute;
            top:50%;
            left: 50%;
            margin-top: -25px;
            margin-left: -25px;
          }

          #cargador_aviso{
            position:absolute;
            top:40%;
            left: 47%;
            margin-top: -25px;
            margin-left: -25px;
          }
        </style>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row" id="list-firma">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Seguro Social <small>Información</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div id="div_carga">
                      <p id="cargador_aviso">Por favor espere...</p>
                      <img id="cargador" src="<?php echo site_url('assets/images/ajax-loader.gif') ?>"/>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>

                    <br>

                    <form id="form-access" class="form-horizontal">

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Cédula</label>
                        <input type="text" name="cedula" required class="form-control has-feedback-left" id="inputSuccess3" placeholder="Cédula" maxlength="13" data-toggle="tooltip" data-placement="top" title="Formato: XX-XXXX-XXXXX" onkeyup="validarCedula(this.value)">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      
                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Base de Datos Access</label>
                        <select class="form-control col-md-6 col-xs-12" name="dbaccess" id="dbaccess" onchange="select_access()">
                          <option disabled selected hidden>Seleccione Base de Datos Access</option>
                          <?php if ($access) {
                            foreach ($access as $row) {
                              echo '<option value="'.$row->id.'">'.$row->nombre.'</option>';
                            }
                          } ?>
                        </select>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Nombre</label>
                        <input type="text" name="nombre" required class="form-control has-feedback-left" id="inputSuccess4" placeholder="Nombre" onkeyup="validarNombre(this.value)">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 buscarnombre">
                        <label>Buscar</label>
                        <button type="button" class="btn btn-primary" onclick="buscar()"><i class="fa fa-search"></i> Buscar</button>
                      </div>

                    </form>
                    <div id="div-seleccion"></div>

                  </div>
                </div>
              </div>
            </div>

        </div>
        <!-- /page content -->


    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>


<script type="text/javascript">

$('.buscarnombre').hide();

function validarCedula(val)
{
  $('#inputSuccess4').val(null);
  $('.buscarnombre').hide();
  var pvalor = null;
  if (val.charAt(0).toUpperCase() == "P" || val.charAt(0).toUpperCase() == "E" || val.charAt(0).toUpperCase() == "N") {
    pvalor = val.charAt(0).toUpperCase() + val.slice(1);
    $("#inputSuccess3").val(pvalor);
  }
  if (val.charAt(1).toUpperCase() == "P" || val.charAt(1).toUpperCase() == "E" || val.charAt(1).toUpperCase() == "N") {
    pvalor = val.charAt(0).toUpperCase() + val.charAt(1).toUpperCase() + val.slice(1);
    $("#inputSuccess3").val(pvalor);
  }
  if (val.charAt(0).toUpperCase() == "-") {
    $("#inputSuccess3").val(null);
  }
  $("#inputSuccess3").inputmask("Regex", { regex: "[pPeEnN0-9]{2}-[0-9]{4}-[0-9]*" });
  fcedula($("#inputSuccess3").val());
}

var flag;
function fcedula(cedula){
    var patron = /^[PEN]{0,2}\d{0,2}\-\d{4}\-\d{5}$/;
    var validar = patron.test(cedula);
    if (validar) {
      flag = true;
      select_access();
    } else {
      flag = false;
    }
}

function select_access(){
  $('#inputSuccess4').val(null);
  $('.buscarnombre').hide();
  if ($('[name="dbaccess"]').val() > 0 && flag == true) {
    $('#div_carga').show();
    $.ajax({
      url : "<?php echo site_url('solicitudes/seg_soc')?>",
      type: "POST",
      data: {
        "cedula": $('[name="cedula"]').val(),
        "db_access_id": $('[name="dbaccess"]').val()
      },
      dataType: "JSON",
      success: function(data)
      {
        $('#div-seleccion').show();
        $('#div-seleccion').html(data);
        $('#div_carga').hide();
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        $('#div_carga').hide();
        alert('Error get data from ajax');
      }
    });
  }
}


function searchCedula(){
  $('.alerta').hide();
  $('.aviso').hide();
  $('#resultado').hide();
  $('#inputSuccess4').val(null);
  $('.buscarnombre').hide();
  if ($('#buscar_cedula').val().length == 8) {
    $('#div_carga').show();
    $.ajax({
        url : "<?php echo site_url('solicitudes/search_cedula_seg')?>",
        type: "POST",
        data: $('#form_search').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          $('#div_carga').hide();
          if (data.datos == "") {
            $('.alerta').show();
          } else {
            $('#resultado').html(data.datos);
            $('#resultado').show();
          }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#div_carga').hide();
            alert('Error get data from ajax');
        }
    });
  }
}


function validarNombre(){
  $('.alerta').hide();
  $('.aviso').hide();
  $('#resultado').hide();
  $('#div-seleccion').hide();
  $("#inputSuccess3").val(null);
  $('.buscarnombre').show();
}

function buscar(){
  if ($('#dbaccess').val() > 0) {
    $('#div_carga').show();
    $.ajax({
        url : "<?php echo site_url('solicitudes/search_by_name')?>",
        type: "POST",
        data: {
          "nombre": $('[name="nombre"]').val(),
          "db_access": $('[name="dbaccess"]').val()
        },
        dataType: "JSON",
        success: function(data)
        {
          $('#div-seleccion').show();
          $('#div-seleccion').html(data.datos);
          $('#div_carga').hide();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#div_carga').hide();
            alert('Error get data from ajax');
        }
    });
  }
}

</script>