
        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row" id="list-prospectos">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Solicitudes Prospectos <small>Listado de Solicitudes</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <button class="btn btn-default btn-xs" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>

                    <div class="row col-lg-3 pull-right">
                      <select class='form-control' name="filtrar_disposiciones" id="filtrar_disposiciones" onchange="filtrar_disposiciones(this.value)">
                      </select>
                    </div>
                    
                    <span class="permission-error label label-danger"></span>

                    <br><br>

                    <table id="datatable-responsive-solicitudes" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Compañia</th>
                          <th>Solicitado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      <tr>
                          <th>Fecha</th>
                          <th>Cliente</th>
                          <th>Cédula</th>
                          <th>Salario</th>
                          <th>Compañia</th>
                          <th>Solicitado</th>
                          <th>Estado</th>
                          <th>Disposición</th>
                          <th>Acciones</th>
                      </tr>
                      </tfoot>
                    </table>

                  </div>
                </div>
              </div>
          </div>



            <div class="row" id="div-prospecto">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Mis Solicitudes <small>Prospecto</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

		              <form id="form_client" class="form-horizontal form-label-left input_mask">
		                <input type="hidden" value="" name="id"/>
		                <input type="hidden" value="" name="fecha_creacion"/>
		                <input type="hidden" value="" name="solicitud_id"/>

		                <div id="msg-error-client" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
		                  <div class="list-errors-client animated shake"></div>
		                </div>

		                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
		                  <h2>Datos Personales</h2>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Nombre:</label>
		                  <input type="text" name="nombre" required class="form-control has-feedback-left" id="inputSuccess" placeholder="Nombre">
		                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Apellido:</label>
		                  <input type="text" name="apellido" required class="form-control has-feedback-left" id="inputSuccess1" placeholder="Apellido">
		                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Género:</label>
		                  <select required class="form-control has-feedback-left" name="genero" id="inputSuccess2" placeholder="Género">
		                    <option disabled selected hidden>Género</option>
		                    <option>Masculino</option>
		                    <option>Femenino</option>
		                  </select>
		                  <span class="fa fa-venus-mars form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Cédula:</label>
			                  <input type="text" name="cedula" required class="form-control has-feedback-left" id="inputSuccess3" placeholder="Cédula" maxlength="13" data-toggle="tooltip" data-placement="top" title="Formato: XX-XXXX-XXXXX" onkeyup="validarCedula(this.value)">
			                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Teléfono:</label>
		                  <input type="text" name="telefono" required class="form-control has-feedback-left" id="inputSuccess4" placeholder="Teléfono">
		                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Celular:</label>
		                  <input type="text" name="celular" required class="form-control has-feedback-left" id="inputSuccess5" placeholder="Celular">
		                  <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Estado Civil</label>
		                  <select required class="form-control has-feedback-left" name="estado_civil" id="inputSuccess6" placeholder="Estado Civil">
		                    <option disabled selected hidden>Estado Civil</option>
		                    <option>Soltero/a</option>
		                    <option>Casado/a</option>
		                    <option>Viudo/a</option>
		                    <option>Unido/a</option>
		                  </select>
		                  <span class="fa fa-spinner form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Correo Electrónico:</label>
		                  <input type="email" name="correo" required class="form-control has-feedback-left" id="inputSuccess7" placeholder="Correo Electrónico">
		                  <span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Fecha de Nacimiento:</label>
		                  <input type="text" name="fecha_nacimiento" required class="form-control has-feedback-left date-picker" id="finputSuccess8" maxlength="10" placeholder="Fecha de Nacimiento">
		                  <span class="fa fa-birthday-cake form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Numero de Dependientes:</label>
		                  <input type="text" name="numero_dependientes" required class="form-control has-feedback-left" id="inputSuccess9" placeholder="Numero de Dependientes">
		                  <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Cantidad Solicitada:</label>
		                  <input type="text" name="cantidad_solicitada" required class="form-control has-feedback-left" id="inputSuccess10" placeholder="Cantidad Solicitada">
		                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Como Escucho de Nosotros</label>
		                  <select required class="form-control has-feedback-left" name="como_escucho_nosotros" id="inputSuccess13" placeholder="Como Escucho de Nosotros" onchange="select_como_escucho(this.value)">
		                    <option disabled selected hidden>Como Escucho de Nosotros</option>
		                    <option>Instagram</option>
		                    <option>Facebook</option>
		                    <option>Google</option>
		                    <option>Referido</option>
		                    <option>Amigo</option>
		                    <option>Punto pago</option>
		                    <option>Otros</option>
		                  </select>
		                  <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback como">
		                  <label>Ingrese Como Escucho de Nosotros</label>
		                    <textarea id="1inputSuccess13" required class="form-control" name="como_escucho_nosotros_otros" placeholder="Ingrese Como Escucho de Nosotros"></textarea>
		                </div>		                

		                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
		                  <label>Fuente del Cliente:</label>
		                  <textarea id="inputSuccess12" required class="form-control" name="fuente_cliente" placeholder="Fuente del Cliente"></textarea>
		                </div>

		                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
		                  <h2>Información Adicional</h2>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Tipo de Vivienda:</label>
		                  <select required class="form-control has-feedback-left" name="tipo_vivienda" id="inputSuccess15" placeholder="Tipo de Vivienda">
		                    <option disabled selected hidden>Tipo de Vivienda</option>
		                    <option>Propia</option>
		                    <option>Rentada</option>
		                    <option>Con los Padres</option>
		                  </select>
		                  <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Tiene Hipotecas:</label>
		                  <select required class="form-control has-feedback-left" name="hipotecas" id="inputSuccess16" placeholder="Tiene Hipotecas" onchange="select_hipoteca(this.value)">
		                    <option disabled selected hidden>Tiene Hipotecas</option>
		                    <option>Si</option>
		                    <option>No</option>
		                  </select>
		                  <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Banco de la Hipoteca:</label>
		                  <input type="text" name="banco" required class="form-control has-feedback-left" id="inputSuccess17" placeholder="Banco de la Hipoteca" readonly="">
		                  <span class="fa fa-university form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Tiene Descuento:</label>
		                  <select required class="form-control has-feedback-left" name="descuento" id="inputSuccess18" placeholder="Tiene Descuento" onchange="select_descuento(this.value)">
		                    <option disabled selected hidden>Tiene Descuento</option>
		                    <option>Si</option>
		                    <option>No</option>
		                  </select>
		                  <span class="fa fa-minus form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group">
		                  <label>Tiempo Restante de Descuento</label>                  
		                  <div class="row">
		                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
		                      <div class="row">
		                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Años</label>
		                        <div class="col-md-8 col-sm-8 col-xs-12">
		                          <input type="text" name="anho_descuento" required class="form-control" id="inputSuccess19" placeholder="Años" readonly="" onkeypress="return justNumbers(event);">
		                        </div>
		                      </div>
		                    </div>
		                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
		                      <div class="row">
		                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Meses</label>
		                        <div class="col-md-8 col-sm-8 col-xs-12">
		                          <input type="text" name="mes_descuento" required class="form-control" id="inputSuccess192" placeholder="Meses" readonly="" onkeypress="return justNumbers(event);">
		                        </div>
		                      </div>
		                    </div>
		                  </div>                  
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Cantidad de Descuento:</label>
		                  <input type="text" name="cantidad_descuento" required class="form-control has-feedback-left" id="oinputSuccess" placeholder="Cantidad de Descuento" readonly="" onkeyup="posee_capacidad()">
		                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="clearfix"></div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Tipo de Descuento</label>
		                  <select required class="form-control has-feedback-left" name="tipo_descuento" id="oinputSuccess1" placeholder="Tipo de Descuento" disabled="" onclick="posee_capacidad()">
		                    <option disabled selected hidden>Tipo de Descuento</option>
		                    <option>Quincenal</option>
		                    <option>Mensual</option>
		                  </select>
		                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
		                  <h2>Dirección del Cliente</h2>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Provincia:</label>
		                  <input type="text" name="provincia" required class="form-control has-feedback-left" id="inputSuccess23" placeholder="Provincia">
		                  <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Distrito:</label>
		                  <input type="text" name="distrito" required class="form-control has-feedback-left" id="inputSuccess22" placeholder="Distrito">
		                  <span class="fa fa-map-o form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Corregimiento:</label>
		                  <input type="text" name="corregimiento" required class="form-control has-feedback-left" id="inputSuccess21" placeholder="Corregimiento">
		                  <span class="fa fa-map-signs form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
		                  <label>Dirección:</label>
		                  <textarea id="inputSuccess24" required="required" class="form-control" name="direccion" placeholder="Dirección"></textarea>
		                </div>

		                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
		                  <h2>Información de la Empresa</h2>
                  		  <div class="fecha_inicio"></div>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Salario:</label>
		                  <input type="text" name="salario" required class="form-control has-feedback-left" id="inputSuccess25" placeholder="Salario" onkeyup="posee_capacidad()">
		                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Posición de Trabajo:</label>
		                  <input type="text" name="posicion_trabajo" required class="form-control has-feedback-left" id="inputSuccess26" placeholder="Posición de Trabajo">
		                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback selectcompany">
		                  <label>Compañia:</label>
		                  <select required class='form-control' name='companias_id' id='inputSuccess27' style="width: 100%"></select>
		                </div>

		                <div class="col-md-1 col-sm-1 col-xs-12 form-group has-feedback">
		                  <br>
		                  <button type="button" id="btncompany" class="btn btn-primary" title="Crear Compañia" onclick="add_companies()"><i class="fa fa-building-o"></i></button>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Inicio de Labores</label>
		                  <input type="text" name="inicio_labores" required class="form-control has-feedback-left date-picker" id="inputSuccess281" maxlength="10" placeholder="Inicio de Labores">
		                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Tiempo Laborando:</label>
		                  <input type="text" name="tiempo_laborando" required class="form-control has-feedback-left" id="inputSuccess28" placeholder="Tiempo Laborando">
		                  <span class="fa fa-clock-o form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Nombre del Jefe Directo:</label>
		                  <input type="text" name="nombre_jefe_directo" required class="form-control has-feedback-left" id="inputSuccess29" placeholder="Nombre del Jefe Directo">
		                  <span class="fa fa-black-tie form-control-feedback left" aria-hidden="true"></span>
		                </div> 

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Capacidad:</label>
		                  <select required class="form-control has-feedback-left" name="capacidad" id="inputSuccess20" placeholder="Capacidad">
		                    <option disabled selected hidden>Capacidad</option>
		                    <option>Si</option>
		                    <option>No</option>
		                  </select>
		                  <span class="fa fa-check form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Area</label>
		                  <input type="text" name="area" required class="form-control has-feedback-left" id="area" placeholder="Area" maxlength="6">
		                  <span class="fa fa-black-tie form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Ministerio</label>
		                  <input type="text" name="ministerio" required class="form-control has-feedback-left" id="ministerio" placeholder="Ministerio" maxlength="6">
		                  <span class="fa fa-black-tie form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Planilla</label>
		                  <input type="text" name="planilla" required class="form-control has-feedback-left" id="planilla" placeholder="Planilla" maxlength="6">
		                  <span class="fa fa-black-tie form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Posición</label>
		                  <input type="text" name="posicion" required class="form-control has-feedback-left" id="posicion" placeholder="Posición" maxlength="6">
		                  <span class="fa fa-black-tie form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
		                  <h2>Referencia Personal</h2><!-- <small>Referencia Uno</small> -->
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Nombre:</label>
		                  <input type="text" name="nombre_referencia" required class="form-control has-feedback-left" id="inputSuccess30" placeholder="Nombre">
		                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Apellido:</label>
		                  <input type="text" name="apellido_referencia" required class="form-control has-feedback-left" id="inputSuccess31" placeholder="Apellido">
		                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Parentesco:</label>
		                  <input type="text" name="parentesco_referencia" required class="form-control has-feedback-left" id="inputSuccess31" placeholder="Parentesco">
		                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-4 form-group has-feedback">
		                  <label>Dirección:</label>
		                  <textarea id="inputSuccess32" required="required" class="form-control" name="direccion_referencia" placeholder="Dirección"></textarea>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Teléfono:</label>
		                  <input type="text" name="telefono_referencia" required class="form-control has-feedback-left" id="inputSuccess31" placeholder="Teléfono">
		                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
		                </div>

		                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
		                  <label>Celular:</label>
		                  <input type="text" name="celular_referencia" required class="form-control has-feedback-left" id="inputSuccess32" placeholder="Celular">
		                  <span class="fa fa-mobile form-control-feedback left" aria-hidden="true"></span>
		                </div>

		              </form>

              		<div class="inforequeridos"></div>
                    <button type="button" onclick="cancel_client()" class="btn btn-danger btn-xs pull-right"><i class="fa fa-close"></i> Cancelar</button>
                    <button type="button" id="btnSave" onclick="anular_solicitud()" class="btn btn-warning btn-xs pull-right"><i class="fa fa-close"></i> Anular</button>
                    <button type="button" id="btnSave" onclick="save_cliente()" class="btn btn-primary btn-xs pull-right"><i class="fa fa-save"></i> Guardar</button>
                    <button type="button" onclick="file_client()" class="btn btn-info btn-xs pull-right"><i class="fa fa-file"></i> Archivos</button>
                    <button type="button" id="btnSaveClient" onclick="confirm_save_client()" class="btn btn-success btn-xs pull-right"><i class="fa fa-check"></i> Enviar a verificar</button>
                    <button type="button" id="btnNotas" onclick="notas_clientes_form()" class="btn btn-warning btn-xs pull-right"><i class="fa fa-file"></i> Notas</button>
                    <button type="button" id="btnDisposiciones" onclick="disposiciones_clientes_form()" class="btn btn-warning btn-xs pull-right"><i class="fa fa-bookmark"></i> Disposiciones</button>
                  </div>
                </div>
              </div>
          </div>




        </div>
        <!-- /page content -->

<!-- Bootstrap modal -->
<!-- <div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Solicitud</h3>
            </div>
            <div class="modal-body form">

              <form id="form" class="form-horizontal form-label-left input_mask">
              <input type="hidden" value="" name="id" id="idc" />
              <input type="hidden" value="" name="solicitud_id" id="ids" />

                <div id="msg-error" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors animated shake"></div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="nombre" required class="form-control has-feedback-left" id="sinputSuccess" placeholder="Nombre" disabled>
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="apellido" required class="form-control" id="sinputSuccess1" placeholder="Apellido" disabled>
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="email" name="correo" required class="form-control has-feedback-left" id="sinputSuccess2" placeholder="Correo Electrónico" disabled>
                  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="telefono" required class="form-control has-feedback-left" id="sinputSuccess3" placeholder="Teléfono" onkeypress="return justNumbers(event);" disabled>
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="cedula" required class="form-control" id="sinputSuccess4" placeholder="Cédula">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="salario" required class="form-control has-feedback-left" id="sinputSuccess5" placeholder="Salario" disabled>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

                 <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="inicio_labores" required class="form-control has-feedback-left date-picker" id="sinputSuccess281" placeholder="Inicio de Labores">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <input type="text" name="tiempo_laborando" required class="form-control" id="sinputSuccess6" placeholder="Tiempo Laborando" readonly="">
                  <span class="fa fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <select required class="form-control has-feedback-left" name="descuento" id="sinputSuccess7" placeholder="Tiene Descuento" disabled>
                    <option disabled selected hidden>Tiene Descuento</option>
                    <option>Si</option>
                    <option>No</option>
                  </select>
                  <span class="fa fa-minus form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <input type="text" name="cantidad_solicitada" required class="form-control has-feedback-left" id="sinputSuccess8" placeholder="Cantidad Solicitada" disabled>
                  <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                </div>

              </form>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div> -->
<!-- End Bootstrap modal -->

<!-- Modal para eliminar -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea eliminar el registro?
      </div>
      <div id="info" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal eliminar -->

<!-- Modal para guardar cliente -->
<div id="myModal2" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        El cliente pasara a estado verificado, deseas continuar?
      </div>
      <div class="modal-footer">
		<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="save_client()" data-dismiss="modal"><i class="fa fa-check"></i> Si</a>
		<a class="btn btn-danger btn-xs" href="javascript:void(0)" data-dismiss="modal"><i class="fa fa-close"></i> No</a>
      </div>
    </div>
  </div>
</div>
<!-- Fin de modal guardar cliente -->

<!-- Modal para anular solicitudes -->
<div id="myModal3" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Esta seguro de anular esta solicitud? Asegurese de haber guardado una nota.
      </div>
      <div class="modal-footer">
      	<div class="infoanular"></div>
      </div>
    </div>
  </div>
</div>
<!-- Fin de modal anular solicitudes -->

<!-- Modal para deselegir -->
<div id="myModal4" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        Realmente desea deseleccionar la solicitud?
      </div>
      <div id="info4" class="modal-footer"><!-- Botones --></div>
    </div>
  </div>
</div>
<!-- Fin de modal deselegir -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_view" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Información de la Solicitud</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <strong><u><h2 class="modal-nombre text-primary"><!-- Nombre --></h2></u></strong>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-fecha-creacion">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-telefono">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-salario">
                      <!-- dato -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback modal-cantidad-solicitada">
                      <!-- dato -->
                    </div>

                </div> 
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                <i class="fa fa-close"></i> Cerrar
              </button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->

<!-- Compañias modal -->
<div class="modal fade" id="modal_form_compania" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title_compania">Añadir Compañias</h3>
            </div>
            <div class="modal-body form">

              <form id="form_compania" class="form-horizontal form-label-left input_mask">
              <input type="hidden" value="" name="id"/>

                <div id="msg-error_compania" class="col-md-12 col-sm-12 col-xs-12 form-group company has-feedback">
                  <div class="list-errors_compania animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group company has-feedback">
                    <input type="text" name="nombre" required class="form-control has-feedback-left" id="inputSuccess" placeholder="Nombre de la Compañia">
                    <span class="fa fa-building-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group company has-feedback">
                  <!-- <label>Nombre:</label> -->
                  <textarea id="inputSuccess1" required class="form-control" name="direccion" placeholder="Dirección"></textarea>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Suma Minima:</label> -->
                  <input type="text" name="telefono" required class="form-control has-feedback-left" id="inputSuccess2" placeholder="Teléfono">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Nombre:</label> -->
                  <input type="text" name="ruc" required class="form-control" id="inputSuccess3" placeholder="RUC">
                  <span class="fa fa-registered form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Suma Minima:</label> -->
                  <input type="text" name="sitio_web" required class="form-control has-feedback-left" id="inputSuccess4" placeholder="Sitio Web">
                  <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Nombre:</label> -->
                  <input type="text" name="ano_constitucion" required class="form-control" id="inputSuccess5" placeholder="Año de Constitución">
                  <span class="fa fa-calendar-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Suma Minima:</label> -->
                  <input type="text" name="correo" required class="form-control has-feedback-left" id="inputSuccess6" placeholder="Correo Electrónico">
                  <span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Nombre:</label> -->
                  <input type="text" name="nombre_rrhh" required class="form-control" id="inputSuccess7" placeholder="Nombre del Recursos Humano">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Suma Minima:</label> -->
                  <input type="text" name="telefono_rrhh" required class="form-control has-feedback-left" id="inputSuccess8" placeholder="Teléfono de Recursos Humanos">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Nombre:</label> -->
                  <input type="text" name="nombre_contabilidad" required class="form-control" id="inputSuccess7" placeholder="Nombre de Contabilidad">
                  <span class="fa fa-bookmark-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Suma Minima:</label> -->
                  <input type="text" name="telefono_contabilidad" required class="form-control has-feedback-left" id="inputSuccess8" placeholder="Teléfono de Contabilidad">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Nombre:</label> -->
                  <input type="text" name="forma_pago" required class="form-control" id="inputSuccess9" placeholder="Forma de Pago">
                  <span class="fa fa-usd form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Suma Minima:</label> -->
                  <input type="text" name="cantidad_empleados" required class="form-control has-feedback-left" id="inputSuccess10" placeholder="Cantidad de Empleados">
                  <span class="fa fa-users form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group company has-feedback">
                  <!-- <label>Nombre:</label> -->
                  <input type="text" name="rubro" required class="form-control" id="inputSuccess11" placeholder="Rubro">
                  <span class="fa fa-bookmark-o form-control-feedback right" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <!-- <label>Nombre Firma</label> -->
                  <input type="text" name="nombre_firma" required class="form-control has-feedback-left" id="nombre_firma" placeholder="Nombre Firma">
                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                  <!-- <label>Posición Firma</label> -->
                  <input type="text" name="posicion_firma" required class="form-control" id="posicion_firma" placeholder="Posición Firma">
                  <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_compania" onclick="save_companies()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Compañias modal -->



<!-- Upload modal -->
<div class="modal fade" id="modal_form_files" role="dialog">
    <!-- <div class="modal-dialog modal-sm"> -->
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Archivos del Cliente</h3>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-xs-12 bottom">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                      <h2 class="modal-nombre-archivo text-info"><!-- Nombre Cliente --></h2>
                    </div>

                     <form method="POST" class="myForm" enctype="multipart/form-data">
                          
                          <input type="hidden" value="" name="cliente_id" id="client"/>

                          <div class="row col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
			                  <div class="row checkbox">
			                    <label>
			                      <input type="checkbox" class="flat" name="archivo_original" value="Si"> Archivo Original
			                    </label>
			                  </div>
                          </div>

                          <div class="col-lg-6 col-sm-6 col-12">                            
                              <div class="input-group">
                                  <label class="input-group-btn">
                                      <span class="btn btn-primary">
                                          Examinar&hellip; <input type="file" name="file" style="display: none;" multiple>
                                      </span>
                                  </label>
                                  <input type="text" name="nombre_archivo" class="form-control" readonly>
                              </div>
                          </div>

                          <div class="col-lg-4 col-sm-4 col-12">
                            <?php echo "<select required class='form-control' name='tipos_archivos_id' id='tipos_archivos_id' placeholder='Tipo de Archivo'><option disabled selected hidden>Tipo de Archivo</option>";
                            if (count($result_tipo_archivos)) {
                              foreach ($result_tipo_archivos as $row) {
                                echo "<option value='". $row['id'] . "'>" . $row['tipo'] . "</option>";
                              }
                            }
                            echo "</select>";?> 
                          </div>


                          <div class="col-lg-2 col-sm-2 col-12">
                              <button type="button" class="btn btn-success" onclick="submitFile();">
                                  <i class="fa fa-upload"></i> Subir
                              </button>
                          </div> 

                          <div class="col-lg-12 col-sm-12 col-12">
                              <textarea id="descripcion_archivo" required class="form-control" name="descripcion_archivo" placeholder="Descripción"></textarea>
                          </div>                  

                      </form>

                    <div class="col-lg-12 col-sm-12 col-12">
                        <span class="permission-error-archivo label label-danger"></span>
                    </div>

                    <hr>

                    <div class="col-lg-3 col-sm-3 col-12">
                      <select required class='form-control' name='filtrar_tipos_id' id='filtrar_tipos_id' placeholder='Filtrar por' onchange="filtrar_archivos($('#client').val())"><option disabled selected hidden>Filtrar por</option>
                        <?php if (count($result_tipo_archivos)) {  
                          foreach ($result_tipo_archivos as $row) {
                            echo "<option value='". $row['id'] . "'>" . $row['tipo'] . "</option>";
                          }
                        } ?>
                      </select>
                    </div>

                    <div class="col-lg-3 col-sm-3 col-12">
                      <button class="btn btn-default" onclick="view_files($('#client').val())"><i class="glyphicon glyphicon-refresh"></i> Reiniciar</button>
                    </div>

                    <div class="col-lg-12 col-sm-12 col-12 table-responsive">
                        <table class="table table-striped table-bordered jambo_table" id="datatable-archivos" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="hidden">Archivo</th>
                                    <th>Tipo</th>
                                    <th>Descripción</th>
                                    <th>Original</th>
                                    <th>Ultima Modificación</th>
                                    <th>Creado Por</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody class="filas">
                            </tbody>
                        </table>
                    </div>

                </div> 
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">
                    <i class="fa fa-close"></i> Cerrar
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End Upload modal-->



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_notas" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Nota</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-nota text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_notas" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="clientes_id"/>

                <div id="msg-error-notas" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-notas animated shake"></div>
                </div>

<!--                 <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback notas_id">
                  <select class="form-control" name="notas_id" id="notas_id" style="width: 100%" onchange="notas(this.value)"></select>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback nombrenota">
                  <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre">
                </div> -->

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <textarea id="nota" required class="form-control" name="nota" placeholder="Nota"></textarea>
                </div>

              </form>

              <div class="table-responsive notes_datos">
                <table class="table table-striped jambo_table bulk_action">
                  <thead>
                    <tr class="headings">
                      <th class="column-title">Fecha</th>
                      <!-- <th class="column-title">Nombre</th> -->
                      <th class="column-title">Nota</th>
                      <th class="column-title">Registrado por</th>
                    </tr>
                  </thead>

                  <tbody class="datos_notas">
                  </tbody>
                </table>
              </div>


            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_notes()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Guardar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_disposiciones" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Añadir Disposición</h3>
            </div>
            <div class="modal-body">

              <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                <h2 class="modal-nombre-disposicion text-info"><!-- Nombre --></h2>
              </div>

              <form id="form_disposiciones" class="form-horizontal form-label-left input_mask">
                
                <input type="hidden" value="" name="solicitud_id"/>
                <input type="hidden" value="" name="bandera"/>

                <div id="msg-error-disposiciones" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                  <div class="list-errors-disposiciones animated shake"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback disposiciones_id">
                  <select class="form-control" name="disposiciones_id" id="disposiciones_id" style="width: 100%"></select>
                </div>

              </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_disposicion()" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Asignar</button>
                <button type="button" class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- End Bootstrap modal -->



    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>



<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {
    table = $('#datatable-responsive-solicitudes').DataTable( {
        "order": [],
        "destroy": true,
        "bRetrieve": true,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Lo siento, no hay registros",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(filtro de _MAX_ registros en total)",
            "sSearch": "Buscar",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "processing": "Procesando..."
        },
        "processing": true,
        "ajax": {
            "url": "<?php echo site_url('mis_solicitudes/my_list/Prospecto')?>",
            "type": "POST"
        }
    } );

    $("#inputSuccess4").inputmask();

    function calcular_tiempo_laborando(){
      $('.fecha_inicio').html('');
      $.ajax({
          url : "<?php echo site_url('clientes/calcular_tiempo_laborando')?>",
          type: "POST",
          data: {"inicio_labores": $('[name="inicio_labores"]').val()},
          dataType: "JSON",
          success: function(data)
          {
              if (data.datos) {
                $('#inputSuccess28').val(data.datos);
              } else {
                $('.fecha_inicio').html(data.error);
                $('#inputSuccess28').val(null);
              }
              if (data.validation) {
                $('.fecha_inicio').html(data.validation);
                $('#inputSuccess28').val(null);                
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error get data from ajax');
          }
      });
    }

    $('#finputSuccess8').datetimepicker({
        format: "DD/MM/YYYY",
        maxDate: new Date()
    });

    $('#inputSuccess281').datetimepicker({
        format: "DD/MM/YYYY",
        maxDate: new Date()
    });
    $("#inputSuccess281").on("dp.change",function (e) {
       calcular_tiempo_laborando();
    });

    $(".select2_group").select2({});
    $(".select2_multiple").select2({
      maximumSelectionLength: 4,
      placeholder: "With Max Selection limit 4",
      allowClear: true
    });


    $("#filtrar_disposiciones").select2({
      placeholder: "Filtrar por Disposición",
      allowClear: false,
      language: "es",
      theme: "classic",
    });

    disposiciones_for_select();


});

function justNumbers(e)
{
   var keynum = window.event ? window.event.keyCode : e.which;
   if ((keynum == 8) || (keynum == 46))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function view_request(id)
{
    $('.permission-error').hide();
    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_view/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {            
            $('#modal_form_view').modal('show');
            $('.modal-title').text('Información de la Solicitud');
            $('.modal-nombre').text(data.nombre);
            $('.modal-fecha-creacion').html('<strong><i class="fa fa-calendar"></i> Fecha de Creación: </strong>' + data.fecha_creacion);
            $('.modal-telefono').html('<strong><i class="fa fa-phone"></i> Teléfono: </strong>' + data.telefono);
            $('.modal-salario').html('<strong><i class="fa fa-usd"></i> Salario: $</strong>' + data.salario);
            $('.modal-cantidad-solicitada').html('<strong><i class="fa fa-usd"></i> Cantidad Solicitada: $</strong>' + data.cantidad_solicitada);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    $('.permission-error').hide();
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    //$('#btnSave').text('procesando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    $.ajax({
        url : "<?php echo site_url('solicitudes/ajax_update')?>",
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if (data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                $('#msg-error').hide();
            }
            else if (data.validation)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake">'+data.validation+'</div>');
            }
            else if (data.permission)
            {
                $('#modal_form').modal('hide');
                $('#msg-error').hide();
                $('.permission-error').show();
                $('.permission-error').html(data.permission);
            }
            else if (data.rol)
            {
                $('#msg-error').show();
                $('.list-errors').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
            }

            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            //$('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_request(id)
{
    if(id) {
        $.ajax({
            url : "<?php echo site_url('solicitudes/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                $('#modal_form').modal('hide');
                reload_table();
                if(data.permission)
                {
                    $('.permission-error').show();
                    $('.permission-error').html(data.permission);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
        $("#myModal").modal('hide');
    }
}

function confirm_delete(id) {

    $('.permission-error').hide();

    $("#myModal").on("show", function() {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function(e) {
            console.log("button pressed");   // just as an example...
            $("#myModal").modal('hide');     // dismiss the dialog
        });
    });

    $("#myModal").on("hide", function() {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
    });
    
    $("#myModal").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
    });
    
    $("#myModal").modal({                    // wire up the actual modal functionality and show the dialog
      "backdrop"  : "static",
      "keyboard"  : true,
      "show"      : true                     // ensure the modal is shown immediately
    });

    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_request('+id+')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');

}

</script>












<script type="text/javascript">

	$('#div-prospecto').hide();

	function prospectos(id){
		$('#list-prospectos').hide();
		$('#div-prospecto').show();
		$('#msg-error-client').hide();
		$('.inforequeridos').hide();
		activar_direcciones();
		view_companies(id);
	}

	var save_method_companies;

	function validarCedula(val)
	{
	    var pvalor = null;
	    if (val.charAt(0).toUpperCase() == "P" || val.charAt(0).toUpperCase() == "E" || val.charAt(0).toUpperCase() == "N") {
	      pvalor = val.charAt(0).toUpperCase() + val.slice(1);
	      $("#inputSuccess3").val(pvalor);
	    }
	    if (val.charAt(1).toUpperCase() == "P" || val.charAt(1).toUpperCase() == "E" || val.charAt(1).toUpperCase() == "N") {
	      pvalor = val.charAt(0).toUpperCase() + val.charAt(1).toUpperCase() + val.slice(1);
	      $("#inputSuccess3").val(pvalor);
	    }
	    if (val.charAt(0).toUpperCase() == "-") {
	      $("#inputSuccess3").val(null);
	    }
	    //$("#inputSuccess3").inputmask("Regex", { regex: "[pPeEnN]{0,2}[0-9]{0,1}-[0-9]*" }); 
	    $("#inputSuccess3").inputmask("Regex", { regex: "[pPeEnN0-9]{2}-[0-9]{4}-[0-9]*" });
	}

	function select_hipoteca(val){
	    if (val == "Si") {
	        $('#inputSuccess17').removeAttr("readonly");
	    } else {
	        $('#inputSuccess17').val(null);
	        $('#inputSuccess17').attr("readonly","readonly");
	    }
	}

	function select_descuento(val){
	    if (val == "Si") {
	        $('#inputSuccess19').removeAttr("readonly");
	        $('#inputSuccess192').removeAttr("readonly");
	        $('#oinputSuccess').removeAttr("readonly");
	        $('#oinputSuccess1').removeAttr("disabled");
	    } else {
	        $('#inputSuccess19').val(null);
	        $('#inputSuccess19').attr("readonly","readonly");
	        $('#inputSuccess192').val(null);
	        $('#inputSuccess192').attr("readonly","readonly");
	        $('#oinputSuccess').val(null);
	        $('#oinputSuccess').attr("readonly","readonly");
	        $('#oinputSuccess1').prop('selectedIndex',0);
	        $('#oinputSuccess1').attr("disabled","disabled");
	    }
	}


	function select_como_escucho(val){
	    $('#1inputSuccess13').val(null);
	    if (val == "Otros") {
	        $('.como').show();
	    } else {
	        $('.como').hide();
	    }
	}

	function posee_capacidad(){

	    var salario = $('#inputSuccess25').val();
	    var descuento = $('#oinputSuccess').val();

	    if (salario >= 0 && salario != "") {
	        if (descuento > 0) {
	            if ($('#oinputSuccess1').val() == "Quincenal") {
	                descuento = descuento * 2;
	            }
	            if (descuento <= (salario * 0.2)) {
	                $('#inputSuccess20').prop('selectedIndex',1);
	            } else {
	                $('#inputSuccess20').prop('selectedIndex',2);
	            }
	        } else {
	            $('#inputSuccess20').prop('selectedIndex',1);
	        }
	    } else {
	        $('#inputSuccess25').val(null);
	        $('#inputSuccess20').prop('selectedIndex',0);
	    }
	}

	function activar_direcciones(){
	    $.ajax({
	        url : "<?php echo site_url('clientes/view_direcciones')?>",
	        type: "GET",
	        dataType: "JSON",
	        success: function(data)
	        {

	          var bancoArray = $.map(data.banco, function(value, key) {
	            return {
	              value: value,
	              data: key
	            };
	          });
	          $('#inputSuccess17').autocomplete({
	            lookup: bancoArray
	          });

	          var corregimientoArray = $.map(data.corregimiento, function(value, key) {
	            return {
	              value: value,
	              data: key
	            };
	          });
	          $('#inputSuccess21').autocomplete({
	            lookup: corregimientoArray
	          });

	          var distritoArray = $.map(data.distrito, function(value, key) {
	            return {
	              value: value,
	              data: key
	            };
	          });
	          $('#inputSuccess22').autocomplete({
	            lookup: distritoArray
	          });

	          var provinciaArray = $.map(data.provincia, function(value, key) {
	            return {
	              value: value,
	              data: key
	            };
	          });
	          $('#inputSuccess23').autocomplete({
	            lookup: provinciaArray
	          });


	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error get data from ajax');
	        }
	    });
	}

	function view_companies(id)
	{
	    $.ajax({
	        url : "<?php echo site_url('clientes/view_companies')?>",
	        type: "GET",
	        dataType: "JSON",
	        success: function(data)
	        {
	            $('#inputSuccess27').empty();
	            var trHTML = "<option></option>";
	            if (data.length > 0) {
	                $.each(data, function (i, item) {
	                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
	                });
	            }
	            $('#inputSuccess27').append(trHTML);

	            $("#inputSuccess27").select2({
	              placeholder: "Empresa",
	              allowClear: true,
	              language: "es",
	              theme: "classic",
	              dropdownParent: $(".selectcompany")
	            });

	            if (id != null) {
	              editar(id);
	            }

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error get data from ajax');
	        }
	    });
	}

	function editar(id){
	    $.ajax({
	        url : "<?php echo site_url('mis_solicitudes/edit_client/')?>" + id,
	        type: "GET",
	        dataType: "JSON",
	        success: function(data)
	        {
	            $('[name="id"]').val(data.id);
	            $('[name="solicitud_id"]').val(data.solicitud_id);
	            $('[name="nombre"]').val(data.nombre);
	            $('[name="apellido"]').val(data.apellido);
	            if (data.genero != null) { $('[name="genero"]').val(data.genero); }              
	            $('[name="cedula"]').val(data.cedula);
	            $('[name="telefono"]').val(data.telefono);
	            $('[name="celular"]').val(data.celular);
	            $('[name="estado_civil"]').val(data.estado_civil);
	            $('[name="correo"]').val(data.correo);
	            $('[name="fecha_nacimiento"]').val(data.fecha_nacimiento);
	            $('[name="numero_dependientes"]').val(data.numero_dependientes);
	            $('[name="cantidad_solicitada"]').val(data.cantidad_solicitada);
	            $('[name="fuente_cliente"]').val(data.fuente_cliente);	 
	            $('[name="como_escucho_nosotros"]').val(data.como_escucho_nosotros);  
	            select_como_escucho(data.como_escucho_nosotros);	                    
	            if (data.como_escucho_nosotros != null) { 
	            	$('[name="como_escucho_nosotros_otros"]').val(data.como_escucho_nosotros_otros);
	            } else {	            	        	
	            	$('#inputSuccess13').prop('selectedIndex',0); 
	            }  
	            if (data.tipo_vivienda != null) { $('[name="tipo_vivienda"]').val(data.tipo_vivienda); } 
	            if (data.hipotecas != null) { $('[name="hipotecas"]').val(data.hipotecas); }
	            if (data.hipotecas == "Si") {$('#inputSuccess17').removeAttr("readonly");}
	            else {$('#inputSuccess17').attr("readonly","readonly");}
	            $('[name="banco"]').val(data.banco);
	            if (data.descuento != null) { $('[name="descuento"]').val(data.descuento); }
	            if (data.descuento == "Si") {
	            	$('#inputSuccess19').removeAttr("readonly");
	            	$('#inputSuccess192').removeAttr("readonly");
	            	$('#oinputSuccess').removeAttr("readonly");
	            	$('#oinputSuccess1').removeAttr("disabled");	            	
	            }
	            else {
	            	$('#inputSuccess19').attr("readonly","readonly");
	        		$('#inputSuccess192').attr("readonly","readonly");
	            	$('#oinputSuccess').attr("readonly","readonly");
	            	$('#oinputSuccess1').attr("disabled","disabled");
	            }
	            $('[name="anho_descuento"]').val(data.anho_descuento);
	            $('[name="mes_descuento"]').val(data.mes_descuento);	            
	            $('[name="cantidad_descuento"]').val(data.cantidad_descuento);
	            if (data.tipo_descuento != null) {
	            	$('[name="tipo_descuento"]').val(data.tipo_descuento);
	            } else {
	            	$('#oinputSuccess1').prop('selectedIndex',0);
	            }
	            if (data.capacidad != null) { $('[name="capacidad"]').val(data.capacidad); }	            
	            $('[name="corregimiento"]').val(data.corregimiento);
	            $('[name="distrito"]').val(data.distrito);
	            $('[name="provincia"]').val(data.provincia);
	            $('[name="direccion"]').val(data.direccion);
	            $('[name="salario"]').val(data.salario);
	            $('[name="posicion_trabajo"]').val(data.posicion_trabajo);
	            $('[name="inicio_labores"]').val(data.inicio_labores);
	            $('[name="tiempo_laborando"]').val(data.tiempo_laborando);
	            $('[name="area"]').val(data.area);
	            $('[name="ministerio"]').val(data.ministerio);
	            $('[name="planilla"]').val(data.planilla);
	            $('[name="posicion"]').val(data.posicion);
	            $('[name="nombre_jefe_directo"]').val(data.nombre_jefe_directo);
	            $('[name="nombre_referencia"]').val(data.nombre_referencia);
	            $('[name="apellido_referencia"]').val(data.apellido_referencia);
	            $('[name="parentesco_referencia"]').val(data.parentesco_referencia);
	            $('[name="direccion_referencia"]').val(data.direccion_referencia);
	            $('[name="telefono_referencia"]').val(data.telefono_referencia);
	            $('[name="celular_referencia"]').val(data.celular_referencia);

	            $('[name="companias_id"]').val(data.companias_id);
	            $('#inputSuccess27').select2().on("select2-selecting", function(e) {
	              //log("selecting val=" + e.val + " choice=" + e.object.text);
	            });
	            $("#inputSuccess27").select2({
	              placeholder: "Empresa",
	              allowClear: true,
	              language: "es",
	              theme: "classic",
	              dropdownParent: $(".selectcompany")
	            });

	            posee_capacidad();

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error get data from ajax');
	        }
	    });
	}

	function add_companies()
	{
	    save_method_companies = 'add_companies';
	    $('#msg-error_compania').hide();
	    $('#form_compania')[0].reset();
	    $('.company').removeClass('has-error');
	    $('#modal_form_compania').modal('show');
	    $('.modal-title_compania').text('Añadir Compañias');
	}

	function select_company(id){
	    $.ajax({
	        url : "<?php echo site_url('clientes/view_companies')?>",
	        type: "GET",
	        dataType: "JSON",
	        success: function(data)
	        {
	            $('#inputSuccess27').empty();
	            var trHTML = "<option></option>";
	            if (data.length > 0) {
	                $.each(data, function (i, item) {
	                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
	                });
	            }
	            $('#inputSuccess27').append(trHTML);
	            $('[name="companias_id"]').val(id);
	            $('#inputSuccess27').select2().on("select2-selecting", function(e) {});
	            $("#inputSuccess27").select2({
	              placeholder: "Empresa",
	              allowClear: true,
	              language: "es",
	              theme: "classic",
	              dropdownParent: $(".selectcompany")
	            });
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error get data from ajax');
	        }
	    });
	}	

	function save_companies()
	{
	    $('#btnSave_compania').attr('disabled',true); //set button disable 
	    var url;
	    if(save_method_companies == 'add_companies') {
	        url = "<?php echo site_url('clientes/add_company')?>";
	    }
	    $.ajax({
	        url : url,
	        type: "POST",
	        data: $('#form_compania').serialize(),
	        dataType: "JSON",
	        success: function(data)
	        {
	            if (data.status)
	            {
	                $('#modal_form_compania').modal('hide');
	                $('#msg-error_compania').hide();
	                //view_companies(null);
	                activar_direcciones();
	                select_company(data.id);
	            }
	            else if (data.validation)
	            {
	                $('#msg-error_compania').show();
	                $('.list-errors_compania').html('<div class="animated shake">'+data.validation+'</div>');
	            }
	            else if (data.permission)
	            {
	                $('#msg-error_compania').show();
	                $('.list-errors_compania').html('<div class="animated shake">'+data.permission+'</div>');
	            }
	            else if (data.rol)
	            {
	                $('#msg-error_compania').show();
	                $('.list-errors_compania').html('<div class="animated shake">'+data.rol+'</div>');
	            }
	            $('#btnSave_compania').attr('disabled',false); //set button enable
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	            $('#btnSave_compania').attr('disabled',false); //set button enable 
	        }
	    });
	}


	function confirm_save_client() {

	    $('.permission-error-archivo').hide();

	    $("#myModal2").on("show", function() {
	        $("#myModal2 a.btn").on("click", function(e) {
	            console.log("button pressed");
	            $("#myModal2").modal('hide');
	        });
	    });

	    $("#myModal2").on("hide", function() {
	        $("#myModal2 a.btn").off("click");
	    });
	    
	    $("#myModal2").on("hidden", function() {
	        $("#myModal2").remove();
	    });
	    
	    $("#myModal2").modal({
	      "backdrop"  : "static",
	      "keyboard"  : true,
	      "show"      : true
	    });
	}

	function save_cliente()
	{
	    $('#btnSaveClient').attr('disabled',true); //set button disable 
	    $.ajax({
	        url : "<?php echo site_url('mis_solicitudes/client_save_update')?>",
	        type: "POST",
	        data: $('#form_client').serialize(),
	        dataType: "JSON",
	        success: function(data)
	        {
	            if (data.status)
	            {
	                $('#msg-error-client').hide();
	                $('.inforequeridos').hide();
	                $('#div-prospecto').hide();
					$('#list-prospectos').show();
	                reload_table();
	            }
	            else if (data.validation)
	            {
	                $('#msg-error-client').show();
	                $('.inforequeridos').show();
	                $('.list-errors-client').html('<div class="animated shake">'+data.validation+'</div>');
	                $('.inforequeridos').html('<span class="label label-danger animated shake">Verifique los campos requeridos al inicio de la ventana.</span>');
	            }
	            else if (data.permission)
	            {
	                $('#msg-error-client').show();
	                $('.inforequeridos').hide();
	                $('.list-errors-client').html(data.permission);
	            }
	            else if (data.rol)
	            {
	                $('#msg-error-client').show();
	                $('.list-errors-client').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
	            }
	            $('#btnSaveClient').attr('disabled',false)
	            window.scrollTo(0, 0);
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	            $('#btnSaveClient').attr('disabled',false);
	        }
	    });
	}

	function save_client()
	{
        $('#msg-error-client').hide();
        $('.inforequeridos').hide();
	    $('#btnSave').attr('disabled',true); //set button disable 
	    $.ajax({
	        url : "<?php echo site_url('mis_solicitudes/client_update')?>",
	        type: "POST",
	        data: $('#form_client').serialize(),
	        dataType: "JSON",
	        success: function(data)
	        {
	            if (data.status)
	            {
	                $('#msg-error-client').hide();
	                $('.inforequeridos').hide();
	                $('#div-prospecto').hide();
					$('#list-prospectos').show();
	                reload_table();
	            }
	            else if (data.validation)
	            {
	                $('#msg-error-client').show();
	                $('.inforequeridos').show();
	                $('.list-errors-client').html('<div class="animated shake">'+data.validation+'</div>');
	                $('.inforequeridos').html('<span class="label label-danger animated shake">Verifique los campos requeridos al inicio de la ventana.</span>');
	            }
	            else if (data.permission)
	            {
	                $('#msg-error-client').show();
	                $('.inforequeridos').hide();
	                $('.list-errors-client').html(data.permission);
	            }
	            else if (data.rol)
	            {
	                $('#msg-error-client').show();
	                $('.list-errors-client').html('<div class="animated shake"><span class="permission-error label label-danger">'+data.rol+'</span></div>');
	            }
	            $('#btnSave').attr('disabled',false)
	            window.scrollTo(0, 0);
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	            $('#btnSave').attr('disabled',false);
	        }
	    });
	}

	function anular_solicitud(){
	    $('.permission-error-archivo').hide();
	    $("#myModal3").modal('show');
	    $('.infoanular').html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="annul()"><i class="fa fa-check"></i> Si</a>'+
			'<a class="btn btn-danger btn-xs" href="javascript:void(0)" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
	}


	function annul(){
		var id = $('[name="solicitud_id"]').val();
		$.ajax({
		  url: "<?php echo site_url('mis_solicitudes/anular_solicitud/')?>" + id,
		  type: 'POST',
		  dataType: "JSON",
		  success: function(data, textSatus, jqXHR){
		      if (data.status) {
		          $('#div-prospecto').hide();
		          $('#list-prospectos').show();
		          reload_table();
		          window.scrollTo(0, 0);
		      }
		  },
		  error: function(jqXHR, textStatus, errorThrown){
		      alert("error");
		  }
		});
		$("#myModal3").modal('hide');
	}

	function cancel_client() {
        $('#div-prospecto').hide();
		$('#list-prospectos').show();
        reload_table();
        window.scrollTo(0, 0);
	}

	function submitFile(){
	    var formData = new FormData($('.myForm')[0]);
	    var id = $('#client').val();
	    $.ajax({
	        url: "<?php echo site_url('clientes/cargar_archivo')?>",
	        type: 'POST',
	        data: formData,
	        mimeType: "multipart/form-data",
	        contentType: false,
	        cache: false,
	        processData: false,
	        dataType: "JSON",
	        success: function(data, textSatus, jqXHR){
	            if (data.status) {
	                view_files(id);
	            } else if(data.permission) {
	                $('.permission-error-archivo').show();
	                $('.permission-error-archivo').html(data.permission);
	            }
	        },
	        error: function(jqXHR, textStatus, errorThrown){
	            alert("error");
	        }
	    });
	}

	function file_client(){
		var id = $('[name="id"]').val();
	    $('#filtrar_tipos_id').prop('selectedIndex',0);
	    $('.permission-error').hide();
	    $('#modal_form_files').modal('show');
	    $('.modal-title').text('Archivos del Cliente');
	    $('[name="cliente_id"]').val(id);
	    view_files(id);
	}

	function view_files(id){
	    $('.permission-error-archivo').hide();
	    $('.myForm')[0].reset();
	    $('#filtrar_tipos_id').prop('selectedIndex',0);
	    $.ajax({
	        url : "<?php echo site_url('clientes/view_files/')?>" + id,
	        type: "GET",
	        dataType: "JSON",
	        success: function(data)
	        {
	            $('.filas').empty();
	            $('.modal-nombre-archivo').text(data.cliente);
	            var trHTML = '';
	            if (data.data.length > 0) {          
	                $.each(data.data, function (i, item) {
	                    trHTML += '<tr>' +
	                      '<td class="hidden">' + item[0] + '</td>' +
	                      '<td>' + item[1] + '</td>' +
	                      '<td>' + item[2] + '</td>' +
	                      '<td>' + item[3] + '</td>' +
	                      '<td>' + item[4] + '</td>' +
	                      '<td>' + item[5] + '</td>' +
	                      '<td>' + item[6] + '</td>' +
	                    '</tr>';
	                });                
	            } else {
	                trHTML += '<tr>' +
	                  '<td colspan="7" align="center">No hay archivos...</td>' +
	                '</tr>';
	            }
	            $('.filas').append(trHTML);
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	        }
	    });
	}

	function filtrar_archivos(cliente_id){
	    var id = $('#filtrar_tipos_id').val();
	    $.ajax({
	        url : "<?php echo site_url('clientes/filtrar_files/')?>/" + id,
	        type: "POST",
	        data: {"cliente_id":cliente_id},
	        dataType: "JSON",
	        success: function(data)
	        {
	            $('.filas').empty();
	            $('.modal-nombre-archivo').text(data.cliente);
	            var trHTML = '';
	            if (data.data.length > 0) {          
	                $.each(data.data, function (i, item) {
	                    trHTML += '<tr>' +
	                      '<td class="hidden">' + item[0] + '</td>' +
	                      '<td>' + item[1] + '</td>' +
	                      '<td>' + item[2] + '</td>' +
	                      '<td>' + item[3] + '</td>' +
	                      '<td>' + item[4] + '</td>' +
	                      '<td>' + item[5] + '</td>' +
	                      '<td>' + item[6] + '</td>' +
	                    '</tr>';
	                });                
	            } else {
	                trHTML += '<tr>' +
	                  '<td colspan="6" align="center">No hay archivos...</td>' +
	                '</tr>';
	            }
	            $('.filas').append(trHTML);
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	        }
	    });
	}

	function confirm_delete_file(file_id) {

	    $('.permission-error-archivo').hide();

	    $("#myModal").on("show", function() {
	        $("#myModal a.btn").on("click", function(e) {
	            console.log("button pressed");
	            $("#myModal").modal('hide');
	        });
	    });

	    $("#myModal").on("hide", function() {
	        $("#myModal a.btn").off("click");
	    });
	    
	    $("#myModal").on("hidden", function() {
	        $("#myModal").remove();
	    });
	    
	    $("#myModal").modal({
	      "backdrop"  : "static",
	      "keyboard"  : true,
	      "show"      : true
	    });

	    $("#info").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="delete_file('+ file_id +')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
	}

	function delete_file(file_id)
	{
	    $('.permission-error-archivo').hide();
	    var id = $('#client').val();
	    $.ajax({
	        url : "<?php echo site_url('clientes/file_delete/')?>/" + file_id,
	        type: "POST",
	        dataType: "JSON",
	        success: function(data)
	        {
	            if (data.status) {
	                view_files(id);
	            }
	            if(data.error)
	            {
	                $('.permission-error-archivo').show();
	                $('.permission-error-archivo').html(data.error);
	            }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            //alert('Error deleting data');
	            $('.permission-error-archivo').show();
	            $('.permission-error-archivo').html('No existe archivo en el directorio.');
	        }
	    });
	    $("#myModal").modal('hide');
	}

	function deselect_request(id){
	    $.ajax({
	        url : "<?php echo site_url('solicitudes/deselect_request/')?>" + id,
	        type: "POST",
	        dataType: "JSON",
	        success: function(data)
	        {
	          if (data.status) {
	              reload_table();
	          }
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error get data from ajax');
	        }
	    });
	    $("#myModal4").modal('hide');
	}

	function confirm_deselect_request(id) {

	    $('.permission-error-archivo').hide();

	    $("#myModal4").on("show", function() {
	        $("#myModal4 a.btn").on("click", function(e) {
	            console.log("button pressed");
	            $("#myModal4").modal('hide');
	        });
	    });

	    $("#myModal4").on("hide", function() {
	        $("#myModal4 a.btn").off("click");
	    });
	    
	    $("#myModal4").on("hidden", function() {
	        $("#myModal4").remove();
	    });
	    
	    $("#myModal4").modal({
	      "backdrop"  : "static",
	      "keyboard"  : true,
	      "show"      : true
	    });

	    $("#info4").html('<a class="btn btn-success btn-xs" href="javascript:void(0)" onclick="deselect_request('+ id +')"><i class="fa fa-check"></i> Si</a> <a class="btn btn-danger btn-xs" data-dismiss="modal"><i class="fa fa-close"></i> No</a>');
	}

	/*Modal dentro de modal*/
	$(document).ready(function () {
	    $('#openBtn').click(function () {
	        $('#modal_form').modal()
	    });

	    $('.modal')
	        .on({
	            'show.bs.modal': function() {
	                var idx = $('.modal:visible').length;
	                $(this).css('z-index', 1040 + (10 * idx));
	            },
	            'shown.bs.modal': function() {
	                var idx = ($('.modal:visible').length) - 1; // raise backdrop after animation.
	                $('.modal-backdrop').not('.stacked')
	                .css('z-index', 1039 + (10 * idx))
	                .addClass('stacked');
	            },
	            'hidden.bs.modal': function() {
	                if ($('.modal:visible').length > 0) {
	                    // restore the modal-open class to the body element, so that scrolling works
	                    // properly after de-stacking a modal.
	                    setTimeout(function() {
	                        $(document.body).addClass('modal-open');
	                    }, 0);
	                }
	            }
	        });
	});
	/*End Modal dentro de modal*/


	$(function() {

	  // We can attach the `fileselect` event to all file inputs on the page
	  $(document).on('change', ':file', function() {
	    var input = $(this),
	        numFiles = input.get(0).files ? input.get(0).files.length : 1,
	        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	    input.trigger('fileselect', [numFiles, label]);
	  });

	  // We can watch for our custom `fileselect` event like this
	  $(document).ready( function() {
	      $(':file').on('fileselect', function(event, numFiles, label) {

	          var input = $(this).parents('.input-group').find(':text'),
	              log = numFiles > 1 ? numFiles + ' files selected' : label;

	          if( input.length ) {
	              input.val(log);
	          } else {
	              if( log ) alert(log);
	          }

	      });
	  });
	  
	});


function disposiciones_clientes_form(){
	var id = $('[name="solicitud_id"]').val();
  	$('[name="bandera"]').val(id);
    disposiciones(id);
}


function notas_clientes_form(){
	var id = $('[name="id"]').val();
    notas_clientes(id);
}


function notas_clientes(id){
  $('#modal_form_notas').modal('show');
  $('.modal-title').text('Añadir Nota');
  $('#form_notas')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="clientes_id"]').val(id);
  $('#msg-error-notas').hide();
  //$('.nombrenota').hide();
  //view_notas();
  datos_notas(id);
}


function datos_notas(id){
  $.ajax({
      url : "<?php echo site_url('solicitudes/notas_cliente/')?>" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {

          $('.modal-nombre-nota').text(data.nombre_cliente);

          $('.datos_notas').empty();
          var trHTML = '';
          if (data.nota.length > 0) {
              $('.notes_datos').show();
              $.each(data.nota, function (i, item) {
                  trHTML += '<tr>' +
                    '<td>'+item.fecha_creacion+'</td>' +
                    '<td>'+item.nota+'</td>' +
                    '<td>'+item.usuarios_id+'</td>' +
                  '</tr>'
              });
          } else {
            $('.notes_datos').hide();
          }
          $('.datos_notas').append(trHTML);

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function save_notes(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_notes')?>",
      type: "POST",
      data: $('#form_notas').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            notas_clientes($('[name="clientes_id"]').val());
  			$('#msg-error-notas').hide();
          } else if (data.validation) {
            $('#msg-error-notas').show();
            $('.list-errors-notas').html('<div class="animated shake">'+data.validation+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}


function view_notas()
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_notas')?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#notas_id').empty();
            var trHTML = "<option></option>";
            if (data.length > 0) {
                $.each(data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            trHTML += "<option value='Otro'>Otro</option>";
            $('#notas_id').append(trHTML);

            $("#notas_id").select2({
              placeholder: "Notas",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".notas_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function notas(id){
  if (id == 'Otro') {
    $('#nota').removeAttr('readonly',false);
    $('[name="nota"]').val(null);
    $('.nombrenota').show();
  } else {
    $('.nombrenota').hide();
    $('#nota').attr('readonly','readonly');
    $.ajax({
        url : "<?php echo site_url('admin/view_nota/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="nota"]').val(data.nota);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('[name="nota"]').val(null);
        }
    }); 
  }
}



function disposiciones_for_select(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/disposiciones_for_select')?>",
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        if (data) {
          var trHTML = '<option value="0">Todos<option>';
          $.each(data, function(i,item){
            trHTML += '<option value='+item.id+'>'+item.nombre+'<option>';
          });
          $("#filtrar_disposiciones").html(trHTML);
          $("#filtrar_disposiciones").select2({
            placeholder: "Filtrar por Disposición",
            allowClear: false,
            language: "es",
            theme: "classic",
          });
        }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}

function disposiciones(id){
  $('#modal_form_disposiciones').modal('show');
  $('.modal-title').text('Añadir Disposición');
  $('#form_disposiciones')[0].reset();
  $('.form-group').removeClass('has-error');
  $('[name="solicitud_id"]').val(id);
  $('#msg-error-disposiciones').hide();
  view_disposiciones(id);
}


function view_disposiciones(id)
{
    $.ajax({
        url : "<?php echo site_url('solicitudes/view_disposiciones/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('.modal-nombre-disposicion').text(data.nombre_cliente);

            $('#disposiciones_id').empty();
            var trHTML = "<option></option>";
            if (data.data.length > 0) {
                $.each(data.data, function (i, item) {
                    trHTML += "<option value='" + item.id + "'>" + item.nombre + "</option>";
                });
            }
            $('#disposiciones_id').append(trHTML);

            $("#disposiciones_id").select2({
              placeholder: "Disposiciones",
              allowClear: true,
              language: "es",
              theme: "classic",
              dropdownParent: $(".disposiciones_id")
            });

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save_disposicion(){
  $.ajax({
      url : "<?php echo site_url('solicitudes/add_disposicion')?>",
      type: "POST",
      data: $('#form_disposiciones').serialize(),
      dataType: "JSON",
      success: function(data)
      {
          if (data.status) {
            $('#modal_form_disposiciones').modal('hide');
            if ($('[name="bandera"]').val() > 0) {
/*                $('#msg-error-client').hide();
                $('.inforequeridos').hide();
                $('#div-prospecto').hide();
				$('#list-prospectos').show();
            	reload_table();
            	window.scrollTo(0,0);*/
            	save_cliente();
            } else {
            	reload_table();
            }  
          } else if (data.validation) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.validation+'</div>');
          } else if (data.permission) {
            $('#msg-error-disposiciones').show();
            $('.list-errors-disposiciones').html('<div class="animated shake">'+data.permission+'</div>');
          }
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error deleting data');
      }
  });
}

function filtrar_disposiciones(disposicion){
  table.ajax.url("<?php echo site_url('mis_solicitudes/my_list/Prospecto/')?>" + disposicion).load();
}

</script>
<!-- /jQuery autocomplete -->
