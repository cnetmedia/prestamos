        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Perfil de Usuario</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Datos Personales</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                      <label>Nombres: </label>
                      <?= $dataUser['nombres'] ?>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                      <label>Apellidos: </label>
                      <?= $dataUser['apellidos'] ?>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                      <label>Correo Electrónico: </label>
                      <?= $dataUser['correo'] ?>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                      <label>Cédula: </label>
                      <?= $dataUser['cedula'] ?>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                      <label>Extensión: </label>
                      <?= $dataUser['extension'] ?>
                    </div>

                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Cambiar Contraseña</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form id="form_password" class="form-horizontal form-label-left input_mask">
                      <input type="hidden" value="<?= $dataUser['id'] ?>" name="idp"/>

                      <div id="msg-error-p" class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                        <div class="list-errors-p animated shake"></div>
                      </div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Contraseña Actual:</label>
                        <input type="password" name="vpassword" required class="form-control has-feedback-left" id="pinputSuccess" placeholder="Contraseña Actual">
                        <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Contraseña Nueva:</label>
                        <input type="password" name="password" required class="form-control has-feedback-left" id="pinputSuccess1" placeholder="Contraseña Nueva">
                        <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="clearfix"></div>

                      <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                        <label>Repita Contraseña Nueva:</label>
                        <input type="password" name="rpassword" required class="form-control has-feedback-left" id="pinputSuccess2" placeholder="Repita Contraseña Nueva">
                        <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>
                      </div>

                    </form>

                    <button class="btn btn-info btn-xs pull-right" onclick="editPassword()" id="btnUpdatePassword"><i class="glyphicon glyphicon-pencil"></i> Actualizar</button>

                  </div>
                </div>
              </div>
            </div>




          </div>
        </div>
        <!-- /page content -->

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">

  function editPassword(){
    $('#msg-error-p').hide();
    $('#btnUpdatePassword').attr('disabled',true);
    $.ajax({
        url : "<?php echo site_url('admin/passwordUpdate')?>",
        type: "POST",
        data: $('#form_password').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if (data.status)
            {
                $('#msg-error-p').show();
                $('.list-errors-p').html('<div class="animated shake">'+data.aviso+'</div>');
                $('#form_password')[0].reset(); 
            }
            else if (data.validation)
            {
                $('#msg-error-p').show();
                $('.list-errors-p').html('<div class="animated shake">'+data.validation+'</div>');
            }
            $('#btnUpdatePassword').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnUpdatePassword').attr('disabled',false);
        }
    });
  }

</script>