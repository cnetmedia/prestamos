
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total de Usuarios</span>
              <div class="count"><?= $total_usuarios ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-product-hunt"></i> Total de Productos</span>
              <div class="count"><?= $total_productos ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-building-o"></i> Total de Compañias</span>
              <div class="count green"><?= $total_companias ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user-plus"></i> Total de Clientes</span>
              <div class="count"><?= $total_clientes ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-share-square-o"></i> Total de Solicitudes</span>
              <div class="count"><?= $total_solicitudes ?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-money"></i> Cobros por Mes</span>
              <div class="count"><?= $total_cobros ?></div>
            </div>
          </div>
          <!-- /top tiles -->

          <?php $avise = $this->session->flashdata('no_insertar');
              if ($avise) {
          ?>
            <div class="row">
              <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button><?= $avise ?>
              </div>
            </div>
          <?php } ?>                    

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Tráfico <small> de Solicitudes</small></h3>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                  <div style="width: 100%;">
                    <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div>
                  </div>
                </div>


                <div class="clearfix"></div>
              </div>
            </div>

          </div>

        </div>
        <!-- /page content -->

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Flot -->
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.pie.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.time.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.stack.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.resize.js') ?>"></script>


    <!-- Flot -->
    <script>
      $(document).ready(function() {

        
        $.ajax({
            url : "<?php echo site_url('admin/grafica')?>",
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {

                console.log(data);

                var data1 = [
                  [gd(data.fechas[6].year, data.fechas[6].month, data.fechas[6].day), data.fechas[6].cant],
                  [gd(data.fechas[5].year, data.fechas[5].month, data.fechas[5].day), data.fechas[5].cant],
                  [gd(data.fechas[4].year, data.fechas[4].month, data.fechas[4].day), data.fechas[4].cant],
                  [gd(data.fechas[3].year, data.fechas[3].month, data.fechas[3].day), data.fechas[3].cant],
                  [gd(data.fechas[2].year, data.fechas[2].month, data.fechas[2].day), data.fechas[2].cant],
                  [gd(data.fechas[1].year, data.fechas[1].month, data.fechas[1].day), data.fechas[1].cant],
                  [gd(data.fechas[0].year, data.fechas[0].month, data.fechas[0].day), data.fechas[0].cant]
                ];

/*                var data2 = [
                  [gd(data.fechas[6].year, data.fechas[6].month, data.fechas[6].day), 1],
                  [gd(data.fechas[5].year, data.fechas[5].month, data.fechas[5].day), 2],
                  [gd(data.fechas[4].year, data.fechas[4].month, data.fechas[4].day), 3],
                  [gd(data.fechas[3].year, data.fechas[3].month, data.fechas[3].day), 4],
                  [gd(data.fechas[2].year, data.fechas[2].month, data.fechas[2].day), 5],
                  [gd(data.fechas[1].year, data.fechas[1].month, data.fechas[1].day), 6],
                  [gd(data.fechas[0].year, data.fechas[0].month, data.fechas[0].day), 7]
                ];*/

                var p = $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
                  data1/*, data2*/
                ], {
                  series: {
                    lines: {
                      show: false,
                      fill: true
                    },
                    splines: {
                      show: true,
                      tension: 0.1,
                      lineWidth: 2,
                      fill: 0.4
                    },
                    points: {
                      //radius: 0,
                      show: true
                    },
                    shadowSize: 2
                  },
                  grid: {
                    verticalLines: true,
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#fff',
                  },
                  colors: ["rgba(38, 185, 154, 0.38)"/*, "rgba(3, 88, 106, 0.38)"*/],
                  xaxis: {
                    tickColor: "rgba(51, 51, 51, 0.06)",
                    mode: "time",
                    tickSize: [1, "day"],
                    //tickLength: 10,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                  },
                  yaxis: {
                    ticks: 1,
                    tickColor: "rgba(51, 51, 51, 0.06)",

/*                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                    axisLabelPadding: 5,
                    tickSize: 10,*/

                  },
                  tooltip: true,

/*                  tooltipOpts: {
                      id:"chart-tooltip",
                      content: "<p><b>20</b> Outgoing Filings</p>" +
                           "<p>Out of <b>10</b> committed;</p>" +
                           "<br />" +
                           "<p><b>30%</b>% Ratio</p>",
                      shifts: {
                          x:-74,
                          y:-125
                      },
                      lines:{
                        track: true
                      },
                      compat: true,
                  },*/

                });

                function gd(year, month, day) {
                  return new Date(year, month - 1, day).getTime();
                }

                $.each(p.getData()[0].data, function(i, item){
                  var o = p.pointOffset({x: item[0], y: item[1]});
                  $('<span class="label label-info">' + item[1] + '</span>').css( {
                    position: 'absolute',
                    left: o.left - 12,
                    top: o.top - 20,
                    display: 'none'
                  }).appendTo(p.getPlaceholder()).fadeIn('slow');
                });

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
      });
    </script>
    <!-- /Flot -->