
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Desarrollado por <a href="http://www.sevenencorp.com/" target="_blank">Sevenen Corporation</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo site_url('gentelella-master/vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo site_url('gentelella-master/vendors/fastclick/lib/fastclick.js') ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo site_url('gentelella-master/vendors/nprogress/nprogress.js') ?>"></script>
    <!-- Chart.js -->
    <script src="<?php echo site_url('gentelella-master/vendors/Chart.js/dist/Chart.min.js') ?>"></script>
    <!-- gauge.js -->
    <script src="<?php echo site_url('gentelella-master/vendors/gauge.js/dist/gauge.min.js') ?>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo site_url('gentelella-master/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>"></script>
    <!-- iCheck -->
    <script src="<?php echo site_url('gentelella-master/vendors/iCheck/icheck.min.js') ?>"></script>
    <!-- Skycons -->
    <script src="<?php echo site_url('gentelella-master/vendors/skycons/skycons.js') ?>"></script>
    <!-- Flot -->
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.pie.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.time.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.stack.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/Flot/jquery.flot.resize.js') ?>"></script>
    <!-- Flot plugins -->
    <script src="<?php echo site_url('gentelella-master/vendors/flot.orderbars/js/jquery.flot.orderBars.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/flot-spline/js/jquery.flot.spline.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/flot.curvedlines/curvedLines.js') ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo site_url('gentelella-master/vendors/DateJS/build/date.js') ?>"></script>
    <!-- JQVMap -->
    <script src="<?php echo site_url('gentelella-master/vendors/jqvmap/dist/jquery.vmap.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/jqvmap/dist/maps/jquery.vmap.world.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo site_url('gentelella-master/production/js/moment/moment.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/production/js/datepicker/daterangepicker.js') ?>"></script>
    <!-- Datatables -->
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons/js/buttons.flash.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/jszip/dist/jszip.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/pdfmake/build/pdfmake.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/pdfmake/build/vfs_fonts.js') ?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo site_url('gentelella-master/build/js/custom.min.js') ?>"></script>

    <!-- jquery.inputmask -->
    <script src="<?php echo site_url('gentelella-master/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') ?>"></script>

    <!-- jQuery autocomplete -->
    <script src="<?php echo site_url('gentelella-master/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') ?>"></script>

    <!-- Dropzone.js -->
    <script src="<?php echo site_url('gentelella-master/vendors/dropzone/dist/min/dropzone.min.js') ?>"></script>

    <!-- assets -->
    <script src="<?php echo site_url('assets/js/jquery.spinner.js') ?>"></script>

    <!-- Select2 -->
    <script src="<?php echo site_url('gentelella-master/vendors/select2/dist/js/select2.full.min.js') ?>"></script>
    <script src="<?php echo site_url('gentelella-master/vendors/select2/dist/js/i18n/es.js') ?>"></script>
    <!-- jQuery Smart Wizard -->
    <!-- <script src="<?php echo site_url('gentelella-master/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js') ?>"></script> -->

    <script src="<?php echo site_url('assets/js/jquery.form.js') ?>"></script>

    <!-- Datetimepicker -->
    <script src="<?php echo site_url('assets/js/bootstrap-datetimepicker.min.js')?>"></script>


    
<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function() {
    $('.dateform').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      calender_style: "picker_4",
      format: "DD/MM/YYYY",
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
    });
  });
</script>
<!-- /bootstrap-daterangepicker -->

    <!-- Skycons -->
    <script>
      $(document).ready(function() {
        var icons = new Skycons({
            "color": "#73879C"
          }),
          list = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
          ],
          i;

        for (i = list.length; i--;)
          icons.set(list[i], list[i]);

        icons.play();
      });
    </script>
    <!-- /Skycons -->
    
    <!-- bootstrap-daterangepicker -->
<!--     <script>
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script> -->
    <!-- /bootstrap-daterangepicker -->



    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();

        $('#datatable-keytable').DataTable({
          keys: true
        });

        //$('#datatable-responsive').DataTable();

        $('#datatable-responsive').DataTable( {
            "order": [[ 0, "asc" ]],
            "destroy": true,
            "bRetrieve": true,
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Lo siento, no hay registros",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtro de _MAX_ registros en total)",
                "sSearch": "Buscar",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                }
            }
        } );

      });
    </script>
    <!-- /Datatables -->

  </body>
</html>
