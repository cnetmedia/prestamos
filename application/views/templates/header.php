<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?=$title?></title>

    <!-- Bootstrap -->
    <link href="<?php echo site_url('gentelella-master/vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo site_url('gentelella-master/vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo site_url('gentelella-master/vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo site_url('gentelella-master/vendors/iCheck/skins/flat/green.css') ?>" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo site_url('gentelella-master/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') ?>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo site_url('gentelella-master/vendors/jqvmap/dist/jqvmap.min.css') ?>" rel="stylesheet"/>
    <!-- Datatables -->
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo site_url('gentelella-master/vendors/animate.css/animate.min.css') ?>" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo site_url('gentelella-master/build/css/custom.min.css') ?>" rel="stylesheet">

    <!-- Dropzone.js -->
    <link href="<?php echo site_url('gentelella-master/vendors/dropzone/dist/min/dropzone.min.css') ?>" rel="stylesheet">

    <!-- assets -->
    <link href="<?php echo site_url('assets/css/bootstrap-spinner.css') ?>" rel="stylesheet">

    <!-- Select2 -->
    <link href="<?php echo site_url('gentelella-master/vendors/select2/dist/css/select2.min.css') ?>" rel="stylesheet">

    <!-- Datetimepicker -->
    <link href="<?php echo site_url('assets/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">

    <style type="text/css">
      .profile_edit {
        margin-left: 25px;
        margin-bottom: 10px; }
      .profile_edit span {
        font-size: 13px;
        line-height: 30px;
        color: #BAB8B8; }
      .profile_edit h2 {
        font-size: 14px;
        color: #ECF0F1;
        margin: 0;
        font-weight: 300; }
    </style>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; ">
              <a href="<?php echo site_url('admin') ?>" class="site_title"><span><img src="<?php echo site_url('assets/images/logo-footer.png') ?>" ></span><img src="<?php echo site_url('assets/images/logo_img.png') ?>" style="margin-left: -5px;"></a>
            </div>

            <div class="clearfix"></div>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">

                <div class="profile profile_edit">
                  <span>Bienvenid@,</span>
                  <h2><?=$this->session->userdata('nombres') . ' '. $this->session->userdata('apellidos')?></h2>
                </div>

                <h3><?=$this->session->userdata('descripcion')?></h3>

                <ul class="nav side-menu">

                  <li>
                    <a href="<?= site_url('admin') ?>"><i class="fa fa-home"></i> Inicio </a>
                  </li>

                <?php if ($modulos_roles) { ?>
                    <?php $menus = ''; ?>
                    <?php foreach ($modulos_roles as $row) { ?>
                      <?php
                          $modulos = $this->admin_model->consultar($row->modulos_id);
                        foreach ($modulos as $mod) {

                          if ($mod->id != 9) {  //9 Configuración pendiente....                    

                      ?>
                          

                          <?php 
                            switch ($mod->id) {
                              // Los numero de casos son los id de cada modulo.
                              case 1:
                                $menus .= '<li>
                                  <a href="'.site_url().'usuarios"><i class="'.$mod->icono.'"></i> '.$mod->modulo.'</a>
                                  </li>
                                ';
                                break;
                              case 2:
                                $menus .= '<li>
                                  <a href="'.site_url().'productos"><i class="'.$mod->icono.'"></i> '.$mod->modulo.'</a>
                                  </li>
                                ';
                                break;
                              case 3:
                                $menus .= '<li>
                                  <a href="'.site_url().'companias"><i class="'.$mod->icono.'"></i> '.$mod->modulo.'</a>
                                  </li>
                                ';
                                break;
                              case 4:
                                $menus .= '<li>
                                  <a href="'.site_url().'cotizacion"><i class="'.$mod->icono.'"></i> '.$mod->modulo.'</a>
                                  </li>
                                ';                             
                                break;
                              case 5:
                                $menus .= '<li>
                                  <a><i class="'.$mod->icono.'"></i> '.$mod->modulo.' <span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'clientes">Gestionar Clientes</a></li>
                                    <li><a href="'.site_url().'clientes/prospectos">Gestionar Prospectos</a></li>
                                  </ul>
                                  </li>';   
                                break;

                              case 6:
                                $menus .= '<li>
                                  <a><i class="'.$mod->icono.'"></i> '.$mod->modulo.' <span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'solicitudes">Gestionar Solicitudes</a></li>
                                    <li><a>Mis Solicitudes<span class="fa fa-chevron-down"></span></a>
                                      <ul class="nav child_menu">
                                        <li><a href="'.site_url().'mis_solicitudes/prospectos">Prospectos</a></li>
                                        <li><a href="'.site_url().'mis_solicitudes/verificacion">Por Verificar</a></li>
                                        <li><a href="'.site_url().'mis_solicitudes/aprobacion">Por Aprobar</a></li>
                                        <li><a href="'.site_url().'mis_solicitudes/firma">Por Firmar</a></li>
                                        <li><a href="'.site_url().'mis_solicitudes/entrega">Por Entregar</a></li>
                                      </ul>
                                    </li>
                                    <li><a href="'.site_url().'solicitudes/seguro_social">Seguro Social</a></li>
                                  </ul>
                                  </li>
                                ';  
                                break;
                              case 7:
                                if ($this->session->userdata('id_rol') == 1 || $this->session->userdata('id_rol') == 2) {
                                  $menus .= '<li>
                                    <a><i class="'.$mod->icono.'"></i> '.$mod->modulo.' <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                      <li><a href="'.site_url().'cobros">Gestionar Cobros</a></li>
                                      <li><a href="'.site_url().'cobros/sin_garantias">Cobros sin garantías</a></li>
                                      <li><a href="'.site_url().'cobros/desempleados">Desempleados</a></li>
                                      <li><a href="'.site_url().'cobros/cobros_completos">Cobros Completados</a></li>
                                    </ul></li>
                                  ';
                                } else if ($this->session->userdata('id_rol') == 4) {
                                  $menus .= '<li>
                                    <a><i class="'.$mod->icono.'"></i> '.$mod->modulo.' <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                      <li><a href="'.site_url().'cobros">Gestionar Cobros</a></li>
                                      <li><a href="'.site_url().'cobros/sin_garantias">Cobros sin garantías</a></li>
                                      <li><a href="'.site_url().'cobros/desempleados">Desempleados</a></li>
                                    </ul></li>
                                  ';
                                } else {
                                 $menus .= '<li>
                                    <a><i class="'.$mod->icono.'"></i> '.$mod->modulo.' <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                      <li><a href="'.site_url().'cobros">Gestionar Cobros</a></li>
                                      <li><a href="'.site_url().'cobros/sin_garantias">Cobros sin garantías</a></li>
                                    </ul></li>
                                  ';
                                }
                                break;

                              case 8:
                                $menus .= '<li>
                                  <a><i class="'.$mod->icono.'"></i> '.$mod->modulo.' <span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'reportes/desembolsos">Desembolsos</a></li>
                                    <li><a href="'.site_url().'reportes/movimiento_usuarios">Movimiento de Usuarios</a></li>
                                    <li><a href="'.site_url().'reportes/cantidad_solicitudes">Cantidad de Solicitudes</a></li>
                                    <li><a href="'.site_url().'reportes/cobros_proyectados">Cobros Proyectados</a></li>
                                    <li><a href="'.site_url().'reportes/cobros_recolectados">Cobros Recolectados</a></li>
                                    <li><a href="'.site_url().'reportes/apc">APC clientes</a></li>
                                    <li><a href="'.site_url().'reportes/refapc">Ref APC clientes</a></li>
                                    <li><a href="'.site_url().'reportes/clientes">Clientes</a></li>
                                    <li><a href="'.site_url().'cobros/total_deudas">Total Deudas</a></li>
                                    <li><a href="'.site_url().'reportes/detalle_cargos">Desglose Cargos e ITBMS</a></li>
                                    <li><a href="'.site_url().'reportes/cobros_recolectados_itbms">Cobros Recolectados + Cargos e ITBMS</a></li>
                                  </ul></li>
                                ';
                                break;

                              case 10:
                                $menus .= '<li>
                                  <a href="'.site_url().'bitacora"><i class="'.$mod->icono.'"></i> '.$mod->modulo.'</a></li>
                                ';
                                break;
                              default:
                                break;
                            }
                          ?>

                        
                      <?php }  } //End 9 Configuración pendiente.... ?>
                    <?php } ?> 
                                 
                <?php } else { ?>
                  <li><?=$this->session->flashdata('no_modulos')?></li>    
                <?php }?>
                <?php echo $menus; ?>    
                </ul>

              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <br>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo site_url('assets/images/logo_img.png') ?>" alt=""><?=$this->session->userdata('correo')?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?= site_url('admin/profile') ?>"><i class="fa fa-user pull-right"></i> Perfil</a></li>
                    <?php if ($this->session->userdata('id_rol') == 1) { ?> 
                      <li><a href="<?= site_url('admin/settings') ?>"><i class="fa fa-cog pull-right"></i> Ajustes</a></li>
                    <?php } ?>
                    <li><a href="<?=site_url()?>login/logout_ci"><i class="fa fa-sign-out pull-right"></i> Cerrar sesión</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->