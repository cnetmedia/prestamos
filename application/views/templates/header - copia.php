<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?=$title?></title>

    <!-- Bootstrap -->
    <link href="<?php echo site_url('gentelella-master/vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo site_url('gentelella-master/vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo site_url('gentelella-master/vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo site_url('gentelella-master/vendors/iCheck/skins/flat/green.css') ?>" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo site_url('gentelella-master/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') ?>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo site_url('gentelella-master/vendors/jqvmap/dist/jqvmap.min.css') ?>" rel="stylesheet"/>
    <!-- Datatables -->
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo site_url('gentelella-master/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo site_url('gentelella-master/vendors/animate.css/animate.min.css') ?>" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="<?php echo site_url('gentelella-master/build/css/custom.min.css') ?>" rel="stylesheet">

    <!-- Dropzone.js -->
    <link href="<?php echo site_url('gentelella-master/vendors/dropzone/dist/min/dropzone.min.css') ?>" rel="stylesheet">

    <!-- assets -->
    <link href="<?php echo site_url('assets/css/bootstrap-spinner.css') ?>" rel="stylesheet">

    <!-- Select2 -->
    <link href="<?php echo site_url('gentelella-master/vendors/select2/dist/css/select2.min.css') ?>" rel="stylesheet">

    <style type="text/css">
      .profile_edit {
        margin-left: 25px;
        margin-bottom: 10px; }
      .profile_edit span {
        font-size: 13px;
        line-height: 30px;
        color: #BAB8B8; }
      .profile_edit h2 {
        font-size: 14px;
        color: #ECF0F1;
        margin: 0;
        font-weight: 300; }
    </style>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; ">
              <a href="<?php echo site_url('admin') ?>" class="site_title"><span><img src="<?php echo site_url('assets/images/logo-footer.png') ?>" ></span><img src="<?php echo site_url('assets/images/logo_img.png') ?>" style="margin-left: -5px;"></a>
            </div>

            <div class="clearfix"></div>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">

                <div class="profile profile_edit">
                  <span>Bienvenid@,</span>
                  <h2><?=$this->session->userdata('nombres') . ' '. $this->session->userdata('apellidos')?></h2>
                </div>

                <h3><?=$this->session->userdata('descripcion')?></h3>

                <ul class="nav side-menu">

                  <li>
                    <a href="<?= site_url('admin') ?>"><i class="fa fa-home"></i> Inicio </a>
                  </li>

                <?php if ($modulos_roles) { ?>
                    <?php foreach ($modulos_roles as $row) { ?>
                      <?php
                          $modulos = $this->admin_model->consultar($row->modulos_id);
                        foreach ($modulos as $mod) {

                          if ($mod->id != 9) {  //9 Configuración pendiente....                    

                      ?>
                        <li>
                          <a><i class="<?=$mod->icono?>"></i> <?=$mod->modulo?> <span class="fa fa-chevron-down"></span></a>

                          <?php 
                            switch ($mod->id) {
                              // Los numero de casos son los id de cada modulo.
                              case 1:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'usuarios">Gestionar Usuarios</a></li>
                                  </ul>
                                ';
                                break;
                              case 2:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'productos">Gestionar Productos</a></li>
                                  </ul>
                                ';
                                break;
                              case 3:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'companias">Gestionar Compañias</a></li>
                                  </ul>
                                ';
                                break;
                              case 4:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'cotizacion">Gestionar Cotización</a></li>
                                  </ul>
                                ';                             
                                break;
                              case 5:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'clientes">Gestionar Clientes</a></li>
                                  </ul>
                                ';   
                                break;
                              case 6:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'solicitudes">Gestionar Solicitudes</a></li>

                                    <li><a>Mis Solicitudes<span class="fa fa-chevron-down"></span></a>
                                      <ul class="nav child_menu">
                                        <li><a href="'.site_url().'mis_solicitudes/prospectos">Prospectos</a></li>
                                        <li><a href="'.site_url().'mis_solicitudes/verificacion">Por Verificar</a></li>
                                        <li><a href="'.site_url().'mis_solicitudes/aprobacion">Por Aprobar</a></li>
                                        <li><a href="'.site_url().'mis_solicitudes/entrega">Por Entregar</a></li>
                                      </ul>
                                    </li>
                                  </ul>
                                ';  
                                break;
                              case 7:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'cobros">Gestionar Cobros</a></li>
                                  </ul>
                                ';
                                break;
                              case 8:
                                break;
/*                              case 9:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="#">Mensajería</a></li>
                                    <li><a href="#">Reporte 2</a></li>
                                    <li><a href="#">Reporte 3</a></li>
                                    <li><a href="#">Reporte 4</a></li>
                                  </ul>
                                ';
                                break;*/
                              case 10:
                                echo '
                                  <ul class="nav child_menu">
                                    <li><a href="'.site_url().'bitacora">Ver Bitácora</a></li>
                                  </ul>
                                ';
                                break;
                              default:
                                break;
                            }
                          ?>

                        </li>
                      <?php }  } //End 9 Configuración pendiente.... ?>
                    <?php } ?>                  
                <?php } else { ?>
                  <li><?=$this->session->flashdata('no_modulos')?></li>    
                <?php }?>

                </ul>

              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <br>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo site_url('assets/images/logo_img.png') ?>" alt=""><?=$this->session->userdata('correo')?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="<?=site_url()?>login/logout_ci"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->