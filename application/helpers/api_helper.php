<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
* API helper with fun stuff
* Author: Jonathan Freites
*/

/**
 * Generic API response with custom error code
 * 
 * @param array data to show
 * @param integer status code for the response optional
 */
if ( ! function_exists('responder')) {
    function responder($data, $statusCode = 200)
    {
        $CI =& get_instance();

        $CI->output->set_content_type('application/json', 'utf-8')
                    ->set_status_header($statusCode)
                    ->set_output(json_encode($data))
                    ->_display();
        exit;
    }
}

/**
 * Transform json data to array-like post data
 */
if ( ! function_exists('json_to_post')) {
    function json_to_post()
    {
        return json_decode(trim(file_get_contents("php://input")), true);
    }
}
?>