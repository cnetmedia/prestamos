<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
    'create_client' => array(
        array(
            'field' => 'name',
            'label' => 'name',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'last_name',
            'label' => 'last name',
            'rules' => 'required|trim|xss_clean',
            /*'errors' => array(
                'required' => 'You must provide a %s.',
            ),*/
        ),
        array(
            'field' => 'gender',
            'label' => 'gender',
            'rules' => 'required|trim|xss_clean|in_list[M,F,m,f]'
        ),
        array(
            'field' => 'email',
            'label' => 'email',
            'rules' => 'valid_email'
        ),
        array(
            'field' => 'documentId',
            'label' => 'document id',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'phone',
            'label' => 'phone',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'dependents_quantity',
            'label' => 'dependents quantity',
            'rules' => 'required|integer|trim'
        ),
        array(
            'field' => 'civil_status',
            'label' => 'civil status',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'address',
            'label' => 'address',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'address',
            'label' => 'address',
            'rules' => 'required|trim|xss_clean'
        ),
        /*array(
            'field' => 'name_organization',
            'label' => 'name_organization',
            'rules' => 'required|trim|xss_clean'
        ),*/
        array(
            'field' => 'organization_id',
            'label' => 'organization_id',
            'rules' => 'required|integer|xss_clean'
        ),
        array(
            'field' => 'dependents_quantity',
            'label' => 'dependents_quantity',
            'rules' => 'required|integer|xss_clean'
        ),
        array(
            'field' => 'user_id',
            'label' => 'user_id',
            'rules' => 'required|integer|xss_clean'
        ),
        array(
            'field' => 'requested_amount',
            'label' => 'requested_amount',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'customer_source',
            'label' => 'customer_source',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'bank',
            'label' => 'bank',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'mortgage',
            'label' => 'mortgage',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'discount',
            'label' => 'discount',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'discount_quantity',
            'label' => 'discount quantity',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'discount_month',
            'label' => 'discount month',
            'rules' => 'required|trim'
        ),
        array(
            'field' => 'discount_year',
            'label' => 'discount year',
            'rules' => 'required|trim|exact_length[4]',
            'errors' => array(
                'exact_length' => 'The %s must be 4 chars, ex: 2018.',
            )
        ),
        array(
            'field' => 'salary',
            'label' => 'salary',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'work_position',
            'label' => 'work position',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'working_time',
            'label' => 'working time',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'direct_boss_name',
            'label' => 'direct boss name',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'area',
            'label' => 'area',
            'rules' => 'required|trim|xss_clean'
        ),
        array(
            'field' => 'ministry',
            'label' => 'ministry',
            'rules' => 'required|trim|xss_clean'
        ),
    ),
);