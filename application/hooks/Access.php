<?php
	if (!defined( 'BASEPATH')) exit('No direct script access allowed'); 
	class Access
	{
		private $ci;

		public function __construct()
		{
			$this->ci =& get_instance();
			!$this->ci->load->library('session') ? $this->ci->load->library('session') : false;
			!$this->ci->load->helper('url') ? $this->ci->load->helper('url') : false;
			$this->ci->load->model(array('login_model','usuarios_model','admin_model'));
		} 

		public function check_permitions()
		{				
			if($this->ci->router->class != 'admin' && $this->ci->router->class != 'login'){	
				$have_access = FALSE;
				$check_user = $this->ci->usuarios_model->getUsuarios($this->ci->session->userdata('id_user'),TRUE);
				if(empty($check_user->correo)){
					redirect(base_url('login'));
				}

				$check_rol = $this->ci->admin_model->consultarModuloByRol($check_user->roles_id);
				foreach ($check_rol as $key => $rol) {
					/*$transliterator = Transliterator::createFromRules(':: Any-Latin; :: Latin-ASCII; :: NFD; :: [:Nonspacing Mark:] Remove; :: Lower(); :: NFC;', Transliterator::FORWARD);
					$normalized = $transliterator->transliterate(mb_strtolower($rol->modulo));*/

					$normalized = $this->sanear_string( mb_strtolower($rol->modulo) );

					if($normalized == $this->ci->router->class){
						$have_access = TRUE;
					}
				}

				if(!$have_access){
					redirect(base_url('admin'));	
				}	
			}
		}

		protected function sanear_string($string)
		{
		 
		    $string = trim($string);
		 
		    $string = str_replace(
		        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
		        $string
		    );
		 
		    $string = str_replace(
		        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
		        $string
		    );
		 
		    $string = str_replace(
		        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
		        $string
		    );
		 
		    $string = str_replace(
		        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
		        $string
		    );
		 
		    $string = str_replace(
		        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
		        $string
		    );
		 
		    $string = str_replace(
		        array('ñ', 'Ñ', 'ç', 'Ç'),
		        array('n', 'N', 'c', 'C',),
		        $string
		    );
		 
		    //Esta parte se encarga de eliminar cualquier caracter extraño
		    /*$string = str_replace(
		        array("\", "¨", "º", "-", "~",
		             "#", "@", "|", "!", """,
		             "·", "$", "%", "&", "/",
		             "(", ")", "?", "'", "¡",
		             "¿", "[", "^", "<code>", "]",
		             "+", "}", "{", "¨", "´",
		             ">", "< ", ";", ",", ":",
		             ".", " "),
		        '',
		        $string
		    );*/
		 		 
		    return $string;
		}
	}
/*
/end hooks/Access.php
*/