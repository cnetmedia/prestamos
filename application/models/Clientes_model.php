<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Clientes_model extends CI_Model {

	var $table = 'clientes';
	var $column_order = array('fecha_creacion','nombre','cedula','salario','cantidad_solicitada','celular','companias_id'); //set column field database for datatable orderable
	var $column_search = array('fecha_creacion','nombre','apellido','cedula','salario','cantidad_solicitada','celular','companias_id'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	var $order = array('id' => 'desc'); // default order 
	
	public function __construct() {
		parent::__construct();
	}

	function get_banco() {
		$this->db->select('banco');
		$query = $this->db->get('clientes');
		$array = array();
		foreach ($query->result() as $row) {
			if ($row->banco != null || $row->banco != "") {
				$array[] = $row->banco;
			}			
		}
		return array_unique($array);
	}

	function get_corregimiento() {
		$this->db->select('corregimiento');
		$query = $this->db->get('clientes');
		$array = array();
		foreach ($query->result() as $row) {
			if ($row->corregimiento != null || $row->corregimiento != "") {
				$array[] = $row->corregimiento;
			}			
		}
		return array_unique($array);
	}

	function get_distrito() {
		$this->db->select('distrito');
		$query = $this->db->get('clientes');
		$array = array();
		foreach ($query->result() as $row) {
			if ($row->distrito != null || $row->distrito != "") {
				$array[] = $row->distrito;
			}			
		}
		return array_unique($array);
	}

	function get_provincia() {
		$this->db->select('provincia');
		$query = $this->db->get('clientes');
		$array = array();
		foreach ($query->result() as $row) {
			if ($row->provincia != null || $row->provincia != "") {
				$array[] = $row->provincia;
			}			
		}
		return array_unique($array);
	}

	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_all()
	{
		$this->db->from($this->table);
		$this->db->order_by('fecha_creacion', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_cedula($cedula)
	{
		$this->db->from($this->table);
		$this->db->where('cedula',$cedula);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_rol_by_id($id)
	{
		$this->db->from('roles');
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	function getCompanies() {
		$data = array();
		$this->db->select('id,nombre');
		$query = $this->db->get('companias');
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row){
				$data[] = $row;
			}
		}
		$query->free_result();
		return $data;
	}

	public function get_companies_by_id($id)
	{
		$this->db->from('companias');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

/*	public function saveArchivos($data)
	{
		$this->db->insert('imagenes', $data);
		return $this->db->insert_id();
	}*/

	public function save_archivos($data)
	{
		$this->db->insert('archivos', $data);
		return $this->db->insert_id();
	}


	public function get_archivos()
	{
		$this->db->from('archivos');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_archivos_by_cliente_id($id)
	{
		$this->db->from('archivos');
		$this->db->where('clientes_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_archivos_by_tipos_archivos_id($tipo_archivo_id,$cliente_id)
	{
		$this->db->from('archivos');
		$this->db->where('tipos_archivos_id',$tipo_archivo_id) and $this->db->where('clientes_id',$cliente_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_cliente_by_id($id)
	{
		$this->db->select('nombre,apellido,usuarios_id');
		$this->db->from('clientes');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_archivos_by_id($id)
	{
		$this->db->from('archivos');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function delete_archivos_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('archivos');
	}

	function get_tipos_archivos()
	{
		$data = array();
		$query = $this->db->get('tipos_archivos');
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row){
				$data[] = $row;
			}
		}
		$query->free_result();
		return $data;
	}

	public function get_tipos_archivos_by_id($id)
	{
		$this->db->from('tipos_archivos');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}



/*	public function get_clientes_prueba()
	{
		$this->db->from('clientes_prueba');
		$query = $this->db->get();
		return $query->result();
	}*/

/*	public function get_solicitudes_prueba()
	{
		$this->db->from('solicitudes_prueba');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_aprobacion_prueba()
	{
		$this->db->from('aprobacion_prueba');
		$query = $this->db->get();
		return $query->result();
	}*/



}