<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Estados_model extends CI_Model {

	var $table = 'estados';
	
	public function __construct() {
		parent::__construct();
	}

	public function get()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

}