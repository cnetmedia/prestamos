<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Cobros_model extends CI_Model {

	var $table = 'cobros';
	var $column_order = array('fecha_correspondiente','fecha_pago','forma_pago','fecha_registro','nota','pago_adelantado','monto','archivo','aprobacion_id','usuarios_id');
	var $column_search = array('fecha_correspondiente','fecha_pago','forma_pago','fecha_registro','nota','pago_adelantado','monto','archivo','aprobacion_id','usuarios_id'); 
	var $order = array('id' => 'desc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query()
	{		
		$this->db->from($this->table);
		$i = 0;	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function aprobacion_get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('aprobacion_id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_aprobacion_id($aprobacion_id)
	{
		$this->db->from($this->table);
		$this->db->where('aprobacion_id',$aprobacion_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function get_cobros_adelantados_by_aprobacion_id($aprobacion_id)
	{
		$array = array();
		$this->db->from($this->table);
		$this->db->where('aprobacion_id',$aprobacion_id) and $this->db->where('pago_adelantado','Si');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function get_estado_cobros_by_id($id)
	{
		$this->db->from('estados_cobro');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_estado_cobros()
	{
		$data = array();
		$query = $this->db->get('estados_cobro');
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row){
				$data[] = $row;
			}
		}
		//$query->free_result();
		return $data;
	}

	public function cobros_by_aprobacion_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('aprobacion_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	public function cobros_by_aprobacion_id_to_excel($id)
	{
		$fields = [
			(object)['name' => 'Deuda'],
			(object)['name' => 'Fecha tope'],
			(object)['name' => 'Abono'],
			(object)['name' => 'Principal'],
			(object)['name' => 'Interés pendiente'],
			(object)['name' => 'Interés'],
			(object)['name' => 'Tasa interés'],
			(object)['name' => 'Cargo pendiente'],
			(object)['name' => 'Cargo administrativo'],
			(object)['name' => 'Comisión'],
			(object)['name' => 'Gastos notariales'],
			(object)['name' => 'Cargo mora'],
			(object)['name' => 'Total'],
			(object)['name' => 'Fecha de pago'],
			(object)['name' => 'Forma de pago'],
			(object)['name' => 'Monto'],
			(object)['name' => 'Nota'],
			(object)['name' => 'Registrado por'],
		];
		$fields = (object)$fields;
		$query = $this->cobros_by_aprobacion_id_query($id);
		return array("fields" => $fields, "query" => $query);
	}

	public function cobros_by_aprobacion_id_query($id)
	{
		$this->db->select("cob.deuda,cob.fecha_correspondiente,	cob.abono,	cob.principal,	cob.interes_restante, cob.interes, cob.tasa_interes,cob.cargo_restante,	cob.cargo_administrativo, (apr.cantidad_aprobada * pro.comision) / 100 as comision, (apr.cantidad_aprobada * pro.gastos_notariales) / 100 as gasto_notarial,	cob.interes_mora,	cob.total,	cob.fecha_pago,	cob.forma_pago,	cob.monto,	cob.nota, CONCAT_WS(' ',us.nombres,us.apellidos) as registrado_por ");	
		$this->db->from($this->table . ' cob');
		$this->db->join('aprobacion apr','apr.id = cob.aprobacion_id');
		$this->db->join('productos pro','pro.id = apr.productos_id');
		$this->db->join('usuarios us','us.id = cob.usuarios_id');
		$this->db->where('cob.aprobacion_id',$id);
		$query = $this->db->get();
		return $query;
	}

	public function cant_cobros_by_aprobacion_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('aprobacion_id',$id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function suma_cobros_by_aprobacion_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('aprobacion_id',$id);
		$query = $this->db->get();
		$array = array();
		foreach ($query->result() as $row) {
			$array[] = $row->monto;
		}
		return array_sum($array);
	}

	public function get_by_fecha_pago_and_aprobacion_id($fecha_pago,$aprobacion_id)
	{
		$this->db->from($this->table);
		$this->db->where('fecha_pago',$fecha_pago) and $this->db->where('aprobacion_id',$aprobacion_id);
		$query = $this->db->get();
		return $query->result();
	}


	public function get_cobros_by_fechas($fecha_i,$fecha_f){
		$this->db->select('fecha_pago,aprobacion_id,monto');
		$this->db->from($this->table);
		$this->db->where('fecha_pago >=',$fecha_i);
		$this->db->where('fecha_pago <=',$fecha_f);
		$query = $this->db->get();
		return $query->result();
	}


/*	public function preparar_database()
	{
		$this->db->from('cobros_primerafecha');
		$query = $this->db->get();
		return $query->result();
	}*/


}