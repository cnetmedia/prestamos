<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Clientes_view_model extends CI_Model {

	var $table = 'clientes_view cliv';
	var $column_order = array('cliv.fecha_creacion','cliv.estado','cliv.cliente','cliv.cedula','cliv.salario','cliv.cantidad_solicitada','cliv.telefono','cliv.celular','cliv.cliv.elegido');
	var $column_search = array('cliv.fecha_creacion','cliv.estado','cliv.cliente','cliv.cedula','cliv.salario','cliv.cantidad_solicitada','cliv.telefono','cliv.celular','cliv.elegido'); 
	var $order = array('cliv.fecha_creacion' => 'desc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables($filtro = NULL, $origen = NULL)
	{
		$this->_get_datatables_query($filtro,$origen);			
		
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query($filtro = NULL, $origen = NULL)
	{		
		$this->db->from($this->table);
		$this->db->join('clientes cli','cli.id = cliv.id');

		if ($filtro == 'sinsolicitudes') {			
			$this->db->where("(cliv.estado IS NULL)");
		}elseif ($filtro == 'potenciales') {			
			$this->db->where("cli.estados_cobro_id",13);
			$this->db->where("cli.id not in (SELECT sol.id from solicitudes sol where cli.id = sol.clientes_id )");
		}elseif ($filtro == 'prospectos') {			
			$this->db->where("cliv.estado",'Prospecto');
		}elseif ($filtro == 'prospectosapp') {			
			$this->db->where("cliv.estado",'Prospecto');
			$this->db->where('cli.fuente_cliente',$origen);
		}elseif ($filtro == 'sinorigen') {			
			$this->db->where("cliv.estado",'Prospecto');
			$this->db->where("(cli.fuente_cliente IS NULL OR cli.fuente_cliente = '')");
		}else{
			$this->db->where("cliv.estado <> 'Prospecto'");			
		}

		$i = 0;	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all($filtro = NULL, $origen = NULL)
	{
		$this->db->from($this->table);
		$this->db->join('clientes cli','cli.id = cliv.id');

		if ($filtro == 'sinsolicitudes') {			
			$this->db->where("(cliv.estado IS NULL)");
		}elseif ($filtro == 'potenciales') {			
			$this->db->where("cli.estados_cobro_id",13);
			$this->db->where("cli.id not in (SELECT sol.id from solicitudes sol where cli.id = sol.clientes_id )");
		}elseif ($filtro == 'prospectos') {			
			$this->db->where("cliv.estado",'Prospecto');
		}elseif ($filtro == 'prospectosapp') {			
			$this->db->where("cliv.estado",'Prospecto');
			$this->db->where('cli.fuente_cliente',$origen);
		}elseif ($filtro == 'sinorigen') {			
			$this->db->where("cliv.estado",'Prospecto');
			$this->db->where("(cli.fuente_cliente IS NULL OR cli.fuente_cliente = '')");
		}else{
			$this->db->where("cliv.estado <> 'Prospecto'");			
		}
		return $this->db->count_all_results();
	}

	function count_filtered($filtro = NULL,$origen = NULL)
	{
		$this->_get_datatables_query($filtro,$origen);
		/*if ($filtro == 'sinsolicitudes') {			
			$this->db->where('estado',NULL);
		}*/
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function get_origenes()
	{
		$this->db->select('cli.fuente_cliente as item');
		$this->db->from($this->table);
		$this->db->join('clientes cli','cli.id = cliv.id');
		$this->db->where("cliv.estado",'Prospecto');
		$this->db->where("fuente_cliente IS NOT NULL AND fuente_cliente <> ''");
		$this->db->group_by("cli.fuente_cliente");
		$this->db->order_by("cli.fuente_cliente");
		$query = $this->db->get();
		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nombre($nombre)
	{
		$this->db->from($this->table);
		$this->db->where('nombre',$nombre);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_all()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}


}