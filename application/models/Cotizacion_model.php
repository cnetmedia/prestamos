<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Cotizacion_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	function getProductos()
	{
		$data = array();
		$query = $this->db->get('productos');
		if ($query->num_rows() > 0) {
			foreach ($query->result_array() as $row){
				$data[] = $row;
			}
		}
		$query->free_result();
		return $data;
	}

	public function get_products_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('productos');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if($query->num_rows() > 0 )		{
			return $query->result();
		}
	}

	public function get_plazo_products_by_id($id)
	{
		$this->db->select('plazo_minimo,plazo_maximo');
		$this->db->from('productos');
		$this->db->where('id',$id);
		$query = $this->db->get();
		if($query->num_rows() > 0 )		{
			return $query->result();
		}
	}


	public function save($data)
	{
		$this->db->insert('cotizacion', $data);
		return $this->db->insert_id();
	}


}