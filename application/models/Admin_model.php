<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Admin_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function consultar($id = false)
	{
		if($id === false)
		{
			$this->db->select('*');
			$this->db->from('modulos');
		}else{
			$this->db->select('*');
			$this->db->from('modulos');
			$this->db->where('id',$id);
		}
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			return $query->result();
		}
		else
		{
			$this->session->set_flashdata('no_modulos','Aun no hay registros.');
		}
	}

	public function consultarModuloByRol($roles_id)
	{
		$this->db->select('*');
		$this->db->from('modulos_roles');
		$this->db->join('modulos mo','mo.id = modulos_roles.modulos_id');
		$this->db->where('roles_id',$roles_id);
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			return $query->result();
		}
		else
		{
			$this->session->set_flashdata('no_modulo_by_rol','Aun no hay registros.');
		}
	}

	public function consultarSubModuloByModulo($modulos_id)
	{
		$this->db->select('*');
		$this->db->from('sub_modulos');
		$this->db->where('modulos_id',$modulos_id);
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			return $query->result();
		}
		else
		{
			return [];
		}
	}

	public function verificarSubModuloRol($roles_id, $sub_modulos_id)
	{
		$this->db->where('roles_id',$roles_id) and $this->db->where('sub_modulos_id',$sub_modulos_id);
		$query = $this->db->get('sub_modulos_roles');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}
	}

	public function totalUsuarios()
	{
		$this->db->select('*');
		$this->db->from('usuarios');
		$query = $this->db->get();
		if($query->num_rows() > 0 )
		{
			return count($query->result());
		}
		else
		{
			return 0;
		}
	}

	public function verify_password($id,$password)
	{
		$this->db->where('id',$id);
		$this->db->where('password',$password);
		$query = $this->db->get('usuarios');
		return $query->row();
	}


	public function consultarRoles()
	{
		$this->db->select('*');
		$this->db->from('roles');
		$query = $this->db->get();
		return $query->result();
	}

	public function consultarModulos()
	{
		$this->db->select('*');
		$this->db->from('modulos');
		$query = $this->db->get();
		return $query->result();
	}

	public function ConsultModuloRol($modulos_id,$roles_id)
	{
		$this->db->select('*');
		$this->db->from('modulos_roles');
		$this->db->where('modulos_id',$modulos_id);
		$this->db->where('roles_id',$roles_id);
		$query = $this->db->get();
		return $query->row();
	}

	public function save_modulorol($data)
	{
		$this->db->insert('modulos_roles', $data);
		return $this->db->insert_id();
	}

	public function delete_modulorol($roles_id,$modulos_id)
	{
		$this->db->where('roles_id', $roles_id) and $this->db->where('modulos_id', $modulos_id);
		$this->db->delete('modulos_roles');
	}

//Permisos

	public function get_insertar($id){
		$this->db->from('roles');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row()->insertar;
	}

	public function get_modificar($id){
		$this->db->from('roles');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row()->modificar;
	}

	public function get_eliminar($id){
		$this->db->from('roles');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row()->eliminar;
	}

	public function get_aprobar($id){
		$this->db->from('roles');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row()->aprobar;
	}

//End Permisos

}