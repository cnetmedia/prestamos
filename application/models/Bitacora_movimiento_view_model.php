<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Bitacora_movimiento_view_model extends CI_Model {

	var $table = 'bitacora_movimiento_view';
	var $column_order = array('fecha','usuario','fecha_solicitud','accion','agente','fecha_verificacion','verificador','aprobador','elegido','estado');
	var $column_search = array('fecha','usuario','fecha_solicitud','accion','agente','fecha_verificacion','verificador','aprobador','elegido','estado'); 
	var $order = array('fecha' => 'desc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables($arg = NULL,$fecha_i = NULL,$fecha_f = NULL,$filtros = NULL)
	{
		$this->_get_datatables_query($arg,$fecha_i,$fecha_f,$filtros);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query($arg,$fecha_i,$fecha_f,$filtros)
	{		
		$this->db->from($this->table);
		if ($arg != NULL && $fecha_i != NULL && $fecha_f != NULL) {
			if ($filtros['rol'] > 0 && $filtros['usuario'] != NULL && $filtros['estado'] != NULL) {
				if ($filtros['rol'] == 1) {
					$rol = 'agente';
				} elseif ($filtros['rol'] == 2) {
					$rol = 'verificador';
				} elseif ($filtros['rol'] == 3) {
					$rol = 'aprobador';
				} elseif ($filtros['rol'] == 4) {
					$rol = 'usuario';
				}
				$this->db->where('estado', $filtros['estado']);
				$this->db->where($rol, $filtros['usuario']);
			}
			elseif ($filtros['rol'] > 0 && $filtros['usuario'] != NULL) {
				if ($filtros['rol'] == 1) {
					$rol = 'agente';
				} elseif ($filtros['rol'] == 2) {
					$rol = 'verificador';
				} elseif ($filtros['rol'] == 3) {
					$rol = 'aprobador';
				} elseif ($filtros['rol'] == 4) {
					$rol = 'usuario';
				}
				$this->db->where($rol, $filtros['usuario']);
			}
			elseif ($filtros['usuario'] != NULL && $filtros['estado'] != NULL) {
				$this->db->where('estado', $filtros['estado']);
				$_POST['search']['value'] = $filtros['usuario'];
			}
			elseif ($filtros['estado'] != NULL) {
				$this->db->where('estado', $filtros['estado']);
			}
			elseif ($filtros['usuario'] != NULL) {
				$_POST['search']['value'] = $filtros['usuario'];
			}

			if ($arg == 'movimiento') {
				$this->db->where('fecha >=', $fecha_i);
				$this->db->where('fecha <=', $fecha_f);
			} else if ($arg == 'registro') {
				$this->db->where('fecha_solicitud >=', $fecha_i);
				$this->db->where('fecha_solicitud <=', $fecha_f);
			} else {
				$this->db->where('fecha_verificacion >=', $fecha_i);
				$this->db->where('fecha_verificacion <=', $fecha_f);
			}
		}
		$i = 0;	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all($arg = NULL,$fecha_i = NULL,$fecha_f = NULL,$filtros = NULL)
	{
		$this->db->from($this->table);
		if ($arg != NULL && $fecha_i != NULL && $fecha_f != NULL) {
			if ($filtros['rol'] > 0 && $filtros['usuario'] != NULL && $filtros['estado'] != NULL) {
				if ($filtros['rol'] == 1) {
					$rol = 'agente';
				} elseif ($filtros['rol'] == 2) {
					$rol = 'verificador';
				} elseif ($filtros['rol'] == 3) {
					$rol = 'aprobador';
				} elseif ($filtros['rol'] == 4) {
					$rol = 'usuario';
				}
				$this->db->where('estado', $filtros['estado']);
				$this->db->where($rol, $filtros['usuario']);
			}
			elseif ($filtros['rol'] > 0 && $filtros['usuario'] != NULL) {
				if ($filtros['rol'] == 1) {
					$rol = 'agente';
				} elseif ($filtros['rol'] == 2) {
					$rol = 'verificador';
				} elseif ($filtros['rol'] == 3) {
					$rol = 'aprobador';
				} elseif ($filtros['rol'] == 4) {
					$rol = 'usuario';
				}
				$this->db->where($rol, $filtros['usuario']);
			}
			elseif ($filtros['usuario'] != NULL && $filtros['estado'] != NULL) {
				$this->db->where('estado', $filtros['estado']);
				$_POST['search']['value'] = $filtros['usuario'];
			}
			elseif ($filtros['estado'] != NULL) {
				$this->db->where('estado', $filtros['estado']);
			}
			elseif ($filtros['usuario'] != NULL) {
				$_POST['search']['value'] = $filtros['usuario'];
			}

			if ($arg == 'movimiento') {
				$this->db->where('fecha >=', $fecha_i);
				$this->db->where('fecha <=', $fecha_f);
			} else if ($arg == 'registro') {
				$this->db->where('fecha_solicitud >=', $fecha_i);
				$this->db->where('fecha_solicitud <=', $fecha_f);
			} else {
				$this->db->where('fecha_verificacion >=', $fecha_i);
				$this->db->where('fecha_verificacion <=', $fecha_f);
			}
		}
		return $this->db->count_all_results();
	}

	function count_filtered($arg = NULL,$fecha_i = NULL,$fecha_f = NULL,$filtros = NULL)
	{
		$this->_get_datatables_query($arg,$fecha_i,$fecha_f,$filtros);
		if ($arg != NULL && $fecha_i != NULL && $fecha_f != NULL) {
			if ($filtros['rol'] > 0 && $filtros['usuario'] != NULL && $filtros['estado'] != NULL) {
				if ($filtros['rol'] == 1) {
					$rol = 'agente';
				} elseif ($filtros['rol'] == 2) {
					$rol = 'verificador';
				} elseif ($filtros['rol'] == 3) {
					$rol = 'aprobador';
				} elseif ($filtros['rol'] == 4) {
					$rol = 'usuario';
				}
				$this->db->where('estado', $filtros['estado']);
				$this->db->where($rol, $filtros['usuario']);
			}
			elseif ($filtros['rol'] > 0 && $filtros['usuario'] != NULL) {
				if ($filtros['rol'] == 1) {
					$rol = 'agente';
				} elseif ($filtros['rol'] == 2) {
					$rol = 'verificador';
				} elseif ($filtros['rol'] == 3) {
					$rol = 'aprobador';
				} elseif ($filtros['rol'] == 4) {
					$rol = 'usuario';
				}
				$this->db->where($rol, $filtros['usuario']);
			}
			elseif ($filtros['usuario'] != NULL && $filtros['estado'] != NULL) {
				$this->db->where('estado', $filtros['estado']);
				$_POST['search']['value'] = $filtros['usuario'];
			}
			elseif ($filtros['estado'] != NULL) {
				$this->db->where('estado', $filtros['estado']);
			}
			elseif ($filtros['usuario'] != NULL) {
				$_POST['search']['value'] = $filtros['usuario'];
			}

			if ($arg == 'movimiento') {
				$this->db->where('fecha >=', $fecha_i);
				$this->db->where('fecha <=', $fecha_f);
			} else if ($arg == 'registro') {
				$this->db->where('fecha_solicitud >=', $fecha_i);
				$this->db->where('fecha_solicitud <=', $fecha_f);
			} else {
				$this->db->where('fecha_verificacion >=', $fecha_i);
				$this->db->where('fecha_verificacion <=', $fecha_f);
			}
		}
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}


}