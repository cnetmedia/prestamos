<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Cobros_proyectados_view_model extends CI_Model {

	var $table = 'cobros_proyectados_view';
	var $column_order = array('cliente','empresa','fecha_primer_descuento','fecha_pendiente','total_letra');
	var $column_search = array('cliente','empresa','fecha_primer_descuento','fecha_pendiente','total_letra'); 
	var $order = array('fecha_pendiente' => 'desc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables($fecha_i = NULL,$fecha_f = NULL)
	{
		$this->_get_datatables_query($fecha_i,$fecha_f);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query($fecha_i,$fecha_f)
	{		
		$this->db->from($this->table);
		if ($fecha_i != NULL && $fecha_f != NULL) {
			$this->db->where('fecha_pendiente >=', $fecha_i);
			$this->db->where('fecha_pendiente <=', $fecha_f);
		}
		$i = 0;	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all($fecha_i = NULL,$fecha_f = NULL)
	{
		$this->db->from($this->table);
		if ($fecha_i != NULL && $fecha_f != NULL) {
			$this->db->where('fecha_pendiente >=', $fecha_i);
			$this->db->where('fecha_pendiente <=', $fecha_f);
		}
		return $this->db->count_all_results();
	}

	function count_filtered($fecha_i = NULL,$fecha_f = NULL)
	{
		$this->_get_datatables_query($fecha_i,$fecha_f);
		if ($fecha_i != NULL && $fecha_f != NULL) {
			$this->db->where('fecha_pendiente >=', $fecha_i);
			$this->db->where('fecha_pendiente <=', $fecha_f);
		}
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nombre($nombre)
	{
		$this->db->from($this->table);
		$this->db->where('nombre',$nombre);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_all()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}


	public function sum_total(){
		$this->db->from($this->table);
		$query = $this->db->get();
		$array = array();
		foreach ($query->result() as $row) {
			$array[] = $row->total_letra;
		}
		return array_sum($array);
	}

	

	public function get_cant_empresas()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		$array = array();
		foreach ($query->result() as $row) {
			if (!in_array($row->empresa, $array)) {
				$array[] = $row->empresa;
			}			
		}
		return count($array);
	}


	public function get_cobros_pendientes_by_fechas($fecha_i,$fecha_f){
		$this->db->from($this->table);
		//$this->db->where('fecha_pendiente >=',$fecha_i);
		$this->db->where('fecha_pendiente <=',$fecha_f);
		$query = $this->db->get();
		return $query->result();
	}


}