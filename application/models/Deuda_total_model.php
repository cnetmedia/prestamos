<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Deuda_total_model extends CI_Model {

	var $table = 'deuda_clientes';
	var $column_order = array('cliente','fecha_desembolso');
	var $column_search = array('estado', 'cliente');
	var $order = array('cliente' => 'asc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if(array_key_exists('length', $_POST) && $_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query()
	{
        $from = $this->formatDate($_POST['from'] . ' 00:00:00');
        $to = $this->formatDate($_POST['to'] . ' 23:59:59');

		$this->db->from($this->table);

		$this->db->where('fecha_entrega >= ', $from);
		$this->db->where('fecha_entrega <= ', $to);

		foreach ($this->column_search as $item)
		{
            if ('estado' == $item &&  array_key_exists('estado', $_POST)) {
                if ('Cancelados' == $_POST['estado']) {
                    $this->db->where('plazo_faltante = ', 0);
                } else {
                    if ('Todos' != $_POST['estado']) {
                        $this->db->where($item, $_POST['estado']);
                        $this->db->where('plazo_faltante != ', 0);
                    }
                }
            }

			if('cliente' == $item && array_key_exists('search', $_POST) && array_key_exists('value', $_POST['search']))
			{
                $this->db->like($item, $_POST['search']['value']);
			}

		}

		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_all()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_sumary()
    {
        $from = $this->formatDate($_POST['from'] . ' 00:00:00');
        $to = $this->formatDate($_POST['to'] . ' 23:59:59');

        $this->db->select("SUM(cantidad_aprobada) AS neto, SUM(deuda_total) AS bruto, COUNT(id) AS cantidad");
        $this->db->where('fecha_entrega >= ', $from);
        $this->db->where('fecha_entrega <= ', $to);

        if (array_key_exists('estado', $_POST)) {
            if ('Cancelados' == $_POST['estado']) {
                $this->db->where('plazo_faltante = ', 0);
            } else {
                if ('Todos' != $_POST['estado']) {
                    $this->db->where('estado', $_POST['estado']);
                    $this->db->where('plazo_faltante != ', 0);
                }
            }
        }

        return $this->db->get('deuda_clientes')->row();
    }

    private function formatDate($value)
    {
        $date = DateTime::createFromFormat('d/m/Y H:i:s', $value);

        return $date->format('Y/m/d H:i:s');
    }

    public function totals($estado)
    {
        if ('Todos' != $estado) {
            return [
                'total_cancelados' => 0,
                'total_activos' => 0,
                'total_desempleados' => 0,
                'total_sin_garantias' => 0
            ];
        }

        $cancelados = $this->get_cancelados();
        $totales = $this->get_totales_estados();

        return [
            'total_cancelados' => $cancelados->cancelados,
            'total_activos' => $totales['total_activos'],
            'total_desempleados' => $totales['total_desempleados'],
            'total_sin_garantias' => $totales['total_sin_garantias']
        ];
    }

    private function get_cancelados()
    {
        $from = $this->formatDate($_POST['from'] . ' 00:00:00');
        $to = $this->formatDate($_POST['to'] . ' 23:59:59');

        $this->db->select('COUNT(id) AS cancelados');
        $this->db->where('plazo_faltante', 0);
        $this->db->where('fecha_entrega >= ', $from);
        $this->db->where('fecha_entrega <= ', $to);

        return $this->db->get('deuda_clientes')->row();
    }

    private function get_totales_estados()
    {
        $from = $this->formatDate($_POST['from'] . ' 00:00:00');
        $to = $this->formatDate($_POST['to'] . ' 23:59:59');

        $totales = [
            'total_activos' => 0,
            'total_desempleados' => 0,
            'total_sin_garantias' => 0,
        ];

        $this->db->select('estado, count(id) AS total');
        $this->db->where('plazo_faltante != ', 0);
        $this->db->where('fecha_entrega >= ', $from);
        $this->db->where('fecha_entrega <= ', $to);
        $this->db->group_by('estado');

        $rows = $this->db->get('deuda_clientes')->result();

        foreach ($rows as $row) {
            switch ($row->estado) {
                case 'Activo':
                    $totales['total_activos'] = $row->total;
                    break;
                case 'Desempleado':
                    $totales['total_desempleados'] = $row->total;
                    break;
                case 'Sin Garantías':
                    $totales['total_sin_garantias'] = $row->total;
                    break;
            }
        }

        return $totales;
    }

}