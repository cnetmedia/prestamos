<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Login_model extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function loginUser($correo,$password)
	{
		$this->db->where('correo',$correo);
		$this->db->where('password',$password);
		/*$this->db->where('estado','Activo');*/
		$query = $this->db->get('usuarios');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}else{
			$this->session->set_flashdata('usuario_incorrecto','Los datos ingresados son incorrectos.');
			redirect('login','refresh');
		}
	}

	public function loginRol($id)
	{
		$this->db->where('id',$id);
		$query = $this->db->get('roles');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}
	}

	public function updateUltimaConexion($id,$ultima_conexion) {
		$data = array(
			'ultima_conexion' => $ultima_conexion
		);
		$this->db->where('id', $id);
		return $this->db->update('usuarios', $data);
	}

	public function verify_email($correo)
	{
		$this->db->where('correo',$correo);
		$query = $this->db->get('usuarios');
		if($query->num_rows() == 1)
		{
			return $query->row();
		}
	}

	public function updatePassword($id,$password) {
		$data = array(
			'password' => sha1($password)
		);
		$this->db->where('id', $id);
		return $this->db->update('usuarios', $data);
	}

	public function resetPassword($id,$password) {
		$data = array(
			'password' => sha1($password),
			'ingreso' => 0
		);
		$this->db->where('id', $id);
		return $this->db->update('usuarios', $data);
	}

	public function updatePasswordIngreso($id,$password) {
		$data = array(
			'password' => sha1($password),
			'ingreso' => 1
		);
		$this->db->where('id', $id);
		return $this->db->update('usuarios', $data);
	}
}