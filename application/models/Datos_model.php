<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Datos_model extends CI_Model {

	var $column_order = array('id','seguro','cedula','nombre','razon_so','tel1','tel2','fecha','salario');
	var $column_search = array('id','seguro','cedula','nombre','razon_so','tel1','tel2','fecha','salario'); 
	var $order = array('id' => 'desc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables($table)
	{
		$this->_get_datatables_query($table);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query($table)
	{		
		$this->db->from($table);
		$i = 0;	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all($table)
	{
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	function count_filtered($table)
	{
		$this->_get_datatables_query($table);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function get_like_cedula($tabla,$cedula)
	{
		$this->db->from($tabla);
		$this->db->like('cedula',$cedula);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_cedula($tabla,$cedula)
	{
		$this->db->from($tabla);
		$this->db->where('cedula',$cedula);
		$query = $this->db->get();
		return $query->result();
	}


	public function get_by_nombre($tabla,$nombre)
	{
		$this->db->from($tabla);
		$this->db->like('nombre',$nombre);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_razon_so($tabla,$razon_so)
	{
		$this->db->from($tabla);
		$this->db->where('razon_so',$razon_so);
		$query = $this->db->get();
		return $query->result();
	}



	public function verifytable($tabla)
	{
		$this->db->from($tabla);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	public function sentenciaSql($sql)
	{
		$query = $this->db->query($sql);
		return $query;
	}

	public function prueba($sql)
	{
		$query = $this->db->query($sql);
		return $query;
	}


	public function numempleados($tabla,$razon_so){

		$this->db->from($tabla);
		$this->db->where('razon_so',$razon_so);
		$query = $this->db->get();
		return $query->num_rows();
	}



}