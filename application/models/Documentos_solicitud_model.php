<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Documentos_solicitud_model extends CI_Model {

	var $table = 'documentos_solicitud';
	
	public function __construct() {
		parent::__construct();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_by_solicitudes_id($solicitudes_id)
	{
		$this->db->from('archivos');
		$this->db->where('solicitudes_id',$solicitudes_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_all_docuentns()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

}