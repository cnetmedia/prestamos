<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Ref_ap_model extends CI_Model {

	var $table = 'ref_ap';
	var $column_order = array('identificacion1','identificacion2','identificacion3','identificacion4');
	var $column_search = array('identificacion1','identificacion2','identificacion3','identificacion4');
	var $order = array('identificacion1' => 'desc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query()
	{		
		$this->db->from($this->table);
		$i = 0;	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_all()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	 public function get_for_excel()
	 {
	 	$this->db->truncate('cotizacion');
		$this->db->from('aprobacion');
		$query = $this->db->get();
		$aprobaciones = $query->result();
		if ($aprobaciones) {
			foreach ($aprobaciones as $apro) {


				$suma = $apro->cantidad_aprobada;
				$productos_id = $apro->productos_id;
				$plazo = $apro->cantidad_meses;

				//$fecha = date("d-m-Y");
				$fecha = $apro->fecha_primer_descuento;

				$tasa = 0.00;
				$cargo = 0.00;

				if ($productos_id) {
					$products = $this->cotizacion_model->get_products_by_id($productos_id);
					foreach ($products as $row) {
						$tasa = $row->tasa_interes_mensual;
						$cargo = $row->cargo_administrativo;
					}
				}


				$suma = round($suma,2);
				$tasa = round($tasa,2);
				$cargo = round($cargo,2);

				$interes = $tasa / 100;

				$p = $suma;
				$x = $interes;
				$n = $plazo;

				$m = $p * ( $x / ( 1 - ( ( 1 + $x ) ** - $n ) ) );

				$cuotaperiodica = $m;
				$cuotaperiodica = round($cuotaperiodica,2);

				$cargototal = $cargo * ($suma / 100);
				$cargototal = round($cargototal,2);

				$pagado = array();	
				$interesasignado = array();

				$sum = $suma;
				for ($i=0; $i<$plazo; $i++) {
					$interesi = $sum * $interes;
					$interesi = round($interesi,2);
					$valori = $cuotaperiodica - $interesi;
					$valori = round($valori,2);
					$restoi = $sum - $valori;
					$restoi = round($restoi,2);
					$sum = $restoi;
					$pagado[$i] = $valori;
					$interesasignado[$i] = $interesi;
				}


				$totalintereses = array_sum($interesasignado);
				$totalintereses = round($totalintereses,2);
				$totalcargo = $cargototal * $plazo;
				$totalcargo = round($totalcargo,2);

				$totalsuma = array();
				$intereses = $totalcargo + array_sum($interesasignado);
				$intereses = round($intereses,2);
				$totalcobrar = array();
				$diferenciasuma = $suma - (array_sum($pagado));
				$diferenciasuma = round($diferenciasuma,2);

				if($diferenciasuma < 0){
					$dec = explode("-0.", $diferenciasuma);
					$num = $dec[1] * 1;
					$m = 0;
					for ($i=0; $i < $num; $i++) {
						$m++;
						$pagado[$plazo-$m] = $pagado[$plazo-$m] - 0.01;
					}
				} elseif($diferenciasuma > 0){
					$dec = explode("0.", $diferenciasuma);
					$num = $dec[1] * 1;
					for ($i=0; $i < $num; $i++) {
						$pagado[$i] = $pagado[$i] + 0.01;
					}
				}

				$data = array();
				$n = 0;
				for ($i=0; $i<$plazo; $i++){
					$n++;
					$fe = date("d-m-Y", strtotime("$fecha + $n months"));
					$data[] = array(
						'fecha' => $fe,
						'principal' => $pagado[$i],
						'interes' => $interesasignado[$i],
						'tasa' => $tasa*1,
						'cargo' => $cargototal,
						'total' => $pagado[$i] + $interesasignado[$i] + $cargototal
					);
					$totalsuma[] = $pagado[$i];
					$totalcobrar[] = $pagado[$i] + $interesasignado[$i] + $cargototal;
				}

				$output = array(
					"suma" => $suma,
					"cargo" => $totalcargo,
					"interes" => $totalintereses,
					"total" => array_sum($totalcobrar),
					"aprobacion_id" => $apro->id
				);

				$insert = $this->cotizacion_model->save($output);

			}
		}

		$fields = $this->db->field_data($this->table);
		$query = $this->db->select('*')->get($this->table);
		return array("fields" => $fields, "query" => $query);
	 }

}