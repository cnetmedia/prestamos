<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Aprobacion_model extends CI_Model {

	var $table = 'aprobacion';
	var $column_order = array('clientes_id','productos_id','cantidad_aprobada','cantidad_meses','fecha_primer_descuento','pago_mensual','pago_total','fecha_entrega','porcentaje');
	var $column_search = array('clientes_id','productos_id','cantidad_aprobada','cantidad_meses','fecha_primer_descuento','pago_mensual','pago_total','fecha_entrega','porcentaje');
	var $order = array('id' => 'desc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query()
	{		
		$this->db->from($this->table);
		$i = 0;	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_entregado_by_clientes_id($clientes_id)
	{
		$this->db->from($this->table);
		$this->db->where('clientes_id',$clientes_id) and $this->db->where('entregado', 1) and $this->db->where('cobrado', 0);
		$query = $this->db->get();
		return $query->row();
	}


	public function get_archivos_by_clientes_id($clientes_id)
	{
		$this->db->from('archivos');
		$this->db->where('clientes_id',$clientes_id);
		$query = $this->db->get();
		return $query->result();
	}


	public function get_by_clientes_id($clientes_id)
	{
		$this->db->from($this->table);
		$this->db->where('clientes_id',$clientes_id);
		$query = $this->db->get();
		return $query->row();
	}


	public function get_all()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}


	public function get_num_view($id){
		$this->db->from('numeros_view');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}	

	public function get_num_view_new($id){
		$this->db->from('numeros_view_new');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}


}