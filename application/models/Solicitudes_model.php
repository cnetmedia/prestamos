<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Solicitudes_model extends CI_Model {

	var $table = 'solicitudes';
	var $column_order = array('fecha_creacion','clientes_id','usuarios_id');
	var $column_search = array('fecha_creacion','clientes_id','usuarios_id');
	var $order = array('id' => 'desc');
	
	public function __construct() {
		parent::__construct();
	}

	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_solicitud_by_cliente($clientes_id)
	{
		$this->db->from($this->table);
		$this->db->where('clientes_id',$clientes_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_solicitud_by_clientes_id($clientes_id)
	{
		$this->db->from($this->table);
		$this->db->where('clientes_id',$clientes_id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_solicitud_by_cliente_and_entregados($clientes_id)
	{
		$this->db->from($this->table);
		$this->db->where('clientes_id',$clientes_id) and $this->db->where('estados_id',7);
		$query = $this->db->get();
		return $query->row();
	}


	public function get_client_by_cedula($cedula)
	{
		$this->db->from('clientes');
		$this->db->where('cedula',$cedula);
		$query = $this->db->get();
		return $query->row();
	}


	public function get_solicitud_by_clientes_id_and_usuarios_id($clientes_id,$usuarios_id)
	{
		$this->db->from($this->table);
		$this->db->where('clientes_id',$clientes_id) and $this->db->where('usuarios_id',$usuarios_id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_verificacion_rp_by_clientes_id($clientes_id)
	{
		$this->db->from('verificacion_rp');
		$this->db->where('clientes_id',$clientes_id);
		$query = $this->db->get();
		return $query->row();
	}


	public function get_archivos_by_clientes_id($clientes_id)
	{
		$this->db->from('archivos');
		$this->db->where('clientes_id',$clientes_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_all_solicitudes()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_solicitud_by_fecha($fecha)
	{
		$this->db->from($this->table);
		$this->db->like('fecha_registro', $fecha);
		//return $this->db->get()->result_array();
		return $this->db->count_all_results();
	}

	public function get_fecha_agente_by_users($fecha_i, $fecha_f, $user)
	{
		$this->db->from($this->table);
		$this->db->where('fecha_agente >=', $fecha_i);
		$this->db->where('fecha_agente <=', $fecha_f);
		if ($user != NULL) {
			$this->db->where('agente', $user);
		}
		$this->db->order_by('fecha_agente','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_fecha_verificacion_by_users($fecha_i, $fecha_f, $user)
	{
		$this->db->from($this->table);
		$this->db->where('fecha_verificacion >=', $fecha_i);
		$this->db->where('fecha_verificacion <=', $fecha_f);
		if ($user != NULL) {
			$this->db->where('verificador', $user);
		}
		$this->db->order_by('fecha_verificacion','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_fecha_aprobado_by_users($fecha_i, $fecha_f, $user)
	{
		$this->db->from($this->table);
		$this->db->where('fecha_aprobado >=', $fecha_i);
		$this->db->where('fecha_aprobado <=', $fecha_f);
		if ($user != NULL) {
			$this->db->where('aprobador', $user);
		}
		$this->db->order_by('fecha_aprobado','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_fecha_firma_by_users($fecha_i, $fecha_f, $user)
	{
		$this->db->from($this->table);
		$this->db->where('fecha_firma >=', $fecha_i);
		$this->db->where('fecha_firma <=', $fecha_f);
		if ($user != NULL) {
			$this->db->where('firma', $user);
		}
		$this->db->order_by('fecha_firma','asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_solicitudes_by_disposicion($disposiciones_id)
	{
		$this->db->from($this->table);
		$this->db->where('disposiciones_id',$disposiciones_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_all_solicitudes_app($user)
	{
		$this->db->select('cli.id as id_cliente, cli.nombre, cli.apellido, cli.cedula, cli.telefono, 
			cli.correo, cli.cantidad_solicitada, cli.salario, cli.tiempo_laborando, cli.posicion_trabajo, 
			cli.usuarios_id, cli.fecha_creacion, cli.datos_solicitud, cli.descuento, cli.cantidad_descuento ');
		$this->db->select('sol.id as id_solicitud ');
		$this->db->select('dsol.cedula as doc_cedula, dsol.pasaporte, dsol.seguro, dsol.trabajo, dsol.otro ');
		$this->db->from('solicitudes sol');
		$this->db->join('clientes cli','sol.clientes_id = cli.id','left');
		$this->db->join('documentos_solicitud dsol','sol.id = dsol.solicitudes_id','left');
		$this->db->where('sol.si_app_solicitud', 1);
		$this->db->where("DATE(sol.fecha_creacion) = '".date('Y-m-d')."'");
		$this->db->where("cli.datos_solicitud LIKE '%".$user."%'");
		$this->db->order_by('sol.id','desc');
		$query = $this->db->get();
		return $query->result();
	}
}