<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 */
class Cobros_view_model extends CI_Model {

	var $table = 'cobros_view cobv';
	var $column_order = array('cobv.cliente','cobv.cedula','cobv.cantidad_aprobada','cobv.fecha_entrega','cobv.fecha_primer_descuento','cobv.ultimo_pago','cobv.monto','cobv.fecha_pendiente','cobv.compania','cobv.estado_cobro');
	var $column_search = array('cobv.cliente','cobv.cedula','cobv.cantidad_aprobada','cobv.fecha_entrega','cobv.fecha_primer_descuento','cobv.ultimo_pago','cobv.monto','cobv.fecha_pendiente','cobv.compania','cobv.estado_cobro');
	var $order = array('cobv.fecha_pendiente' => 'asc');
	
	public function __construct() {
		parent::__construct();
	}

	function get_datatables($filtro=NULL,$estado=NULL)
	{
		$this->_get_datatables_query($filtro,$estado);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	private function _get_datatables_query($filtro=NULL,$estado)
	{		
		$this->db->select("cobv.*");
		$this->db->from($this->table);
		$this->db->join('clientes cli','cli.id = cobv.clientes_id');
		if(!empty($filtro) && $filtro == "singarantias"){
			$this->db->where("cli.estados_cobro_id IN (9,10,11,12,13,14,15)");
		}else{
			$this->db->where("(cli.estados_cobro_id NOT IN (9,10,11,12,13,14,15) OR cli.estados_cobro_id IS NULL)");
		}

		if(!empty($estado)){
			$this->db->where("cli.estados_cobro_id",$estado);
		}

		$i = 0;	
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function count_all($filtro=NULL,$estado=NULL)
	{
		$this->_get_datatables_query($filtro,$estado);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_filtered($filtro=NULL,$estado=NULL)
	{
		$this->_get_datatables_query($filtro,$estado);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	public function get_all()
	{
		$this->db->from($this->table);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_state($state)
	{
		$this->db->from($this->table);
		$this->db->where('estado',$state);
		$query = $this->db->get();
		return $query->result();
	}


	public function get_by_company($compania)
	{
		$this->db->from($this->table);
		$this->db->where('compania',$compania);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_morosos($fecha)
	{
		$this->db->from($this->table);
		$fecha = date("Y-m-d", strtotime("$fecha - 3 months"));
		$this->db->where('fecha_pendiente <=',$fecha);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_clientes_by_estado($estado)
	{		
		$query = $this->_get_clientes_by_estado($estado);
		return $query->result();
	}

	public function get_clientes_by_estado_to_excel($estado)
	{		
		$fields = [
			(object)['name' => 'Cliente'],
			(object)['name' => 'Documento de identidad'],
			(object)['name' => 'Celular'],
			(object)['name' => 'Telefono'],
			(object)['name' => 'Correo electronico'],
			(object)['name' => 'Estado Actual'],
		];
		$fields = (object)$fields;
		$query = $this->_get_clientes_by_estado($estado,TRUE);
		return array("fields" => $fields, "query" => $query);
	}

	public function _get_clientes_by_estado($estado, $excel = FALSE)
	{
		/*Pago pendiente*/
		$this->db->from($this->table);
		$fecha = date("Y-m-d");

		if($excel){
			$this->db->select("cobv.cliente,cobv.cedula,cli.celular,cli.telefono,cli.correo");
			$this->db->select("( CASE 
									WHEN cobv.fecha_pendiente <= '".$fecha ."' THEN 'Moroso'
	           						WHEN cobv.fecha_pendiente > '".$fecha ."' THEN 'Al dia' 
	           						WHEN cli.estados_cobro_id = 9 THEN 'Desempleado' 
	           						END ) AS estado");			
		}else{
			$this->db->select("cobv.clientes_id,cobv.cliente,cobv.cedula,cobv.fecha_pendiente");
			$this->db->select("cli.telefono,cli.celular,cli.correo");			
			$this->db->select("( CASE 
									WHEN cobv.fecha_pendiente <= '".$fecha ."' THEN 'morosos'
	           						WHEN cobv.fecha_pendiente > '".$fecha ."' THEN 'aldia' 
	           						WHEN cli.estados_cobro_id = 9 THEN 'desempleado'
	           						END ) AS estado");
		}
		$this->db->select("( CASE 
									WHEN cobv.fecha_pendiente <= '".$fecha ."' THEN 'moroso'
	           						WHEN cobv.fecha_pendiente > '".$fecha ."' THEN 'aldia' 
	           						WHEN cli.estados_cobro_id = 9 THEN 'desempleado' 
	           						END ) AS estado_str");

		$this->db->join('clientes cli','cli.id = cobv.clientes_id');
		
		$subQuery1 = $this->db->get_compiled_select();
		$this->db->reset_query();

		/*Pago completado*/
		if($excel){
			$this->db->select("cv.cliente,cv.cedula,cli.celular,cli.telefono,cli.correo");
			$this->db->select("'Completado' AS estado");			
		}else{
			$this->db->select("cv.cliente_id as clientes_id,cv.cliente,cv.cedula, '' as fecha_pendiente");
			$this->db->select("cli.telefono,cli.celular,cli.correo");			
			$this->db->select("'completado' AS estado");
		}
		$this->db->select("'completado' AS estado_str");
		
		$this->db->from("cobrados_view cv");
		$this->db->join('clientes cli','cli.id = cv.cliente_id');

		$subQuery2 = $this->db->get_compiled_select();
		$this->db->reset_query();

		
		if($excel){
			$this->db->select("cliente,cedula,celular,telefono,correo,estado");			
		}else{
			$this->db->select("clientes_id,cliente,cedula, fecha_pendiente, telefono,celular,correo, estado");
		}

		$this->db->from("( $subQuery1 UNION $subQuery2 ) x ");
		if(!empty($estado)){
			$this->db->where("( estado = '".$estado."' or estado_str = '".$estado."')");			
		}

		$query = $this->db->get();
		return $query;	
	}

	public function verify_morosos($id,$fecha)
	{
		$this->db->from($this->table);
		$fecha = date("Y-m-d", strtotime("$fecha - 3 months"));
		$this->db->where('id', $id);
		$this->db->where('fecha_pendiente <=',$fecha);
		$query = $this->db->get();
		return $query->row();
	}

}